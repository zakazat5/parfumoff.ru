<?php
if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
} else {
    $id = 0;
}

$arApiKeys = array(
    'parfumoff-ru' => 'z70kcnZP9AF7jzl0GYFjdiQ7i5i1piFD',
    'enigme-ru' => 'bSvaGTtoUdMNkUT6zXIwWU994fAR6v3T'
);

if (isset($_GET['site'])) {
    if(isset($arApiKeys[$_GET['site']])) {
        $apiKey = $arApiKeys[$_GET['site']];
    } else {
        $apiKey = false;
    }
} else {
    $apiKey = false;
}

if ($id > 0 && $apiKey) {
    //ini_set('display_errors', 1);
    //error_reporting(E_ALL);

    require '../retailcrm/bootstrap.php';

    $container = Container::getInstance();
    $xmlDocument = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp');

    $client = new RequestProxy(
        $container->settings['api']['url'],
        $apiKey
    );

    $xmlDom = new SimpleXMLElement($xmlDocument);

    foreach ($xmlDom->Valute as $valute) {
        if ($valute->CharCode == 'USD') {
            $course = reset($valute->Value);
            $course = str_replace(',', '.', $course);

            $order = array(
                'id' => $id,
                'customFields' => array(
                    'course' => round($course, 2)
                )
            );

            $thief = function (RequestProxy $client) {
                return $client->api;
            };

            $thief = Closure::bind($thief, null, $client);
            $api = $thief($client);

            $result = $api->ordersEdit($order, 'id');

            break;
        }
    }
}