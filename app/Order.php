<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 03.08.14
 * Time: 16:34
 */

require_once "Order/Row/Item.php";

class Order
{

    /**
     * Навзание таблицы
     * @var string
     */
    private $_table = "m_mag_Orders";

    /**
     * Получить заказ по id
     *
     * @param $id int
     *
     * @return Order_Row_Item|null
     */
    public function getById($id)
    {
        $content = mysql_fetch_assoc(
            mysql_query(
                "SELECT *, DATE_FORMAT(DateEnd, '%d.%m.%Y') AS DateEnd, DateEnd as Delivery_Date FROM {$this->_table} WHERE id = {$id} LIMIT 1"
            )
        );

        if ($content) {

            return new Order_Row_Item(array(
                'data' => $content,
                'table' => $this->_table,
                'stored' => true
            ));
        } else {
            return null;
        }
    }

    /**
     * Получение заказов из массива id
     *
     * @param $ids array
     *
     * @return array|null
     */
    public function getByArray($ids)
    {
        if (!is_array($ids)) {
            return null;
        }

        $ids = implode(", ", $ids);
        $result = array();
        $sql = mysql_query(
            "SELECT *, DATE_FORMAT(DateEnd, '%d.%m.%Y') AS DateEnd, DateEnd as Delivery_Date FROM {$this->_table} WHERE id IN ({$ids})"
        );

        while ($content = mysql_fetch_assoc($sql)) {

            $result[] = new Order_Row_Item(array(
                'data' => $content,
                'table' => $this->_table,
                'stored' => true
            ));
        }

        return $result;
    }
} 