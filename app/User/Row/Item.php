<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 01.08.14
 * Time: 17:45
 */

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Abstract.php";

class User_Row_Item extends Row
{

    /**
     * Определяет связи id в select с названиями районов столиц
     * @var array
     */
    private static $_districts = array(
        '7700000000000' => 'Москва в пределах МКАД',
        '0000000000001' => 'Жулебино',
        '0000000000002' => 'Косино',
        '0000000000003' => 'Новокосино',
        '0000000000004' => 'Кожухово',
        '0000000000005' => 'Бутово',
        '0000000000006' => 'Солнцево и Ново-Переделкино',
        '0000000000007' => 'Ново-Переделкино',
        '0000000000009' => 'Митино',
        '0000000000010' => 'Куркино',
        '7800000000000' => 'Санкт-Петербург в пределах КАД',
        '7800000900000' => 'Пушкин',
        '7800000700000' => 'Павловск',
        '780000400000' => 'Красное Село',
        '7800000800000' => 'Петергоф'
    );

    /**
     * Форматирование телефона из вида 7904**
     * в вид 8904**
     *
     * @param null $phone
     *
     * @return null|string
     */
    static function formatPhone($phone = null)
    {
        if ($phone == null) {
            return "";
        }

        if ($phone[0] == 7) {
            $phone = "8" . substr($phone, 1);
        }

        return $phone;
    }

    /**
     * Функция получения всей необходимой информации о пользователе
     *
     * @param $userId int
     * @param $content Order_Row_Item
     *
     * @return array
     */
    static function collectUserInfo($userId, $content)
    {
        $user = mysql_fetch_assoc(mysql_query("SELECT * FROM m_mag_Users WHERE id='{$userId}' LIMIT 1 "));
        //подчищаем пробелы
        foreach($user as &$col){
            $col = trim($col);
        }

        // если пользователя не было у заказа
        // то есть заказ создал, через старую форму быстрого заказа
        // без привязки к пользователю
        if ($user == false) {

            $user['address'] = $content->pAddress;
            $user['fullName'] = $content->pFio;
            $user['phone'] = self::formatPhone($content->pPhone);

            // список обязательных параметров пользователя
            $user['city'] = "Москва";
            $user['region'] = "";
            $user['rayon'] = "";
            $user['postcode'] = "";
            $user['email'] = "";
            $user['address_street'] = "";
            $user['address_settlement'] = "";
            $user['address_house'] = "";
            $user['address_flat'] = "";
            $user['address_stroenie'] = "";
            $user['address_corpus'] = "";
        } else {

            // если адрес пользователя обработан,
            // то у него заполнено это поле
            // иначе обрабатываем адрес по старой схеме
            if (trim($user['address_city']) === "") {

                // г. Москва записывается в регион, частенько
                if (trim($user['address_region']) !== "") {
                    $user['city'] = $user['address_region'];
                }

                $user['rayon'] = "";
            } else {

                $user['city'] = $user['address_city'];
                $user['region'] = $user['address_region'];
                $user['rayon'] = self::$_districts[$user['address_part']];
                $user['address'] = preg_replace(
                    '/ , ,*/',
                    ' ',
                    trim(
                        trim(
                            implode(
                                ', ',
                                array(
                                    $user['address_street'],
                                    $user['address_house'] == null ? "" : "д " . $user['address_house'],
                                    $user['address_corpus'] == null ? "" : "корп " . $user['address_corpus'],
                                    $user['address_stroenie'] == null ? "" : "стр " . $user['address_stroenie'],
                                    $user['address_flat'] == null ? "" : "кв " . $user['address_flat']
                                )
                            ),
                            " "
                        ),
                        ","
                    )
                );
                //чистим лишнее
                $user['address'] = trim($user['address']);
                $user['address'] = trim($user['address'],',');

                // если у адреса есть "поселение" address_settlement,
                // то в данном случае районом считать значение поля address_city,
                // а городом address_settlement
                if ($user['address_settlement'] != "") {
                    $user['city'] = $user['address_settlement'];
                    $user['rayon'] = $user['address_city'];
                }
            }

            $user['fullName'] = trim($user['name']) === "" ? $user['fio'] : trim(
                implode(
                    ' ',
                    array(
                        $user['surname'],
                        $user['name'],
                        $user['patronymic']
                    )
                )
            );

            $user['phone'] = self::formatPhone($user['phone']);
        }

        $user['fullAddress'] = preg_replace(
            '/ , /',
            ' ',
            trim(
                trim(
                    implode(
                        ', ',
                        array(
                            $user['region'],
                            $user['rayon'],
                            $user['city'],
                            $user['address_street'],
                            $user['address_house'] == null ? "" : "д " . $user['address_house'],
                            $user['address_corpus'] == null ? "" : "корп " . $user['address_corpus'],
                            $user['address_stroenie'] == null ? "" : "стр " . $user['address_stroenie'],
                            $user['address_flat'] == null ? "" : "кв " . $user['address_flat']
                        )
                    ),
                    " "
                ),
                ","
            )
        );
        //чистим лишнее
        $user['fullAddress'] = trim($user['fullAddress']);
        $user['fullAddress'] = trim($user['fullAddress'],',');

        // иногда появляется mosсow в регионе походу
        $user['fullAddress'] = preg_replace('/(moscow, |region, )/', "", $user['fullAddress']);

        // вычищаем лишние пробелы по краям
        foreach ($user as $k => $v) {
            $user[$k] = trim($v);
        }

        return $user;
    }

    /**
     * Получить ФИО правильного образца, если фио не распозналось, выводить исходник
     * @return mixed|string
     */
    public function getFio()
    {
        return ($this->name != '' || $this->surname != '' || $this->patronymic != '') ? $this->surname . ' ' . $this->name . ' ' . $this->patronymic : $this->fio;
    }

}