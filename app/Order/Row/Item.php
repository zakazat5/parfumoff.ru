<?php
/**
 * Created by PhpStorm.
 * User: loram
 * Date: 03.08.14
 * Time: 14:19
 */

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Abstract.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User.php";

class Order_Row_Item extends Row
{
    /**
     * Устанавливает название родительского класса
     * от которого будет создаваться setRow
     * @var string
     */
    protected $rowClass = "Order_Row_Item";

    /**
     * Дефолтовая инфа по Почте России
     * @var array 
     */
    private $_post = array(
        'delivery_days' => '5-12',
        'delivery_type' => 'самовывоз',
        'delivery_payment' => 'наличными при получении',
        'delivery_cost_1' => 200,
        'delivery_cost_3' => 290,
        'courier_id' => 5,
        'courier_name' => 'Почта России',
        'id' => 0,
        'name_city' => null,
        'delivery_news_to_operator' => "Заказы высылаются в течение 1 дня ускоренной посылкой 1 класса. Получить и оплатить товар Вы сможете на своем почтовом отделении. В момент прибытия посылки Вы получите извещение от Почты России. При оплате наложенным платежом Почта России взимает комиссию от 2 до 5 % за перевод денег на наш счет."
    );

    /**
     * Соотношение id к названию типов оплаты заказа
     * @var array
     */
    private $_paymentType = array(
        0 => "Не указано",
        1 => "Наличными при получении",
        2 => "Предоплата по квитанции",
        3 => "Предоплата Robokassa"
    );

    /**
     * Массив по логистической компании
     * @var null|array
     */
    private $_logistic = null;

    /**
     * Массив товаров в заказе
     * @var null|array
     */
    private $_products = null;

    /**
     * Получение ID заказа с префиксом
     * @return string
     */
    public function getFullId() {

        return $this->isRegion() ? "RPF-" . $this->getRow()->id : "MPF-" . $this->getRow()->id;
    }

    /**
     * Функция получения логистической компании для заказа
     * @return array
     */
    public function getLogistic()
    {
        if ($this->_logistic == null) {

            $id = $this->courier_address_id;

            // если курьерка не проставлена, то выводим Почту России
            if ((int)$id === 0) {
                $this->_logistic = $this->_post;
            } else {
                $this->_logistic = mysql_fetch_assoc(
                    mysql_query("SELECT * FROM couriers_addresses WHERE id='{$id}' LIMIT 1 ")
                );
            }
        }

        return $this->_logistic;
    }

    /**
     * Получение информации о службе доставки
     * @return string
     */
    public function getCourier()
    {
        $logistic = $this->getLogistic();
        $courier = mysql_fetch_assoc(
            mysql_query("SELECT * FROM couriers WHERE couriersId='{$logistic[courier_id]}' LIMIT 1 ")
        );

        if ($courier === false) {
            return "Не указан";
        }

        return $courier['name'];
    }

    /**
     * Получение кода пункта самовывоза
     * @return string|null
     */
    public function getDeliveryCode()
    {
        $logistic = $this->getLogistic();
        if (!isset($logistic['delivery_code'])) {
            return null;
        }

        $delivery_code = preg_replace("/\.0000/", "", $logistic['delivery_code']);
        return $delivery_code == 0 ? null : $delivery_code;
    }

    /**
     * Получение даты доставки в формате dd-MM-YYYY, HH:mm
     * либо получение сообщения о том, что дата доставки не указана
     * @return string
     */
    public function getDeliveryDate()
    {
        // заполняем нестандартное поле
        if (!isset($this->Delivery_Date)) {
            $this->Delivery_Date = $this->DateEnd;
        }

        if ($this->Delivery_Date === "" || $this->Delivery_Date === "0000-00-00 00:00:00") {
           return "Не указана";
        }

        $locale = new Zend_Locale("ru_RU");
        $date = new Zend_Date($this->Delivery_Date, null, $locale);
        return $date->toString("dd-MM-YYYY, HH:mm");
    }

    /**
     * Получение всех товаров в заказе
     * @return array|null
     */
    public function getProducts()
    {
        if ($this->_products == null) {

            $id = $this->id;
            $sql = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$id}'");

            while ($item = mysql_fetch_assoc($sql)) {

                // дозаполняем массив товара артикулом
                $productId = $item['id_catalog_data_order'];
                $product = mysql_fetch_assoc(mysql_query("SELECT * FROM m_catalog_data_order WHERE id='{$productId}'"));
                $item['articul'] = $product['articul'];

                $this->_products[] = $item;
            }
        }

        return $this->_products;
    }

    /**
     * Получение кол-ва товаров в заказе
     * @return int
     */
    public function getProductsCount()
    {
        $id = $this->id;
        $item = mysql_fetch_assoc(mysql_query("SELECT COUNT(*) as cnt FROM m_mag_OrdersSum WHERE id_order='{$id}'"));
        return (int)$item['cnt'];
    }

    /**
     * Получение стоимости доставки для заказа
     * определяется типом оплаты
     * для оплаты наличными и без указания типа оплаты стоимость равна delivery_code_3
     * иначе стоимость доставки будет минимальна
     *
     * @return int
     */
    public function getDeliveryCost()
    {
        // переосмыслили получение стоимости доставки, через фиксацию при оформлении
        return $this->delivery_cost;

        $logistic = $this->getLogistic();
        $cost = $logistic['delivery_cost_3'];

        if ($this->getPaymentType() !== "Наличными при получении" && $this->getPaymentType() !== "Не указано") {
            $cost = $logistic['delivery_cost_1'];
        }

        return $cost;
    }

    /**
     * Получение способа доставки
     * @return string
     */
    public function getDeliveryType()
    {
        $logistic = $this->getLogistic();
        return ucfirst($logistic['delivery_type']);
    }

    /**
     * Получение станции метро для доставки
     * @return string|null
     */
    public function getDeliveryMetro()
    {
        $user = $this->getUser();
        $id = $user['metro'];

        $item = mysql_fetch_assoc(mysql_query("SELECT * FROM m_geo_metro WHERE id = {$id}"));
        return $item !== false ? $item['name'] : "Не указано";
    }

    /**
     * Получение названия типа оплаты заказа
     * @return string
     */
    public function getPaymentType()
    {
        if ($this->payment_type == null) {
            $this->payment_type = 0;
        }

        return $this->_paymentType[$this->payment_type];
    }

    /**
     * Определение самовывоз или нет
     * @return bool
     */
    public function isLogisticSelf()
    {
        $logistic = $this->getLogistic();
        return mb_strtolower($logistic['delivery_type'],'utf-8') === "самовывоз" ? true : false;
    }

    /**
     * Определение региональности доставки
     * @return bool
     */
    public function isRegion()
    {
        $user = $this->getUser();
        return preg_replace('/г /', '', trim(mb_strtolower($user['city'],'utf-8'))) === "москва" ? false : true;
    }

    /**
     * Получение общей стоимости заказа,
     * с доставкой и без
     *
     * @param bool $withDelivery
     *
     * @return int
     */
    public function getTotal($withDelivery = false)
    {
        if ($withDelivery) {
            return (int)($this->dSum + $this->getDeliveryCost());
        } else {
            return (int)$this->dSum;
        }
    }

    public function getCancelReason(){
        if ($this->cancel_cause_id>0){
            $item = mysql_fetch_assoc(mysql_query("SELECT * FROM m_cancel_causes WHERE cause_id = {$this->cancel_cause_id}"));
            return $item['cause_name'];
        }
        return '';
    }

    /**
     * Функция получения всей необходимой информации о пользователе
     * @return array
     */
    public function getUser()
    {
        $userId = $this->id_user;
        return User_Row_Item::collectUserInfo($userId, $this);
    }
} 