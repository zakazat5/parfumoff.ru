<?php

if ($canon_name4 && !is_numeric($canon_name4)){
    err_301($canon_name1.'/'.$canon_name2.'/');
}

if ($canon_name3 && $canon_name3=='page' && !$canon_name4){
    err_301($canon_name1.'/'.$canon_name2.'/');
}

if ($canon_name5){
    err_301($canon_name1.'/'.$canon_name2.'/');
}

$sql_m_catalog_block = '';
$sql_m_catalog_data_block = '';
$sql_m_catalog_data_a_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$sql_m_catalog_block = ' AND id != 106 AND id != 83 AND id != 176 AND id != 460';
	$sql_m_catalog_data_block = ' AND id_catalog != 106 AND id_catalog != 83 AND id_catalog != 176 AND id_catalog != 460';
	$sql_m_catalog_data_a_block = ' AND a.id_catalog != 106 AND a.id_catalog != 83 AND a.id_catalog != 176 AND a.id_catalog != 460';
}

if (isset($canon_name2) or $canon_name3 == 'page') {

	if (strlen($canon_name2) > 100) err_404();
	$find = eregi_replace('^ a-zA-Z0-9]', '', $canon_name2);
	

	$all_items 	= $db->fetchOne("SELECT count(*) FROM m_catalog_data WHERE tagl LIKE '%".$find."%' AND price_s!=0 AND id_cat=82{$sql_m_catalog_data_block} ");
	$p 			= pages_content($all_items, 40, $canon_name4, 'pages.tpl');
	$start 		= $p['start']; $limit = 40; $pagis = $p['pages'];
	

	$select = "SELECT "
	."a.id, a.title, a.url, a.price_s, a.img, a.text, "
	."b.title AS brend_title, b.url AS brend_url "
	."FROM m_catalog_data AS a "
	."JOIN m_catalog AS b ON b.id=a.id_catalog "
	."WHERE a.tagl LIKE '%".$find."%' AND a.price_s!=0 AND a.id_cat=82{$sql_m_catalog_data_a_block} "
	."LIMIT $start, $limit";

	$q_catalog = $db->query($select);
	while ($c_catalog = $q_catalog->fetch()){

		if ($c_catalog['img']=='') {
			$img = '/images/noimage.jpg';
			$img_big = '/images/noimage.jpg';
		}else {
			$img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
			$img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
		}

		$catalog_item[] = array(
		'title' 		=> $c_catalog['title'],
		'brend_title' 	=> $c_catalog['brend_title'],
		'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
		'img' 			=> $img,
		'img_big' 		=> $img_big,
		'url' 			=> '/catalog/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
		'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...'
		);
	}
	if(is_array($catalog_item))
	$view->catalog_item = $catalog_item;

	$title_tag = $db->fetchOne("SELECT title FROM m_catalog_data_tag WHERE url='$canon_name2' ");
	$view->title_tag = $title_tag;
	$view->pages = $pagis;

	/* -=========================================
	META
	*/
	if($canon_name4!=null){
		$constructor['meta_title'] = "{$title_tag} - ����, ������ � ����� ������� \"{$title_tag}\". �������� <<{$canon_name4}>> �� Parfumoff.ru";
		$constructor['meta_description'] = "����� ������ � ������� \"{$title_tag}\" �������� <<{$canon_name4}>>";
	}else {
		$constructor['meta_title'] = "{$title_tag} - ����, ������ � ����� ������� \"{$title_tag}\"";	
		$title_tab_upper = strtoupper($title_tag);
		$constructor['meta_description'] = "��������� ���� � �������� {$title_tab_upper} � ��������-�������� ���������� Parfumoff.ru.";
	}
	


	$constructor['content'] = $view->render('content_tag.php');

}