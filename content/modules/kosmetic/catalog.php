<?


$brend = $db->fetchRow("SELECT id, title, title_1 FROM m_catalog WHERE id_cat=82 AND url='$canon_name3' and is_block=0 ");
if ($brend == false) err_404();



$select = "SELECT "
    ."id, id_catalog, title, title_1, img, alt, text, url, pol, tag, price_s, meta_title, meta_title_1, meta_description "
    ."FROM k_catalog_data "
    ."WHERE url='$canon_name4' and id_catalog='{$brend['id']}' "
    ."LIMIT 1";

$q_catalog_data = $db->query($select);
while ($c_catalog_data = $q_catalog_data->fetch()){

    $id_catalog_data 	= $c_catalog_data['id'];
    $tag 				= $c_catalog_data['tag'];
    $price 				= $c_catalog_data['price_s']+$_SESSION['_GMARGIN'];

    if ($c_catalog_data['img']=='') {
        $img = '/images/noimage.jpg';
    }else {
        $img = '/images/uploads/catalog/'.$c_catalog_data['id'].'/big/'.$c_catalog_data['img'];
    }

    if ($c_catalog_data['pol'] == 'M') $pol='Мужской'; else $pol='Женский';

    $catalog_full = array(
        'id' 			=> $c_catalog_data['id'],
        'title' 		=> $c_catalog_data['title'],
        'title_1' 		=> $c_catalog_data['title_1'],
        'meta_title' 	=> $c_catalog_data['meta_title'],
        'url' 			=> $c_catalog_data['url'],
        'img' 			=> $img,
        'alt' 			=> $c_catalog_data['alt'],
        'text' 			=> word_to_link($c_catalog_data['text'], '\.', 'http://'.BASE_URL),
        'pol' 			=> $pol,
    );
}
if (!is_array($catalog_full)) err_404();
$view->catalog_full 	= $catalog_full;
$view->brend_title		= $brend['title'];
$view->brend_url		= $canon_name3;


/* -=========================================
ИЗБРАННОЕ
*/
if(!empty($ses_note->id)){
    if (in_array($catalog_full['id'], $ses_note->id)){
        $view->is_note = true;
    }else{
        $view->is_note = false;
    }
}else {
    $view->is_note = false;
}




/* -=========================================
список модификаций
*/
$select = "SELECT id, v, type, price, price_old, is_block "
    ."FROM k_catalog_data_order "
    ."WHERE id_catalog_data='$id_catalog_data' "
    ."ORDER BY price ASC";

$num=0;
$q_catalog_order = $db->query($select);
while ($c_catalog_order = $q_catalog_order->fetch()) {

    if ($c_catalog_order['type']=='N') {
        $type = $c_catalog_order['v'];
    }else {
        $type = $aroma_type[$c_catalog_order['type']] .' '. $c_catalog_order['v'] . ' мл';
    }

    $catalog_item_order[] = array(
        'title' => $title,
        'type' 	=> $type,
        't' 	=> $c_catalog_order['type'],
        'id' 	=> $c_catalog_order['id'],
        'is_block' 	=> $c_catalog_order['is_block'],
        'price' => $c_catalog_order['price']+$_SESSION['_GMARGIN'],
        'price_old' => $c_catalog_order['price_old'],
        'bg' 	=> ($num % 2 == 0) ? '1' : '2',
    );

    $num++;
}
if (is_array($catalog_item_order))	$view->catalog_item_order = $catalog_item_order;


/* -=========================================
TAG
*/
if ($tag!='') {
    $tag = explode(',', $tag);
    foreach ($tag as $key => $value) {
        if ($value == '') continue;
        $item_tag[] = array(
            'url' => translit($value),
            'title' => $value,
        );
    }
    $view->catalog_item_tag = $item_tag;
}

/* -=========================================
список комментариев
*/
$select = "SELECT "
    ."id, user_name, user_text, user_date "
    ."FROM m_catalog_com "
    ."WHERE id_catalog_data='$id_catalog_data' "
    ."ORDER BY user_date ";

$q_catalog_com = $db->query($select);
while ($c_catalog_com = $q_catalog_com->fetch()) {
    $catalog_item_com[] = array(
        'id' 			=> $c_catalog_com['id'],
        'user_name' 	=> $c_catalog_com['user_name'],
        'user_text' 	=> $c_catalog_com['user_text'],
        'user_date' 	=> $c_catalog_com['user_date'],
    );
}
$view->catalog_item_com = $catalog_item_com;

/* -=========================================
ароматы по схожей цене
*/
if ($price != 0) {
    $price_start = $price-500;
    $price_end = $price+500;

    $select = "SELECT "
        ."a.id, a.title, a.title_1, a.url, a.price_s, a.img, "
        ."b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
        ."FROM k_catalog_data AS a "
        ."JOIN m_catalog AS b ON b.id=a.id_catalog "
        ."WHERE a.id_cat=82 AND a.id!='$id_catalog_data' AND a.price_s > '$price_start' AND a.price_e < '$price_end' "
        ."ORDER BY price_s "
        ."LIMIT 6";
}else {
    $select = "SELECT "
        ."a.id, a.title, a.title_1, a.url, a.price_s, a.img, "
        ."b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
        ."FROM k_catalog_data AS a "
        ."JOIN m_catalog AS b ON b.id=a.id_catalog "
        ."WHERE a.id_cat=82 AND a.id!='$id_catalog_data' AND a.price_s != 0  AND a.price_e < 2000 "
        ."ORDER BY price_s "
        ."LIMIT 6";
}
$q_catalog_pre = $db->query($select);
while ($c_catalog_pre = $q_catalog_pre->fetch()) {
    if ($c_catalog_pre['img']=='') {
        $img = '/images/noimage.jpg';
    }else {
        $img = '/images/uploads/catalog/'.$c_catalog_pre['id'].'/small/'.$c_catalog_pre['img'];
    }

    $catalog_item_pre[] = array(
        'title' 		=> ($c_catalog_pre['title_1']!='') ? $c_catalog_pre['title_1'] : $c_catalog_pre['title'],
        'brend_title' 	=> ($c_catalog_pre['brend_title']!='') ? $c_catalog_pre['brend_title_1'] : $c_catalog_pre['brend_title'],
        'price' 		=> $c_catalog_pre['price_s']+$_SESSION['_GMARGIN'],
        'url' 			=> '/catalog/'.$c_catalog_pre['brend_url'].'/'.$c_catalog_pre['url'].'/',
        'img'			=> $img,

    );
}
$view->catalog_item_pre = $catalog_item_pre;



/* -=========================================
ароматы бренда
*/
$select = "SELECT "
    ."a.id, a.title, a.title_1, a.url, a.price_s, a.img, "
    ."b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
    ."FROM k_catalog_data AS a "
    ."JOIN m_catalog AS b ON b.id=a.id_catalog "
    ."WHERE a.id_catalog='{$brend['id']}' AND a.id!='$id_catalog_data' AND a.price_s!=0 "
    ."ORDER BY price_s "
    ."LIMIT 6";
$q_catalog_br = $db->query($select);
while ($c_catalog_br = $q_catalog_br->fetch()) {
    if ($c_catalog_br['img']=='') {
        $img = '/images/noimage.jpg';
    }else {
        $img = '/images/uploads/catalog/'.$c_catalog_br['id'].'/small/'.$c_catalog_br['img'];
    }

    $catalog_item_br[] = array(
        'title' 		=> ($c_catalog_br['title_1']!='') ? $c_catalog_br['title_1'] : $c_catalog_br['title'],
        'brend_title' 	=> ($c_catalog_br['brend_title']!='') ? $c_catalog_br['brend_title_1'] : $c_catalog_br['brend_title'],
        'price' 		=> $c_catalog_br['price_s']+$_SESSION['_GMARGIN'],
        'url' 		=> '/catalog/'.$c_catalog_br['brend_url'].'/'.$c_catalog_br['url'].'/',
        'img'		=> $img,
    );
}
$view->catalog_item_br = $catalog_item_br;



/* -=========================================
Ароматы бренда справа
*/
$select = "SELECT "
    ."title, url, pol "
    ."FROM k_catalog_data "
    ."WHERE id_catalog='{$brend['id']}' AND price_s!=0 ORDER BY title";

$q_catalog_data = $db->query($select);
while ($c_catalog_data = $q_catalog_data->fetch()){
    $catalog_item[] = array(
        'title' 	=> $c_catalog_data['title'],
        'url'	 	=> $c_catalog_data['url'],
        'pol'	 	=> $c_catalog_data['pol'],
    );
}
$view->catalog_item = $catalog_item;
$view->catalog_count_f = $db->fetchOne("SELECT count(*) FROM k_catalog_data WHERE pol='F' AND id_catalog='{$brend['id']}' AND price_s!=0 ");
$view->catalog_count_m = $db->fetchOne("SELECT count(*) FROM k_catalog_data WHERE pol='M' AND id_catalog='{$brend['id']}' AND price_s!=0 ");


/* -=========================================
META
*/
if($brend['title_1']=='') $m_brend_title_1 = $brend['title']; else $m_brend_title_1 = $brend['title_1'];
if($catalog_full['title_1']==''){
    $constructor['meta_title'] = "{$brend['title']} {$catalog_full['title']}, купить духи {$m_brend_title_1} {$catalog_full['title']} по отличной цене, {$catalog_full['pol']} аромат, парфюм, туалетная вода";
    $constructor['meta_description'] = "Неповторимый аромат духов {$brend['title']} {$catalog_full['title']}, купите духи {$m_brend_title_1} {$catalog_full['title']} ({$catalog_full['pol']} аромат) по лучшей цене со скидкой! Лучшие цены на ароматы {$brend['title']} в интернет-магазине парфюмерии Энигма";
}else{

    $constructor['meta_title'] = "{$brend['title']} {$catalog_full['title']}, купить духи {$m_brend_title_1} {$catalog_full['title_1']} по отличной цене, {$catalog_full['pol']} аромат, парфюм, туалетная вода";
    $constructor['meta_description'] = "Неповторимый аромат духов {$brend['title']} {$catalog_full['title']}, купите духи {$m_brend_title_1} {$catalog_full['title_1']} ({$catalog_full['pol']} аромат) по лучшей цене со скидкой! Лучшие цены на ароматы {$brend['title']} в интернет-магазине парфюмерии Энигма";
}







//отзывы
$shop_comments[] = array(
    'name'=>'Алексей',
    'date'=>'10 Декабря',
    'text'=>'Заказал аромат детства - Ван Мен Шоу от богарта. Я был маленький, а все дяди вокруг носили этот аромат. Гуд! Доставка в Липецк заняла 4 дня. Я рад!'
);
$shop_comments[] = array(
    'name'=>'Агеев А.',
    'date'=>' 8 Октября',
    'text'=>'Я знаю этот магазинчик «Энигме», но я там сам ни разу не был, а я просто воспользовался услугами один раз интернета и заказал себе и для жены духи, я точно знал какие, название и милимитраж, просто так удобнее всего, ведь на сайте конечно есть подробная информация о каждом товаре и даже картинки, но всё же лучше знать уже продукцию в лицо, так как тебе привезут и чтобы точно знать, что это именно то, что ты заказывал или тем, что ты раньше пользовался. Я вообще заплатил за духи женские 800 руб. в магазине точно такие же стоят в два раза дороже! А мне ещё их доставили бесплатно в тот же день и очень вежливые люди, так что я остался всем доволен, особенно жена.'
);
$shop_comments[] = array(
    'name'=>'Елена',
    'date'=>'05 августа',
    'text'=>'Случайно нашли с подругой сайт этого магазина.Заказывали парфюм уже три раза у них.Каждый раз все присылали в течении 10-12 дней.Лучше магазина по обслуживанию и цене я в интернете не нашла.Парфюм не поддельный,мне есть с чем сравнить,т.к. я сначала выбираю себе духи в фирменном магазине (рив гош, летуаль) у себя в городе,а потом заказываю в "энигме" и точно знаю как они выглядят и пахнут !'
);
$shop_comments[] = array(
    'name'=>'SOSO',
    'date'=>'27 июля',
    'text'=>'Заказывала недавно Chanel Chance Eau Fraiche качество отличное и с доставкой все в порядке. Честно очень обрадовалась, когда наткнулась на этот интернет-магазин! '
);
$shop_comments[] = array(
    'name'=>'nensy666',
    'date'=>'20 июля',
    'text'=>'Хороший магазин: на сайте можно найти весь *ходовой* товар, полное описание, и - главное - невысокие цены'
);
$shop_comments[] = array(
    'name'=>'Сергей',
    'date'=>'05 мая',
    'text'=>'Этот магазин стал одним из самых любимых. А весь секрет в том, что на главной странице можно сразу выбрать ценовую категорию аромата. Это весьма удобно, когда располагаешь строго ограниченными финансами + не знаешь, что бы такого купить :)'
);

shuffle($shop_comments);


$view->shop_comments = $shop_comments;

//доставка
$view->dostavka = $db->fetchRow("SELECT text_2,text_3 FROM m_text WHERE id=2 ");




/* -=========================================
рендерим
*/


$constructor['is_right']=1;
if (isset($_GET['template']) && $_GET['template']=='new'){
    $constructor['meta_noindex'] = true;
    $constructor['content'] = $view->render('content_catalog_full_new.php');
}else{
    $constructor['content'] = $view->render('content_catalog_full.php');
}