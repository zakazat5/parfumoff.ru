<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 14.02.14
 * Time: 18:35
 */

header('content-type: application/json');

mail('truth4oll@gmail.com','234',$_POST);

switch ($canon_name2) {
    /**
     * Выставить рейтинг товара
     */
    case 'rate':
        $data['target_type'] = 'parfum';
        $data['target_id'] = $_POST['target_id'];
        $data['value'] = $_POST['val'];
        $data['ip'] = $_SERVER['REMOTE_ADDR'];

        if ($data['target_id']!='' && $data['value']!=''){
            $db->insert('m_catalog_rate',$data);
        }

        $rate = $db->fetchOne($db->select()->from('m_catalog_rate',new Zend_Db_Expr('AVG(value) as rate'))->where('target_id=?',$data['target_id'])->group('target_id'));

        $where = $db->quoteInto('articul = ?', $data['target_id']);
        $db->update('m_catalog_data',['rate'=>$rate],$where);



        break;
    case 'image':
        $articul = $_GET['articul'];

        $select = "SELECT id,img FROM m_catalog_data WHERE articul=?";

        $oProduct = $db->fetchRow($select, $articul);

        $result = array(
            'small' => "http://www.parfumoff.ru/images/uploads/catalog/{$oProduct['id']}/small/{$oProduct['img']}",
            'big' => "http://www.parfumoff.ru/images/uploads/catalog/{$oProduct['id']}/big/{$oProduct['img']}"
        );

        echo json_encode($result);
        break;
    case "logistic_get":

        $addr_id = (int)$_GET['k'];

        // текущий город
        $city = "";

        $select = "SELECT couriers_addresses.*, couriers.name AS courier_name FROM couriers_addresses
                        LEFT JOIN couriers ON couriers_addresses.courier_id = couriers.couriersId
                        WHERE couriers_addresses.id = ? ORDER BY delivery_metro ASC";
        $row = $db->fetchRow($select, $addr_id);

        // если ничего не пришло - значит почта россии по умолчанию
        if ($row == null) {
            $row['id'] = 0;
        }

        /**
         * Соотношение id к названию типов оплаты заказа
         * @var array
         */
        $_paymentType = array(
            0 => "Не указано",
            1 => "Наличными при получении",
            2 => "Предоплата по квитанции",
            3 => "Предоплата Robokassa"
        );

        if (!isset($row['delivery_cost_3'])) {
            $row['delivery_cost_3'] = 290;
        }

        // $_GET['t'] ~ playment_type ~ id
        if (isset($_GET['t']) && ((int) $_GET['t'] !== 0 && (int) $_GET['t'] !== 1)) {
            $row['delivery_cost'] = (int) $row['delivery_cost_1'];
        } else {
            $row['delivery_cost'] = (int) $row['delivery_cost_3'];
        }

        if (!isset($_GET['t'])) {
            $row['delivery_cost'] = (int) $row['delivery_cost_3'];
        }

        /*foreach ($row as $k => $v) {
            $row[$k] = iconv('windows-1251', 'utf-8', $v);
        }*/

        header('Content-Type: application/json; charset=windows-1251');
        echo json_encode($row);
        break;
    case "logistic":

        include_once ('includes/class/checkout.php');

        $fias_id = $_GET['fs'];

        // сессия текущего состояния корзины
        $ses_cart = new Zend_Session_Namespace('korzina');

        //Класс Checkout.ru
        $checkout = new Checkout();

        $carriers = $checkout->getCarriers(0, $ses_cart->sum, $ses_cart->kol, $ses_cart->sum, $fias_id);

        /**
         * Соотношение id к названию типов оплаты заказа
         * @var array
         */
        $_paymentType = array(
            0 => "Не указано",
            1 => "Наличными при получении",
            2 => "Предоплата по квитанции",
            3 => "Предоплата Robokassa"
        );

        header('Content-Type: application/json; charset=utf-8');
        echo $carriers;
        break;
    case 'geo':
        $action = $_GET['action'];
        if ($action == '') {
            $action = $_POST['action'];
        }
        if ($action == 'get_cities') {
            $region_id = (int)$_GET['region_id'];
            if ($region_id == 0) {
                $sql = "SELECT a.*,b.region_name FROM m_geo_city a  LEFT JOIN m_geo_region b on a.region_id=b.id  WHERE is_top=1 ORDER BY a.city_name ";
            } else {
                $sql = "SELECT a.*,b.region_name FROM m_geo_city a  LEFT JOIN m_geo_region b on a.region_id=b.id WHERE a.region_id =? ORDER BY a.city_name";
            }

            $aCities = $db->fetchAll($sql, $region_id);
            foreach ($aCities as $key => $city) {
                $aCities[$key]['city_name'] = iconv('windows-1251', 'utf-8', $city['city_name']);
                $aCities[$key]['region_name'] = iconv('windows-1251', 'utf-8', $city['region_name']);
                $letter = mb_substr($aCities[$key]['city_name'], 0, 2);
                $aLetter[$letter][] = $key;
            }
            echo json_encode(array('aCities' => $aCities, 'aLetters' => $aLetter));
            //set location
        } elseif ($action == 'set_location') {

            $city_id = $_GET['city_id'];
            $sql = "SELECT a.*,b.region_name FROM m_geo_city a  LEFT JOIN m_geo_region b on a.region_id=b.id WHERE a.id =? ORDER BY a.city_name";
            if ($row = $db->fetchRow($sql, $city_id)) {
                //capitals moscow and spb
                if ($city_id == 1250 or $city_id == 123) {
                    $row['region_name'] = $row['city_name'];
                }

                // send to session utf-8
                $ses_location = new Zend_Session_Namespace('location');
                $ses_location->location = $row;

                //answer win-1251, fucking encoding
                $row['city_name'] = iconv('windows-1251', 'utf-8', $row['city_name']);
                $row['region_name'] = iconv('windows-1251', 'utf-8', $row['region_name']);
                echo json_encode(array('city' => $row));
            } else {
                if ($_POST['city_name'] != '') {
                    $row['city_name'] = iconv('utf-8', 'windows-1251', $_POST['city_name']);
                    $row['region_name'] = iconv('utf-8', 'windows-1251', $_POST['region_name']);
                }

                $ses_location = new Zend_Session_Namespace('location');
                $ses_location->location = $row;
            };
        }
        break;

    case 'cart':
        $action = $_POST['action'];
        $uid = $_POST['uid'];

        $cartClass = new Cart();
        switch ($action) {
            case 'asc':
                $cartClass->change_amount($uid, $action);
                break;
            case 'desc':
                $cartClass->change_amount($uid, $action);
                break;
        }

        $result = $cartClass->getFormated();
        echo json_encode($result);
        break;

    case 'dadata':

        $action = $_POST['action'];
        $result = dadata_get('https://dadata.ru/api/v2/suggest/fio', 'Авер');
        print_r($result);

        switch ($action) {
            case 'name':
                $data = dadata_get('https://dadata.ru/api/v2/suggest/fio', $data);
                break;

            case 'address':
                break;
        }
    case 'aromat':
        $items = $db->fetchAll($db->select()->from(['a'=>'m_catalog_data'])->where('a.id_cat=82 AND (a.find LIKE ? OR a.find_ru LIKE ? )','%'.$_POST['search'].'%'));
        foreach ($items as $item) {
            $result[$item['articul']] = html_entity_decode($item['title']);
        }
        echo json_encode($result);


        break;
    case 'sendcomment':

        break;
}
