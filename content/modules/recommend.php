<?

if ($_POST) {
    $data = [
        'order_id' =>(int) $_POST['order_id'],
        'user_name' => $_POST['user_name'],
        'text' => $_POST['text'],
        'rate' => $_POST['rate'],
    ];

    if ($_POST['bot']){
        return false;

    }
    $view->recommend = $data;

    foreach($data as $key=>$item){
        if ($item==''){
            switch($key){
                case 'user_name':
                    $aErrors[] = 'Не заполено поле "имя"';
                    break;
                case 'text':
                    $aErrors[] = 'Не заполен текст отзыва';
                    break;
                case 'order_id':
                    $aErrors[] = 'Не заполнен номер заказа';
                    break;
                case 'rate':
                    $aErrors[] = 'Не выставлен рейтинг';
                    break;
            }
        }
    }

    if ($data['order_id']===0){
        $aErrors[] = 'Номер заказа должен быть числом';
    }

    if (!$aErrors){
        $data['ip']=$_SERVER['REMOTE_ADDR'];
        $data['created_at']=time();
        if ($db->insert('m_recommend', $data)){
            $view->thanks = true;
        }
    }else{
        $view->aErrors = $aErrors;
    }



}


$page = (int)$_GET['page'];

$adapter = new Zend_Paginator_Adapter_DbSelect(
    $db->select()->from('m_recommend')->joinLeft(
        'm_catalog_data',
        'm_recommend.articul=m_catalog_data.articul',
        ['title', 'url']
    )
        ->joinLeft('m_catalog', 'm_catalog.id=m_catalog_data.id_catalog', ['brand_url' => 'url'])
    ->order('id DESC')
);
$adapter->setRowCount(
    $db->select()
        ->from(
            'm_recommend',
            array(
                new Zend_Db_Expr('COUNT(*) AS `zend_paginator_row_count`')
            )
        )
);
$paginator = new Zend_Paginator($adapter);
$paginator->setCurrentPageNumber($page);
$paginator->setItemCountPerPage(10);

$aRecommend = $paginator;

$view->aRecommend = $aRecommend;

$constructor['meta_title'] = 'Отзывы о магазине парфюмерии Парфюмофф.ру';

$constructor['content'] = $view->render('content_recommend.php');




?>