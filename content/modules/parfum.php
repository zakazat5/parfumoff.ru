<?php
/*
if ($canon_name2){
    err_301($canon_name1.'/');
}
*/

/*
$sql_m_catalog_block = '';
$sql_m_catalog_data_a_block = '';
$sql_m_catalog_b_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$sql_m_catalog_block = ' AND id != 106 AND id != 83 AND id != 176 AND id != 460';
	$sql_m_catalog_b_block = ' AND b.id != 106 AND b.id != 83 AND b.id != 176 AND b.id != 460';
	$sql_m_catalog_data_a_block = ' AND a.id_catalog != 106 AND a.id_catalog != 83 AND a.id_catalog != 176 AND a.id_catalog != 460';
	$sql_m_catalog_data_block = ' AND id_catalog != 106 AND id_catalog != 83 AND id_catalog != 176 AND id_catalog != 460';
}
*/


if ($canon_name2 == "men") {
	/**
         * Filter
         */
        if ($_GET['aroma_type'] || $_GET['s100'] || $_GET['pol']) {
            //$constructor['noindex'] = true;
            foreach ($_GET['aroma_type'] as $key => $type) {
                if (!array_key_exists($type, $aroma_type)) {
                    unset($_GET['aroma_type'][$key]);
                } else {
                    $aroma_type_checked[$_GET['aroma_type'][$key]] = 1;
                }
            }
            //выбранные селекты
            $checked['aroma_type'] = $aroma_type_checked;

            $select = $db->select();
			$select->limit(40);
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type', new Zend_Db_Expr('m_catalog.title as brend_title'), new Zend_Db_Expr('m_catalog.title_1 as brend_title_1'), new Zend_Db_Expr('m_catalog.url as brend_url')]
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
			$select->joinLeft('m_catalog', 'm_catalog_data.id_catalog=m_catalog.id', '');

            if (count($_GET['s100']) > 0) {
                foreach ($_GET['s100'] as $key => $item) {
                    $item = (int)$item;
                    if ($item == 0) {
                        $select->orWhere('m_catalog_data_order.s100 <1000');
                    } elseif ($item == 10) {
                        $select->orWhere('m_catalog_data_order.s100 >10000');
                    } else {
                        $from = $item * 1000 + 1;
                        $to = $item * 1000 + 1000;
                        $select->orWhere("m_catalog_data_order.s100>{$from} and m_catalog_data_order.s100<={$to}");

                    }
                    $checked['s100'][$_GET['s100'][$key]] = 1;
                }
            }
			
			$select->Where('m_catalog_data.pol=?', 'M');
			
            if (count($_GET['pol']) > 0) {
                $aPol = ['M', 'F'];
                foreach ($_GET['pol'] as $key => $item) {
                    if (in_array($item, $aPol)) {
                        $select->Where('m_catalog_data.pol=?', $item);
                        $checked['pol'][$item] = 1;
                    } else {
                        err_404();
                    }
                }
            }

            $subquery = $select->getPart(Zend_Db_Select::WHERE);
            $select->reset(Zend_Db_Select::WHERE);
			
            if (count($_GET['aroma_type']) > 0) {
                if (in_array('TW',$_GET['aroma_type'])) {
					$aroma_type_mod = $_GET['aroma_type'];
				} else {
					$aroma_type_mod = array('TW');
				}
            } else {
				$aroma_type_mod = array('TW');
			}
			$select->where('m_catalog_data_order.type  IN (?)', $aroma_type_mod);
			
            $select->where('m_catalog_data_order.price>0');
            //$select->where('m_catalog_data.id_catalog=?', $brend['id']);
			
			//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
			if ($ses_user->is_login == false) {
				$select->where('m_catalog_data.id_catalog != 106');
				$select->where('m_catalog_data.id_catalog != 83');
				$select->where('m_catalog_data.id_catalog != 176');
				$select->where('m_catalog_data.id_catalog != 460');
			}
			
            if ($subquery) {
                $select->where(implode(' ', $subquery));
            }
            $select->group('m_catalog_data.id');
        } else {
            $select = $db->select();
			$select->limit(40);
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type', new Zend_Db_Expr('m_catalog.title as brend_title'), new Zend_Db_Expr('m_catalog.title_1 as brend_title_1'), new Zend_Db_Expr('m_catalog.url as brend_url')]
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
			$select->joinLeft('m_catalog', 'm_catalog_data.id_catalog=m_catalog.id', '');
            /*if ($_GET['all']!=1){
                $select->where('m_catalog_data.price_s!=0');
            }*/
            //$select->where('m_catalog_data.id_catalog=?', $brend['id']);
			
			$select->where('m_catalog_data.pol = "M"');
			
			$select->where('m_catalog_data_order.type = "TW"');
			
			//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
			if ($ses_user->is_login == false) {
				$select->where('m_catalog_data.id_catalog != 106');
				$select->where('m_catalog_data.id_catalog != 83');
				$select->where('m_catalog_data.id_catalog != 176');
				$select->where('m_catalog_data.id_catalog != 460');
			}
						
            $select->group('m_catalog_data.id');
        }


        /**
         * s100
         */
        $s100select = clone $select;
        $s100select->reset(Zend_Db_Select::GROUP);
        $s100select->reset(Zend_Db_Select::WHERE);
		$s100select->limit(200);
        $s100select->where('m_catalog_data.price_s!=0');
        $s100select->where('m_catalog_data_order.price>0');
        //$s100select->where('m_catalog_data.id_catalog=?', $brend['id']);
				
        $s100 = $db->fetchAll($s100select);

        foreach ($s100 as $item) {
            switch (true) {
                case ($item['s100'] <= 1000):
                    $s100filter[0] = 'До 1000';
                    break;
                case ($item['s100'] > 1001 && $item['s100'] <= 2000):
                    $s100filter[1] = 'От 1001 до 2000';
                    break;
                case ($item['s100'] > 2001 && $item['s100'] <= 3000):
                    $s100filter[2] = 'От 2001 до 3000';
                    break;
                case ($item['s100'] > 3001 && $item['s100'] <= 4000):
                    $s100filter[3] = 'От 3001 до 4000';
                    break;
                case ($item['s100'] > 4001 && $item['s100'] <= 5000):
                    $s100filter[4] = 'От 4001 до 5000';
                    break;
                case ($item['s100'] > 5001 && $item['s100'] <= 6000):
                    $s100filter[5] = 'От 5001 до 6000';
                    break;
                case ($item['s100'] > 6001 && $item['s100'] <= 7000):
                    $s100filter[6] = 'От 6001 до 7000';
                    break;
                case ($item['s100'] > 7001 && $item['s100'] <= 8000):
                    $s100filter[7] = 'От 7001 до 8000';
                    break;
                case ($item['s100'] > 8001 && $item['s100'] <= 9000):
                    $s100filter[8] = 'От 8001 до 9000';
                    break;
                case ($item['s100'] > 9001 && $item['s100'] <= 10000):
                    $s100filter[9] = 'От 9001 до 10000';
                    break;
                case ($item['s100'] > 10000):
                    $s100filter[10] = 'Больше 10000';
                    break;
            }
        }
        ksort($s100filter);
        $view->s100filter = $s100filter;

        $aromaTypeSelect = clone $select;
        $aromaTypeSelect->reset(Zend_Db_Select::GROUP);
        $aromaTypeSelect->reset(Zend_Db_Select::WHERE);
		$aromaTypeSelect->limit(200);
        $aromaTypeSelect->where('m_catalog_data_order.price!=0');
        //$aromaTypeSelect->where('m_catalog_data.id_catalog=?', $brend['id']);
		
		$aromaTypeSelect->group('m_catalog_data_order.type');
        $aromaTypeFetch = $db->fetchAll($aromaTypeSelect);

		//var_dump($aromaTypeFetch);
		
        foreach ($aromaTypeFetch as $key => $item) {

            $aromaTypeFilter[$item['type']] = $aroma_type[$item['type']];
        }
        unset($aromaTypeFilter[0]);
        $view->aromaTypeFilter = $aromaTypeFilter;


        if ($_GET['sort'] == 'abc' || $_GET['sort'] == 'popular') {
            switch ($_GET['sort']) {
                case 'abc':
                    $select->order('title ASC');
                    break;
                case 'popular':
                    $select->order('id  ASC');
                    break;
            }
        } else {
            $select->order('title ASC');
        }


        $q_catalog_data = $db->query($select);
        while ($c_catalog_data = $q_catalog_data->fetch()) {

            if ($c_catalog_data['img'] == '') {
                $img = '/images/noimage.jpg';
                $img_big = '/images/noimage.jpg';
            } else {
                $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/small/' . $c_catalog_data['img'];
                $img_big = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
            }

            if (!$og_image) {
                if ($c_catalog_data['img'] != '') {
                    $og_image = $img;
                }
            }

            if ($c_catalog_data['pol'] == 'F') {
                $catalog_item_f[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    //'brend_title' => $brend['title'],
					'brend_title' => $c_catalog_data['brend_title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $c_catalog_data['brend_url'] . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $c_catalog_data['brend_title'],
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            } elseif ($c_catalog_data['pol'] == 'M') {
                $catalog_item_m[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    //'brend_title' => $brend['title'],
					'brend_title' => $c_catalog_data['brend_title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $c_catalog_data['brend_url'] . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $c_catalog_data['brend_url'],
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            }
        }

        //$GLOBALS['dataLayer']['PageType'] = 'ListingPage';
		
        if (is_array($catalog_item_f)) {
            $view->catalog_item_f = $catalog_item_f;
			/*
            $where = [];
            foreach ($catalog_item_f as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
			*/
        }
        if (is_array($catalog_item_m)) {
            $view->catalog_item_m = $catalog_item_m;
			/*
            $where = [];
            foreach ($catalog_item_m as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
			*/
        }

        $show_outstock = true;
        foreach ($catalog_item_f as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }
        foreach ($catalog_item_m as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }

		$checked['aroma_type']['TW'] = 1;
		$checked['pol']['M'] = 1;
		
        $view->checked = $checked;
        $view->show_outstock = $show_outstock;

		$constructor['meta_title'] = "Туалетная вода для мужчин, купить в интернет-магазине Parfumoff.ru";
        $constructor['meta_description'] = "Популярная туалетная вода для мужчин в интернет-магазине оригинальной парфюмерии Parfumoff. Купить мужскую туалетную воду известных брендов по низкой цене с доставкой.";
		$constructor['meta_keywords'] = "Туалетная вода для мужчин, модная, оригинальная, купить, цена, интернет-магазин";
		
        $constructor['og:title'] = $constructor['meta_title'];
        $constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/big/', '/small/', $og_image);
        $constructor['og:description'] = $constructor['meta_description'];
        $constructor['og:url'] = $_SERVER['REQUEST_URI'];		
	
} elseif ($canon_name2 == "woman") {
		/**
         * Filter
         */
        if ($_GET['aroma_type'] || $_GET['s100'] || $_GET['pol']) {
            //$constructor['noindex'] = true;
            foreach ($_GET['aroma_type'] as $key => $type) {
                if (!array_key_exists($type, $aroma_type)) {
                    unset($_GET['aroma_type'][$key]);
                } else {
                    $aroma_type_checked[$_GET['aroma_type'][$key]] = 1;
                }
            }
            //выбранные селекты
            $checked['aroma_type'] = $aroma_type_checked;

            $select = $db->select();
			$select->limit(40);
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type', new Zend_Db_Expr('m_catalog.title as brend_title'), new Zend_Db_Expr('m_catalog.title_1 as brend_title_1'), new Zend_Db_Expr('m_catalog.url as brend_url')]
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
			$select->joinLeft('m_catalog', 'm_catalog_data.id_catalog=m_catalog.id', '');

            if (count($_GET['s100']) > 0) {
                foreach ($_GET['s100'] as $key => $item) {
                    $item = (int)$item;
                    if ($item == 0) {
                        $select->orWhere('m_catalog_data_order.s100 <1000');
                    } elseif ($item == 10) {
                        $select->orWhere('m_catalog_data_order.s100 >10000');
                    } else {
                        $from = $item * 1000 + 1;
                        $to = $item * 1000 + 1000;
                        $select->orWhere("m_catalog_data_order.s100>{$from} and m_catalog_data_order.s100<={$to}");

                    }
                    $checked['s100'][$_GET['s100'][$key]] = 1;
                }
            }
			
			$select->Where('m_catalog_data.pol=?', 'F');
			
            if (count($_GET['pol']) > 0) {
                $aPol = ['M', 'F'];
                foreach ($_GET['pol'] as $key => $item) {
                    if (in_array($item, $aPol)) {
                        $select->Where('m_catalog_data.pol=?', $item);
                        $checked['pol'][$item] = 1;
                    } else {
                        err_404();
                    }
                }
            }

            $subquery = $select->getPart(Zend_Db_Select::WHERE);
            $select->reset(Zend_Db_Select::WHERE);
			
            if (count($_GET['aroma_type']) > 0) {
                if (in_array('TW',$_GET['aroma_type'])) {
					$aroma_type_mod = $_GET['aroma_type'];
				} else {
					$aroma_type_mod = array('TW');
				}
            } else {
				$aroma_type_mod = array('TW');
			}
			$select->where('m_catalog_data_order.type  IN (?)', $aroma_type_mod);
			
            $select->where('m_catalog_data_order.price>0');
            //$select->where('m_catalog_data.id_catalog=?', $brend['id']);
			
			//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
			if ($ses_user->is_login == false) {
				$select->where('m_catalog_data.id_catalog != 106');
				$select->where('m_catalog_data.id_catalog != 83');
				$select->where('m_catalog_data.id_catalog != 176');
				$select->where('m_catalog_data.id_catalog != 460');
			}
			
            if ($subquery) {
                $select->where(implode(' ', $subquery));
            }
            $select->group('m_catalog_data.id');
        } else {
            $select = $db->select();
			$select->limit(40);
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type', new Zend_Db_Expr('m_catalog.title as brend_title'), new Zend_Db_Expr('m_catalog.title_1 as brend_title_1'), new Zend_Db_Expr('m_catalog.url as brend_url')]
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
			$select->joinLeft('m_catalog', 'm_catalog_data.id_catalog=m_catalog.id', '');
            /*if ($_GET['all']!=1){
                $select->where('m_catalog_data.price_s!=0');
            }*/
            //$select->where('m_catalog_data.id_catalog=?', $brend['id']);
			
			$select->where('m_catalog_data.pol = "F"');
			
			$select->where('m_catalog_data_order.type = "TW"');
			
			//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
			if ($ses_user->is_login == false) {
				$select->where('m_catalog_data.id_catalog != 106');
				$select->where('m_catalog_data.id_catalog != 83');
				$select->where('m_catalog_data.id_catalog != 176');
				$select->where('m_catalog_data.id_catalog != 460');
			}
						
            $select->group('m_catalog_data.id');
        }


        /**
         * s100
         */
        $s100select = clone $select;
        $s100select->reset(Zend_Db_Select::GROUP);
        $s100select->reset(Zend_Db_Select::WHERE);
		$s100select->limit(200);
        $s100select->where('m_catalog_data.price_s!=0');
        $s100select->where('m_catalog_data_order.price>0');
        //$s100select->where('m_catalog_data.id_catalog=?', $brend['id']);
				
        $s100 = $db->fetchAll($s100select);

        foreach ($s100 as $item) {
            switch (true) {
                case ($item['s100'] <= 1000):
                    $s100filter[0] = 'До 1000';
                    break;
                case ($item['s100'] > 1001 && $item['s100'] <= 2000):
                    $s100filter[1] = 'От 1001 до 2000';
                    break;
                case ($item['s100'] > 2001 && $item['s100'] <= 3000):
                    $s100filter[2] = 'От 2001 до 3000';
                    break;
                case ($item['s100'] > 3001 && $item['s100'] <= 4000):
                    $s100filter[3] = 'От 3001 до 4000';
                    break;
                case ($item['s100'] > 4001 && $item['s100'] <= 5000):
                    $s100filter[4] = 'От 4001 до 5000';
                    break;
                case ($item['s100'] > 5001 && $item['s100'] <= 6000):
                    $s100filter[5] = 'От 5001 до 6000';
                    break;
                case ($item['s100'] > 6001 && $item['s100'] <= 7000):
                    $s100filter[6] = 'От 6001 до 7000';
                    break;
                case ($item['s100'] > 7001 && $item['s100'] <= 8000):
                    $s100filter[7] = 'От 7001 до 8000';
                    break;
                case ($item['s100'] > 8001 && $item['s100'] <= 9000):
                    $s100filter[8] = 'От 8001 до 9000';
                    break;
                case ($item['s100'] > 9001 && $item['s100'] <= 10000):
                    $s100filter[9] = 'От 9001 до 10000';
                    break;
                case ($item['s100'] > 10000):
                    $s100filter[10] = 'Больше 10000';
                    break;
            }
        }
        ksort($s100filter);
        $view->s100filter = $s100filter;

        $aromaTypeSelect = clone $select;
        $aromaTypeSelect->reset(Zend_Db_Select::GROUP);
        $aromaTypeSelect->reset(Zend_Db_Select::WHERE);
		$aromaTypeSelect->limit(200);
        $aromaTypeSelect->where('m_catalog_data_order.price!=0');
        //$aromaTypeSelect->where('m_catalog_data.id_catalog=?', $brend['id']);
		
		$aromaTypeSelect->group('m_catalog_data_order.type');
        $aromaTypeFetch = $db->fetchAll($aromaTypeSelect);

		//var_dump($aromaTypeFetch);
		
        foreach ($aromaTypeFetch as $key => $item) {

            $aromaTypeFilter[$item['type']] = $aroma_type[$item['type']];
        }
        unset($aromaTypeFilter[0]);
        $view->aromaTypeFilter = $aromaTypeFilter;


        if ($_GET['sort'] == 'abc' || $_GET['sort'] == 'popular') {
            switch ($_GET['sort']) {
                case 'abc':
                    $select->order('title ASC');
                    break;
                case 'popular':
                    $select->order('id  ASC');
                    break;
            }
        } else {
            $select->order('title ASC');
        }


        $q_catalog_data = $db->query($select);
        while ($c_catalog_data = $q_catalog_data->fetch()) {

            if ($c_catalog_data['img'] == '') {
                $img = '/images/noimage.jpg';
                $img_big = '/images/noimage.jpg';
            } else {
                $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/small/' . $c_catalog_data['img'];
                $img_big = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
            }

            if (!$og_image) {
                if ($c_catalog_data['img'] != '') {
                    $og_image = $img;
                }
            }

            if ($c_catalog_data['pol'] == 'F') {
                $catalog_item_f[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    //'brend_title' => $brend['title'],
					'brend_title' => $c_catalog_data['brend_title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $c_catalog_data['brend_url'] . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $c_catalog_data['brend_title'],
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            } elseif ($c_catalog_data['pol'] == 'M') {
                $catalog_item_m[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    //'brend_title' => $brend['title'],
					'brend_title' => $c_catalog_data['brend_title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $c_catalog_data['brend_url'] . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $c_catalog_data['brend_url'],
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            }
        }

        //$GLOBALS['dataLayer']['PageType'] = 'ListingPage';
		
        if (is_array($catalog_item_f)) {
            $view->catalog_item_f = $catalog_item_f;
			/*
            $where = [];
            foreach ($catalog_item_f as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
			*/
        }
        if (is_array($catalog_item_m)) {
            $view->catalog_item_m = $catalog_item_m;
			/*
            $where = [];
            foreach ($catalog_item_m as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
			*/
        }

        $show_outstock = true;
        foreach ($catalog_item_f as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }
        foreach ($catalog_item_m as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }

		$checked['aroma_type']['TW'] = 1;
		$checked['pol']['F'] = 1;
		
        $view->checked = $checked;
        $view->show_outstock = $show_outstock;

		$constructor['meta_title'] = "Туалетная вода для женщин, купить в интернет-магазине Parfumoff.ru";
        $constructor['meta_description'] = "Популярная туалетная вода для женщин в интернет-магазине оригинальной парфюмерии Parfumoff. Купить женскую туалетную воду известных брендов по низкой цене с доставкой.";
		$constructor['meta_keywords'] = "Туалетная вода для женщин, модная, оригинальная, купить, цена, интернет-магазин";
		
        $constructor['og:title'] = $constructor['meta_title'];
        $constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/big/', '/small/', $og_image);
        $constructor['og:description'] = $constructor['meta_description'];
        $constructor['og:url'] = $_SERVER['REQUEST_URI'];
	
} else {
        /**
         * Filter
         */
        if ($_GET['aroma_type'] || $_GET['s100'] || $_GET['pol']) {
            //$constructor['noindex'] = true;
            foreach ($_GET['aroma_type'] as $key => $type) {
                if (!array_key_exists($type, $aroma_type)) {
                    unset($_GET['aroma_type'][$key]);
                } else {
                    $aroma_type_checked[$_GET['aroma_type'][$key]] = 1;
                }
            }
            //выбранные селекты
            $checked['aroma_type'] = $aroma_type_checked;

            $select = $db->select();
			$select->limit(50);
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type', new Zend_Db_Expr('m_catalog.title as brend_title'), new Zend_Db_Expr('m_catalog.title_1 as brend_title_1'), new Zend_Db_Expr('m_catalog.url as brend_url')]
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
			$select->joinLeft('m_catalog', 'm_catalog_data.id_catalog=m_catalog.id', '');

            if (count($_GET['s100']) > 0) {
                foreach ($_GET['s100'] as $key => $item) {
                    $item = (int)$item;
                    if ($item == 0) {
                        $select->orWhere('m_catalog_data_order.s100 <1000');
                    } elseif ($item == 10) {
                        $select->orWhere('m_catalog_data_order.s100 >10000');
                    } else {
                        $from = $item * 1000 + 1;
                        $to = $item * 1000 + 1000;
                        $select->orWhere("m_catalog_data_order.s100>{$from} and m_catalog_data_order.s100<={$to}");

                    }
                    $checked['s100'][$_GET['s100'][$key]] = 1;
                }
            }

            if (count($_GET['pol']) > 0) {
                $aPol = ['M', 'F'];
                foreach ($_GET['pol'] as $key => $item) {
                    if (in_array($item, $aPol)) {
                        $select->Where('m_catalog_data.pol=?', $item);
                        $checked['pol'][$item] = 1;
                    } else {
                        err_404();
                    }
                }
            }

            $subquery = $select->getPart(Zend_Db_Select::WHERE);
            $select->reset(Zend_Db_Select::WHERE);
			
            if (count($_GET['aroma_type']) > 0) {
                if (in_array('TW',$_GET['aroma_type'])) {
					$aroma_type_mod = $_GET['aroma_type'];
				} else {
					$aroma_type_mod = array('TW');
				}
            } else {
				$aroma_type_mod = array('TW');
			}
			$select->where('m_catalog_data_order.type  IN (?)', $aroma_type_mod);
			
            $select->where('m_catalog_data_order.price>0');
            //$select->where('m_catalog_data.id_catalog=?', $brend['id']);
			
			//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
			if ($ses_user->is_login == false) {
				$select->where('m_catalog_data.id_catalog != 106');
				$select->where('m_catalog_data.id_catalog != 83');
				$select->where('m_catalog_data.id_catalog != 176');
				$select->where('m_catalog_data.id_catalog != 460');
			}
			
            if ($subquery) {
                $select->where(implode(' ', $subquery));
            }
            $select->group('m_catalog_data.id');
        } else {
            $select = $db->select();
			$select->limit(50);
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type', new Zend_Db_Expr('m_catalog.title as brend_title'), new Zend_Db_Expr('m_catalog.title_1 as brend_title_1'), new Zend_Db_Expr('m_catalog.url as brend_url')]
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
			$select->joinLeft('m_catalog', 'm_catalog_data.id_catalog=m_catalog.id', '');
            /*if ($_GET['all']!=1){
                $select->where('m_catalog_data.price_s!=0');
            }*/
            //$select->where('m_catalog_data.id_catalog=?', $brend['id']);
			
			$select->where('m_catalog_data_order.type = "TW"');
			
			//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
			if ($ses_user->is_login == false) {
				$select->where('m_catalog_data.id_catalog != 106');
				$select->where('m_catalog_data.id_catalog != 83');
				$select->where('m_catalog_data.id_catalog != 176');
				$select->where('m_catalog_data.id_catalog != 460');
			}
						
            $select->group('m_catalog_data.id');
        }


        /**
         * s100
         */
        $s100select = clone $select;
        $s100select->reset(Zend_Db_Select::GROUP);
        $s100select->reset(Zend_Db_Select::WHERE);
		$s100select->limit(200);
        $s100select->where('m_catalog_data.price_s!=0');
        $s100select->where('m_catalog_data_order.price>0');
        //$s100select->where('m_catalog_data.id_catalog=?', $brend['id']);
		
		//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
		/*
		if ($ses_user->is_login == false) {
			$s100select->where('m_catalog_data.id_catalog != 106');
			$s100select->where('m_catalog_data.id_catalog != 83');
			$s100select->where('m_catalog_data.id_catalog != 176');
			$s100select->where('m_catalog_data.id_catalog != 460');
		}
		*/
		
        $s100 = $db->fetchAll($s100select);


        foreach ($s100 as $item) {
            switch (true) {
                case ($item['s100'] <= 1000):
                    $s100filter[0] = 'До 1000';
                    break;
                case ($item['s100'] > 1001 && $item['s100'] <= 2000):
                    $s100filter[1] = 'От 1001 до 2000';
                    break;
                case ($item['s100'] > 2001 && $item['s100'] <= 3000):
                    $s100filter[2] = 'От 2001 до 3000';
                    break;
                case ($item['s100'] > 3001 && $item['s100'] <= 4000):
                    $s100filter[3] = 'От 3001 до 4000';
                    break;
                case ($item['s100'] > 4001 && $item['s100'] <= 5000):
                    $s100filter[4] = 'От 4001 до 5000';
                    break;
                case ($item['s100'] > 5001 && $item['s100'] <= 6000):
                    $s100filter[5] = 'От 5001 до 6000';
                    break;
                case ($item['s100'] > 6001 && $item['s100'] <= 7000):
                    $s100filter[6] = 'От 6001 до 7000';
                    break;
                case ($item['s100'] > 7001 && $item['s100'] <= 8000):
                    $s100filter[7] = 'От 7001 до 8000';
                    break;
                case ($item['s100'] > 8001 && $item['s100'] <= 9000):
                    $s100filter[8] = 'От 8001 до 9000';
                    break;
                case ($item['s100'] > 9001 && $item['s100'] <= 10000):
                    $s100filter[9] = 'От 9001 до 10000';
                    break;
                case ($item['s100'] > 10000):
                    $s100filter[10] = 'Больше 10000';
                    break;
            }
        }
        ksort($s100filter);
        $view->s100filter = $s100filter;

        $aromaTypeSelect = clone $select;
        $aromaTypeSelect->reset(Zend_Db_Select::GROUP);
        $aromaTypeSelect->reset(Zend_Db_Select::WHERE);
		$aromaTypeSelect->limit(200);
        $aromaTypeSelect->where('m_catalog_data_order.price!=0');
        //$aromaTypeSelect->where('m_catalog_data.id_catalog=?', $brend['id']);
		
		//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
		/*
		if ($ses_user->is_login == false) {
			$aromaTypeSelect->where('m_catalog_data.id_catalog != 106');
			$aromaTypeSelect->where('m_catalog_data.id_catalog != 83');
			$aromaTypeSelect->where('m_catalog_data.id_catalog != 176');
			$aromaTypeSelect->where('m_catalog_data.id_catalog != 460');
		}
		*/
		
        $aromaTypeSelect->group('m_catalog_data_order.type');
        $aromaTypeFetch = $db->fetchAll($aromaTypeSelect);

		//var_dump($aromaTypeFetch);
		
        foreach ($aromaTypeFetch as $key => $item) {

            $aromaTypeFilter[$item['type']] = $aroma_type[$item['type']];
        }
        unset($aromaTypeFilter[0]);
        $view->aromaTypeFilter = $aromaTypeFilter;


        if ($_GET['sort'] == 'abc' || $_GET['sort'] == 'popular') {
            switch ($_GET['sort']) {
                case 'abc':
                    $select->order('title ASC');
                    break;
                case 'popular':
                    $select->order('id  ASC');
                    break;
            }
        } else {
            $select->order('title ASC');
        }


        $q_catalog_data = $db->query($select);
        while ($c_catalog_data = $q_catalog_data->fetch()) {

            if ($c_catalog_data['img'] == '') {
                $img = '/images/noimage.jpg';
                $img_big = '/images/noimage.jpg';
            } else {
                $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/small/' . $c_catalog_data['img'];
                $img_big = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
            }

            if (!$og_image) {
                if ($c_catalog_data['img'] != '') {
                    $og_image = $img;
                }
            }

            if ($c_catalog_data['pol'] == 'F') {
                $catalog_item_f[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    //'brend_title' => $brend['title'],
					'brend_title' => $c_catalog_data['brend_title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $c_catalog_data['brend_url'] . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $c_catalog_data['brend_title'],
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            } elseif ($c_catalog_data['pol'] == 'M') {
                $catalog_item_m[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    //'brend_title' => $brend['title'],
					'brend_title' => $c_catalog_data['brend_title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $c_catalog_data['brend_url'] . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $c_catalog_data['brend_url'],
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            }
        }

        //$GLOBALS['dataLayer']['PageType'] = 'ListingPage';
		
        if (is_array($catalog_item_f)) {
            $view->catalog_item_f = $catalog_item_f;
			/*
            $where = [];
            foreach ($catalog_item_f as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
			*/
        }
        if (is_array($catalog_item_m)) {
            $view->catalog_item_m = $catalog_item_m;
			/*
            $where = [];
            foreach ($catalog_item_m as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
			*/
        }

        $show_outstock = true;
        foreach ($catalog_item_f as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }
        foreach ($catalog_item_m as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }

		$checked['aroma_type']['TW'] = 1;
		
        $view->checked = $checked;
        $view->show_outstock = $show_outstock;
/*
        $view->brend_title = $brend['title'];
        $view->brend = $brend;
        $view->brend_anot = $brend['anot'];
        $view->brend_url = $canon_name2;
  
        //META
        
        $meta_title = $brend['meta_title'];
        $meta_title_1 = $brend['meta_title_1'];

        $view->meta_title = $meta_title;
        $view->meta_title_1 = $meta_title;

        if ($brend['title_1'] == '') {
            $m_brend_title_1 = $brend['title'];
        } else {
            $m_brend_title_1 = $brend['title_1'];
        }

		$view->body_text = $brend['text'];
		$view->seo_description = "<p>Интернет-магазин Parfumoff предлагает Вам выбрать оригинальный аромат из коллекции знаменитого парфюма от {$brend['title']}.</p><br>";
		$view->seo_description.= "<p>Купить туалетную воду или духи {$brend['title_1']}, значит, стать обладателем качественного парфюма, который будет подчеркивать Ваш высокий статус в обществе.</p><br>";
		$view->seo_description.= "<p>В нашем каталоге представлена только оригинальная парфюмерия {$brend['title']} с подробным описанием и отзывами покупателей.</p><br>";
		$view->seo_description.= "<p>Покупая парфюм в нашем магазине через интернет, вы можете быть уверены не только в качестве товара, которое подтверждается многочисленными отзывами наших клиентов, но и в том, что цена на духи {$brend['title']}, значительно ниже, чем стоимость аналогичного товара в обычном магазине.</p>";
		*/
		$constructor['meta_title'] = "Туалетная вода, купить в интернет-магазине Parfumoff.ru";
        $constructor['meta_description'] = "Популярная туалетная вода в интернет-магазине оригинальной парфюмерии Parfumoff. Купить туалетную воду известных брендов по низкой цене с доставкой.";
		$constructor['meta_keywords'] = "Туалетная вода, модная, оригинальная, купить, цена, интернет-магазин";
		
        $constructor['og:title'] = $constructor['meta_title'];
        $constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/big/', '/small/', $og_image);
        $constructor['og:description'] = $constructor['meta_description'];
        $constructor['og:url'] = $_SERVER['REQUEST_URI'];		
}
				
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$result = $db->fetchAll($db->select()->from('m_catalog')->where('is_block=0')->where('id!=106')->where('id!=83')->where('id!=176')->where('id!=460')->order('title ASC')); 
} else {
	$result = $db->fetchAll($db->select()->from('m_catalog')->where('is_block=0')->order('title ASC'));
}
foreach ($result as $item) {
	$letter = trim(mb_substr($item['title'],0,1));
	if (!is_numeric($letter) && $letter!=''){
		$aBrands['letters'][$letter][] = $item;
	}elseif(is_numeric($letter)){
		$aBrands['numbers']['1-9'][] = $item;
	}
}
$view->aBrands = $aBrands;
$constructor['content'] = $view->render('content_catalog_tw.php');