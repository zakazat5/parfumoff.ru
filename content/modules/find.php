<?php


if ($canon_name2){
    err_301($canon_name1.'/');
}

$sql_m_catalog_data_a_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$sql_m_catalog_data_a_block = 'AND a.id_catalog != 106 AND a.id_catalog != 83 AND a.id_catalog != 176 AND a.id_catalog != 460 ';
}


if (strlen($_POST['find']) > 100) err_404();
$find = preg_replace("[^ а-яa-z0-9&#;]", '', format_text(trim($_POST['find'])));
$str_count = strlen(format_text_out($find));
$limit = '';
if ($str_count < 3) $limit = "LIMIT 99";
if ($str_count!=0) {
/*
	$select = $db->select()
		->from(['a'=>'m_catalog_data'])
		->joinLeft(['b'=>'m_catalog'],'b.id=a.id_catalog',['brent_title'=>'b.title','brend_url'=>'b.url'])
		->where('a.price_s > 0')->where($sql_m_catalog_data_a_block)->where('a.find LIKE ?', "%$find%")->orWhere('a.find_ru LIKE ?', "%$find%")->orWhere('a.articul LIKE ?', "%$find%")->orWhere('a.title LIKE ?', "%$find%")->orWhere('b.title LIKE ?', "%$find%")->limit(99);
*/
$select ="SELECT a.*, b.title AS brent_title, b.url AS brend_url FROM m_catalog_data AS a LEFT JOIN m_catalog AS b ON b.id=a.id_catalog "
			."WHERE (a.find LIKE '%$find%' OR a.find_ru LIKE '%$find%' OR a.articul LIKE '%$find%' OR a.title LIKE '%$find%' OR b.title LIKE '%$find%') "
			."AND a.price_s > 0 ".$sql_m_catalog_data_a_block."LIMIT 99";
	$find_select = $db->query($select);
	$aItems = $find_select->fetchAll();
	//$aItems = $db->fetchAll($select);
	foreach ($aItems as $c_catalog) {
		if ($c_catalog['img']=='') {
			$img = '/images/noimage.jpg';
			$img_big = '/images/noimage.jpg';
		}else {
			$img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
			$img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
		}
		$catalog_item[] = array(
			'title' 		=> $c_catalog['title'],
			'brend_title' 	=> $c_catalog['brend_title'],
			'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
			'price_e' 		=> $c_catalog['price_e'],
			'img' 			=> $img,
			'img_big' 		=> $img_big,
			'url' 			=> '/production/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
			'id_catalog'	=> $c_catalog['id'],
			'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...'
		);
	}

	$view->catalog_item = $catalog_item;
	$view->find = $find;

}

$view->str_find = format_text_out($find);
$constructor['content'] = $view->render('find.php');