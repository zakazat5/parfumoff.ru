<?php  
if ($canon_name2){
    err_301($canon_name1.'/');
}

if ($canon_name1 == 'thankyou_call'){
	if (isset($_POST['callMe_name'])) {
		$name = trim($_POST['callMe_name']);
		$phone = trim($_POST['callMe_phone']);
		$message = "
			Имя: $name <br>
			Телефон: $phone <br>
			Дата: ".date('Y-m-d H:i:s')." <br>
			";
		$call = array(
			'name' => $name,
			'phone' => $phone,
			'created_at' => time(),
		);
		$db->insert('m_back_call', $call);
	}
	$constructor['meta_title'] = "";
	$constructor['meta_description'] = "";
	$constructor['content'] = $view->render('content_thankyou_call.php');
}

if ($canon_name1 == 'thankyou_quickorder'){
	$constructor['meta_title'] = "";
	$constructor['meta_description'] = "";
	
	if (isset($_SESSION['dt_qb_TransactionID'])) {
		$GLOBALS['dataLayer']['PageType'] = 'TransactionPage';
		$GLOBALS['dataLayer']['ProductTransactionProducts'][] = array(
				'id' => $_SESSION['dt_qb_id'],
				'price' => $_SESSION['dt_qb_price'],
				'quantity' => 1
			);
		$GLOBALS['dataLayer']['email'] = trim(strtolower($_SESSION['dt_qb_email']));
		$GLOBALS['dataLayer']['TransactionID'] = $_SESSION['dt_qb_TransactionID'];
		$_SESSION['dt_qb_id'] = null;
		$_SESSION['dt_qb_price'] = null;
		$_SESSION['dt_qb_email'] = null;
		$_SESSION['dt_qb_TransactionID'] = null;
	}
	
	$constructor['content'] = $view->render('content_thankyou_quickorder.php');
}

?>