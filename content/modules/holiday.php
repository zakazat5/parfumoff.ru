<?php
$per_page= 25;
$page = ($canon_name3)?$canon_name3:1;

$page = (int)(($page-1)*$per_page);


$select = "SELECT "
."a.id, a.title, a.url, a.pol, a.price_s, a.img, a.text, a.tag, "
."b.title AS brend_title, b.url AS brend_url "
."FROM m_catalog_data AS a "
."JOIN m_catalog AS b ON b.id=a.id_catalog "
."WHERE a.id_cat=82 AND a.is_holiday=1 and a.pol='M' "
."LIMIT $page,$per_page";

$q_catalog = $db->query($select);
while ($c_catalog = $q_catalog->fetch()) {
	if ($c_catalog['img']=='') {
		$img = '/images/noimage.jpg';
		$img_big = '/images/noimage.jpg';
	}else {
		$img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
		$img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
	}
	if ($c_catalog['tag'] != '') {
		$tag = explode(',', $c_catalog['tag']);
		foreach ($tag as $key => $value) {
			if ($value == '') continue;
			$item_tag[] = array(
			'url' => translit($value),
			'title' => $value,
			);
		}
	}

	/* -=========================================
	ИЗБРАННОЕ
	*/
	if(!empty($ses_note->id)){
		if (in_array($c_catalog['id'], $ses_note->id)){
			$is_note = true;
		}else{
			$is_note = false;
		}
	}else {
		$is_note = false;
	}

	$catalog_item_m[] = array(
	'id' 			=> $c_catalog['id'],
	'pol' 			=> $c_catalog['pol'],
	'title' 		=> $c_catalog['title'],
	'brend_title' 	=> $c_catalog['brend_title'],
	'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
	'img' 			=> $img,
	'img_big' 		=> $img_big,
	'url' 			=> '/catalog/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
	'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...',
	'tag' 			=> $item_tag,
	'is_note' 		=> $is_note,
	);
	
	$item_tag = null;
}




$select = "SELECT "
    ."a.id, a.title, a.url, a.pol, a.price_s, a.img, a.text, a.tag, "
    ."b.title AS brend_title, b.url AS brend_url "
    ."FROM m_catalog_data AS a "
    ."JOIN m_catalog AS b ON b.id=a.id_catalog "
    ."WHERE a.id_cat=82 AND a.is_holiday=1 and a.pol='F' "
    ."LIMIT $page,$per_page";

$q_catalog = $db->query($select);
while ($c_catalog = $q_catalog->fetch()) {
    if ($c_catalog['img']=='') {
        $img = '/images/noimage.jpg';
        $img_big = '/images/noimage.jpg';
    }else {
        $img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
        $img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
    }
    if ($c_catalog['tag'] != '') {
        $tag = explode(',', $c_catalog['tag']);
        foreach ($tag as $key => $value) {
            if ($value == '') continue;
            $item_tag[] = array(
                'url' => translit($value),
                'title' => $value,
            );
        }
    }

    /* -=========================================
    ИЗБРАННОЕ
    */
    if(!empty($ses_note->id)){
        if (in_array($c_catalog['id'], $ses_note->id)){
            $is_note = true;
        }else{
            $is_note = false;
        }
    }else {
        $is_note = false;
    }

    $catalog_item_f[] = array(
        'id' 			=> $c_catalog['id'],
        'pol' 			=> $c_catalog['pol'],
        'title' 		=> $c_catalog['title'],
        'brend_title' 	=> $c_catalog['brend_title'],
        'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
        'img' 			=> $img,
        'img_big' 		=> $img_big,
        'url' 			=> '/catalog/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
        'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...',
        'tag' 			=> $item_tag,
        'is_note' 		=> $is_note,
    );

    $item_tag = null;
}

$count_m =  $db->fetchOne('SELECT count(*) FROM m_catalog_data WHERE is_holiday=1 and pol="M"');
$count_f =  $db->fetchOne('SELECT count(*) FROM m_catalog_data WHERE is_holiday=1 and pol="F"');

$count = ($count_m>$count_f)?$count_m:$count_f;


$p 		= pages_content($count, $per_page, $canon_name3, 'pages.tpl');




$view->catalog_item_m = $catalog_item_m;
$view->catalog_item_f = $catalog_item_f;


$view->p = $p;




$constructor['meta_title'] = 'Парфюмерия на День всех влюбленных! ';
$constructor['content'] = $view->render('content_holiday.php');
?>