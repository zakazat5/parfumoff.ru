<?php


require_once $base_path.'/includes/library/Int/Service/Amazon/Ses.php';
require_once $base_path.'/includes/library/Int/Service/Amazon/Ses/Email.php';

switch ($canon_name2) {

    case 'unsubscribe':

        $invite = $canon_name3;
        if( !empty($invite) ){

            $data_update = array(
                'is_maillist' => 0,
            );

            $db->update('m_mag_Users', $data_update, "email_hash='{$invite}' ");
        }
        $constructor['content'] = $view->render('users/unsubscribe.php');


        break;

    case 'reset':
        /* -=========================================
        ВОСТАНОВЛЕНИЕ ПАРОЛЯ
        */

        if (isset($_POST['send_passw'])){

            $email = trim($_POST['forgot_email']);
            if(valid_email($email)){

                $c_user = $db->fetchRow("SELECT id,fio,email FROM m_mag_Users WHERE email='{$email}' LIMIT 1 ");
                if(!empty($c_user)){

                    $passw = generate_password_simple(4);
                    $md_passw = md5($passw);

                    $data_update = array(
                        'passw'	=> $md_passw,
                    );

                    $view->fio = $c_user['fio'];
                    $view->email = $c_user['email'];
                    $view->passw = $passw;

                    $message = $view->render("orders_mail_forgot.php");


                    /*Int_Service_Amazon_Ses::setKeys('AKIAJRZF4VNOC6YNBAZQ', 'Z3XtxspW9qkUUf/8K3WiYYwGyeLxOvPKvU47gvFL');

                    $mail = new Int_Service_Amazon_Ses_Email();
                    $mail->addTo($email)
                        ->setFrom('orders@parfumoff.ru', 'parfumoff')
                        ->setSubject('Восстановление пароля [parfumoff.ru]')
                        ->setReturnPath('orders@parfumoff.ru')
                        ->setBodyHtml($message, 'utf-8');

                    $ses = new Int_Service_Amazon_Ses();
                    $messageId = $ses->sendEmail($mail);*/

                    $mail = new Zend_Mail('utf-8');
                    $mail->setBodyHtml($message);
                    $mail->setFrom('orders@parfumoff.ru', 'parfumoff');
                    $mail->addTo($email, $c_user['fio']);
                    $mail->setSubject('Восстановление пароля [parfumoff.ru]');
                    $mail->send();

                    $db->update('m_mag_Users', $data_update, "id={$c_user['id']} ");

                    $view->err = "На почтовый ящик $email Вам выслан новый пароль.";

                }else{
                    $view->err = 'данный E-mail не существует';
                }

            }else {
                $view->err = 'неверный E-mail';
            }


        }


        $constructor['content'] = $view->render('users/forgot-password.php');
        break;



    case 'register':
        /* -=========================================
        СТРАНИЦА РЕГИСТРАЦИИ
        */

        // если уже зарегестрирован
        if($ses_user->is_login) go('/users/');

        $view->capital_districts = $capital_districts;

        if((count($_POST)>0)){

            $data['fio'] 		= $_POST['regs_fio'];
            $data['phone'] 		= eregi_replace('[^0-9 ()-]', '', $_POST['regs_phone']);
            $data['email'] 		= trim($_POST['regs_email']);

            $data['p'] 		= intval($_POST['regs_p']);
            $data['passw'] 		= trim($_POST['regs_passw']);
            $data['repassw'] 	= trim($_POST['regs_repass']);
            $data['is_maillist'] = isset($_POST['regs_maillist']) ? 1:0;

            //new data
            $data['address-region'] = $_POST['region'];
            $data['address-part'] = $_POST['part'];
            $data['address-other'] = $_POST['other'];
            $data['address-city'] = $_POST['city'];
            $data['address-street'] = $_POST['street'];
            $data['address-settlement'] = $_POST['settlement'];
            $data['address-house'] = $_POST['house'];
            $data['address-corpus'] = $_POST['corpus'];
            $data['address-floor'] = $_POST['floor'];
            $data['address-stroenie'] = $_POST['stroenie'];
            $data['address-domophone'] = $_POST['domophone'];
            $data['address-flat'] = $_POST['flat'];
            $data['postcode'] = $_POST['regs_postcode'];

            $data['fio_data'] = (trim($_POST['fio_data']));
            $data['address_data'] = (trim($_POST['address_data']));

            $address_data = json_decode(iconv('cp1251','utf8',$data['address_data']));
            $region = (iconv('utf-8','cp1251',$address_data->region));
            $data['region'] = ($region=='г Москва')?'moscow':'region' ;


            $view->data = $data;
            $flag = true;

            if (valid_email($data['email'])){

                $is_email = $db->fetchOne("SELECT count(*) FROM m_mag_Users WHERE email='{$data['email']}' ");
                if($is_email == 0){


                    if( $data['fio']=='' ){
                        $view->err_fio = 'Заполните поле "ФИО".';
                        $flag = false;
                        $constructor['content'] = $view->render('users/register.php');

                    }elseif ( $data['phone']=='' ){
                        $view->err_phone = 'Заполните поле "Телефон".';
                        $flag = false;
                        $constructor['content'] = $view->render('users/register.php');

                    }

                    /* -=========================================
                    АДРЕС
                    */
                    if ( $data['region']==1 ){

                        if( $data['address']=='' ){
                            $view->err_address = 'Заполните поле "Адрес".';
                            $flag = false;
                            $constructor['content'] = $view->render('users/register.php');
                        }

                    }elseif ( $data['region']==2 ){

                        if( $data['city']=='' ){
                            $view->err_city = 'Заполните поле "Город".';
                            $flag = false;
                            $constructor['content'] = $view->render('users/register.php');

                        }elseif ( $data['postcode']=='' ){
                            $view->err_address = 'Заполните поле "Индекс".';
                            $flag = false;
                            $constructor['content'] = $view->render('users/register.php');

                        }elseif ( $data['address']=='' ){
                            $view->err_address = 'Заполните поле "Адрес".';
                            $flag = false;
                            $constructor['content'] = $view->render('users/register.php');

                        }

                    }

                    /* -=========================================
                    ПАРОЛЬ
                    */
                    if ( $data['p']==2 ){

                        if ($data['passw']=='' OR $data['repassw']=='' ){
                            $view->err_passw = 'Введите пароль.';
                            $flag = false;
                            $constructor['content'] = $view->render('users/register.php');

                        }elseif( $data['passw'] != $data['repassw'] ) {
                            $view->err_passw = 'Пароли не совпадают.';
                            $flag = false;
                            $constructor['content'] = $view->render('users/register.php');

                        }else{
                            $passw = md5($data['passw']);
                        }

                    }else {
                        $data['passw'] = generate_password_simple(4);
                        $passw = md5($data['passw']);
                    }

                    $view->data['passw'] = $data['passw'];


                    if ($flag){
                        $m_catalog_client = array(
                            'email'				=> $data['email'],
                            'fio'				=> $data['fio'],
                            'phone'				=> $data['phone'],
                            'is_maillist'		=> $data['is_maillist'],
                            'is_block'			=> 0,
                            'region'			=> ($data['region']==1) ? 'moscow' : 'region',
                            'passw'				=> $passw,
                            'date_register'		=> new Zend_Db_Expr('NOW()'),
                            'date_last_visit'	=> new Zend_Db_Expr('NOW()'),
                            'fio_data' => $data['fio_data'],
                            'address_data' => $data['address_data'],
                            'address_region' => $data['address-region'],
                            'address_part' => $data['address-part'],
                            'address_other' => $data['address-other'],
                            'address_city' => $data['address-city'],
                            'address_settlement' => $data['address-settlement'],
                            'address_street' => $data['address-street'],
                            'address_house' => $data['address-house'],
                            'address_corpus' => $data['address-corpus'],
                            'address_stroenie' => $data['address-stroenie'],
                            'address_floor' => $data['address-floor'],
                            'address_flat' => $data['address-flat'],
                            'address_domophone' => $data['address-domophone'],
                            'postcode' => $data['postcode']
                        );
                        $db->insert('m_mag_Users', $m_catalog_client);
                        $data['user_id'] = $db->lastInsertId();


                        // инициализируем сессиии
                        $ses_user->is_login = true;
                        $ses_user->user = $db->fetchRow("
							SELECT 
								* 
							FROM 
								m_mag_Users 
							WHERE 
								id='{$data['user_id']}'
						");

                        // сообщение о регистрации
                        $message_register = $view->render("orders_mail_register.php");
                        if($data['email']!=''){

                            $mail = new Zend_Mail('utf-8');
                            $mail->setBodyHtml($message);
                            $mail->setFrom('info@parfumoff.ru', 'parfumoff');
                            $mail->addTo('info@parfumoff.ru');
                            $mail->setSubject('Заявка на обратный звонок [Parfumoff]');
                            $mail->send();


                            $mail = new Zend_Mail('utf-8');
                            $mail->addTo($data['email'])
                                ->setFrom('orders@parfumoff.ru', 'parfumoff')
                                ->setSubject('Вы зарегистрированы в интернет магазине [parfumoff.ru]')
                                ->setReturnPath('orders@parfumoff.ru')
                                ->setBodyHtml($message_register, 'utf-8');

                            $mail->send();
                        }
                        // переход в личный кабинет.
                        go('/users/');


                    }

                }else{
                    $view->err_email = 'Данный e-mail занят.';
                    $constructor['content'] = $view->render('users/register.php');
                }

            }else{
                $view->err_email = 'Неверный e-mail.';
                $constructor['content'] = $view->render('users/register.php');
            }

        }else {
            $constructor['content'] = $view->render('users/register.php');
        }

        //$view->id_captcha = generateCaptcha();


        break;

    case 'login':

        /* -=========================================
        СТРАНИЦА ЛОГИНА
        */

        if($ses_user->is_login){
            switch($_POST['back_url']){
                case '/korzina/':
                    go('/korzina/');
                    break;
                case '/thankyou_basket/':
                    go('/korzina/thankyou_basket/');
                    break;
                case '/korzina/thankyou_basket_force/':
                    go('/korzina/thankyou_basket_force/');
                    break;

                default:
                    go('/users/');
                    break;
            }
        }

        if (isset($_POST['register'])){
            // если уже зарегестрирован
            // логинимся
            $email = trim($_POST['log_email']);
            $passw = md5($_POST['log_passw']);

            if (valid_email($email) AND $passw!='' ) {

                $is_auth = $db->fetchOne("SELECT count(*) FROM m_mag_Users WHERE email='$email' AND passw='$passw'  ");
                if( $is_auth !=0 ) {

                    $user = $db->fetchRow("
						SELECT 
							*
						FROM 
							m_mag_Users 
						WHERE 
							email='$email' 
					");

                    // инициализируем сессиии
                    $ses_user->user 	= $user;
                    $ses_user->is_login = true;
                    $ses_note->id 		= $user['note'];

                    // обновляем дату посещения
                    $db->update('m_mag_Users', array('date_last_visit'=>new Zend_Db_Expr('NOW()')), "id={$user['id']} ");

                    if ($_POST['back_url']!=''){
                        switch($_POST['back_url']){
                            case '/korzina/':
                                go('/korzina/');
                                break;
                            case '/thankyou_basket/':
                                go('/korzina/thankyou_basket/');
                                break;
                            case '/korzina/thankyou_basket_force/':
                                go('/korzina/thankyou_basket_force/');
                                break;

                            default:
                                //go('/users/');
                                break;
                        }
                    }else{
                        go($_SERVER['HTTP_REFERER']);
                    }

                }else{
                    $view->err = 'Ваш E-Mail или пароль неверны.';
                }

            }else{
                $view->err = 'Ваш E-Mail или пароль неверны.';
            }
        }

        $constructor['content'] = $view->render('users/login.php');

        break;

    case 'register-ajax':
        $tmpl = '';

        $data['fio'] = $_POST['fio'];
        $data['fio_data'] = $_POST['fio_data'];
        $data['phone'] = preg_replace('/[^0-9]/', '', $_POST['phone']);
        $data['email'] = trim($_POST['log_email']);

        if ($data['fio']==''){
            $aErrors['fio'] = true;
        }

        if ($data['phone']==''){
            $aErrors['phone'] = true;
        }

        if ($data['email']==''){
            $aErrors['email'] = true;
        }

        if ($aErrors){
            echo json_encode(['errors'=>$aErrors]);

            return;
        }


        if (!$oUser = $db->fetchRow($db->select()->from('m_mag_Users')->where('email=?', $data['email']))) {
            $passw = generate_password_simple(6);
            $data['passw'] = md5($passw);
            $data['date_register'] = new Zend_Db_Expr('NOW()');
            $data['date_last_visit'] = new Zend_Db_Expr('NOW()');

            $data['utm_source_first'] = (isset($_COOKIE['utm_source_first']))?$_COOKIE['utm_source_first']:'';
            $data['utm_medium_first'] = (isset($_COOKIE['utm_medium_first']))?$_COOKIE['utm_medium_first']:'';
            $data['utm_campaign_first'] = (isset($_COOKIE['utm_campaign_first']))?$_COOKIE['utm_campaign_first']:'';
            $data['utm_term_first'] = (isset($_COOKIE['utm_term_first']))?$_COOKIE['utm_term_first']:'';

            if ($data['fio_data']!=''){
                $dadata = json_decode($data['fio_data']);
                if ($dadata->value==$data['fio']){
                    if ($dadata->data->name!=null){
                        $data['name'] = $dadata->data->name;
                    }
                    if ($dadata->data->patronymic!=null){
                        $data['patronymic'] =$dadata->data->patronymic;
                    }
                    if ($dadata->data->surname!=null){
                        $data['surname'] =$dadata->data->surname;
                    }
                    if ($dadata->data->gender!=null){
                        if ($dadata->data->gender=='FEMALE'){
                            $data['pol'] = 'F';
                        }
                        if ($dadata->data->gender=='MALE'){
                            $data['pol'] = 'M';
                        }
                    }
                }
            }
            $db->insert('m_mag_Users', $data);
            $data['id'] = $db->lastInsertId();
            $data['passw'] = $passw;
            // инициализируем сессиии
            $ses_user->is_login = true;
            $ses_user->user = $db->fetchRow($db->select()->from('m_mag_Users')->where('id=?', $data['id']));

            $view->data = $data;

            // сообщение о регистрации
            $message_register = $view->render("orders_mail_register.php");
            if ($data['email'] != '') {
                $mail = new Zend_Mail('utf-8');
                $mail->addTo($data['email'])
                    ->setFrom('orders@parfumoff.ru', 'parfumoff')
                    ->setSubject('Вы зарегистрированы в интернет магазине [parfumoff.ru]')
                    ->setReturnPath('orders@parfumoff.ru')
                    ->setBodyHtml($message_register, 'utf-8');

                $mail->send();
            }
            // переход в личный кабинет.
            echo json_encode(['login'=>true]);
        }else{
            $ses_user->is_login = true;
            $ses_user->user = $oUser;
            if ($oUser['phone']!=$data['phone'] &&  $oUser['phone2']!=$data['phone']){
                $db->update('m_mag_Users',['phone2'=>$data['phone']],'id='.$oUser['id']);
            }
            echo json_encode(['login'=>true]);
        }
        break;

    case 'login-ajax':
        $tmpl = '';

        $email = trim($_POST['log_email']);
        $phone = trim($_POST['phone']);
        $phone = preg_replace('/[^0-9]/', '', $phone);

        if ($email !='' && valid_email($email)) {
            $query = $db->select()->from('m_mag_Users')->where('email=?', $email);


            if ($user = $db->fetchRow($query)) {
                // инициализируем сессиии
                $ses_user->user = $user;
                $ses_user->is_login = true;
                $ses_note->id = $user['note'];

                // обновляем дату посещения
                $db->update('m_mag_Users', array('date_last_visit' => new Zend_Db_Expr('NOW()')), "id={$user['id']} ");
                echo json_encode(['login' => true]);
            } else {
                echo json_encode(['login' => false]);
            }
        } elseif($phone!='') {
            $query = $db->select()->from('m_mag_Users')->where('phone=?', $phone)->orWhere('phone2=?', $phone);
            if ($user = $db->fetchRow($query)) {
                // инициализируем сессиии
                $ses_user->user = $user;
                $ses_user->is_login = true;
                $ses_note->id = $user['note'];

                // обновляем дату посещения
                $db->update('m_mag_Users', array('date_last_visit' => new Zend_Db_Expr('NOW()')), "id={$user['id']} ");
                echo json_encode(['login' => true]);
            } else {
                echo json_encode(['login' => false]);
            }
        }




        break;


    case 'edit':
        /* -=========================================
		Сохранение второго шага заказа
		*/
        // если не зашел
        if(!$ses_user->is_login) go('/users/login/');

        // полные данные из таблицы
        $id = intval($ses_user->user['id']);
        $user_options = $db->fetchRow("SELECT * FROM m_mag_Users WHERE id='$id'  ");

        // шаблон
        $view->users_options = $user_options;
        $view->users_options_birthday = explode('-', $user_options['date_birthday']);
        // нажал сохранить
        if(isset($_POST['save_options'])){

            $data['region'] 	= ($_POST['regs_region']==1) ? 'moscow' : 'region';
            $data['city'] 		= eregi_replace('[^А-Яа-яa-zA-Z ]', '', $_POST['regs_city']);
            $data['postcode'] 	= eregi_replace('[^0-9]', '', $_POST['regs_postcode']);
            $data['address'] 	= eregi_replace('[^-a-zА-Яа-я0-9\.\, ]', '', $_POST['regs_address']);

            /* -=========================================
            вносим изменения в базу
            */
            $db->update('m_mag_Users', $data, "id={$user_options['id']} ");

            unset($data);
            $data['pComment'] = HTMLToTxt( trim(substr( $_POST['regs_comment'],0,1000)) );

            $db->update('m_mag_Orders', $data, "id='".intval($_SESSION['unic'])."'");

            $ses_user->user = $db->fetchRow("SELECT * FROM m_mag_Users WHERE id='$id'  ");
            go('/users/options/');
        }


        $constructor['content'] = $view->render('users/users_options.php');
        break;


    case 'options':
        /* -=========================================
        ОПЦИИ
        */

        // если не зашел
        if(!$ses_user->is_login) go('/users/login/');

        // полные данные из таблицы
        $id = intval($ses_user->user['id']);
        $user_options = $db->fetchRow("SELECT * FROM m_mag_Users WHERE id='$id'  ");

        // шаблон
        $view->users_options = $user_options;

        $view->capital_districts = $capital_districts;


        $view->users_options_birthday = explode('-', $user_options['date_birthday']);


        // нажал сохранить
        if(isset($_POST['save_options'])){

            // fio
            $data['fio'] = $_POST['fio'];

            // Пол
            if($_POST['pol']==1) $data['pol'] = 'M';
            elseif($_POST['pol']==2) $data['pol'] = 'F';
            else $data['pol'] = 'F';

            // День рождения
            if( $_POST['day']!=0 AND $_POST['month']!=0 AND $_POST['year']!=0 ){
                $day 	= intval($_POST['day']);
                $month 	= intval($_POST['month']);
                $year 	= intval($_POST['year']);
                $data['date_birthday'] = "$year-$month-$day";
            }else{
                $data['date_birthday'] = '';
            }

            $data['phone'] 		= eregi_replace('[^0-9 ()-]', '', $_POST['phone']);

            // Пароль
            if($_POST['passw']!='' AND $_POST['repassw']!=''){
                if($_POST['passw']==$_POST['repassw']) {
                    $data['passw'] = md5($_POST['passw']);
                }else{
                    $data['passw'] = $user_options['passw'];
                }
            }

            /* -=========================================
            вносим изменения в базу
            */
            $db->update('m_mag_Users', $data, "id={$user_options['id']} ");

            $ses_user->user = $db->fetchRow("SELECT * FROM m_mag_Users WHERE id='$id'  ");
            go('/users/options/');
        }


        $constructor['content'] = $view->render('users/users_options.php');
        break;

    case 'discounts':
        $constructor['content'] = $view->render('users/users_discounts.php');
        break;



    case 'favorite':

        // если не зашел
        if(!$ses_user->is_login) go('/users/login/');

        $note = implode(',', $ses_note->id);

        if($note){

            $select = "
			SELECT 
				a.id, a.title, a.url, a.price_s, a.img, a.text, a.pol, a.tag, 
				b.title AS brend_title, b.url AS brend_url 
			FROM 
				m_catalog_data AS a 
			JOIN 
				m_catalog AS b ON b.id=a.id_catalog 
			WHERE 
				a.id IN ($note) 
			";
            $q_catalog = $db->fetchAll($select);


            foreach ($q_catalog as $c_catalog_data) {

                if ($c_catalog_data['tag'] != '') {
                    $tag = explode(',', $c_catalog_data['tag']);
                    foreach ($tag as $key => $value) {

                        if ($value == '') continue;

                        $item_tag[] = array(
                            'url' => translit($value),
                            'title' => $value,
                        );
                    }
                }

                $catalog_item[] = array(
                    'id' 			=> $c_catalog_data['id'],
                    'title' 		=> $c_catalog_data['title'],
                    'brend_title' 	=> $c_catalog_data['brend_title'],
                    'brend_url' 	=> "/catalog/{$c_catalog_data['brend_url']}/",
                    'price_s' 		=> $c_catalog_data['price_s']+$_SESSION['_GMARGIN'],
                    'pol' 			=> ($c_catalog_data['pol'] == 'M') ? 'мужская парфюмерия' : 'женская парфюмерия',
                    'img' 			=> '/images/uploads/catalog/'.$c_catalog_data['id'].'/small/'.$c_catalog_data['img'],
                    'url' 			=> '/catalog/'.$c_catalog_data['brend_url'].'/'.$c_catalog_data['url'].'/',
                    'anot' 			=> maxsite_str_word($c_catalog_data['text'], 9) .' ...',
                    'tag' 			=> $item_tag,
                );

                $item_tag = null;

            }
            $view->catalog_item = $catalog_item;
        }

        $constructor['content'] = $view->render('users/users_favorite.php');
        break;


    case 'history':
        /* -=========================================
        История покупок
        */
        // если не зашел
        if(!$ses_user->is_login) go('/users/login/');

        $status = intval($canon_name4);
        if($status==0) go('/users/');
        $view->status = $status;

        $orders = $db->fetchAll("
			SELECT 
				*
			FROM 
				m_mag_Orders 
			WHERE 
				id_user='{$ses_user->user['id']}' AND status='$status'
			ORDER BY DateAdd DESC 
		");

        foreach ($orders as $vorders) {

            $order = $db->fetchAll("
				SELECT 
					a.id_catalog_data, a.title, a.sum, a.kol, a.is_sale,
					b.img
				FROM 
					m_mag_OrdersSum AS a
				LEFT JOIN 
					m_catalog_data AS b ON b.id=a.id_catalog_data
				WHERE 
					a.id_order='{$vorders['id']}' 
			");

            $data_order[] = array(
                'id' 				=> $vorders['id'],

                'dType' 			=> $vorders['dType'],
                'dSum' 				=> $vorders['dSum'],
                'Sum' 				=> $vorders['Sum'],
                'dPercent' 			=> $vorders['dPercent'],

                'DateAdd' 			=> $vorders['DateAdd'],
                'status' 			=> $vorders['status'],
                'order' 			=> $order
            );

        }
        $view->orders = $data_order;

        $constructor['content'] = $view->render('users/users_history.php');
        break;

    case 'comments':

        // если не зашел
        if(!$ses_user->is_login) go('/users/login/');
        $user_id = intval($ses_user->user['id']);

        $select = "
			SELECT 
				a.id, a.title, a.url, a.price_s, a.img, a.pol,
				c.id AS comment_id, c.user_date AS comment_date, c.user_text AS comment_text,
				b.title AS brend_title, b.url AS brend_url 
			FROM 
				m_catalog_com AS c
			LEFT JOIN 
				m_catalog_data AS a ON a.id=c.id_catalog_data
			LEFT JOIN 
				m_catalog AS b ON b.id=a.id_catalog
			WHERE 
				c.id_user='$user_id'
			ORDER BY 	
				c.id DESC
			LIMIT 40
		";
        $q_catalog = $db->fetchAll($select);

        foreach ($q_catalog as $c_catalog_data) {

            $catalog_item[] = array(
                'id' 			=> $c_catalog_data['id'],
                'title' 		=> $c_catalog_data['title'],
                'brend_title' 	=> $c_catalog_data['brend_title'],
                'brend_url' 	=> "/catalog/{$c_catalog_data['brend_url']}/",
                'comment_id' 	=> $c_catalog_data['comment_id'],
                'comment_date' 	=> $c_catalog_data['comment_date'],
                'comment_text' 	=> $c_catalog_data['comment_text'],
                'price_s' 		=> $c_catalog_data['price_s']+$_SESSION['_GMARGIN'],
                'pol' 			=> ($c_catalog_data['pol'] == 'M') ? 'мужская парфюмерия' : 'женская парфюмерия',
                'img' 			=> '/images/uploads/catalog/'.$c_catalog_data['id'].'/small/'.$c_catalog_data['img'],
                'url' 			=> '/catalog/'.$c_catalog_data['brend_url'].'/'.$c_catalog_data['url'].'/',
            );

        }
        $view->catalog_item = $catalog_item;



        $constructor['content'] = $view->render('users/users_comments.php');
        break;
    case 'wishlist':
        $constructor['content'] = $view->render('users/users_wishlist.php');
        break;
    case 'involve':
        $constructor['content'] = $view->render('users/users_involve.php');
        break;
    case 'gifts':
        $constructor['content'] = $view->render('users/users_gifts.php');
        break;
    case 'exit':
        Zend_Session::namespaceUnset('user');
        //$classCart->clean();
        go('/');
        break;

    case 'sale':
        $select = "
			SELECT 
				a.id, a.title, a.url, a.price_s, a.img, a.text, a.pol, a.tag, 
				b.title AS brend_title, b.url AS brend_url 
			FROM 
				m_catalog_data AS a 
			JOIN 
				m_catalog AS b ON b.id=a.id_catalog 
			WHERE 
				a.id_cat=82 AND a.is_sale=1 AND a.price_s != 0 
			LIMIT 10
		";

        $q_catalog_top = $db->query($select);
        while ($c_catalog_data = $q_catalog_top->fetch()) {

            if ($c_catalog_data['tag'] != '') {
                $tag = explode(',', $c_catalog_data['tag']);
                foreach ($tag as $key => $value) {

                    if ($value == '') continue;

                    $item_tag[] = array(
                        'url' => translit($value),
                        'title' => $value,
                    );
                }
            }

            /* -=========================================
            ИЗБРАННОЕ
            */
            if(!empty($ses_note->id)){
                if (in_array($c_catalog_data['id'], $ses_note->id)){
                    $is_note = true;
                }else{
                    $is_note = false;
                }
            }else {
                $is_note = false;
            }

            $catalog_item_top[] = array(
                'id' 			=> $c_catalog_data['id'],
                'is_note' 		=> $is_note,
                'title' 		=> $c_catalog_data['title'],
                'brend_title' 	=> $c_catalog_data['brend_title'],
                'brend_url' 	=> "/catalog/{$c_catalog_data['brend_url']}/",
                'price_s' 		=> $c_catalog_data['price_s']+$_SESSION['_GMARGIN'],
                'pol' 			=> ($c_catalog_data['pol'] == 'M') ? 'мужская парфюмерия' : 'женская парфюмерия',
                'img' 			=> '/images/uploads/catalog/'.$c_catalog_data['id'].'/small/'.$c_catalog_data['img'],
                'url' 			=> '/catalog/'.$c_catalog_data['brend_url'].'/'.$c_catalog_data['url'].'/',
                'anot' 			=> maxsite_str_word($c_catalog_data['text'], 9) .' ...',
                'tag' 			=> $item_tag,
            );

            $item_tag = null;

        }
        $view->catalog_item_top = $catalog_item_top;


        $constructor['content'] = $view->render('users/users_sale.php');

        break;

    default:

        // если не зашел
        if(!$ses_user->is_login) go('/users/login/');

        $select = "
			SELECT 
				a.id, a.title, a.url, a.price_s, a.img, a.text, a.pol, a.tag, 
				b.title AS brend_title, b.url AS brend_url 
			FROM 
				m_catalog_data AS a 
			JOIN 
				m_catalog AS b ON b.id=a.id_catalog 
			WHERE 
				a.id_cat=82 AND a.is_sale=1 AND a.price_s != 0 
			LIMIT 10
		";

        $q_catalog_top = $db->query($select);
        while ($c_catalog_data = $q_catalog_top->fetch()) {

            if ($c_catalog_data['tag'] != '') {
                $tag = explode(',', $c_catalog_data['tag']);
                foreach ($tag as $key => $value) {

                    if ($value == '') continue;

                    $item_tag[] = array(
                        'url' => translit($value),
                        'title' => $value,
                    );
                }
            }

            /* -=========================================
            ИЗБРАННОЕ
            */
            if(!empty($ses_note->id)){
                if (in_array($c_catalog_data['id'], $ses_note->id)){
                    $is_note = true;
                }else{
                    $is_note = false;
                }
            }else {
                $is_note = false;
            }

            $catalog_item_top[] = array(
                'id' 			=> $c_catalog_data['id'],
                'is_note' 		=> $is_note,
                'title' 		=> $c_catalog_data['title'],
                'brend_title' 	=> $c_catalog_data['brend_title'],
                'brend_url' 	=> "/catalog/{$c_catalog_data['brend_url']}/",
                'price_s' 		=> $c_catalog_data['price_s']+$_SESSION['_GMARGIN'],
                'pol' 			=> ($c_catalog_data['pol'] == 'M') ? 'мужская парфюмерия' : 'женская парфюмерия',
                'img' 			=> '/images/uploads/catalog/'.$c_catalog_data['id'].'/small/'.$c_catalog_data['img'],
                'url' 			=> '/catalog/'.$c_catalog_data['brend_url'].'/'.$c_catalog_data['url'].'/',
                'anot' 			=> maxsite_str_word($c_catalog_data['text'], 9) .' ...',
                'tag' 			=> $item_tag,
            );

            $item_tag = null;

        }
        $view->catalog_item_top = $catalog_item_top;


        $constructor['content'] = $view->render('users/users.php');
        break;
}