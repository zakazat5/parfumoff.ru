<?php 


/* -=========================================
RSS news
*/
if($canon_name2=='rss'){


	// Ссылка на ленту
	$urlPrefix = 'http://'.BASE_URL.'/news/';

	$feedArray = array(
	'title'       => "Новости парфюмерии - parfumoff.ru",
	'link'        => $urlPrefix,
	'description' => 'Интернет-магазин парфюмерии parfumoff.ru',
	'charset'     => 'UTF-8',
	'image'       => "http://parfumoff.ru/images/logo1.gif",
	'entries'     => array()
	);


	$select = "SELECT "
	."id, url, title, anot, text, img, registerDate "
	."FROM m_news "
	."WHERE is_block=0 and id_cat='$cat_id' "
	."ORDER BY registerDate desc "
	."LIMIT 10";

	$q_rss = $db->query($select);
	while ($c_rss = $q_rss->fetch()) {
		$enclosure = array();

		// Дату создания статьи необходимо привести к формату timestamp
		$date = new Zend_Date($c_rss['registerDate'], 'YYYY-MM-dd HH:mm:ss');
		$itemTimestamp = $date->getTimestamp();

		/*
		$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
		$image_type = finfo_file($finfo, "http://enigme.ru/images/uploads/news/{$c_rss['id']}/trumbs/{$c_rss['img']}");
		finfo_close($finfo);
		
		mime_content_type("http://enigme.ru/images/uploads/news/{$c_rss['id']}/trumbs/{$c_rss['img']}")
		*/


		$enclosure[] = array(
		'url' => "http://parfumoff.ru/images/uploads/news/{$c_rss['id']}/trumbs/{$c_rss['img']}",
		'length' => filesize("./images/uploads/news/{$c_rss['id']}/trumbs/{$c_rss['img']}"),
		'type' => "image/jpeg" 
		);


		$feedArray['entries'][] = array(
		'title'       => to_utf8(format_text_out($c_rss['title'])),
		'link'        => 'http://'.BASE_URL . '/' . $canon_name1 . '/' . $c_rss['url'] . '/',
		'description' => to_utf8(html_entity_decode($c_rss['anot'])),
		'yandex' 		=> to_utf8(html_entity_decode($c_rss['text']). "<br /><a href=\"http://parfumoff.ru/\">Parfumoff.ru - интернет-магазин парфюмерии.</a>"),
		'lastUpdate'  => $itemTimestamp,
		'enclosure'   => $enclosure,
		);

	}

	$feed = Zend_Feed::importArray($feedArray, 'rss');
	
	$feed->send();
	exit();
}

/* -=========================================
NEWS Full
*/
if ( $site_count_url == 2 AND $canon_name2 != 'page' ) {

	$select = "
	SELECT 
		a.*,
		b.title AS brend_title, b.url AS brend_url,
		c.title AS aromat_title, c.url AS aromat_url
	FROM 
		m_news AS a
	LEFT JOIN 
		m_catalog AS b ON b.id=a.id_brend
	LEFT JOIN 
		m_catalog_data AS c ON c.id=a.id_catalog_data
	WHERE 
		a.is_block=0 AND a.id_cat='$cat_id' AND a.url='$canon_name2'
	LIMIT 1
	";

	// QUERY
	$stmt = $db->query($select);
	while ($content = $stmt->fetch()) {
		$id_news = $content['id'];

		$constructor['meta_title'] = $content['title'] .' - Parfumoff.ru';
		$constructor['meta_description'] = $content['meta_description'];

		if($content['is_links']==1){
			$text = word_to_link( to_1251($content['text']), '\.', 'http://'.BASE_URL);
		}else {
			$text = to_1251($content['text']);
		}

		$news_item = array(
			'id' => $content['id'],
			'title' => $content['title'],
			'brend_title' => $content['brend_title'],
			'brend_url' => $content['brend_url'],
			'aromat_title' => $content['aromat_title'],
			'aromat_url' => $content['aromat_url'],

			'is_imgf' => $content['is_imgf'],
			'img' => $content['img'],
			'alt' => $content['alt'],
			'date' => rusdate($content['registerDate'],1),
			'text' => $text,
			'anot' => to_1251($content['anot']),
			'url' => $content['url']
		);
	}

	$q_foto = $db->query("SELECT img,alt FROM m_news_foto WHERE id_news='$id_news' ");
	while ($c_foto = $q_foto->fetch()) {
		$foto_item[] = array(
		'img' => $c_foto['img'],
		'alt' => $c_foto['alt'],
		);
	}

	$view->news_item 	= $news_item;
	$view->foto_item 	= $foto_item;
	$view->canon_name1 	= $canon_name1;
	$view->cat_name 	= $cat_name;

	$constructor['content'] = $view->render('content_news_full.php');

	/* -=========================================
	NEWS List
	*/
} elseif ( $site_count_url == 1 or $canon_name2=='page') {

    if ($canon_name4){
        err_301($canon_name1.'/'.$canon_name2.'/'.$canon_name3.'/');
    }

    if ($canon_name3 && !is_numeric($canon_name3)){
        err_301($canon_name1);
    }




	// PAGES
	$all_items 	= $db->fetchOne("SELECT count(*) FROM m_news WHERE is_block=0 and id_cat='$cat_id' ");
	$p 			= pages_content($all_items, 3, $canon_name3, 'pages.tpl');
	$start 		= $p['start']; $limit = 3; $pagis = $p['pages'];

	$select = "
	SELECT 
		a.id, a.url, a.title, a.anot, a.img, a.alt, a.registerDate,
		b.title AS brend_title, b.url AS brend_url,
		c.title AS aromat_title, c.url AS aromat_url
	FROM 
		m_news AS a
	LEFT JOIN 
		m_catalog AS b ON b.id=a.id_brend
	LEFT JOIN 
		m_catalog_data AS c ON c.id=a.id_catalog_data
	WHERE 
		a.is_block=0 and a.id_cat='$cat_id' 
	ORDER BY 
		a.id DESC 
	LIMIT 
		$start, $limit
	";

	$stmt = $db->query($select);
	while ($content = $stmt->fetch()) {
		$news_item[] = array(
		'id' 	=> $content['id'],
		'url' 	=> $content['url'],
		'title' => $content['title'],
		'brend_title' => $content['brend_title'],
		'brend_url' => $content['brend_url'],
		'aromat_title' => $content['aromat_title'],
		'aromat_url' => $content['aromat_url'],
		'img' => $content['img'],
		'alt' => ($content['alt']=='') ? $content['title'] : $content['alt'],
		'date' => rusdate(strtotime($content['registerDate']),9),
		'anot' 	=> word_to_link(to_1251($content['anot']), '\.', 'http://'.BASE_URL),
		);
	}

	$view->news_item 	= $news_item;
	$view->pages 		= $pagis;
	$view->canon_name1 	= $canon_name1;
	$view->cat_name 	= $cat_name;

	$constructor['content'] = $view->render('content_news.php');
}
?>