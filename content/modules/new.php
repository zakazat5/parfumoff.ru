<?php



//check canon names
if ($canon_name2 && $canon_name2!='page'){
    err_301($canon_name1);
}
if ($canon_name3 && !is_numeric($canon_name3)){
    err_301($canon_name1);
}

if ($canon_name4){
    err_301($canon_name1);
}



$all_items 	= $db->fetchOne("SELECT count(*) FROM m_catalog_data WHERE text!='' AND price_s!=0 AND id_cat=82 ");
$p 			= pages_content($all_items, 12, $canon_name3, 'pages.tpl');
$start 		= $p['start']; $limit = 12; $pagis = $p['pages'];

$select = "SELECT "
."a.id, a.title, a.url, a.price_s, a.img, a.text, "
."b.title AS brend_title, b.url AS brend_url "
."FROM m_catalog_data AS a "
."JOIN m_catalog AS b ON b.id=a.id_catalog "
."WHERE a.id_cat=82 AND a.text!='' AND a.price_s != 0 "
."LIMIT $start, $limit";

$q_catalog = $db->query($select);
while ($c_catalog = $q_catalog->fetch()) {
	
	if ($c_catalog['img']=='') {
		$img = '/images/noimage.jpg';
		$img_big = '/images/noimage.jpg';
	}else {
		$img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
		$img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
	}

	$catalog_item[] = array(
	'title' 		=> $c_catalog['title'],
	'brend_title' 	=> $c_catalog['brend_title'],
	'price_s' 		=> $c_catalog['price_s']+$_SESSION['_GMARGIN'],
	'img' 			=> $img,
	'img_big' 		=> $img_big,
	'url' 			=> '/catalog/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
	'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...'
	);
}
$view->catalog_item = $catalog_item;
$view->pages = $pagis;

$constructor['content'] = $view->render('content_new.php');
?>