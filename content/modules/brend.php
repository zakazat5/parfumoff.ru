<?php

//check canon names
if ($canon_name4 && ($canon_name4 != 'send' && $canon_name4 != 'order')) {
    err_404($canon_name1 . '/' . $canon_name2 . '/' . $canon_name3 . '/');
}

$sql_m_catalog_block = '';
$sql_m_catalog_data_a_block = '';
$sql_m_catalog_b_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$sql_m_catalog_block = ' AND id != 106 AND id != 83 AND id != 176 AND id != 460';
	$sql_m_catalog_b_block = ' AND b.id != 106 AND b.id != 83 AND b.id != 176 AND b.id != 460';
	$sql_m_catalog_data_a_block = ' AND a.id_catalog != 106 AND a.id_catalog != 83 AND a.id_catalog != 176 AND a.id_catalog != 460';
	$sql_m_catalog_data_block = ' AND id_catalog != 106 AND id_catalog != 83 AND id_catalog != 176 AND id_catalog != 460';
}

if ($canon_name2 == 'fix') {
    $brands = $db->fetchAll("SELECT * FROM m_catalog WHERE 1=1{$sql_m_catalog_block} ");
    print_r($brands);
    die();


    //iconv('cp1251','utf-8',word_to_link(to_1251($brend['anot']), '\.', 'http://'.BASE_URL));
}


if ($canon_name4 == 'send') {
    /* -=========================================
    добвление коммента
    */

    if ($ses_user->is_login != true) {
        go('/production/' . $canon_name2 . '/' . $canon_name3 . '/');
    }

    if (isset($_POST['com_submit'])) {

        $brend = $db->fetchRow("SELECT id,title,url,anot FROM m_catalog WHERE id_cat=82 AND url='$canon_name2'{$sql_m_catalog_block} ");
        $catalog_data = $db->fetchRow(
            "SELECT id,title,url FROM m_catalog_data WHERE url='$canon_name3' AND id_catalog='{$brend['id']}' "
        );

        if ($catalog_data != false) {
            $filter = new anti_mate();
            $com_name = substr(trim($_POST['com_name']), 0, 15);
            $com_name = $filter->filter(eregi_replace("[^ a-zа-яА-Я0-9]", '', $com_name));
            $com_text = HTMLToTxt($filter->filter(textwrap(format_text($_POST['com_comment']), 1000)));

            if ($ses_user->is_login) {
                $user_id = intval($ses_user->user['id']);
            } else {
                $user_id = 0;
            }

            if ($com_name != '' AND $com_text != '') {
                $m_com = array(
                    'id_catalog_data' => $catalog_data['id'],
                    'id_user' => $user_id,
                    'user_name' => $com_name,
                    'user_text' => $com_text,
                    'user_date' => new Zend_Db_Expr('NOW()'),
                    'catalog_title' => "{$brend['title']} {$catalog_data['title']}",
                    'catalog_url' => "http://" . $_SERVER['HTTP_HOST'] . "/production/{$brend['url']}/{$catalog_data['url']}/",
                );
                $db->insert('m_catalog_com', $m_com);
            }
            go('/production/' . $canon_name2 . '/' . $canon_name3 . '/');
        } else {
            go('/');
        }
    }

    //страница
} elseif (isset($canon_name3) AND isset($canon_name2)) {
    if ($canon_name3 !== mb_strtolower($canon_name3) || $canon_name2 !== mb_strtolower($canon_name2)) {
        $canon_name2 = mb_strtolower($canon_name2);
        $canon_name3 = mb_strtolower($canon_name3);
        err_301('production/' . $canon_name2 . '/' . $canon_name3 . '/');
    }

	//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
	if ($ses_user->is_login == false) {
		$brend = $db->fetchRow(
			"SELECT id, title, title_1,url,anot FROM m_catalog WHERE  url='$canon_name2' and is_block=0{$sql_m_catalog_block} "
		);
	} else {
		$brend = $db->fetchRow(
			"SELECT id, title, title_1,url,anot FROM m_catalog WHERE  url='$canon_name2' and is_block=0"
		);
	}
	
    if ($brend == false) {
        err_404();
    }
	
	if ($canon_name3 == 'woman' || $canon_name3 == 'men') {		
		//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
		/*
		if ($ses_user->is_login == false) {
			$brend = $db->fetchRow($db->select()->from('m_catalog')->where('url=?', [$canon_name2])->where('is_block=0')->where('id != 106')->where('id != 83')->where('id != 176')->where('id != 460'));
		} else {
			$brend = $db->fetchRow($db->select()->from('m_catalog')->where('url=?', [$canon_name2])->where('is_block=0'));
		}
		*/
		
        //echo $canon_name2;
		/*
        if ($brend == false) {
            err_404();
        }
		*/
		
        /**
         * Filter
         */
        if ($_GET['aroma_type'] || $_GET['s100'] || $_GET['pol']) {
            $constructor['noindex'] = true;
            foreach ($_GET['aroma_type'] as $key => $type) {
                if (!array_key_exists($type, $aroma_type)) {
                    unset($_GET['aroma_type'][$key]);
                } else {
                    $aroma_type_checked[$_GET['aroma_type'][$key]] = 1;
                }
            }
            //выбранные селекты
            $checked['aroma_type'] = $aroma_type_checked;

            $select = $db->select();
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type']
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');

            if (count($_GET['s100']) > 0) {
                foreach ($_GET['s100'] as $key => $item) {
                    $item = (int)$item;
                    if ($item == 0) {
                        $select->orWhere('m_catalog_data_order.s100 <1000');
                    } elseif ($item == 10) {
                        $select->orWhere('m_catalog_data_order.s100 >10000');
                    } else {
                        $from = $item * 1000 + 1;
                        $to = $item * 1000 + 1000;
                        $select->orWhere("m_catalog_data_order.s100>{$from} and m_catalog_data_order.s100<={$to}");

                    }
                    $checked['s100'][$_GET['s100'][$key]] = 1;
                }
            }

            if (count($_GET['pol']) > 0) {
                $aPol = ['M', 'F'];
                foreach ($_GET['pol'] as $key => $item) {
                    if (in_array($item, $aPol)) {
                        $select->Where('m_catalog_data.pol=?', $item);
                        $checked['pol'][$item] = 1;
                    } else {
                        err_404();
                    }
                }
            }

            $subquery = $select->getPart(Zend_Db_Select::WHERE);
            $select->reset(Zend_Db_Select::WHERE);
            if (count($_GET['aroma_type']) > 0) {
                $select->where('m_catalog_data_order.type  IN (?)', $_GET['aroma_type']);
            }
            $select->where('m_catalog_data_order.price>0');
            $select->where('m_catalog_data.id_catalog=?', $brend['id']);
            if ($subquery) {
                $select->where(implode(' ', $subquery));
            }
            $select->group('m_catalog_data.id');
        } else {
            $select = $db->select();
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type']
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
            /*if ($_GET['all']!=1){
                $select->where('m_catalog_data.price_s!=0');
            }*/
            $select->where('m_catalog_data.id_catalog=?', $brend['id']);
            $select->group('m_catalog_data.id');
        }

        /**
         * s100
         */
        $s100select = clone $select;
        $s100select->reset(Zend_Db_Select::GROUP);
        $s100select->reset(Zend_Db_Select::WHERE);
        $s100select->where('m_catalog_data.price_s!=0');
        $s100select->where('m_catalog_data_order.price>0');
        $s100select->where('m_catalog_data.id_catalog=?', $brend['id']);
        $s100 = $db->fetchAll($s100select);

        foreach ($s100 as $item) {
            switch (true) {
                case ($item['s100'] <= 1000):
                    $s100filter[0] = 'До 1000';
                    break;
                case ($item['s100'] > 1001 && $item['s100'] <= 2000):
                    $s100filter[1] = 'От 1001 до 2000';
                    break;
                case ($item['s100'] > 2001 && $item['s100'] <= 3000):
                    $s100filter[2] = 'От 2001 до 3000';
                    break;
                case ($item['s100'] > 3001 && $item['s100'] <= 4000):
                    $s100filter[3] = 'От 3001 до 4000';
                    break;
                case ($item['s100'] > 4001 && $item['s100'] <= 5000):
                    $s100filter[4] = 'От 4001 до 5000';
                    break;
                case ($item['s100'] > 5001 && $item['s100'] <= 6000):
                    $s100filter[5] = 'От 5001 до 6000';
                    break;
                case ($item['s100'] > 6001 && $item['s100'] <= 7000):
                    $s100filter[6] = 'От 6001 до 7000';
                    break;
                case ($item['s100'] > 7001 && $item['s100'] <= 8000):
                    $s100filter[7] = 'От 7001 до 8000';
                    break;
                case ($item['s100'] > 8001 && $item['s100'] <= 9000):
                    $s100filter[8] = 'От 8001 до 9000';
                    break;
                case ($item['s100'] > 9001 && $item['s100'] <= 10000):
                    $s100filter[9] = 'От 9001 до 10000';
                    break;
                case ($item['s100'] > 10000):
                    $s100filter[10] = 'Больше 10000';
                    break;
            }
        }
        ksort($s100filter);
        $view->s100filter = $s100filter;

        $aromaTypeSelect = clone $select;
        $aromaTypeSelect->reset(Zend_Db_Select::GROUP);
        $aromaTypeSelect->reset(Zend_Db_Select::WHERE);
        $aromaTypeSelect->where('m_catalog_data_order.price!=0');
        $aromaTypeSelect->where('m_catalog_data.id_catalog=?', $brend['id']);
        $aromaTypeSelect->group('m_catalog_data_order.type');
        $aromaTypeFetch = $db->fetchAll($aromaTypeSelect);


        foreach ($aromaTypeFetch as $key => $item) {

            $aromaTypeFilter[$item['type']] = $aroma_type[$item['type']];
        }
        unset($aromaTypeFilter[0]);
        $view->aromaTypeFilter = $aromaTypeFilter;


        if ($_GET['sort'] == 'abc' || $_GET['sort'] == 'popular') {
            switch ($_GET['sort']) {
                case 'abc':
                    $select->order('title ASC');
                    break;
                case 'popular':
                    $select->order('id  ASC');
                    break;
            }
        } else {
            $select->order('title ASC');
        }


        $q_catalog_data = $db->query($select);
        while ($c_catalog_data = $q_catalog_data->fetch()) {

            if ($c_catalog_data['img'] == '') {
                $img = '/images/noimage.jpg';
                $img_big = '/images/noimage.jpg';
            } else {
                $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/small/' . $c_catalog_data['img'];
                $img_big = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
            }

            if (!$og_image) {
                if ($c_catalog_data['img'] != '') {
                    $og_image = $img;
                }
            }

            if ($c_catalog_data['pol'] == 'F' && $canon_name3 == 'woman') {
                $catalog_item_f[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            } elseif ($c_catalog_data['pol'] == 'M' && $canon_name3 == 'men') {
                $catalog_item_m[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            }
        }

		/*
        $GLOBALS['dataLayer']['PageType'] = 'ListingPage';
		*/
        if (is_array($catalog_item_f)) {
            $view->catalog_item_f = $catalog_item_f;
			/*
            $where = [];
            foreach ($catalog_item_f as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
			*/
        }
        if (is_array($catalog_item_m)) {
            $view->catalog_item_m = $catalog_item_m;
			/*
            $where = [];
            foreach ($catalog_item_m as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
			*/
        }
		
        $show_outstock = true;
        foreach ($catalog_item_f as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }
        foreach ($catalog_item_m as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }

        $view->checked = $checked;
        $view->show_outstock = $show_outstock;

        $view->brend_title = $brend['title'];
        $view->brend = $brend;
        $view->brend_anot = $brend['anot'];
        $view->brend_url = $canon_name2;

        /* -=========================================
        META
        */
		
		/*
        $meta_title = $brend['meta_title'];
        $meta_title_1 = $brend['meta_title_1'];

        $view->meta_title = $meta_title;
        $view->meta_title_1 = $meta_title;

        if ($brend['title_1'] == '') {
            $m_brend_title_1 = $brend['title'];
        } else {
            $m_brend_title_1 = $brend['title_1'];
        }
		*/
		
        //$constructor['meta_title'] = "{$brend['title']} духи женские и мужские - туалетная вода по низкой цене, купить парфюм {$m_brend_title_1} в интернет-магазине Parfumoff.ru";
		
        /*$constructor['meta_description'] = "Неповторимый аромат духов {$brend['title']}, купите духи {$m_brend_title_1} по лучшей цене со скидкой! Лучшие цены на любые ароматы и парфюм от {$brend['title']} ({$m_brend_title_1}) в интернет-магазине парфюмерии Энигма.";*/
       // $constructor['meta_description'] = "Женские и мужские духи, туалетная вода {$brend['title']}, купить парфюм {$m_brend_title_1} по низкой цене с доставкой по {городу}, рейтинг и отзывы покупателей.";
        //$constructor['meta_description'] = $brend['meta_description'];
        //$view->body_text = "Выберите понравившийся вам аромат бренда {$brend['title']}: духи, мужская или женская туалетная вода, парфюмированная вода или одеколон, а может быть дезодорант {$m_brend_title_1} - лучшие предложения в нашем магазине парфюмерии.";
		//$view->body_text = $brend['text'];
		$view->seo_description = "";
		if ($canon_name3 == 'woman') {
			$constructor['meta_title'] = "Женские духи {$brend['title']}, купить туалетную воду и парфюм {$brend['title_1']}, цена в интернет-магазине Parfumoff.ru";
			$constructor['meta_description'] = "Женские духи {$brend['title_1']} в интернет-магазине оригинальной парфюмерии Parfumoff. Купить парфюм и туалетную воду {$brend['title']} по низкой цене с доставкой.";
			$constructor['meta_keywords'] = "Женские духи {$brend['title']}, {$brend['title_1']} для женщин, туалетная вода, парфюм, парфюмерия";
			/*
			$view->seo_description = "<p>Интернет-магазин Parfumoff предлагает Вам выбрать оригинальный аромат из коллекции знаменитого женского парфюма от {$brend['title']}.</p><br>";
			$view->seo_description.= "<p>Купить женскую туалетную воду или духи {$brend['title_1']}, значит, стать обладателем качественного парфюма, который будет подчеркивать Ваш высокий статус в обществе.</p><br>";
			$view->seo_description.= "<p>В нашем каталоге представлена только оригинальная парфюмерия {$brend['title']} для женщин с подробным описанием и отзывами покупателей.</p><br>";
			$view->seo_description.= "<p>Покупая парфюм в нашем магазине через интернет, вы можете быть уверены не только в качестве товара, которое подтверждается многочисленными отзывами наших клиентов, но и в том, что цена на женские духи {$brend['title']}, значительно ниже, чем стоимость аналогичного товара в обычном магазине.</p>";
			*/
			$constructor['og:title'] = $constructor['meta_title'];
			$constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/big/', '/small/', $og_image);
			$constructor['og:description'] = $constructor['meta_description'];
			$constructor['og:url'] = $_SERVER['REQUEST_URI'];
			$checked['pol']['F'] = 1;
			$view->checked = $checked;
			$constructor['content'] = $view->render('content_catalog_woman.php');
		}
		
		if ($canon_name3 == 'men') {
			$constructor['meta_title'] = "Мужские духи {$brend['title']}, купить туалетную воду и парфюм {$brend['title_1']}, цена в интернет-магазине Parfumoff.ru";
			$constructor['meta_description'] = "Мужские духи {$brend['title_1']} в интернет-магазине оригинальной парфюмерии Parfumoff. Купить парфюм и туалетную воду {$brend['title']} по низкой цене с доставкой.";
			$constructor['meta_keywords'] = "Мужские духи {$brend['title']}, {$brend['title_1']} для мужчин, туалетная вода, парфюм, парфюмерия";
			/*
			$view->seo_description = "<p>Интернет-магазин Parfumoff предлагает Вам выбрать оригинальный аромат из коллекции знаменитого мужского парфюма от {$brend['title']}.</p><br>";
			$view->seo_description.= "<p>Купить мужскую туалетную воду или духи {$brend['title_1']}, значит, стать обладателем качественного парфюма, который будет подчеркивать Ваш высокий статус в обществе.</p><br>";
			$view->seo_description.= "<p>В нашем каталоге представлена только оригинальная парфюмерия {$brend['title']} для мужчин с подробным описанием и отзывами покупателей.</p><br>";
			$view->seo_description.= "<p>Покупая парфюм в нашем магазине через интернет, вы можете быть уверены не только в качестве товара, которое подтверждается многочисленными отзывами наших клиентов, но и в том, что цена на мужские духи {$brend['title']}, значительно ниже, чем стоимость аналогичного товара в обычном магазине.</p>";
			*/
			$constructor['og:title'] = $constructor['meta_title'];
			$constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/big/', '/small/', $og_image);
			$constructor['og:description'] = $constructor['meta_description'];
			$constructor['og:url'] = $_SERVER['REQUEST_URI'];
			$checked['pol']['M'] = 1;
			$view->checked = $checked;
			$constructor['content'] = $view->render('content_catalog_men.php');
		}	
	} else {
    //сообщить о поступлении
    if ($_POST && $_POST['waitlist']) {
        $waitlist = [
            'name' => ($_POST['name']) ? $_POST['name'] : '',
            'articul' => ($_POST['name']) ? $_POST['articul'] : '',
            'phone' => ($_POST['name']) ? $_POST['phone'] : '',
            'email' => ($_POST['name']) ? $_POST['email'] : ''
        ];

        $db->insert('m_mag_Waitlist', $waitlist);
    }

    $select = "SELECT "
        . " * "
        . "FROM m_catalog_data "
        . "WHERE url='$canon_name3' and id_catalog='{$brend['id']}' "
        . "LIMIT 1";


    $q_catalog_data = $db->query($select);


    //
    $c_catalog_data = $db->fetchRow($db->select()->from('m_catalog_data')->where('url=?', $canon_name3)->where('id_catalog=?', $brend['id']));

    $id_catalog_data = $c_catalog_data['id'];
    $tag = $c_catalog_data['tag'];
    $price = $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'];

    if ($c_catalog_data['img'] == '') {
        $img = '/images/noimage.jpg';
    } else {
        $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/small/' . $c_catalog_data['img'];
    }

    if ($c_catalog_data['pol'] == 'M') {
        $pol = 'Мужской';
    } else {
        $pol = 'Женский';
    }

    if ($c_catalog_data['pol'] == 'M') {
        $pol2 = 'мужские';
    } else {
        $pol2 = 'женские';
    }
	
	if ($c_catalog_data['pol'] == 'M') {
        $pol3 = 'мужчин';
    } else {
        $pol3 = 'женщин';
    }
	
    $catalog_full = array(
        'id' => $c_catalog_data['id'],
        'title' => $c_catalog_data['title'],
        'is_spec' => $c_catalog_data['is_spec'],
        'title_1' => $c_catalog_data['title_1'],
        'meta_title' => $c_catalog_data['meta_title'],
        'url' => $c_catalog_data['url'],
		'id_catalog'	=> $c_catalog_data['id'],
        'articul' => $c_catalog_data['articul'],
        'rate' => $c_catalog_data['rate'],
        'price_s' => $c_catalog_data['price_s'],
        'img' => $img,
        'alt' => $c_catalog_data['alt'],
        'text' => word_to_link($c_catalog_data['text'], '\.', 'http://' . BASE_URL),
        'text2' => $c_catalog_data['text2'],
        'pol' => $pol,
        'pol2' => $pol2,
		'pol3' => $pol3,
        'video_key' => $c_catalog_data['video_key']
    );
    
    if (!is_array($catalog_full)) {
        err_404();
    }


    //gallery
    $aPhotos = $db->fetchAll($db->select()->from('m_catalog_gallery')->where('id_catalog_data=?', $catalog_full['id']));


    $view->catalog_full = $catalog_full;
    $view->aPhotos = $aPhotos;
    $view->brend_title = $brend['title'];
    $view->brend = $brend;
    $view->brend_url = $canon_name2;


    /* -=========================================
    ИЗБРАННОЕ
    */
    if (!empty($ses_note->id)) {
        if (in_array($catalog_full['id'], $ses_note->id)) {
            $view->is_note = true;
        } else {
            $view->is_note = false;
        }
    } else {
        $view->is_note = false;
    }

    /* -=========================================
	список модификаций
	*/
    $select = "SELECT id, v, type, price, price_old, is_block,price_usd1,price_usd13,articul "
        . "FROM m_catalog_data_order "
        . "WHERE id_catalog_data='$id_catalog_data' "
        . "ORDER BY type,price";

    if ($canon_name4 == 'order') {
        $articul = $canon_name5;
        $view->is_order = $articul;
        $constructor['noindex'] = true;
        //$select = $db->select()->from(['m_catalog_data_order'])->where('articul = ?', $articul)->where('price!=0');
    }
	$res_category = mysql_query("SELECT * FROM m_category_product WHERE (id_category=14 AND id_product={$id_catalog_data})");
	if ($result_cat = mysql_fetch_assoc($res_category)) {
			$category_sale = 1;
	}
	else {
			$category_sale = 0;
	}
	$view->category_sale = $category_sale;
    $num = 0;
    $q_catalog_order = $db->fetchAll($select);
    //print_r($q_catalog_order);
    foreach ($q_catalog_order as $c_catalog_order) {
        if ($c_catalog_order['type'] == 'N') {
            $type = $c_catalog_order['v'];
            $volume = 'набор';
        } else {
            $type = $aroma_type[$c_catalog_order['type']];
            $volume = $c_catalog_order['v'] . ' мл';
        }

        if ($c_catalog_order['price_usd1'] > 0 or $c_catalog_order['price_usd13'] > 0) {
            $is_spec = 1;
        } else {
            $is_spec = 0;
        }
        $catalog_item_order[] = array(
            'title' => $title,
            'is_spec' => $is_spec,
            'type' => $type,
            'volume' => $volume,
            't' => $c_catalog_order['type'],
            'id' => $c_catalog_order['id'],
			'id_catalog'	=> $id_catalog_data,
            'is_block' => $c_catalog_order['is_block'],
            'price' => $c_catalog_order['price'] + $_SESSION['_GMARGIN'],
            'price_old' => $c_catalog_order['price_old'],
            'bg' => ($num % 2 == 0) ? '1' : '2',
            'articul' => $c_catalog_order['articul']
        );

        $GLOBALS['dataLayer']['PageType'] = 'ProductPage';
        $GLOBALS['dataLayer']['ProductID'] = $c_catalog_order['id'];

        $num++;
    }
    if (is_array($catalog_item_order)) {
        $view->catalog_item_order = $catalog_item_order;
    }


    /* -=========================================
    TAG
    */
    if ($tag != '') {
        $tag = explode(',', $tag);
        foreach ($tag as $key => $value) {
            if ($value == '') {
                continue;
            }
            $item_tag[] = array(
                'url' => translit($value),
                'title' => $value,
            );
        }
        $view->catalog_item_tag = $item_tag;
    }


    /* -=========================================
    список комментариев
    */

    $stmt = $db->query('SELECT * FROM m_catalog_com WHERE id_catalog_data = ? and id>74182 order by user_date DESC', array($id_catalog_data));
    $catalog_item_com = $stmt->fetchAll();


    $view->catalog_item_com = $catalog_item_com;

    /* -=========================================
    ароматы по схожей цене
    */
    if ($price != 0) {
        $price_start = $price - 500;
        $price_end = $price + 500;

        $select = "SELECT "
            . "a.id, a.title, a.title_1, a.url, a.price_s,a.price_e, a.img,a.is_spec,a.is_top, "
            . "b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
            . "FROM m_catalog_data AS a "
            . "JOIN m_catalog AS b ON b.id=a.id_catalog "
            . "WHERE a.id_cat=82 AND a.id!='$id_catalog_data' AND a.price_s > '$price_start' AND a.price_e < '$price_end'{$sql_m_catalog_data_a_block} "
            . "ORDER BY price_s "
            . "LIMIT 30";
    } else {
        $select = "SELECT "
            . "a.id, a.title, a.title_1, a.url, a.price_s,a.price_e, a.img,is_spec,a.is_top, "
            . "b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
            . "FROM m_catalog_data AS a "
            . "JOIN m_catalog AS b ON b.id=a.id_catalog "
            . "WHERE a.id_cat=82 AND a.id!='$id_catalog_data' AND a.price_s != 0  AND a.price_e < 2000{$sql_m_catalog_data_a_block} "
            . "ORDER BY price_s "
            . "LIMIT 30";
    }
    $q_catalog_pre = $db->query($select);
    while ($c_catalog_pre = $q_catalog_pre->fetch()) {
        if ($c_catalog_pre['img'] == '') {
            $img = '/images/noimage.jpg';
        } else {
            $img = '/images/uploads/catalog/' . $c_catalog_pre['id'] . '/small/' . $c_catalog_pre['img'];
        }

        $catalog_item_pre[] = array(
            'title' => ($c_catalog_pre['title_1'] != '') ? $c_catalog_pre['title_1'] : $c_catalog_pre['title'],
            'brend_title' => ($c_catalog_pre['brend_title_1'] != '') ? $c_catalog_pre['brend_title_1'] : $c_catalog_pre['brend_title'],
            'price_s' => $c_catalog_pre['price_s'] + $_SESSION['_GMARGIN'],
            'price_e' => $c_catalog_pre['price_e'] + $_SESSION['_GMARGIN'],
            'url' => '/production/' . $c_catalog_pre['brend_url'] . '/' . $c_catalog_pre['url'] . '/',
			'id_catalog'	=> $c_catalog_pre['id'],
            'img' => $img,
            'is_spec' => $c_catalog_pre['is_spec'],

        );
    }

    shuffle($catalog_item_pre);
    $catalog_item_pre = array_slice($catalog_item_pre, 0, 8);


    $view->catalog_item_pre = $catalog_item_pre;


    /* -=========================================
    ароматы бренда
    */
    $select = "SELECT "
        . "a.id, a.title, a.title_1, a.url, a.price_s, a.img,a.is_spec, "
        . "b.title AS brend_title, b.title_1 AS brend_title_1, b.url AS brend_url "
        . "FROM m_catalog_data AS a "
        . "JOIN m_catalog AS b ON b.id=a.id_catalog "
        . "WHERE a.id_catalog='{$brend['id']}' AND a.id!='$id_catalog_data' AND a.price_s!=0 "
        . "ORDER BY price_s "
        . "LIMIT 6";
    $q_catalog_br = $db->query($select);
    while ($c_catalog_br = $q_catalog_br->fetch()) {
        if ($c_catalog_br['img'] == '') {
            $img = '/images/noimage.jpg';
        } else {
            $img = '/images/uploads/catalog/' . $c_catalog_br['id'] . '/small/' . $c_catalog_br['img'];
        }

        $catalog_item_br[] = array(
            'title' => ($c_catalog_br['title_1'] != '') ? $c_catalog_br['title_1'] : $c_catalog_br['title'],
            'brend_title' => ($c_catalog_br['brend_title'] != '') ? $c_catalog_br['brend_title_1'] : $c_catalog_br['brend_title'],
            'price' => $c_catalog_br['price_s'] + $_SESSION['_GMARGIN'],
            'url' => '/production/' . $c_catalog_br['brend_url'] . '/' . $c_catalog_br['url'] . '/',
			'id_catalog'	=> $c_catalog_br['id'],
            'img' => $img,
            'is_spec' => $c_catalog_br['is_spec'],
        );
    }
    $view->catalog_item_br = $catalog_item_br;


    /* -=========================================
    Ароматы бренда справа
    */
    $select = "SELECT "
        . "title, url, pol,is_spec "
        . "FROM m_catalog_data "
        . "WHERE id_catalog='{$brend['id']}' AND price_s!=0 ORDER BY title ";

    $q_catalog_data = $db->query($select);
    while ($c_catalog_data = $q_catalog_data->fetch()) {
        if ($c_catalog_data['pol'] == 'M') {
            $catalog_item_m[] = array(
                'title' => $c_catalog_data['title'],
                'url' => $c_catalog_data['url'],
                'pol' => $c_catalog_data['pol'],
                'is_spec' => $c_catalog_data['is_spec'],
            );
        } else {
            $catalog_item_f[] = array(
                'title' => $c_catalog_data['title'],
                'url' => $c_catalog_data['url'],
                'pol' => $c_catalog_data['pol'],
                'is_spec' => $c_catalog_data['is_spec'],
            );
        }

    }
    $view->catalog_item_m = $catalog_item_m;
    $view->catalog_item_f = $catalog_item_f;
    $view->catalog_count_f = $db->fetchOne(
        "SELECT count(*) FROM m_catalog_data WHERE pol='F' AND id_catalog='{$brend['id']}' AND price_s!=0 "
    );
    $view->catalog_count_m = $db->fetchOne(
        "SELECT count(*) FROM m_catalog_data WHERE pol='M' AND id_catalog='{$brend['id']}' AND price_s!=0 "
    );


    /* -=========================================
    META
    */
    if ($brend['title_1'] == '') {
        $m_brend_title_1 = $brend['title'];
    } else {
        $m_brend_title_1 = $brend['title_1'];
    }
	/*
    if ($catalog_full['title_1'] == '') {
        		if ($c_catalog_data['pol'] == 'M') {
			$constructor['meta_title'] = "Мужские духи {$brend['title']} {$catalog_full['title']}, купить парфюм и туалетную воду {$m_brend_title_1} {$catalog_full['title']}, цена в интернет-магазине Parfumoff.ru";
		} else {
			$constructor['meta_title'] = "Женские духи {$brend['title']} {$catalog_full['title']}, купить парфюм и туалетную воду {$m_brend_title_1} {$catalog_full['title']}, цена в интернет-магазине Parfumoff.ru";
		}
        
        
        $constructor['meta_description'] = mb_ucfirst($catalog_full['pol2']) . " духи, туалетная вода {$brend['title']} {$catalog_full['title']} по цене от {$catalog_full['price_s']} рублей, купите с доставкой по {городу}, почитайте отзывы к аромату или сравните цены на разные объемы.";
        $view->body_text = "Интернет-магазин парфюмерии Parfumoff.ru предлагает вам " . mb_strtolower($catalog_full['pol'], 'utf-8') . " аромат: {$brend['title']} {$catalog_full['title']}. Купите духи или туалетную воду (тестер) с доставкой по {городу}, изучите описание и мнение покупателей. Если вы оставите отзыв или поставите рейтинг аромату {$brend['title_1']} {$catalog_full['title']} мы будем очень вам благодарны.";
    } else {
        $constructor['meta_title'] = "{$brend['title']} {$catalog_full['title']}, купить духи {$m_brend_title_1} {$catalog_full['title_1']} по отличной цене, {$catalog_full['pol']} аромат, парфюм, туалетная вода";
        $constructor['meta_description'] = mb_ucfirst($catalog_full['pol2']) . " духи, туалетная вода {$brend['title']} {$catalog_full['title_1']} по ценам от {$catalog_full['price_s']} рублей, купите с доставкой по {городу}, почитайте отзывы к аромату или сравните цены.";
        $view->body_text = "Интернет-магазин парфюмерии Parfumoff.ru предлагает вам " . mb_strtolower($catalog_full['pol'], 'utf-8') . " аромат: {$brend['title']} {$catalog_full['title']}. Купите духи с доставкой по {городу}, изучите описание и мнение покупателей. Если вы оставите отзыв или поставите рейтинг аромату {$brend['title_1']} {$catalog_full['title_1']} мы будем очень вам благодарны.";
    }
	*/
	$constructor['meta_title'] = mb_ucfirst($catalog_full['pol2']) . " духи {$brend['title']} {$catalog_full['title']}, купить парфюм и туалетную воду {$m_brend_title_1} {$catalog_full['title']}, цена в интернет-магазине Parfumoff.ru";
	$constructor['meta_description'] = mb_ucfirst($catalog_full['pol2']) . " духи {$m_brend_title_1} {$catalog_full['title']} в интернет-магазине оригинальной парфюмерии Parfumoff. Купить парфюм и туалетную воду {$brend['title']} {$catalog_full['title']} по низкой цене с доставкой.";
	$constructor['meta_keywords'] = mb_ucfirst($catalog_full['pol2']) . " духи {$brend['title']} {$catalog_full['title']}, {$m_brend_title_1} для {$catalog_full['pol3']}, туалетная вода, парфюм, парфюмерия";
	
    $constructor['og:title'] = $constructor['meta_title'];
    $constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/big/', '/small/', $catalog_full['img']);
    $constructor['og:description'] = $constructor['meta_description'];
    $constructor['og:url'] = $_SERVER['REQUEST_URI'];


    //отзывы
    $shop_comments[] = array(
        'name' => 'Алексей',
        'date' => '10 Декабря',
        'text' => 'Заказал аромат детства - Ван Мен Шоу от богарта. Я был маленький, а все дяди вокруг носили этот аромат. Гуд! Доставка в Липецк заняла 4 дня. Я рад!'
    );
    $shop_comments[] = array(
        'name' => 'Агеев А.',
        'date' => ' 8 Октября',
        'text' => 'Я знаю этот магазинчик «Энигме», но я там сам ни разу не был, а я просто воспользовался услугами один раз интернета и заказал себе и для жены духи, я точно знал какие, название и милимитраж, просто так удобнее всего, ведь на сайте конечно есть подробная информация о каждом товаре и даже картинки, но всё же лучше знать уже продукцию в лицо, так как тебе привезут и чтобы точно знать, что это именно то, что ты заказывал или тем, что ты раньше пользовался. Я вообще заплатил за духи женские 800 руб. в магазине точно такие же стоят в два раза дороже! А мне ещё их доставили бесплатно в тот же день и очень вежливые люди, так что я остался всем доволен, особенно жена.'
    );
    $shop_comments[] = array(
        'name' => 'Елена',
        'date' => '05 августа',
        'text' => 'Случайно нашли с подругой сайт этого магазина.Заказывали парфюм уже три раза у них.Каждый раз все присылали в течении 10-12 дней.Лучше магазина по обслуживанию и цене я в интернете не нашла.Парфюм не поддельный,мне есть с чем сравнить,т.к. я сначала выбираю себе духи в фирменном магазине (рив гош, летуаль) у себя в городе,а потом заказываю в "энигме" и точно знаю как они выглядят и пахнут !'
    );
    $shop_comments[] = array(
        'name' => 'SOSO',
        'date' => '27 июля',
        'text' => 'Заказывала недавно Chanel Chance Eau Fraiche качество отличное и с доставкой все в порядке. Честно очень обрадовалась, когда наткнулась на этот интернет-магазин! '
    );
    $shop_comments[] = array(
        'name' => 'nensy666',
        'date' => '20 июля',
        'text' => 'Хороший магазин: на сайте можно найти весь *ходовой* товар, полное описание, и - главное - невысокие цены'
    );
    $shop_comments[] = array(
        'name' => 'Сергей',
        'date' => '05 мая',
        'text' => 'Этот магазин стал одним из самых любимых. А весь секрет в том, что на главной странице можно сразу выбрать ценовую категорию аромата. Это весьма удобно, когда располагаешь строго ограниченными финансами + не знаешь, что бы такого купить :)'
    );

    shuffle($shop_comments);


    $view->shop_comments = $shop_comments;

    //доставка
    $view->dostavka = $db->fetchRow("SELECT text_2,text_3 FROM m_text WHERE id=2 ");


    /* -=========================================
    рендерим
    */

    if ($db->fetchRow("SELECT * FROM m_catalog_rate WHERE target_id=:target_id and ip=:ip", ['target_id' => $catalog_full['articul'], 'ip' => $_SERVER['REMOTE_ADDR']])) {
        $view->rated = true;
    }

    //текст остерегайтесь подделок
    $view->fake_text = $db->fetchOne($db->select()->from('m_text', 'text')->where('id_cat=120'));
    $view->delivery_tab = $db->fetchOne($db->select()->from('m_text', 'text')->where('id_cat=121'));
    $view->payment_tab = $db->fetchOne($db->select()->from('m_text', 'text')->where('id_cat=122'));
    $view->discount_tab = $db->fetchOne($db->select()->from('m_text', 'text')->where('id_cat=123'));

    if (isset($_GET['fast_view'])) {
        $tmpl = '';
        echo $view->render('content_catalog_fastview.php');
    } elseif ($_GET['new'] == 2) {
        $constructor['is_right'] = 1;
        $constructor['canonical'] = '/production/' . $view->brend_url . '/' . $view->catalog_full['url'] . '/';
        $constructor['content'] = $view->render('content_catalog_full_new_2.php');
    } else {
        $constructor['is_right'] = 1;
        $constructor['canonical'] = '/production/' . $view->brend_url . '/' . $view->catalog_full['url'] . '/';

        if ($view->catalog_full['text'] == '') {
            $constructor['content'] = $view->render('content_catalog_full_new_3.php');
        } else {
            $constructor['content'] = $view->render('content_catalog_full_new.php');
        }


    }

	
	
	}
	
	

//preg_match('/$\?utm.*/')

} elseif (isset($canon_name2)) {

    if (strlen($canon_name2) == 1) {
        $liter = strtolower($canon_name2{0});
        $liter = eregi_replace('[^a-z]', '', $liter);
        if ($liter == '') {
            err_404();
        }


        $view->liter = $liter;
        $constructor['meta_title'] = 'Торговые дома - Parfumoff.ru';

        $brend_item = [];
        foreach ($view->brend_item as $key => $item) {
            if ($liter == $item['liter']) {
                $brend_item[] = $item;
            }
        }
        $view->brend_item = $brend_item;


        $constructor['content'] = $view->render('content_liter_item.php');

    } elseif (preg_match('/^utm.*/', $canon_name2)) {
        $constructor['meta_title'] = 'Торговые дома - Parfumoff.ru';
        $constructor['content'] = $view->render('content_brend_all.php');
    } else {
		//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
		if ($ses_user->is_login == false) {
			$brend = $db->fetchRow($db->select()->from('m_catalog')->where('url=?', [$canon_name2])->where('is_block=0')->where('id != 106')->where('id != 83')->where('id != 176')->where('id != 460'));
		} else {
			$brend = $db->fetchRow($db->select()->from('m_catalog')->where('url=?', [$canon_name2])->where('is_block=0'));
		}

        //echo $canon_name2;
        if ($brend == false) {
            err_404();
        }

        /**
         * Filter
         */
        if ($_GET['aroma_type'] || $_GET['s100'] || $_GET['pol']) {
            $constructor['noindex'] = true;
            foreach ($_GET['aroma_type'] as $key => $type) {
                if (!array_key_exists($type, $aroma_type)) {
                    unset($_GET['aroma_type'][$key]);
                } else {
                    $aroma_type_checked[$_GET['aroma_type'][$key]] = 1;
                }
            }
            //выбранные селекты
            $checked['aroma_type'] = $aroma_type_checked;

            $select = $db->select();
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type']
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');

            if (count($_GET['s100']) > 0) {
                foreach ($_GET['s100'] as $key => $item) {
                    $item = (int)$item;
                    if ($item == 0) {
                        $select->orWhere('m_catalog_data_order.s100 <1000');
                    } elseif ($item == 10) {
                        $select->orWhere('m_catalog_data_order.s100 >10000');
                    } else {
                        $from = $item * 1000 + 1;
                        $to = $item * 1000 + 1000;
                        $select->orWhere("m_catalog_data_order.s100>{$from} and m_catalog_data_order.s100<={$to}");

                    }
                    $checked['s100'][$_GET['s100'][$key]] = 1;
                }
            }

            if (count($_GET['pol']) > 0) {
                $aPol = ['M', 'F'];
                foreach ($_GET['pol'] as $key => $item) {
                    if (in_array($item, $aPol)) {
                        $select->Where('m_catalog_data.pol=?', $item);
                        $checked['pol'][$item] = 1;
                    } else {
                        err_404();
                    }
                }
            }

            $subquery = $select->getPart(Zend_Db_Select::WHERE);
            $select->reset(Zend_Db_Select::WHERE);
            if (count($_GET['aroma_type']) > 0) {
                $select->where('m_catalog_data_order.type  IN (?)', $_GET['aroma_type']);
            }
            $select->where('m_catalog_data_order.price>0');
            $select->where('m_catalog_data.id_catalog=?', $brend['id']);
            if ($subquery) {
                $select->where(implode(' ', $subquery));
            }
            $select->group('m_catalog_data.id');
        } else {
            $select = $db->select();
            $select->from(
                'm_catalog_data_order',
                ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type']
            );
            $select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
            /*if ($_GET['all']!=1){
                $select->where('m_catalog_data.price_s!=0');
            }*/
            $select->where('m_catalog_data.id_catalog=?', $brend['id']);
            $select->group('m_catalog_data.id');
        }


        /**
         * s100
         */
        $s100select = clone $select;
        $s100select->reset(Zend_Db_Select::GROUP);
        $s100select->reset(Zend_Db_Select::WHERE);
        $s100select->where('m_catalog_data.price_s!=0');
        $s100select->where('m_catalog_data_order.price>0');
        $s100select->where('m_catalog_data.id_catalog=?', $brend['id']);
        $s100 = $db->fetchAll($s100select);


        foreach ($s100 as $item) {
            switch (true) {
                case ($item['s100'] <= 1000):
                    $s100filter[0] = 'До 1000';
                    break;
                case ($item['s100'] > 1001 && $item['s100'] <= 2000):
                    $s100filter[1] = 'От 1001 до 2000';
                    break;
                case ($item['s100'] > 2001 && $item['s100'] <= 3000):
                    $s100filter[2] = 'От 2001 до 3000';
                    break;
                case ($item['s100'] > 3001 && $item['s100'] <= 4000):
                    $s100filter[3] = 'От 3001 до 4000';
                    break;
                case ($item['s100'] > 4001 && $item['s100'] <= 5000):
                    $s100filter[4] = 'От 4001 до 5000';
                    break;
                case ($item['s100'] > 5001 && $item['s100'] <= 6000):
                    $s100filter[5] = 'От 5001 до 6000';
                    break;
                case ($item['s100'] > 6001 && $item['s100'] <= 7000):
                    $s100filter[6] = 'От 6001 до 7000';
                    break;
                case ($item['s100'] > 7001 && $item['s100'] <= 8000):
                    $s100filter[7] = 'От 7001 до 8000';
                    break;
                case ($item['s100'] > 8001 && $item['s100'] <= 9000):
                    $s100filter[8] = 'От 8001 до 9000';
                    break;
                case ($item['s100'] > 9001 && $item['s100'] <= 10000):
                    $s100filter[9] = 'От 9001 до 10000';
                    break;
                case ($item['s100'] > 10000):
                    $s100filter[10] = 'Больше 10000';
                    break;
            }
        }
        ksort($s100filter);
        $view->s100filter = $s100filter;

        $aromaTypeSelect = clone $select;
        $aromaTypeSelect->reset(Zend_Db_Select::GROUP);
        $aromaTypeSelect->reset(Zend_Db_Select::WHERE);
        $aromaTypeSelect->where('m_catalog_data_order.price!=0');
        $aromaTypeSelect->where('m_catalog_data.id_catalog=?', $brend['id']);
        $aromaTypeSelect->group('m_catalog_data_order.type');
        $aromaTypeFetch = $db->fetchAll($aromaTypeSelect);


        foreach ($aromaTypeFetch as $key => $item) {

            $aromaTypeFilter[$item['type']] = $aroma_type[$item['type']];
        }
        unset($aromaTypeFilter[0]);
        $view->aromaTypeFilter = $aromaTypeFilter;


        if ($_GET['sort'] == 'abc' || $_GET['sort'] == 'popular') {
            switch ($_GET['sort']) {
                case 'abc':
                    $select->order('title ASC');
                    break;
                case 'popular':
                    $select->order('id  ASC');
                    break;
            }
        } else {
            $select->order('title ASC');
        }

		$iter=0;
		$where = [];
        $q_catalog_data = $db->query($select);
        while ($c_catalog_data = $q_catalog_data->fetch()) {

            if ($c_catalog_data['img'] == '') {
                $img = '/images/noimage.jpg';
                $img_big = '/images/noimage.jpg';
            } else {
                $img = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/small/' . $c_catalog_data['img'];
                $img_big = '/images/uploads/catalog/' . $c_catalog_data['id'] . '/big/' . $c_catalog_data['img'];
            }

            if (!$og_image) {
                if ($c_catalog_data['img'] != '') {
                    $og_image = $img;
                }
            }

            if ($c_catalog_data['pol'] == 'F') {
                $catalog_item_f[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            } elseif ($c_catalog_data['pol'] == 'M') {
                $catalog_item_m[] = array(
                    'id' => $c_catalog_data['id'],
                    'title' => $c_catalog_data['title'],
                    'brend_title' => $brend['title'],
                    'pol' => $c_catalog_data['pol'],
                    'url' => '/production/' . $canon_name2 . '/' . $c_catalog_data['url'] . '/',
					'id_catalog'	=> $c_catalog_data['id'],
                    'brend_url' => $canon_name2,
                    'img' => $img,
                    'is_top' => $c_catalog_data['is_top'],
                    'is_spec' => $c_catalog_data['is_spec'],
                    'img_big' => $img_big,
                    'alt' => $c_catalog_data['alt'],
                    'price_s' => $c_catalog_data['price_s'] + $_SESSION['_GMARGIN'],
                    'price_e' => $c_catalog_data['price_e'] + $_SESSION['_GMARGIN'],
                    'anot' => maxsite_str_word(html_entity_decode($c_catalog_data['text']), 10) . ' ...'
                );
            }
			
			if ($iter < 3) {
				$where[] = $c_catalog_data['id'];
			}
			$iter++;
        }
        $GLOBALS['dataLayer']['PageType'] = 'ListingPage';
		if (is_array($catalog_item_f)) {
            $view->catalog_item_f = $catalog_item_f;
		}
		if (is_array($catalog_item_m)) {
            $view->catalog_item_m = $catalog_item_m;
		}
		if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
        }
		/*
        if (is_array($catalog_item_f)) {
            $view->catalog_item_f = $catalog_item_f;
            $where = [];
            foreach ($catalog_item_f as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
        }
        if (is_array($catalog_item_m)) {
            $view->catalog_item_m = $catalog_item_m;
            $where = [];
            foreach ($catalog_item_m as $item) {
                $where[] = $item['id'];
            }
            if (!empty($where)) {
                $select = "SELECT "
                    ."id "
                    ."FROM m_catalog_data_order  "
                    ."WHERE id_catalog_data IN ("
                    .implode(',', $where)
                    .")";
                $items = $db->query($select);
                while ($item = $items->fetch()) {
                    $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
                }
            }
        }
		*/
		
        $show_outstock = true;
        foreach ($catalog_item_f as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }
        foreach ($catalog_item_m as $item) {
            if ($item['price_s'] != 0) {
                $show_outstock = false;
            }
        }


        $view->checked = $checked;
        $view->show_outstock = $show_outstock;


        $view->brend_title = $brend['title'];
        $view->brend = $brend;
        $view->brend_anot = $brend['anot'];
        $view->brend_url = $canon_name2;

        /* -=========================================
        META
        */
        $meta_title = $brend['meta_title'];
        $meta_title_1 = $brend['meta_title_1'];

        $view->meta_title = $meta_title;
        $view->meta_title_1 = $meta_title;

        if ($brend['title_1'] == '') {
            $m_brend_title_1 = $brend['title'];
        } else {
            $m_brend_title_1 = $brend['title_1'];
        }
		$view->seo_description = "";
        $constructor['meta_title'] = "{$brend['title']} духи женские и мужские - туалетная вода по низкой цене, купить парфюм {$m_brend_title_1} в интернет-магазине Parfumoff.ru";
        /*$constructor['meta_description'] = "Неповторимый аромат духов {$brend['title']}, купите духи {$m_brend_title_1} по лучшей цене со скидкой! Лучшие цены на любые ароматы и парфюм от {$brend['title']} ({$m_brend_title_1}) в интернет-магазине парфюмерии Энигма.";*/
       // $constructor['meta_description'] = "Женские и мужские духи, туалетная вода {$brend['title']}, купить парфюм {$m_brend_title_1} по низкой цене с доставкой по {городу}, рейтинг и отзывы покупателей.";
        $constructor['meta_description'] = $brend['meta_description'];
        //$view->body_text = "Выберите понравившийся вам аромат бренда {$brend['title']}: духи, мужская или женская туалетная вода, парфюмированная вода или одеколон, а может быть дезодорант {$m_brend_title_1} - лучшие предложения в нашем магазине парфюмерии.";
		$view->body_text = $brend['text'];
		/*
		$view->seo_description = "<p>Интернет-магазин Parfumoff предлагает Вам выбрать оригинальный аромат из коллекции знаменитого парфюма от {$brend['title']}.</p><br>";
		$view->seo_description.= "<p>Купить туалетную воду или духи {$brend['title_1']}, значит, стать обладателем качественного парфюма, который будет подчеркивать Ваш высокий статус в обществе.</p><br>";
		$view->seo_description.= "<p>В нашем каталоге представлена только оригинальная парфюмерия {$brend['title']} с подробным описанием и отзывами покупателей.</p><br>";
		$view->seo_description.= "<p>Покупая парфюм в нашем магазине через интернет, вы можете быть уверены не только в качестве товара, которое подтверждается многочисленными отзывами наших клиентов, но и в том, что цена на духи {$brend['title']}, значительно ниже, чем стоимость аналогичного товара в обычном магазине.</p>";
		*/
        $constructor['og:title'] = $constructor['meta_title'];
        $constructor['og:image'] = 'http://' . $_SERVER['HTTP_HOST'] . str_replace('/big/', '/small/', $og_image);
        $constructor['og:description'] = $constructor['meta_description'];
        $constructor['og:url'] = $_SERVER['REQUEST_URI'];

        $constructor['content'] = $view->render('content_catalog.php');
    }

} else {
    $constructor['meta_title'] = 'Торговые дома - Parfumoff.ru';
    $constructor['content'] = $view->render('content_brend_all.php');
}


if($_SERVER['REQUEST_URI'] == '/production/'.$brend['url'].'/'){
	$constructor['meta_title'] = "Духи {$brend['title']}, купить туалетную воду и парфюм {$brend['title_1']}, цена в интернет-магазине Parfumoff.ru";
	$constructor['meta_description'] = "Духи {$brend['title_1']} в интернет-магазине оригинальной парфюмерии Parfumoff. Купить парфюм и туалетную воду {$brend['title']} по низкой цене с доставкой.";
	$constructor['meta_keywords'] = "Духи {$brend['title']}, туалетная вода, парфюм {$brend['title_1']}, парфюмерия";
}


/*
if($_SERVER['REQUEST_URI'] == '/production/abercrombie-fitch/'){
	$constructor['meta_title'] = "Abercrombie & Fitch духи, купить парфюм с доставкой по {городу} - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/adidas/'){
	$constructor['meta_title'] = " Adidas парфюм, духи Адидас для женщин и мужчин, купить парфюм по низкой цене - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/agent-provocateur/'){
	$constructor['meta_title'] = " Agent Provocateur парфюм, купить духи Агент Провокатор по низкой цене с доставкой по {городу} - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/amouage/'){
	$constructor['meta_title'] = "Amouge духи, купить парфюм для женщин Амуаж по привлекательной цене - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/antonio-banderas/'){
	$constructor['meta_title'] = " Antonio Banderas духи, купить мужские и женские духи Антонио Бандерас с быстрой доставкой по {городу}- оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/armand-basi/'){
	$constructor['meta_title'] = "Armand Basi духи, купить парфюм Арманд Баси,  цены в {городе} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/baldinini/'){
	$constructor['meta_title'] = "Baldinini духи, купить парфюм с доставкой по {городу} - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/banana-republic/'){
	$constructor['meta_title'] = "Banana Republic духи, купить женские духи Банана Репаблик - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/blumarine/'){
	$constructor['meta_title'] = "Blumarine духи, купить парфюмерию Блюмарин в {городе}, цены и отзывы  - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/bogart/'){
	$constructor['meta_title'] = "Bogart парфюм, купить духи богарт по низкой цене в {городе} - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/britney-spears/'){
	$constructor['meta_title'] = "Britney Spears духи, купить парфюм Бритни Спирс, цены и отзывы, доставка по {городу}  - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/bruno-banani/'){
	$constructor['meta_title'] = "Bruno Banani духи, купить парфюм Бруно Банани недорого, цены и отзывы, быстрая доставка по {городу} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/burberry/'){
	$constructor['meta_title'] = "Burberry духи, женские и мужски духи Барбери, купить парфюм по низкой цене с быстрой доставкой по {городу} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/bvlgari/'){
	$constructor['meta_title'] = "Bvlgari парфюм, духи мужские и женские Булгари, купить по доступной цене с быстрой доставкой по {городу}  - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/cacharel/'){
	$constructor['meta_title'] = "Cacharel парфюм, купить духи Кашарель, цены и отзывы, быстрая доставка по {городу} - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/calvin-klein/'){
	$constructor['meta_title'] = "  Calvin Klein духи, женские и мужские духи Кельвин Кляйн, купить по низкой цене, быстрая доставка по {городу}  - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/carolina-herrera/'){
	$constructor['meta_title'] = "Carolina Herrera духи, купить парфюм Каролина Херрара, цены и отзывы, доставка по {городу} - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/cartier/'){
	$constructor['meta_title'] = "Cartier духи, купить духи Картье, цены на женские и мужские ароматы - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/cerruti/'){
	$constructor['meta_title'] = "Cerruti духи, купить парфюм Черутти, цены на духи и туалетную воду Черутти - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/chanel/n5/'){
	$constructor['meta_title'] = "Chanel 5 духи, купить парфюм Шанель 5 оригинал в {городе}, цены и отзывы, быстрая доставка - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/chanel/allure/'){
	$constructor['meta_title'] = "Chanel Allure парфюм, духи Шанель Аллюр купить в {городе}, цена и отзывы, быстрая доставка - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/chanel/chance/'){
	$constructor['meta_title'] = "Chanel Chance духи, купить парфюм Шанель Шанс,  цены и отзывы, женская туалетная вода Шанель Шанс  - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/chanel/coco/'){
	$constructor['meta_title'] = "Chanel Coco парфюм, купить духи Коко Шанель, низкие цены и реальные отзывы, быстрая доставка по {городу} - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/chanel/'){
	$constructor['meta_title'] = " Духи Chanel (Шанель), цена на парфюм Шанель, купить в интернет магазине с доставкой по {городу} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/chloe/love/'){
	$constructor['meta_title'] = "Chloe Love духи, аромат Хлое Лав для женщин, купить парфюм, узнать отзывы и цены  - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/chloe/'){
	$constructor['meta_title'] = "Chloe духи, купить парфюм Хлоя в интернет магазине, цены и отзывы, быстрая доставка по {городу} - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/chloe/love/'){
	$constructor['meta_title'] = " Chloe Love парфюм, купить женские духи Хлое Лав, узнать цены и отзывы об аромате - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/chopard/'){
	$constructor['meta_title'] = " Chopard духи, купить туалетную воду Шопард для женщин, цены и отзывы, быстрая доставка по {городу} - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/chopard/wish/'){
	$constructor['meta_title'] = "Chopard Wish винтажные духи, купить парфюм (туалетную воду) для женщин  - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/christian-dior/'){
	$constructor['meta_title'] = "Christian Dior парфюм, купить духи Кристиан Диор для женщин и мужчин, цены и отзывы, быстрая доставка - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/coach/'){
	$constructor['meta_title'] = "Coach духи, купить парфюм Коуч, женские и мужские ароматы, отзывы и цены, доставка по {городу} - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/comme-des-garcons/'){
	$constructor['meta_title'] = "Comme Des Garcons парфюм, купить духи Ком Де Гарсон, ароматы для мужчин и женщин, цены и отзывы - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/creed/'){
	$constructor['meta_title'] = "Духи Creed, купить парфюм Крид, цена и отзывы, доставка по {городу} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/davidoff/'){
	$constructor['meta_title'] = " Духи Davidoff, элитная парфюмерия Davidoff, купить парфюм Давидофф в интернет магазине, цены и отзывы- оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/dolce-gabbana/'){
	$constructor['meta_title'] = "Dolce & Gabbana духи, женский и мужской парфюм Дольче Габбана, цена и отзывы об аромате - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/dolce-gabbana/dolce/'){
	$constructor['meta_title'] = "Dolce & Gabbana Dolce женская туалетная вода, купить парфюм по низкой цене, реальные отзывы об аромате - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/donna-karan/dkny/'){
	$constructor['meta_title'] = " Духи DKNY, купить Донна Каран парфюм от Donna Karan, цены и отзывы о женском аромате DKNY - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/donna-karan/'){
	$constructor['meta_title'] = "Donna Karan парфюм, купить туалетную воду и духи Донна Каран, цена и отзывы об аромате - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/dsquared2/'){
	$constructor['meta_title'] = " Dsquared 2 духи, купить парфюм Дискваред 2, цены на духи и туалетную воду в {городе}, отзывы об аромате - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/dupont/'){
	$constructor['meta_title'] = "S.T. Dupont духи, купить элитную парфюмерию  Дюпон, цены на парфюм и отзывы об аромате - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/elizabeth-arden/'){
	$constructor['meta_title'] = "Духи Elizabeth Arden, купить парфюм Элизабет Арден, цены на женские и мужские ароматы, отзывы о парфюме - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/escada/'){
	$constructor['meta_title'] = "Escada духи, купить парфюм Эскада в {городе} женские и мужские ароматы, цены и отзывы о парфюме - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/escentric-molecules/'){
	$constructor['meta_title'] = "Escentric Molecules духи, парфюм Молекула Эксцентрик цены в {городе}, женские и мужские ароматы, отзывы о парфюме - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/estee-lauder/'){
	$constructor['meta_title'] = "Estee Lauder парфюм, купить духи Эсти Лайдер по низкой цене, быстрая доставка по {городу}, отзывы об аромате - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/fendi/'){
	$constructor['meta_title'] = "Fendi  духи, парфюм для женщин и мужчин Фенди, купить туалетную воду Fendi, цены и отзывы об аромате  - оригинальная парфюмерия Parfumoff";
}




if($_SERVER['REQUEST_URI'] == '/production/fragonard/'){
	$constructor['meta_title'] = "Fragonard (Фрагонар) духи, купить парфюм Фрагонар в {городе}, цены и отзывы - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/franck-olivier/'){
	$constructor['meta_title'] = "Franck Olivier духи, купить парфюм Франк Оливер (Франция), цены и отзывы, доставка по {городу} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/giorgio-armani/'){
	$constructor['meta_title'] = "Giorgio Armani духи, купить парфюм Джорджио Армани женские и мужские ароматы, цены в {городе} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/givenchy/'){
	$constructor['meta_title'] = " Givenchy духи, женский и мужской парфюм Givenchy(Живанши), купить духи Живанши в {городе} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/gucci/'){
	$constructor['meta_title'] = "Парфюм Gucci, купить духи и туалетную воду Гуччи, цены в {городе} и отзывы об аромате Gucci (Гуччи) - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/guerlain/'){
	$constructor['meta_title'] = "Guerlain духи, купить женскую и мужскую парфюмерию Герлен (Guerlen), каталог парфюма Герлен, цены и отзывы - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/guess/'){
	$constructor['meta_title'] = "Духи Guess, купить парфюм Guess, цены и отзывы на аромат - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/hermes/'){
	$constructor['meta_title'] = "Hermes духи, купить парфюм Гермес мужской и женский, цены в {городе}, быстрая доставка по {городу} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/hugo-boss/'){
	$constructor['meta_title'] = "Hugo Boss  духи, купить духи и туалетную воду Hugo Boss для женщин и мужчин, цены и отзывы, доставка по {городу}  - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/jean-paul-gaultier/'){
	$constructor['meta_title'] = "Jean Paul Gaultier духи, купить парфюм Жан Поль Готье  в интернет магазине в {городе}, цены и отзывы об аромате - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/kenzo/'){
	$constructor['meta_title'] = "Kenzo духи, женский и мужской парфюм  Кензо, купить духи и туалетную воду с доставкой по {городу} - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/lacoste/'){
	$constructor['meta_title'] = "Lacoste парфюм, купить женские и мужские духи Лакост (Lacoste), цены и отзывы об аромате Лакост - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/lalique/'){
	$constructor['meta_title'] = "Lalique духи, купить парфюм Лалик в {городе}, цены на духи и туалетную воду Lalique - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/lancome/'){
	$constructor['meta_title'] = "Lancome парфюм,  купить духи и туалетную воду Ланком, цены на ароматы Lancome (Ланком), отзывы - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/lanvin/'){
	$constructor['meta_title'] = "Lanvin духи, купить парфюм Ланвин, цены на духи и туалетную Lanvin, отзывы - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/marc-jacobs/'){
	$constructor['meta_title'] = " Marc Jacobs парфюм, купить духи Марк Джейкобс, цены на парфюмерию Marc Jacobs - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/mexx/'){
	$constructor['meta_title'] = "Mexx парфюм, купить туалетную воду и духи Мекс (Mexx), цены на парфюм, отзывы об аромате - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/montale/'){
	$constructor['meta_title'] = "Montale духи, купить парфюм Montale, цены на духи и туалетную воду Монталь (Франция)  - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/moschino/'){
	$constructor['meta_title'] = "Moschino духи, купить парфюм Москино, цены на духи и туалетную воду  Moschino (Москино)  - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/narciso-rodriguez/'){
	$constructor['meta_title'] = " Narciso Rodriguez духи, купить парфюм Нарциссо Родригес, цены и отзывы об аромате - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/nina-ricci/'){
	$constructor['meta_title'] = "Nina Ricci парфюм, купить духи и туалетную воду Нина Ричи (Nina Ricci), цены и отзывы на парфюм  - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/paco-rabanne/'){
	$constructor['meta_title'] = "  Paco Rabanne духи, купить парфюм Пако Рабан, цены на духи и туалетную воду Paco Rabanne в {городе}, отзывы - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/prada/'){
	$constructor['meta_title'] = "Prada парфюм, купить духи Прада, цены на мужской и женский парфюм Prada, отзывы - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/ralph-lauren/'){
	$constructor['meta_title'] = "Ralph Lauren парфюм, купить духи Ральф Лорен, цены на женский и мужской парфюм Ralph Lauren - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/roberto-cavalli/'){
	$constructor['meta_title'] = "Roberto Сavalli духи, купить духи Роберто Кавалли, цены на парфюм Roberto Cavalli - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/salvador-dali/'){
	$constructor['meta_title'] = "Salvador Dali духи, купить парфюм Сальвадор Дали, цены на духи и туалетную воду Salvador Dali - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/salvatore-ferragamo/'){
	$constructor['meta_title'] = "Salvatore Ferragamo духи, купить парфюм Salvatore Ferragamo, цены и отзывы об аромате - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/thierry-mugler/'){
	$constructor['meta_title'] = "Духи Thierry Mugler, купить туалетную воду и духи Тьерри Мюглер, цены на парфюм Thierry Mugler - оригинальная парфюмерия Parfumoff";
}


if($_SERVER['REQUEST_URI'] == '/production/trussardi/'){
	$constructor['meta_title'] = "Ttrussardi духи, купить туалетную воду и духи Труссарди, цены на парфюм Trussardi в {городе} - оригинальная парфюмерия Parfumoff";
}



if($_SERVER['REQUEST_URI'] == '/production/valentino/'){
	$constructor['meta_title'] = "Духи Valentino (Валентино), купить парфюм Валентино по низкой цене в {городе} - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/versace/'){
	$constructor['meta_title'] = "Versace духи, купить парфюм Версаче, цены на духи или туалетную воду Versace - оригинальная парфюмерия Parfumoff";
}

if($_SERVER['REQUEST_URI'] == '/production/yves-saint-laurent/'){
	$constructor['meta_title'] = "Yves Saint Laurent духи, купить туалетную воду и духи Ив Сен Лоран, цены на женский и мужской парфюм - оригинальная парфюмерия Parfumoff";
}
*/
?>