<?

if (count($_GET)==0){
    err_301('');
}

$sql_m_catalog_block = '';
$sql_m_catalog_data_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$sql_m_catalog_block = ' AND id != 106 AND id != 83 AND id != 176 AND id != 460';
	$sql_m_catalog_data_block = 'm_catalog_data.id_catalog != 106';
}


//prepare main query
$select = $db->select();
$select->from(
    'm_catalog_data_order',
    ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type','brend_url'=>'m_catalog.url','brend_title'=>'m_catalog.title']
);
$select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
$select->joinLeft('m_catalog', 'm_catalog.id=m_catalog_data.id_catalog', '');
$select->limit(200);


//берем брэнд
if ($_GET['brand_id'] &&$_GET['brand_id']!=0){
    $brend = $db->fetchRow("SELECT * FROM m_catalog WHERE  id=? and is_block=0{$sql_m_catalog_block} ",[$_GET['brand_id']]);
    //echo $canon_name2;
    if ($brend == false) {
       // err_404();
    }
    $view->brend = $brend;
}

//Пол
if ($pol = $_GET['pol']){
    $checked['pol'][$pol]=1;
}

if (!$_GET['brand_id'] && !$_GET['pol']){
    //err_404();
}






/**
 * s100 //100ml высчитываем исходя из брэнда и пола
 */
$s100select = clone $select;
$s100select->reset(Zend_Db_Select::GROUP);
$s100select->reset(Zend_Db_Select::WHERE);
$s100select->where('m_catalog_data.price_s!=0');
$s100select->where('m_catalog_data_order.price>0');
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$s100select->where('m_catalog_data.id_catalog != 106')->where('m_catalog_data.id_catalog != 83')->where('m_catalog_data.id_catalog != 176')->where('m_catalog_data.id_catalog != 460');
}
if ($brend){
    $s100select->where('m_catalog_data.id_catalog=?', $brend['id']);
}
if ($pol){
    $s100select->where('m_catalog_data.pol in (?)', $pol);
}

if (!$brend && !$pol){
    $s100[]['s100'] = 500;
    $s100[]['s100'] = 1500;
    $s100[]['s100'] = 2500;
    $s100[]['s100'] = 3500;
    $s100[]['s100'] = 4500;
    $s100[]['s100'] = 5500;
    $s100[]['s100'] = 6500;
    $s100[]['s100'] = 7500;
    $s100[]['s100'] = 8500;
    $s100[]['s100'] = 9500;
    $s100[]['s100'] = 10500;

}else{
    $s100 = $db->fetchAll($s100select);
}



foreach ($s100 as $item) {
    switch (true) {
        case ($item['s100'] <= 1000):
            $s100filter[0] = 'До 1000';
            break;
        case ($item['s100'] > 1001 && $item['s100'] <= 2000):
            $s100filter[1] = 'От 1001 до 2000';
            break;
        case ($item['s100'] > 2001 && $item['s100'] <= 3000):
            $s100filter[2] = 'От 2001 до 3000';
            break;
        case ($item['s100'] > 3001 && $item['s100'] <= 4000):
            $s100filter[3] = 'От 3001 до 4000';
            break;
        case ($item['s100'] > 4001 && $item['s100'] <= 5000):
            $s100filter[4] = 'От 4001 до 5000';
            break;
        case ($item['s100'] > 5001 && $item['s100'] <= 6000):
            $s100filter[5] = 'От 5001 до 6000';
            break;
        case ($item['s100'] > 6001 && $item['s100'] <= 7000):
            $s100filter[6] = 'От 6001 до 7000';
            break;
        case ($item['s100'] > 7001 && $item['s100'] <= 8000):
            $s100filter[7] = 'От 7001 до 8000';
            break;
        case ($item['s100'] > 8001 && $item['s100'] <= 9000):
            $s100filter[8] = 'От 8001 до 9000';
            break;
        case ($item['s100'] > 9001 && $item['s100'] <= 10000):
            $s100filter[9] = 'От 9001 до 10000';
            break;
        case ($item['s100'] > 10000):
            $s100filter[10] = 'Больше 10000';
            break;
    }
}
ksort($s100filter);
$view->s100filter = $s100filter;
if (count($_GET['s100'])>0){
    $subquery = $db->select();
    foreach ($_GET['s100'] as $key => $item) {
        $item = (int)$item;
        if ($item == 0) {
            $subquery->orWhere('m_catalog_data_order.s100 <1000');
        } elseif ($item == 10) {
            $subquery->orWhere('m_catalog_data_order.s100 >10000');
        } else {
            $from = $item * 1000 + 1;
            $to = $item * 1000 + 1000;
            $subquery->orWhere("m_catalog_data_order.s100>{$from} and m_catalog_data_order.s100<={$to}");
        }
        $checked['s100'][$_GET['s100'][$key]] = 1;
    }
    $subquery = $subquery->getPart(Zend_Db_Select::WHERE);
    $select->where(implode(' ',$subquery));
}

$aromaTypeSelect = clone $select;
$aromaTypeSelect->reset(Zend_Db_Select::GROUP);
$aromaTypeSelect->reset(Zend_Db_Select::WHERE);
$aromaTypeSelect->where('m_catalog_data_order.price!=0');
if ($brend['id']){
    $aromaTypeSelect->where('m_catalog_data.id_catalog=?', $brend['id']);
}

$aromaTypeSelect->group('m_catalog_data_order.type');
$aromaTypeFetch = $db->fetchAll($aromaTypeSelect);
foreach ($aromaTypeFetch as $key => $item) {
    $aromaTypeFilter[$item['type']] = $aroma_type[$item['type']];
}
unset($aromaTypeFilter[0]);
$view->aromaTypeFilter = $aromaTypeFilter;

if ($_GET['aroma_type']){
    foreach ($_GET['aroma_type'] as $key => $type) {
        if (!array_key_exists($type, $aroma_type)) {
            unset($_GET['aroma_type'][$key]);
        } else {
            $aroma_type_checked[$_GET['aroma_type'][$key]] = 1;
        }
    }
    $checked['aroma_type'] = $aroma_type_checked;
    if (count($_GET['aroma_type'])>0){
        $select->where('m_catalog_data_order.type  IN (?)',$_GET['aroma_type']);
    }
}

if ($brend['id']){
    $select->where('m_catalog_data.id_catalog=?', $brend['id']);
}
if ($pol){
    $select->where('m_catalog_data.pol in (?)', $pol);
}

//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$select->where('m_catalog_data.id_catalog != 106')->where('m_catalog_data.id_catalog != 83')->where('m_catalog_data.id_catalog != 176')->where('m_catalog_data.id_catalog != 460');
}
$select->where('m_catalog_data_order.price>0');
$select->group('m_catalog_data.id');





$catalog_items  =$db->fetchAll($select);

foreach ($catalog_items as &$catalog_item) {
    if ($catalog_item['img'] == '') {
        $catalog_item['img'] = '/images/noimage.jpg';
        $catalog_item['img_big'] = '/images/noimage.jpg';
    } else {
        $catalog_item['img'] = '/images/uploads/catalog/' . $catalog_item['id'] . '/small/' . $catalog_item['img'];
        $catalog_item['img_big'] = '/images/uploads/catalog/' . $catalog_item['id'] . '/big/' . $catalog_item['img'];
    }
    $catalog_item['url'] = '/production/'.$catalog_item['brend_url'].'/'.$catalog_item['url'].'/';
    $catalog_item['anot'] = maxsite_str_word(html_entity_decode($catalog_item['text']), 10) . ' ...';
}


$view->catalog_items = $catalog_items;

$GLOBALS['dataLayer']['PageType'] = 'ListingPage';
$where = [];
foreach ($catalog_items as $key=>$item) {
	if ($key < 3) {
		$where[] = $item['id'];
	} else {
		break;
	}
}
if (!empty($where)) {
    $select = "SELECT "
        ."id "
        ."FROM m_catalog_data_order  "
        ."WHERE id_catalog_data IN ("
        .implode(',', $where)
        .")";
    $items = $db->query($select);
    while ($item = $items->fetch()) {
        $GLOBALS['dataLayer']['List'] = $where;
        $GLOBALS['dataLayer']['ProductIDList'][] = $item['id'];
    }
}

$view->checked = $checked;

if (isset($_GET['ajax'])){
    $tmpl = '';
    echo $view->render('partials/catalog_items.php');
}

$constructor['content'] = $view->render('filter_test.php');
