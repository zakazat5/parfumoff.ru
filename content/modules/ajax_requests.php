<?php
if ($_GET['ajax_quick_buy'] == 1) {
	$id_product = 0;
	$id_catalog = intval($_POST['quick_buy_id_catalog']);
	if ($id_catalog > 0) {
		$db_res = $db->fetchRow("SELECT id FROM m_catalog_data_order WHERE id_catalog_data='$id_catalog' AND is_block=0 AND price>0 LIMIT 1");
		$id_product = $db_res['id'];
		if (is_null($id_product)) {
			$id_product = 0;
		}
	} 
	$tmpl = "ajax.php";
	$result_ajax = array('id_product' => $id_product);
	echo json_encode($result_ajax);
}	

if ($_GET['ajax_quick_buy_submit'] == 1) {
	
	$classCart->clean();
	$ses_user->is_login = false;
  
    $id = intval($_POST['quick_buy_id_product']);
    // цена
    $price = $db->fetchRow("SELECT price, article, is_block FROM m_catalog_data_order WHERE id='$id' LIMIT 1");
    $price['price'] = $price['price']+$_SESSION['_GMARGIN'];
    $uid = md5($price['article'].$id);

    if ($id != 0 AND $price != 0 ){

        $ses_korzina->kol++;
        $ses_korzina->sum = $ses_korzina->sum + $price['price'];
        $arr_id[] = array(
            'id' 		=> $id,
            'uid' 		=> $uid,
            'kol' 		=> 1,
            'sum' 		=> $price['price']
        );
        $ses_korzina->id = $arr_id;
		$classCart->recount();
		$data['fio'] = 'ФИО';
		$data['phone'] = $_POST['quick_buy_phone'];
		$data['email'] = 'mail@quickbuy' . mt_rand(10000, 10000000) . '.loc' . mt_rand(10000, 10000000);
        $data['name'] = 'Имя';
        $data['patronymic'] = 'Отчество';
        $data['surname'] = 'Фамилия';
		$data['pol'] = 'M';
	
		if (!isset($aErrors)) {
			if (!$oUser = $db->fetchRow($db->select()->from('m_mag_Users')->where('email=?', $data['email']))) {
				$passw = generate_password_simple(6);
				$data['passw'] = md5($passw);
				$data['date_register'] = new Zend_Db_Expr('NOW()');
				$data['date_last_visit'] = new Zend_Db_Expr('NOW()');
				if ($data['fio_data']){
					$data['fio_data'] = '';
				}
			/*
            $data['utm_source_first'] = (isset($_COOKIE['utm_source_first']))?$_COOKIE['utm_source_first']:'';
            $data['utm_medium_first'] = (isset($_COOKIE['utm_medium_first']))?$_COOKIE['utm_medium_first']:'';
            $data['utm_campaign_first'] = (isset($_COOKIE['utm_campaign_first']))?$_COOKIE['utm_campaign_first']:'';
            $data['utm_term_first'] = (isset($_COOKIE['utm_term_first']))?$_COOKIE['utm_term_first']:'';
			*/
				$db->insert('m_mag_Users', $data);
				$data['id'] = $db->lastInsertId();
				$data['passw'] = $passw;
				// инициализируем сессиии
				$ses_user->is_login = true;
				$ses_user->user = $db->fetchRow($db->select()->from('m_mag_Users')->where('id=?', $data['id']));
   
			} else {
				// инициализируем сессиии
				$ses_user->is_login = true;
				$ses_user->user = $db->fetchRow($db->select()->from('m_mag_Users')->where('id=?', $oUser['id']));
				$db->update('m_mag_Users',['phone2'=>$data['phone']],'id='.$oUser['id']);
			}
		}
	
        /* -=========================================
        ДОБАВЛЯЕМ ЗАКАЗ
        */
        $m_catalog_client_orders = array(
            'id_user'	=> intval( $ses_user->user['id'] ),
            'status'	=> 1,
            'pComment'	=> 'Быстрый заказ',
            'DateAdd'	=> new Zend_Db_Expr('NOW()'),
            'dType'     => $classCart->getDiscountType(),
            'dPercent'	=> 0,
            'dSum'		=> $classCart->getTotal(),
            'Sum'		=> $classCart->getSubTotal(),
            'payment_type'=> 1,
            'courier_address_id' => $_POST['courier_address_id'],
            'delivery_cost' => (int) $_POST['delivery_cost']
        );

        if ($m_catalog_client_orders['courier_address_id']==''){
            $m_catalog_client_orders['courier_address_id']=0;
        }

		/*
        $m_catalog_client_orders['utm_source_last'] = (isset($_COOKIE['utm_source_last']))?$_COOKIE['utm_source_last']:'';
        $m_catalog_client_orders['utm_medium_last'] = (isset($_COOKIE['utm_medium_last']))?$_COOKIE['utm_medium_last']:'';
        $m_catalog_client_orders['utm_campaign_last'] = (isset($_COOKIE['utm_campaign_last']))?$_COOKIE['utm_campaign_last']:'';
        $m_catalog_client_orders['utm_term_last'] = (isset($_COOKIE['utm_term_last']))?$_COOKIE['utm_term_last']:'';
		*/
        $db->insert('m_mag_Orders', $m_catalog_client_orders);
        $unic = $db->lastInsertId();
		$products = $classCart->getItems();
		$product = $products[0];
   
        $price_usd = $product["price_usd".$product['sklad']];
        $price_usd 	= str_replace(",",".", $price_usd);
        $margin = catalog_creat_price_level($price_usd);
		
        $m_catalog_client_sum = array(
            'id_order'				=> $unic,
            'id_user'				=> intval( $ses_user->user['id'] ),
            'id_brend'				=> $product['brand_id'],
            'id_catalog_data'		=> $product['aromat_id'],
            'id_catalog_data_order'	=> $product['id'],
            'articul_catalog_data_order'	=> $product['articul'],
            'sklad'					=> $product['sklad'],
            'price_usd'				=> $price_usd,
            'margin'				=> $margin,
            'title'					=> '['.$product['pol'].'-'.$product['sklad'].'] '.$product['title'],
            'sum'					=> $product['price'],
            'gmargin'				=> $_SESSION['_GMARGIN'],
            'kol'					=> 1,
            'is_sale'				=> $product['is_block'],
        );
            $db->insert('m_mag_OrdersSum', $m_catalog_client_sum);
			
			$_SESSION['dt_qb_id'] = $product['id'];
			$_SESSION['dt_qb_price'] = $product['price'];
			$_SESSION['dt_qb_email'] = trim($data['email']);
			$_SESSION['dt_qb_TransactionID'] = $unic;
			
            $arr_ids[] = $product['id'];
			
            //добавляем общую сумму маржи
            $margin_summ = $margin;
			
       // }
		
        $analytics->Purchase($unic);
        $db->update('m_mag_Orders',[
            'summ_margin'		=> $margin_summ,
        ],'id='.$unic);
        $classCart->setIdOrder($unic);
        //выполняем шаг1, очищаем корзину, сохраняем номер заказа
		$classCart->clean();
		$ses_user->is_login = false;
		Zend_Session::namespaceUnset('user');
    }
	$tmpl = "ajax.php";
}