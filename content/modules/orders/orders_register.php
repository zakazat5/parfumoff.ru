<?php
$constructor['is_right'] = 1;
require_once 'Int/Service/Amazon/Ses.php';
require_once 'Int/Service/Amazon/Ses/Email.php';


$capital_districts= array(
    'm'=>array('7700000000000'=>'Москва в пределах МКАД',
        '0000000000001'=> 'Жулебино',
        '0000000000002'=>'Косино',
        '0000000000003'=> 'Новокосино',
        '0000000000004'=> 'Кожухово',
        '0000000000005'=> 'Бутово',
        '0000000000006'=> 'Солнцево и Ново-Переделкино',
        '0000000000007'=> 'Ново-Переделкино',
        '0000000000008'=> 'Зеленоград',
        '0000000000009'=> 'Митино',
        '0000000000010'=> 'Куркино'),
    's'=>array(
        '7800000000000'=> 'Санкт-Петербург в пределах КАД',
        '7800000900000'=> 'Пушкин',
        '7800000700000'=> 'Павловск',
        '780000400000'=> 'Красное Село',
        '7800000800000'=> 'Петергоф'
    ));
$view->capital_districts = $capital_districts;


if (isset($_POST['complete'])){
    if ($ses_user->is_login){
        //update address data
        $user_data['address_region'] = $_POST['region'];
        $user_data['address_other'] = $_POST['other'];
        $user_data['address_city'] = $_POST['city'];
        $user_data['address_settlement'] = $_POST['settlement'];
        $user_data['address_street'] = $_POST['street'];
        $user_data['address_house'] = $_POST['house'];
        $user_data['address_corpus'] = $_POST['corpus'];
        $user_data['address_stroenie'] = $_POST['stroenie'];
        $user_data['address_floor'] = $_POST['foor'];
        $user_data['address_flat'] = $_POST['flat'];
        $user_data['address_domophone'] = $_POST['domophone'];
        $user_data['postcode'] = $_POST['postcode'];

        $where= $db->quoteInto('id=?',$classCart->user['id']);
        $db->update('m_mag_Users',$user_data,$where);

        /* -=========================================
        ДОБАВЛЯЕМ ЗАКАЗ
        */
        $m_catalog_client_orders = array(
            'id_user'	=> intval( $ses_user->user['id'] ),
            'status'	=> 1,
            'pComment'	=> $_POST['pComment'],
            'DateAdd'	=> new Zend_Db_Expr('NOW()'),
            'dType'     => $classCart->getDiscountType(),
            'dPercent'	=> $classCart->getDiscount(),
            'dSum'		=> $classCart->getTotal(),
            'Sum'		=> $classCart->getSubTotal(),
            'summ_margin'		=> 10,
            'courier_address_id' => $_POST['courier_address_id'],
            'delivery_cost' => (int) $_POST['delivery_cost']
        );
        $db->insert('m_mag_Orders', $m_catalog_client_orders);
        $unic = $db->lastInsertId();


        foreach($classCart->getItems() as $product){
            $price_usd = $product["price_usd".$product['sklad']];
            $price_usd 	= str_replace(",",".", $price_usd);
            $margin = catalog_creat_price_level($price_usd);
            $m_catalog_client_sum = array(
                'id_order'				=> $unic,
                'id_user'				=> $classCart->getUser('id'),
                'id_brend'				=> $product['brand_id'],
                'id_catalog_data'		=> $product['aromat_id'],
                'id_catalog_data_order'	=> $product['id'],
                'sklad'					=> $product['sklad'],
                'price_usd'				=> $price_usd,
                'margin'				=> $margin,
                'title'					=> '['.$product['pol'].'-'.$product['sklad'].'] '.$product['title'],
                'sum'					=> $product['price'],
                'gmargin'				=> $_SESSION['_GMARGIN'],
                'kol'					=> $product['kol'],
                'is_sale'				=> $product['is_block'],
            );
            $db->insert('m_mag_OrdersSum', $m_catalog_client_sum);

            $mail_orders[] = array(
                'title' 	=> '['.$product['pol'].'] '.$product['title'],
                'price' 	=> $product['price'],
                'kol' 		=> $product['kol'],
                'productId' => $product['id'],
                'margin' => $margin
            );
            $arr_ids[] = $product['id'];
            //добавляем общую сумму маржи
            $margin_summ += $margin*$product['kol'];
        }

        /* -=========================================
				пишем письмо клиенту
				*/
        $view->unic		= $unic;
        $view->fio		= $classCart->getUser('fio');
        $view->phone	= $classCart->getUser('phone');
        $view->percent	= $classCart->getDiscount();
        $view->sum		= $classCart->getSubTotal();
        $view->big_sum	= $classCart->getTotal();
        $view->orders 	= $mail_orders;
        $view->margin_summ	= $margin_summ;


        $message = $view->render("users/orders/orders_mail.php");

        /* -=========================================
        письмо юзеру
        */
        Int_Service_Amazon_Ses::setKeys('AKIAJRZF4VNOC6YNBAZQ', 'Z3XtxspW9qkUUf/8K3WiYYwGyeLxOvPKvU47gvFL');

        $mail = new Int_Service_Amazon_Ses_Email();
        $mail->addTo('mr.nobody.lip@gmail.com')
            ->setFrom('orders@parfumoff.ru', 'parfumoff')
            ->setSubject(iconv('windows-1251', 'utf-8', 'Ваша заявка принята [parfumoff.ru]'))
            ->setReturnPath('orders@parfumoff.ru')
            ->setBodyHtml($message, 'utf-8');

        $ses = new Int_Service_Amazon_Ses();
        $messageId = $ses->sendEmail($mail);

        /* -=========================================
        письмо партнеру
        */
        Int_Service_Amazon_Ses::setKeys('AKIAJRZF4VNOC6YNBAZQ', 'Z3XtxspW9qkUUf/8K3WiYYwGyeLxOvPKvU47gvFL');

        /*$mail = new Int_Service_Amazon_Ses_Email();
		$mail->addTo('nexusmsk@gmail.com')
			->setFrom('orders@parfumoff.ru', 'parfumoff')
			->setSubject(iconv('windows-1251', 'utf-8', 'Ваша заявка принята [parfumoff.ru]'))
			->setReturnPath('orders@parfumoff.ru')
			->setBodyHtml($message, 'utf-8');

		$ses = new Int_Service_Amazon_Ses();
        $messageId = $ses->sendEmail($mail);*/
        $classCart->clean();

        $view->orders_ids = array_unique($arr_ids);
        $_SESSION['unic'] = $unic;
        $_SESSION['mail_orders'] = $mail_orders;
        $_SESSION['margin_summ'] = $margin_summ;
        $_SESSION['summ'] = $classCart->getSubTotal();
        $_SESSION['percent'] = $classCart->getDiscount();

        go('/orders/purchase/send/');
    }
}



if ($canon_name2 == 'purchase'){
	   if(isset($_SESSION['unic'])){
           $view->data['unic'] = $_SESSION['unic'];
           $view->unic = $_SESSION['unic'];
		   $view->orders = $_SESSION['mail_orders'];
		   $view->orders = $_SESSION['mail_orders'];
		   $view->percent = $_SESSION['percent'];
		   $view->margin_summ = $_SESSION['margin_summ'];

           $constructor['content'] = $view->render('orders_send.php');
	   }else{
			$view->korzina_message = 'Ваша корзина пуста';
			$constructor['content'] = $view->render('users/orders/orders_purchase.php');
        }


}elseif ($canon_name2 == 'addOrder'){

	$id = intval($canon_name3);

	// цена
	$price = $db->fetchRow("SELECT price, article, is_block FROM m_catalog_data_order WHERE id='$id' LIMIT 1");
	$price['price'] = $price['price']+$_SESSION['_GMARGIN'];
    $uid = md5($price['article'].$id);

	if ($id != 0 AND $price != 0 ){

		$ses_korzina->kol++;
		$ses_korzina->sum = $ses_korzina->sum + $price['price'];

		// просчет дублей просто увеличиваем для ID количество и сумму
		$arr_id = $ses_korzina->id;
		foreach ($arr_id as &$value){
			if ($value['uid'] == $uid ){

				$value['is_sale'] = $price['is_block'];
				$value['kol']++;
				$value['sum'] += $price['price'];
				$ses_korzina->id = $arr_id;

				go('/orders/');
				break;
			}
		}

		$arr_id[] = array(
		'id' 		=> $id,
		'uid' 		=> $uid,
		'kol' 		=> 1,
		'sum' 		=> $price['price']
		);
		$ses_korzina->id = $arr_id;

		go('/orders/');
	}

}elseif ($canon_name2 == 'DescOrder'){


	$uid = $canon_name3;
	$sum = intval($canon_name4);
	$kol = 1;

	$arr_id = $ses_korzina->id;
	foreach ($arr_id as &$value){

		if ($value['uid'] == $uid){

			if ($value['kol'] == 1 ) go('/orders/');

			$ses_korzina->kol = $ses_korzina->kol - $kol;
			$ses_korzina->sum = $ses_korzina->sum - $sum;

			$value['kol']--;
			$value['sum'] -= $sum;
			$ses_korzina->id = $arr_id;


			go('/orders/');
			break;

		}

	}


}elseif ($canon_name2 == 'AscOrder'){

	$uid = $canon_name3;
	$sum = intval($canon_name4);
	$kol = 1;

	$ses_korzina->kol = $ses_korzina->kol + $kol;
	$ses_korzina->sum = $ses_korzina->sum + $sum;

	$arr_id = $ses_korzina->id;
	foreach ($arr_id as &$value){

		if ($value['uid'] == $uid ){
			$value['kol']++;
			$value['sum']+= $sum;
			$ses_korzina->id = $arr_id;
			go('/orders/');
			break;
		}

	}

}elseif ($canon_name2 == 'deleteOrder'){

	$uid = $canon_name3;
	$sum = intval($canon_name4);
	$kol = intval($canon_name5);

	$ses_korzina->kol = $ses_korzina->kol - $kol;
	$ses_korzina->sum = $ses_korzina->sum - $sum;

	$arr_id = $ses_korzina->id;
	foreach ($arr_id as $key => &$value){
		if ($value['uid'] == $uid){
			unset($arr_id[$key]);
			break;
		}
	}
	$arr_id = array_values($arr_id);
	$ses_korzina->id = $arr_id;
	go('/orders/');
}else {

    //листинг корзины
    $view->dostavka = $db->fetchRow("SELECT text_2,text_3 FROM m_text WHERE id=2 ");
    //$cart = $classCart->getFormated();
    $view->cart = $classCart;
    $constructor['content'] = $view->render('orders_register.php');;
}

?>