<?php

if ($canon_name2){
    err_301($canon_name1.'/');
}

$sql_m_catalog_block = '';
$sql_m_catalog_data_a_block = '';
$sql_m_catalog_b_block = '';
$first_page_pointers_view = false;
$last_page_pointers_view = false;
$prev_page = 1;
$nex_page = 1;
$numbered_page_array = array();
if ($page_number && $page_number > 0) {
	$page = $page_number;
} else {
	$page = 1;
}
$products_per_page = 16;
$select ='';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$sql_m_catalog_block = ' AND m_catalog.id != 106 AND m_catalog.id != 83 AND m_catalog.id != 176 AND m_catalog.id != 460';
	$sql_m_catalog_b_block = ' AND b.id != 106 AND b.id != 83 AND b.id != 176 AND b.id != 460';
	$sql_m_catalog_data_a_block = ' AND a.id_catalog != 106 AND a.id_catalog != 83 AND a.id_catalog != 176 AND a.id_catalog != 460';
}
$select = "SELECT "
."COUNT(*) "
."FROM m_catalog_data AS a "
."JOIN m_catalog AS b ON b.id=a.id_catalog "
."WHERE a.id_cat=82 AND a.pol='F' AND a.price_s != 0{$sql_m_catalog_data_a_block}";
$res_query = $db->query($select);
$quantity_prod_arr = $res_query->fetch();
$quantity_prod = (integer)$quantity_prod_arr['COUNT(*)'];
if ($quantity_prod > 0) {
	$last_page_float = $quantity_prod / $products_per_page;
	$last_page = intval($last_page_float);
	if (($last_page_float - floor($last_page_float)) > 0) {
		$last_page++;
	}
	if ($page > $last_page || $page <= 0) {
		err_404($canon_name1.'/');
	}
	if ($page == 1) {
		$item_start = 0;
	} else {
		$item_start = ($page - 1) * $products_per_page;
	}
	if ($page >= 4) {
		$first_page_pointers_view =true;
		$prev_page = $page - 1;
	}
	if ((($last_page - $page) > 2) && $last_page > 5) {
		$last_page_pointers_view = true;
		$next_page = $page + 1;
	}
	if ($last_page > 1) {
			$page_num = $page;
			if ($page <= 3) {
				$i = $page;
			} else {
				if (($last_page - $page) >= 2) {
					$i = 3;
				}
				if (($last_page - $page) < 2) {
					$i = 5 - ($last_page - $page);
				}
			}
			while ($page_num > 0 && $i > 0) {
				$numbered_page_array[$i] = $page_num;
				$page_num--;
				$i--;
			}
			$kol_vo_znach = count($numbered_page_array);
			$i = 5 - $kol_vo_znach;
			$it = $kol_vo_znach;
			$page_num = $page;
			while ($page_num <= $last_page && $i > 0) {
				$it++;
				$page_num++;
				$numbered_page_array[$it] = $page_num;
				$i--;
			}
			$numbered_page = array();
			if (!empty($numbered_page_array)) {
				$kol_vo_znach = count($numbered_page_array);
				$i = 1;
				while ($i < 6 && $i <= $kol_vo_znach) {
					$numbered_page[] = $numbered_page_array[$i];
					$i++;
				}
			}
	}
} else {
	$item_start = 0;
	$last_page = 1;
}
$select ='';
$select = "SELECT "
."a.id, a.title, a.url, a.price_s,a.price_e, a.img, a.text, "
."b.title AS brend_title, b.url AS brend_url "
."FROM m_catalog_data AS a "
."JOIN m_catalog AS b ON b.id=a.id_catalog "
."WHERE a.id_cat=82 AND a.pol='F' AND a.price_s != 0{$sql_m_catalog_data_a_block}  "
."LIMIT " . $item_start . ", " . $products_per_page;

$q_catalog = $db->query($select);
$catalog_item = array();
while ($c_catalog = $q_catalog->fetch()) {

	if ($c_catalog['img']=='') {
		$img = '/images/noimage.jpg';
		$img_big = '/images/noimage.jpg';
	}else {
		$img = '/images/uploads/catalog/'.$c_catalog['id'].'/small/'.$c_catalog['img'];
		$img_big = '/images/uploads/catalog/'.$c_catalog['id'].'/big/'.$c_catalog['img'];
	}

	$catalog_item[] = array(
	'title' 		=> $c_catalog['title'],
	'brend_title' 	=> $c_catalog['brend_title'],
	'price_s' 		=> $c_catalog['price_s'],
	'price_e' 		=> $c_catalog['price_e'],
	'img' 			=> $img,
	'img_big' 		=> $img_big,
	'url' 			=> '/production/'.$c_catalog['brend_url'].'/'.$c_catalog['url'].'/',
	'id_catalog'	=> $c_catalog['id'],
	'anot' 			=> maxsite_str_word(html_entity_decode($c_catalog['text']), 10) .' ...'
	);
}
$view->catalog_item_f = $catalog_item;
$view->last_page = $last_page;
$view->prev_page = $prev_page;
$view->next_page = $next_page;
$view->first_page_pointers_view = $first_page_pointers_view;
$view->last_page_pointers_view = $last_page_pointers_view;
$view->numbered_page = $numbered_page;
$view->page_url = "/woman/";
// переформировываем список брендов
$brend_item = array();

$select = "SELECT id,
            m_catalog.title as title, m_catalog.url as url, m_catalog.is_top as is_top, m_catalog.pol as pol
            FROM m_catalog
            WHERE id IN (SELECT DISTINCT a.id_catalog
                FROM m_catalog_data AS a
                JOIN m_catalog AS b ON b.id=a.id_catalog
                WHERE a.id_cat=82 AND a.pol='F' AND a.price_s != 0) AND m_catalog.is_block=0{$sql_m_catalog_block}
            ORDER BY title";

$stmt = $db->query($select);
while ($content = $stmt->fetch()) {
    $liter = strtolower( $content['title']{0} );

    $brend_item[] = array(
        'id' 	=> $content['id'],
        'title' 	=> $content['title'],
        'url' 		=> $content['url'],
        'liter' 	=> $liter,
        'is_top' 	=> $content['is_top'],
        'pol' 		=> $content['pol'],
    );
}

$view->brend_item = $brend_item;

//filter
$select = $db->select();
$select->limit(200);
$select->from(
    'm_catalog_data_order',
    ['m_catalog_data.*', 'm_catalog_data_order.s100', 'm_catalog_data_order.type']
);
$select->joinLeft('m_catalog_data', 'm_catalog_data_order.id_catalog_data=m_catalog_data.id', '');
/*if ($_GET['all']!=1){
    $select->where('m_catalog_data.price_s!=0');
}*/
$select->where('m_catalog_data.id_catalog=?', $brend['id']);
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$select->where('m_catalog_data.id_catalog!=106')->where('m_catalog_data.id_catalog!=83')->where('m_catalog_data.id_catalog!=176')->where('m_catalog_data.id_catalog!=460');
}
$select->group('m_catalog_data.id');
/**
 * s100
 */
$s100select = clone $select;
$s100select->reset(Zend_Db_Select::GROUP);
$s100select->reset(Zend_Db_Select::WHERE);
$s100select->where('m_catalog_data.price_s!=0');
$s100select->where('m_catalog_data.pol="F"');
$s100select->where('m_catalog_data_order.price>0');

$s100 = $db->fetchAll($s100select);


foreach ($s100 as $item) {
    switch (true) {
        case ($item['s100'] <= 1000):
            $s100filter[0] = 'До 1000';
            break;
        case ($item['s100'] > 1001 && $item['s100'] <= 2000):
            $s100filter[1] = 'От 1001 до 2000';
            break;
        case ($item['s100'] > 2001 && $item['s100'] <= 3000):
            $s100filter[2] = 'От 2001 до 3000';
            break;
        case ($item['s100'] > 3001 && $item['s100'] <= 4000):
            $s100filter[3] = 'От 3001 до 4000';
            break;
        case ($item['s100'] > 4001 && $item['s100'] <= 5000):
            $s100filter[4] = 'От 4001 до 5000';
            break;
        case ($item['s100'] > 5001 && $item['s100'] <= 6000):
            $s100filter[5] = 'От 5001 до 6000';
            break;
        case ($item['s100'] > 6001 && $item['s100'] <= 7000):
            $s100filter[6] = 'От 6001 до 7000';
            break;
        case ($item['s100'] > 7001 && $item['s100'] <= 8000):
            $s100filter[7] = 'От 7001 до 8000';
            break;
        case ($item['s100'] > 8001 && $item['s100'] <= 9000):
            $s100filter[8] = 'От 8001 до 9000';
            break;
        case ($item['s100'] > 9001 && $item['s100'] <= 10000):
            $s100filter[9] = 'От 9001 до 10000';
            break;
        case ($item['s100'] > 10000):
            $s100filter[10] = 'Больше 10000';
            break;
    }
}
ksort($s100filter);
$view->s100filter = $s100filter;

$aromaTypeSelect = clone $select;
$aromaTypeSelect->reset(Zend_Db_Select::GROUP);
$aromaTypeSelect->reset(Zend_Db_Select::WHERE);
$aromaTypeSelect->where('m_catalog_data_order.price!=0');
$aromaTypeSelect->group('m_catalog_data_order.type');
$aromaTypeFetch = $db->fetchAll($aromaTypeSelect);

foreach ($aromaTypeFetch as $key => $item) {
    $aromaTypeFilter[$item['type']] = $aroma_type[$item['type']];
}

$checked['pol']['F'] = 1;

$view->aromaTypeFilter = $aromaTypeFilter;
$view->checked = $checked;

$constructor['meta_title'] = 'Женские духи, купить популярный парфюм для женщин в интернет-магазине Parfumoff.ru';
$constructor['meta_description'] = "Популярные женские духи в интернет-магазине оригинальной парфюмерии Parfumoff. Купить парфюм для женщин известных брендов по низкой цене с доставкой.";
$constructor['meta_keywords'] = "Женские духи, парфюм для женщин, хорошие духи, элитные, популярные, модные, купить, цена, интернет-магазин";

$constructor['content'] = $view->render('content_girl.php');
?>