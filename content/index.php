<?php

/**
 * Баннеры на главной
 */
$aBanners = $db->fetchAll($db->select()->from('m_banner_simple')->where('is_block=0'));
$view->aBanners = $aBanners;


$sql_m_catalog_data_a_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$sql_m_catalog_data_a_block = 'AND a.id_catalog != 106 AND a.id_catalog != 83 AND a.id_catalog != 176 AND a.id_catalog != 460 ';
}

/**
 * Статьи на главной
 */

$select = "SELECT "
    ."id, url, title, anot, img, alt, registerDate "
    ."FROM m_news "
    ."WHERE is_block=0 and id_cat=101 "
    ."ORDER BY id desc "
    ."LIMIT 6";

// QUERY
$stmt = $db->query($select);
while ($content = $stmt->fetch()) {
    $index_news[] = array(
        'id' 	=> $content['id'],
        'url' 	=> $content['url'],
        'title' => $content['title'],
        'img' => $content['img'],
        'alt' => ($content['alt']=='') ? $content['title'] : $content['alt'],
        'date' => date('Y-m-d',strtotime($content['registerDate'])),
        'anot' 	=> word_to_link(($content['anot']), '\.', 'http://'.BASE_URL),
    );
}
$view->index_news = $index_news;


/**
 *  Ароматы на главной
 */
$select = "SELECT "
    ."a.id, a.title, a.url, a.price_s,a.price_e, a.img, a.text, a.pol,a.is_spec, "
    ."b.title AS brend_title, b.url AS brend_url "
    ."FROM m_catalog_data AS a "
    ."JOIN m_catalog AS b ON b.id=a.id_catalog "
    ."WHERE a.id_cat=82 AND a.price_s != 0 AND is_main=1 ".$sql_m_catalog_data_a_block
    ."LIMIT 19";

$q_catalog_top = $db->query($select);
while ($c_catalog_data = $q_catalog_top->fetch()) {

    $catalog_item_top[] = array(
        'title' 		=> $c_catalog_data['title'],
        'is_spec' 		=> $c_catalog_data['is_spec'],
        'brend_title' 	=> $c_catalog_data['brend_title'],
        'price_s' 		=> $c_catalog_data['price_s']+$_SESSION['_GMARGIN'],
        'price_e' 		=> $c_catalog_data['price_e']+$_SESSION['_GMARGIN'],
        'pol' 			=> ($c_catalog_data['pol'] == 'M') ? 'мужская парфюмерия' : 'женская парфюмерия',
        'img' 			=> '/images/uploads/catalog/'.$c_catalog_data['id'].'/small/'.$c_catalog_data['img'],
        'img_big'		=> '/images/uploads/catalog/'.$c_catalog_data['id'].'/big/'.$c_catalog_data['img'],
        'url' 			=> '/production/'.$c_catalog_data['brend_url'].'/'.$c_catalog_data['url'].'/',
		'id_catalog'	=> $c_catalog_data['id'],
        'anot' 			=> maxsite_str_word($c_catalog_data['text'], 9) .' ...'
    );
}
$view->catalog_item_top = $catalog_item_top;


//Список категорий
$select = "SELECT "
    ."id, name, seo_url "
    ."FROM m_category "
	."WHERE active=1 "
    ."ORDER BY sort_order";
	
$categories = $db->query($select);
while ($category = $categories->fetch()) {
	$category_items[] = array(
		'id'		=>	$category['id'],
		'name'		=>	$category['name'],
		'active'	=>	0,
		'url'		=>	$category['seo_url'] . "/"
	);
}
$view->category_items = $category_items;

?>