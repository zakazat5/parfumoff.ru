<?php
$brend_item = array();

$sql_m_catalog_block = '';
//Block brend Estee Lauder, Clinique, Jo Malone, Bobbi Brown
if ($ses_user->is_login == false) {
	$sql_m_catalog_block = ' AND id != 106 AND id != 83 AND id != 176 AND id != 460';
}

$select = "SELECT "
."id,title, url, is_top, pol "
."FROM m_catalog "
."WHERE is_block=0{$sql_m_catalog_block} "
."ORDER BY title ";

$stmt = $db->query($select);
while ($content = $stmt->fetch()) {
	$liter = strtolower( $content['title']{0} );

	$brend_item[] = array(
	'id' 	=> $content['id'],
	'title' 	=> $content['title'],
	'url' 		=> $content['url'],
	'liter' 	=> $liter,
	'is_top' 	=> $content['is_top'],
	'pol' 		=> $content['pol'],
	);
}

$view->brend_item = $brend_item;
$view->brend_liter = array('a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');