<?php

// ini_set('display_errors',1);

class Checkout
{
	public $encoding = 'utf-8';
	public $API_KEY = 'uVZf2fKG7WeIEC2S0GwoGP1Y';
	public $proto = 'https://';
	public $server = '';
	public $url = '.checkout.ru/service';
	public $platform_url = '';
	public $calculate_url = '';

	function __construct($apiKey = false, $server = 'platform')
    {
        if ($apiKey) {
            $this->API_KEY = $apiKey;
        }

		$this->server = $server;

		$this->platform_url = $this->proto . $this->server . $this->url;
    }

	private function getTicket()
	{
		$data = $this->getCurl($this->platform_url.'/login/ticket/'.$this->API_KEY);
		$response = json_decode($data,true);
		return $response['ticket'];
	}

	public function getPlaceByPostcode($postcode)
	{
		$ticket = $this->getTicket();
		$data = $this->getCurl($this->platform_url.'/checkout/getPlaceByPostalCode?ticket='.$ticket.'&postIndex='.$postcode);
		return $data;
	}

	public function getPlaceByCity($place)
	{
		$ticket = $this->getTicket();
		$data = $this->getCurl($this->platform_url.'/checkout/getPlacesByQuery?ticket='.$ticket.'&place='.$place);
		return $data;
	}

	public function getStreet($placeId,$street)
	{
		$ticket = $this->getTicket();
		$data = $this->getCurl($this->platform_url.'/checkout/getStreetsByQuery?ticket='.$ticket.'&placeId='.$placeId.'&street='.$street);
		return $data;
	}

	public function getPostindex($streetId,$house,$housing,$building)
	{
		$ticket = $this->getTicket();
		$data = $this->getCurl($this->platform_url.'/checkout/getPostalCodeByAddress?ticket='.$ticket.'&streetId='.$streetId.'&house='.$house.'&housing='.$housing.'&building='.$building);
		return $data;
	}

	public function getCarriers($weight, $pay_cost, $count, $assessed_cost, $placeId, $index = false, $paymentMethod = 'cash', $express_id = false){
		$ticket = $this->getTicket();

		$this->calculate_url = $this->platform_url.'/checkout/calculation?ticket='.$ticket.'&placeId='.$placeId.'&totalSum='.$pay_cost.'&assessedSum='.$assessed_cost.'&totalWeight='.$weight.'&itemsCount='.$count.'&paymentMethod='.$paymentMethod;

		if ($express_id) {
			$this->calculate_url .= '&expressDeliveryId='.$express_id;
		}

		if ($index) {
			$this->calculate_url .= '&postIndex='.$index;
		}

		$data = $this->getCurl($this->calculate_url);

		return $data;
	}

	public function Calculator($placeId, $totalWeight, $totalSum, $deliveryId = null, $fiasIdFrom = null, $ndsFlag = 0, $postIndex = null) {
		$this->calculate_url = $this->platform_url.'/checkout/calculator?placeId='.$placeId.'&totalSum='.$totalSum.'&totalWeight='.$totalWeight.'&ndsFlag='.$ndsFlag;

		if ($postIndex) {
			$this->calculate_url .= '&postIndex='.$postIndex;
		}

		if ($deliveryId) {
			$this->calculate_url .= '&deliveryId='.$deliveryId;
		}

		if ($fiasIdFrom) {
			$this->calculate_url .= '&fiasIdFrom='.$fiasIdFrom;
		}

		$data = $this->getCurl($this->calculate_url);

		return $data;
	}

	public function createOrder($params,$order_id = 'create')
	{
		$request_array = array(
			'apiKey' => $this->API_KEY,
			'order' => $params
		);

		$request_json = json_encode($request_array);
		$url = $this->platform_url.'/order/'.$order_id;
		$response = $this->getCurl($url, $request_json, true);

		return $response;
	}

	public function infoOrder($order_id)
	{
		$request_array = array(
			'apiKey' => $this->API_KEY,
		);

		$request_json = json_encode($request_array);
		$url = $this->platform_url.'/order/info/'.$order_id;
		$response = $this->getCurl($url, $request_json, true);

		return $response;
	}

	private function getCurl($get, $request = '', $post = false)
	{
		if ($tuCurl = curl_init())
		{
			curl_setopt($tuCurl, CURLOPT_URL, str_replace(' ', '+',$get));
			curl_setopt($tuCurl, CURLOPT_VERBOSE, 0);
			curl_setopt($tuCurl, CURLOPT_HEADER, 0);
			curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
			if ($post == true) {
				curl_setopt($tuCurl, CURLOPT_POST, true);
				curl_setopt($tuCurl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
				curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $request);
			}
			$tuData = curl_exec($tuCurl);
			if(!curl_errno($tuCurl)){
				$info = curl_getinfo($tuCurl);
			} else {
				$error = 'Curl error: ' . curl_error($tuCurl);
			}
			curl_close($tuCurl);

			return $tuData;
		}
		return false;
	}

	public function returnData($data)
	{
		header('Content-Type: application/json; charset=utf-8');
		echo iconv('utf-8', $this->encoding, strval($data));
	}
}

$checkout = new Checkout();

// $ses_cart = new Zend_Session_Namespace('korzina');

if (isset($_REQUEST['postIndex'])) {

	$data = $checkout->getPlaceByPostcode($_REQUEST['postIndex']);
	$checkout->returnData($data);

} else if (isset($_REQUEST['place'])) {

	$data = $checkout->getPlaceByCity($_REQUEST['place']);
	$checkout->returnData($data);

} else if ((isset($_REQUEST['placeId'])) && (isset($_REQUEST['street']))) {

	$data = $checkout->getStreet($_REQUEST['placeId'], $_REQUEST['street']);
	$checkout->returnData($data);

} else if ((isset($_REQUEST['streetId'])) && (isset($_REQUEST['house']))) {

	$data = $checkout->getPostindex($_REQUEST['streetId'], $_REQUEST['house'], $_REQUEST['housing'], $_REQUEST['building']);
	$checkout->returnData($data);

} else if ((isset($_REQUEST['weight'])) && (isset($_REQUEST['price']))) {

	$data = $checkout->getCarriers($_REQUEST['weight'],$_REQUEST['price'],$_REQUEST['quantity'],$_REQUEST['assessed_price'],$_REQUEST['placeIdFias']);
	$checkout->returnData($data);

}
