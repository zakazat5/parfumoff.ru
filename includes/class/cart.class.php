<?



/**
 * Class Cart
 * Class for work with cart
 */
class Cart
{
    /**
     * The cart item objects
     *
     * @var array
     */
    public $_items = array();
    /**
     * Once discount
     *
     * @var float
     */
    protected $_discount = 0.00;
    /**
     * Once discount
     *
     * @var float
     */
    protected $_discountType = 0.00;
    /**
     * Total before shipping
     * @var float
     */
    protected $_subTotal = 0;
    /**
     * Total with shipping
     * @var float
     */
    protected $_total = 0;

    /**
     * сумма распродажи
     * @var int
     */
    protected $_saleTotal = 0;

    /**
     * Total count
     * @var int
     */
    protected $_count = 0;

    /**
     * Total margin
     * @var int
     */
    protected $_margin = 0;

    /**
     * The shipping cost
     * @var float
     */
    protected $_shipping = 0;

    protected $_userDiscount = 0;
    /**
     * ZNS for Persistance
     *
     * @var Zend_Session_Namespace
     */
    protected $_sessionNamespace;

    protected $_coupon = null;

    /**
     *
     */
    public function __construct()
    {
        global $ses_korzina;
        global $ses_user;
        global $db;

        require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User.php";




        $this->ses_korzina = $ses_korzina;
        $this->user = $ses_user->user;

        $this->db = $db;

        $this->recount();
    }


    /**
     * Получить сумму без скидки
     * @return mixed
     */
    public function getSubTotal()
    {
        return $this->_subTotal;
    }

    /**
     * Получить сумму со скидкой
     * @return mixed
     */
    public function getTotal()
    {
        return $this->_total;
    }

    /**
     * получить сумму с распродажи
     * @return int
     */
    public function getSaleTotal()
    {
        return $this->_saleTotal;
    }

    /**
     * Total count
     * @return int
     */
    public function getCount()
    {
        return $this->_count;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->_discount;
    }

    /**
     * @return float
     */
    public function getMargin()
    {
        return $this->_margin;
    }

    /**
     * @return float
     */
    public function getDiscountType()
    {
        return $this->_discountType;
    }

    /**
     * @param string|boolean $row_name
     *
     * @return mixed
     */
    public function getUser($row_name = false)
    {
        if ($row_name) {
            return $this->user[$row_name];
        }
        return $this->user;
    }


    /**
     * @param bool $name Имя параметра
     *
     * @return mixed
     */
    public function getDiscountData($name = false)
    {
        $result = out_discount($this->user['d_percent'], intval($this->getSubTotal()), $this->getSaleTotal());
        if ($name) {
            return $result[$name];
        }
        return $result;
    }

    /**
     * Меняем количество товара в корзине
     *
     * @param $uid string Идентификатор
     * @param $type string тип (asc,desc)
     * @param int $value
     *
     * @return array
     */
    public function change_amount($uid, $type, $value = 1)
    {
        $arr_id = $this->ses_korzina->id;
        foreach ($arr_id as &$value) {
            //change amount
            if ($value['uid'] == $uid) {
                switch ($type) {
                    case 'asc':
                        $value['kol']++;
                        break;
                    case 'desc':
                        if ($value['kol'] > 1) {
                            $value['kol']--;
                        }

                        break;
                }
                $this->ses_korzina->id = $arr_id;
                break;
            }
        }
        $this->recount();
    }

    /**
     *
     */
    public function add_item()
    {

        $this->recount();
    }

    /**
     * @param $uid
     */
    public function remove_item($uid)
    {
        $arr_id = $this->ses_korzina->id;
        foreach ($arr_id as $key => &$value) {
            if ($value['uid'] == $uid) {
                unset($arr_id[$key]);
                break;
            }
        }
        $arr_id = array_values($arr_id);
        $this->ses_korzina->id = $arr_id;

        $this->recount();
    }

    /**
     * Перерасчет показателей
     */
    public function recount()
    {
        $arr_id = $this->ses_korzina->id;
        $this->_subTotal = 0;
        $this->_count = 0;
        $this->_items = array();

        foreach ($arr_id as &$value) {
            $product = $this->getProductByModId($value['id']);
            $product['kol'] = $value['kol'];
            $product['sum'] = $value['kol'] * $product['price'];
            //merge session with database
            $product += $value;
            $this->_items[] = $product;
            $this->_subTotal += $product['sum'];
            $this->_saleTotal += ($product['is_block'] == 1) ? $product['sum'] : 0;
            $this->_count += $value['kol'];
            $this->_margin += $value['margin'];
        }
        $discount_data = $this->getDiscountData();
        $this->_total = $discount_data['big_sum'];
        $this->_discount = ($discount_data['dType'] == 1) ? $discount_data['percent_d'] : $discount_data['percent_o'];
        $this->_discountType = $discount_data['dType'];

    }

    /**
     * Получить общий массив корзины со всеми параметрами
     *
     * @param $recount bool
     *
     * @return array
     */
    public function getFormated($recount = false)
    {
        if ($recount) {
            $this->recount();
        }
        $cart = array(
            'items' => $this->_items,
            'subTotal' => $this->_subTotal,
            'total' => $this->_total,
            'discount' => $this->_discount,
            'kol' => $this->_count,
            'user' => $this->user
        );

        return $cart;
    }

    /**
     * Ролучить объекты корзины
     *
     * @param int $offset
     * @param bool $limit
     *
     * @return array
     */
    public function getItems($offset = 0, $limit = false)
    {

        if ($limit) {
            return array_slice($this->_items, $offset, $limit);
        }
        if ($offset) {
            return array_slice($this->_items, $offset);
        }
        return $this->_items;
    }

    /**
     * @param bool $id
     *
     * @return array
     */
    public function getProductByModId($id = false)
    {
        global $aroma_type;
        $select = $this->db->select();
        //$select->from(array('a'=>'m_catalog_data_order'),array('id','price','type','v','is_block','sklad'))
        $select->from(array('a' => 'm_catalog_data_order'))
            ->joinLeft(array('b' => 'm_catalog_data'), 'a.id_catalog_data=b.id',
                array('aromat_title' => 'title', 'aromat_id' => 'id', 'aromat_img' => 'img', 'pol'))
            ->joinLeft(array('c' => 'm_catalog'), 'b.id_catalog=c.id', array('brand' => 'title', 'brand_id' => 'id'))
            ->where('a.id=?', array($id));

        if (!$row = $this->db->fetchRow($select)){
            return false;
        }

        $title = $row['brand'] . ' ' . $row['aromat_title'] . ' ' . $aroma_type[$row['type']] . ' ' . $row['v'] . ' мл';
        $i = 0;


        $result = array(
            'title' => $title,
            'img_s' => '/images/uploads/catalog/' . $row['aromat_id'] . '/small/' . $row['aromat_img'],
            'bg' => ($i % 2 == 0) ? '1' : '2',
        );

        $result = $row + $result;

        return $result;
    }

    /**
     * @param $id
     */
    public function setIdOrder($id)
    {
        $this->ses_korzina->id_order = $id;
    }

    /**
     * @return mixed
     */
    public function getIdOrder()
    {
        return $this->ses_korzina->id_order;
    }

    /**
     * чистим корзину, оставляем номер заказа
     *
     * @param $id_order
     *
     * @return mixed
     */
    public function step1($id_order)
    {
        $this->clean();
        return $this->id_order = $id_order;
    }

    /**
     * Чистим всю корзину
     */
    public function clean()
    {
        Zend_Session::namespaceUnset('korzina');
        $this->_items = array();
        $this->_count = 0;
        $this->_discountType = '';
        $this->_discount = 0;
        $this->_subTotal = 0;
        $this->_total = 0;
        $this->_saleTotal = 0;
        $this->id_order = 0;
    }
}
