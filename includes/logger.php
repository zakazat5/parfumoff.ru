<?php

/**
 * Модуль логирования
 */
class MagLogger {

    public function get_history($module_name,$pid){
        global $db;
        $select = $db->select();
        $select->from(array('l'=>'m_mag_Log'));
        $select->joinLeft(array('u'=>'base_users'),'l.user_id=u.id',array('user_name'=>'login'));
        $select->where('module_name=?',$module_name);
        $select->where('pid=?',$pid);
        $select->order('l.id DESC');

        $aRows = $db->fetchAll($select);


        foreach($aRows as $row){
            list($action,$text) =$this->getText($module_name,$row);
            $result[] = array(
                'user_name'=>$row['user_name'],
                'date'=>date('Y-m-d',strtotime($row['date'])),
                'time'=>date('H:i:s',strtotime($row['date'])),
                'action'=>$action,
                'text'=>$text
            );
        }

        return $result;
    }

    private function getText($module_name,$row){
        global $db;
        $row['text'] = $row['text'];
        if ($module_name=='mag_orders'){

            switch($row['action']){
                case 'change_kol':
                    $action ='Количесвто';
                    $text = " ({$row['text']}) {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'payment_type':
                    $action ='Тип оплаты';
                    $values = array(1=>'Наличными при получении',2=>'Предоплата по квитацнии',3=>'Предоплата Robokassa');
                    if ($row['value_before']==''){
                        $text = "{$values[$row['value_after']]}";
                    }else{
                        $text = "{$values[$row['value_before']]}->{$values[$row['value_after']]}";
                    }
                    break;
                case 'pComment':
                    $action ='Комментарий';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'DateEnd':
                    $action ='Дата доставки';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'is_send_rect':
                    $action ='Квитацния:';
                    $text = "Отправить квитанцию: {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'track':
                    $action ='Трек номер:';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'track_date':
                    $action ='Трек дата:';
                    $text = "Трек дата {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'delete':
                    $action ='Удален';
                    $text = "{$row['text']}";
                    break;
                case 'add':
                    $action ='Добавлен';
                    $text = "{$row['text']} {$row['value_after']} шт";
                    break;
                case 'logistic_add':
                    $values = array(1=>'Отправлен',2=>'Оплачен',3=>'Деньги получены',4=>'Возврат',5=>'Возврат получен');
                    $action ='Логистика';
                    $text = $values[$row['value_after']];
                    break;
                case 'logistic_delete':
                    $values = array(1=>'Отправлен',2=>'Оплачен',3=>'Деньги получены',4=>'Возврат',5=>'Возврат получен');
                    $action ='Логистика (отмена)';
                    $text = $values[$row['value_before']];
                    break;
                case 'payment_date':
                    $action ='Дата оплаты:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'DateMoneyIncome':
                    $action ='Дата поступления денег';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'TotalMoneyIncome':
                    $action ='Поступление денег:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'delivery_cost_fact':
                    $action ='Стоимость доставки фактич.';
                    $text = "{$row['value_before']}->{$row['value_after']}";
                    break;
                case 'courier_address_id':
                    $action ='Тип доставки';

                    $select = "SELECT couriers_addresses.*, couriers.name AS courier_name FROM couriers_addresses
                        LEFT JOIN couriers ON couriers_addresses.courier_id = couriers.couriersId
                        WHERE couriers_addresses.id = ?";

                    // если ничего не пришло - значит почта россии по умолчанию
                    if ($row == null) {
                        $row['id'] = 0;
                    }

                    if ($row['value_before']==''){
                        $value = $db->fetchRow($select, $row['value_after']);
                        $text = "{$value['courier_name']}";
                        $text = iconv('utf-8','cp1251',$text);
                    }else{
                        if (!$value_before = $db->fetchRow($select, $row['value_before'])){
                            $value_before['courier_name'] = 'Почта России';
                        }else{
                            $value_before['courier_name'] = iconv('utf-8','cp1251',$value_before['courier_name']);
                        }

                        if (!$value_after = $db->fetchRow($select, $row['value_after'])){
                            $value_after['courier_name'] = 'Почта России';
                        }else{
                            $value_after['courier_name'] = iconv('utf-8','cp1251',$value_after['courier_name']);
                        }

                        $text = "{$value_before['courier_name']}->{$value_after['courier_name']}";
                    }
                    break;
                case 'address_region':
                    $action ='Юзер [регион]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_part':
                    $action ='Юзер [столичный район]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_other':
                    $action ='Юзер [регион другой]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_city':
                    $action ='Юзер [город]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_settlement':
                    $action ='Юзер [нас. пункт]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_street':
                    $action ='Юзер [улица]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_house':
                    $action ='Юзер [дом]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_corpus':
                    $action ='Юзер [корпус]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_stroenie':
                    $action ='Юзер [строение]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_floor':
                    $action ='Юзер [корпус]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_flat':
                    $action ='Юзер [корпус]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'address_domophone':
                    $action ='Юзер [корпус]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'postcode':
                    $action ='Юзер [Индекс]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'fio':
                    $action ='Юзер [ФИО]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'email':
                    $action ='Юзер [емэйл]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'phone':
                    $action ='Юзер [телефон]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'pol':
                    $action ='Юзер [пол]:';
                    $text = " {$row['value_before']}->{$row['value_after']}";
                    break;
                case 'status':
                    $action ='Статус заказа:';
                    if ($row['value_after']==1){
                        $text = "Новый";
                    }
                    if ($row['value_after']==2){
                        $text = "Подтвержден";
                    }
                    if ($row['value_after']==3){
                        $text = "Доставлен";
                    }
                    if ($row['value_after']==3){
                        $text = "В обработке";
                    }
                    if ($row['value_after']==4){
                        $text = "На склад";
                    }
                    if ($row['value_after']==0){
                        $text = "Отклонен";
                    }

                    break;
            }
        }

        return array($action,$text);
    }

    static public function add($module_name, $text){

		mysql_query("
			INSERT INTO m_mag_Log
			(
				user_id, 
				user_name, 
				module_name, 
				text, 
				date
			)
			VALUES 
			(
				'{$_SESSION['user_id']}',
				'{$_SESSION['admin_user_login']}',
				'$module_name',
				'$text',
				NOW()
			)
		");
	}
}
?>