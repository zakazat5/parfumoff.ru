<?php
// water mark
function create_watermark ($path) {
	// water Mark
	$config['source_image'] = $path;
	$config['quality'] = '100';
	$config['wm_type'] = 'overlay';
	$config['wm_overlay_path'] = '../images/watermark.gif';
	$config['wm_vrt_alignment'] = 'middle';

	$cl = new CI_Image_lib($config);
	$cl->watermark();
	$cl->clear();
}

// ������ �����������
function upload_image($image_name, $image_name_out, $path_to, $max_width = 0, $max_height = 0){

	list($width, $height) = getimagesize($image_name);
	$x_ratio = @($max_width / $width);
	$y_ratio = @($max_height / $height);

	// calculate dimensions of thumbnail
	if ( ($width <= $max_width) && ($height <= $max_height)) {
		$rev_height =$height;
		$rev_width =$width;
	}
	else if (($x_ratio * $height) < $max_height) {
		$rev_height = ceil($x_ratio * $height);
		$rev_width =$max_width;
	}
	else {
		$rev_width = ceil($y_ratio * $width);
		$rev_height=$max_height;
	}

	try{
		$image=& new image($image_name);


		if ($max_width!=0 && $max_height!=0) {
			$image->resize(round($rev_width),round($rev_height));
			
			$type = strtolower($image->getImageType());
			$image->saveImage($path_to.$image_name_out, $image->getImageData());
			$image->clean();
				
		}else{
			$type = strtolower($image->getImageType());
			move_uploaded_file($image_name, $path_to.$image_name_out.'.'.$type);
		}
		
		
		
		
		return $image_name_out.'.'.$type;
	}
	catch(Exception $e){
		$err =  '������: ' . $e->getMessage() . '<br />' . '������: <pre>' . $e->getTraceAsString() . '</pre>';
		err_message($err);
	}
}
?>