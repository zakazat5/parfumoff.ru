[
    {
        "domain": "msk",
        "name": "������",
        "soname": "������",
        "uname": "������",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (495) 540-47-23<br>8 (800) 333-76-71",
        "address": "��. ������������, �. 48, ���. 2 ",
        "map": "",
		"yametrika": "",
        "mode": "<p></p>",
        "delivery": ""
    },
	{
        "domain": "spb",
        "name": "�����-���������",
        "soname": "�����-����������",
        "uname": "�����-����������",
        "region": "������������� �������",
        "soregion": "������������� �������",
        "phone": "8 (812) 426-11-15",
        "address": "��-� ���� ��������, �. 2",
        "map": "",
		"yametrika": "",
        "mode": "<p></p>",
        "delivery": ""
    },
	{
        "domain": "nov",
        "name": "������� ��������",
        "soname": "������� ���������",
        "uname": "�������� ���������",
        "region": "������������ �������",
        "soregion": "������������ �������",
        "phone": "8 (800) 333-76-71",
        "address": "���. ���������� ��������, �. 22",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09:00 - 18:00</p>",
        "delivery": ""
    },
	{
        "domain": "pskov",
        "name": "�����",
        "soname": "������",
        "uname": "������",
        "region": "��������� �������",
        "soregion": "��������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "���. ���������, �. 1/2",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "smolensk",
        "name": "��������",
        "soname": "���������",
        "uname": "���������",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "���. �������, �. 4",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "bryansk",
        "name": "������",
        "soname": "�������",
        "uname": "�������",
        "region": "�������� �������",
        "soregion": "�������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ����������, �. 10",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00; �� 11.00 - 13.00</p>",
        "delivery": ""
    },
	{
        "domain": "kaluga",
        "name": "������",
        "soname": "������",
        "uname": "������",
        "region": "��������� �������",
        "soregion": "��������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 51, ��. 120",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 19.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "oryol",
        "name": "���",
        "soname": "���",
        "uname": "���",
        "region": "��������� �������",
        "soregion": "��������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. �������, �. 26",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "kursk",
        "name": "�����",
        "soname": "������",
        "uname": "������",
        "region": "������� �������",
        "soregion": "������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������� �����, �. 29�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.30 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "belg",
        "name": "��������",
        "soname": "���������",
        "uname": "���������",
        "region": "������������ �������",
        "soregion": "������������ �������",
        "phone": "8 (800) 333-76-71",
        "address": "�� �����������, �. 132�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "voroneg",
        "name": "�������",
        "soname": "��������",
        "uname": "��������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� ���������, �. 30",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 19.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "lipetsk",
        "name": "������",
        "soname": "�������",
        "uname": "�������",
        "region": "�������� �������",
        "soregion": "�������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� ������, �. 106�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "rostov",
        "name": "������-��-����",
        "soname": "�������-��-����",
        "uname": "�������-��-����",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� �������, �. 15�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 19.00; �� 11.00 - 16.00</p>",
        "delivery": ""
    },
	{
        "domain": "volgograd",
        "name": "���������",
        "soname": "����������",
        "uname": "����������",
        "region": "������������� �������",
        "soregion": "������������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ����������, �. 8�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 9.00 - 18.00; �� 10.00 - 14.00</p>",
        "delivery": ""
    },
	{
        "domain": "krd",
        "name": "���������",
        "soname": "����������",
        "uname": "����������",
        "region": "������������� ����",
        "soregion": "������������� ����",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 151/1",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 20.00</p>",
        "delivery": ""
    },
	{
        "domain": "stavropol",
        "name": "����������",
        "soname": "����������",
        "uname": "����������",
        "region": "�������������� ����",
        "soregion": "�������������� ����",
        "phone": "8 (800) 333-76-71",
        "address": "��-� �. ������, �. 6",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 19.00; �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "nalchik",
        "name": "�������",
        "soname": "��������",
        "uname": "��������",
        "region": "���������-���������� ����������",
        "soregion": "���������-���������� ����������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� �����������, �. 13�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.30 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "vladikavkaz",
        "name": "�����������",
        "soname": "������������",
        "uname": "������������",
        "region": "���������� �������� ������-������",
        "soregion": "���������� �������� ������-������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ���������, �. 36",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.30 - 19.00; �� 10.30 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "makhachkala",
        "name": "���������",
        "soname": "���������",
        "uname": "���������",
        "region": "���������� ��������",
        "soregion": "���������� ��������",
        "phone": "8 (800) 333-76-71",
        "address": "��. �. ��������, �. 75",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.30 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "elista",
        "name": "������",
        "soname": "������",
        "uname": "������",
        "region": "����������  ��������",
        "soregion": "����������  ��������",
        "phone": "8 (800) 333-76-71",
        "address": "��. �. �. �������, �. 5",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 08.00 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "astrakhan",
        "name": "���������",
        "soname": "���������",
        "uname": "���������",
        "region": "������������ �������",
        "soregion": "������������ �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. �����������, �. 8",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 11.00 - 19.00; �� - �� 11.00 - 16.00</p>",
        "delivery": ""
    },
	{
        "domain": "saratov",
        "name": "�������",
        "soname": "��������",
        "uname": "��������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 181",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "tambov",
        "name": "������",
        "soname": "�������",
        "uname": "�������",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������������, �. 172, ��. 45",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 19.00; �� 11.00 - 14.00</p>",
        "delivery": ""
    },
	{
        "domain": "penza",
        "name": "�����",
        "soname": "�����",
        "uname": "�����",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. 5-� �����������, �. 8",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00; �� 09.00 - 13.00</p>",
        "delivery": ""
    },
	{
        "domain": "saransk",
        "name": "�������",
        "soname": "��������",
        "uname": "��������",
        "region": "���������� ��������",
        "soregion": "���������� ��������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ��������������, �. 13",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 11.00 - 20.00</p>",
        "delivery": ""
    },
	{
        "domain": "ulyanovsk",
        "name": "���������",
        "soname": "����������",
        "uname": "����������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "�. ����������, �. 72",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "ryazan",
        "name": "������",
        "soname": "������",
        "uname": "������",
        "region": "��������� �������",
        "soregion": "��������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ��������, �. 59 �",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 08.00 - 20.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "tula",
        "name": "����",
        "soname": "����",
        "uname": "����",
        "region": "�������� �������",
        "soregion": "�������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ��������, �. 14",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "kazan",
        "name": "������",
        "soname": "������",
        "uname": "������",
        "region": "���������� ���������",
        "soregion": "���������� ���������",
        "phone": "8 (800) 333-76-71",
        "address": "��. �������������, �. 50",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "samara",
        "name": "������",
        "soname": "������",
        "uname": "������",
        "region": "��������� �������",
        "soregion": "��������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��.  �������, �. 30",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 22.00</p>",
        "delivery": ""
    },
	{
        "domain": "cheb",
        "name": "���������",
        "soname": "����������",
        "uname": "����������",
        "region": "��������� ����������",
        "soregion": "��������� ����������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� ������,  �. 39, ��. 5",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 08.00 - 20.00; �� 10.00 - 17.00</p>",
        "delivery": ""
    },
	{
        "domain": "yoshkarola",
        "name": "������-���",
        "soname": "������-���",
        "uname": "������-���",
        "region": "���������� ����� ��",
        "soregion": "���������� ����� ��",
        "phone": "8 (800) 333-76-71",
        "address": "��. �������, �. 14",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 08.00 - 20.00; �� - �� 09.00 - 17.00</p>",
        "delivery": ""
    },
	{
        "domain": "novgorod",
        "name": "������ ��������",
        "soname": "������ ���������",
        "uname": "������� ���������",
        "region": "������������� �������",
        "soregion": "������������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ���������� �., �. 56",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 08.00 - 21.00</p>",
        "delivery": ""
    },
	{
        "domain": "vladimir",
        "name": "��������",
        "soname": "���������",
        "uname": "���������",
        "region": "������������ �������",
        "soregion": "������������ �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ����, �. 44, �. 9",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00; �� 10.00 - 17.00</p>",
        "delivery": ""
    },
	{
        "domain": "ivanovo",
        "name": "�������",
        "soname": "�������",
        "uname": "�������",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� ������, �. 2�, ��. 9",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "tver",
        "name": "�����",
        "soname": "�����",
        "uname": "�����",
        "region": "�������� �������",
        "soregion": "�������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "���. ���������, �. 1",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 19.00; �� - �� 10.00 - 17.00</p>",
        "delivery": ""
    },
	{
        "domain": "yaroslavl",
        "name": "���������",
        "soname": "���������",
        "uname": "���������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. �����������, �. 62/30",
        "map": "",
		"yametrika": "",
        "mode": "<p> �� - �� 10.00 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "kostroma",
        "name": "��������",
        "soname": "��������",
        "uname": "��������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ���������, �. 97",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18:00</p>",
        "delivery": ""
    },
	{
        "domain": "vologda",
        "name": "�������",
        "soname": "�������",
        "uname": "�������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ���������,  �. 11",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "kirov",
        "name": "�����",
        "soname": "������",
        "uname": "������",
        "region": "��������� �������",
        "soregion": "��������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ��������, �. 5",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 08.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "igevsk",
        "name": "������",
        "soname": "�������",
        "uname": "�������",
        "region": "���������� ����������",
        "soregion": "���������� ����������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ����� ������, �. 285",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 19.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "ufa",
        "name": "���",
        "soname": "���",
        "uname": "���",
        "region": "���������� ������������",
        "soregion": "���������� �������������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 52/1",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 20.00; �� 10.00 - 17.00</p>",
        "delivery": ""
    },
	{
        "domain": "orenburg",
        "name": "��������",
        "soname": "���������",
        "uname": "���������",
        "region": "������������ �������",
        "soregion": "������������ �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. 9 ������, �. 42",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 20.00; �� 11.00 - 14.00</p>",
        "delivery": ""
    },
	{
        "domain": "chelyabinsk",
        "name": "���������",
        "soname": "����������",
        "uname": "����������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ����������, �. 25",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 21.00</p>",
        "delivery": ""
    },
	{
        "domain": "ektb",
        "name": "������������",
        "soname": "�������������",
        "uname": "�������������",
        "region": "������������ �������",
        "soregion": "������������ �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. 8 �����, �. 8�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 21.00</p>",
        "delivery": ""
    },
	{
        "domain": "kurgan",
        "name": "������",
        "soname": "�������",
        "uname": "�������",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ���������, �. 75",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "tumen",
        "name": "������",
        "soname": "������",
        "uname": "������",
        "region": "��������� �������",
        "soregion": "��������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��.  ���������� �����, �. 165/1",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00; �� 10.00 - 14.00</p>",
        "delivery": ""
    },
	{
        "domain": "perm",
        "name": "�����",
        "soname": "�����",
        "uname": "�����",
        "region": "�������� ����",
        "soregion": "�������� ����",
        "phone": "8 (800) 333-76-71",
        "address": "��. ��������������, �. 163, ��. 104",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 17.30</p>",
        "delivery": ""
    },
	{
        "domain": "omsk",
        "name": "����",
        "soname": "�����",
        "uname": "�����",
        "region": "������ �������",
        "soregion": "������ �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 53",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00; �� 09.00 - 14.00</p>",
        "delivery": ""
    },
	{
        "domain": "khantymansiysk",
        "name": "�����-��������",
        "soname": "�����-���������",
        "uname": "�����-���������",
        "region": "�����-���������� ���������� �����",
        "soregion": "�����-���������� ���������� ������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 8",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 19.00; �� 10.00 - 14.00</p>",
        "delivery": ""
    },
	{
        "domain": "novosibirsk",
        "name": "�����������",
        "soname": "������������",
        "uname": "������������",
        "region": "������������� �������",
        "soregion": "������������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ����� ��������, �. 83",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 20.00</p>",
        "delivery": ""
    },
	{
        "domain": "barnaul",
        "name": "�������",
        "soname": "��������",
        "uname": "��������",
        "region": "��������� ����",
        "soregion": "��������� ����",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������ �������, �. 235�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "kemerovo",
        "name": "��������",
        "soname": "��������",
        "uname": "��������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� ������, �. 64�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 08.00 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "tomsk",
        "name": "�����",
        "soname": "������",
        "uname": "������",
        "region": "������� �������",
        "soregion": "������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ����� ������,  �. 36",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00; C� 12.00 - 14.00</p>",
        "delivery": ""
    },
	{
        "domain": "abakan",
        "name": "������",
        "soname": "�������",
        "uname": "�������",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ��������, �. 2�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00; �� 12.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "krasnoyarsk",
        "name": "����������",
        "soname": "�����������",
        "uname": "�����������",
        "region": "������������ ����",
        "soregion": "������������ ����",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �.26",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 19.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "irkutsk",
        "name": "�������",
        "soname": "��������",
        "uname": "��������",
        "region": "��������� �������",
        "soregion": "��������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������������, �. 71",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 18.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "ulanude",
        "name": "����-���",
        "soname": "����-���",
        "uname": "����-���",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 28�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "chita",
        "name": "����",
        "soname": "����",
        "uname": "����",
        "region": "������������� ����",
        "soregion": "������������� ����",
        "phone": "8 (800) 333-76-71",
        "address": "��. �������������, �. 32",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "birobidzhan",
        "name": "����������",
        "soname": "�����������",
        "uname": "�����������",
        "region": "��������� ���������� �������",
        "soregion": "��������� ���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� 60-����� ����, �. 16",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 08.30 - 19.00</p>",
        "delivery": ""
    },
	{
        "domain": "khabarovsk",
        "name": "���������",
        "soname": "����������",
        "uname": "����������",
        "region": "����������� ����",
        "soregion": "����������� ����",
        "phone": "8 (800) 333-76-71",
        "address": "��. ���������-���������, �. 23/55",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 20.00; �� 14.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "vladivostok",
        "name": "�����������",
        "soname": "������������",
        "uname": "������������",
        "region": "���������� ����",
        "soregion": "���������� ����",
        "phone": "8 (800) 333-76-71",
        "address": "��. �����������, �. 53",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 20.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "yakutsk",
        "name": "������",
        "soname": "�������",
        "uname": "�������",
        "region": "���������� ���� (������)",
        "soregion": "���������� ���� (������)",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������������, �. 35�",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 19.00; �� 10.00 - 15.00</p>",
        "delivery": ""
    },
	{
        "domain": "magadan",
        "name": "�������",
        "soname": "��������",
        "uname": "��������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ���������, �. 5",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 10.00 - 18.00 (���� 14.00 - 15.00)</p>",
        "delivery": ""
    },
	{
        "domain": "syktyvkar",
        "name": "���������",
        "soname": "����������",
        "uname": "����������",
        "region": "���������� ����",
        "soregion": "���������� ����",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 55",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "arkhangelsk",
        "name": "�����������",
        "soname": "������������",
        "uname": "������������",
        "region": "������������� �������",
        "soregion": "������������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� ����������, �. 144",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 20.00; �� 10.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "murmansk",
        "name": "��������",
        "soname": "���������",
        "uname": "���������",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��-� ������, �. 82",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 09.00 - 19.00; �� 10.00 - 17.00</p>",
        "delivery": ""
    },
	{
        "domain": "petrozavodsk",
        "name": "������������",
        "soname": "�������������",
        "uname": "�������������",
        "region": "���������� �������",
        "soregion": "���������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 2",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 12.00 - 19.00; �� - �� 14.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "novokuzneck",
        "name": "�����������",
        "soname": "������������",
        "uname": "������������",
        "region": "����������� �������",
        "soregion": "����������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 2",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 12.00 - 19.00; �� - �� 14.00 - 18.00</p>",
        "delivery": ""
    },
	{
        "domain": "kgd",
        "name": "�����������",
        "soname": "������������",
        "uname": "������������",
        "region": "��������������� �������",
        "soregion": "��������������� �������",
        "phone": "8 (800) 333-76-71",
        "address": "��. ������, �. 2",
        "map": "",
		"yametrika": "",
        "mode": "<p>�� - �� 12.00 - 19.00; �� - �� 14.00 - 18.00</p>",
        "delivery": ""
    }
]