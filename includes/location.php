<?

include_once(application_path.'/includes/geoip/SxGeo.php');


$ses_location = new Zend_Session_Namespace('location');

if (isset($ses_location->location['city_name'])){
    $location['region_name'] =$ses_location->location['region_name'];
    $location['city_name'] = $ses_location->location['city_name'];
}else{
    $ip = $_SERVER['REMOTE_ADDR'];

    //$ip = '195.218.132.254';
    //$ip = '195.190.96.1';

    $SxGeo = new SxGeo(application_path.'/includes/geoip/SxGeoCity.dat', SXGEO_BATCH | SXGEO_MEMORY); // Самый быстрый режим
    $oCity = $SxGeo->getCityFull($ip); // выполняет getCountry либо getCity в зависимости от типа базы



    foreach($oCity as $key=>$value){
        $oGeoip[$key] = $value;
    }

    $location['region_name'] = $oGeoip['region_name'];
    $location['city_name'] = $oGeoip['city_name'];

}




/**
 * get regions from db
 */
$query = "SELECT * FROM m_geo_region ORDER BY region_name";

$aGeoRegions = $db->fetchAll($query);



?>