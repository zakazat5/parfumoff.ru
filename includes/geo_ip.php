<?php
require('DadataGeo.php');
use Dadata\DadataGeo as DadataGeo;
$real_ip = $_SERVER['REMOTE_ADDR'];
$black_list_array = array('66.249.66.56','157.55.39.42','51.255.66.113','66.249.66.59','93.158.152.74','66.249.64.23','66.249.64.138','141.8.142.27','66.249.73.56');
if (!in_array($real_ip, $black_list_array) && !isBot()) {
	session_start();
	if (isset($_SESSION['user_segment_geo']) && isset($_SESSION['user_segment_geo_ip']) && $_SESSION['user_segment_geo_ip'] == $real_ip) {
		$GLOBALS["user_segment_geo"] = $_SESSION['user_segment_geo'];
	} else {
		$url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/detectAddressByIp?ip=' . $real_ip;
		$token = 'a4ad873506c4c01d447a363f53494903f0c19d53';
		$dadata = new DadataGeo($url, $token);
		$resp = json_decode($dadata->clean(),true);
		if (isset($resp['location']['data']['city']) && isset($resp['location']['data']['region'])) {
			if ($resp['location']['data']['city'] == 'Москва' || $resp['location']['data']['region'] == 'Московская') {
				$GLOBALS["user_segment_geo"] = '1';
			} else {
				$GLOBALS["user_segment_geo"] = '2';
			}
			$_SESSION['user_segment_geo'] = $GLOBALS["user_segment_geo"];
			$_SESSION['user_segment_geo_ip'] = $real_ip;
		} else {
			$GLOBALS["user_segment_geo"] = '';
		}
	}
} else {
	$GLOBALS["user_segment_geo"] = '';
}

function isBot() {
$bots = array(
    'rambler','googlebot','aport','yahoo','msnbot','turtle','mail.ru','omsktele',
    'yetibot','picsearch','sape.bot','sape_context','gigabot','snapbot','alexa.com',
    'megadownload.net','askpeter.info','igde.ru','ask.com','qwartabot','yanga.co.uk',
    'scoutjet','similarpages','oozbot','shrinktheweb.com','aboutusbot','followsite.com',
    'dataparksearch','google-sitemaps','appengine-google','feedfetcher-google',
    'liveinternet.ru','xml-sitemaps.com','agama','metadatalabs.com','h1.hrn.ru',
    'googlealert.com','seo-rus.com','yadirectbot','yandeg','yandex',
    'yandexsomething','copyscape.com','adsbot-google','domaintools.com',
    'nigma.ru','bing.com','dotnetdotcom','bingbot','yandexmetrika','yandexbot'
  );
  foreach($bots as $bot)
    if(stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false){
      return true;
    }
  return false;
}
?>