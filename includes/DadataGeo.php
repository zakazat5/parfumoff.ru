<?php
namespace Dadata;
class DadataGeo {
    private $url, $token;
    public function __construct($url, $token) {
        $this->url = $url;
        $this->token = $token;
    }
    public function clean() {
        $options = array(
            'http' => array(
                'method'  => 'GET',
                'header'  => array(
                    'Content-type: application/json',
					'Accept: application/json',
                    'Authorization: Token ' . $this->token
                    )
            ),
        );
        $context = stream_context_create($options);
        $result = file_get_contents($this->url, false, $context);
        return $result;
    }
}
?>