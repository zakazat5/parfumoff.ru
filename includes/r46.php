<?php

	function r46_sync($status, $items) {
		$conv_status = array(
			'1' => 0,
			'2' => 0,
			'3' => 1,
			'0' => 2
		);

		if ($conv_status[$status] > 0) {

			$orders = array();

			foreach ($items as $item) {
				$orders[] = array(
					'id' => $item,
					'status' => $conv_status[$status]
				);
			}

			$json = array(
				'shop_id' => '32f7c3e11cc04298e31945b8ffba6f',
				'shop_secret' => '311ff8725562633f4ea1fd9ad846df12',
				'orders' => $orders
			);

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'http://api.rees46.com/import/sync_orders');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($json));
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			curl_exec($curl);
		}
	}

	function r46_import($orders) {

		$jorders = array();

		foreach ($orders as $order) {

			$jitems = array();

			foreach ($order['items'] as $item) {
				$jitems[] = array(
					'id' => $item['id_catalog_data'],
					'price' => $item['sum'],
					'is_available' => true,
					'amount' => $item['kol']
				);
			}

			$jorders[] = array(
				'id' => $order['id'],
				'user_id' => $order['user']['id'],
				'user_email' => $order['user']['email'],
				'date' => strtotime($order['DateAdd']),
				'items' => $jitems
			);
		}

		$jjorders = array_chunk($jorders, 1000);

		foreach ($jjorders as $jorders) {

			$json = array(
				'shop_id' => '32f7c3e11cc04298e31945b8ffba6f',
				'shop_secret' => '311ff8725562633f4ea1fd9ad846df12',
				'orders' => $jorders
			);

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'http://api.rees46.com/import/orders');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($json));
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			echo curl_exec($curl);
		}
	}

	function r46_audience($users) {

		$jusers = array();

		foreach ($users as $user) {

			$jusers[] = array(
				'id' => $user['id'],
				'email' => $user['email']
			);
		}

		$jjusers = array_chunk($jusers, 1000);

		foreach ($jjusers as $jusers) {

			$json = array(
				'shop_id' => '32f7c3e11cc04298e31945b8ffba6f',
				'shop_secret' => '311ff8725562633f4ea1fd9ad846df12',
				'audience' => $jusers
			);

			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, 'http://api.rees46.com/import/orders');
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($json));
			curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
			echo curl_exec($curl);
		}
	}

?>
