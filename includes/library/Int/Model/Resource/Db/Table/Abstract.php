<?php

abstract class Int_Model_Resource_Db_Table_Abstract extends Zend_Db_Table_Abstract implements Int_Model_Resource_Db_Interface
{

    public function __construct ($config = array())
    {
        parent::__construct($config);

        if ( Zend_Db_Table_Abstract::getDefaultMetadataCache() === null ) {

            $frontendOptions = array(
                'lifetime'                => 604800,
                'automatic_serialization' => true,
                'cache_id_prefix'         => 'waa_',
                'ignore_user_abort'       => true,
                'debug_header'            => false,
                'default_options'         => array(
                    'cache_with_cookie_variables'   => true, //кешировать даже если присутствуют куки
                    'make_id_with_cookie_variables' => false, //в генерации id кеша не использовать куки
                )
            );

            $backendOptions = array( 'cache_dir' => APPLICATION_PATH . '/../data/cache/' );

            $cache = Zend_Cache::factory( 'Core', 'File', $frontendOptions, $backendOptions );
            Zend_Db_Table_Abstract::setDefaultMetadataCache( $cache );
        }
    }

    /**
     * Unexistent methods handler
     *
     * @param string $name
     * @param mixed  $arguments
     * @return bool|\Model_User|null
    */
    public function __call($name, $arguments)
    {
        //handles get by dynamic finder like getByNameAndPasswordOrDate()
        if (strpos($name, 'getBy') === 0) {
            return $this->__getByColumnsFinder(str_replace('getBy', '', $name), $arguments);
        } else {
            return false;
        }
    }

    /**
     * getByColumnsFinder
     *
     * <code>
     *    $this->getByLoginOrPasswordAndEmail(
     *        'vasya',
     *        md5(123456),
     *        'vasya
     * @mail             .ru'
     *    )
     * </code>
     *
     * <code>
     *    //fields like UserLogin => Userlogin
     *    //fields like user_login => User_login
     *    $this->getByUser_loginOrUser_passwordAndUser_email(
     *        'vasya',
     *        md5(123456),
     *        'vasya@mail.ru'
     *    )
     * </code>
     *
     * @param string $query
     * @param array  $values
     * @return null | Model_User
     */
    private function __getByColumnsFinder($query, $values)
    {
        if ($params = $this->__parseQuery($query)) {
            $select = $this->__buildSelect($params, $values);
            return $this->fetchRow($select);
        }
        return null;
    }

    /**
     * Parse query to array
     *
     * @param string $query
     * @return array
     */
    private function __parseQuery($query)
    {
        if (preg_match_all('/[A-Z][^A-Z]+/', $query, $matches)) {
            return array_map('strtolower', $matches['0']);
        }
        return false;
    }

    /**
     * Build Zend_Db_Table_Select object
     *
     * @param array $params
     * @param array $values
     * @return object Zend_Db_Table_Select
     */
    private function __buildSelect($params, $values)
    {
        $select = $this->select();

        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        $fields = array_map('strtolower', $fields);

        $condition = '';

        foreach ($params as $param) {
            if (in_array($param, $fields)) {
                if ($value = array_shift($values)) {
                    if ($value instanceof Zend_Db_Expr) {
                        $value = $value->__toString();
                    }
                    if ($condition == 'or') {
                        $select->orWhere($param . '=?', $value);
                    } else {
                        $select->where($param . '=?', $value);
                    }
                } else {
                    throw new Int_Exception('No value for field ' . $param);
                }
            } elseif (in_array($param, array('or', 'and'))) {
                $condition = $param;
            } else {
                throw new Int_Exception(
                    'No such condition must be OR or ' .
                    'AND, got ' . $param
                );
            }
        }
        return $select;
    }

    /**
     * Save a row to the database
     *
     * @param array             $info The data to insert/update
     * @param Zend_DB_Table_Row $row Optional The row to use
     * @return mixed The primary key
     */
    public function saveRow ( $info, $row = null ) {
        if ( null === $row ) {
            $row = $this->createRow();
        }

        $columns = $this->info( 'cols' );
        foreach ( $columns as $column ) {
            if ( array_key_exists( $column, $info ) ) {
                $row->$column = $info[$column];
            }
        }
        return $row->save();
    }

}
