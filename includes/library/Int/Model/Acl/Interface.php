<?php
 
interface Int_Model_Acl_Interface {
    public function setIdentity($identity);

    public function getIdentity();

    public function checkAcl($action);

    public function setAcl(Int_Acl_Interface $acl);

    public function getAcl();
}
