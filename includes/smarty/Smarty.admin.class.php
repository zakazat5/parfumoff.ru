<?php
// загружаем библиотеку Smarty
require('Smarty.class.php');

class Smarty_Admin extends Smarty {

	function Smarty_Admin($path){
		$this->Smarty();

		$this->template_dir = $path . '/smarty/templates/';
		$this->compile_dir  = $path . '/smarty/compile_dir/';
		$this->config_dir   = $path . '/smarty/configs/';
		$this->cache_dir    = $path . '/smarty/cache/';

		$this->caching = false;
		$this->force_compile = true;
		$this->assign('app_name', 'Adminka');
	}
}
?>