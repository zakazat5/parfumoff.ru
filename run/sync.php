<?php
/**
 * Created by PhpStorm.
 * User: truth4oll
 * Date: 03.12.13
 * Time: 14:20
 */
$start_time = microtime(true);

error_reporting(E_ALL);
ini_set('display_errors',1);
$base_path = __DIR__;


define('application_path',$_SERVER['DOCUMENT_ROOT']);

function to_1251($str) {
    //$str = html_entity_decode($str, null, 'WINDOWS-1252');
    return $str;
}

//composer
require(__DIR__ . '/../vendor/autoload.php');

include_once($base_path.'/../includes/config.php');
include_once($base_path.'/../includes/db.php');
include_once($base_path.'/../includes/function.php');
include_once($base_path.'/../includes/valid.php');
include_once($base_path.'/../includes_auto/mag_param.php');
include_once($base_path.'/../includes/class/measurement.class.php');

connect_db();



$db_config['dbname'] = 'parfumoff_new';

set_time_limit(0);

mysql_query("SET NAMES utf8");

// ИНИЦИАЛИЗИРЕМ СОЕДИНЕНИЕ БД
$db = Zend_Db::factory('Pdo_Mysql', $db_config);
$db->setFetchMode(Zend_Db::FETCH_ASSOC);
$db->query("SET NAMES 'utf8' ");



if ($file = file_get_contents('http://enigme.ru/run/sync.php')) {
    if (file_put_contents('sync.xml', $file)) {
        //обновляем флаги синхронизации

        $doc = new DOMDocument();
        if ($doc->load('sync.xml')) {


            $sql = "UPDATE m_catalog_data_order SET sync=0";
            mysql_query($sql);


            $data = array();
            $brands = $doc->getElementsByTagName('brand');

            //бренды
            foreach ($brands as $item) {
                $data['id'] = $item->getAttribute('id');

                $data['articul'] = $item->getAttribute('articul');
                $data['title'] = $item->getAttribute('title');
                $data['title_1'] = $item->getAttribute('title_1');
                $data['title_2'] = $item->getAttribute('title_2');
                $data['url'] = $item->getAttribute('url');
                $data['date_edit'] = $item->getAttribute('date_edit');

                if (syncBrend($data)) {}
            }
            $data = array();
            $products = $doc->getElementsByTagName('product');


            echo "production\n";
//production
            foreach ($products as $product) {
                $data['catalog_articul'] = $product->getAttribute('catalog_articul');
                $data['title'] = $product->getAttribute('title');
                $data['url'] = $product->getAttribute('url');
                $data['articul'] = $product->getAttribute('articul');
                $data['pol'] = $product->getAttribute('pol');
                $data['img'] = $product->getAttribute('img');
                $data['img_update'] = $product->getAttribute('img_update');
                $data['img_path'] = $product->getAttribute('img_path');
                //if product is isset, then sync modifications
                if ($product_id = syncProduct($data)) {
                    foreach ($product->getElementsByTagName('mod') as $mod) {
                        $data_mod['articul'] = $mod->getAttribute('articul');
                        $data_mod['id_catalog_data'] = $product_id;
                        $data_mod['article'] = $mod->getAttribute('article');
                        $data_mod['article2'] = $mod->getAttribute('article2');
                        $data_mod['article3'] = $mod->getAttribute('article3');
                        $data_mod['article4'] = $mod->getAttribute('article4');
                        $data_mod['article5'] = $mod->getAttribute('article5');
                        $data_mod['article6'] = $mod->getAttribute('article6');
                        $data_mod['article7'] = $mod->getAttribute('article7');
                        $data_mod['article8'] = $mod->getAttribute('article8');
                        $data_mod['article9'] = $mod->getAttribute('article9');
                        $data_mod['article10'] = $mod->getAttribute('article10');
                        $data_mod['article11'] = $mod->getAttribute('article11');
                        $data_mod['article12'] = $mod->getAttribute('article12');
                        $data_mod['article13'] = $mod->getAttribute('article13');
                        $data_mod['article14'] = $mod->getAttribute('article14');
                        $data_mod['article15'] = $mod->getAttribute('article15');
                        $data_mod['article16'] = $mod->getAttribute('article16');
                        $data_mod['article17'] = $mod->getAttribute('article17');
                        $data_mod['article18'] = $mod->getAttribute('article18');
                        $data_mod['price_usd1'] = $mod->getAttribute('price_usd1');
                        $data_mod['price_usd2'] = $mod->getAttribute('price_usd2');
                        $data_mod['price_usd3'] = $mod->getAttribute('price_usd3');
                        $data_mod['price_usd4'] = $mod->getAttribute('price_usd4');
                        $data_mod['price_usd5'] = $mod->getAttribute('price_usd5');
                        $data_mod['price_usd6'] = $mod->getAttribute('price_usd6');
                        $data_mod['price_usd7'] = $mod->getAttribute('price_usd7');
                        $data_mod['price_usd8'] = $mod->getAttribute('price_usd8');
                        $data_mod['price_usd9'] = $mod->getAttribute('price_usd9');
                        $data_mod['price_usd10'] = $mod->getAttribute('price_usd10');
                        $data_mod['price_usd11'] = $mod->getAttribute('price_usd11');
                        $data_mod['price_usd12'] = $mod->getAttribute('price_usd12');
                        $data_mod['price_usd13'] = $mod->getAttribute('price_usd13');
                        $data_mod['price_usd14'] = $mod->getAttribute('price_usd14');
                        $data_mod['price_usd15'] = $mod->getAttribute('price_usd15');
                        $data_mod['price_usd16'] = $mod->getAttribute('price_usd16');
                        $data_mod['price_usd17'] = $mod->getAttribute('price_usd17');
                        $data_mod['price_usd18'] = $mod->getAttribute('price_usd18');
                        $data_mod['type'] = $mod->getAttribute('type');
                        $data_mod['v'] = $mod->getAttribute('v');
                        if (syncMod($data_mod)) {
                            echo 'sync<br>';
                        }
                    }
                }
            }
            // Удаляем необновленные модификации
            $sql = "DELETE FROM  m_catalog_data_order where `sync`=0 and sklad>0";
            mysql_query($sql);



            $query = mysql_query("SELECT * FROM m_catalog_data_order ");
            while ($content = mysql_fetch_array($query)) {
                if ($content['freeze']==1) continue;

                $id_order = $content['id'];
                $price_usd1 = $content['price_usd1'];
                $price_usd2 = $content['price_usd2'];
                $price_usd3 = $content['price_usd3'];
                $price_usd4 = $content['price_usd4'];
                $price_usd5 = $content['price_usd5'];
                $price_usd6 = $content['price_usd6'];
                $price_usd7 = $content['price_usd7'];
                $price_usd8 = $content['price_usd8'];
                $price_usd9 = $content['price_usd9'];
                $price_usd10 = $content['price_usd10'];
                $price_usd11 = $content['price_usd11'];
                $price_usd12 = $content['price_usd12'];
                $price_usd13 = $content['price_usd13'];
                $price_usd14 = $content['price_usd14'];
                $price_usd15 = $content['price_usd15'];
                $price_usd16 = $content['price_usd16'];
                $price_usd17 = $content['price_usd17'];
                $price_usd18 = $content['price_usd18'];

                // расчитываем приоритет
                $priority = catalog_priority(
                    $price_usd1,
                    $price_usd2,
                    $price_usd3,
                    $price_usd4,
                    $price_usd5,
                    $price_usd6,
                    $price_usd7,
                    $price_usd8,
                    $price_usd9,
                    $price_usd10,
                    $price_usd11,
                    $price_usd12,
                    $price_usd13,
                    $price_usd14,
                    $price_usd15,
                    $price_usd16,
                    $price_usd17,
                    $price_usd18
                );
                $price = catalog_creat_price($priority['price']);
                $sklad = $priority['sklad'];


                 mysql_query("UPDATE m_catalog_data_order SET price='$price',sklad='$sklad' WHERE id='$id_order' ");




            }
            $q_data = mysql_query("SELECT id FROM m_catalog_data ");
            while ($c_data = mysql_fetch_array($q_data)) {

                $id_catalog_data = $c_data['id'];
                $price = mysql_fetch_array(
                    mysql_query(
                        "SELECT MAX(price) as max_price, MIN(price) as min_price FROM m_catalog_data_order WHERE id_catalog_data='$id_catalog_data' AND price!=0 "
                    )
                );

                $price_s = $price['min_price'];
                $price_e = $price['max_price'];

                mysql_query(
                    "UPDATE m_catalog_data SET price_s='$price_s', price_e='$price_e' WHERE id='$id_catalog_data' "
                );
            }




            $exec_time = microtime(true) - $start_time;

            echo 'sync_ended';
        }

        waitlist();

        //unlink('sync.xml');
    }


    return false;
}

function syncBrend($data)
{
    $sql = "SELECT id,articul,title,url,date_edit FROM m_catalog WHERE `articul`= '{$data['articul']}'  ";
    if ($result = mysql_fetch_assoc(mysql_query($sql))) {
        return true;
    } else {
        //добавляем новый
        foreach ($data as $key => $item) {
            $data[$key] = mysql_real_escape_string($item);
        }
        $sql = "INSERT INTO m_catalog (articul,title,title_1,title_2,url) VALUES ('{$data['articul']}','{$data['title']}','{$data['title_1']}','{$data['title_2']}','{$data['url']}')";
        mysql_query($sql);
    };
    return false;
}


function syncProduct($data)
{
    $sql = "SELECT id,articul,id_catalog,title,url,date_edit,img,img_update,pol FROM m_catalog_data WHERE `articul`= '{$data['articul']}'  ";
    //продукт существует->
    if ($result = mysql_fetch_assoc(mysql_query($sql))) {
        //todo image update
        //проверяем картинку
        $small_dir = $_SERVER['DOCUMENT_ROOT'] . '/images/uploads/catalog/' . $result['id'] . '/small/';
        $trumbs_dir = $_SERVER['DOCUMENT_ROOT'] . '/images/uploads/catalog/' . $result['id'] . '/trumbs/';

        if ($data['img_update'] != '') {
            if ($result['img_update'] != $data['img_update']) {
                if ($img = file_get_contents($data['img_path'])) {
                    //big image
                    file_put_contents($small_dir . $data['img'], $img);
                    //small image
                    $data['img_path'] = str_replace('/big/', '/small/', $data['img_path']);
                    $img = file_get_contents($data['img_path']);
                    file_put_contents($trumbs_dir . $data['img'], $img);

                    $sql = "UPDATE  m_catalog_data SET img='{$data['img']}', img_update='{$data['img_update']}' WHERE id={$result['id']}";
                    mysql_query($sql);
                }
            }
        }

        $data['title'] = html_entity_decode($data['title']);

        //обновляем данные
        global $db;

        //проверяем принадлежность бренда
        if ($brand = $db->fetchRow($db->select()->from('m_catalog')->where('articul=?',$data['catalog_articul']))){

        }


        if ($result['title'] != $data['title'] || $result['pol'] != $data['pol'] || $brand['id']!=$result['id_catalog']){
            $db->update('m_catalog_data',['pol'=>$data['pol'],'title'=>$data['title'],'id_catalog'=>$brand['id']],'id='.$result['id']);
        }



        /*//обновляем title
        if ($result['title'] != $data['title']) {
            $data['title'] = mysql_real_escape_string($data['title']);
            $sql = ("UPDATE  m_catalog_data SET title='{$data['title']}' WHERE id={$result['id']}");
            mysql_query($sql);
            print_r(mysql_error());
        }

        if ($result['pol'] != $data['pol']) {
            $data['pol'] = mysql_real_escape_string($data['pol']);
            $sql = ("UPDATE  m_catalog_data SET pol='{$data['pol']}' WHERE id={$result['id']}");
            mysql_query($sql);
            print_r(mysql_error());
        }*/


        return $result['id'];
    } else {
        //add new aromat
        $data['id_cat'] = 82; //parfum
        $id_brend = mysql_result(
            mysql_query("SELECT id FROM m_catalog WHERE articul='{$data['catalog_articul']}'"),
            0,
            0
        );
        $data['id_catalog'] = ($id_brend) ? $id_brend : 0; //parfum


        foreach ($data as $key => $item) {
            $data[$key] = mysql_real_escape_string($item);
        }
        //add
        $sql = "INSERT INTO m_catalog_data (id_cat,id_catalog, articul,title,url,pol) VALUES (
            {$data['id_cat']},
            {$data['id_catalog']},
            '{$data['articul']}',
            '{$data['title']}',
            '{$data['url']}',
            '{$data['pol']}'
        )";
        mysql_query($sql);

        $id = DGetLast();
        $dir = $_SERVER['DOCUMENT_ROOT'] . '/images/uploads/catalog/' . $id;
        $small_dir = $_SERVER['DOCUMENT_ROOT'] . '/images/uploads/catalog/' . $id . '/small/';
        $trumbs_dir = $_SERVER['DOCUMENT_ROOT'] . '/images/uploads/catalog/' . $id . '/trumbs/';

        mkdir($dir);
        mkdir($small_dir);
        mkdir($trumbs_dir);

        if ($img = file_get_contents($data['img_path'])) {
            //big image

            if (file_put_contents($small_dir . $data['img'], $img)) {
                $sql = "UPDATE  m_catalog_data SET img='{$data['img']}' WHERE id=$id";
                mysql_query($sql);
            };

            //small image
            $data['img_path'] = str_replace('/big/', '/small/', $data['img_path']);
            $img = file_get_contents($data['img_path']);
            file_put_contents($trumbs_dir . $data['img'], $img);
        }
        return $id;
    };
    return false;
}

function syncMod($data)
{
    //ищем модификацию
    $sql = "SELECT * FROM m_catalog_data_order WHERE articul='{$data['articul']}'";
    mysql_query($sql);

    if ($result = mysql_fetch_assoc(mysql_query($sql))) {

        //обновляем данные
        $sql = "UPDATE  m_catalog_data_order SET
             article = '{$data['article']}',
             id_catalog_data = '{$data['id_catalog_data']}',
             article2 = '{$data['article2']}',
             article3 = '{$data['article3']}',
             article4 = '{$data['article4']}',
             article5 = '{$data['article5']}',
             article6 = '{$data['article6']}',
             article7 = '{$data['article7']}',
             article8 = '{$data['article8']}',
             article9 = '{$data['article9']}',
             article10 = '{$data['article10']}',
             article11 = '{$data['article11']}',
             article12 = '{$data['article12']}',
             article13 = '{$data['article13']}',
             article14 = '{$data['article14']}',
             article15 = '{$data['article15']}',
             article16 = '{$data['article16']}',
             article17 = '{$data['article17']}',
             article18 = '{$data['article18']}',
             price_usd1 = '{$data['price_usd1']}',
             price_usd2 = '{$data['price_usd2']}',
             price_usd3 = '{$data['price_usd3']}',
             price_usd4 = '{$data['price_usd4']}',
             price_usd5 = '{$data['price_usd5']}',
             price_usd6 = '{$data['price_usd6']}',
             price_usd7 = '{$data['price_usd7']}',
             price_usd8 = '{$data['price_usd8']}',
             price_usd9 = '{$data['price_usd9']}',
             price_usd10 = '{$data['price_usd10']}',
             price_usd11 = '{$data['price_usd11']}',
             price_usd12 = '{$data['price_usd12']}',
             price_usd13 = '{$data['price_usd13']}',
             price_usd14 = '{$data['price_usd14']}',
             price_usd15 = '{$data['price_usd15']}',
             price_usd16 = '{$data['price_usd16']}',
             price_usd17 = '{$data['price_usd17']}',
             price_usd18 = '{$data['price_usd18']}',
             type = '{$data['type']}',
             v = '{$data['v']}',
             sync = 1
             WHERE articul='{$data['articul']}'
             ";
        mysql_query($sql);

    } else {
        //add modification
        $sql = "
         INSERT INTO m_catalog_data_order (
          articul,
          id_catalog_data,
          article,
          article2,
          article3,
          article4,
          article5,
          article6,
          article7,
          article8,
          article9,
          article10,
          article11,
          article12,
          article13,
          article14,
          article15,
          article16,
          article17,
          article18,
          price_usd1,
          price_usd2,
          price_usd3,
          price_usd4,
          price_usd5,
          price_usd6,
          price_usd7,
          price_usd8,
          price_usd9,
          price_usd10,
          price_usd11,
          price_usd12,
          price_usd13,
          price_usd14,
          price_usd15,
          price_usd16,
          price_usd17,
          price_usd18,
          type,
          v,
          sync
          )VALUES (
          '{$data['articul']}',
          '{$data['id_catalog_data']}',
          '{$data['article']}',
          '{$data['article2']}',
          '{$data['article3']}',
          '{$data['article4']}',
          '{$data['article5']}',
          '{$data['article6']}',
          '{$data['article7']}',
          '{$data['article8']}',
          '{$data['article9']}',
          '{$data['article10']}',
          '{$data['article11']}',
          '{$data['article12']}',
          '{$data['article13']}',
          '{$data['article14']}',
          '{$data['article15']}',
          '{$data['article16']}',
          '{$data['article17']}',
          '{$data['article18']}',
          '{$data['price_usd1']}',
          '{$data['price_usd2']}',
          '{$data['price_usd3']}',
          '{$data['price_usd4']}',
          '{$data['price_usd5']}',
          '{$data['price_usd6']}',
          '{$data['price_usd7']}',
          '{$data['price_usd8']}',
          '{$data['price_usd9']}',
          '{$data['price_usd10']}',
          '{$data['price_usd11']}',
          '{$data['price_usd12']}',
          '{$data['price_usd13']}',
          '{$data['price_usd14']}',
          '{$data['price_usd15']}',
          '{$data['price_usd16']}',
          '{$data['price_usd17']}',
          '{$data['price_usd18']}',
          '{$data['type']}',
          '{$data['v']}',
          1)";
        mysql_query($sql);


    }


}

function waitlist(){
    global $db;
    //рассылка
    $aItems = $db->fetchAll($db->select()->from(['a'=>'m_mag_Waitlist'],['a.*','b.price'])
            ->joinLeft(['b'=>'m_catalog_data_order'],'a.articul=b.articul','')
            ->where('mail_sended=0')->where('b.price>0'));
    foreach ($aItems as $item) {
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml('Появился новый аромат');
        $mail->setFrom('office@parfumoff.ru', 'parfumoff');
        $mail->addTo( $item['email'] );
        $mail->setSubject("parfumoff - появился аромат, который вы долго ждали");
        $mail->send();
        $db->update('m_mag_Waitlist',['mail_sended'=>1],'id='.$item['id']);
    }
}

