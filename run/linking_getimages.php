<?php
/**
 * Created by PhpStorm.
 * User: truth4oll
 * Date: 03.12.13
 * Time: 14:20
 */

set_time_limit(300);

ini_set('include_path', ini_get('include_path') . ':../includes/library');
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();
include_once('../includes/config.php');
include_once('../includes/output.php');
include_once('../includes/csv.php');

$db_config['charset'] = 'cp1251';

$db = Zend_Db::factory('MySqli', $db_config);
$db->setFetchMode(Zend_Db::FETCH_ASSOC);

$links = $db->fetchAll($db->select()
    ->from('m_linking3')
    ->where('preview is NULL')->orWhere('preview=""'));
//->limit(10000));
foreach ($links as $link) {
    echo $image =  getPreview($link);
    echo "\n";
    $data['preview'] = $image;
    $db->update('m_linking3',$data  ,'id='.(int)$link['id']);

}


function getPreview($link)
{

    $html = file_get_contents($link['to']);

    libxml_use_internal_errors(true); // Yeah if you are so worried about using @ with warnings
    $doc = new DomDocument();
    $doc->loadHTML($html);
    $xpath = new DOMXPath($doc);
    $query = '//*/meta[starts-with(@property, \'og:\')]';
    $metas = $xpath->query($query);
    foreach ($metas as $meta) {
        $property = $meta->getAttribute('property');
        $content = $meta->getAttribute('content');
        $rmetas[$property] = $content;
    }


    if (isset($rmetas['og:image'])){
        $rmetas['og:image'] = str_replace('/big/','/small/',$rmetas['og:image']);
        return $rmetas['og:image'];
    }else{
        return '';
    }

    //return $this->hasOne(Files::className(), ['id' => 'file_id']);


}