<?php
/**
 * Created by PhpStorm.
 * User: truth4oll
 * Date: 03.12.13
 * Time: 14:20
 */
$start_time = microtime(true);


error_reporting(E_ALL);
ini_set("display_errors", 1);

ini_set('include_path', ini_get('include_path') . ':../includes/library');
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();
include_once('../includes/config.php');
include_once('../includes/db.php');
include_once('../includes_auto/mag_param.php');
include_once('../includes/cart.php');
include_once('../includes/function.php');

//$db_config['dbname'] = 'parfumoff.ru_test';

connect_db();

set_time_limit(0);

mysql_query("SET NAMES utf8");

// ИНИЦИАЛИЗИРЕМ СОЕДИНЕНИЕ БД
$db = Zend_Db::factory('Pdo_Mysql', $db_config);
$db->setFetchMode(Zend_Db::FETCH_ASSOC);
$db->query("SET NAMES 'utf8' ");


//очищаем
$db->query('TRUNCATE m_catalog');
$db->query('TRUNCATE m_catalog_data');
$db->query('TRUNCATE m_catalog_data_order');
$db->query('TRUNCATE m_catalog_history');
$db->query('TRUNCATE m_catalog_moder');
$db->query('TRUNCATE m_linking');
$db->query('TRUNCATE m_mag_Folders');
$db->query('TRUNCATE m_mag_Log');
$db->query('TRUNCATE m_mag_Orders');
$db->query('TRUNCATE m_mag_OrdersSum');
$db->query('TRUNCATE m_mag_Users');


//бренды
$db->query('INSERT INTO `parfumoff_new`.`m_catalog`(`id`, `id_cat`, `articul`, `title`, `title_1`, `title_2`, `anot`, `url`, `sort`, `is_block`, `meta_title`, `meta_description`, `date_edit`)
                                             SELECT `id`, 82,       `articul`, `title`, `title_1`, `title_2`, `anot`, `url`, `sort`, `is_block`, `meta_title`, `meta_description`, `date_edit` FROM `parfumoff`.`m_catalog_brend`');


//забираем ароматы
$db->query('INSERT INTO `parfumoff_new`.`m_catalog_data`(`id`, `id_cat`, `id_catalog`, `articul`,`title`,`text`,`img`,`img_update`, `alt`,`registerDate`,`is_block`,`is_sale`,`is_holiday`,`is_holiday_23`, `is_holiday_8`,
    `sort`, `url`, `tag`, `tagl`, `pol`, `price_s`, `price_e`,  `meta_title`,  `meta_title_1`,  `meta_description`,  `date_edit`)
    SELECT `id`, `id_cat`, `id_brend`, `articul`, `title`,  `text`, `img`, `img_update`, `alt`, `registerDate`, `is_block`, `is_sales`,  `is_holiday`, `is_holiday_23`, `is_holiday_8`,
      `sort`,`url`,`p_tag`, `p_tagl`, `pol`, `price_s`, `price_e`, `meta_title`, `meta_title_1`, `meta_description`, `date_edit` FROM `parfumoff`.`m_catalog_data`
    ');

//забираем модификации

$db->query('INSERT INTO `parfumoff_new`.`m_catalog_data_order`(`id`, `id_catalog_data`, `articul`, `article`, `article2`, `article3`, `article4`, `article5`, `article6`, `article7`, `article8`, `article9`, `article10`, `article11`, `article12`, `article13`, `article14`, `article15`, `article16`, `article17`, `article18`, `type`, `v`, `price`, `price_usd1`, `price_usd2`, `price_usd3`, `price_usd4`, `price_usd5`, `price_usd6`, `price_usd7`, `price_usd8`, `price_usd9`, `price_usd10`, `price_usd11`, `price_usd12`, `price_usd13`, `price_usd14`, `price_usd15`, `price_usd16`, `price_usd17`, `price_usd18`, `is_block`, `sklad`, `sync`)
                                                        SELECT `id`, `id_catalog_data`, `articul`, `article`, `article2`, `article3`, `article4`, `article5`, `article6`, `article7`, `article8`, `article9`, `article10`, `article11`, `article12`, `article13`, `article14`, `article15`, `article16`, `article17`, `article18`, `type`, `v`, `price`, `price_usd1`, `price_usd2`, `price_usd3`, `price_usd4`, `price_usd5`, `price_usd6`, `price_usd7`, `price_usd8`, `price_usd9`, `price_usd10`, `price_usd11`, `price_usd12`, `price_usd13`, `price_usd14`, `price_usd15`, `price_usd16`, `price_usd17`, `price_usd18`, `is_block`, `sklad`, `sync` FROM `parfumoff`.`m_catalog_data_order`
');
//s100

$db->query('UPDATE `m_catalog_data_order` SET  s100=((price/v)*100)');

//папки
$db->query("INSERT INTO `parfumoff_new`.`m_mag_Folders` SELECT * FROM `parfumoff`.`m_mag_Folders`");

//заказы
$db->query('INSERT INTO `parfumoff_new`.`m_mag_Orders`(`id`, `id_user`, `status`, `is_sale`, `folder_id`, `DateSklad`, `DateAdd`, `DateEdit`, `DateEnd`, `DateClose`, `DateSendRect`, `DateSendPost`, `is_send_rect`, `is_send_post`, `dType`, `dPercent`, `dSum`, `Sum`, `pPostNumber`, `pComment`, `pCommentSklad`, `pFio`, `pEmail`, `pPhone`, `pAddress`, `pRegion`, `id_manager`, `is_phone`, `cancel_cause_id`, `cancel_cause`) SELECT `id`, `id_user`, `status`, `is_sale`, `folder_id`, `DateSklad`, `DateAdd`, `DateEdit`, `DateEnd`, `DateClose`, `DateSendRect`, `DateSendPost`, `is_send_rect`, `is_send_post`, `dType`, `dPercent`, `dSum`, `Sum`, `pPostNumber`, `pComment`, `pCommentSklad`, `pFio`, `pEmail`, `pPhone`, `pAddress`, `pRegion`, `id_manager`, `is_phone`, `cancel_cause_id`, `cancel_cause` FROM `parfumoff`.`m_mag_Orders`');

//доп поля заказов
$db->query('INSERT INTO `parfumoff_new`.`m_mag_OrdersSum`(`id`, `id_order`, `id_user`, `id_brend`, `id_catalog_data`, `id_catalog_data_order`, `articul_catalog_data_order`, `sklad`, `price_usd`, `margin`, `title`, `sum`, `gmargin`, `kol`, `is_sale`) SELECT `id`, `id_order`, `id_user`, `id_brend`, `id_catalog_data`, `id_catalog_data_order`, `articul_catalog_data_order`, `sklad`, `price_usd`, `margin`, `title`, `sum`, `gmargin`, `kol`, `is_sale` FROM `parfumoff`.`m_mag_OrdersSum`');


//users
$db->query('INSERT INTO `parfumoff_new`.`m_mag_Users`(`id`, `fio`, `email`, `passw`, `phone`, `region`, `address`, `city`, `postcode`, `date_register`, `date_last_visit`, `date_birthday`, `pol`, `is_block`, `is_maillist`, `note`, `d_percent`, `d_sum_all`, `d_sum_eco`, `d_virtual`) SELECT `id`, `fio`, `email`, `passw`, `phone`, `region`, `address`, `city`, `postcode`, `date_register`, `date_last_visit`, `date_birthday`, `pol`, `is_block`, `is_maillist`, `note`, `d_percent`, `d_sum_all`, `d_sum_eco`, `d_virtual` FROM `parfumoff`.`m_mag_Users`');
