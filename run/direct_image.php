<?php

error_reporting(E_ALL);
ini_set('display_errors',1);
ini_set('include_path',ini_get('include_path').':../includes/library');
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();
include_once ('../includes/config.php');
include_once ('../includes/output.php');
include_once ('../includes/csv.php');
include_once ('../includes/Images/Lib.php');

$db = Zend_Db::factory('MySqli', $db_config);
$db->setFetchMode(Zend_Db::FETCH_ASSOC);

$select = $db->select();
$select->from('m_catalog_data');
$select->where('skip=2');
$select->where("img!=''");
//$select->where("id=27");
$select->limit(1000);

$rows = $db->fetchAll($select);


foreach($rows as $row){
    $filename = $row['img'];

    $db->query("UPDATE m_catalog_data set skip=3 WHERE id={$row['id']}");

    $full_path  = $_SERVER['DOCUMENT_ROOT']."/images/uploads/catalog/{$row['id']}/big/".$filename;

    if (file_exists($full_path)){
        $image = new Int_Images_Lib($full_path);

        $image -> setTransparency(false);
        $width = $image->getWidth();
        $height = $image->getHeight();

        if ($width!=$height){
            if ($width>$height){
                $height = $width;
            }else{
                $width = $height;
            }
        }

        $image->resizeImage(150,150,'auto');
        $image->saveImage($_SERVER['DOCUMENT_ROOT'].'/images/direct/'.$filename);
        $image = new Int_Images_Lib($_SERVER['DOCUMENT_ROOT'].'/images/direct/'.$filename);
        $image->cropImage(150,150);
        $image->saveImage($_SERVER['DOCUMENT_ROOT'].'/images/direct/'.$filename);

        $db->query("UPDATE m_catalog_data set skip=1 WHERE id={$row['id']}");
    }else{
        //$db->query("UPDATE m_catalog_data set skip=2 WHERE id={$row['id']}");
    }



    //echo "<img src='/images/direct/{$filename}'>";





}



?>