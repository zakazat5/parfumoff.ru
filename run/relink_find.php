<?php die();

ini_set('include_path',ini_get('include_path').':../includes/library');
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();
include_once ('../includes/config.php');
include_once ('../includes/output.php');
include_once ('../includes/csv.php');

$db = Zend_Db::factory('MySqli', $db_config);
$db->setFetchMode(Zend_Db::FETCH_ASSOC);

$brends = $db->fetchAll("SELECT * FROM m_catalog ");
foreach ($brends as $brend) {
	$ar = $db->fetchAll("SELECT * FROM m_catalog_data WHERE id_catalog='{$brend['id']}' ");
	
	foreach ($ar as $a) {
		
		$id = $a['id'];
		
		$title_brend 	= format_text_out($brend['title']);
		$title_brend_ru = format_text_out($brend['title_1']);
		
		$title_ar 		= format_text_out($a['title']);
		$title_ar_ru	= format_text_out($a['title_1']);
		
		if($title_brend_ru=='') $title_brend_ru = $title_brend;
		if($title_ar_ru=='') $title_ar_ru = $title_ar;
		

		$find = "$title_brend $title_ar";
		$find_ru = "$title_brend_ru $title_ar_ru";
		
		$db->update('m_catalog_data', 
		array(
			'find' => $find,
			'find_ru' => $find_ru,
		), 
		"id={$id} "
		);
	}
	
	
}