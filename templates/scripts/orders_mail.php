<?php require_once(__DIR__ . '/../../includes/geo_domain.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="title" content="" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
</head>

<body>

<div id="wrapper">
    <div id="header">
    </div><!-- #header-->


    <div id="content">

        <div style="width:600px">

            <table>
                <tr>
                    <td valign="bottom" align="left">
                        <h2 style="font: 18px/18px Tahoma;font-weight:normal">Здравствуйте, <?php echo $this->data['fio'];?>!</h2>
                        <font color="#b079df"><b style="font: 12px/18px Tahoma;font-weight:bold;color:#b079df;">
                                Вы оформили заказ №<?php echo $this->data['unic'];?>
                            </b></font>

                    </td>
                </tr>
            </table>
            <br>

            <table border="0" cellpadding="3" cellspacing="0">

                <?php foreach ($this->data['orders'] as $orders) {?>

                    <tr>
                        <td style="font: 12px/18px Tahoma" width="400" valign="top" align="left">
                            <?php echo geo_replace_utf8($city_utf8, $orders['title']);?>
                        </td>
                        <td style="font: 12px/18px Tahoma" width="50" valign="top" align="center">
                            <?php echo $orders['kol'];?>
                        </td>
                        <td style="font: 12px/18px Tahoma" valign="top" align="left">
                            RUR <?php echo $orders['price'];?>.00<br>
                        </td>
                    </tr>

                <?php } ?>

                <tr>
                    <td width="400" valign="top" align="left">
                        <font style="font: 12px/18px Tahoma" color="#b079df"><b style="font: 12px/18px Tahoma;font-weight: bold">Сумма заказа:</b></font>
                    </td>
                    <td></td>
                    <td valign="top" align="left">
                        <font color="#b079df" size="2"><b style="font: 12px/18px Tahoma;font-weight:bold;">RUR <?php echo $this->discount['sum'];?>.00</b></font>
                    </td>
                </tr>
                <tr>
                    <td width="400" valign="top" align="left">
                        <font style="font: 12px/18px Tahoma" color="#b079df"><b style="font: 12px/18px Tahoma;font-weight: bold">Скидка:</b></font>
                    </td>
                    <td></td>
                    <td valign="top" align="left">
                        <font color="#b079df" size="2"><b style="font: 12px/18px Tahoma;font-weight:bold;"><?php echo $this->discount['percent'];?> %</b></font>
                    </td>
                </tr>
                <tr>
                    <td width="400" valign="top" align="left">
                        <font style="font: 12px/18px Tahoma" color="#b079df"><b style="font: 12px/18px Tahoma;font-weight: bold">Всего:</b></font>
                    </td>
                    <td></td>
                    <td valign="top" align="left">
                        <font color="#b079df" size="2"><b style="font: 12px/18px Tahoma;font-weight:bold;">RUR <?php echo $this->discount['big_sum'];?>.00</b></font>
                    </td>
                </tr>
            </table>

            <br>
			<span style="font: 12px/18px Tahoma">Ваш контактный телефон: <?php echo $this->phone;?><br /><br />
			
			
			<font style="font: 12px/18px Tahoma">Скоро Вам позвонят сотрудники Parfumoff.ru для уточнения параметров заказа и условий доставки.
                <br />
                <br />Спасибо за выбор нашего магазина!
            </font>
			<br>
			</span>
            <br>
            <br>

            <div style="clear:both;font:12px/18px Tahoma"></div><br /><br />
            телефон для справок: <strong><?=$city_utf8['phone']?></strong>

        </div>

    </div><!-- #content-->




    <div id="footer"></div><!-- #footer --></div><!-- #wrapper -->
</body>
</html>