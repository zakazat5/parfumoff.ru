
<div class="bread_crumps">
    <a href="/">Главная</a>
    <span>&gt;</span>
    <a href="/korzina/">Корзина товаров</a>
    <span>&gt;</span>
    Заказ оформлен
</div>

<div class="news" style="overflow:hidden"><br />
    <div style="float:left;width:370px">

        <h2 style="font-weight:normal" class="georgia_head">Спасибо за покупку!</h2>
        <br /><br />
        <div  style="font-weight:normal">Ваш заказ принят.</div>
        <div class="purple" style="font-weight:normal">

         Код Вашего заказа <span style="font-size:18px">[<?=$this->order['id']?>]</span></div>

        <?php
            $products = array();

            foreach ($this->order_items as $order_item) {
                $products[] = array(
                    'id' => $order_item['id'],
                    'price' => $order_item['sum'],
                    'amount' => $order_item['kol']
                );
            }
        ?>

        <input class="rees46 track purchase" value='<?=json_encode(array(
            'products' => $products,
            'order' => $this->order['id'],
            'order_price' => $this->order['SUM']
        ))?>' type="hidden" />

        <input class="rees46 profile set" value='<?=json_encode(array(
            'id' => $this->user['id'],
            'email' => $this->user['email']
        ))?>' type="hidden" />

        <br /><br />
        В ближайшее время наши сотрудники с Вами свяжутся.<br />
        Информация о заказе отправлена на Вашу почту.<br />
        Статус заказа Вы можете посмотреть в истории <a href="/users/history/status/1/" class="purple">заказов</a>.
        <br /><br /><br />


        <?=$form?>

        <h2 style="font-weight:normal" class="georgia_head">Будьте в курсе!</h2>

        <br /><br />
        <div style="padding-bottom:8px">Информация о наших акциях и скидках:</div>
        <a href="/discount/" class="a_nak">накопительные скидки до 15%</a><br />
        <!--<a href="" class="a_pod">подарки</a><br />-->
        <a href="/sale/" class="a_ras">распродажи</a><br />



        <br /><br /><br />
    </div>
    <div style="float:right;width:350px;">

    </div>
</div>


<!--Трэкер "Основной"-->
<script>document.write('<img src="http://mixmarket.biz/tr.plx?e=3779414&r='+escape(document.referrer)+'&t='+(new Date()).getTime()+'" width="1" height="1"/>');</script>
<!--Трэкер "Основной"-->

