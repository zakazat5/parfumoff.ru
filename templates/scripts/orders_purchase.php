




<link rel="stylesheet" href="/templates/js/bootstrap-select/bootstrap-select.css">
<script src="/templates/js/bootstrap-select/bootstrap-select.js"></script>


<link rel="stylesheet" href="/templates/css/cart.css">
<script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>


</div>


<div id="user_popup">
    <div class="top_search">
        <form name="form" enctype="multipart/form-data" method="post" action="/find/" class="search_form">
            <input name="find" type="text" placeholder="Поиск товара по названию">
            <button type="submit">
                <i class="icon-search"></i>
            </button>
        </form>
    </div>
</div>

<div id="phone_popup" class="ta_center">
    <a href="tel:<?=$this->main['global_phone']?>" class="color_blue d_block td_underline fs26 cursor_pointer mb25 bold"><?=$this->main['global_phone']?></a>

    <a href="tel:8 (812) 426-11-15" class="color_blue d_block td_underline fs26 cursor_pointer mb25 bold">8 (812) 426-11-15</a>
    <div><a href="" class="color_blue dashed callMe-js" onclick="$('body').click()">Позвоните мне</a></div>
</div>

<header class="orders_header">
    <div class="container">
        <div id="logo"><a href="/">Парфюмофф.ру</a></div>
        <div id="top_phones">
            <div class="mb5 ta_right phone_item">Москва <span class="pl5 fs24 "><?=$this->main['global_phone']?></span></div>
            <div class="mb5 ta_right phone_item">Санкт-Петербург <span class="pl5 fs24 ">8 (812) 426-11-15</span></div>
            <div id="callme"><a href="" class="btn4 callMe-js">Позвоните мне</a></div>
        </div>
    </div>
</header>














<? if (isset($_GET['nenakhov'])) { ?>






<div class="cart_crossale hide_979">
    <div class="container">
        <div class="cc_title">Вас могут заинтересовать наши спецпредложения</div>
        <div class="ccSlider ">
            <ul class="ccSlider-js">
                <? foreach ($this->aProductsMore as $oProduct):?>
                    <li>
                        <img data-big="/images/uploads/catalog/<?=$oProduct['id']?>/small/<?=$oProduct['img']?>.jpg" src="/images/uploads/catalog/<?=$oProduct['id']?>/small/<?=$oProduct['img']?>" alt="<?=$oProduct['title']?>">
                        <div class="ccContent">
                            <div class="ccTitle"><?=$oProduct['title']?></div>
                            <div class="bold"><?=$oProduct['price']?> руб</div>
                            <a href="<?='?add_product='.$oProduct['articul']?>" class="color_purple ccBuy">Купить</a>
                        </div>
                    </li>
                <? endforeach;?>
            </ul>
        </div>
    </div>
</div>



<link rel="stylesheet" href="/templates/lib/bxslider-4/dist/jquery.bxslider.min.css">
<script src="/templates/lib/bxslider-4/dist/jquery.bxslider.min.js"></script>
<script>
$(function(){
    $('.ccSlider-js').bxSlider({
        autoHover: true,
        minSlides: 4,
        maxSlides: 4,
        slideWidth: 235,
        slideMargin: 3,
        moveSlides: 1,
        speed: 600,
        auto: true,
        useCSS: true,
    })    
})
</script>


<? } ?>

























<div class="container">

<?
/** @var Cart $cart */
$cart = $this->cart;
?>
<?php if (count($cart->getItems())==0): ?>
    <div class="news">
        <br />
        <h2 class="georgia_head">В данный момент Ваша корзина пуста.</h2> <br />
        Для совершения покупки необходимо выбрать аромат. <br />
        Это можно сделать несколькими способами: <br />
        - через <a href="/catalog/">каталог брендов</a>; <br />
        - через <a href="/girl/">каталог женских ароматов</a>;<br />
        - или <a href="/men/">мужских ароматов</a>;<br />
        - используя поиск по сайту (в верхнем правом углу);<br /><br />
        <h2 class="georgia_head">Желаем Вам приятных покупок!</h2><br />
    </div>
<? else:?>
    <form autocomplete="off" action="/korzina/" method="post" enctype="multipart/form-data" name="orders_list" id="orders">
        <div>
            <!-- <br /><br />
            <h2 style="margin-bottom:50px;font-weight:normal;font-size:24px;line-height:30px" class="georgia_head">
                Оформление заказа
                <div style="font-size:18px;">
                    (На сумму <b><span class="val_subTotal"><?//=$this->order['dSum']?></span> руб</b>, в количестве <b><span class="val_kol"><?//=$this->order['total_count']?></span> шт</b>.)
                </div>
            </h2> -->

            <div class="row">
                <div class="b-step col-sm-12 col-md-10 hidden-xs">
                    <div class="clearfix">
                        <p class="pull-left fs20 ml10">1 этап: Контактные данные</p>
                        <p class="pull-right fs20 mr10 color_green">2 этап: Адрес и способы доставки</p>
                    </div>
                    <div class="step-border">
                        <div class="step-border__green" style="right:0;"></div>
                        <div class="step-border__circle circle1"></div>
                        <div class="step-border__circle circle2 bd_green"></div>
                        <div class="step-border__circle circle3 bd_green"></div>
                    </div>
                </div>
            </div>

            <? include('partials/modals.php') ?>
            <? include('orders/step2_reg.php') ?>
        </div>
    </form>
    <div style="display:none">
        <div class="box-modal" id="login_form_popup" style="background:white;width:295px;padding:45px 105px;">
            <a class="box-modal_close close1"><span></span></a>
            <div class="title">Вход в личный кабинет</div>
            <div style="margin-bottom:20px;">
                Для оформления заказа введите ваш логин (совпадает с email) и пароль, полученный при регистрации на сайте.
            </div>
            <form action="/users/login/" method="post" name="reg_form">
                <div class="line"><input name="log_email" type="text" placeholder="Логин" /></div>
                <div class="line"><input name="log_passw" type="password" placeholder="Пароль" /></div>
                <div class="forget"><a href="/users/forgot-password/">Забыли пароль?</a></div>
                <input type="hidden" name="back_url" value="/korzina/">
                <div style="text-align:center;"><button name="register" class="cart_btn1">Войти</button></div>
            </form>

        </div>
    </div>



<? endif;?>




