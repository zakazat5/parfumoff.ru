<?php require_once(__DIR__ . '/../../includes/geo_domain.php'); ?>
<?php 
$timestamp = time();
$LastModified_unix = $timestamp; // время последнего изменения страницы
$LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
$IfModifiedSince = false;
if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
    $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));  
if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
    $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
    exit;
}
header('Last-Modified: '. $LastModified);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<? require_once('partials/head_files.php'); ?>

<body>



<? require_once('partials/data_layer.php'); ?>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PX333L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PX333L');</script>
<!-- End Google Tag Manager -->

<? require_once('partials/header.php'); ?>

<div class="wrapper content clearfix">

	<?php  if ($this->aBanners) {
        //rand banner
        $banner_id = rand(0,count($this->aBanners)-1); 
    ?>
	<!--	
	<div class="banner_type1">
		<a href="/christmassale/"><img src="/templates/images/new-year-banner.png" alt="Vybiraem Novogodnie podarki vmeste" /></a>
	</div>	
	-->
    <?php } ?>
	<div class="content__top">
		<div class="categories">
            <div class="categories__header">Категории</div>
			<?php if (isset($this->category_items)) { ?>
				<div class="categories__list">
					<?php foreach ($this->category_items as $category_item) { ?>
						<a class="categories__link<?php if ($category_item['id'] == 14) { echo ' category_sale_menu'; } ?>" href="<?php echo $category_item['url']; ?>"><?php echo $category_item['name']; ?></a>
					<?php } ?>
				</div>
			<?php } ?>
        </div>

		<div class="mobi-cat">
		<p style="text-align: center;    margin-bottom: 20px;">Категории</p>
			<div class="row">
			<div class="col-xs-4">
					<a href="sale/"><i class="fa fa-percent ic1"></i><p>Распродажа</p></a>
			</div>
			<div class="col-xs-4">
					<a href="bestsellers/"><i class="fa fa-thumbs-o-up ic2"></i><p>Хиты продаж</p></a>
			</div>
			<div class="col-xs-4">
					<a href="new-items/"><i class="fa fa-plus ic3"></i><p>Новинки</p></a>
			</div>
			<div class="col-xs-4">
					<a href="stars-goods/"><i class="fa fa-star-o ic4"></i><p>Ароматы<br>от звёзд</p></a>
			</div>
			<div class="col-xs-4">
					<a href="selective-perfumery/"><i class="fa fa-star ic5"></i><p>Селективная<br>парфюмерия</p></a>
			</div>
			<div class="col-xs-4">
					<a href="cosmetic/"><i class="fa fa-eye ic6"></i><p>Косметика</p></a>
			</div>
			<div class="col-xs-4 col-xs-offset-4">
					<a href="gifts/"><i class="fa fa-gift ic7"></i><p>Подарочные наборы</p></a>
			</div>
			</div>
        </div>

		<div class="content__catalog-top">
			<div class="header__category-top">
				<div class="name__category-top name__category-top--main">
					<h1 class="content__header">Новинки парфюмерии</h1>
				</div>
				<div class="banner_free_ship">
					<img class="banner_free_ship_img" src="/templates/images/parfumoff1-1.png" alt="">
				</div>
			</div>
			<div class="category_filter_m_f"> 
				<a class="category_filter_f_link" href="/woman/">для женщин</a>&nbsp;/&nbsp;<a class="category_filter_m_link" href="/men/">для мужчин</a>
			</div>
			<div class="content__catalog-top-items">
				<?php if (isset($this->catalog_item_top)) { 
					foreach ($this->catalog_item_top as $key=>$catalog_item) {
						if ($key <= 2) {
							include 'partials/catalog_item.php'; 
						}
					} 
				} ?>
			</div>
		</div>
	</div>
	<div class="catalog clearfix">
		<?php if (isset($this->catalog_item_top)) { 
			foreach ($this->catalog_item_top as $key=>$catalog_item) {
				if ($key > 2) {
					include 'partials/catalog_item.php'; 
				}
			} 
		} ?>
	</div>
</div>
<div class="wrapper">
	<div class="comment">
		<h1 class="comment__header">Интернет-магазин оригинальной парфюмерии Parfumoff.ru</h1>
		<p class="comment__content">Добро пожаловать в яркий, красочный, зачаровывающий мир ароматов! В интернет-магазине Parfumoff.ru собрана оригинальная парфюмерия
			всех ведущих мировых брендов. Здесь Вы сможете купить недорого духи и туалетную воду для дополнения любого стильного
			образа: повседневного, делового, спортивного, праздничного, вечернего.</p>
		<p class="comment__content">Мы предлагаем парфюм для покупателей с любым бюджетом. Вы можете заказать в интернет-магазине и недорогие духи, и парфюм
			средней ценовой категории, и элитные ароматы. </p>
		<h2 style="font-size:2.6rem" class="comment__header">Parfumoff.ru: только фирменная продукция</h2>
		<p class="comment__content">Наш магазин дорожит своей репутацией, поэтому предлагает только фирменные товары. Все представленные в интернет-магазине
			оригиналы духов, парфюма, туалетной воды и иной продукции имеют сертификаты и декларации соответствия. Мы работаем только
			с надежными поставщиками и гарантируем 100%-ную защиту от подделок. Узнать больше и посмотреть примеры разрешительных
			документов на продукцию Вы можете в разделе <a href="/kachestvo/">«Качество и гарантии».</a></p>
		<h2 style="font-size:2.6rem" class="comment__header">Удобные и выгодные покупки в Parfumoff.ru</h2>
		<p class="comment__content">Список наших преимуществ не ограничивается огромным ассортиментом оригинальной парфюмерии в интернет-магазине в Москве. Мы
			также создали комфортные условия для онлайн-покупок, чтобы Вы оформляли заказ без лишней траты времени, удобно оплачивали
			и получали его.</p>
		<ul class="comment__content list-arrows">
			<li>Вы можете купить парфюмерию в Москве в пределах МКАД с бесплатной доставкой в течение 1 дня.</li>
			<li>Доставка в Подмосковье выполняется в течение 1-2 дней по доступной стоимости.</li>
			<li>Несколько способов удобной оплаты (в том числе и через платежные сервисы).</li>
			<li>Купить духи в Москве всего за пару минут могут даже незарегистрированные пользователи.</li>
			<li>Зарегистрированным покупателям предоставляются дополнительные скидки.</li>
		</ul>
	</div>
	<?php require_once('partials/top_brands.php'); ?>
	<?php require_once('partials/social_block.php'); ?>
</div>

<? require_once('partials/footer.php'); ?>

<? require_once('partials/modals.php'); ?>

</body>
</html>