<div class="bread_crumps">
    <a href="/">Главная</a>
    <span>&gt;</span>
    <?php echo $this->cat_name; ?>
</div>

<div class="row">
    <div class="col-xs-12">
        <h1>Статьи о парфюмерии</h1>
        <?php foreach ($this->news_item as $news_item): ?>
        <div class="row listing-article">
            <?php if($news_item['img']!=''): ?>
            <div class="listing-article_image hidden-xs col-md-2">
                <img width="170" align="left" src="/images/uploads/news/<?php echo $news_item['id'];?>/trumbs/<?php echo $news_item['img'];?>" alt="<?php echo $news_item['title'];?>" title="<?php echo $news_item['title'];?>"/>
            </div>
            <?php endif; ?>
            <div class="listing-article_wrap col-xs-12 col-md-10">
                <h3 class="listing-article_title mt20"><a href="/<?php echo $this->canon_name1; ?>/<?php echo $news_item['url']; ?>/"><?php echo $news_item['title']; ?></a></h3>
                <div class="listing-article_anot-wrapper">
                    <span class="color_gray1 fs14 listing-article_date"><?php echo $news_item['date']; ?></span>
                    <div class="listing-article_anot mt20">
                        <?php echo preg_replace("/<img.*\/>/", "", $news_item['anot']); ?>
                        <script>document.write('<a rel="nofollow" class="listing-article_link color_blue td_underline mt10" href="/<?php echo $this->canon_name1; ?>/<?php echo $news_item['url']; ?>/">Читать дальше</a>');</script>
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>

        <br />
        <?php echo $this->pages; ?>
        <br />
    </div>
</div>