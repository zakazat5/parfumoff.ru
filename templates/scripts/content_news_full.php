
<div class="bread_crumps">
   <a href="/">Главная</a>
    <span>&gt;</span>
   <a href="/article/">Статьи</a>
    <span>&gt;</span>
    <?php echo $this->news_item['title']; ?>
    <?php //if($this->news_item['brend_title']!=''): ?>
<!--    <span>&gt;</span>-->
<!--    <a href="/production/--><?php //echo $this->news_item['brend_url'];?><!--/">--><?php //echo $this->news_item['brend_title'];?><!--</a>-->
<?php //endif; ?>
</div>

<h1 class="mt20 pt20 mb10"><?php echo $this->news_item['title']; ?></h1>

<?php if($this->news_item['brend_title']!=''){ ?>
    <div style="margin-left:40px;position:relative;top:-20px;font-size:12px">// <span style="">Бренд:</span> <a href="/production/<?php echo $this->news_item['brend_url'];?>/"><?php echo $this->news_item['brend_title'];?></a>
        <?php if($this->news_item['aromat_title']!=''){ ?>
            // <span style="">Аромат:</span> <a href="/production/<?php echo $this->news_item['brend_url'];?>/<?php echo $this->news_item['aromat_url'];?>/"><?php echo $this->news_item['aromat_title'];?></a>
        <?php } ?>
    </div>
    <div class="clear"></div>
<?php } ?>

<div class="pl1">
    <?php if($this->news_item['img']!='' && 1==2){ ?>
        <img style="margin-left:40px;*margin-left:10px;border:1px solid black;margin-right:10px;" align="left" width="200" src="<?php echo $this->news_item['img']; ?>" alt="<?php echo $this->news_item['alt'];?>">
    <?php } ?>
    <div style="margin-top:-18px;display:block">
        <?php echo $this->news_item['text']; ?>
    </div>
</div>

<div class="b_bottom fs34 pt40 pb20 mb15 lite">Ещё новости парфюмерии</div>
<div class="news__wrapper pb60">
    <div class="row">
        <?php foreach ($this->index_news as $news_item): ?>
            <? include 'partials/new_item_for_main.php'; ?>
        <?php endforeach; ?>
    </div>
    <div class="row text-center">
        <a href="/article/" class="btn4">
            <span class="hide_mobile">Все новости</span>
        </a>
    </div>
</div>