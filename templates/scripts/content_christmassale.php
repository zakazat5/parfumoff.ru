<link rel="stylesheet" href="/templates/css/christmassale.css" type="text/css"  />
<div class="wrapper">
	<div class="categories">
		<a class="categories__item categories__item--she" href="/girl/">Для неё!</a>
		<a class="categories__item categories__item--he" href="/men/">Для него!</a>
	</div>
	<div class="catalog">
         
          <div class="item">
			<a class="item__brand" href="/production/hermes/">Hermes</a>
			<a class="item__name" href="/production/hermes/eau-des-merveilles/">Eau Des Merveilles</a>
			<a class="item__picture-container" href="/production/hermes/eau-des-merveilles/">
				<img class="item__picture" src="/images/uploads/catalog/1293/small/1250252794.jpg" alt=""></a>
            <div class="item__price">от 991 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/hermes/eau-des-merveilles/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/annick-goutal/">Annick Goutal</a>
			<a class="item__name" href="/production/annick-goutal/nuit-etoilee/">Nuit Etoilee</a>
			<a class="item__picture-container" href="/production/annick-goutal/nuit-etoilee/">
			<img class="item__picture" src="/images/uploads/catalog/6627/small/1352453729.jpg" alt=""></a>
            <div class="item__price">от 3221 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/annick-goutal/nuit-etoilee/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/odin/">Odin</a>
			<a class="item__name" href="/production/odin/02-owari/">02 Owari</a>
			<a class="item__picture-container" href="/production/odin/02-owari/">
			<img class="item__picture" src="/images/uploads/catalog/5071/small/1315331456.jpg" alt=""></a>
            <div class="item__price">от 5690 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/odin/02-owari/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/kenzo/">Kenzo</a>
			<a class="item__name" href="/production/kenzo/pour-homme/">Pour Homme</a>
			<a class="item__picture-container" href="/production/kenzo/pour-homme/">
			<img class="item__picture" src="/images/uploads/catalog/1555/small/1351146532.jpg" alt=""></a>
            <div class="item__price">от 1192 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/kenzo/pour-homme/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/chanel/">Chanel</a>
			<a class="item__name" href="/production/chanel/no-5-l039eau/">No 5 L'Eau</a>
			<a class="item__picture-container" href="/production/chanel/no-5-l039eau/">
			<img class="item__picture" src="/images/uploads/catalog/11879/small/1473686603.jpg" alt=""></a>
            <div class="item__price">от 5113 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/chanel/no-5-l039eau/');return false;" href="">Заказать</a>
          </div>
		   <div class="item">
			<a class="item__brand" href="/production/nina-ricci/">Nina Ricci</a>
			<a class="item__name" href="/production/nina-ricci/luna/">Luna</a>
			<a class="item__picture-container" href="/production/nina-ricci/luna/">
			<img class="item__picture" src="/images/uploads/catalog/11936/small/1475065369.jpg" alt=""></a>
            <div class="item__price">от 2599 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/nina-ricci/luna/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/angel-schlesser/">Angel Schlesser</a>
			<a class="item__name" href="/production/angel-schlesser/angel-schlesser_m/">Angel Schlesser</a>
			<a class="item__picture-container" href="/production/angel-schlesser/angel-schlesser_m/">
			<img class="item__picture" src="/images/uploads/catalog/116/small/1352366887.jpg" alt=""></a>
            <div class="item__price">от 1775 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/angel-schlesser/angel-schlesser_m/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/givenchy/">Givenchy</a>
			<a class="item__name" href="/production/givenchy/gentlemen-only-absolute/">Gentlemen Only Absolute</a>
			<a class="item__picture-container" href="/production/givenchy/gentlemen-only-absolute/">
			<img class="item__picture" src="/images/uploads/catalog/11738/small/1469439823.jpg" alt=""></a>
            <div class="item__price">от 3165 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/givenchy/gentlemen-only-absolute/');return false;" href="">Заказать</a>
          </div>
		  
          <div class="item">
			<a class="item__brand" href="/production/narciso-rodriguez/">Narciso Rodriguez</a>
			<a class="item__name" href="/production/narciso-rodriguez/narciso-poudree/">Narciso Poudree</a>
			<a class="item__picture-container" href="/production/narciso-rodriguez/narciso-poudree/">
				<img class="item__picture" src="/images/uploads/catalog/11247/small/1456392057.jpg" alt=""></a>
            <div class="item__price">от 3225 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/narciso-rodriguez/narciso-poudree/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/christian-dior/">Christian Dior </a>
			<a class="item__name" href="/production/christian-dior/poison-girl/">Poison Girl</a>
			<a class="item__picture-container" href="/production/christian-dior/poison-girl/">
			<img class="item__picture" src="/images/uploads/catalog/10938/small/1453123801.jpg" alt=""></a>
            <div class="item__price">от 1507 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/christian-dior/poison-girl/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			 <a class="item__brand" href="/production/dsquared2/">DSQUARED2</a>
			<a class="item__name" href="/production/dsquared2/he-wood/">He Wood</a>
			<a class="item__picture-container" href="/production/dsquared2/he-wood/">
			<img class="item__picture" src="/images/uploads/catalog/847/small/1273765791.jpg" alt=""></a>
            <div class="item__price">от 1782 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/dsquared2/he-wood/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/giorgio-armani/">Giorgio Armani</a>
			<a class="item__name" href="/production/giorgio-armani/code-profumo/">Code Profumo</a>
			<a class="item__picture-container" href="/production/giorgio-armani/code-profumo/">
			<img class="item__picture" src="/images/uploads/catalog/11357/small/1459157501.jpg" alt=""></a>
            <div class="item__price">от 1996 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/giorgio-armani/code-profumo/');return false;" href="">Заказать</a>
          </div>
		  
		  
          <div class="item">
			<a class="item__brand" href="/production/hermes/">Hermes</a>
			<a class="item__name" href="/production/hermes/24-faubourg/">24 Faubourg</a>
			<a class="item__picture-container" href="/production/hermes/24-faubourg/">
				<img class="item__picture" src="/images/uploads/catalog/1284/small/1250252655.jpg" alt=""></a>
            <div class="item__price">от 3098 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/hermes/24-faubourg/');return false;" href="">Заказать</a>
          </div>
		  
          <div class="item">
			
          </div>
          <div class="item">
			<a class="item__brand" href="/production/cacharel/">Cacharel</a>
			<a class="item__name" href="/production/cacharel/liberte/">Liberte</a>
			<a class="item__picture-container" href="/production/cacharel/liberte/">
			<img class="item__picture" src="/images/uploads/catalog/448/small/1353917469.jpg" alt=""></a>
            <div class="item__price">от 1989 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/cacharel/liberte/');return false;" href="">Заказать</a>
          </div>
          <div class="item">
			<a class="item__brand" href="/production/gucci/">Gucci</a>
			<a class="item__name" href="/production/gucci/by-gucci-sport/">By Gucci Sport </a>
			<a class="item__picture-container" href="/production/gucci/by-gucci-sport/">
			<img class="item__picture" src="/images/uploads/catalog/3187/small/1285090960.jpg" alt=""></a>
            <div class="item__price">от 1390 руб.</div>
			<a class="item__button" onclick="show_fast_view('/production/gucci/by-gucci-sport/');return false;" href="">Заказать</a>
          </div>
    </div>  
	<div class="brands">
		<?php require_once('partials/social_block.php'); ?>	
	</div>	
</div>
