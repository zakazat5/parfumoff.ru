<?php require_once(__DIR__ . '/../../includes/geo_function.php'); ?>
<div class="bread_crumps">
    <a href="/">Главная</a>
    <span>&gt;</span>
    Контакты и доставка
</div>
<div id="contacts">
<br>
<div class="heading_type1" style="border-bottom:0">Контакты</div>

<div class="row mb40 row1">
	<div class="col-xs-4">
		<p>ООО "Смарт Студио"</p>
		<p>ОГРН 1167746299346</p>
	</div>
	<div class="col-xs-4">
		<p>
            <div>Юридический адрес:</div> <span class="color_purple">г. Москва</span>, ул. Николоямская, <br>д. 48, стр. 2
            <br>
            <span class="">Тел.: <span class="call_phone_1">+7 (495) 540-47-23</span></span>
        </p>
	</div>
	<?php if (geo_city() === "spb") { ?>
	<div class="col-xs-4">
		<p>Адрес в <span class="color_purple">Санкт-Петербурге:&nbsp;</span><br>
		<span >пр-т Юрия Гагарина, д. 2</span></p>
        <p>Тел.: +7(812) 426-11-15</p>
	</div>
	<?php } ?>
</div>

<div class="row mb40 row1">
	<p>
		<div>Адрес:</div>
		<span class="color_purple">г. {город}</span>, {адрес}
		<br />
		<span class="">Тел.: <span class="call_phone_1 phone_alloka">{телефон}</span></span>
	</p>
</div>
<div class="row mb40 row1"><script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=USmq7UiG1U7Yv8BwQLei5wUzZA6gh16e&amp;width=500&amp;height=400&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script></div>

<p class="color_gray2 fs14"><br>Вопросы и предложения присылайте на электронную почту <a class="color_gray2" href="mailto:info@parfumoff.ru">info@parfumoff.ru</a></p>
<div style="border-bottom:1px solid #f2f2f2;padding-top:70px;margin-bottom:30px"></div>

<div class="heading_type1" style="border-bottom:0">
	<a name="dostavka"></a>
	Доставка
	<!--
	<a href="" class="btn2 fs18 pl20 pr20 relative" style="left:30px" onclick="return false;$('.delivery').show();return false;">Проверьте ваш город</a>

    <script src="/templates/js/suggest_delivery.js"></script>
    <script>
        $(function(){
            AddressSuggestions.initForm();
        })
    </script>
	-->

</div>
    <div class="delivery" style="display: none;">
    <div class="b-delivery container" style="display:block;">
    <div class="line">
        <label for="step2_region">Регион / Область:<span class="star">*</span></label>
        <input name="region" value="" placeholder="Москва, Московская обл, Тульская обл, Карелия" id="step2_region" type="text" autocomplete="off">
    </div>

    <div class="line part" style="display: block;">
        <label for="step2_town">Уточните район:<span class="star">*</span></label>
        <select name="part" id="select_part">
            <option style="display: inline;" data-location="m" value="7700000000000">Москва в пределах МКАД</option>
            <option style="display: inline;" data-location="m" value="0000000000001">Жулебино</option>
            <option style="display: inline;" data-location="m" value="0000000000002">Косино</option>
            <option style="display: inline;" data-location="m" value="0000000000003">Новокосино</option>
            <option style="display: inline;" data-location="m" value="0000000000004">Кожухово</option>
            <option style="display: inline;" data-location="m" value="0000000000005">Бутово</option>
            <option style="display: inline;" data-location="m" value="0000000000006">Солнцево и Ново-Переделкино</option>
            <option style="display: inline;" data-location="m" value="0000000000007">Ново-Переделкино</option>
            <option style="display: inline;" data-location="m" value="0000000000009">Митино</option>
            <option style="display: inline;" data-location="m" value="0000000000010">Куркино</option>
            <option style="display: none" data-location="s" value="7800000000000">Санкт-Петербург в пределах КАД</option>
            <option style="display: none" data-location="s" value="7800000900000">Пушкин</option>
            <option style="display: none" data-location="s" value="7800000700000">Павловск</option>
            <option style="display: none" data-location="s" value="780000400000">Красное Село</option>
            <option style="display: none" data-location="s" value="7800000800000">Петергоф</option>
        </select>

        <div class="other" style="display: none;">
            <div style="clear: both"></div>
            <br>
            <label for="other" style="width:120px;float:left;">Другой:</label>
            <input style="padding:2px;font: 11px Arial;width:300px" value="" type="text" name="other" id="other">
        </div>
    </div>

    <div class="line">
        <label for="step2_town">Город / Район:<span class="star">*</span></label>
        <input name="city" value="" id="step2_town" placeholder="Введите свой город" type="text" autocomplete="off">
    </div>
    <div class="line settlement" style="display: none;">
        <label for="step2_settlement">Населенный пункт:<span class="star">*</span></label>
        <input name="settlement" value="" id="step2_settlement" type="text" autocomplete="off">
    </div>
    </div>


    <h2 style="margin-bottom: 20px; margin-top: 30px">В Ваш город/населенный пункт мы сможем доставить заказ следующими способами:</h2>
    <div id="couriers__wrapper">
        <div class="dostavka_item" id="courier" data-type="курьером" data-modal="courier">
            <div class="l" style="position:relative;top:0px">
                <div style="position:relative;top:-10px" class="text">Доставка  курьером</div>
            </div>
            <div class="r" style="margin-left: 0;">
                <table style="margin: 0;">
                    <thead class="courier__table-header head">
                    <tr>
                        <th style="padding-left: 0;">Служба доставки</th>
                        <th style="text-align:left;width:40px;padding-left:0">Срок</th>
                        <th style="width:50px">Цена</th>
                    </tr>
                    </thead>
                    <tbody class="courier__table-body"></tbody>
                    <tr style="display: none;">
                        <td class="tooltip_bg_blue">
                            <div data-width="250"  class="dostavka_item__checkbox" data-toggle="tooltip" data-content="Доставка ПН-СБ с 10 до 20:00. В день доставки Вам приходит смс-уведомление с номером телефона водителя, который впоследствии Вам позвонит.">b2bpl</div>
                        </td>
                        <td>
                            <div class="">4 дн.</div>
                        </td>
                        <td>117 р</td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="dostavka_item" id="post" data-type="почта" data-modal="post">
            <div class="l">
                <div style="position:relative;top:-10px" class="text" >Почта  России</div>
            </div>
            <div class="r" style="margin-left: 0;">
                <table style="margin: 0;">
                    <thead class="courier__table-header head">
                    <tr>
                        <th style="padding-left: 0;">Служба доставки</th>
                        <th style="text-align:left;width:40px;padding-left:0">Срок</th>
                        <th style="width:50px">Цена</th>
                    </tr>
                    </thead>
                    <tbody class="courier__table-body"></tbody>
                    <tr style="display: none;">
                        <td class="tooltip_bg_blue">
                            <div class="dostavka_item__checkbox" data-width="250" data-toggle="tooltip" data-content="Заказы высылаются в течение 1 дня ускоренной посылкой 1 Класса. Получить и оплатить товар Вы сможете на своем почтовом отделении. В момент прибытия посылки Вы получите извещение от Почты России. При оплате наложенным платежом Почта России взимает от 2 до 5% комиссию за перевод денег на наш счет.">Почта России</div>
                        </td>
                        <td>
                            <div >5-12 дн.</div>
                        </td>
                        <td><div style="cursor:pointer" data-toggle="tooltip" data-content="Самая выгодная стоимость доставки" class="green_label">117 р</div></td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>

        <div class="dostavka_item" id="self" data-type="самовывоз" data-modal="self">
            <div class="l" >
                <div style="position:relative;top:-10px" class="text">Самовывоз</div>
            </div>


            <div class="r" style="margin-left: 0;">
                <table style="margin: 0;">
                    <thead class="courier__table-header head">
                    <tr>
                        <th style="padding-left: 0;">Адреса пунктов выдачи</th>
                        <th style="text-align:left;width:40px;padding-left:0">Срок</th>
                        <th style="width:50px">Цена</th>
                    </tr>
                    </thead>
                    <tbody class="courier__table-body"></tbody>
                </table>
            </div>

            <div class="clear"></div>
        </div>
    </div>
    </div>

<?php if (geo_city() === "def") { ?>
<table width="100%" cellspacing="0" cellpadding="0" border="0" >
<tbody>
<tr class="head">
<th width="300" style="">Город</th>
<th width="180" style="">Стоимость <br> доставки, руб</th>
<th width="180" style="">Сроки доставки</th>
<th style="">Способы оплаты</th>
</tr>
<tr>
<td style=""><div class=""><strong>Москва</strong> (В пределах МКАД)</div></td>
<td style="">Бесплатно</td>
<td style="">1 день</td>
<td style="">Наличными курьеру</td>
</tr>
<tr>
<td style="">

<div class="mb15"><strong> Московская область</strong></div>
<div class="">
<div class="mb10">- до 10 км от МКАД <span style="display:none" class="mobile_help">&mdash; 290 руб</span></div>
<div class="mb10">- от 10 до 15 км от МКАД <span style="display:none" class="mobile_help">&mdash; 400 руб</span></div>
<div class="mb10">- от 15 до 25 км от МКАД <span style="display:none" class="mobile_help">&mdash; 600 руб</span></div>
</div>
</td>
<td style="">
<div class="mobile_col">
<div class="mb10">&nbsp;</div>
<div class="mb10">290</div>
<div class="mb10">400</div>
<div class="mb10">600</div>
</div>
</td>
<td style="">
<br class="mobile_col"> 1-2 дня</td>
<td style="">
<br class="mobile_col"> Наличными курьеру
</td>
</tr>
<!--
<tr>
<td style=""><strong>Санкт-Петербург</strong></td>
<td style="">290 <span style="display:none" class="mobile_help">руб</span></td>
<td style="">2 дня</td>
<td style="">Наличными курьеру</td>
</tr>
<tr>
<td class="hide_br" style="">
<div class="mb15"><b>Города России:</b></div>
Брянск<br> Владимир<br>
Вологда<br> Иваново<br> Калуга<br>
Кострома<br> Нижний Новгород<br> Орел<br>
Рязань<br> Тверь<br> Тула<br> Ярославль<br></td>
<td style="">290 <span style="display:none" class="mobile_help">руб</span></td>
<td style="">2 дня</td>
<td style="">Наличными курьеру</td>
</tr>
<tr>
<td class="hide_br" style="">
<div class="mb15"><b>Города России:</b></div>
Екатеринбург<br>
Ростов-на-Дону<br>
Тюмень<br>
Челябинск
</td>
<td style="">290 <span style="display:none" class="mobile_help">руб</span></td>
<td style="">3-4 дня</td>
<td style="">Наличными курьеру</td>
</tr>
<tr>
<td style="">
<div class="mb15" style=" font-weight: bold;">Другие регионы России</div></td>
<td style="">290</td>
<td style="">4-10 дней</td>
<td style="">Наложенный платеж.&nbsp;Комиссия Почты от 2 до 4%</td>
</tr>
-->
</tbody>
</table>
<?php } ?>
<p>&nbsp;</p><p>&nbsp;</p>


<div class="row relative" style="padding-bottom:80px;border-bottom:1px solid #f2f2f2;line-height:1.4;margin:0 0px 30px">
	<?php if (geo_city() === "def") { ?>
	<div>
		<h1 style="font-weight:400" class="fs20 mb20 color_purple">Доставка по Москве</h1>
		<p><b>осуществляется БЕСПЛАТНО</b> в пределах МКАД курьером в день заказа, если заказ сделан до 11.00, и на
		следующий день, если заказ сделан после 11.00. Удобное для вас время доставки наши менеджеры согласуют с
		вами по телефону.&nbsp;При отсутствии свободных курьеров Parfumoff.ru вправе изменить условия доставки.</p>
		<br><br>
		<h1 style="font-weight:400" class="fs20 mb20 color_purple">Доставка по Московской области.</h1>
		<p>Доставка по Московской области осуществляется курьерской службой и составляет:<br>
		<b>до 10 км от МКАД - 290 рублей.<br>свыше 10 км от МКАД - 400 рублей.&nbsp;</b></p>
		<br><br>
		<h1 style="font-weight:400" class="fs20 mb20 color_purple">Доставка по России.</h1>
		<p>Осуществляется Федеральной Почтовой службой, курьерской службой Boxberry, DPD, SPSR, PickPoint, СДЭК.<br>
		Стоимости доставки рассчитывается индивидуально в зависимости от суммы заказа.</p>
	</div>
	<?php } ?>
	<!--
	<div class="col-xs-4" style="padding: 0 25px">
		<h1 style="font-weight:400" class="fs20 mb20 color_purple">Доставка по Москве</h1>
		<p><b>осуществляется БЕСПЛАТНО</b> в пределах МКАД курьером в день заказа, если заказ сделан до 10.00, и на
		следующий день, если заказ сделан после 10.00. Удобное для вас время доставки наши менеджеры согласуют с
		вами по телефону.&nbsp;При отсутствии свободных курьеров Parfumoff.ru вправе изменить условия доставки.</p>
		<br><br>
		<h1 style="font-weight:400" class="fs20 mb20 color_purple">Доставка по Московской области.</h1>
		<p>Доставка по Московской области осуществляется курьерской службой и составляет:<br>
		<b>до 10 км от МКАД - 250 рублей.<br>свыше 10 км от МКАД - 400 рублей.&nbsp;</b></p>
	</div>
	-->
	<?php if (geo_city() != "def") { ?>
	<div class="col-xs-4" style="padding: 0 25px">
		<h1 style="font-weight:400" class="fs20 mb20 color_purple">Доставка в регионы России</h1>
		<p>В города Брянск, Владимир, Вологда, Екатеринбург, Иваново, Калуга, Кострома, Нижний Новгород, Орел, 
		Ростов-на-Дону, Рязань, Санкт-Петербург, Тверь, Тула, Тюмень, Челябинск, Ярославль, Белгород, Воронеж, Ижевск, 
		Иркутск, Казань, Краснодар, Киров, Красноярск, Курск, Липецк, Новокузнецк, Новосибирск, Омск, Пенза, Пермь, Сургут, 
		Волгоград, Тамбов, Томск, Чебоксары, Самара, Уфа 
		<span class="color_purple">заказы доставляются курьером на указанный Вами адрес.</span></p><br>
		<p><strong>Стоимость доставки - 290руб<br>Оплата наличными при получении заказа.</strong></p>
	</div>
	<div class="col-xs-4" style="padding: 0 25px">
		<h1 style="font-weight:400" class="fs20 mb20 color_purple">В другие города России (кроме вышеуказанных)</h1>
		<p>доставка осуществляется через Почту России. <br>Оплата заказа осуществляется на
		почтовом отделении при получении заказа. <br><br>
		<b>
			Стоимость доставки - 290руб.<br>
			Комиссия Почты от 2 до 4%</b>

		<br><br><span class="color_purple">В случае предоплаты стоимость доставки составит 200 руб!
		При получении заказа никаких комиссий не платите!&nbsp;</span></p>
	</div>
	<?php } ?>
	<div class="absolute color_gray1 fs14" style="right:0;bottom:30px">При отсутствии свободных курьеров Parfumoff.ru вправе изменить условия доставки.</div>
</div>


<div class="heading_type1" style="border-bottom:0"><a name="zakaz"></a>Как заказать на нашем сайте?</div>
<div class="row steps">
	<div class="col-xs-7">
		<div class="step">
			<div class="l">Шаг 1.</div>
			<div class="r">Необходимо поместить нужные Вам товары в корзину, нажав кнопку</div>
		</div>
		<div class="step">
			<div class="l">Шаг 2.</div>
			<div class="r">Затем перейти в корзину и нажать «Оформить заказ».</div>
		</div>
		<div class="step">
			<div class="l">Шаг 3.</div>
			<div class="r">Вы можете оформить заказ без регистрации, где просто заполняете все поля и следуете инструкциям.</div>
		</div>
		<div class="step">
			<div class="l">Шаг 4.</div>
			<div class="r">В маленьком окошке справа нажмите кнопку «Отправить заказ».</div>
		</div>
	</div>
	<div class="col-xs-5 ta_center">
		<span style="height:36px;line-height:36px" class="fs16 pl20 pr20 btn2">В корзину</span> <br>
		<img  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAgCAAAAADcPrxpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQ5NEU3MjRERTlFRjExRTRCMUIxRjZCQUUzMTJGOEUyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjQ5NEU3MjRFRTlFRjExRTRCMUIxRjZCQUUzMTJGOEUyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDk0RTcyNEJFOUVGMTFFNEIxQjFGNkJBRTMxMkY4RTIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NDk0RTcyNENFOUVGMTFFNEIxQjFGNkJBRTMxMkY4RTIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6M2seAAAAAsklEQVQoz+3SwU7CUBAF0Pf/v3OqG4JBiCigNamIK39CJbXSvnGhVcAmLtw6y7O692ZSDF/61dfFzaAX1EOO7b//ybuIlJ/zkbdLVaQr582BdwuKSCdMd3ueV7iP9IiL9stziTJHig0uu94rrHJEirjDMn/4GovuM+ctyoztA+Ztnz9fo8IGs/a7V7fAqUKfre/bzjHGpDncYTcD49fjfd4mGNU/d2vOjF6G9qynT3t/+w43m6schwdX9gAAAABJRU5ErkJggg==" />
		<br>
		<span style="height:36px;line-height:36px" class="fs16 pl20 pr20 btn3">Оформить заказ</span> <br>
		<img  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAgCAAAAADcPrxpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQ5NEU3MjRERTlFRjExRTRCMUIxRjZCQUUzMTJGOEUyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjQ5NEU3MjRFRTlFRjExRTRCMUIxRjZCQUUzMTJGOEUyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDk0RTcyNEJFOUVGMTFFNEIxQjFGNkJBRTMxMkY4RTIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NDk0RTcyNENFOUVGMTFFNEIxQjFGNkJBRTMxMkY4RTIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6M2seAAAAAsklEQVQoz+3SwU7CUBAF0Pf/v3OqG4JBiCigNamIK39CJbXSvnGhVcAmLtw6y7O692ZSDF/61dfFzaAX1EOO7b//ybuIlJ/zkbdLVaQr582BdwuKSCdMd3ueV7iP9IiL9stziTJHig0uu94rrHJEirjDMn/4GovuM+ctyoztA+Ztnz9fo8IGs/a7V7fAqUKfre/bzjHGpDncYTcD49fjfd4mGNU/d2vOjF6G9qynT3t/+w43m6schwdX9gAAAABJRU5ErkJggg==" />
		<div style="line-height:1.2">
			1. Укажите регион и город <br>
			2. Выберите способ доставки
		</div>
		<img  src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAgCAAAAADcPrxpAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjQ5NEU3MjRERTlFRjExRTRCMUIxRjZCQUUzMTJGOEUyIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjQ5NEU3MjRFRTlFRjExRTRCMUIxRjZCQUUzMTJGOEUyIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6NDk0RTcyNEJFOUVGMTFFNEIxQjFGNkJBRTMxMkY4RTIiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6NDk0RTcyNENFOUVGMTFFNEIxQjFGNkJBRTMxMkY4RTIiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6M2seAAAAAsklEQVQoz+3SwU7CUBAF0Pf/v3OqG4JBiCigNamIK39CJbXSvnGhVcAmLtw6y7O692ZSDF/61dfFzaAX1EOO7b//ybuIlJ/zkbdLVaQr582BdwuKSCdMd3ueV7iP9IiL9stziTJHig0uu94rrHJEirjDMn/4GovuM+ctyoztA+Ztnz9fo8IGs/a7V7fAqUKfre/bzjHGpDncYTcD49fjfd4mGNU/d2vOjF6G9qynT3t/+w43m6schwdX9gAAAABJRU5ErkJggg==" />
		<br>
		<span style="height:36px;line-height:36px;display:inline-block" class="fs16 pl20 pr20 btn1">Отправить заказ</span>
	</div>
</div>

</div>






<style>
.steps {
	margin-bottom: 80px;
}
.steps .step {
	margin-bottom: 35px;
}
.steps .step .l {
	float: left;
	font-weight: bold;
}
.steps .step .r {
	margin-left: 65px;
}
.steps img {
	display: inline-block;
	margin: 10px 0;
}
tr.head th {
	background: #f5f5f5;
	color: #929191;
	font-size: 16px;
	font-weight: 400;
	height: 68px;
	padding: 0 15px;
	line-height: 1.3;
	border: 1px solid #eee;
}
td {
	border: 1px solid #eee;
	padding: 20px 15px;
	line-height: 1.5;
}
</style>