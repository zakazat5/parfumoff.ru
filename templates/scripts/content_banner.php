
<div id="content_banner">
    <a href="/catalog/usluga/podarochnaja-karta/" class="fl_left" style="width:207px;padding-left:15px;padding-top:5px">
        <img src="/images/top_banner/1.png" alt="">
        <div class="fl_left" style="padding-top:3px">Подарочные <br> карты</div>
    </a>
    <a href="/discount/usluga-imperial-set/" class="fl_left" style="width:249px;padding-left:10px;padding-top:5px">
        <img src="/images/top_banner/2.png" alt="">
        <div class="fl_left" style="width:97px;padding-top:3px">Услуга <br>Imperial Set</div>
    </a>
    <a href="/discount/ekspert/" class="fl_left" style="width:192px;padding-left:10px;position:relative;padding-top:5px;padding-right:10px">
        <img style="padding-top:5px" src="/images/top_banner/3.png" alt="">
        <div class="fl_left" style="padding-top:3px">Услуга <br>Expert</div>
    </a>
</div>

<style>
    #content_banner {
        -webkit-box-shadow: 0 0 5px 1px rgba(178,178,178,1);
        box-shadow: 0 0 5px 1px rgba(178,178,178,1);
        border: 1px solid #b2b2b2;
        border-top: 1px solid #e8e8e8;
        border-left: 1px solid #b6b7b7;
        border-right: 1px solid #b6b7b7;
        overflow: hidden;
        border-radius: 10px;
        height: 60px;
        font-family: Georgia;
        font-style: italic;
        font-size:17px;
        width:696px;
        line-height:22px;
    }
    #content_banner img {
        margin-right: 10px;
        float: left;
    }
    #content_banner a {
        height:60px;
        overflow:hidden;
    }
    #content_banner a:hover {
        background: #fffcdd;

    }
</style>
