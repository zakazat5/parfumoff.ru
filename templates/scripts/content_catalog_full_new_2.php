<?php require_once(__DIR__ . '/../../includes/geo_function.php'); ?>
<div class="pl25 pr25 aromat">
    <div class="breadcrumbs fs14 mb30">
        <a class="td_underline" href="/production/">Каталог продукции</a>
        <span>&gt;</span>
        <a class="td_underline" href="/production/<?=$this->brend['url']?>/"><?=$this->brend['title']?></a>
        <span class="">&gt;</span>
        <span><?=$this->catalog_full['title']?></span>
    </div>

    <h1 class="listing_title" style="float: left">
		<span class="listing_title__item mr10">Духи <?= $this->brend['title'] ?> <?= $this->catalog_full['title'] ?></span>
    </h1>    
    <div class="clear"></div>
    <span style="top:-25px" class="pr50 relative fs14 pol_sort_line"><?=$this->catalog_full['pol']?> аромат</span>
    <div class="clear"></div>

    <? require_once('partials/sort_rate_new.php');?>

    <div class="row top_aromat">

        <div class="col-sm-4 col-xs-12">
            <div class="aromat_img mb30">
                <img id="product_cat_<?php echo $this->catalog_full['id_catalog']; ?>" src="<?=$this->catalog_full['img']; ?>" alt="<?php echo $this->catalog_full['title']; ?>" />
            </div>

        </div>

        <div class="col-xs-12 col-sm-8">
            <div class="listing_sort mobile">
                <div class="top">
                    <span class="sort_title d_inline-block mr25">Рейтинг:</span>
                    <div class="stars d_inline-block mr30 relative" style="top:6px">
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_ua"></div>
                    </div>
                </div>
                <span class="sort_lh18 ml30 mr10 d_inline-block"><span class="br_line">Ароматы </span>в наличии</span>
                <input type="checkbox" class="filter_aromats" />
                <span class="ml10 sort_lh18 d_inline-block"><span class="br_line">Все</span> ароматы</span>
            </div>
            <?=$this->render('content_catalog_full/_aromats.php')?>
            
            <?php if($this->is_order==1) { ?>
            <div style="font-size:15px;">
                <a href="/production/<?php echo $this->brend_url; ?>/<?php echo $this->catalog_full['url']; ?>/">Посмотреть все варианты и цены для <?php echo $this->catalog_full['title']; ?></a>
            </div>
            <?php } ?>



            </div>

    </div>
	<div class="middle_cart_product">
		<ul class="plus_wrap wide">
                <li class="i1">
                    <a>
                        <i></i>

                        <div>
                            <span class="bold">Оригинальная</span> парфюмерия
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>В ассортименте нашего магазина представлена только оригинальная парфюмерия! Без исключений! В разделе "Качество и гарантии" Вы можете ознакомиться с примерами сертификатов и деклараций соответствия.</noindex>
                    </div>
                </li>
                <li class="i2">
                    <a>
                        <i></i>

                        <div>
                            Широкий <span class="bold">ассортимент</span>. <br>
                            Более 19000 товаров
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>Колоссальный ассортимент нашего магазина очередной раз подтверждает оригинальное происхождение продукции. У нас есть все - духи, туалетная вода, дезодоранты, гели для душа, крема для тела, лосьоны, бальзамы, шампуни, подарочные наборы.</noindex>
                    </div>
                </li>
                <li class="i3">
                    <a>
                        <i></i>

                        <div>
                            <span class="bold">Официальные</span> дистрибьюторы<br>
                            и надежные поставщики
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>Мы более 8 лет сотрудничаем с официальными дистрибьюторами и другими надежными поставщиками парфюмерии на территории РФ.</noindex>
                    </div>
                </li>
                <li class="i4">
                    <a>
                        <i></i>

                        <div>
                            Легкий <span class="bold">возврат</span> денег
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>В течение 14 дней с момента получения парфюмерии Вы можете вернуть или обменять товар с учетом сохранения всех товарных ценностей.</noindex>
                    </div>
                </li>
                <li class="i5">
                    <a>
                        <i></i>

                        <div>
                            Доставка <span class="bold">в течение суток</span> <br>
                            в 20 городах России
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>В 20 городах России доставка возможна уже на следующий рабочий день после заказа! Москва, Санкт-Петербург, Белгород, Брянск, Владимир, Вологда, Иваново, Калуга, Кострома, Нижний Новгород, Орел, Рязань, Смоленск, Тамбов, Тверь, Тула, Ярославль и др.</noindex>
                    </div>
                </li>
                <li class="i6">
                    <a>
                        <i></i>

                        <div>
                            Более <span class="bold">300 пунктов</span> самовывоза по России
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>По всей территории России свои заказы Вы можете получать в пунктах самовывоза. У нас более 300 пунктов, и их список постоянно расширяется.</noindex>
                    </div>
                </li>
                <li class="i7">
                    <a>
                        <i></i>

                        <div>
                            Лучшие цены и накопительная<br>
                            <span class="bold">система скидок</span>
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>Мы нацелены на долгосрочные отношения с нашими клиентами, поэтому всегда держим самые лучшие цены. А накопительная система скидок дает возможность совершать покупки с выгодой до 15%.</noindex>
                    </div>
                </li>
        </ul>
			<script>
                $(function () {
                    $('.plus_wrap.sb .li_popup').each(function (index, item) {
                        var
                            e = $(item),
                            e_h = e.outerHeight() / 2

                        e.css('margin-top', -e_h)

                    })
                })
            </script>
		<div class="middle_cart_product_description">
            <?= $this->catalog_full['text'] ?>
        </div>
	</div>
	<div class="rees46 recommend similar"></div>
    </div>

    <?=$this->render('content_catalog_full/_tabs.php')?>
<!--
    <div class="mb20">
    <?php //echo $this->body_text; ?>
    </div>
-->
    <div id="no_aromat_popup" style="display:none">
        <span class="close">&times;</span>
        <form action="" method="post" >
            <div class="mb20"><input type="text" name="name" placeholder="Имя" /></div>
            <div class="mb20"><input type="text" name="phone" placeholder="Телефон" /></div>
            <div class="mb20"><input type="text" name="email" placeholder="e-mail" /></div>
            <input type="hidden" name="articul" id="modal_articul">
            <button name="waitlist" value="1" class="btn2">Сообщить о поступлении</button>
        </form>
    </div>
    