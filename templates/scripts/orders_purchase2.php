
<body>
<? require_once('partials/data_layer.php'); ?>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PX333L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PX333L');</script>
<!-- End Google Tag Manager -->
<link rel="stylesheet" href="/templates/css/cart/cart.css" />
<link rel="stylesheet" href="/templates/css/checkout.css" />

<script src="/templates/js/checkout.js"></script>
<script>
    ckt.init({autodetection_city: false, autorun: false, show_map_list: true});
</script>

<script src="/templates/css/cart/cart.js"></script>

<script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>

<!-- CallTouch-->
<script type="text/javascript">
function ct(w,d,e,c){
var a='all',b='tou',src=b+'c'+'h';src='m'+'o'+'d.c'+a+src;
var jsHost="https://"+src,s=d.createElement(e),p=d.getElementsByTagName(e)[0];
s.async=1;s.src=jsHost+"."+"r"+"u/d_client.js?param;"+(c?"client_id"+c+";":"")+"ref"+escape(d.referrer)+";url"+escape(d.URL)+";cook"+escape(d.cookie)+";";
if(!w.jQuery){var jq=d.createElement(e);
jq.src=jsHost+"."+"r"+'u/js/jquery-1.7.min.js';
jq.onload=function(){
p.parentNode.insertBefore(s,p);};
p.parentNode.insertBefore(jq,p);}else{
p.parentNode.insertBefore(s,p);}}
if(!!window.GoogleAnalyticsObject){window[window.GoogleAnalyticsObject](function(tracker){
if (!!window[window.GoogleAnalyticsObject].getAll()[0])
{ct(window,document,'script', window[window.GoogleAnalyticsObject].getAll()[0].get('clientId'))}
else{ct(window,document,'script', null);}});
}else{ct(window,document,'script', null);}
</script>



<div id="user_popup">
    <div class="top_search">
        <form name="form" enctype="multipart/form-data" method="post" action="/find/" class="search_form">
            <input name="find" type="text" placeholder="Поиск товара по названию">
            <button type="submit">
                <i class="icon-search"></i>
            </button>
        </form>
    </div>
</div>

<div id="phone_popup" class="ta_center">
    <a href="tel:{телефон}" class="color_blue d_block td_underline fs26 cursor_pointer mb25 bold">{телефон}</a>
	<!--
    <a href="tel:8 (812) 426-11-15" class="color_blue d_block td_underline fs26 cursor_pointer mb25 bold">8 (812) 426-11-15</a>
	-->
    <div><a href="" class="color_blue dashed callMe-js" onclick="$('body').click()">Позвоните мне</a></div>
</div>

<header class="orders_header">
    <div class="container">
        <div id="logo"><a href="/">Парфюмофф.ру</a></div>
        <div id="top_phones" class="call_phone_top_1">
            <div class="mb5 ta_right phone_item">{город} <span class="pl5 fs24  phone_alloka">{телефон}</span></div>
			<!--
            <div class="mb5 ta_right phone_item">Санкт-Петербург <span class="pl5 fs24 ">8 (812) 426-11-15</span></div>
			-->
            <div id="callme"><a id="phone__callback" href="" class="btn2">Позвоните мне</a></div>
        </div>
    </div>
</header>



<div class="container">

    <form onsubmit="yaCounter21451555.reachGoal('order_step2_er');" autocomplete="off" action="/korzina/" method="post" enctype="multipart/form-data" name="orders_list" id="orders">
        <div>

            <div class="row">
                <div class="b-step col-sm-12 col-md-10 hidden-xs">
                    <div class="clearfix">
                        <p class="pull-left fs20 ml10">1 этап: Контактные данные</p>
                        <p class="pull-right fs20 mr10 color_green">2 этап: Адрес и способы доставки</p>
                    </div>
                    <div class="step-border">
                        <div class="step-border__green" style="right:0;"></div>
                        <div class="step-border__circle circle1"></div>
                        <div class="step-border__circle circle2 bd_green"></div>
                        <div class="step-border__circle circle3 bd_green"></div>
                    </div>
                </div>
            </div>

            <div class="cart_hello_text">
                На данном шаге мы уже видим Ваш заказ. Если Вы затрудняетесь с выбором способа
                доставки, вскоре мы сами перезвоним Вам и поможем завершить оформление заказа.
            </div>

            <div id="login" style="display:none">
                <span class="close">&times;</span>
                <form action="">
                    <div class="mb20"><input type="text" placeholder="Имя" /></div>
                    <div class="mb20"><input type="text" placeholder="Телефон" /></div>
                    <button class="btn2">Сообщить о поступлении</button>
                </form>
            </div>


            <div id="register" style="display:none">
                <span class="close">&times;</span>
                <form action="">
                    <div class="mb20"><input type="text" placeholder="Имя" /></div>
                    <div class="mb20"><input type="text" placeholder="Телефон" /></div>
                    <button class="btn2">Сообщить о поступлении</button>
                </form>
            </div>
			<!--
            <div id="callMe" class="popup" style="padding-top:15px;display:none">
                <span class="close">&times;</span>
                <div class=" fs34 mb35 lite">Заказать звонок</div>
                <form method="post" action="/call_me/">
                    <div class="mb20"><input type="text" name="name" placeholder="Имя" /></div>
                    <div class="mb20"><input type="text" class="phone_input" name="phone" placeholder="Телефон" /></div>
                    <button class="btn2 ">Перезвоните мне</button>
                </form>
            </div>
			-->
            <div id="fast_view" class="popup" style="display:none">
                <span class="close">&times;</span>
                <div class="fast_view__content">

                </div>
            </div>





            <script>
                $(function(){
                    //index suggest
                    AddressSuggestions.initForm();
                })
            </script>
            <?
            $cart = $this->cart;
            /** @var array $user */
            $user = $cart->getUser();
            ?>

            <div id="cart_step2" class="b-delivery container">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="row" style=" background: #f5f5f5; margin: 0 -11px; padding: 0 15px;border-top: 1px solid #00d5b4!important;border-bottom: 1px solid #00d5b4 !important;padding-bottom: 10px;">
                            <div class="b-delivery__title mb20 pt20">В какой регион и населенный пункт будем доставлять заказ?</div>
                            <div class="row">
                                <label class="col-xs-4 col-sm-3 col-md-3" for="">Регион</label>
                                <div class="col-xs-8 col-sm-5 col-md-5">
                                    <input value="<?php echo $user['address_region']; ?>" onclick="this.value=''" type="text" name="region">
                                </div>
                                <div class="clearfix"></div>
                                <label class="col-xs-4 col-sm-3 col-md-3 other" for="">Другой</label>
                                <div class="col-xs-8 col-sm-5 col-md-5 other">
                                    <input  type="text" name="other">
                                </div>
                                <div class="clearfix"></div>
                                <label class="col-xs-4 col-sm-3 col-md-3" for="">Город / Район</label>
                                <div class="col-xs-8 col-sm-5 col-md-5">
                                    <input value="<?php echo $user['address_city']; ?>" onclick="this.value=''" type="text" name="city" id="step2_town">
                                </div>
                                <div class="clearfix"></div>
                                <label class="col-xs-4 col-sm-3 col-md-3 settlement" style="display:none" for="">Населенный пункт</label>
                                <div class="col-xs-8 col-sm-5 col-md-5 settlement" style="display:none">
                                    <input  type="text" value="<?php echo $user['address_settlement']; ?>" name="settlement" id="step2_settlement">
                                </div>
                            </div>
                        </div>

                        <div class="b-delivery__wrap">
                            <div class="b-delivery__title">Выберите удобный для Вас способ доставки:</div>
                            <div id="load" style="text-align: center; position: absolute; width: 100.3%; top: 60px; left: -1px; background: white; z-index: 999; height: 100%; border: 2px solid #f5f5f5;display: none;"><img style="margin-top: 140px;" src="/templates/images/loader.gif"/></div>
                            <table class="b-delivery__table">
                                <tr class="b-delivery__table_main">
                                    <td class="bd__kind">Тип доставки</td>
                                    <td class="bd__data">Срок</td>
                                    <td class="bd__cost">Цена</td>
                                    <td class="bd__select hidden-xs">Выбрать</td>
                                </tr>
                                <tr class="item_js b-delivery__courier tr_delivery_item express" id="courier__delivery" data-id="" style="display:none;">
                                    <td><strong>Курьерская доставка</strong></td>
                                    <td><span class="days">1-2</span> дн.</td>
                                    <td>от <span class="cost">0</span> руб.</td>
                                    <td class="hidden-xs">
                                        <div class="cart_checkbox"></div>
                                    </td>
                                </tr>
                                <tr class="hide hide_this_block full_tr express" id="courier__delivery_full">
                                    <td colspan="4" style="padding: 20px 0 10px;">
                                        <div class="dop_info">
                                            <div class="line line1 fl_left" style="margin-right:15px;width:240px">
                                                <label style="width:auto;padding:0" for="">Улица:</label>
                                                <input name="street" value="" id="" type="text" style="width:216px;padding: 0 6px;" autocomplete="off">
                                            </div>
                                            <div class="line line2 fl_left" style="margin-right:15px;width:80px">
                                                <label style="width:auto;padding:0" for="">Дом:</label>
                                                <input name="house" value="" type="text" style="width:68px;padding:6px;">
                                            </div>
                                            <div class="line line3 fl_left" style="width:120px;margin-right:15px">
                                                <label style="width:auto;padding:0" for="">Корпус:</label>
                                                <input name="corpus" value="" type="text" style="width:108px;padding:6px;">
                                            </div>

                                            <div class="line line4 fl_left" style="width:120px">
                                                <label style="width:auto;padding:0" for="">Строение:</label>
                                                <input name="stroenie" value="" type="text" style="width:113px;padding:6px;">
                                            </div>
                                            <div class="clear"></div>
                                            <div class="fl_left line5 line" style="margin-right:15px;width:105px">
                                                <label style="width:auto;padding:0" for="">Номер квартиры:</label>
                                                <input name="flat" value="" type="text" style="width:86px;padding:6px;">
                                            </div>
                                            <div class="line line6 fl_left" style="width:117px;margin-right:17px;">
                                                <label style="width:auto;padding:0" for="">Номер домофона:</label>
                                                <input name="domophone" value="" type="text" style="width:100px;padding:6px;">
                                            </div>
                                            <div class="line line7 fl_left" style="width:81px;margin-right:15px">
                                                <label style="width:auto;padding:0;height:32px" for="">Индекс:</label>
                                                <input name="regs_postcode" value="" type="text" style="width:69px;padding:6px;">
                                            </div>
                                            <div class="line fl_left metro" style="width: 260px; display: block;">
                                                <label style="width:auto;padding:0;height:32px" for="">Ближайшая станция метро:</label>
                                                <select name="metro" style="width: 233px; border: 1px solid rgb(206, 204, 205); padding: 8px;">
                                                    <option value="0">Не выбрано</option>
                                                    <? foreach($this->aMetro as $metro):?>
                                                        <option class="metro-1 metro-option" value="<?=$metro['id']?>"><?=$metro['name']?></option>
                                                    <? endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="hide full_tr_btn express">
                                    <td colspan="4">
                                        <button name="complete" class="btn1 cart_btn1 fs18" style="cursor:pointer">Отправить заказ</button>
                                    </td>
                                </tr>

                                <tr class="separator express">
                                    <td colspan="4"></td>
                                </tr>

                                <tr class="item_js tr_delivery_item punkts pvz" id="self__delivery_pvz" data-id="" style="display:none;">
                                    <td><strong>Пункты самовывоза</strong></td>
                                    <td><span class="days">1-2</span> дн.</td>
                                    <td>от <span class="cost">0</span> руб.</td>
                                    <td class="hidden-xs">
                                        <div class="cart_checkbox"></div>
                                    </td>
                                </tr>
                                <tr class="hide full_tr_self full_tr_btn pvz">
                                    <td colspan="4">
                                        <button name="complete" class="btn1 cart_btn1 fs18" style="cursor:pointer">Отправить заказ</button>
                                    </td>
                                </tr>

                                <tr class="separator pvz">
                                    <td colspan="4"></td>
                                </tr>

                                <tr class="item_js tr_delivery_item punkts postamat" id="self__delivery_postamat" data-id="" style="display:none;">
                                    <td><strong>Постаматы (автомат. пункты выдачи)</strong></td>
                                    <td><span class="days">1-2</span> дн.</td>
                                    <td>от <span class="cost">0</span> руб.</td>
                                    <td class="hidden-xs">
                                        <div class="cart_checkbox"></div>
                                    </td>
                                </tr>
                                <tr class="hide full_tr_self full_tr_btn postamat">
                                    <td colspan="4">
                                        <button name="complete" class="btn1 cart_btn1 fs18" style="cursor:pointer">Отправить заказ</button>
                                    </td>
                                </tr>

                                <tr class="separator postamat">
                                    <td colspan="4"></td>
                                </tr>

                                <tr class="item_js b-delivery__courier tr_delivery_item mail" id="pochta__delivery" data-id="" >
                                    <td><strong>Почта России (наложенный платеж)</strong></td>
                                    <td><span class="days">---</span></td>
                                    <td>от <span class="cost">уточняйте у оператора</span></td>
                                    <td class="hidden-xs">
                                        <div class="cart_checkbox"></div>
                                    </td>
                                </tr>
                                <tr class="hide hide_this_block full_tr mail" id="pochta__delivery_full">
                                    <td colspan="4" style="padding: 20px 0 10px;">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="col-xs-7">
                                                    <label class="col-xs-2 one_line" for="">Улица</label>
                                                    <div class="col-xs-10">
                                                        <input name="street" value="" type="text" autocomplete="off">
                                                    </div>

                                                </div>
                                                <div class="col-xs-5 pl0 pr0">
                                                    <div class="col-xs-5 pl0">
                                                        <label class="" for="">Дом</label>
                                                        <input name="house" class="input-house" value="" type="text">
                                                    </div>
                                                    <div class="col-xs-6 pr0">
                                                        <label class="" for="">Корпус</label>
                                                        <input name="corpus" class="input-corpus" value="" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 pt10">
                                                <div class="col-xs-3 pr0">
                                                    <label class=" " for="">Строение</label>
                                                    <input name="stroenie" class="input-stroenie" value="" type="text">
                                                </div>
                                                <div class="col-xs-3 pr0">
                                                    <label class="" for="">Квартира</label>

                                                    <input name="flat" class="input-flat" value="" type="text">

                                                </div>
                                                <div class="col-xs-3 pr0">
                                                    <label class=" one_line" for="">Домофон</label>

                                                    <input name="domophone" class="input-domophone" value="" type="text">

                                                </div>
                                                <div class="col-xs-3">
                                                    <label class=" one_line" for="">Индекс</label>

                                                    <input name="regs_postcode" class="input-regs_postcode" value="" type="text">

                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="hide full_tr_btn mail">
                                    <td colspan="4">
                                        <button name="complete" class="btn1 cart_btn1 fs18" style="cursor:pointer">Отправить заказ</button>
                                    </td>
                                </tr>

                                <tr class="separator mail">
                                    <td colspan="4"></td>
                                </tr>




                            </table>
                        </div>


                    </div>






                    <div class="col-xs-12 col-md-4 b-order">
                        <div class="b-order__wrap">
                            <div class="b-order__title bold">Ваш заказ № <?=$this->order['id']?></div>
                            <? if (strpos($_COOKIE['utm_source_last'],'kupikupon')!==false ):?>
                                <img src="http://www.kupikupon.ru/affiliate_tracks?source=parfumoff&utm_medium=<?=$this->order['id']?>&comission=200" width="1" height="1" alt="" />
                            <? endif;?>
                            <? foreach ($this->order_items as $item_order):?>
							<? $mixmarket_ids[]=$item_order['id'];?>
                            <table class="b-order__table">
                                <tbody>

                                    <tr colspan="3" class="cart_td cart_">
                                        <td colspan="3" class="first_td pr10" style="padding-left:14px;">
                                            <span style="font-size:14px"><?=$item_order['title']?></span>
                                        </td>
                                    </tr>


                                <tr>
                                    <td style="padding-left:14px;">
                                        <div class="color_gray1">Цена</div>
                                        <span class="price"> <?=parsePrice(round(($item_order['sum'])+(($item_order['sum'])*$this->sell)))?></span> руб.
                                        </span>
                                    </td>

                                    <td>
                                        <div class="color_gray1">Кол-во</div>

                                        <div class="korz_kol-vo"><span class="val"><?=$item_order['kol'];?></span></div></td>

                                    <td>
                                        <div class="color_gray1">Стоимость</div>
                                        <span class="sum"><?=parsePrice($item_order['sum']*$item_order['kol']);?></span> руб.
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <? endforeach;?>
                            <table class="b-order__table" style="border-top:0">
                                <tr>
                                    <td  class="first_td " style="padding:14px">
                                        <div class="mb5">Комментарий к заказу:</div>
                                        <textarea placeholder="" name="pComment" id="" style="font-family:Tahoma;height:34px;padding:4px;width:100%;height: 80px;"></textarea>
                                    </td>
                                </tr>
                            </table>





                            <div class="b-order__total">
                                <div class="col-xs-7">Сумма заказа:</div>
                                <div class="col-xs-5"><span class="val_subTotal" id="big_sum"><?=$this->order['Sum']?></span> руб</div>

                                <div class="col-xs-7">Накопительная скидка:</div>
                                <div class="col-xs-5">
                                    <div class="nakop_sk" style="left:140px;"></div>
                                    <div class="nakop_id" style="right:auto;left:-185px;height: 150px; width: 430px; padding: 10px 20px; display: none;">
                                        <span style="font-size:18px;display:block;padding-bottom:16px" class="georgia_head">Накопительные скидки в магазине Parfumoff.ru:</span>
                                        3% от стоимости заказа при покупке на сумму от 3000 руб до 10 000 руб<br>
                                        5%  от стоимости заказа при покупке на сумму от 10 001 руб до 20 000 руб<br>
                                        7%  от стоимости заказа при покупке на сумму от 20 001 руб до 70 000 руб<br>
                                        10% от стоимости заказа при покупке на сумму от 70 001 руб до 139 999 руб<br>
                                        15% от стоимости заказа при покупке на сумму свыше 140 000 руб</div>
                                    <br />
                                    <?=$this->cart->getDiscountData('percent_d')?>%
                                </div>

                                <div class="col-xs-7">Доставка:</div>
                                <div class="col-xs-5">
                                    <span id="delivery_cost-text">0</span> <span>руб</span>
                                </div>

                                <input type="hidden" name="courier_address_id" id="courier_address_id" value="0"/>
                                <input type="hidden" name="delivery_cost" id="delivery_cost" value="0"/>
                                <input type="hidden" name="add_inf" id="add_inf" value=""/>
                                <input type="hidden" name="delivery_type" id="delivery_type" value=""/>

                                <div class="clear"></div>

                                <div class="itogo color_purple">
                                    <div class="col-xs-7">Итоговая сумма:</div>
                                    <div class="col-xs-5">
                                        <b><span class="val_total" id="big_sum_all"><?=$this->order['dSum']?></span></b> руб
                                        <input type="hidden" name="dSum" id="dSum" value="<?=$this->order['dSum']?>"/>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="btn-wrap relative">
                                    <button type="submit" name="complete" class="btn1 cart_btn1" >Отправить заказ</button>
                                    <div class="tester_popup" id="error_delivery_popup">
                                        Перед отправкой заказа, пожалуйста, проверьте регион и город доставки и выберите удобный способ доставки.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div> <!-- b-delivery -->

            <div id="dostavka_map" style="display: none">

            </div>

        </div>
    </form>
    <div style="display:none">
        <div class="box-modal" id="login_form_popup" style="background:white;width:295px;padding:45px 105px;">
            <a class="box-modal_close close1"><span></span></a>
            <div class="title">Вход в личный кабинет</div>
            <div style="margin-bottom:20px;">
                Для оформления заказа введите ваш логин (совпадает с email) и пароль, полученный при регистрации на сайте.
            </div>
            <form action="/users/login/" method="post" name="reg_form">
                <div class="line"><input name="log_email" type="text" placeholder="Логин" /></div>
                <div class="line"><input name="log_passw" type="password" placeholder="Пароль" /></div>
                <div class="forget"><a href="/users/forgot-password/">Забыли пароль?</a></div>
                <input type="hidden" name="back_url" value="/korzina/">
                <div style="text-align:center;"><button name="register" class="cart_btn1">Войти</button></div>
            </form>

        </div>
    </div>

<div id="callMe" class="popup" style="display:none">
    <span class="close">&times;</span>
    <div class=" fs34 mb35 lite">Заказать звонок</div>
    <form method="post" action="/thankyou_call/" name="callMe_form" onsubmit="return validate_callMe_form();">
        <div class="mb20"><input type="text" name="callMe_name" placeholder="Имя" /></div>
		<div id="callMe_form_name_err" class="mb20" style="display:none">Пожалуйста заполните поле 'Имя'</div>
        <div class="mb20"><input type="text" class="phone_input" name="callMe_phone" placeholder="Телефон" /></div>
		<div id="callMe_form_phone_err" class="mb20" style="display:none">Пожалуйста заполните поле 'Телефон'</div>
		<div class="d_button_callMe"><button class="btn2 ">Перезвоните мне</button></div>
    </form>
</div>

</div>
<?php if ($GLOBALS["user_segment"] == '1') { ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter21451555 = new Ya.Metrika({id:21451555,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21451555" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php } else { ?>
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter42164749 = new Ya.Metrika({
                    id:42164749,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/42164749" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php } ?>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    function getRetailCrmCookie(name) {
        var matches = document.cookie.match(new RegExp(
            '(?:^|; )' + name + '=([^;]*)'
        ));

        return matches ? decodeURIComponent(matches[1]) : '';
    }

    ga('create', 'UA-52259840-1', 'parfumoff.ru');
    ga('set', 'dimension1', getRetailCrmCookie('_ga'));
    ga('send', 'pageview');

</script>

<!--Трэкер-контейнер "Основной"--> 
<script type="text/javascript">
window.__mixm__ = window.__mixm__ || [];
window.__mixm__.push(['mAdvId',1294983815]);
// только на страницах карточки товара передавайте его айди (вместо 'ID товара') из вашего прайса и раскомментируйте вызов этого параметра
window.__mixm__.push(['skulist', '<?php echo $this->catalog_full['id']; ?>']);

(function(){function t(){if(!e){e=1;var t=0,a="def";for(i=0;o.__mixm__.length>i;i++){if("uAdvArId"==o.__mixm__[i][0]){t="u"+o.__mixm__[i][1];break}"mAdvId"==o.__mixm__[i][0]&&(a="m"+o.__mixm__[i][1])}t||(t=a);var n=document.createElement("script");n.type="text/javascript",n.async=!0,n.src=("https:"==document.location.protocol?"https://":"http://")+"js.mixmarket.biz/a"+t+".js?t="+(new Date).getTime();var r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(n,r)}}var e=0,a=document,n=a.documentElement,o=window;"complete"==a.readyState||"loaded"==a.readyState||"interactive"==a.readyState?t():a.addEventListener?a.addEventListener("DOMContentLoaded",t,!1):a.attachEvent?(n.doScroll&&o==o.top&&function(){try{n.doScroll("left")}catch(e){return setTimeout(arguments.callee,0),void 0}t()}(),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t()})):o.onload=t})();
</script> 
<!--Трэкер-контейнер "Основной"-->
<!--Трэкер "Покупка"--> 
<script type="text/javascript">
window.__mixm__.push(['pay',{skulist:'<?php echo implode(",",$mixmarket_ids) ?>'}]);
</script> 
<!--Трэкер "Покупка"-->
<script>
function validate_callMe_form ()
{
	var valid = true;
    if ($.trim($("input[name='callMe_name']").val()) == "" )
    {
        $('#callMe_form_name_err').show();
        valid = false;
    }
	 if ($.trim($("input[name='callMe_phone']").val()) == "" )
    {
        $('#callMe_form_phone_err').show();
        valid = false;
    }
    return valid;
}
</script>