<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Paco Rabanne</title>
	<meta http-equiv="content-type" content="text/html; charset=windows-1251" />
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="stylesheet" href="/templates/css/project/1-million.css" type="text/css" media="screen" />

	<script type="text/javascript" src="/templates/js/jquery.js"></script>
	<script type="text/javascript" src="/templates/js/jquery.timers.js"></script>
	<script type="text/javascript" src="/templates/js/project/1-million.js"></script>
	<!--[if IE 6]>
	<script type="text/javascript" src="/templates/js/pngfix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('.opacity'); 
	</script>
	<![endif]-->
</head>

<body>

<div id="wrapper">

	<div id="content">
		<h1 id="logo"><a href="http://enigme.ru/offers/1-million/">Paco Rabanne</a></h1>

		<div id="left_wrap">
			<h2 class="opacity">1 MILLION</h2>
			<div id="text_wrap">
				Стремление к шику и власти, мечты о богатстве<br /> и славе свойственны целеустремленным личностям,<br /> выбирающим из широкого
				парфюмерного ассорти<br /> новый пикантный аромат 1 Million, придуманный для<br /> самоуверенного мужчины. Насыщенный дерзкими<br />

				нотами и облаченный во флакончик в форме золотого<br /> слитка аромат 1 Million должен быть неотъемлеммым<br />
				атрибутом стильного, удачливого	мужчины.<br />
				<h3>Ноты аромата</h3><br class="clear" />
				перечная мята, красный мандарин, корица,<br /> абсолю розы, кардамон, амбра, кожа &nbsp;
			</div>

		</div>
		<a id="enigme" href="http://enigme.ru/">Спецпроект Parfumoff.ru</a>
		<div id="right_wrap">
			<?php if(isset($this->message)){ ?>
				<div id="form" style="display:block;">
				<div id="table" style="display:block;">
					<span><?php echo $this->message;?></span>
				</div>
				</div>
					
			<?php } else {?>
				<div id="photo"></div>
				<div id="form">
				
				<div id="table">
					<span>КОЛИЧЕСТВО</span><span>СУММА ЗАКАЗА</span>
					<form autocomplete="off" action="/offers/1-million/send/" method="post" enctype="multipart/form-data" name="offers_send">
						<input name="kol" maxlength="2" id="kol" type="text" onfocus="this.value='';" value="1" />
						<div id="sum"><b>1470</b> руб <a id="re">пересчитать</a></div>
						<div id="inputs">
							<input name="fio" type="text" id="name" class="inputs" onfocus="if(this.value=='Ваше имя: *') this.value='';" onblur="if(this.value=='') this.value='Ваше имя: *';" value="Ваше имя: *" />
							<input name="phone" type="text" id="phone_input" class="inputs" onfocus="if(this.value=='Телефон: *') this.value='';" onblur="if(this.value=='') this.value='Телефон: *';" value="Телефон: *" />
							<input name="address" type="text" id="adres" class="inputs" onfocus="if(this.value=='Адрес доставки: *') this.value='';" onblur="if(this.value=='') this.value='Адрес доставки: *';" value="Адрес доставки: *" />

							<p>* - обязательно поле для заполнения</p>
							<input name="email" type="text" class="inputs" onfocus="if(this.value=='E-mail') this.value='';" onblur="if(this.value=='') this.value='E-mail';" value="E-mail" />
							<textarea name="comment" class="inputs" onfocus="if(this.value=='Комментарии к заказу:') this.value='';" onblur="if(this.value=='') this.value='Комментарии к заказу:';" id="com" value="Комментарии к заказу:">Комментарии к заказу:</textarea>
							<input name="submit" type="submit" id="gogogo" value="ОТПРАВИТЬ ЗАКАЗ" />
						</div>
					</form>
				</div>
				</div>
				
				<?php } ?>
			

		</div>
		<div id="bottom_wrap">
			<div id="price">1470 рублей за 100 мл</div>
			<a id="buy">КУПИТЬ!</a>
			<span>(495) 220 21 95</span>
		</div>
	</div><!-- #content-->

	<div id="footer">
		&copy; Parfumoff.ru<br />
		Все права на материалы, представленные на сайте, принадлежат Parfumoff.ru. Копирование информации запрещено.<br />
		г. Москва, ул. Олеко Дундича, д. 3
	</div><!-- #footer -->

</div><!-- #wrapper -->

</body>

</html>