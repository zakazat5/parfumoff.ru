<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Nina Ricci Nina Edition Prestige - 1490 руб | 80 ml. Купить духи Нина Ричи Nina Edition Prestige - Parfumoff.ru</title>
	<meta http-equiv="content-type" content="text/html; charset=windows-1251" />
	<meta name="description" content="" />

	<link rel="stylesheet" href="/templates/css/project/NinaRicci.css" type="text/css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/js/fancy/jquery.fancybox-1.2.5.css" media="screen" />
	
	<script type="text/javascript" src="/js/jquery/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery/jquery.pngFix.pack.js"></script>
	<script type="text/javascript" src="/js/fancy/jquery.fancybox-1.2.5.pack.js"></script>
	<script type="text/javascript" src="/js/jquery/jquery.timers.js"></script>
	<script type="text/javascript" src="/templates/js/project/NinaRicci.js"></script>

</head>

<body>

<div id="wrapper">
	
	<div id="header">
		<div id="phone">
			<span>ТЕЛЕФОН:</span> +7 (495) 220-21-95
		</div>
		<h1 id="logo"><a href="http://parfumoff.ru/offers/nina-ricci/">Nina Ricci Edition Prestige</a></h1>
		<a href="http://parfumoff.ru/" id="enigme"></a>
		<div id="flakon"></div>
		<div id="spec_wrap" class="h80">
			<div id="non_op"></div>	
				<div id="cont_1">
					<span>СПЕЦИАЛЬНАЯ ЦЕНА</span>
					<div class="price">1490 <span>руб (80 мл)</span></div>
					<span class="buy">КУПИТЬ!</span>
				</div>
				<div id="cont_2">
					<div id="img_wr"><img src="/images/NinaRicci/1.jpg"title="" alt="" /></div>
					<div id="kol_d">КОЛИЧЕСТВО</div>
					<div id="sum_d">СУММА ЗАКАЗА</div>
					
					<form autocomplete="off" action="/offers/nina-ricci/send/" method="post" enctype="multipart/form-data" name="offers_send">
					
					<div id="kol"><input type="text" name="kol" value="1" maxlength="2" /></div>
					<div id="sum">1490 (руб)</div>
					<a href="#" id="re">пересчитать</a>
				</div>
		</div>
		<div id="text_wrap">
			<div id="flower"></div>
			<?php if(isset($this->message)){ ?>
			<p style="font-size:15px;line-height:18px;font-weight:bold;">
			<?php echo $this->message;?>
			</p>
			<?php } else {?>
			<p>
			Волшебное рождественское утро... Чуть тронутое морозом красное яблоко,
			покрытое хрустальным инеем, таит в себе волшебный аромат. Фонтан зимней
			свежести, раскрываясь начальными нотами калабрианского лайма и лимона
			растворяется в хороводе нот яблока, пиона, лунного цветка и пралине.
			А сердце аромата согревают теплые аккорды яблочного дерева и белого 
			кедрового мускуса.
			</p>
			<p>
			Это Nina Edition Prestige - ограниченный рождественский выпуск любимого аромата
			Nina в новом изящном оформлении. Флакон в форме яблока насыщенного алого цвета, 
			сверкает перламутром, словно драгоценное елочное украшение и зовет сделать шаг навстречу 
			этой зимней сказке.
			</p>
			<?php } ?>
		</div>
		<div id="cont_3">
		
			<input id="name" type="text" name="fio" value="Ваше имя: *" onfocus="if(this.value=='Ваше имя: *') this.value='';" onblur="if(this.value=='') this.value='Ваше имя: *';" />
			<input id="phone_input" type="text" name="phone" value="Телефон: *" onfocus="if(this.value=='Телефон: *') this.value='';" onblur="if(this.value=='') this.value='Телефон: *';" />
			<input id="adres" type="text" name="address" value="Адрес доставки: *" onfocus="if(this.value=='Адрес доставки: *') this.value='';" onblur="if(this.value=='') this.value='Адрес доставки: *';" />
			<div id="form_desc"></div>
			<input id="email" type="text" name="email" value="E-mail" onfocus="if(this.value=='E-mail') this.value='';" onblur="if(this.value=='') this.value='E-mail';" />
			<textarea name="comment" onfocus="if(this.value=='Комментарии к заказу:') this.value='';" onblur="if(this.value=='') this.value='Комментарии к заказу:';">Комментарии к заказу:</textarea>
			<input type="submit" id="submit" name="submit" value="ОТПРАВИТЬ ЗАКАЗ" />
			
			</form>

			
		</div>

		<div id="gallery">
			<div id="left_arrow"></div>
			<div id="right_arrow"></div>
			<div id="inner">
				<a href="/images/NinaRicci/big/01.jpg" rel="group" class="zoom1"><img src="/images/NinaRicci/mini/1.jpg" title="" alt="" /></a>
				<a href="/images/NinaRicci/big/02.jpg" rel="group" class="zoom1"><img src="/images/NinaRicci/mini/2.jpg" title="" alt="" /></a>
				<a href="/images/NinaRicci/big/03.jpg" rel="group" class="zoom1"><img src="/images/NinaRicci/mini/3.jpg" title="" alt="" /></a>
			</div>
		</div>
	</div><!-- #header-->
	<div id="footer">
		<div id="copy">
			<p>&copy; Parfumoff.ru</p>
			<p>Все права на материалы, предствленные на сайте, принадлежат Parfumoff.ru. Копирование информации запрещено</p>
			<p>г. Москва, ул. Олеко Дундича, д. 3</p>
		</div>
		<div id="counter_wrap">
			
<script type="text/javascript">document.write("<a href='http://www.liveinternet.ru/click' "+
"target=_blank><img src='http://counter.yadro.ru/hit?t14.1;r"+
escape(document.referrer)+((typeof(screen)=="undefined")?"":
";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
";"+Math.random()+
"' alt='' title='LiveInternet: показано число просмотров за 24"+
" часа, посетителей за 24 часа и за сегодня' "+
"border='0' width='88' height='31'><\/a>")</script><!--/LiveInternet-->
		</div>
	</div><!-- #footer -->

	

	

</div><!-- #wrapper -->

</body>
</html>