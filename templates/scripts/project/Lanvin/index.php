<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Lanvin Eclat d'Arpege по лучшей цене!</title>
	<meta http-equiv="content-type" content="text/html; charset=windows-1251" />
	<meta name="title" content="Lanvin Eclat D'Arpege - 1530 руб | 100 ml. Купить духи Ланвин Eclat D'Arpege - Enigme.ru" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<link rel="stylesheet" href="/templates/css/project/lanvin.css" type="text/css" media="screen" />
	
	<script type="text/javascript" src="/templates/js/jquery.js"></script>
	<script type="text/javascript" src="/templates/js/jquery.timers.js"></script>
	<script type="text/javascript" src="/templates/js/project/lanvin.js"></script>
	<!--[if IE 6]>
	<script type="text/javascript" src="/templates/js/pngfix.js"></script>
	<script type="text/javascript">
		DD_belatedPNG.fix('#ugol_right, #top_left_ugol, #price_ugol'); 
	</script>
	<![endif]-->
</head>

<body>

<div id="wrapper">

	
	<div id="middle">

		<div id="container">
			<div id="content">
				<div id="top_left_ugol"></div>
				<img id="big" src="/images/default/lanvin/image.jpg" alt="" />
				<a href="http://parfumoff.ru/offers/lanvin-eclat/" id="image_a">Eclat d'Arpege туалетные духи</a>
				<div id="fon_cont">
					<div id="price_wrap">
						<div id="price_ugol"><div><span>1530</span>РУБ</div></div>
						<div id="vert_line">|</div>
						<div id="price_ml"><div><span>100</span>МЛ</div></div>
						<div id="special">СПЕЦИАЛЬНАЯ ЦЕНА</div>
						<span id="to_buy">КУПИТЬ!</span>
					</div>
				</div>
			</div><!-- #content-->
		</div><!-- #container-->

		<div class="sidebar sr">
			<h1 id="logo"><a href="http://parfumoff.ru/offers/lanvin-eclat/"></a></h1>
			<div id="ugol_right"></div>
			<div id="text">
			<?php if(isset($this->message)){ ?>
				<p style="font-size:13px;line-height:18px;font-weight:normal;">
				<?php echo $this->message;?>
				</p>
			<?php } else {?>
				<p>Когда в далеком 1927 году Джейн Ланвин создала аромат Arpege в честь своей дочери Маргариты, она вряд ли представляла
				себе, что он прославится на века. Вдохновленный классическим парфюмом торговый дом Lanvin совместно с дизайнером
				Карин Дабриель выпустил обновленну версию шедевра, аромат Eclat d'Arpege. Как и его предшественник, новый парфюм был создан,
				под влиянием всей любви и нежности, на какое способна женщина. Ее чувства, подобно цветочному букету переливаются нотами и 
				оттенками.</p>
				<p>Аромат начинается нотами сирени и сицилийского лимона, которые затихая, уступают силу звучания цветам персика, глицинии, 
				красного пеона, китайского османтуса и прохладной ноте зеленого чая. Завершают композицию белый ливанский кедр, сладкий 
				мускус и драгоценная амбра.</p>
				<p>Lanvin прославляет женщину, ее мир, ее женственность. И символ рассвета женской красоты - материнство, запечатлен на изящном
				сферическом флаконе, выполненном в стиле ар-деко.</p>
			<?php } ?>
			</div>
			<div id="form">
				<div id="table">
					<span>КОЛИЧЕСТВО</span><span>СУММА ЗАКАЗА</span>
					<form autocomplete="off" action="/offers/lanvin-eclat/send/" method="post" enctype="multipart/form-data" name="offers_send">
						<input name="kol" maxlength="2" id="kol" type="text" onfocus="this.value='';" value="1" />
						<div id="sum"><b>1530</b> руб <a id="re">пересчитать</a></div>
						<div id="inputs">
							<input name="fio" type="text" id="name" class="inputs" onfocus="if(this.value=='Ваше имя: *') this.value='';" onblur="if(this.value=='') this.value='Ваше имя: *';" value="Ваше имя: *" />
							<input name="phone" type="text" id="phone_input" class="inputs" onfocus="if(this.value=='Телефон: *') this.value='';" onblur="if(this.value=='') this.value='Телефон: *';" value="Телефон: *" />
							<input name="address" type="text" id="adres" class="inputs" onfocus="if(this.value=='Адрес доставки: *') this.value='';" onblur="if(this.value=='') this.value='Адрес доставки: *';" value="Адрес доставки: *" />
							<p>* - обязательно поле для заполнения</p>
							<input name="email" type="text" class="inputs" onfocus="if(this.value=='E-mail') this.value='';" onblur="if(this.value=='') this.value='E-mail';" value="E-mail" />
							<textarea name="comment" class="inputs" onfocus="if(this.value=='Комментарии к заказу:') this.value='';" onblur="if(this.value=='') this.value='Комментарии к заказу:';" id="com" value="Комментарии к заказу:">Комментарии к заказу:</textarea>
							<input name="submit" type="image" id="gogogo" src="/images/default/lanvin/go.png" />
						</div>
					</form>
				</div>
			</div>
		</div><!-- .sidebar.sr -->

	</div><!-- #middle-->

	<div id="footer">
		<div id="phone">| <span>(495) 220 21 95</span> | <span><a href="http://parfumoff.ru/"><img src="/images/default/lanvin/enigme.png" alt="parfumoff.ru" /></a></span> |</div>
		&copy; Parfumoff.ru<br />
		Все права на материалы, представленные на сайте, принадлежат Parfumoff.ru. Копирование информации запрещено.<br />
		г. Москва, ул. Олеко Дундича, д. 3
	</div><!-- #footer -->

</div><!-- #wrapper -->

</body>
</html>