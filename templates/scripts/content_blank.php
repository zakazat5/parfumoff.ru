<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<? require_once('partials/head_files.php'); ?>
<body>


<div class="container">
    <?php echo $this->main['content']; ?>
    <? require_once('partials/social_block.php');?>
</div>

<? require_once('partials/footer.php'); ?>

</body>
</html>