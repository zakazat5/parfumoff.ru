<div class="bread_crumps">
    <a href="/">Главная</a>&nbsp;/
    <a href="/production/">Торговые дома</a>&nbsp;/
    Поиск
</div>

<h1>Поиск</h1>

<div class="lines">
	<?php if (count($this->catalog_item) > 0) { ?>
		<div id="first" class="act">по Вашему запросу «<?=$this->find?>» найдено <?php echo count($this->catalog_item); ?> результатов.</div>
	<?php } else { ?>
		<span>К сожалению, по Вашему запросу «<?=$this->find?>» найдено 0 результатов. И пока мы работаем над тем, чтобы наш Поиск стал лучше, воспользуйтесь поиском по <a href="/production/">алфавиту Брендов</a>!</span>
	<?php } ?>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="items main_page">
            <?php if (isset($this->catalog_item)): ?>

                <?php $i = 0; foreach ($this->catalog_item as $catalog_item): ?>
                    <? include 'partials/catalog_item.php';?>
                <?php ++$i; endforeach; ?>

            <?php endif; ?>
        </div>
    </div>
</div>
<div id="user_popup">
    <div class="top_search">
        <form name="form" enctype="multipart/form-data" method="post" action="/find/" class="search_form">
            <input name="find" type="text" placeholder="Поиск товара по названию">
            <button type="submit">
                <i class="icon-search"></i>
            </button>
        </form>
    </div>
</div>

<h1>Новый поиск</h1>
<duv>
    <div class="search">
        <form name="form" enctype="multipart/form-data" method="post" action="/find/" class="search__form new_search_form">
            <input class="new_search" name="find" type="text" placeholder="Поиск товара по названию">
            <button class="search__button" type="submit"></button>
        </form>
    </div>
</duv>