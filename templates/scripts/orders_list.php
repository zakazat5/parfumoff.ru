<link rel="stylesheet" href="/templates/js/bootstrap-select/bootstrap-select.css">
<script src="/templates/js/bootstrap-select/bootstrap-select.js"></script>

<link rel="stylesheet" href="/templates/css/cart.css">
<script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>

<div class="bread_crumps">
    <a href="/">�������</a>&nbsp;/
    ������� �������
</div>


<div id="cart_step1">

    <?php if ($this->item_order == ''){ ?>
        <div class="news">
            <br />
            <h2 class="georgia_head">� ������ ������ ���� ������� �����.</h2> <br />

            ��� ���������� ������� ���������� ������� ������. <br />
            ��� ����� ������� ����������� ���������: <br />
            - ����� <a href="/catalog/">������� �������</a>; <br />
            - ����� <a href="/girl/">������� ������� ��������</a>;<br />
            - ��� <a href="/men/">������� ��������</a>;<br />
            - ��������� ����� �� ����� (� ������� ������ ����);<br /><br />

            <h2 class="georgia_head">������ ��� �������� �������!</h2><br />
        </div>

    <?php }else{ ?>

        <form autocomplete="off" action="/orders/reload/" method="post" enctype="multipart/form-data" name="orders_list">
            <div class="news">
                <br /><br />
                <h2 style="margin-bottom:50px;font-weight:normal;font-size:24px;line-height:30px" class="georgia_head">
                    ���������� ������
                    <div style="font-size:18px;">
                        (�� ����� <b>5 995 ���</b>, � ���������� <b>3 ��</b>.)
                    </div>
                </h2>

                <div class="cart_steps">
                    <ul>
                        <li class="active"><span><b>��� 1.</b> ����������� - </span></li>
                        <li><span><b>��� 2.</b> ����� � ������ ��������</span></li>
                    </ul>
                </div>

                <table cellspacing="0" cellpadding="0" id="cart_wrap" width="695" style="width:695px">
                    <tbody>
                    <tr>
                        <th align="left" style="width:500px" class="head_cart" >�������� ������</th>
                        <th width="115" align="left" class="head_cart">����, ���.</th>
                        <th width="85" align="left" class="head_cart">���-��</th>
                        <th align="left" width="150" class="head_cart">���������, ���.</th>
                    </tr>

                    <?php if ($this->item_order != ''){ ?>
                        <?php foreach ($this->item_order as $item_order) { ?>

                            <tr id="tr_<?php echo $item_order['uid']; ?>" class="cart_td cart_<?php echo $item_order['bg']; ?>">

                                <td width="240" class="first_td">
                                    <img width="70" height="70" align="left" src="<?php echo $item_order['img_s']; ?>" alt="" class="cart_img"/>
                                    <span style="font-size:14px"><?php echo $item_order['title']; ?></span>

                                    <?php if($item_order['is_block']==1){ ?>
                                        <span style="font-size:11px;;display:block" class="purple">
								�� ������ ����� ������ �� ����������������!
							</span>
                                    <?php } ?>

                                </td>

                                <td>
						<span class="price">

							<?php if($item_order['is_block']==1){ ?>
                                <img width="24" height="24" src="/images/default/sale_icon.png" border="0" align="left">
                            <?php } ?>
                            <? /*
							<?
								$iNewPrice = 0.1*$item_order['price'] + $item_order['price'];
								$iNewPrice = round($iNewPrice, 0);
							?>
							<s><?php echo $iNewPrice; ?> ���</s>
							*/ ?>
                            <span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>

                            <? if ($this->sell):?>
                                <s>
                                    <?=parsePrice(round(($item_order['price'])+(($item_order['price'])*$this->sell)))?>
                                </s> &nbsp;&nbsp;
                            <? endif;?>

                            <?php echo parsePrice($item_order['price']); ?>

						</span>
                                    <!--<span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>-->
                                    </span>
                                </td>

                                <td>
                                    <div class="korz_kol-vo"><span class="val"><?php echo $item_order['kol'];?></span>
                                        <a data-uid=<?=$item_order['uid'];?> data-action="asc"   href="/orders/AscOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="plus"></a>
                                        <a data-uid=<?=$item_order['uid'];?> data-action="desc" href="/orders/DescOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="minus"></a>
                                    </div>
                                    <span style="float:right;padding-right:10px;font-size:11px;">=</span>
                                    <input type="hidden" name="kol_<?php echo $item_order['uid']; ?>" maxlength="2" value="<?php echo $item_order['kol']; ?>" id="kol_<?php echo $item_order['uid']; ?>" class="input_cart" style="font-size:11px;" />
                                </td>

                                <td>
                                    <span class="sum"><?php echo parsePrice($item_order['sum']); ?></span> <br />
                                    <a style="text-decoration:none;color:#828282;border-bottom:1px dotted" href="/orders/deleteOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['sum'];?>/<?php echo $item_order['kol'];?>/">�������</a>
                                </td>
                            </tr>

                        <?php } ?>
                    <?php } ?>

                    </tbody>
                </table>
            </div>
        </form>

        <div class="cart_step1_wrap">
            <div class="georgia_head">����� �������� ����� ������� ������ � ����� ��� �������������:</div>
            <div class="lCol">
                <div class="title">� ����� �������</div>
                <form action="">
                    <div class="line"><label for="step1_fio">���<span class="star">*</span></label><input id="step1_fio" type="text"></div>
                    <div class="line" style="position:relative"><label for="step1_phone">�������:<span class="star">*</span></label>
                        <div id="step1_phone_before">+7</div><input id="step1_phone" type="text"></div>
                    <div class="line"><label for="step1_mail">E-mail:<span class="star">*</span></label><input id="step1_mail" type="text"></div>
                    <button style="float:right;position:relative;top:11px;right:2px" class="cart_btn1">������������������ � ��������</button>
                </form>
            </div>
            <div class="rCol">
                <div class="title">� ��� ������� �����</div>

                <div style="padding:37px 0 55px">
                    ���� �� ��� ������ ����� � ����� �������� � �������� ������ � ������� ��������,
                    �� ��� ���������� ������ ���� ����� � ������ � ���������� ���������� ������.
                </div>
                <a data-click="show_login_form" style="text-align:center;width:194px;float:right" class="cart_btn2">����� � ��������</a>
            </div>
        </div>

    <?php } ?>

</div>


<div id="cart_step2" style="display:none">
<div class="news">
<br /><br />

<h2 style="margin-bottom:50px;font-weight:normal;font-size:24px;line-height:30px" class="georgia_head">
    ���������� ������
    <div style="font-size:18px;">
        (�� ����� <b>5 995 ���</b>, � ���������� <b>3 ��</b>.)
    </div>
</h2>

<div class="cart_steps">
    <ul>
        <li class=""><span><b>��� 1.</b> �����������</span></li>
        <li class="active"><span><b>��� 2.</b> ����� � ������ ��������</span></li>
    </ul></div>

<div class="lCol">
    <div class="title georgia_head">�������, ����������, ������ ��������:</div>
    <form action="">
        <div class="line"><label for="step2_region">������ / �������:<span class="star">*</span></label>
            <input placeholder="������, ���������� ���, �������� ���, �������" id="step2_region" type="text"></div>
        <div class="line"><label for="step2_town">����� / �����:<span class="star">*</span></label>
            <input id="step2_town" placeholder="������, �������, ���������� �-�, ���, ������" type="text"></div>
        <div class="line" style="display:none"><label for="step2_punkt">���������� �����:<span class="star">*</span></label><input id="step2_mail" type="text"></div>

        <div style="padding-top:50px" class="title georgia_head">�������� ������� ������ �������� � ��� ������:</div>

        <div class="dostavka_item active">
            <div class="l" style="position:relative;top:30px">
                <div style="position:relative;top:10px" style="position:relative;top:10px" class="text">��������  ��������</div>
            </div>
            <div class="r">
                <table>
                    <tr class="head">
                        <td>������ ��������</td>
                        <td>�����</td>
                        <td>����</td>
                    </tr>
                    <tr>
                        <td class="tooltip_bg_blue">
                            <div data-width="250" class="dostavka_item__checkbox checked" data-toggle="tooltip" data-content="�������� �� ������ ��-�� � 11 �� 19:00. ����� ��������� ������ �������� ��� �� ��������� �������.">b2bpl</div>
                        </td>
                        <td>
                            <div class="">4 ��.</div>
                        </td>
                        <td>117 �</td>
                    </tr>
                    <tr>
                        <td class="tooltip_bg_blue">
                            <div data-width="250"  class="dostavka_item__checkbox" data-toggle="tooltip" data-content="�������� ��-�� � 10 �� 20:00. � ���� �������� ��� �������� ���-����������� � ������� �������� ��������, ������� ������������ ��� ��������.">b2bpl</div>
                        </td>
                        <td>
                            <div class="">4 ��.</div>
                        </td>
                        <td>117 �</td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
            <div class="dop_info">
                <div class="line fl_left" style="margin-right:15px;width:220px">
                    <label style="width:auto;padding:0" for="">�����:</label>
                    <input id="" type="text" style="width:206px;padding:6px;">
                </div>
                <div class="line fl_left" style="margin-right:15px;width:80px">
                    <label style="width:auto;padding:0" for="">���:</label>
                    <input type="text" style="width:68px;padding:6px;">
                </div>
                <div class="line fl_left" style="width:80px">
                    <label style="width:auto;padding:0" for="">������:</label>
                    <input type="text" style="width:68px;padding:6px;">
                </div>
                <div class="clear"></div>
                <div class="line fl_left" style="margin-right:15px;width:75px">
                    <label style="width:auto;padding:0" for="">��������:</label>
                    <input type="text" style="width:63px;padding:6px;">
                </div>
                <div class="fl_left line" style="margin-right:15px;width:100px">
                    <label style="width:auto;padding:0" for="">����� ��������:</label>
                    <input type="text" style="width:86px;padding:6px;">
                </div>
                <div class="line fl_left" style="width:110px;margin-right:15px;">
                    <label style="width:auto;padding:0" for="">����� ��������:</label>
                    <input type="text" style="width:98px;padding:6px;">
                </div>
                <div class="line fl_left" style="width:80px">
                    <label style="width:auto;padding:0" for="">������:</label>
                    <input type="text" style="width:68px;padding:6px;">
                </div>
                <div class="clear"></div>
                <div class="line fl_left" style="margin-right:15px;width:330px">
                    <label style="width:auto;padding:8px 8px 8px 0;" for="">��������� ������� �����:</label>
                    <select name="" id="" style="width:153px;border:1px solid #cecccd;padding:8px;">
                        <option value="">&mdash;</option>
                    </select>
                </div>

            </div>
        </div>

        <div class="clear"></div>

        <div class="dostavka_item ">
            <div class="l">
                <div style="position:relative;top:10px" class="text" >�����  ������</div>
            </div>
            <div class="r">
                <table>
                    <tr class="head">
                        <td>������ ��������</td>
                        <td>�����</td>
                        <td>����</td>
                    </tr>
                    <tr>
                        <td class="tooltip_bg_blue">
                            <div class="dostavka_item__checkbox" data-width="250" class="dostavka_item__checkbox " data-toggle="tooltip" data-content="������ ���������� � ������� 1 ��� ���������� �������� 1 ������. �������� � �������� ����� �� ������� �� ����� �������� ���������. � ������ �������� ������� �� �������� ��������� �� ����� ������. ��� ������ ���������� �������� ����� ������ ������� �� 2 �� 5% �������� �� ������� ����� �� ��� ����.">����� ������</div>
                        </td>
                        <td>
                            <div >5-12 ��.</div>
                        </td>
                        <td><div style="cursor:pointer" data-toggle="tooltip" data-content="����� �������� ��������� ��������" class="green_label">117 �</div></td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
            <div class="dop_info" style="padding-top:5px;display:none">
                <div class="line fl_left" style="margin-right:15px;width:220px">
                    <label style="width:auto;padding:0" for="">�����:</label>
                    <input id="" type="text" style="width:206px;padding:6px;">
                </div>
                <div class="line fl_left" style="margin-right:15px;width:80px">
                    <label style="width:auto;padding:0" for="">���:</label>
                    <input type="text" style="width:68px;padding:6px;">
                </div>
                <div class="line fl_left" style="width:80px">
                    <label style="width:auto;padding:0" for="">������:</label>
                    <input type="text" style="width:68px;padding:6px;">
                </div>
                <div class="clear"></div>
                <div class="line fl_left" style="margin-right:15px;width:75px">
                    <label style="width:auto;padding:0" for="">��������:</label>
                    <input type="text" style="width:63px;padding:6px;">
                </div>
                <div class="fl_left line" style="margin-right:15px;width:100px">
                    <label style="width:auto;padding:0" for="">����� ��������:</label>
                    <input type="text" style="width:86px;padding:6px;">
                </div>
                <div class="line fl_left" style="width:110px;margin-right:15px;">
                    <label style="width:auto;padding:0" for="">����� ��������:</label>
                    <input type="text" style="width:98px;padding:6px;">
                </div>
                <div class="line fl_left" style="width:80px">
                    <label style="width:auto;padding:0" for="">������:</label>
                    <input type="text" style="width:68px;padding:6px;">
                </div>
            </div>
        </div>

        <div class="clear"></div>

        <div class="dostavka_item ">
            <div class="l" >
                <div style="position:relative;top:10px" style="position:relative;top:10px" style="position:relative;top:10px" class="text">���������</div>
            </div>
            <div class="r">
                <table>
                    <tr class="head">
                        <td>������ ��������</td>
                        <td>�����</td>
                        <td>����</td>
                    </tr>
                    <tr>
                        <td>
                            <div class="dostavka_item__checkbox ">b2bpl</div>
                        </td>
                        <td>
                            <div style="cursor:pointer" data-toggle="tooltip" data-content="����� ������� ��������" class="green_label">4 ��.</div>
                        </td>
                        <td>117 �</td>
                    </tr>
                </table>
            </div>
            <div class="clear"></div>
            <div class="dop_info" style="display:none">
                <div style="padding:15px 0;">�������� �� ����� ������������ ��� ����� ���������� � ������� "�������"</div>
                <div  id="dostavka_map">
                    <div class="dostavka_map__baloon">
                        <div class="close"></div>
                        ������ ���� ������ ��� ����� �����
                    </div>
                </div>
            </div>
        </div>

    </form>
</div>
<div class="rCol">
    <div class="slideBlock">
        <div class="content">
            <div class="georgia_head title">��� ����� �123124</div>
            <table cellspacing="0" cellpadding="0" width="100%" style="width:100% !important">
                <tbody>
                <?php if ($this->item_order != ''){ ?>
                    <?php foreach ($this->item_order as $item_order) { ?>

                        <tr id="tr_<?php echo $item_order['uid']; ?>" class="cart_td cart_<?php echo $item_order['bg']; ?>">

                            <td colspan="3" class="first_td">
                                <span style="font-size:14px"><?php echo $item_order['title']; ?></span>

                                <?php if($item_order['is_block']==1){ ?>
                                    <span style="font-size:11px;;display:block" class="purple">
													�� ������ ����� ������ �� ����������������!
												</span>
                                <?php } ?>

                            </td>
                        </tr>
                        <tr>
                            <td class="fl_left">
                                <div class="color_gray1">����, ���</div>
											<span class="price">

												<?php if($item_order['is_block']==1){ ?>
                                                    <img width="24" height="24" src="/images/default/sale_icon.png" border="0" align="left">
                                                <?php } ?>
                                                <? /*
												<?
													$iNewPrice = 0.1*$item_order['price'] + $item_order['price'];
													$iNewPrice = round($iNewPrice, 0);
												?>
												<s><?php echo $iNewPrice; ?> ���</s>
												*/ ?>
                                                <span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>

                                                <? if ($this->sell):?>
                                                    <s>
                                                        <?=parsePrice(round(($item_order['price'])+(($item_order['price'])*$this->sell)))?>
                                                    </s> &nbsp;&nbsp;
                                                <? endif;?>

                                                <?php echo parsePrice($item_order['price']); ?>

											</span>
                                <!--<span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>-->
                                </span>
                            </td>

                            <td class="fl_left">
                                <div class="color_gray1">���-��</div>
                                <div class="korz_kol-vo"><span class="val"><?php echo $item_order['kol'];?></span>
                                    <a href="/orders/AscOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="plus"></a>
                                    <a href="/orders/DescOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="minus"></a>
                                </div>
                                <span style="float:right;padding-right:10px;font-size:11px;">=</span>
                                <input type="hidden" name="kol_<?php echo $item_order['uid']; ?>" maxlength="2" value="<?php echo $item_order['kol']; ?>" id="kol_<?php echo $item_order['uid']; ?>" class="input_cart" style="font-size:11px;" />
                            </td>

                            <td class="fl_left">
                                <div class="color_gray1">���������</div>
                                <span class="sum"><?php echo parsePrice($item_order['sum']); ?></span> <br />
                                <a style="text-decoration:none;color:#828282;border-bottom:1px dotted" href="/orders/deleteOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['sum'];?>/<?php echo $item_order['kol'];?>/">�������</a>
                            </td>
                        </tr>

                    <?php } ?>
                <?php } ?>

                </tbody>
            </table>
            <div class=" more_tovars">
                <a href="">��� 6 �������</a>
            </div>
            <table cellspacing="0" cellpadding="0" width="100%" style="display:none;width:100% !important">
                <tbody>
                <?php if ($this->item_order != ''){ ?>
                    <?php foreach ($this->item_order as $item_order) { ?>

                        <tr id="tr_<?php echo $item_order['uid']; ?>" class="cart_td cart_<?php echo $item_order['bg']; ?>">

                            <td class="first_td">
                                <span style="font-size:14px"><?php echo $item_order['title']; ?></span>

                                <?php if($item_order['is_block']==1){ ?>
                                    <span style="font-size:11px;;display:block" class="purple">
													�� ������ ����� ������ �� ����������������!
												</span>
                                <?php } ?>

                            </td>
                        </tr>
                        <tr>
                            <td class="fl_left">
                                <div class="color_gray1">����, ���</div>
											<span class="price">

												<?php if($item_order['is_block']==1){ ?>
                                                    <img width="24" height="24" src="/images/default/sale_icon.png" border="0" align="left">
                                                <?php } ?>
                                                <? /*
												<?
													$iNewPrice = 0.1*$item_order['price'] + $item_order['price'];
													$iNewPrice = round($iNewPrice, 0);
												?>
												<s><?php echo $iNewPrice; ?> ���</s>
												*/ ?>
                                                <span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>

                                                <? if ($this->sell):?>
                                                    <s>
                                                        <?=parsePrice(round(($item_order['price'])+(($item_order['price'])*$this->sell)))?>
                                                    </s> &nbsp;&nbsp;
                                                <? endif;?>

                                                <?php echo parsePrice($item_order['price']); ?>

											</span>
                                <!--<span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>-->
                                </span>
                            </td>

                            <td class="fl_left">
                                <div class="color_gray1">���-��</div>
                                <div class="korz_kol-vo"><span class="val"><?php echo $item_order['kol'];?></span>
                                    <a href="/orders/AscOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="plus"></a>
                                    <a href="/orders/DescOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="minus"></a>
                                </div>
                                <span style="float:right;padding-right:10px;font-size:11px;">=</span>
                                <input type="hidden" name="kol_<?php echo $item_order['uid']; ?>" maxlength="2" value="<?php echo $item_order['kol']; ?>" id="kol_<?php echo $item_order['uid']; ?>" class="input_cart" style="font-size:11px;" />
                            </td>

                            <td class="fl_left">
                                <div class="color_gray1">���������</div>
                                <span class="sum"><?php echo parsePrice($item_order['sum']); ?></span> <br />
                                <a style="text-decoration:none;color:#828282;border-bottom:1px dotted" href="/orders/deleteOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['sum'];?>/<?php echo $item_order['kol'];?>/">�������</a>
                            </td>
                        </tr>

                    <?php } ?>
                <?php } ?>

                </tbody>
            </table>

            <div class="line" style="margin-bottom:10px">
                <textarea placeholder="����������� � ������" name="" id="" style="font-family:Tahoma;height:34px;padding:4px;width:95%"></textarea>
            </div>


            <div class="row">
                <div class="col-8">����� ������:</div>
                <div class="col-4">1 320 ���</div>
            </div>
            <div class="row">
                <div class="col-8">
                    ������� ������:
                    <div class="nakop_sk"></div>
                    <div class="nakop_id" style="right:auto;left:-185px;height: 150px; width: 430px; padding: 10px 20px; display: none;">
                        <span style="font-size:18px;display:block;padding-bottom:16px" class="georgia_head">������� ������ � �������� Parfumoff.ru:</span>
                        3% �� ��������� ������ ��� ������� �� ����� �� 3000 ��� �� 5000 ���<br>
                        4%  �� ��������� ������ ��� ������� �� ����� �� 5001 ��� �� 7000 ���<br>
                        5%  �� ��������� ������ ��� ������� �� ����� �� 7001 ��� �� 10000 ���<br>
                        7% �� ��������� ������ ��� ������� �� ����� �� 10001 ��� �� 14999 ���<br>
                        10% �� ��������� ������ ��� ������� �� ����� ����� 15000 ���</div>
                </div>
                <div class="col-4">0%</div>
            </div>
            <div class="row">
                <div class="col-8">��������:</div>
                <div class="col-4">320 ���</div>
            </div>

            <div class="row itogo">
                ����� �����:
                <b>1 640</b> ���
            </div>
            <div style="text-align:center">
                <a href="" class="cart_btn1">��������� ����������</a>
            </div>
        </div>
    </div>
</div>

</div>
</div>





<div id="cart_step3" style="display:none">
    <div class="news">
        <br /><br />

        <h2 style="margin-bottom:50px;font-weight:normal;font-size:24px;line-height:30px" class="georgia_head">
            ���������� ������
            <div style="font-size:18px;">
                (�� ����� <b>5 995 ���</b>, � ���������� <b>3 ��</b>.)
            </div>
        </h2>

        <div class="cart_steps">
            <ul>
                <li class=""><span><b>��� 1.</b> �����������</span></li>
                <li class=""><span><b>��� 2.</b> ����� � ������ ��������</span></li>
            </ul>
        </div>

        <form action="">
            <div class="lCol">
                <div style="margin-bottom:25px" class="fs18 color_purple georgia_head">
                    * ������������ ����
                </div>
                <div class="line">
                    <label for="">���:<span class="star">*</span></label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">�������:<span class="star">*</span></label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">E-mail:<span class="star">*</span></label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">������/�������:<span class="star">*</span></label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">�����/�����:<span class="star">*</span></label>
                    <input type="text">
                </div>
                <div class="line">
                    <label style="padding:0;position:relative;top:-2px" for="">���������� <br> �����:<span class="star">*</span></label>
                    <input type="text">
                </div>
                <div class="line" style="margin-bottom:45px;">
                    <label for="">�����:<span class="star">*</span></label>
                    <input type="text">
                </div>

                <div style="margin-bottom:15px" class="generate dostavka_item__checkbox checked">������ ������������� ������������� (���������� �� �����)</div>
                <div style="margin-bottom:15px" class="dostavka_item__checkbox ">������ ������</div>

                <div class="pass line" style="display:none">
                    <label for="">������:<span class="star">*</span></label>
                    <input type="password">
                </div>
                <div class="pass line" style="display:none">
                    <label style="padding:0;position:relative;top:-2px" for="">����������� ������:<span class="star">*</span></label>
                    <input type="password">
                </div>

                <button style="margin-left:154px" class="cart_btn1">��������� ����������</button>
            </div>
            <div class="rCol">
                <div style="margin-bottom:25px" class="fs18 georgia_head">
                    �������������� ����
                </div>
                <div class="line">
                    <label for="">���:</label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">������:</label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">��������:</label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">����� ��������:</label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">����� ��������:</label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">������:</label>
                    <input type="text">
                </div>
                <div class="line">
                    <label for="">�����������:</label>
                    <textarea name="" id=""></textarea>
                </div>
                <div style="margin-bottom:15px" class="dostavka_item__checkbox gray checked">�������� �� ������� � �����</div>
            </div>
        </form>
    </div>
</div>


</div>



<div style="display:none">
    <div class="box-modal" id="login_form_popup" style="background:white;width:295px;padding:45px 105px;">
        <a class="box-modal_close close1"><span></span></a>
        <div class="title">���� � ������ �������</div>
        <div style="margin-bottom:20px;">
            ��� ���������� ������ ������� ��� ����� (��������� � email) � ������, ���������� ��� ����������� �� �����.
        </div>

        <form action="">
            <div class="line"><input type="text" placeholder="�����" /></div>
            <div class="line"><input type="text" placeholder="������" /></div>
            <div class="forget"><a href="/users/forgot-password/">������ ������?</a></div>
            <div style="text-align:center;"><button class="cart_btn1">�����</button></div>
        </form>

    </div>
</div>

