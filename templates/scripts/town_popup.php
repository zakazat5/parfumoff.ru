<div id="town_popup_outer" style="display:none">
    <div id="town_popup" class="box-modal">
        <a class="box-modal_close"><span></span></a>
        <div class="town_title"><?=$this->main['location']['city_name']?></div>
        <div class="country_region">
            <div class="country">Россия,</div>
            <div class="region"><?=$this->main['location']['region_name']?></div>
        </div>



        <div class="town_wrap">
            <div class="lCol">
                <div style="font-size:14px;margin-bottom:10px">Регион</div>
                <div class="scroll">
                    <div class="region_list">
                        <ul>

                            <li class="active init"><a data-id="0" href="">Крупные населенные пункты</a></li>
                            <? foreach($this->main['geo_regions'] as $region):?>
                            <li><a data-id="<?=$region['id']?>" href=""><?=$region['region_name']?></a></li>
                            <? endforeach;?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="rCol">
                <div style="font-size:14px;margin-bottom:10px">Населенный пункт</div>
                <div class="town_list">

                </div>
            </div>
        </div>

        <div class="footer">

        </div>
    </div>
</div>