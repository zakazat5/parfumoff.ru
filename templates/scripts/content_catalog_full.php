<div class="pl25 pr25 aromat">
    <div class="breadcrumbs fs14 mb30">
        <a class="td_underline" href="/production/">Каталог продукции</a>
        <span>&gt;</span>
        <a class="td_underline" href="/production/<?=$this->brend['url']?>/"><?=$this->brend['title']?></a>
        <span class="">&gt;</span>
        <span><?=$this->catalog_full['title']?></span>
    </div>

    <h1 class="listing_title" style="float: left">
		<span class="listing_title__item mr10">Духи <?= $this->brend['title'] ?> <?= $this->catalog_full['title'] ?></span>
    </h1>
    <span class="aromat_type" style="top: 5px;position: relative;"><?=$this->catalog_full['pol']?> аромат</span>
    <div class="clear"></div>

    <? require_once('partials/sort_rate.php');?>
    <div>Цена снижена</div>
    <div class="row top_aromat">
        <div class="col-sm-4 col-xs-12">
            <div class="aromat_img mb30">
                <img src="<?=$this->catalog_full['img']; ?>" alt="<?php echo $this->catalog_full['title']; ?>" />
            </div>

        </div>
        <div class="col-xs-12 col-sm-8">
            <div class="listing_sort mobile">
                <div class="top">
                    <span class="sort_title d_inline-block mr25">Рейтинг:</span>
                    <div class="stars d_inline-block mr30 relative" style="top:6px">
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_ua"></div>
                    </div>
                </div>
                <span class="sort_lh18 ml30 mr10 d_inline-block"><span class="br_line">Ароматы </span>в наличии</span>
                <input type="checkbox" class="filter_aromats" />
                <span class="ml10 sort_lh18 d_inline-block"><span class="br_line">Все</span> ароматы</span>
            </div>
            <table class="aromat_table table table-bordered table-striped">

                <?php foreach ($this->catalog_item_order as $key=>$catalog_item_order) { ?>
                    <tr class="<?=($catalog_item_order['price']==0)?'disabled':''?>" <?=($_COOKIE['outstock'])?'style="display: table-row;"':''?>>
                        <td width="270" class="first_col" data-id="<?=$catalog_item_order['articul']?>">
                            <?=$catalog_item_order['title']; ?><?=$catalog_item_order['type']; ?>
                        </td>
                        <td class="second_col bold" width="190">
                            <? if ($catalog_item_order['price']==0):?>
                                Нет в наличии
                            <? else:?>
                                <? if ($this->sell):?>
                                        <?=round($catalog_item_order['price']+$catalog_item_order['price']*$this->sell);?> руб.
                                <? endif;?>
                                <? if($this->user['d_percent']!=0 AND $catalog_item_order['is_block']==0){ ?>
                                    <? if (!$this->sell):?>
                                        <? echo $catalog_item_order['price'];?>
                                    /<br />
                                    <? endif;?>
                                    <? echo show_users_percent($catalog_item_order['price'], $this->user['d_percent']); ?> руб.
                                <? }else{ ?>
                                    <? if($catalog_item_order['is_block']==1){ ?>
                                            <? echo $catalog_item_order['price_old'];?>
                                        /<br />
                                        <? echo $catalog_item_order['price'];?> руб.
                                    <? }else{ ?>
                                        <? echo $catalog_item_order['price'];?> руб.
                                    <? } ?>
                                <? } ?>
                            <? endif;?>
                        </td>
                        <td class="third_col ta_center">
                            <? if ($catalog_item_order['price']):?>
                                <a href="/korzina/addOrder/<? echo $catalog_item_order['id'];?>/" class="btn3"><i class="icon-cart_green show_mobile"></i><span class="hide_mobile">В&nbsp;корзину</span></a>
                            <? else:?>
                                <a href="" class="btn4" data-articul="<?=$catalog_item_order['articul']?>"><i class="icon-no_aromat show_mobile"></i><span class="hide_mobile">Сообщить</span></a>
                            <? endif;?>
                        </td>
                    </tr>
                <?php } ?>
            </table>
            <?php if($this->is_order==1) { ?>
            <div style="font-size:15px;">
                <a href="/production/<?php echo $this->brend_url; ?>/<?php echo $this->catalog_full['url']; ?>/">Посмотреть все варианты и цены для <?php echo $this->catalog_full['title']; ?></a>
            </div>
            <?php } ?>



            </div>
    </div>

    <div class="row">
        <div class="col-span-9 w100">
            <div class="hidden_desctop show_mobile tabs_type1">
                <ul>
                    <li class="one_line active"><a href="#tab1">Описание</a></li>
                    <li class="one_line"><a href="#tab2">Отзывы</a></li>
                    <li><a href="#tab3">Похожие ароматы</a></li>
                </ul>
            </div>
            <div id="tab1">
            <div class="text mb45 fs14">
                    <?=$this->catalog_full['text']?>
                </div>
            </div>
            <div id="tab2" class="hidden">
                <h2 class="heading_type1 hide_mobile">
                    Отзывы о <?=$this->brend['title']?> <?=$this->catalog_full['title']?>
                </h2>

                <? if ($this->rated!=true):?>
                <div class="mb30 clear">
                    <div class="fl_left hidden_mobile">
                        <span class="pr15">Оцените аромат</span>
                        <div style="top:6px" class="relative stars d_inline-block">
                            <input name="star_bottom" type="radio" value="1" class="star2 required"/>
                            <input name="star_bottom" type="radio" value="2" class="star2"/>
                            <input name="star_bottom" type="radio" value="3" class="star2"/>
                            <input name="star_bottom" type="radio" value="4" class="star2"/>
                            <input name="star_bottom" type="radio" value="5" class="star2"/>
                        </div>
                    </div>
                    <div class="fl_right">
							<script type="text/javascript" src="//yastatic.net/share2/share.js" async="async" charset="utf-8"></script>
							<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter" data-counter></div>
                    </div>
                </div>
                <? endif;?>


                <div id="hypercomments_widget" style="min-height:250px;background-color: #FFF;padding: 20px;"></div>

                <script type="text/javascript">
                    _hcwp = window._hcwp || [];
                    _hcwp.push({widget:"Stream", widget_id: 19481,xid:"<?=$this->catalog_full['id']; ?>"});
                    (function() {
                        if("HC_LOAD_INIT" in window)return;
                        HC_LOAD_INIT = true;
                        var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
                        var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
                        hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/19481/"+lang+"/widget.js";
                        var s = document.getElementsByTagName("script")[0];
                        s.parentNode.insertBefore(hcc, s.nextSibling);
                    })();
                </script>

                <?php if(isset($this->catalog_item_com)) { ?>

                    <div id="static_comments">

                        <?php foreach ($this->catalog_item_com as $catalog_item_com) { ?>
                            <a name="comment_<?php echo $catalog_item_com['id']; ?>"></a>
                            <div class="comment">
                                <div class="name"><?php echo $catalog_item_com['user_name']; ?></div>
                                <div class="comment_text">
                                    <?php echo $catalog_item_com['user_text']; ?>
                                </div>
                            </div>
                            <div class="comment_border"></div>
                            <div class="comment_end"></div>
                        <?php } ?>

                    </div>

                <?php } ?>
            </div>

            <div id="tab3" class="hidden">
                <div class="heading_type1 hide_mobile">
                    Схожие по цене ароматы
                </div>
                <div class="items main_page">

                    <?php foreach ($this->catalog_item_pre as $key=>$catalog_item) { ?>
                        <? include 'partials/catalog_item_mini.php';?>
                        <?} ?>

                </div>
            </div>

        </div>
        <div class="col-span-3 ">
            <script>
                $(function(){
                    $('.scroll_aromats').scrollbar();
                })
            </script>
            <?php if(isset($this->catalog_item_f)){ ?>
                <div class="mb20">Женские ароматы:</div>
                <div class="scroll_aromats scrollbar-outer" style="max-height:350px !important">
                    <ul class="list-type1">
                        <?php foreach ($this->catalog_item_f as $catalog_item) {?>
                            <li class="mb5">
                                <?php if(strtolower($catalog_item['url']) == $this->aromat_url){ ?>
                                    <?php echo $catalog_item['title']; ?>
                                <?php }else{ ?>
                                    <a class="td_underline color_blue" href="/production/<?php echo $this->brend_url; ?>/<?php echo $catalog_item['url']; ?>/">
                                        <? ($catalog_item['is_top'])?'<b>':''?><?php echo $catalog_item['title']; ?><? ($catalog_item['is_top'])?'</b>':''?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <?php if(isset($this->catalog_item_m)){ ?>
                <br>
                <br>
                <div class="mb20">Мужские ароматы:</div>
                <div class="scroll_aromats scrollbar-outer" style="max-height:350px !important">
                    <ul class="list-type1">
                        <?php foreach ($this->catalog_item_m as $catalog_item) {?>
                            <li class="mb5">
                                <?php if(strtolower($catalog_item['url']) == $this->aromat_url){ ?>
                                    <?php echo $catalog_item['title']; ?>
                                <?php }else{ ?>
                                    <a class="td_underline color_blue" href="/production/<?php echo $this->brend_url; ?>/<?php echo $catalog_item['url']; ?>/">
                                        <? ($catalog_item['is_top'])?'<b>':''?><?php echo $catalog_item['title']; ?><? ($catalog_item['is_top'])?'</b>':''?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <div class="h30"></div>



        </div>
    </div>

    <? require('partials/links_preview_horizontal.php')?>
<!--
    <div class="mb20">
    <?php //echo $this->body_text; ?>
    </div>
	-->
    <div id="no_aromat_popup" style="display:none">
        <span class="close">&times;</span>
        <form action="" method="post" >
            <div class="mb20"><input type="text" name="name" placeholder="Имя" /></div>
            <div class="mb20"><input type="text" name="phone" placeholder="Телефон" /></div>
            <div class="mb20"><input type="text" name="email" placeholder="e-mail" /></div>
            <input type="hidden" name="articul" id="modal_articul">
            <button name="waitlist" value="1" class="btn2">Сообщить о поступлении</button>
        </form>
    </div>
</div>