<?php if ($this->canon_name1 == 'christmassale') { ?>
<div id="top_line">
	<div class="container">
		<div class="fl_left" id="last_buy">
			Только что купили: <a href="<?=$this->main['bestseller']['url']?>"><?=geo_replace_utf8($city_utf8, $this->main['bestseller']['text'])?></a>
		</div>
		<div class="fl_left" data-target="#menu" id="menu_btn"></div>
		<div class="fl_right" id="user_links">
            <? if ($this->is_login):?>
                <span class="pr20"><a href="/users/" class="dashed">Личный кабинет</a></span>
                <span class=""><a href="/users/exit/" class="dashed">Выйти</a></span>
            <?else:?>
                <span class="pr20"><a href="/users/">Войти</a></span>
                <a href="/users/register/" class="dashed">Регистрация</a>
            <? endif;?>
		</div>
		<a id="mobile_cart" href="/korzina/">
			<div class="btn1">
				<i class="icon-cart"></i>
				<div class="fl_left"><?=$this->cart->getCount()?></div>
			</div>
		</a>
		<div id="mobile_user_cab" data-target="#user_popup"></div>
		<div id="mobile_phone" data-target="#phone_popup"></div>
	</div>
</div>

<div id="user_popup">
	<div class="top_search">
		<form name="form" enctype="multipart/form-data" method="post" action="/find/" class="search_form">
			<input name="find" type="text" placeholder="Поиск товара по названию">
			<button type="submit">
				<i class="icon-search"></i>
			</button>
		</form>
	</div>
</div>

<div id="phone_popup" class="ta_center">
	<a href="tel:<?=$city_utf8['phone']?>" class="color_blue d_block td_underline fs26 cursor_pointer mb25 bold"><?=$city_utf8['phone']?></a>
	<!--
	<a href="tel:8 (812) 426-11-15" class="color_blue d_block td_underline fs26 cursor_pointer mb25 bold">8 (812) 426-11-15</a>
	-->
	<div><a href="" class="color_blue dashed callMe-js" onclick="$('body').click()">Позвоните мне</a></div>
</div>

<div id="menu">
	<div class="container">
		<ul>
			<?php /*if (!$this->is_login):?>
			<li><a href="/users/" class="color_purple">Войти</a></li>
			<li><a href="/users/register/" class="color_purple">Зарегистироваться</a></li>
			<?php else: ?>
			<li><a href="/users/" class="color_purple">Личный кабинет</a></li>
			<li><a href="/users/exit/" class="color_purple">Выйти</a></li>
			<?php endif; */ ?>
			<li><a href="/about/#contact">Контакты</a></li>
			<li><a href="/worktime/">Режим работы</a></li>
			<li><a href="/about/#dostavka">Доставка</a></li>
			<li><a href="/oplata/">Оплата</a></li>
			<li><a href="/kachestvo/">Качество и гарантии</a></li>
			<li><a href="/disc/">Скидки</a></li>
			<li><a href="/article/">Статьи</a></li>
			<li><a href="/recommend/">Отзывы</a></li>
			<li><a href="/o_nas/">О нас</a></li>
			<li><a href="/return/">Возврат</a></li>
		</ul>
	</div>
</div>

<header>
	<div class="container">
		<div id="logo"><a href="/">Парфюмофф.ру</a></div>
		<div id="top_phones" class="call_phone_top_1">
			<div class="mb5 ta_right phone_item"><?=$city_utf8['name']?> <span class="pl5 fs24"><?=$city_utf8['phone']?></span></div>
			<!--
			<div class="mb5 ta_right phone_item">Санкт-Петербург <span class="pl5 fs24">8 (812) 426-11-15</span></div>
			-->
			<div id="callme"><a href="" class="btn4 callMe-js">Позвоните мне</a></div>
		</div>
		
		<div id="top_cart">
			<a href="/korzina/" style="display:block" class="btn1 header" >Корзина: <?=$this->cart->getCount()?> товаров</a>
			<div class=" fs14 content">				
				<div class="fs16"><a class="color_blue td_underline" href="/korzina/">Оформить заказ</a></div>
			</div>
		</div>
	
	
		<style>
		.plus_wrap i {
			background: url(../templates/images/icons-preimush1.png) 0 0 no-repeat;
		}
		#bottom_nav .pay{
			margin-left: 29rem;
		}
		.plus_wrap {
			padding:0px;
			margin-bottom: 0px;
		}
		#bottom_txt {
			margin-bottom: -30px;/* -40px;*/
		}
		#top_footer {
			background: #db0319;
		}
		.block_list1 .col-span-4 .title {
			font-size: 2.2rem;
			margin-bottom: 2rem;
		}
		.block_list1 .col-span-4 {
			height: 324px; 
		}
		.container {
			width: 98rem;
		}
		header {
			height: auto;
		}
		.christmassale_banner {
			margin-top: 150px;
			margin-bottom: 10px;
		}
		</style>
		<div class="christmassale_banner">
			<img src="/templates/img/christmassale_banner.png" alt="">
		</div>	

	</div>
	<script>
	</script>
</header>

<section id="letter_wrap">
	<div class="container">
		<div class="pt8 top ">
			<span style="padding-right:60px" class="header">Торговые дома по алфавиту</span>
			<a href="/production/" class="td_underline hidden-xs hidden_480">Посмотреть все бренды</a>
		</div>
		<ul>
			<li><a href="/liter/a/">A</a></li>
			<li><a href="/liter/b/">B</a></li>
			<li><a href="/liter/c/">C</a></li>
			<li><a href="/liter/d/">D</a></li>
			<li><a href="/liter/e/">E</a></li>
			<li><a href="/liter/f/">F</a></li>
			<li><a href="/liter/g/">G</a></li>
			<li><a href="/liter/h/">H</a></li>
			<li><a href="/liter/i/">I</a></li>
			<li><a href="/liter/j/">J</a></li>
			<li><a href="/liter/k/">K</a></li>
			<li><a href="/liter/l/">L</a></li>
			<li><a href="/liter/m/">M</a></li>
			<li><a href="/liter/n/">N</a></li>
			<li><a href="/liter/o/">O</a></li>
			<li><a href="/liter/p/">P</a></li>
			<li><a href="/liter/q/">Q</a></li>
			<li><a href="/liter/r/">R</a></li>
			<li><a href="/liter/s/">S</a></li>
			<li><a href="/liter/t/">T</a></li>
			<li><a href="/liter/u/">U</a></li>
			<li><a href="/liter/v/">V</a></li>
			<li><a href="/liter/w/">W</a></li>
			<li><a href="/liter/x/">X</a></li>
			<li><a href="/liter/y/">Y</a></li>
			<li><a href="/liter/z/">Z</a></li>

			<li><span class="dot"></span></li>
			<li><a href="">1-9</a></li>

			<li><span class="dot"></span></li>
			<li><a href="">А-Я</a></li>
		</ul>
		<div class="visible-xs text-left mb10">
			<a href="/production/" class="td_underline fs14 hidden_480">Посмотреть все бренды</a>
		</div>
	</div>
</section>
<?php } else { ?>

<div class="header">
    <div class="top-bar">
        <div class="wrapper top-bar__wrapper">
            <div class="menu">
				<a class="menu__item" href="/about/#contact">Контакты</a>
				<a class="menu__item" href="/worktime/">Режим работы</a>
				<a class="menu__item" href="/about/#dostavka">Доставка</a>
				<a class="menu__item" href="/oplata/">Оплата</a>
				<a class="menu__item" href="/kachestvo/">Качество и гарантии</a>
				<a class="menu__item" href="/disc/">Скидки</a>
				<a class="menu__item" href="/article/">Статьи</a>
				<a class="menu__item" href="/recommend/">Отзывы</a>
				<a class="menu__item" href="/o_nas/">О нас</a>
				<a class="menu__item" href="/return/">Возврат</a>
			</div>
            <div class="enter">
				<?php if ($this->is_login) { ?>
					<a class="enter__link" href="/users/exit/">Выйти</a>
					<div class="enter__divider">/</div>
					<a class="enter__link" href="/users/">Личный кабинет</a>
				<?php } else { ?>
					<a class="enter__link" href="/users/">Войти</a>
					<div class="enter__divider">/</div>
					<a class="enter__link" href="/users/register/">Регистрация</a>
				<?php } ?>
            </div>
        </div>
    </div>



<div class="mobile-menu">
<div class="head-mobile-menu">
<div class="pull-left">
  <img onclick="myFunction()" class="dropbtn" src="/templates/img/hum.png">

<a href="/"><img src="/templates/img/logo-mob.png" class="mobile-logo">
</a>
<span class="phone__callback-mob">Магазин оригинальной парфюмерии</span>
</div>
	

	<div class="pull-right">
	<a data-click="show_login_form" class="login-mob"><i class="fa fa-user"></i></a>
	<a class="header__mobile_cart <?php if ($this->cart->getCount() > 0) { echo 'cart_mod'; } ?>" href="/korzina/"><img src="/templates/img/cart.png" class="mobile-cart"><span><?=$this->cart->getCount()?></span></a>

	</div>
	</div>
	

  <div id="myDropdown" class="dropdown-content">
 <?php if ($this->is_login) { ?>
					<a href="/users/exit/">Выйти</a>
					<a href="/users/">Личный кабинет</a>
				<?php } else { ?>
					<a href="/users/">Войти</a>
					<a href="/users/register/">Регистрация</a>
				<?php } ?>

				<a href="/about/#contact">Контакты</a>
				<a href="/worktime/">Режим работы</a>
				<a href="/about/#dostavka">Доставка</a>
				<a href="/oplata/">Оплата</a>
				<a href="/kachestvo/">Качество и гарантии</a>
				<a href="/disc/">Скидки</a>
				<a href="/article/">Статьи</a>
				<a href="/recommend/">Отзывы</a>
				<a href="/o_nas/">О нас</a>
				<a href="/return/">Возврат</a>
				<a href="/production">Все бренды</a>
  </div>

</div>


<script type="text/javascript">
	/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {

    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>




    <div class="wrapper header__wrapper">
    <img class="banner_free_ship_img-mob" src="/templates/images/parfumoff1-1.png" alt="">
		<a class="logo" href="/">
			<img class="logo__picture" src="/templates/img/logo.png" alt="Parfumoff.ru – интернет-магазин парфюмерии.">
			<div class="logo__title">оригинальная парфюмерия</div>
            <div class="logo__age">Мы работаем с 2008 года</div>
		</a>
		<!-- black friday  -->
		<!--
		<a href="/sale/"><div class="header_top_m" style="width:248px; margin:0 auto; margin-bottom:10px; margin-top: 10px; display:none"><img src="/images/friday_sale_mob.png"></div></a>
		-->
        <div class="phone">
            <div class="phone__header"><?=$city_utf8['name']?></div>
           <!--  <div class="phone__content phone_alloka"><?=$city_utf8['phone']?></div> -->
           <div class="phone__content phone_alloka">
           	<a data-rel="external" href="tel:84955404723">8 (495) 540-47-23</a>
           	<a data-rel="external" href="tel:88003337671">8 (800) 333-76-71</a>
           </div>
			<a id="phone__callback" class="phone__callback" href="">заказать обратный звонок</a>
        </div>
        <div class="search">
           	<form class="search__form" enctype="multipart/form-data" method="post" action="/find/">
				<input class="search__input" name="find" type="text" placeholder="Поиск товара по названию">
				<button class="search__button" type="submit"></button>
			</form>
        </div>
		<div>
			<?
				$cart = array();

				foreach ($this->cart->_items as $item) {
						$cart[] = $item['id'];
				}
			?>
			<input class="rees46 track cart" value='<?=json_encode($cart)?>' type="hidden" />
			<a class="button header__cart <?php if ($this->cart->getCount() > 0) { echo 'cart_mod'; } ?>" href="/korzina/">Корзина: <?=$this->cart->getCount()?> товаров</a>
			<div class="header__cart_block" style="display:<?php if ($this->cart->getCount() > 0) {echo "block";} else {echo "none";}?>;">
				<div>
					<a class="" href="/korzina/">Оформить заказ</a>
				</div>
				<div>
					<a class="" href="/">Продолжить покупки</a>
				</div>
			</div>
		</div
    </div>
</div>
<div class="alphabet">
    <div class="wrapper">
        <div class="alphabet__header">
			<div class="alphabet__link_block_brands">
				<div class="alphabet__title">Торговые дома по алфавиту</div>
				<a class="alphabet__link" href="/production/">Посмотреть все бренды</a>
			</div>	
			<div class="alphabet__link_block_toilet_water">
				<a class="alphabet__link_secondary" href="/">Духи</a>
				<a class="alphabet__link_secondary" href="/tualetnaya-voda/">Туалетная вода</a>
				<a class="alphabet__link_secondary" href="/men/">Парфюм для мужчин</a>
				<a class="alphabet__link_first" href="/woman/">Парфюм для женщин</a>
			</div>
        </div>
        <div class="alphabet__content">
            <div class="alphabet__category">
				<a class="alphabet__item" href="/liter/a/">A</a>
				<a class="alphabet__item" href="/liter/b/">B</a>
				<a class="alphabet__item" href="/liter/c/">C</a>
				<a class="alphabet__item" href="/liter/d/">D</a>
				<a class="alphabet__item" href="/liter/e/">E</a>
				<a class="alphabet__item" href="/liter/f/">F</a>
				<a class="alphabet__item" href="/liter/g/">G</a>
				<a class="alphabet__item" href="/liter/h/">H</a>
				<a class="alphabet__item" href="/liter/i/">I</a>
				<a class="alphabet__item" href="/liter/j/">J</a>
				<a class="alphabet__item" href="/liter/k/">K</a>
				<a class="alphabet__item" href="/liter/l/">L</a>
				<a class="alphabet__item" href="/liter/m/">M</a>
				<a class="alphabet__item" href="/liter/n/">N</a>
				<a class="alphabet__item" href="/liter/o/">O</a>
				<a class="alphabet__item" href="/liter/p/">P</a>
				<a class="alphabet__item" href="/liter/q/">Q</a>
				<a class="alphabet__item" href="/liter/r/">R</a>
				<a class="alphabet__item" href="/liter/s/">S</a>
				<a class="alphabet__item" href="/liter/t/">T</a>
				<a class="alphabet__item" href="/liter/u/">U</a>
				<a class="alphabet__item" href="/liter/v/">V</a>
				<a class="alphabet__item" href="/liter/w/">W</a>
				<a class="alphabet__item" href="/liter/x/">X</a>
				<a class="alphabet__item" href="/liter/y/">Y</a>
				<a class="alphabet__item" href="/liter/z/">Z</a>
			</div>
            <div class="alphabet__category">
				<a class="alphabet__item" href="">1-9</a>
			</div>
            <div class="alphabet__category">
				<a class="alphabet__item" href="">А-Я</a>
			</div>
        </div>
		
    </div>
</div>
<!-- wednesday  banner and green color for prices-->
 <script>
	$(document).ready(function(){
		$(".aromat_table .head td:eq(2)").css("color", "#75C202").text("Цена снижена");
});
</script>
<!--
<div  class=" wrapper top_banner_big" style="margin-bottom:10px"><a href="/sale/"><img src="/images/friday_sale.png"></a></div>
-->

<!--
<script>
if(window.location.pathname === "/sale/" || window.location.pathname === "sale/page/2/filter/pol/m/" || window.location.pathname === "/sale/filter/pol/m/" || window.location.pathname === "/sale/filter/pol/f/" || window.location.pathname === "/sale/page/2/filter/pol/f/" || window.location.pathname === "/sale/page/3/filter/pol/f/" || window.location.pathname === "/sale/page/4/filter/pol/f/" ||window.location.pathname === "/sale/page/5/filter/pol/f/" || window.location.pathname === "/sale/page/2/" || window.location.pathname === "/sale/page/3/" || window.location.pathname === "/sale/page/4/" || window.location.pathname === "/sale/page/5/" || window.location.pathname === "/sale/page/6/" || window.location.pathname === "/sale/page/7/" || window.location.pathname === "/sale/page/2/" ) {
	
   $(function(){

   $("h1.content__header").html("Весенняя мегараспродажа");

}); 
   
   
};
 
</script>
-->
<?php } ?>