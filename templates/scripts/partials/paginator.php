<?php if ($this->pageCount > 1): ?>
    <div class="pages">
        <ul>
            <? if ($this->previous): ?> <li class="arrow"><a href="?page=<?=$this->previous?>" class="icon icon-Prev">&nbsp;</a></li><? endif;?>
            <?php foreach ($this->pagesInRange as $page):
                if ($page == $this->current): ?>
                    <li class="active"><span><?=$page?></span></li>
                <? else: ?>
                    <?if($page==1){?>
                        <li><a href="<?=$_SERVER[REQUEST_URI]?>"><?= $page ?></a></li>
                    <?} else{?>
                         <li><a href="?<?=http_build_query($this->get)?>&page=<?=$page?>"><?= $page ?></a></li>
                    <?};?>
                <? endif; ?>
            <? endforeach; ?>
            <? if ($this->next): ?><li class="arrow"><a href="<?=$this->next?>" class="icon icon-Next">&nbsp;</a></li> <? endif; ?>
        </ul>

    </div>
<?php endif; ?>
