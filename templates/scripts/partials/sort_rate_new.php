<div class="listing_sort">
    

	<div class="block_relative_sort_ch">
		<span class="top fs14">
			<span class="sort_title d_inline-block mr15">Рейтинг:</span>
			<div class="stars d_inline-block mr30 relative" style="top:6px">
				<input name="star_top" type="radio" value="1" class="star1 disabled" <?=($this->catalog_full['rate']==1)?' checked="checked"':''?>/>
				<input name="star_top" type="radio" value="2" class="star1 disabled" <?=($this->catalog_full['rate']==2)?' checked="checked"':''?>/>
				<input name="star_top" type="radio" value="3" class="star1 disabled" <?=($this->catalog_full['rate']==3)?' checked="checked"':''?>/>
				<input name="star_top" type="radio" value="4" class="star1 disabled" <?=($this->catalog_full['rate']==4)?' checked="checked"':''?>/>
				<input name="star_top" type="radio" value="5" class="star1 disabled" <?=($this->catalog_full['rate']==5)?' checked="checked"':''?>/>
			</div>
		</span>
		<div>
			<span class="fs14 sort_lh18 ml20 mr10 d_inline-block"><span class="br_line">Ароматы </span>в наличии</span>
			<input type="checkbox" <?=($_COOKIE['outstock']||$this->show_outstock==true)?'checked="checked"':''?> class="filter_aromats" />
			<span class="fs14 ml10 sort_lh18 d_inline-block"><span class="br_line">Все</span> ароматы</span>
		</div>
	</div>
	<div class="block_free_ship_img_prod">
		<?php if (geo_city() === "def") { ?>
			<img src="/templates/images/parfumoff1-1.png" alt="">
		<?php } ?>	
	</div>
	<div class="block_danger_img">	
		<img src="/templates/images/danger_non_original.png" alt="" class="danger_img">
	</div>
</div>

<script>
    $(function () {
        $('input.star1').rating({
        });

        $('input.star2').rating({
            callback: function (value, link) {
                $('input.star2').rating('readOnly',true)
                $.post('/api/rate/',{val:value,target_id:'<?=$this->catalog_full['articul']?>'},function(data){
                    console.log(data)
                })

            }
        });

        $('.danger_img').click(function(){
            var popup = $('#danger_popup').clone()
            popup.addClass('second')
            $('body').append(popup)
            popup.show()
            $('body').append('<div class="popup_overlay"></div>')
        })
        $(document).on('click', '#danger_popup .close, .popup_overlay', function(e){
            $('#danger_popup.second').remove()
            $('.popup_overlay').remove()
        })

    });
</script>
<!--noindex-->
<div id="danger_popup" class="popup_text" style="display:none">
    <div class="close">&times;</div>
    <div class="listing_title" style="margin-bottom:20px; font-size: 40px;">Остерегайтесь подделок</div>

    <div class="text">
        <?=$this->fake_text?>
    </div>
</div>
<!--/noindex-->