<a class="new" href="/<?=$news_item['url']; ?>/">
	<?php if($news_item['img']!=''){ ?>
		<div class="img"><img src="<?php echo $news_item['img']; ?>" alt="<?php echo $news_item['alt'];?>" /></div>
	<?php } ?>
	<div class="title"><?php echo $news_item['title']; ?></div>
	<div class="date"><?=$news_item['date']?></div>
	<div class="text"><?=strip_tags($news_item['anot']) ?></div>
	<span class="more color_blue td_underline">Читать далее</span>
</a>