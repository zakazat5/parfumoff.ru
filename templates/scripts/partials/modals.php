<div id="login" style="display:none">
    <span class="close">&times;</span>
    <form action="">
        <div class="mb20"><input type="text" placeholder="Имя" /></div>
        <div class="mb20"><input type="text" placeholder="Телефон" /></div>
        <button class="btn2">Сообщить о поступлении</button>
    </form>
</div>


<div id="register" style="display:none">
    <span class="close">&times;</span>
    <form action="">
        <div class="mb20"><input type="text" placeholder="Имя" /></div>
        <div class="mb20"><input type="text" placeholder="Телефон" /></div>
        <button class="btn2">Сообщить о поступлении</button>
    </form>
</div>

<div id="callMe" class="popup" style="display:none">
    <span class="close">&times;</span>
    <div class=" fs34 mb35 lite">Заказать звонок</div>
    <form method="post" action="/thankyou_call/" name="callMe_form" onsubmit="return validate_callMe_form();">
        <div class="mb20"><input type="text" name="callMe_name" placeholder="Имя" /></div>
		<div id="callMe_form_name_err" class="mb20" style="display:none">Пожалуйста заполните поле 'Имя'</div>
        <div class="mb20"><input type="text" class="phone_input" name="callMe_phone" placeholder="Телефон" /></div>
		<div id="callMe_form_phone_err" class="mb20" style="display:none">Пожалуйста заполните поле 'Телефон'</div>
		<div class="d_button_callMe"><button class="btn2 ">Перезвоните мне</button></div>
    </form>
</div>

<script>

</script>

<div id="quick_view" class="popup top-30" data-id_product="" style="display:none;">
    <span class="quick_view_close"></span>
    <div class="quick_view__content">
		<div class="quick-orders">
            <form>
                <div class="one-quick-input">
                	<input class="one-quick-text" type="text" name="quick_buy_phone" value="" placeholder="Ваш номер телефона">
                </div>
                <div class="one-quick-input">
                	<input class="one-quick-bt" type="button" value="Заказать">
                </div>
            </form>
            <div class="quick-mes">
                Для уточнения деталей мы свяжемся <br />с вами в ближайшее время
            </div>
        </div>
    </div>
</div>

<div id="quick_view_thankyou" class="popup top-30" style="display:none;">
    <span class="quick_view_close"></span>
    <div class="quick_view__content_thankyou">
		<div class="quick-orders">
            <div class="quick-mes_thankyou">
                Спасибо за заказ!
            </div>
        </div>
    </div>
</div>

<div id="fast_view" class="popup" style="display:none">
    <span class="close">&times;</span>
    <div class="fast_view__content">

    </div>
</div>


<script>
    function show_fast_view(url) {

        url = url + '?fast_view=1'
        $.ajax({
            method: "POST",
            url: url
        })
            .done(function (msg) {
                var
                    fv_wrap = $('#fast_view'),
                    fv_close = fv_wrap.find('.close')
                fv_wrap.find('.fast_view__content').html(msg)

                fv_wrap.show()

                $('body').append($('<div/>').attr('id', 'overlay'))
				/*
                var fv_h = parseInt(fv_wrap.height() / 2) * -1 + 'px'
                fv_wrap.css('margin-top', fv_h)
				*/
				fv_wrap.css('margin-top', 0);	
				
                fv_close.click(function () {
                    $('#fast_view').hide()
                    $('#overlay').remove()
                })

                $('#overlay').click(function () {
                    $('.popup').hide()
                    $(this).remove()
                })

            });


    }
	
	$(document).ready(function() {
		if (screen.width <= 480) {
			$("html").css({ 'overflow-x': 'scroll'});
		}
	});

</script>

<script>
function quick_buy_click(id_catalog) {
        $.ajax({
            method: "POST",
			url: "/ajax-requests/?ajax_quick_buy=1",
			data: "quick_buy_id_catalog=" + id_catalog
        })
		.done(function (msg) {
			var res_data = eval("(" + msg + ")");
			if (Number(res_data.id_product) > 0) {
				$('#fast_view').hide();
                $('#overlay').remove();
				var fv_wrap = $('#quick_view');
				var fv_close = fv_wrap.find('.quick_view_close');
				var fv_input_phone = fv_wrap.find("input[name='quick_buy_phone']");
				fv_input_phone.val('');
				$('#quick_view').attr("data-id_product", res_data.id_product);
				fv_wrap.show();
				$('body').append($('<div/>').attr('id', 'overlay'));
				fv_wrap.css('margin-top', 0);
				fv_close.click(function () {
					$('#quick_view').hide();
					$('#overlay').remove();
				});
				$('#overlay').click(function () {
					$('.popup').hide();
					$(this).remove();
				});
			}
            yaCounter21451555.reachGoal('onclick_katalog_er');
		});
}

$(document).ready(function() {
	$('.one-quick-bt').click(function () {
		if ($("input[name='quick_buy_phone']").val() != '') {
			$.ajax({
				method: "POST",
				url: "/ajax-requests/?ajax_quick_buy_submit=1",
				data: "quick_buy_id_product=" + Number($('#quick_view').attr("data-id_product")) + "&quick_buy_phone=" + $("input[name='quick_buy_phone']").val()
			})
			.done(function (msg) {
				$('#quick_view').hide();
				$('#overlay').remove();
				location.replace("/thankyou_quickorder/");
			});
		}
	});
	$("input[name='quick_buy_phone']").mask("7 (999) 999-99-99");
});
</script>
<script>
function validate_callMe_form ()
{
	var valid = true;
    if ($.trim($("input[name='callMe_name']").val()) == "" )
    {
        $('#callMe_form_name_err').show();
        valid = false;
    }
	 if ($.trim($("input[name='callMe_phone']").val()) == "" )
    {
        $('#callMe_form_phone_err').show();
        valid = false;
    }
    return valid;
}
</script>

<script>
function quick_buy_click_product(id_product) {
	if (Number(id_product) > 0) {
		var fv_wrap = $('#quick_view');
		var fv_close = fv_wrap.find('.quick_view_close');
		var fv_input_phone = fv_wrap.find("input[name='quick_buy_phone']");
		fv_input_phone.val('');
		$('#quick_view').attr("data-id_product", Number(id_product));
		fv_wrap.show();
		$('body').append($('<div/>').attr('id', 'overlay'));
		fv_wrap.css('margin-top', 0);
		fv_close.click(function () {
			$('#quick_view').hide();
			$('#overlay').remove();
		});
		$('#overlay').click(function () {
			$('.popup').hide();
			$(this).remove();
		});
	}
}
</script>