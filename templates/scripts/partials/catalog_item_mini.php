<?php
/**
 * @var $catalog_item array product_item
 */
?>
<div class="col-md-3 col-sm-6 item_mini <?php echo ($catalog_item['price_s']==0)?'outstock':''?>" <?php echo ($_COOKIE['outstock'])?'style="display:block;"':''?>>
    <?= ($catalog_item['is_spec']) ? '<div class="badge_discount"></div>' : '' ?>

    <a href="<?php echo $catalog_item['url']; ?>">
        <div class="img hidden-xs">
            <img alt="<?php echo $catalog_item['title']; ?>" src="<?php echo $catalog_item['img']; ?>"/>
        </div>
        <div class="title hidden-xs">
            <span><?php echo $catalog_item['brend_title']; ?></span>
            <span class="color_purple"><?php echo $catalog_item['title']; ?></span>
        </div>
        <span class="visible-xs-inline-block color_blue td_underline"><?php echo $catalog_item['brend_title']; ?> <?php echo $catalog_item['title']; ?></span>
    </a>
    
</div>
