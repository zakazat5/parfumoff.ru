<div class="brand__list">
    <?php foreach ($this->brend_liter as $brend_liter): ?>
        <div class="brand__list-item">
            <div class="brand__letter"><?php echo strtoupper($brend_liter); ?></div>
            <div class="brand__letter-list clearfix">
                <?php foreach ($this->brend_item as $brend_item): ?>
                    <?php if ($brend_liter == $brend_item['liter']): ?>
                        <div class="brand__letter-item col-md-3 col-xs-6">
                            <a href="/production/<?php echo $brend_item['url'].'/'.$suffix; ?>">
                                <?php if ((int) $brend_item['is_top'] === 1): ?>
                                    <strong><i class="icon icon-star"></i><?php echo $brend_item['title']; ?></strong>
                                <?php else: ?>
                                    <?php echo $brend_item['title']; ?>
                                <?php endif; ?>
                            </a>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
</div>