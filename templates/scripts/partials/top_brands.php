<div class="brands">
    <h2 class="brands__header">ТОП 300 брендов</h2>
		<div class="brands__alphabet">
			<?php foreach ($this->aTop300['letters'] as $letter => $groupItem) { ?>
				<a class="brands__alphabet-letter <?= ($letter == 'A') ? 'brands__alphabet-letter--active' : '' ?>" href=""><?= $letter ?></a>
			<?php } ?>
			<?php foreach ($this->aTop300['numbers'] as $letter => $groupItem) { ?>
				<a class="brands__alphabet-letter <?= ($letter == 'A') ? 'brands__alphabet-letter--active' : '' ?>" href=""><?= $letter ?></a>
			<?php } ?>	
		</div>
       	<?php foreach ($this->aTop300['letters'] as $letter => $groupItem) { ?>
			<div  class="brands__list" data-letter="<?= $letter ?>" <?=($letter!='A')?'style="display:none"':''?>>
				<?php foreach ($groupItem as $item) { ?>
					<a class="brands__item" href="/production/<?=$item['url']?>/"><?=($item['is_top300_ancor'])?></a>
				<?php } ?>
			</div>
		<?php } ?>
		<?php foreach ($this->aTop300['numbers'] as $letter => $groupItem) { ?>
			<div  class="brands__list" data-letter="<?= $letter ?>" style="display:none">
				<?php foreach ($groupItem as $item) { ?>
					<a class="brands__item" href="/production/<?=$item['url']?>/"><?=($item['is_top300_ancor'])?></a>
				<?php } ?>
			</div>
		<?php } ?>
</div>
<div class="brands">
    <h2 class="brands__header">Популярные ароматы</h2>
    <div class="brands__alphabet">
			<?php foreach ($this->aTop300aromat['letters'] as $letter => $groupItem) { ?>
				<a class="brands__alphabet-letter <?= ($letter == 'A') ? 'brands__alphabet-letter--active' : '' ?>" href=""><?= $letter ?></a>
			<?php } ?>
			<?php foreach ($this->aTop300aromat['numbers'] as $letter => $groupItem) { ?>
				<a class="brands__alphabet-letter <?= ($letter == 'A') ? 'brands__alphabet-letter--active' : '' ?>" href=""><?= $letter ?></a>
			<?php } ?>	
	</div>
       	<?php foreach ($this->aTop300aromat['letters'] as $letter => $groupItem) { ?>
			<div  class="brands__list" data-letter="<?= $letter ?>" <?=($letter!='A')?'style="display:none"':''?>>
				<?php foreach ($groupItem as $item) { ?>
					<a class="brands__item" href="/production/<?=$item['brand_url']?>/<?=$item['url']?>/"><?=($item['is_top300_ancor'])?></a>
				<?php } ?>
			</div>
		<?php } ?>
		<?php foreach ($this->aTop300aromat['numbers'] as $letter => $groupItem) { ?>
			<div  class="brands__list" data-letter="<?= $letter ?>" style="display:none">
				<?php foreach ($groupItem as $item) { ?>
					<a class="brands__item" href="/production/<?=$item['brand_url']?>/<?=$item['url']?>/"><?=($item['is_top300_ancor'])?></a>
				<?php } ?>
			</div>
		<?php } ?>      
          
</div>
<script>
        $(function () {
            $('.brands__alphabet a').click(function (e) {
                e.preventDefault()

                var elem = $(this),
                    letter = elem.text(),
                    letter_items = elem.parents('.brands__alphabet').next()

                elem.parents('.brands__alphabet').find('.brands__alphabet-letter').removeClass('brands__alphabet-letter--active')
                elem.addClass('brands__alphabet-letter--active')

                letter_items.parent().find('.brands__list').hide()
                letter_items.parent().find('.brands__list[data-letter=' + letter + ']').show()

            })
        })
</script>