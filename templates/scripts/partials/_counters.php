<!-- Код тега ремаркетинга Google -->
<!--------------------------------------------------
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
--------------------------------------------------->

<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 958656022;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>

<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/958656022/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

<div class="code-footer-links">
<div>
<!--LiveInternet counter-->
<script type="text/javascript"><!--
    document.write("<a href='http://www.liveinternet.ru/click' "+
    "target=_blank><img src='http://counter.yadro.ru/hit?t45.13;r"+
    escape(document.referrer)+((typeof(screen)=="undefined")?"":
    ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
    ";"+Math.random()+
    "' alt='' title='LiveInternet' "+
    "border='0' width='31' height='31'><\/a>")
    //--></script>
	<!--/LiveInternet-->
</div>
</div>


    <? if ($this->main['canon_name1'] != 'korzina' ):?>

        <!--
        BEGIN JIVOSITE CODE {literal} -->
		
		<!--
        <script type='text/javascript'>
		if (screen.width > 480) {
			(function(){
			var widget_id = '1nW2vPalyZ';
			var s =
				document.createElement('script'); s.type = 'text/javascript'; s.async = true;
			s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss =
            document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s,
            ss);})();
		}	
		</script>
		-->
		
<!-- {/literal} END JIVOSITE CODE -->
    <? endif;?>

	
<?php if ($city_utf8['domain'] === 'msk') { ?>	

<!-- Yandex.Metrika counter -->

<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter21451555 = new Ya.Metrika({id:21451555,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21451555" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<!-- /Yandex.Metrika counter -->

<?php } else { ?>

<!-- Yandex.Metrika counter -->

<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter42164749 = new Ya.Metrika({
                    id:42164749,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/42164749" style="position:absolute; left:-9999px;" alt="" /></div></noscript>

<!-- /Yandex.Metrika counter -->

<?php } ?>



<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
    function getRetailCrmCookie(name) {
        var matches = document.cookie.match(new RegExp(
            '(?:^|; )' + name + '=([^;]*)'
        ));

        return matches ? decodeURIComponent(matches[1]) : '';
    }

    ga('create', 'UA-52259840-1', 'parfumoff.ru');
    ga('set', 'dimension1', getRetailCrmCookie('_ga'));
	ga('set','dimension2','<?php echo $this->catalog_full['id']; ?>'); 	
	ga('set','dimension3','product'); 	
    ga('send', 'pageview');

	
    /* Accurate bounce rate by time */
    if (!document.referrer || document.referrer.split('/')[2].indexOf(location.hostname) != 0)
        setTimeout(function () {
            ga('send', 'event', '15_seconds', location.pathname);
        }, 15000);
	
</script>

<!--Трэкер-контейнер "Основной"--> 
<script type="text/javascript">
window.__mixm__ = window.__mixm__ || [];
window.__mixm__.push(['mAdvId',1294983815]);
// только на страницах карточки товара передавайте его айди (вместо 'ID товара') из вашего прайса и раскомментируйте вызов этого параметра
window.__mixm__.push(['skulist', '<?php echo $this->catalog_full['id']; ?>']);

(function(){function t(){if(!e){e=1;var t=0,a="def";for(i=0;o.__mixm__.length>i;i++){if("uAdvArId"==o.__mixm__[i][0]){t="u"+o.__mixm__[i][1];break}"mAdvId"==o.__mixm__[i][0]&&(a="m"+o.__mixm__[i][1])}t||(t=a);var n=document.createElement("script");n.type="text/javascript",n.async=!0,n.src=("https:"==document.location.protocol?"https://":"http://")+"js.mixmarket.biz/a"+t+".js?t="+(new Date).getTime();var r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(n,r)}}var e=0,a=document,n=a.documentElement,o=window;"complete"==a.readyState||"loaded"==a.readyState||"interactive"==a.readyState?t():a.addEventListener?a.addEventListener("DOMContentLoaded",t,!1):a.attachEvent?(n.doScroll&&o==o.top&&function(){try{n.doScroll("left")}catch(e){return setTimeout(arguments.callee,0),void 0}t()}(),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t()})):o.onload=t})();
</script> 
<!--Трэкер-контейнер "Основной"-->


<? //require_once ('ecommerce.php')?>
