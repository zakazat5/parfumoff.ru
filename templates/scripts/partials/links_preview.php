<?php if ($this->linking) { ?>
    <div class="ccSlider hide_979" style="padding: 40px 0 0;margin-bottom:60px;height:auto;overflow:hidden">
        <ul class="vertical">
            <? foreach ($this->linking as $key => $link): ?>
                <li><a href="<?= $link['to']; ?>">
                    <img data-big="<?= $link['preview']; ?>"
                         src="<?= $link['preview']; ?>" alt="<?= $link['ancor']; ?>">

                    <div class="ccContent">
                        <div class="ccTitle"><?= $link['ancor']; ?></div>
                    </div>
                    </a>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
<?php } ?>
