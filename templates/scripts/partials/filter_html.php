<div class="filter">
    <form method="get" action="/filter_test/">
        <div class="filter__top">
            <div style="width:250px" class="fl_left">
                <div class="filter_select pt5">
                    <a href="" class=" td_none relative open_list filter_1">
                    <span class="dashed ">
                        Тип парфюма</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open filter_select__open_double" style="display:none">
                        <? foreach ($this->aromaTypeFilter as $key => $aroma_type) : ?>
                            <div class="">
                                <label class="label_check <?= ($this->checked['aroma_type'][$key]) ? 'c_on' : '' ?>"
                                       for="" data-id="<?= $key ?>">
                                    <input name="aroma_type[]" type="checkbox"
                                           value="<?= $key ?>" <?= ($this->checked['aroma_type'][$key]) ? 'checked' : '' ?> /><?= $aroma_type ?>
                                </label>
                            </div>

                        <? endforeach; ?>


                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>
            </div>
            <div style="width:340px" class="fl_left">
                <div class="filter_select pt5">
                    <a href="" class=" td_none relative open_list filter_2">
                    <span class="dashed ">
                        Стоимость 100 мл аромата</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open filter_select__open_double" style="display:none">
                        <? foreach ($this->s100filter as $key => $item): ?>
                            <div class="">
                                <label class="label_check <?= ($this->checked['s100'][$key]) ? 'c_on' : '' ?>"
                                       for="" data-id="<?= $key ?>"><input
                                        name="s100[]" <?= ($this->checked['s100'][$key]) ? 'checked' : '' ?>
                                        value="<?= $key ?>" type="checkbox"/><?= $item ?></label>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>
            </div>
            <div style="width:165px" class="fl_left">
                <div class="filter_select pt5">
                    <a href="" class=" td_none relative open_list filter_3">
                    <span class="dashed ">
                        Пол</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open filter_select__open_double" style="display:none">
                        <div class="">
                            <label class="label_check <?= ($this->checked['pol']['F']) ? 'c_on' : '' ?>" for="" data-id="F"><input
                                    name="pol[]" value="F" <?= ($this->checked['pol']['F']) ? 'checked' : '' ?>
                                    type="checkbox"/>женский</label>
                        </div>
                        <div class="">
                            <label class="label_check <?= ($this->checked['pol']['M']) ? 'c_on' : '' ?>" for="" data-id="M"><input
                                    name="pol[]" value="M" <?= ($this->checked['pol']['M']) ? 'checked' : '' ?>
                                    type="checkbox"/>мужской</label>
                        </div>
                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>
            </div>
            <div style="" class="fl_right">
                <a href="" id="show_filter" class="btn2 btn2-big fs16">Еще параметры</a>
            </div>
        </div>
        <div class="filter__bottom" style="display:none">
            <div class="lCol">
                <div class="filter_select mb20">
                    <a href="" class=" td_none relative open_list filter_1">
                    <span class="dashed ">
                        Тип парфюма</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open filter_select__open_double" style="display:none">
                        <? foreach ($this->aromaTypeFilter as $key => $aroma_type) : ?>
                            <div class="">
                                <label class="label_check <?= ($this->checked['aroma_type'][$key]) ? 'c_on' : '' ?>"
                                       for="" data-id="<?= $key ?>">
                                    <input name="aroma_type[]" type="checkbox"
                                           value="<?= $key ?>" <?= ($this->checked['aroma_type'][$key]) ? 'checked' : '' ?> /><?= $aroma_type ?>
                                </label>
                            </div>

                        <? endforeach; ?>
                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>
                <div class="filter_select mb20">
                    <a href="" class=" td_none relative open_list filter_2">
                    <span class="dashed ">
                        Стоимость 100 мл аромата</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open filter_select__open_double" style="display:none">
                        <? foreach ($this->s100filter as $key => $item): ?>
                            <div class="">
                                <label class="label_check <?= ($this->checked['s100'][$key]) ? 'c_on' : '' ?>"
                                       for="" data-id="<?= $key ?>"><input
                                        name="s100[]" <?= ($this->checked['s100'][$key]) ? 'checked' : '' ?>
                                        value="<?= $key ?>" type="checkbox"/><?= $item ?></label>
                            </div>
                        <? endforeach; ?>
                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>
                <!--<div class="filter_select mb20" style="display: none">
                    <a href="" class=" td_none relative open_list filter_3">
                    <span class="dashed ">
                        Применение</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open filter_select__open_double" style="display:none">
                        <div class="">
                            <label class="label_check" for="" data-id="9"><input type="checkbox"/>вечерние</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="10"><input type="checkbox"/>дневные</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="11"><input type="checkbox"/>классические</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="12"><input type="checkbox"/>клубные</label>
                        </div>
                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>-->
                <!--<div class="filter_select mb20" style="display:none">
                    <a href="" class=" td_none relative open_list">
                    <span class="dashed ">
                        Время года</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open" style="display:none">
                        <div class="">
                            <label class="label_check" for="" data-id="13"><input type="checkbox"/>вечерние</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="14"><input type="checkbox"/>дневные</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="15"><input type="checkbox"/>классические</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="16"><input type="checkbox"/>клубные</label>
                        </div>
                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>
                <div class="filter_select mb20" style="display:none">
                    <a href="" class=" td_none relative open_list">
                    <span class="dashed ">
                        Семейство</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open" style="display:none">
                        <div class="">
                            <label class="label_check" for="" data-id="17"><input type="checkbox"/>вечерние</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="18"><input type="checkbox"/>дневные</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="19"><input type="checkbox"/>классические</label>
                        </div>
                        <div class="">
                            <label class="label_check" for="" data-id="20"><input type="checkbox"/>клубные</label>
                        </div>
                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>-->
                <div class="filter_select mb20">
                    <a href="" class=" td_none relative open_list filter_3">
                    <span class="dashed ">
                        Пол</span>
                        <i class="icon-arrow-black"></i>
                    </a>

                    <div class="filter_select__open filter_select__open_double" style="display:none">
                        <div class="">
                            <label class="label_check <?= ($this->checked['pol']['F']) ? 'c_on' : '' ?>" for="" data-id="F"><input
                                    name="pol[]" value="F" <?= ($this->checked['pol']['F']) ? 'checked' : '' ?>
                                    type="checkbox"/>женский</label>
                        </div>
                        <div class="">
                            <label class="label_check <?= ($this->checked['pol']['M']) ? 'c_on' : '' ?>" for="" data-id="M"><input
                                    name="pol[]" value="M" <?= ($this->checked['pol']['M']) ? 'checked' : '' ?>
                                    type="checkbox"/>мужской</label>
                        </div>
                    </div>
                    <div class="filter_options_mobile_wrap"></div>
                </div>
            </div>
            <div class="mCol ">
                <div class="ta_center empty_text">
                    <div style="line-height:20px" class="fs20 mb10 lite">
                        Вы пока не выбрали <br/> ни одного параметра.
                    </div>
                    <div class="fs14 lite" style="line-height:16px">
                        Сейчас отображаются <br/>
                        все возможные товары. <br/>
                        Укажите один или несколько <br/>
                        интересующих вас параметров и <br/>
                        нажмите: &laquo;Показать избранное&raquo;.
                    </div>
                </div>
                <div style="display:none" class="results"></div>
            </div>
            <div class="rCol">
                <div class="mb20">
                    <a href="" id="hide_filter" class="color_gray1 dashed">Скрыть параметры поиска</a>
                    <a href="" id="hide_filter_mobile" class="color_gray1 dashed">Скрыть параметры поиска</a>
                </div>
                <div class="empty_filter_wrap" style="margin-bottom:25px">
                    <a href="" id="empty_filter" class="dashed">Очистить характеристики</a>
                </div>
                <div id="filter_it_wrap">
                    <button type="submit" id="filter_it" class="btn2 fs16">Показать выбранное</button>
                </div>
            </div>
        </div>

        <? if ($this->brend):?>
        <input type="hidden" name="brand_id" value="<?=$this->brend['id']?>">
        <? endif;?>
    </form>
</div>