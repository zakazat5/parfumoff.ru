<div class="item <?= ($catalog_item['is_spec']) ? 'item--discount' : '' ?>">
	<a class="item__picture-link" href="<?php echo $catalog_item['url']; ?>">
		<img id="product_cat_<?php echo $catalog_item['id_catalog']; ?>" class="item__picture" src="<?php echo $catalog_item['img']; ?>">
	</a>
	<a class="item__title" href="<?php echo $catalog_item['url']; ?>">
        <div class="item__brand"><?php echo $catalog_item['brend_title']; ?></div>
        <div class="item__name"><?php echo $catalog_item['title']; ?></div>
	</a>
    <div class="item__dscr"><?php echo $catalog_item['anot']; ?></div>
	<?php if ($catalog_item['price_s'] != 0) { ?>
		<div class="item__price">
			<div class="item__price-from<?php if ($catalog_item['category_sale'] == 1) { echo ' bold category_sale'; } ?>">от <?php echo $catalog_item['price_s'] ?></div>
			<div class="item__price-to<?php if ($catalog_item['category_sale'] == 1) { echo ' bold category_sale'; } ?>">до <?php echo $catalog_item['price_e'] ?> руб.</div>
		</div>
		<div class="item__footer">
			<a class="item__buy button" onclick="show_fast_view('<?php echo $catalog_item['url']; ?>');return false;">В корзину</a>
			<a class="item__quick_buy button" onclick="quick_buy_click(<?php echo $catalog_item['id_catalog']; ?>);return false;">Купить в 1 клик</a>
		</div>
		<div class="it-det">
			<a class="item__link" href="<?php echo $catalog_item['url']; ?>">Подробнее</a>
		</div>
	<?php } else { ?>
		<div class="item__price">
			<div class="item__price-to not_available_prod"></div>
		</div>
		<div class="item__footer">
		</div>
		<div class="it-det">
			<a class="item__link" href="<?php echo $catalog_item['url']; ?>">Подробнее</a>
		</div>
	<?php } ?>
</div>