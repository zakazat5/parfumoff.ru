<?/* if ($ecommerce = $this->main['ecommerce']): */?><!--

    <script>
        ga('require', 'ecommerce');

        <?php
/*
        $trans = $ecommerce['order'];
        echo getTransactionJs($trans);

        $items = $this->main['ecommerce']['order_items'];
        foreach ($items as &$item) {
          echo getItemJs($trans['id'], $item);
        }
        */?>
        ga('ecommerce:send');
    </script>

<?/* endif; */?>

--><?/*

// Function to return the JavaScript representation of a TransactionData object.
function getTransactionJs(&$trans)
{
    return <<<HTML
        ga('ecommerce:addTransaction', {
          'id': '{$trans['id']}',
          'affiliation': 'parfumoff.ru',
          'revenue': '{$trans['dSum']}',
          'shipping': '{$trans['delivery_cost']}',
          'tax': '{$trans['tax']}',
          'currency': 'RUB'
        });
HTML;
}


// Function to return the JavaScript representation of an ItemData object.
function getItemJs(&$transId, &$item)
{
    return <<<HTML
ga('ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['title']}',
  'sku': '{$item['articul_catalog_data_order']}',
  'category': 'parfum',
  'price': '{$item['sum']}',
  'quantity': '{$item['kol']}',
  'currency': 'RUB'
});
HTML;
}

*/?>