<head>
    <title><?php echo geo_replace_utf8($city_utf8, $this->main['meta_title']);?></title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />	
	<meta name="description" content="<?php echo geo_replace_utf8($city_utf8, $this->main['meta_description']);?>" />
	<meta name="keywords" content="<?php echo geo_replace_utf8($city_utf8, $this->main['meta_keywords']);?>" />
    <meta name="apple-mobile-webapp-capable" content="yes"/>

    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

    <? if ($this->main['noindex']==true) echo '<meta name="robots" content="noindex"/>';?>
    <? if ($this->main['og:title']!='') echo '<meta property="og:title" content="'.geo_replace_utf8($city_utf8, $this->main['og:title']).'" /> '?>
    <? if ($this->main['og:image']!='') echo '<meta property="og:image" content="'.$this->main['og:image'].'" /> '?>
    <? if ($this->main['og:description']!='') echo '<meta property="og:description" content="'.geo_replace_utf8($city_utf8, $this->main['og:description']).'" /> '?>
    <? if ($this->main['og:url']!='') echo '<meta property="og:url" content="'.$this->main['og:url'].'" /> '?>

	<?php if ($this->canon_name1 != 'christmassale') { ?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php } ?>
    <meta name="google-site-verification" content="iIenX44wjOQwSqAas3KPi-VGArn1bbHhl8JSwnaqqqs" />
    <meta name='yandex-verification' content='409ef96cc5640923' />
	<meta name="mailru-verification" content="0afd7cdfb1b4f685" />
    <meta name="yandex-verification" content="c7a0d3eee56e5148" />

	<link rel="stylesheet" href="/templates/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="/templates/css/grid.css" type="text/css"  />
	<link rel="stylesheet" href="/templates/css/style.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/retina.css" type="text/css"  />
	<link rel="stylesheet" href="/templates/css/responsive.css" type="text/css"  />
	<link rel="stylesheet" href="/templates/css/cart.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/suggestions.css" type="text/css" />
    <link rel="stylesheet" href="/templates/css/button.css" type="text/css" />
    <link rel="stylesheet" href="/templates/css/bootstrap/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/templates/js/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="/templates/css/suggestions.css" type="text/css" />
    <link rel="stylesheet" href="/templates/lib/star-rating/jquery.rating.css" type="text/css" />
	
	<link rel="stylesheet" href="/templates/css/style_new.css" type="text/css" />

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>	
	<script type="text/javascript" src="/templates/js/script.js"></script>
	<script src="/templates/lib/iphone_checkbox/iphone_checkbox.js"></script>
	<link rel="stylesheet" href="/templates/lib/iphone_checkbox/style.css" />
    <script src="/templates/js/cart.js"></script>
	<script type="text/javascript" src="/templates/js/scripts.js"></script>
	<script type="text/javascript" src="/templates/js/filter_v2.js"></script>
    <script type="text/javascript" src="/templates/lib/star-rating/jquery.rating.pack.js"></script>

    <link href="/templates/lib/select2-3.5.1/select2.css" rel="stylesheet"/>
    <script src="/templates/lib/select2-3.5.1/select2.js"></script>

    <script type="text/javascript" src="/templates/js/dadata/suggestions-jquery-4.1.min.js"></script>
    <script type="text/javascript" src="/templates/js/dadata.js"></script>
    <script type="text/javascript" src="/templates/js/suggest.js"></script>
    <script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>
    <script src="/templates/js/bootstrap-select/bootstrap-select.js"></script>
    <script src="/templates/js/validate/jquery.validate.js"></script>
    <script src="/templates/js/validate/localization/messages_ru.min.js"></script>
    <script src="/templates/js/bootstrap.js"></script>
    <script src="/templates/js/jquery_lazyload/jquery.lazyload.js?v=1.9.1"></script>
    <script src="http://vk.com/js/api/openapi.js" type="text/javascript"></script>
    <? if (isset($this->main['canonical'])):?>
    <!--<link rel="canonical" href="http://<?//php echo $host; ?>/<?//=$this->main['canonical']?>">-->
    <? endif;?>

    <link rel="stylesheet" href="/templates/js/jquery.scrollbar/jquery.scrollbar.css" />
    <script src="/templates/js/jquery.scrollbar/jquery.scrollbar.js"></script>


    <link rel="stylesheet" href="/templates/js/arcticmodal/jquery.arcticmodal-0.3.css?v=1" type="text/css" media="screen" />
    <script src="/templates/js/arcticmodal/jquery.arcticmodal-0.3.min.js?v=1"></script>
<script src="https://use.fontawesome.com/8e9e721e48.js"></script>
	
	

    <script>
        var region_name = '<?=$this->main['location']['region_name']?>';
        var city_name = '<?=$this->main['location']['city_name']?>';
    </script>
	
<!-- CallTouch-->	
<script type="text/javascript">
function ct(w,d,e,c){
var a='all',b='tou',src=b+'c'+'h';src='m'+'o'+'d.c'+a+src;
var jsHost="https://"+src,s=d.createElement(e),p=d.getElementsByTagName(e)[0];
s.async=1;s.src=jsHost+"."+"r"+"u/d_client.js?param;"+(c?"client_id"+c+";":"")+"ref"+escape(d.referrer)+";url"+escape(d.URL)+";cook"+escape(d.cookie)+";";
if(!w.jQuery){var jq=d.createElement(e);
jq.src=jsHost+"."+"r"+'u/js/jquery-1.7.min.js';
jq.onload=function(){
p.parentNode.insertBefore(s,p);};
p.parentNode.insertBefore(jq,p);}else{
p.parentNode.insertBefore(s,p);}}
if(!!window.GoogleAnalyticsObject){window[window.GoogleAnalyticsObject](function(tracker){ 
if (!!window[window.GoogleAnalyticsObject].getAll()[0])
{ct(window,document,'script', window[window.GoogleAnalyticsObject].getAll()[0].get('clientId'))}
else{ct(window,document,'script', null);}});
}else{ct(window,document,'script', null);}
</script>

<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;

n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,

document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1291083647577345');

fbq('track', 'PageView');

</script>

<noscript><img height="1" width="1" style="display:none"

src="https://www.facebook.com/tr?id=1291083647577345&ev=PageView&noscript=1"

/></noscript>

<!-- DO NOT MODIFY -->

<!-- End Facebook Pixel Code -->

<!-- Rees46 Code -->

<script>
(function(r){window.r46=window.r46||function(){(r46.q=r46.q||[]).push(arguments)};var s=document.getElementsByTagName(r)[0],rs=document.createElement(r);rs.async=1;rs.src='//cdn.rees46.com/v3.js';s.parentNode.insertBefore(rs,s);})('script');
// Init everything
r46('init', '32f7c3e11cc04298e31945b8ffba6f');
</script>

<!-- End Rees46 Code -->

<script type="text/javascript">

var _alloka = {
objects: {
'614dbe6096cbe04d': {
block_class: 'phone_alloka'
}
},
trackable_source_types: ["utm"],
url_params: {'utm_source': ['yad', 'google_RF', 'YandexDirect', 'google_MSK']},
last_source: false,
use_geo: true
 };
</script>
<script src="https://analytics.alloka.ru/v4/alloka.js" type="text/javascript"></script>

</head> 