<a class="article pull-left col-md-4 col-xs-12 color_blue" href="/<?=$news_item['url']; ?>/">
    <?php if($news_item['img']!=''): ?>
    <div class="img">
        <img src="<?php echo $news_item['img']; ?>" alt="<?php echo $news_item['alt'];?>" />
    </div>
    <?php endif; ?>
    <div class="title"><?php echo $news_item['title']; ?></div>
    <div class="date color_gray2"><? echo $news_item['date']?></div>
</a>