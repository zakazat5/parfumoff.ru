<?php if ($this->canon_name1 == 'christmassale') { ?>
<? if (!$this->main['hide_bottom_bar']):?>

<!--
<div style="text-align: center">
   <a href="http://www.parfumoff.ru/discount/atomajzer-v-podarok/" target="_blank"><img src="/templates/images/banner2.jpg"></a>
</div><br>
-->

<div class="social_wrap">
<div class="row block_list1">
	<div class="col-span-4 social_block">
		<div class="title">Мы в соцсетях</div>
        <div id="vk_groups"></div>
        <script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "100"}, 2679687);
        </script>
        <!--<iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fparfumoff.ru&amp;width&amp;height=258&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=true" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:258px;" allowTransparency="true"></iframe>-->
	</div>
	<div class="col-span-4 hover plusses_block" style="border-left:0">
		<div class="title">Почему выбирают нас</div>

		<ul class="plus_wrap sb">
            <li class="i1">
                <a>
                    <i class="active"></i>

                    <div>
                        <span class="bold">Оригинальная</span> парфюмерия
                    </div>
                </a>

            </li>
           
            <li class="i3">
                <a>
                    <i class="active"></i>

                    <div>
                        <span class="bold">Официальные</span> дистрибьюторы<br>
                        и надежные поставщики
                    </div>
                </a>
            </li>
            <li class="i4">
                <a>
                    <i class="active"></i>

                    <div>
                        Легкий <span class="bold">возврат</span> денег
                    </div>
                </a>

               
            </li>
            
            <li class="i7">
                <a>
                    <i class="active"></i>

                    <div>
                        Лучшие цены и накопительная<br>
                        <span class="bold">система скидок</span>
                    </div>
                </a>

               
            </li>
        </ul>
		<? /*
		<div class="mb20">
			<div class="circle">
				<div class=" icon-dost"></div>
			</div>
			<div style="margin-left:85px;line-height:60px;height:60px">Бесплатная доставка</div>
		</div>
		<div class="mb20">
			<div class="circle">
				<div class=" icon-percent"></div>
			</div>
			<div style="margin-left:85px;padding-top:10px;line-height:22px;height:60px">Накопительная система скидок</div>
		</div>
		<div class="mb">
			<div class="circle">
				<div class=" icon-heart"></div>
			</div>
			<div style="margin-left:85px;line-height:20px;height:60px">Нам доверяют десятки тысяч клиентов по всей России</div>
		</div>
		*/ ?>
	</div>
	<div class="col-span-4 comments_block" style="border-left:0">
		<div class="title mb20">Отзывы о магазине</div>		
		<? foreach ($this->main['aRecommend'] as $oRecomend):?>
			<div class="comment">
				<div class="comment__title"><?=$oRecomend['user_name']?></div>
				<div class="stars ">
					<div class="star_a"></div>
					<div class="<?=($oRecomend['rate']>1)?'star_a':'star_ua'?>"></div>
					<div class="<?=($oRecomend['rate']>2)?'star_a':'star_ua'?>"></div>
					<div class="<?=($oRecomend['rate']>3)?'star_a':'star_ua'?>"></div>
					<div class="<?=($oRecomend['rate']>4)?'star_a':'star_ua'?>"></div>
				</div>
				<div class="text"><?=mb_substr($oRecomend['text'],0,60,'utf-8')?>...</div>
			</div>

		<? endforeach;?>

		<a class="color_blue td_underline" href="/recommend/">Еще отзывы</a>
	</div>
</div>
</div>
<? endif;?>
<?php } else { ?>

<?php if (!$this->main['hide_bottom_bar']) { ?>
<div class="info">
    <div class="social info__item">
        <div class="social__header">Мы в соцсетях</div>
        <div class="social__content" id="vk_groups"></div>
		<script type="text/javascript">
            VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "100"}, 2679687);
        </script>
    </div>
    <div class="advantages info__item">
        <div class="advantages__header">Почемы выбирают нас</div>
        <div class="advantages__list">
            <div class="advantages__item">
                <div class="advantages__item-icon advantages__item-icon--original"></div>
                <div class="advantages__item-content"> <span class="advantages__item-content-bold">Оригинальная </span>парфюмерия</div>
            </div>
            <div class="advantages__item">
                <div class="advantages__item-icon advantages__item-icon--distributors"></div>
                <div class="advantages__item-content"> <span class="advantages__item-content-bold">Официальные </span>дистрибьюторы и надежные поставщики</div>
            </div>
            <div class="advantages__item">
                <div class="advantages__item-icon advantages__item-icon--cashback"></div>
                <div class="advantages__item-content">Легкий <span class="advantages__item-content-bold">возврат </span>денег</div>
            </div>
            <div class="advantages__item">
                <div class="advantages__item-icon advantages__item-icon--discounts"></div>
                <div class="advantages__item-content">Лучшие цены и накопительная <span class="advantages__item-content-bold">система скидок</span></div>
            </div>
        </div>
    </div>
    <div class="reviews info__item">
        <div class="reviews__header">Отзывы о магазине</div>
        <div class="reviews__list">
			<?php foreach ($this->main['aRecommend'] as $oRecomend) { ?>
            <div class="reviews__item">
                <div class="reviews__item-author"><?=$oRecomend['user_name']?></div>
                    <div class="stars">
						<div class="stars__item"></div>
						<div class="<?=($oRecomend['rate']>1)?'stars__item':''?>"></div>
						<div class="<?=($oRecomend['rate']>2)?'stars__item':''?>"></div>
						<div class="<?=($oRecomend['rate']>3)?'stars__item':''?>"></div>
						<div class="<?=($oRecomend['rate']>4)?'stars__item':''?>"></div>
                    </div>
                  <div class="reviews__item-content"><?=mb_substr($oRecomend['text'],0,60,'utf-8')?>...</div>
            </div>
			<?php } ?>
        </div><a class="reviews__link" href="/recommend/">Еще отзывы</a>
    </div>
</div>
<?php } ?>
<?php } ?>