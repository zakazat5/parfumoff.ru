<div class="filter">
    <form method="get" action="/filter/">
        <div class="filter__top">
            <div class="filter_options-wrap">
                <div style="width:170px" class="fl_left">
                    <div class="filter_select pt5">
                        <a href="" class=" td_none relative open_list filter_1">
                        <span class="dashed ">
                            Тип парфюма</span>
                            <i class="icon-arrow-black"></i>
                        </a>

                        
                        <div class="filter_select__open filter_select__open_double" style="display:none;width: 300px;">
                            <div class="scroll scrollbar-outer">
                            <? foreach ($this->aromaTypeFilter as $key => $aroma_type) : ?>
                                <div class="">
                                    <label
                                        class="label_check <?= $this->checked['aroma_type'][$key] ?> <?= ($this->checked['aroma_type'][$key]) ? 'c_on' : '' ?>"
                                        for="">
                                        <input name="aroma_type[]"
                                               value="<?= $key ?>" <?= ($this->checked['aroma_type'][$key]) ? 'checked' : '' ?>
                                               type="checkbox"/><?= $aroma_type ?></label>
                                </div>
                            <? endforeach; ?>
                            </div>
                        </div>
                        
                        <div class="filter_options-selected">
                            <?php
                                foreach ($this->checked['aroma_type'] as $key=>$type) {
                                    echo '<span class="tag2"><span class="text">'.$this->aromaTypeFilter[$key].'</span><span class="close">×</span></span>';
                                }
                            ?>
                        </div>

                        <div class="filter_options_mobile_wrap"></div>
                    </div>
                </div>

                <div style="width:270px" class="fl_left">
                    <div class="filter_select pt5">
                        <a href="" class=" td_none relative open_list filter_2">
                        <span class="dashed ">
                            Стоимость 100 мл аромата</span>
                            <i class="icon-arrow-black"></i>
                        </a>

                        <div class="filter_select__open filter_select__open_double" style="display:none">
                            <div class="scroll scrollbar-outer">
                            <? foreach ($this->s100filter as $key => $item): ?>
                                <div class="">
                                    <label class="label_check <?= ($this->checked['s100'][$key]) ? 'c_on' : '' ?>"
                                           for=""><input
                                            name="s100[]" <?= ($this->checked['s100'][$key]) ? 'checked' : '' ?>
                                            value="<?= $key ?>" type="checkbox"/><?= $item ?></label>
                                </div>
                            <? endforeach; ?>
                            </div>
                        </div>
                        
                        <div class="filter_options-selected">
                            <?php
                            foreach ($this->checked['s100'] as $key=>$type) {
                                echo '<span class="tag2"><span class="text">'.$this->s100filter[$key].'</span><span class="close">×</span></span>';
                            }
                            ?>
                        </div>
                        <div class="filter_options_mobile_wrap"></div>
                    </div>
                </div>
                <div style="width:160px" class="fl_left">
                    <div class="filter_select pt5">
                        <a href="" class=" td_none relative open_list filter_3">
                        <span class="dashed ">
                            Пол</span>
                            <i class="icon-arrow-black"></i>
                        </a>

                        <div class="filter_select__open filter_select__open_double" style="display:none">
                            <div class="scroll scrollbar-outer">
                            <div class="">
                                <label class="label_check <?= ($this->checked['pol']['F']) ? 'c_on' : '' ?>" for=""><input
                                        name="pol" value="F" <?= ($this->checked['pol']['F']) ? 'checked' : '' ?> type="checkbox"/>женский</label>
                            </div>
                            </div>
                            <div class="">
                                <label class="label_check <?= ($this->checked['pol']['M']) ? 'c_on' : '' ?>" for=""><input
                                        name="pol" value="M" <?= ($this->checked['pol']['M']) ? 'checked' : '' ?> type="checkbox"/>мужской</label>
                            </div>
                        </div>

                        <div class="filter_options-selected">
                            <?php
                            foreach ($this->checked['pol'] as $key=>$type) {
                                if ($this->checked['pol']['F']){
                                    $pol = 'женский';
                                }
                                if ($this->checked['pol']['M']){
                                    $pol = 'мужской';
                                }
                                echo '<span class="tag2"><span class="text">'.$pol.'</span><span class="close">×</span></span>';
                            }
                            ?>
                        </div>
                        <div class="filter_options_mobile_wrap"></div>
                    </div>
                </div>
                <div style="width:150px" class="fl_left">
                    <div class="filter_select pt5">
                        <select name="brand_id">
                            <option value="0">Выберите бренд</option>
                            <? foreach($this->brend_item as $item):?>
                                <option <?=(isset($this->brend) && $this->brend['id']==$item['id'])?'SELECTED':''?> value="<?=$item['id']?>"><?=$item['title']?></option>
                            <? endforeach;?>
                        </select>
                    </div>
                    </div>
            </div>
            <div class="fl_right text-center" style="max-width: 180px;">
                <input type="hidden" name="sort" value="<?= ($_GET['sort']) ? $_GET['sort'] : 'abc' ?>">
                <button type="submit" class="btn2 btn2-big fs16" >Показать выбранное</button>
                <div id="empty_filter" class="color color_def td_dotted pt20" style="display: inline-block;">Очистить фильтр</div>
                <!--<a href="" id="show_filter" class="">Показать выбранное</a>-->
            </div>
        </div>
    </form>
</div>


<link rel="stylesheet" href="/templates/js/jquery.scrollbar/jquery.scrollbar.css" />
<script src="/templates/js/jquery.scrollbar/jquery.scrollbar.js"></script>

<script>
$(function(){
$('.filter_select__open .scroll').scrollbar();
})
</script>