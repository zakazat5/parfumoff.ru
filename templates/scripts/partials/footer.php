<div id="toTop">^</div>

<script type="text/javascript">
 
$(function() {
 
$(window).scroll(function() {
 
if($(this).scrollTop() != 0) {
 
$('#toTop').fadeIn();
 
} else {
 
$('#toTop').fadeOut();
 
}
 
});
 
$('#toTop').click(function() {
 
$('body,html').animate({scrollTop:0},800);
 
});
 
});
 
</script>

<?php if ($this->canon_name1 == 'christmassale') { ?>
<footer>



    <div id="top_footer">
        <div class="container">
            <div id="bottom_phones_outer">
                <div id="bottom_phones" style="margin-right:55px">
                    <div class="fs24 head pt20 mb10"><?=$city_utf8['name']?></div>
                    <div class="fs14">Адрес: <?=$city_utf8['address']?></div>
                    <span class="call_phone_bottom_1"><a href="tel:<?=$city_utf8['phone']?>"
                       class="phone d_block mobile_phone fs14 lite mb5">Тел.: <?=$city_utf8['phone']?></a></span>
                    <br>
                </div>
				<!--
                <div id="bottom_phones" style="margin-right:55px">
                    <div class="fs24 head pt20 mb10">Санкт-Петербург</div>
                    <div class="fs14">Адрес: пр-кт Юрия Гагарина, д. 2</div>
                    <span class="call_phone_bottom_1"><a href="tel:8 (812) 426-11-15"
                       class="phone d_block mobile_phone fs14 lite mb5"><br>Тел.: 8 (812) 426-11-15</a></span>
                    <br>
                </div>
				-->
            </div>
            <div id="bottom_nav">
                <ul class="fs14" >
                    <li><a href="/girl/">Женская парфюмерия</a></li>
                    <li><a href="/men/">Мужская парфюмерия</a></li>
                    <li style="width:17%"><a href="/recommend/">Отзывы</a></li>
                </ul>
                <ul class="fs14 pay">
                    <li><a href="/oplata/">Мы принимаем:</a></li>
                    <li><a href="/oplata/"><img style="margin-top: 5px;" src="/templates/images/pay1.png"></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div id="bottom_txt">
                Вопросы и предложения присылайте на электронную почту info@parfumoff.ru
            </div>
        </div>
    </div>
    <div id="bottom_footer">
        <div class="container">
            <ul id="bottom_menu">
                <li><a href="/about/#contact">Контакты</a></li>
                <li><a href="/worktime/">Режим работы</a></li>
                <li><a href="/about/#dostavka">Доставка</a></li>
                <li><a href="/oplata/">Оплата</a></li>
                <li><a href="/kachestvo/">Качество и гарантии</a></li>
                <li><a href="/disc/">Скидки</a></li>
                <li><a href="/article/">Статьи</a></li>
                <li><a href="/o_nas/">О нас</a></li>
                <li><a href="/return/">Возврат</a></li>
                <li><a href="/map/">Карта сайта</a></li>
            </ul>
            <div class="fl_left copyright" style="width: 100%">
                &copy; Parfumoff.ru 2008 - <?= date('Y') ?>
                <div style="float: right;text-align: right">
                <? include_once('_counters.php') ?>
                </div>
            </div>

        </div>
    </div>
</footer>
<?php } else { ?>
<div class="footer">
    <div class="footer__content">
        <div class="wrapper footer__wrapper">
            <div class="footer__content-left">
				<div class="footer__city"><?=$city_utf8['name']?></div>
				<div class="footer__contacts">Адрес: <?=$city_utf8['address']?> <br>Тел.: <span class="phone_alloka"><?=$city_utf8['phone']?></span> <br> Вопросы и предложения: info@parfumoff.ru</div>
				<div class="footer__menu">
					<a class="footer__menu-link" href="/about/#contact">Контакты</a>
					<a class="footer__menu-link" href="/worktime/">Режим работы</a>
					<a class="footer__menu-link" href="/about/#dostavka">Доставка</a>
					<a class="footer__menu-link" href="/oplata/">Оплата</a>
					<a class="footer__menu-link" href="/kachestvo/">Качество и гарантии</a>
					<a class="footer__menu-link" href="/disc/">Скидки</a>
					<a class="footer__menu-link" href="/article/">Статьи</a>
					<a class="footer__menu-link" href="/o_nas/">О нас</a>
					<a class="footer__menu-link" href="/return/">Возврат</a>
                    <a class="footer__menu-link" href="/map/">Карта сайта</a>
				</div>
            </div>
			<a class="footer__payments" href="/oplata/">
				<div class="footer__payments-header">Мы принимаем:</div>
				<img class="footer__payments-list" src="/templates/img/payments.png" alt="Payments">
			</a>
        </div>
    </div>
</div>
<div class="copyright">
    <div class="wrapper copyright__wrapper">
        <div class="copyright__content">© Parfumoff.ru 2008 - <?= date('Y') ?></div>
		<div class="copyright__lv-link">
			<?php include_once('_counters.php'); ?>
		</div>
    </div>
</div>
<?php } ?>

