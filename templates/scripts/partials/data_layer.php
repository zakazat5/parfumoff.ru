<?php
	if ($canon_name1 != 'thankyou_quickorder') {
		if (isset($this->user['email'])) {
			$source_address = $this->user['email'];
			$processed_address = strtolower($source_address);
			$GLOBALS['dataLayer']['email'] = trim($processed_address);
		} else {
			$GLOBALS['dataLayer']['email'] = '';
		}
	}
	if ($_SERVER['REQUEST_URI'] == '/') {
		$GLOBALS['dataLayer']['PageType'] = 'HomePage';
	}
	$GLOBALS['dataLayer']['UserSegment'] = $GLOBALS["user_segment_geo"];
?>
<!-- dataLayer -->

<script type="text/javascript">
window.dataLayer = window.dataLayer || [];
window.dataLayer.push(
<?php echo json_encode($GLOBALS['dataLayer']); ?>
);
</script>

<!-- End dataLayer -->