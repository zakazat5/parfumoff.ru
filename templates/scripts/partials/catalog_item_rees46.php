<?php
/**
 * @var $item array
 */
?>
<div class="col-md-2 col-sm-6 item_mini">
    <a href="<?=$item['url'];?>">
        <div class="img hidden-xs">
            <img alt="<?$item['title'];?>" src="<?=$item['img'];?>"/>
        </div>
        <div class="title hidden-xs">
            <span><?=$item['brend_title'];?></span>
            <span class="color_purple">
              <?=$item['title'];?>
            </span>
        </div>
        <span class="visible-xs-inline-block color_blue td_underline">
          <?=$item['brend_title'];?>
          <?=$item['title'];?>
        </span>
    </a>
</div>
