<div class="aromat_table_outer">
    <?

    function orderByKeys(array $array, array $keys)
    {
        $keys = array_intersect($keys, array_keys($array));
        return array_replace(array_flip($keys), $array);
    }
    foreach ($this->catalog_item_order as &$item) {
        foreach ($this->aroma_type_group as $key=>$group) {
            if (in_array($item['t'],$group)){
                $aTypes[$key] = $key;
                $item['group'] = $key;
            }
        }
    }
    if ($aTypes){
        $aTypes = orderByKeys($aTypes, ['Туалетная вода', 'Туалетные духи','Одеколон','Парфюм','Наборы','Дезодоранты','Уход']);
    }

    ?>

    <ul onclick="ga('send', 'event', 'vidtipa', 'action')" class="aromat_table_tabs <?=(count($aTypes)>4)?'short_padding':''?>">
        <li class="active"><a data-group="all" href="">Все</a></li>
        <? foreach ($aTypes as $key => $type): ?>
            <li><a data-group="<?= $key ?>" href=""><?= $key ?></a></li>
        <? endforeach; ?>
    </ul>
    <table class="aromat_table table_new table table-bordered table-striped">
        <thead style="background: #fc0">
        <tr class="head">
            <td>Название</td>
            <td>Объем</td>
			<?php if ($this->category_sale == 1) { ?>
				<td class="bold category_sale">Цена снижена</td>
			<?php } else { ?>
				<td>Цена</td>
			<?php } ?>
            <td>Купить</td>
			<td>Купить в 1 клик</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($this->catalog_item_order as $key => $catalog_item_order) { ?>
            <tr <?=($this->is_order==$catalog_item_order['articul'])?'style="background-color: #d4feb2"':''?> data-group="<?= $catalog_item_order['group'] ?>"
                class="<?= ($catalog_item_order['price'] == 0) ? 'disabled' : '' ?>" <?= ($_COOKIE['outstock']) ? 'style="display: table-row;"' : '' ?>>
                <td width="330" class="first_col" data-id="<?= $catalog_item_order['articul'] ?>">
                                <span
                                    class="mr10"><?= $catalog_item_order['title']; ?><?= $catalog_item_order['type']; ?></span>
                                <?  if (strpos ( $catalog_item_order['type'],' тестер')):?>
								<!--noindex-->
                                <span class="tester_wrap">
                                    <i class="aromat_table_i"></i>
                                    <div class="tester_popup">
                                        <div class="tester_title">Что такое тестер?</div>
                                        Тестер &mdash; это демонстрационный вариант аромата.
                                        Тестер ни ароматом, ни внешним видом флакона НЕ ОТЛИЧАЕТСЯ
                                        от стандартных духов. Упаковывается в транспортную картонную
                                        упаковку без красочной полиграфии.
                                    </div>
                                </span>
								<!--/noindex-->
                                <?endif;?>
                </td>
                <td>
                    <?= $catalog_item_order['volume']; ?>
                </td>
                <td class="second_col bold <?php if ($this->category_sale == 1 && $catalog_item_order['price'] > 0) { echo ' category_sale'; } ?> <? if ($catalog_item_order['price'] == 0): ?> not_available_prod  <? endif; ?>">
                    <? if ($catalog_item_order['price'] == 0): ?>
                    
                    <? else: ?>
                        <? if ($this->sell): ?>
                            <?= round($catalog_item_order['price'] + $catalog_item_order['price'] * $this->sell); ?>&nbsp;руб.
                        <? endif; ?>
                        <? if ($this->user['d_percent'] != 0 AND $catalog_item_order['is_block'] == 0) { ?>
                            <? if (!$this->sell): ?>
                                <? echo $catalog_item_order['price']; ?>
                                /<br/>
                            <? endif; ?>
                            <? echo show_users_percent($catalog_item_order['price'],
                                $this->user['d_percent']); ?>&nbsp;руб.
                        <? } else { ?>
                            <? if ($catalog_item_order['is_block'] == 1) { ?>
                                <? echo $catalog_item_order['price_old']; ?>
                                /<br/>
                                <? echo $catalog_item_order['price']; ?>&nbsp;руб.
                            <? } else { ?>
                                <? echo $catalog_item_order['price']; ?>&nbsp;руб.
                            <? } ?>
                        <? } ?>
                    <? endif; ?>
                </td>
                <td class="third_col ta_center">
                    <? if ($catalog_item_order['price']): ?>
                        <a data-id_product="<? echo $catalog_item_order['id'];?>" data-id_catalog="<? echo $catalog_item_order['id_catalog'];?>" href="" class="btn3 add_to_cart"><i
                                class="icon-cart_green show_mobile"></i><span class="hide_mobile">В&nbsp;корзину</span></a>

                        <input class="rees46 track view" value='<?=json_encode(array(
                            'id' => $catalog_item_order['id'],
                            'stock' => true,
                            'price' => $catalog_item_order['price'],
                            'name' => $this->catalog_full['title'] . ' ' . $catalog_item_order['type'] . ' ' . $catalog_item_order['volume'],
                            'categories' => array($catalog_item_order['id_catalog']),
                            'image' => 'http://www.parfumoff.ru' . $this->catalog_full['img'],
                            'url' => 'http://www.parfumoff.ru/production/' . $this->brend['url'] . '/' . $this->catalog_full['url'] . '/'
                        ))?>' type="hidden" />
                    <? else: ?>
                        <a href="" class="btn4" data-articul="<?= $catalog_item_order['articul'] ?>"><i
                                class="icon-no_aromat show_mobile"></i><span
                                class="hide_mobile">Сообщить</span></a>
                    <? endif; ?>
                </td>
				<td class="quick_buy_col">
					<?php  if ($catalog_item_order['price']) { ?>
						<a class="item__quick_buy button" onclick="quick_buy_click_product(<?php echo $catalog_item_order['id']; ?>);return false;">Купить&nbsp;в&nbsp;один&nbsp;клик</a>		
                    <?php } else { ?>
                        <a href="" class="btn4" data-articul="<?= $catalog_item_order['articul'] ?>"><i
                                class="icon-no_aromat show_mobile"></i><span
                                class="hide_mobile">Сообщить</span></a>
                    <?php } ?>
            </tr>
        <?php } ?>
        </tbody>
    </table>

    <a class="item__quick_buy button" onclick="quick_buy_click_product(<?php echo $catalog_item_order['id']; ?>);return false;">Купить&nbsp;в&nbsp;один&nbsp;клик</a>
		
		<?php if (isset($this->catalog_item_tag)){ ?>
		<strong>Ноты аромата:</strong>
			<div class="notes">
				<?php foreach ($this->catalog_item_tag as $catalog_item_tag) { ?>
				<!--<a href="/tag/<?php echo $catalog_item_tag['url']; ?>/"><?php echo $catalog_item_tag['title']; ?></a>,-->
				<?php echo $catalog_item_tag['title']; ?>,
				<?php } ?>
			</div>
			<a name="prices"></a>
		<?php }else{ ?>
			<br />
		<?php } ?>
		
<!--
    <?php if ($this->is_order == 1) { ?>
        <div style="font-size:15px;border:1px solid #eb0081;padding:10px 15px;" >
            <a href="/production/<?php echo $this->brend_url; ?>/<?php echo $this->catalog_full['url']; ?>/">Посмотреть
                все варианты и цены для <?php echo $this->catalog_full['title']; ?></a>
        </div>
    <?php } ?>
-->
</div>
<input id="global_geo_segment" type="hidden" data-geo_segment="<?php echo $GLOBALS["user_segment"]; ?>">
<script type="text/javascript" src="/templates/js/add_to_cart.js"></script>	