<ul class="card_tabs1">
    <li class="active"><a data-type="reviews" href="">Отзывы</a></li>
    <li><a data-type="delivery" href="">Доставка</a></li>
    <li><a data-type="payment" href="">Оплата</a></li>
    <li><a data-type="discount" href="">Скидки</a></li>
</ul>


<div class="row">
    <div class="col-span-9 w100 tab_items">
        <div class="hidden_desctop show_mobile tabs_type1">
            <ul>
                <li class="one_line active"><a href="#tab1">Описание</a></li>
                <li class="one_line"><a href="#tab2">Отзывы</a></li>
                <li><a href="#tab3">Похожие ароматы</a></li>
            </ul>
        </div>

        <div data-type="reviews" id="tab1" class="tab_item hidden">
            <h2 class="heading_type1 hide_mobile">
                Отзывы о <?= $this->brend['title'] ?> <?= $this->catalog_full['title'] ?>
            </h2>

            <? if ($this->rated != true): ?>
                <div class="mb30 clear">
                    <div class="fl_left hidden_mobile">
                        <span class="pr15">Оцените аромат</span>

                        <div style="top:6px" class="relative stars d_inline-block">
                            <input name="star_bottom" type="radio" value="1" class="star2 required"/>
                            <input name="star_bottom" type="radio" value="2" class="star2"/>
                            <input name="star_bottom" type="radio" value="3" class="star2"/>
                            <input name="star_bottom" type="radio" value="4" class="star2"/>
                            <input name="star_bottom" type="radio" value="5" class="star2"/>
                        </div>
                    </div>
                    <div class="fl_right">
							<script type="text/javascript" src="//yastatic.net/share2/share.js" async="async" charset="utf-8"></script>
							<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter" data-counter></div>
                    </div>
                </div>
            <? endif; ?>


            <div id="hypercomments_widget" style="min-height:250px;background-color: #FFF;padding: 20px;"></div>

            <div id="hypercomments_widget"></div>
            <script type="text/javascript">
                _hcwp = window._hcwp || [];
                _hcwp.push({widget:"Stream", widget_id: 19481, xid: "<?=$this->catalog_full['id']; ?>"});
                (function() {
                    if("HC_LOAD_INIT" in window)return;
                    HC_LOAD_INIT = true;
                    var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
                    var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
                    hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/19481/"+lang+"/widget.js";
                    var s = document.getElementsByTagName("script")[0];
                    s.parentNode.insertBefore(hcc, s.nextSibling);
                })();
            </script>
            <a rel="nofollow" href="http://hypercomments.com" class="hc-link" title="comments widget">comments powered by HyperComments</a>

            <?php if (isset($this->catalog_item_com)) { ?>

                <div id="static_comments">

                    <?php foreach ($this->catalog_item_com as $catalog_item_com) { ?>
                        <a name="comment_<?php echo $catalog_item_com['id']; ?>"></a>
                        <div class="comment">
                            <div class="name"><?php echo $catalog_item_com['user_name']; ?></div>
                            <div class="comment_text">
                                <?php echo $catalog_item_com['user_text']; ?>
                            </div>
                        </div>
                        <div class="comment_border"></div>
                        <div class="comment_end"></div>
                    <?php } ?>

                </div>

            <?php } ?>
        </div>
        <div data-type="delivery" class="tab_item">
            <?=$this->delivery_tab?>
        </div>
        <div data-type="payment" class="tab_item">
            <?=$this->payment_tab?>
        </div>
        <div data-type="discount" class="tab_item">
            <?=$this->discount_tab?>
        </div>

        <!-- <div id="tab3" class="hidden">
            <div class="heading_type1 hide_mobile">
                Схожие по цене ароматы
            </div>
            <div class="items main_page">

                <?php foreach ($this->catalog_item_pre as $key => $catalog_item) {  ?>
                    <? // include __DIR__.'/../partials/catalog_item_mini.php'; ?>
                <? } ?>

            </div>
        </div> -->

    </div>
    <div class="col-span-3 ">
        <script>
            $(function () {
                $('.scroll_aromats').scrollbar();
            })
        </script>
        <?php if (isset($this->catalog_item_f)) { ?>
            <div class="mb20">Женские ароматы:</div>
            <div class="scroll_aromats scrollbar-outer" style="max-height:350px !important">
                <ul class="list-type1">
                    <?php foreach ($this->catalog_item_f as $catalog_item) { ?>
                        <li class="mb5">
                            <?php if (strtolower($catalog_item['url']) == $this->aromat_url) { ?>
                                <?php echo $catalog_item['title']; ?>
                            <?php } else { ?>
                                <a class="td_underline color_blue"
                                   href="/production/<?php echo $this->brend_url; ?>/<?php echo $catalog_item['url']; ?>/">
                                    <? ($catalog_item['is_top']) ? '<b>' : '' ?><?php echo $catalog_item['title']; ?><? ($catalog_item['is_top']) ? '</b>' : '' ?>
                                </a>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
        <?php if (isset($this->catalog_item_m)) { ?>
            <br>
            <br>
            <div class="mb20">Мужские ароматы:</div>
            <div class="scroll_aromats scrollbar-outer" style="max-height:350px !important">
                <ul class="list-type1">
                    <?php foreach ($this->catalog_item_m as $catalog_item) { ?>
                        <li class="mb5">
                            <?php if (strtolower($catalog_item['url']) == $this->aromat_url) { ?>
                                <?php echo $catalog_item['title']; ?>
                            <?php } else { ?>
                                <a class="td_underline color_blue"
                                   href="/production/<?php echo $this->brend_url; ?>/<?php echo $catalog_item['url']; ?>/">
                                    <? ($catalog_item['is_top']) ? '<b>' : '' ?><?php echo $catalog_item['title']; ?><? ($catalog_item['is_top']) ? '</b>' : '' ?>
                                </a>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>
        <div class="h30"></div>

    </div>
</div>