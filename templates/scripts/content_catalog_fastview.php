<div class="listing_title" >
    <div class="listing_title__item"><?=$this->brend['title']?></div>
    <span class="listing_title__item color_purple"><?=$this->catalog_full['title']?></span>
</div>
<table class="aromat_table table_new  table table-bordered table-striped">
    <tbody>
    <?php foreach ($this->catalog_item_order as $key=>$catalog_item_order): ?>
        <tr class="<?=($catalog_item_order['price']==0)?'disabled':''?>" <?=($_COOKIE['outstock'])?'style="display: table-row;"':''?>>
            <td width="270" class="first_col">
                <?=$catalog_item_order['title']; ?><?=$catalog_item_order['type']; ?> <?=$catalog_item_order['volume']?>
            </td>
            <td class="second_col bold" width="190">
                <? if ($catalog_item_order['price']==0):?>
                    Нет в наличии
                <? else:?>
                    <? if ($this->sell):?>
                        <?=round($catalog_item_order['price']+$catalog_item_order['price']*$this->sell);?> руб
                    <? endif;?>
                    <? if($this->user['d_percent']!=0 AND $catalog_item_order['is_block']==0){ ?>
                        <? if (!$this->sell):?>
                            <? echo $catalog_item_order['price'];?>
                            /<br />
                        <? endif;?>
                        <? echo show_users_percent($catalog_item_order['price'], $this->user['d_percent']); ?> руб
                    <? }else{ ?>
                        <? if($catalog_item_order['is_block']==1){ ?>
                            <? echo $catalog_item_order['price_old'];?>
                            /<br />
                            <? echo $catalog_item_order['price'];?> руб
                        <? }else{ ?>
                            <? echo $catalog_item_order['price'];?> руб
                        <? } ?>
                    <? } ?>
                <? endif;?>
            </td>
            <td class="third_col ta_center">
                <? if ($catalog_item_order['price']):?>
                    <a data-id_product="<? echo $catalog_item_order['id'];?>" data-id_catalog="<? echo $catalog_item_order['id_catalog'];?>" href="" class="btn3 add_to_cart"><i class="icon-cart_green show_mobile"></i><span class="hide_mobile">В&nbsp;корзину</span></a>
                <? else:?>
                    <a href="" class="btn4" data-articul="<?=$catalog_item_order['articul']?>"><i class="icon-no_aromat show_mobile"></i><span class="hide_mobile">Сообщить</span></a>
                <? endif;?>
            </td>
        </tr>

    <? endforeach;?>
    </tbody>
</table>
<div class="item__footer">
	<a class="item__quick_buy button" onclick="quick_buy_click(<?php echo $this->catalog_full['id']; ?>);return false;">Купить в 1 клик</a>
</div>
<input id="global_geo_segment" type="hidden" data-geo_segment="<?php echo $GLOBALS["user_segment"]; ?>">
<script type="text/javascript" src="/templates/js/add_to_cart.js"></script>	