<? require_once('partials/filter.php');?>
<? //require_once('partials/filter_html.php');?>

<?php if (isset($this->catalog_items)) { ?>


    <div class="row">
        <div class="col-span-12" >
            <div class="items  ">
                <?php $i=1;foreach ($this->catalog_items as $catalog_item) { ?>
                    <? include 'partials/catalog_item.php';?>
                    <? $i++;} ?>
            </div>
        </div>
        <div class="col-span-3 relative" style="width:27%;">
            <?php if(isset($this->catalog_item_f)){ ?>
                <div class="mb20">Все ароматы <?=$this->brend_title;?> для женщин:</div>
                <ul class="list-type1">
                    <?php foreach ($this->catalog_item_f as $catalog_item) { ?>
                        <?php if($catalog_item['pol']=='F'){ ?>
                            <li class="mb5">
                                <?php if(strtolower($catalog_item['url']) == $this->aromat_url){ ?>
                                    <?php echo $catalog_item['title']; ?>
                                <?php }else{ ?>
                                    <a class="td_underline color_blue" href="<?php echo $catalog_item['url']; ?>">
                                        <?php echo $catalog_item['title']; ?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>
<? } ?>

<?php if (isset($this->catalog_item_m)) { ?>
    <a name="man"></a>
    <div class="listing_title">
        <div class="listing_title__item"><?php echo $this->brend_title; ?></div>
        <a class="listing_title__item disabled active">Мужские ароматы</a>
        <a href="#woman" data-init="scroll_to" class="listing_title__item ">Женские ароматы</a>
    </div>

    <? require_once('partials/filter.php');?>

    <div class="row">
        <div class="col-span-9" style="width:73%">
            <div class="items  ">
                <?php $i=1;foreach ($this->catalog_item_m as $catalog_item) { ?>
                    <? include 'partials/catalog_item.php';?>
                    <? if ($i%3==0) echo '<div class="clear"></div>'; ?>
                    <? $i++;} ?>
            </div>
        </div>
        <div class="col-span-3 relative" style="width:27%;">
            <?php if(isset($this->catalog_item_m)){ ?>
                <div class="mb20">Все ароматы <?=$this->brend_title;?> для мужчин:</div>
                <ul class="list-type1">
                    <?php foreach ($this->catalog_item_m as $catalog_item) { ?>
                        <?php if($catalog_item['pol']=='M'){ ?>
                            <li class="mb5">
                                <?php if(strtolower($catalog_item['url']) == $this->aromat_url){ ?>
                                    <?php echo $catalog_item['title']; ?>
                                <?php }else{ ?>
                                    <a class="td_underline color_blue" href="<?php echo $catalog_item['url']; ?>">
                                        <?php echo $catalog_item['title']; ?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            <?php } ?>
        </div>
    </div>
<? } ?>

<? if ($this->catalog_items instanceof Zend_Paginator): ?>
    <?= $this->paginationControl($this->catalog_items, 'Elastic', 'partials/paginator.php',['get'=>$this->get]); ?>
<? endif; ?>
