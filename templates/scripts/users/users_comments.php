<br />
<div class="bread_crumps">
	<a href="/">Главная</a>&nbsp;/
	<a href="/users/">Личный кабинет</a>&nbsp;/
	Мои комментарии
</div>
<br />
<div class="main_text" >
	<div class="row">
        <div class="col-span-9 content">
            <? if(count($this->catalog_item)!=0){ ?>
				<? foreach ($this->catalog_item as $catalog_item) { ?>
				
					<div class="comment_user_wrap ie6">
					<div class="img_wr">
						<img src="<?php echo $catalog_item['img']; ?>" width="70" alt="" title="" />
				
					</div>
					<div class="desc_wr">
						<a class="title" href="<?php echo $catalog_item['brend_url']; ?>"><?php echo $catalog_item['brend_title']; ?></a>
							<a class="title1" href="<?php echo $catalog_item['url']; ?>"><?php echo $catalog_item['title']; ?></a>
						<div class="text">
							<span class="genre"><?php echo $catalog_item['pol']; ?></span>
							<p><?php echo $catalog_item['comment_text']; ?></p>
				
							<div class="undercomment">
								<span class="date">Добавлено: <?php echo $catalog_item['comment_date']; ?></span>
								<a href="<?php echo $catalog_item['url']; ?>#comment_<?php echo $catalog_item['comment_id']; ?>" class="view">Посмотреть комментарий</a>
								<a href="<?php echo $catalog_item['url']; ?>" class="more">Подбробнее</a>
								<div class="clear"></div>
							</div>
						</div>
				
					</div>
					</div>
					
				<? } ?>
			<? }else{ ?>
				
				Вы не добавили ни одного комментария.
				
			<? } ?>

            <br /><br /><br />
        </div>



        <div class="col-span-3 sidebar">
            <div class=" purple fs22 mb15">Личный кабинет:</div>
            <ul>
                <li class="mb10"><a class="td_underline color_blue" href="/users/discount/">Система скидок</a></li>
                <li class="mb10"><a class="td_underline color_blue" href="/users/history/status/1/">Мои покупки</a></li>
                <li class="mb10"><a class="color_purple " href="/users/comments/">Мои комментарии</a></li>
                <li class="">&nbsp;</li>
                <li><a class="td_underline color_blue" href="/users/options/">Настройки</a></li>
            </ul>
            <br>
            <a href="/users/exit/" class="logout btn4 ta_center fs18" style="display:block;height:46px;line-height:46px">Выход</a>
            <br>
            <br>
            <br>
        </div>

        
    </div>
</div>

