<script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>

<div class="bread_crumps">
    <a href="/">Главная</a>&nbsp;/
    <a href="/users/">Личный кабинет</a>&nbsp;/
    Настройки
</div>

<br/>


<div class="main_text user_wrap"  align="justify">
    <div class="row">
        <div class="col-span-9 content">
            <div class=" fs34 mb35 lite">Настройки аккаунта</div><br>

            

            <div class="flora" id="example">
                <ul class="ui-tabs-nav">                    
                    <li class="active_price first_price"><a href="#fragment-1">Анкета</a>
                    </li>
                    <li class="second_price"><a href="#fragment-2">Смена пароля</a>
                    </li>
                </ul>
            </div>

            <form action="/users/options/" method="post" enctype="multipart/form-data" name="reg_form" id="reg_form"
                  autocomplete="off">

                <div class="cat_wrap first" style="margin-top:0">

                    <label class="lab mb5">Ваш ID:</label>
                    <span><?php echo $this->users_options['id']; ?></span><br/>

                    <label class="lab mb10">E-mail:</label>
                    <span><?php echo $this->users_options['email']; ?></span><br/>

                    <label class="lab" for="fio">Ф.И.О:</label>
                    <input class="inp" type="text" value="<?php echo $this->users_options['fio']; ?>" name="fio"
                           id="fio"/><br/>

                    <label class="lab" for="pol">Пол:</label>
                    <select id="pol" name="pol">
                        <option value="0" selected>- Пол не выбран -</option>
                        <option value="1" <?php if ($this->users_options['pol'] == 'M') { ?> selected <?php } ?> >Он</option>
                        <option value="2" <?php if ($this->users_options['pol'] == 'F') { ?> selected <?php } ?> >Она</option>
                    </select><br/>

                    <label class="lab" for="date">День рождения:</label>
                    <select id="day" name="day">
                        <option value="0">День</option>
                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                            <option <?php if ($this->users_options_birthday[2] == $i) { ?> selected <?php } ?>
                                value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    </select>

                    <select id="month" name="month">
                        <option value="0">Месяц</option>
                        <option value="1" <?php if ($this->users_options_birthday[1] == 1) { ?> selected <?php } ?> >Январь</option>
                        <option value="2" <?php if ($this->users_options_birthday[1] == 2) { ?> selected <?php } ?>>Февраль</option>
                        <option value="3" <?php if ($this->users_options_birthday[1] == 3) { ?> selected <?php } ?>>Март</option>
                        <option value="4" <?php if ($this->users_options_birthday[1] == 4) { ?> selected <?php } ?>>Апрель</option>
                        <option value="5" <?php if ($this->users_options_birthday[1] == 5) { ?> selected <?php } ?>>Май</option>
                        <option value="6" <?php if ($this->users_options_birthday[1] == 6) { ?> selected <?php } ?>>Июнь</option>
                        <option value="7" <?php if ($this->users_options_birthday[1] == 7) { ?> selected <?php } ?>>Июль</option>
                        <option value="8" <?php if ($this->users_options_birthday[1] == 8) { ?> selected <?php } ?>>Август</option>
                        <option value="9" <?php if ($this->users_options_birthday[1] == 9) { ?> selected <?php } ?>>Сентябрь
                        </option>
                        <option value="10" <?php if ($this->users_options_birthday[1] == 10) { ?> selected <?php } ?>>Октябрь
                        </option>
                        <option value="11" <?php if ($this->users_options_birthday[1] == 11) { ?> selected <?php } ?>>Ноябрь
                        </option>
                        <option value="12" <?php if ($this->users_options_birthday[1] == 12) { ?> selected <?php } ?>>Декабрь
                        </option>
                    </select>

                    <select id="year" name="year">
                        <option value="0">Год</option>
                        <?php for ($i = 2005; $i >= 1900; $i--) { ?>
                            <option <?php if ($this->users_options_birthday[0] == $i) { ?> selected <?php } ?>
                                value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    </select><br/>

                    <label class="lab" for="phone">Телефон:</label>
                    <input class="inp" type="text" value="<?php echo $this->users_options['phone']; ?>" name="phone" id="phone"/><br/><br/>
                </div>
                <div class="cat_wrap second">

                    <label class="lab" for="passw">Пароль:</label>
                    <input class="inp" type="password" value="" name="passw" id="passw"/><br/>

                    <label class="lab" for="repass">Пароль повторно:</label>
                    <input class="validate[confirm[passw]] inp" type="password" value="" name="repassw" id="repass"/><br/>
        
                </div>

               
                <button type="submit" class="btn1 header fs16" id="reg3" name="save_options">Сохранить</button>
                
            </form>

            <script>
                $(function () {

                    $('.flora a').click(function() {
                        $('.cat_wrap').hide()
                        var ind = $(this).parent().index()
                        $('.cat_wrap').eq(ind).show()

                        $(this).parents('ul').find('li').removeClass('active_price')
                        $(this).parent().addClass('active_price')
                    })

                    $("#show2form").click(function (e) {
                        e.preventDefault()
                        $(this).parent().slideUp(300)
                        $(this).parent().next().slideDown(300)
                    })

                    //suggetions fio
                    $("input[name=regs_fio]").suggestions({
                        serviceUrl: "https://dadata.ru/api/v1/suggest/fio",
                        selectOnSpace: true,
                        token: "15309822a7c43aacc9f604670c10489fc4b19a0b",
                        /* Вызывается, когда пользователь выбирает одну из подсказок */
                        onSelect: function (suggestion) {
                            console.log(suggestion)
                            suggestion = suggestion;
                            $('input[name=fio_data]').val(JSON.stringify(suggestion));
                        }
                    });

                    console.log(region_name)
                    //index suggest
                    AddressSuggestions.initForm();

                    $('#phone').mask("7(999) 999-99-99")
                })
            </script>


            <br /><br /><br />
        </div>



        <div class="col-span-3 sidebar">
            <div class=" purple fs22 mb15">Личный кабинет:</div>
            <ul>
                <li class="mb10"><a class="td_underline color_blue" href="/users/discount/">Система скидок</a></li>
                <li class="mb10"><a class="td_underline color_blue" href="/users/history/status/1/">Мои покупки</a></li>
                <!-- <li class="mb10"><a class="td_underline color_blue" href="/users/comments/">Мои комментарии</a></li> -->
                <li class="">&nbsp;</li>
                <li><a class="color_purple" href="/users/options/">Настройки</a></li>
            </ul>
            <br>
            <a href="/users/exit/" class="logout btn4 ta_center fs18" style="display:block;height:46px;line-height:46px">Выход</a>
            <br>
            <br>
            <br>
        </div>


    </div>
    

    

</div>






<style>
.cat_wrap.first {
    display: block;
    line-height: 2.5;
}
.cat_wrap.second {
    display: none;
    line-height: 2.5;
}
.flora li {
    float: left;
    margin-bottom: 20px;
    margin-right: 90px;
}
.flora {
    overflow: hidden;
    border-bottom: 1px solid #eee;
    margin-bottom: 25px;
}
.flora a {
    color: #35a1e2;
    text-decoration: none !important;
    border-bottom: 1px dashed;
}
.flora .active_price a {
    color: #ea0080;
    border-bottom-color: transparent;
}

.cat_wrap label {
    width: 30%;
    display: inline-block;
}
.cat_wrap input, .cat_wrap select {
    width: 60%;
    padding: 5px 10px;
    font-size: 15px;
    color: #111;
    margin-top: 10px;
    margin-bottom: 15px;
    border-radius: 0;
    height: 35px;
}
select#pol {
    width: 30%;
}
select#day {
    width: 19%;
}
select#month {
    width: 20%;
}
select#year {
    width: 20%;
}
</style>
