<br />
<div class="bread_crumps">
	<a href="/">Главная</a>&nbsp;/
	<a href="/users/">Личный кабинет</a>&nbsp;/
	Распродажа
</div>
<br />

<h2 style="padding-left:27px;font-weight:normal;padding-bottom:10px;" class="georgia_head">Распродажа недели:</h2>


<div class="cat_wrap first" style="margin-top:0">

	<?php foreach ($this->catalog_item_top as $catalog_item) { ?>
	
		<div class="comment_user_wrap">
			<div class="img_wr">
				<img src="<?php echo $catalog_item['img']; ?>" width="70" alt="" title="" />
			</div>
			<div class="desc_wr">
				
				<a class="title georgia_head" href="<?php echo $catalog_item['url']; ?>"><?php echo $catalog_item['brend_title']; ?>
				
				<span class="title1 georgia_head" ><?php echo $catalog_item['title']; ?></span></a>
				
				<div class="text">
					<span class="genre"><?php echo $catalog_item['pol']; ?></span>
					
					<?php if ( $catalog_item['tag']!='' ){ ?>
					<div class="notes">
						<?php foreach ($catalog_item['tag'] as $catalog_item_tag) { ?>
							<a href="/tag/<?php echo $catalog_item_tag['url']; ?>/"><?php echo $catalog_item_tag['title']; ?></a>,
						<?php } ?>
					</div>
					<?php } ?>
					
					
					<p><?php echo $catalog_item['anot']; ?></p>
					
					<!--<p><s><?php echo $catalog_item['price_s']; ?> руб</s></p>
					<div class="new_price">
						С учетом вашей скидки: <span class="georgia_head"><?php echo show_users_percent($catalog_item['price_s'], $this->user['d_percent']); ?> руб</span>
					</div>-->
					
					<div class="undercomment" style="margin-top:3px;">
		
						<a id="<?php echo $catalog_item['id']; ?>" class="izbr bloknot <? if($catalog_item['is_note']) echo 'active'; ?>" style="cursor:pointer;">Добавить в избранное</a>
						<a href="<?php echo $catalog_item['url']; ?>" class="more">Подбробнее</a>
						<div class="clear"></div>
		
					</div>
				</div>
			</div>
		</div>
		
	<?php } ?>
	
</div>