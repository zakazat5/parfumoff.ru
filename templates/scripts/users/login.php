<div class="row log-in">
    <br>
    <div class="col-xs-5">
        <div class="log-in__title mb30" >Вход</div>

        <form action="/users/login/" method="post" enctype="multipart/form-data" name="reg_form" id="reg_form" >

            <div class="row">
                <label class="col-xs-4 " for="">E-Mail:</label>
                <div class="col-xs-8 ">
                    <input class="inp_reg" type="text" name="log_email" id="login1" />
                </div>
            </div>
            <br>

            <div class="row">
                <label class="col-xs-4 " for="">Пароль:</label>
                <div class="col-xs-8 ">
                    <input class="inp_reg" type="password" name="log_passw"/><br /><br />
                    <div style="color:red;">
                        <?php echo $this->err; ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12  col-xs-offset-4">
                <button type="submit" name="register" class="btn1 relative" style="left:-5px">Войти</button>
                <a href="/users/reset/" class="color_blue dashed relative" style="left:25px">Забыли пароль?</a>
            </div>
        </form>
    </div>
    <div class="col-xs-4 col-xs-offset-1">
    <br>
    <br>
        <a class="btn2" style="padding:0 25px;" href="/users/register/">Регистрация</a>
    </div>
        <div class="clear"></div>
        <br>
        <br>
        <br>
        <br>
</div>