<div class="bread_crumps">
    <a href="/">Главная</a>&nbsp;/
    Регистрация
</div>
<br/>
<div class="row sign-up">
    <div class="col-xs-12">
        <div class="sign-up__title">Регистрация</div>


        <form action="/users/register/" method="post" enctype="multipart/form-data" name="reg_form_new" id="reg_form"
              autocomplete="off" style="padding:0px;">

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="log_fio2">ФИО <span class="color color_purple">*</span>:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<? echo $this->data['fio']; ?>" type="text" name="regs_fio" id="log_fio2">
                </div>
                <? if (isset($this->err_fio)) { ?>
                    <br/><span style="color:red;font-size:11px;"><strong><? echo $this->err_fio; ?></strong></span>
                <? } ?>
            </div>
            <br/>

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="login2">E-mail <span class="color color_purple">*</span>:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<? echo $this->data['email']; ?>" type="text" name="regs_email" id="login2">
                </div>
                <? if (isset($this->err_email)) { ?>
                    <br/><span
                        style="color:red;font-size:11px;">&nbsp;&nbsp;<strong><? echo $this->err_email; ?></strong></span>
                <? } ?>
            </div>
            <br/>

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="phone2">Телефон <span class="color color_purple">*</span>:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input class="phone_input" value="<? echo $this->data['phone']; ?>" type="text" name="regs_phone" id="phone2">
                </div>
                <? if (isset($this->err_phone)) { ?>
                    <br/><span
                        style="color:red;font-size:11px;">&nbsp;&nbsp;<strong><? echo $this->err_phone; ?></strong></span>
                <? } ?>
            </div>
            <br/>

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="region">Регион/область <span
                        class="color color_purple">*</span>:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-region']; ?>" type="text" name="region" id="region">
                </div>
            </div>
            <br/>

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="part">Уточните район <span
                        class="color color_purple">*</span>:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <select name="part" id="select_part">
                        <? foreach ($this->capital_districts as $letter => $capital): ?>
                            <? foreach ($capital as $key => $item): ?>
                                <option  <?=($this->data['address-part']==$key)?'SELECTED':''?> style="display: none" data-location="<?= $letter ?>"
                                        value="<?= $key ?>"><?= $item ?></option>
                            <? endforeach; ?>
                        <? endforeach; ?>
                    </select>
                </div>
            </div>
            <br/>

            <div class="row hide">
                <label class="col-xs-4 col-sm-3 col-md-3" for="other" style="width:120px;float:left;">Другой:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-other']; ?>" type="text" name="other" id="other">
                </div>
            <br/>
            </div>

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="city">Город / район <span
                        class="color color_purple">*</span>:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-city']; ?>" type="text" name="city" id="city">
                </div>
            </div>
            <br/>

            <div class="row hide">
                <label class="col-xs-4 col-sm-3 col-md-3" for="settlement">Населенный пункт <span
                        class="color color_purple">*</span>:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-settlement']; ?>" type="text" name="settlement" id="settlement">
                </div>
            <br/>
            </div>

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="street">Улица <span class="color color_purple">*</span>:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-street']; ?>" type="text" name="street" id="street">
                </div>
            </div>
            <br>

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="house">Дом:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-house']; ?>" type="text" name="house" id="house">
                </div>
            </div>
            <br>
            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="corpus">Корпус:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-corpus']; ?>" type="text" name="corpus" id="corpus">
                </div>
            </div>
            <br>
            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="stroenie">Строение:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-stroenie']; ?>" type="text" name="stroenie" id="stroenie">
                </div>
            </div>    
            <br>
            <div class="row">   
                <label class="col-xs-4 col-sm-3 col-md-3" for="floor">Этаж:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-floor']; ?>" type="text" name="floor" id="floor">
                </div>
            </div>    
            <br>
            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="flat">Номер квартиры:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-flat']; ?>" type="text" name="flat" id="flat">
                </div>
            </div>    
            <br>
            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="domophone">Номер домофона:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['address-domophone']; ?>" type="text" name="domophone" id="domophone">
                </div>
            </div>
            <br>

            <div class="row">
                <label class="col-xs-4 col-sm-3 col-md-3" for="regs_postcode">Индекс:</label>
                <div class="col-xs-8 col-sm-5 col-md-4">
                    <input value="<?=$this->data['postcode']; ?>" type="text" name="regs_postcode" id="regs_postcode">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-12 col-sm-5 col-sm-offset-3 col-md-4 col-md-offset-3">
                    *поля, обязательные для заполнения
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-xs-4">
                    <div>
                        <label class="label_check c_on">
                            <input name="regs_p" value="1" type="checkbox" <? if ($this->data['p'] == 1) echo 'checked'; ?> />Пароль сгенерировать автоматически
                        </label>
                    </div>
                    <br>
                    <div>
                        <label id="toggle_pass" class="label_check<?php if ($this->data['p'] == 2) echo ' c_on';?>">
                            <input name="regs_p" value="2" type="checkbox" <? if ($this->data['p'] == 2) echo 'checked'; ?> />Задать пароль
                        </label>
                    </div>
                </div>
                <div class="col-xs-8 text-left">
                    <br/>
                    <br/>
                    <button class="btn1" type="submit" id="reg1" name="register_submit">Зарегистрироваться</button>
                </div>
            </div>
            <br/>

            <div style="<? if ($this->data['p'] == 1 OR !isset($this->data['p'])) echo 'display:none;'; ?>" id="pass-wrap">
                <div class="row">
                    <label class="col-xs-4 col-sm-3 col-md-3" for="pass4">Пароль *:</label>
                    <div class="col-xs-8 col-sm-5 col-md-4">
                        <input value="" type="password" name="regs_passw" id="pass4">
                    </div>
                </div>
                <br>

                <div class="row">
                    <label class="col-xs-4 col-sm-3 col-md-3" for="pass5">Повтор пароля *:</label>
                    <div class="col-xs-8 col-sm-5 col-md-4">
                        <input value="" type="password" name="regs_repass" id="pass5">
                    </div>
                </div>
                <? if (isset($this->err_passw)) { ?>
                    <br/><span
                        style="color:red;font-size:11px;">&nbsp;&nbsp;<strong><? echo $this->err_passw; ?></strong></span>
                <? } ?>
                <br/><br/>
            </div>
            <br/>
            <input hidden name="fio_data" value="<? echo htmlentities($this->data['fio_data'], ENT_COMPAT, 'cp1251'); ?>">
            <input hidden name="address_data"
                   value="<? echo htmlentities($this->data['address_data'], ENT_COMPAT, 'cp1251'); ?>">
            <br/><br/><br/><br/>
        </form>

        <script>
            $(function () {
                //suggetions fio
                $("input[name=regs_fio]").suggestions({
                    serviceUrl: "https://dadata.ru/api/v1/suggest/fio",
                    selectOnSpace: true,
                    token: "15309822a7c43aacc9f604670c10489fc4b19a0b",
                    /* Вызывается, когда пользователь выбирает одну из подсказок */
                    onSelect: function (suggestion) {
                        $('input[name=fio_data]').val(JSON.stringify(suggestion));
                    }
                });

                //index suggest
                AddressSuggestions.initForm();

                $('#toggle_pass').on('click', function(e) {
                    e.preventDefault();

                    var $this = $(this),
                        $pw = $('#pass-wrap');

                    if ($this.hasClass('c_on')) {
                        $pw.show(0);
                    } else {
                        $pw.hide(0);
                    }
                });
            })
        </script>

    </div>
</div>