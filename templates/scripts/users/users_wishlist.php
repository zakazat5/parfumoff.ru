<br />
<div class="bread_crumps">
	<a href="/">Главная</a>&nbsp;/
	<a href="/users/">Личный кабинет</a>&nbsp;/
	Мой список желаний
</div>
<br />
<div class="main_text" style="font-size:11px;padding-left:27px;padding-right:20px;line-height:16px" align="justify">
	<h1>Я хочу:</h1> 
	<form style="padding-left:0" id="reg_form" action="">
		<label class="lab" for="nazv">Название товара:</label>
		<input class="inp1" type="text" name="nazv" id="nazv" /><br /><br />

		<label class="lab" for="kart">Картинка:</label>
		<input type="file" name="kart" id="kart" />
		<input type="checkbox" name="del" id="del" />
		<label for="del">Удалить</label>
		<br /><br />
		<label class="lab" for="link">Ссылка на товар:</label>
		<input class="inp1" type="text" name="link" id="link" /><br /><br />

		<label class="lab" for="price">Цена:</label>
		<input class="inp1" type="text" name="price" id="price" /><br /><br />
		
		<input type="checkbox" id="inf" name="inf" />
		<label for="inf" class="inf">Информируйте меня, когда товар появится</label>
		<button id="reg2">Готово</button><br /><br />
	</form>
</div>

<br />
<div class="main_text" style="font-size:11px;padding-left:27px;padding-right:20px;line-height:16px" align="justify">
	<h1>Мой список желаний</h1> 
	
</div>

<br />
<div class="flora" id="example">
	<ul class="ui-tabs-nav">
	<li class="first_">&nbsp;</li>
	<li class="active_price first_price"><a href="#fragment-1">Неосуществленные</a><div></div></li>

	<li class="sep"></li>
	<li class="second_price"><a href="#fragment-2">Осуществленные</a><div></div></li>
	<li class="sep"></li>
	<li class="third_price"><a href="#fragment-3">В процессе</a><div></div></li>
	<li class="sep" style="width:193px"></li>
	
	</ul>
</div>

<div class="cat_wrap first" style="margin-top:0">	
	<div class="comment_user_wrap" style="_padding-bottom:15px">

		<div class="img_wr">
			<img src="/images/uploads/catalog/967/small/1254761408.jpg" width="70" alt="" title="" />
		</div>
		<div class="desc_wr">
			<span class="title1">Аромат Eclat D'Arpege</span>
			<div class="text">
				<span class="genre">ссылка на товар: <a href="">Parfumoff.ru</a></span>

				<span class="genre1">Цена: 1750 руб</span>

				<div class="undercomment" style="margin-top:3px;">
					<span class="date">Добавлено: сегодня в 13:07</span>
					<a href="" class="done">Исполнено!</a>
					<a href="" class="del">Удалить</a>
					<div class="clear"></div>

				</div>
			</div>
		</div>
	</div>	
	
	<div class="comment_user_wrap" style="_padding-bottom:15px">
		<div class="img_wr">
			<img src="/images/uploads/catalog/967/small/1254761408.jpg" width="70" alt="" title="" />
		</div>
		<div class="desc_wr">

			<span class="title1">Аромат Eclat D'Arpege</span>
			<div class="text">
				<span class="genre">ссылка на товар: <a href="">Parfumoff.ru</a></span>
				<span class="genre1">Цена: 1750 руб</span>

				<div class="undercomment" style="margin-top:3px;">
					<span class="date">Добавлено: сегодня в 13:07</span>

					<a href="" class="done">Исполнено!</a>
					<a href="" class="del">Удалить</a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>	
	
</div>

<div class="cat_wrap second">
	
</div>
<div class="cat_wrap third">
	
</div>
<div class="cat_wrap fourth">
	
</div>
