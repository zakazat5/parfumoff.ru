<br />
<div class="bread_crumps">
	<a href="/">Главная</a>&nbsp;/
	<a href="/users/">Личный кабинет</a>&nbsp;/
	История покупок
</div>
<br />

<div class="main_text user_wrap"  align="justify">
    <div class="row">
        <div class="col-span-9 content">
            <div class=" fs34 mb35 lite">История покупок</div><br>


			<br />

			<div id="example" class="flora history">
				<ul class="ui-tabs-nav">
				
				<?php if($this->status==1){ ?>
				<li class="active_price first_price">
					<a>Не подтвержденные</a>
				<div></div></li>
				<?php }else{ ?>
				<li class="first_price">
					<a href="/users/history/status/1/">Не подтвержденные</a>
				</li>
				<?php } ?>
				
				<?php if($this->status==2){ ?>
				<li class="active_price second_price">
					<a>Подтвержденные</a>
				</li>
				<?php }else{ ?>
				<li class="second_price">
					<a href="/users/history/status/2/">Подтвержденные</a>
				</li>
				<?php } ?>

					<?php if($this->status==4){ ?>
						<li class="active_price second_price">
							<a>В обработке</a>
						</li>
					<?php }else{ ?>
						<li class="second_price">
							<a href="/users/history/status/4/">В обработке</a>
						</li>
					<?php } ?>
				
				<?php if($this->status==3){ ?>
				<li class="active_price third_price">
					<a >Доставленные</a>
				</li>
				<?php }else{ ?>
				<li class="third_price">
					<a href="/users/history/status/3/">Доставленные</a>
				</li>
				<?php } ?>
				
				</ul>
			</div>


			<div class="cat_wrap first" style="margin-top:0">

				<?php foreach ($this->orders as $orders) { ?>

					<div class="comment_user_wrap">
						<div class="img_wr">
						</div>
						<div class="desc_wr">
							<div class="desc_top">
								<strong class="title">Номер заказа <span class="color_purple">[<?php echo $orders['id']; ?>]</span></strong>
								<div class="ta_center">Дата: 14.01.1970</div>
								<div class="fl_right toggle_desc_wrap"><a href="" class="toggle_desc">Скрыть подробности</a></div>
							</div>
							<div class="tovar_table">
								<table border="0" cellspacing="0" cellpadding="0" id="cart_wrap" width="680">
									<tr >
										<th  align="left" class="head_cart" >Название товара</th>
										<th  width="95" align="left" class="head_cart">Кол-во</th>
										<th  width="95" align="left" class="head_cart">Цена, <span class="">руб</span></th>
										<th  width="130" align="left" class="head_cart">Стоимость, <span class="">руб</span></th>
									</tr>
								
									<?php foreach ($orders['order'] as $order) { ?>
									<tr class="cart_td">
										
										<td style="padding:10px" class="first_td" valign="top"><img width="70" height="70" align="left" src="/images/uploads/catalog/<?php echo $order['id_catalog_data']; ?>/small/<?php echo $order['img']; ?>" alt="" class="cart_img"/>
										<?php echo substr($order['title'],5); ?></td>
										
										<td style="padding:10px" valign="top" align="left"><div class="show_help" style="display:none">Количество: </div>
											<span class="value"><?php echo $order['kol']; ?></span>
										</td> 
										
										<td style="padding:10px" valign="top" align="left">
											<div class="show_help" style="display:none">Цена: </div>
											
											<span class="value"><?php echo parsePrice($order['sum']); ?></span>
										</td>
										
										<td style="padding:10px" valign="top" align="left"><div class="show_help" style="display:none">Сумма: </div>
										<span class="value"><?php echo parsePrice($order['sum']*$order['kol']); ?></span>
										</td>
									</tr>
									<?php } ?>
									
									<tr class="total">
										<td colspan="5" align="left" height="70" style="padding-left:0px;border:0px;">
										
											<table width="100%" border="0" cellspacing="10" cellpadding="0" style="font:14px Arial;">
											<tr>
											<td>
												<div class="date_history" style="padding-top:10px;float:left;margin-bottom:10px"><span style="color:#828282;">
													Закано: <?php echo $orders['DateAdd']; ?></span>
												</div>
												<div style="padding-top:10px;margin-bottom:10px;float:right;">Сумма заказа:</div></td>
											<td class="total_right_td" align="right" width="109">
												<strong class="color_purple fs18" style="font-weight:400"><?php echo parsePrice($orders['Sum']); ?> руб</strong> 
											</td>
											</tr>
											
											<tr>
											<td align="right" >
												<div class="your_economy" style="float:left">
												<span style="color:#828282;">
												Вы сэкономили: (<?php echo parsePrice($orders['Sum']-$orders['dSum']); ?> руб)
												</span></div>
												<div style="float:right;position:relative;margin-bottom:10px;"><? if($orders['dType']==1) echo 'Накопительная'; else echo 'Разовая'; ?> 
												скидка: </td>
											<td align="right">
												<div class="mb10"><strong class="color_purple fs18 " style="font-weight:400"><?php echo $orders['dPercent']; ?>%</strong></div>
											</td>
											</tr>
											
											
											
											<tr>
											<td  align="right">
												<span class="total" style="font-size:18px;font-weight:400">Общая сумма</span>
												<span style="font-size:11px;position:relative;top:-1px">(с учетом  скидки)</span>
											</td>
											<td style="font-size:18px" align="right">
												<span style="font-size:18px"><strong class="color_purple fs18" style="font-weight:400"><?php echo parsePrice($orders['dSum']); ?> руб</strong> 
											</td>
											</tr>
											</table>
											
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				
				<?php } ?>
				
				<br><br>
			</div>

		</div>



        <div class="col-span-3 sidebar">
            <div class=" purple fs22 mb15">Личный кабинет:</div>
            <ul>
                <li class="mb10"><a class="td_underline color_blue" href="/users/discount/">Система скидок</a></li>
                <li class="mb10"><a class="color_purple " href="/users/history/status/1/">Мои покупки</a></li>
                <!-- <li class="mb10"><a class="td_underline color_blue" href="/users/comments/">Мои комментарии</a></li> -->
                <li class="">&nbsp;</li>
                <li><a class="color_blue td_underline" href="/users/options/">Настройки</a></li>
            </ul>
            <br>
            <a href="/users/exit/" class="logout btn4 ta_center fs18" style="display:block;height:46px;line-height:46px">Выход</a>
            <br>
            <br>
            <br>
        </div>


    </div>
    

    

</div>












<style>
.cat_wrap.first {
    display: block;
    line-height: 2.5;
}
.cat_wrap.second {
    display: none;
    line-height: 2.5;
}
.flora li {
    float: left;
    margin-bottom: 20px;
    margin-right: 30px;
}
.flora {
    overflow: hidden;
    border-bottom: 1px solid #eee;
    margin-bottom: 25px;
}
.flora a {
    color: #35a1e2;
    text-decoration: none !important;
    border-bottom: 1px dashed;
}
.flora .active_price a {
    color: #ea0080;
    border-bottom-color: transparent;
}
.comment_user_wrap {
	background: #f5f5f5;
	border: 1px solid #eaeaea;
	padding: 15px 30px;
	margin-bottom: 30px;
}
.comment_user_wrap .title {
	font-size: 22px;
	font-weight: normal;
	float: left;
}
.comment_user_wrap .desc_top .ta_center {
	position: absolute;
	width: 200px;
	left: 50%;
	margin-left: -100px;
	top: 0;
	height: 55px;
	line-height: 55px;
}
.comment_user_wrap .desc_top .fl_right {
	height: 55px;
	line-height: 55px;
}
.comment_user_wrap .desc_top .fl_right a {
	border-bottom: 1px dashed
}
.comment_user_wrap .desc_top .fl_right a:hover {
	border-color: transparent;
	text-decoration: none;
}
.comment_user_wrap .desc_top {
	position: relative;
	overflow: hidden;
	padding-bottom: 10px;	
}
.comment_user_wrap .tovar_table {
	margin: 0 -30px;
	border-top: 1px solid #eaeaea;
	padding: 20px 30px 0;
}
#cart_wrap {
	position: relative;
	width: 100%;
}
#cart_wrap th {
	font-weight: 400;
	color: #989898
}
#cart_wrap .cart_td td {
	background: white;
	border: 1px solid #eaeaea;
	line-height: 1.1;
	padding: 5px 10px;
	vertical-align: middle;
}
#cart_wrap .cart_td td img {
	margin-right: 20px;
}
</style>

<script>
$(function() {
	$('.toggle_desc').click(function(e) {
		e.preventDefault()

		var 
			elem = $(this),
			text = elem.text(),
			wrap = elem.parents('.desc_top').next()

		if (text == 'Скрыть подробности') {
			wrap.slideUp(200)
			elem.text('Показать подробности')
		} else {
			wrap.slideDown(200)
			elem.text('Скрыть подробности')
		}


	})
})
</script>