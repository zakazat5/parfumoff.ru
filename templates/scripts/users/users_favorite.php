<br />
<div class="bread_crumps">
	<a href="/">Главная</a>&nbsp;/
	<a href="/users/">Личный кабинет</a>&nbsp;/
	Избранное
</div>
<br />
<div class="main_text" style="font-size:11px;padding-left:27px;padding-right:20px;line-height:16px" align="justify">
	<h1>Товары, добавленные в избранное</h1> 
	
</div>

<br />


<div class="cat_wrap first" style="margin-top:0">

	

	<? if(count($this->catalog_item)!=0){ ?>
		<? foreach ($this->catalog_item as $catalog_item) { ?>
		
			<div class="comment_user_wrap note-<?php echo $catalog_item['id']; ?>" style="_padding-bottom:15px">
				<div class="img_wr">
					<img src="<?php echo $catalog_item['img']; ?>" width="70" alt="" title="" />
				</div>
				<div class="desc_wr">
					
					<a class="title" href="<?php echo $catalog_item['brend_url']; ?>"><?php echo $catalog_item['brend_title']; ?></a>
					
					<a class="title1" href="<?php echo $catalog_item['url']; ?>"><?php echo $catalog_item['title']; ?></a>
					
					<div class="text">
						<span class="genre"><?php echo $catalog_item['pol']; ?></span>
						
						<?php if ( $catalog_item['tag']!='' ){ ?>
						<div class="notes">
							<?php foreach ($catalog_item['tag'] as $catalog_item_tag) { ?>
								<a href="/tag/<?php echo $catalog_item_tag['url']; ?>/"><?php echo $catalog_item_tag['title']; ?></a>,
							<?php } ?>
						</div>
						<?php } ?>
						
						<p><?php echo $catalog_item['anot']; ?></p>
						
						<p><s><?php echo $catalog_item['price_s']; ?> руб</s></p>
						
						<div class="new_price">
							С учетом вашей скидки: <span><?php echo show_users_percent($catalog_item['price_s'], $this->user['d_percent']); ?> руб</span>
						</div>
						
						<div class="undercomment" style="margin-top:3px;">
							<a href="<?php echo $catalog_item['url']; ?>" class="korzi">В корзину</a>
							<a id="<?php echo $catalog_item['id']; ?>" class="del bloknot_del" style="cursor:pointer">Удалить</a>
							<a href="<?php echo $catalog_item['url']; ?>" class="more">Подбробнее</a>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
			
		<? } ?>
	<? }else{ ?>
		<div style="padding-left:30px">
		Избранное пусто.
		</div>
	<? } ?>

	
	
	
	
</div>
