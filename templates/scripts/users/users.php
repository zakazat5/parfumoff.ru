<br />
<div class="bread_crumps">
	<a href="/">Главная</a>&nbsp;/
	Личный кабинет
</div>
<br />

<div class="main_text user_wrap"  align="justify">
    <div class="row">
        <div class="col-span-9 content">
            <div class=" fs34 mb35 lite">Личный кабинет покупателя</div><br>

            <div class=" fs22 mb15">Здравствуйте, <span class="color_purple"><?php echo $this->user['fio']; ?>!</span></div>
            <span class="color_gray1">Ваш ID номер:</span> <?php echo $this->user['id']; ?><br />
            <p class="mb15"><span class="color_gray1">Ваша накопительная скидка:</span> <span class="color_purple"><?php echo $this->user['d_percent'] ?>%</span>
            <p>Спасибо Вам за то, что пользуетесь услугами нашего магазина.</p>
            </p>



            <br /><br />
            <div class=" purple fs22 mb15">Накопительные скидки</div>
            Для зарегистрированных покупателей в магазине Parfumoff.ru <br> предусмотрена система накопительных скидок. <br />
            <div class="h10"></div>
            <span class="color_purple">3%</span> - при накоплении суммы покупок 3000 руб<br />
            <span class="color_purple">5%</span> - при накоплении суммы покупок 9500 руб<br />
            <span class="color_purple">7%</span> - при накоплении суммы покупок 21000 руб<br />
            <span class="color_purple">10%</span> - при накоплении суммы покупок 65000 руб<br />
            <span class="color_purple">15%</span> - при накоплении суммы покупок 150000 руб<br /><br />
            <p>Чтобы получать накопительные скидки Вам необходимо <br> зарегистрироваться на сайте Parfumoff.ru</p>
            <br /><br /><br />
        </div>



        <div class="col-span-3 sidebar">
            <div class=" purple fs22 mb15">Личный кабинет:</div>
            <ul>
                <li class="mb10"><a class="td_underline color_blue" href="/users/discount/">Система скидок</a></li>
                <li class="mb10"><a class="td_underline color_blue" href="/users/history/status/1/">Мои покупки</a></li>
                <!-- <li class="mb10"><a class="td_underline color_blue" href="/users/comments/">Мои комментарии</a></li> -->
                <li class="">&nbsp;</li>
                <li><a class="td_underline color_blue" href="/users/options/">Настройки</a></li>
            </ul>
            <br>
            <a href="/users/exit/" class="logout btn4 ta_center fs18" style="display:block;height:46px;line-height:46px">Выход</a>
            <br>
            <br>
            <br>
        </div>


    </div>
    

	

</div>

