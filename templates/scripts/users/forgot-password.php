<div class="bread_crumps">
<a href="/">Главная</a>&nbsp;/
Запрос пароля
</div>
<br />
<div class="main_text" style="font-size:11px;padding-left:27px;padding-right:20px;line-height:16px" align="justify">
	<h1 style="font-weight:normal;font-style:italic;font-family:Georgia;font-size: 20px;">Запрос пароля</h1> 
</div>

<div style="font-size:14px;padding-left:27px;">Если Вы забыли пароль, введите Ваш E-Mail. Новый пароль будет выслан Вам по E-Mail.

<? if(isset($this->err)){ ?>
	<br />
	<span style="color:red"><? echo $this->err; ?></span>
<? } ?>

</div>
<br />



<form class="mb20" style="padding-left:27px" action="/users/reset/" method="post" enctype="multipart/form-data" name="forgot_form">
	
	<label for="forgot_email">E-Mail:</label>&nbsp;
	<input style="width:200px;" type="text" name="forgot_email" id="forgot_email" /><br /><br />
	
	<input class="btn1" style="width:auto;" type="submit" name="send_passw" value="Выслать">
</form>