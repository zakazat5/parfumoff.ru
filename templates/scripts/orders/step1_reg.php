<form autocomplete="off" action="/korzina/" method="post" enctype="multipart/form-data" name="orders_list" id="orders">
    <div id="cart_step1" style="display: none">
        <table cellspacing="0" cellpadding="0" id="cart_wrap" width="695" style="width:695px">
            <tbody>
            <tr>
                <th align="left" style="width:500px" class="head_cart">Название товара</th>
                <th width="115" align="left" class="head_cart">Цена, руб.</th>
                <th width="85" align="left" class="head_cart">Кол-во</th>
                <th align="left" width="150" class="head_cart">Стоимость, руб.</th>
            </tr>

            <?php if ($this->cart->getCount() != 0) { ?>
                <?php foreach ($this->cart->getItems() as $item_order) { ?>

                    <tr class="tr_<?php echo $item_order['uid']; ?> cart_td cart_<?php echo $item_order['bg']; ?>">

                        <td width="240" class="first_td">
                            <img width="70" height="70" align="left" src="<?php echo $item_order['img_s']; ?>" alt=""
                                 class="cart_img"/>
                            <span style="font-size:14px"><?php echo $item_order['title']; ?></span>

                            <?php if ($item_order['is_block'] == 1) { ?>
                                <span style="font-size:11px;;display:block" class="purple">
								На данный товар скидки не распространяются!
							</span>
                            <?php } ?>

                        </td>

                        <td>
						<span class="price">

							<?php if ($item_order['is_block'] == 1) { ?>
                                <img width="24" height="24" src="/images/default/sale_icon.png" border="0" align="left">
                            <?php } ?>
                            <span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>

                            <? if ($this->sell): ?>
                                <s>
                                    <?= parsePrice(round(($item_order['price']) + (($item_order['price']) * $this->sell))) ?>
                                </s> &nbsp;&nbsp;
                            <? endif; ?>

                            <?php echo parsePrice($item_order['price']); ?>

						</span>
                            <!--<span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>-->
                            </span>
                        </td>

                        <td>
                            <div class="korz_kol-vo"><span class="val"><?php echo $item_order['kol']; ?></span>
                                <a data-action="asc" data-uid="<?= $item_order['uid']; ?>"
                                   href="/orders/AscOrder/<?= $item_order['uid']; ?>/<?php echo $item_order['price']; ?>/"
                                   class="plus"></a>
                                <a data-action="desc" data-uid="<?= $item_order['uid']; ?>"
                                   href="/orders/DescOrder/<?= $item_order['uid']; ?>/<?php echo $item_order['price']; ?>/"
                                   class="minus"></a>
                            </div>
                            <span style="float:right;padding-right:10px;font-size:11px;">=</span>
                            <input type="hidden" name="kol_<?php echo $item_order['uid']; ?>" maxlength="2"
                                   value="<?php echo $item_order['kol']; ?>" id="kol_<?php echo $item_order['uid']; ?>"
                                   class="input_cart" style="font-size:11px;"/>
                        </td>

                        <td>
                            <span class="sum"><?php echo parsePrice($item_order['sum']); ?></span> <br/>
                            <a style="text-decoration:none;color:#828282;border-bottom:1px dotted"
                               href="/orders/deleteOrder/<?php echo $item_order['uid']; ?>/<?php echo $item_order['sum']; ?>/<?php echo $item_order['kol']; ?>/">Удалить</a>
                        </td>
                    </tr>

                <?php } ?>
            <?php } ?>

            </tbody>
        </table>
    </div>
</form>