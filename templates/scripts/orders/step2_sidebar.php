

<div class="col-xs-12 col-md-4 b-order">
    <div class="b-order__wrap">
        <div class="b-order__title">Ваш заказ № <?=$this->order['id']?></div>
        <? if (strpos($_COOKIE['utm_source_last'],'kupikupon')!==false ):?>
            <img src="http://www.kupikupon.ru/affiliate_tracks?source=parfumoff&utm_medium=<?=$this->order['id']?>&comission=200" width="1" height="1" alt="" />
        <? endif;?>
        <table class="b-order__table">
            <tbody>
            <?php if (count($this->order_items) != 0){ ?>
                <?php foreach ($this->order_items as $item_order) {  ?>
                    <tr colspan="3" class="cart_td cart_<?php echo $item_order['bg']; ?>">
                        <td colspan="3" class="first_td pr10" style="padding-left:14px;">
                            <span style="font-size:14px"><?php echo $item_order['title']; ?></span>
                            <?php if($item_order['is_sale']==1){ ?>
                                <span style="font-size:11px;;display:block" class="purple">
                                    На данный товар скидки не распространяются!
                                </span>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left:14px;">
                            <div class="color_gray1">Цена</div>
                                <span class="price">
                                    <?php if($item_order['is_sale']==1){ ?>
                                        <img width="24" height="24" src="/images/default/sale_icon.png" border="0" align="left">
                                    <?php } ?>
                                    <? if ($this->sell):?>
                                        <s>
                                            <?=parsePrice(round(($item_order['sum'])+(($item_order['sum'])*$this->sell)))?>
                                        </s> &nbsp;&nbsp;
                                    <? endif;?>
                                    <?php echo parsePrice($item_order['sum']); ?>
                                </span> руб.
                            </span>
                        </td>

                        <td>
                            <div class="color_gray1">Кол-во</div>

                            <div class="korz_kol-vo"><span class="val"><?php echo $item_order['kol'];?></span></div>

                            <? /*
                            <div class="korz_kol-vo"><span class="val"><?php echo $item_order['kol'];?></span>
                                <a data-action="asc" data-uid="<?=$item_order['uid'];?>" href="/orders/AscOrder/<?=$item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="plus"></a>
                                <a data-action="desc" data-uid="<?=$item_order['uid'];?>" href="/orders/DescOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="minus"></a>
                            </div>

                            <input type="hidden" name="kol_<?php echo $item_order['uid']; ?>" maxlength="2" value="<?php echo $item_order['kol']; ?>" id="kol_<?php echo $item_order['uid']; ?>" class="input_cart" style="font-size:11px;" />
                            */?>
                        </td>

                        <td>
                            <div class="color_gray1">Стоимость</div>
                            <span class="sum"><?php echo parsePrice($item_order['sum']*$item_order['kol']); ?></span> руб.
                        </td>
                    </tr>

                <?php } ?>
            <?php } ?>

            </tbody>
        </table>
        <? if ($more = (count($this->cart->getItems(2))>0)):?>
            <div class=" more_tovars">
                <a href="">Еще товаров: <?=$more?> </a>
            </div>
        <? endif;?>
        <table cellspacing="0" cellpadding="0" width="100%" style="display:none;width:100% !important">
            <tbody>
            <?php if ($this->cart->getCount()!= ''){ ?>
                <?php foreach ($this->cart->getItems(2) as $item_order) { ?>

                    <tr  class="cart_td cart_<?php echo $item_order['bg']; ?>">

                        <td class="first_td">
                            <span style="font-size:14px"><?php echo $item_order['title']; ?></span>

                            <?php if($item_order['is_block']==1){ ?>
                                <span style="font-size:11px;;display:block" class="purple">
                                                На данный товар скидки не распространяются!
                                            </span>
                            <?php } ?>

                        </td>
                    </tr>
                    <tr class="tr_<?php echo $item_order['uid']; ?>">
                        <td class="fl_left">
                            <div class="color_gray1">Цена, руб</div>
                                        <span class="price">

                                            <?php if($item_order['is_block']==1){ ?>
                                                <img width="24" height="24" src="/images/default/sale_icon.png" border="0" align="left">
                                            <?php } ?>
                                            <? /*
                                            <?
                                                $iNewPrice = 0.1*$item_order['price'] + $item_order['price'];
                                                $iNewPrice = round($iNewPrice, 0);
                                            ?>
                                            <s><?php echo $iNewPrice; ?> руб</s>
                                            */ ?>
                                            <span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>

                                            <? if ($this->sell):?>
                                                <s>
                                                    <?=parsePrice(round(($item_order['price'])+(($item_order['price'])*$this->sell)))?>
                                                </s> &nbsp;&nbsp;
                                            <? endif;?>

                                            <?php echo parsePrice($item_order['price']); ?>

                                        </span>
                            <!--<span style="float:right;padding-right:10px;font-size:11px;_margin-top:-18px">x</span>-->
                            </span>
                        </td>

                        <td class="fl_left">
                            <div class="color_gray1">Кол-во</div>
                            <div class="korz_kol-vo"><span class="val"><?php echo $item_order['kol'];?></span>
                                <a data-action="asc" data-uid="<?=$item_order['uid'];?>" href="/orders/AscOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="plus"></a>
                                <a data-action="desc" data-uid="<?=$item_order['uid'];?>" href="/orders/DescOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['price'];?>/" class="minus"></a>
                            </div>
                            <span style="float:right;padding-right:10px;font-size:11px;">=</span>
                            <input type="hidden" name="kol_<?php echo $item_order['uid']; ?>" maxlength="2" value="<?php echo $item_order['kol']; ?>" id="kol_<?php echo $item_order['uid']; ?>" class="input_cart" style="font-size:11px;" />
                        </td>

                        <td class="fl_left">
                            <div class="color_gray1">Стоимость</div>
                            <span class="sum"><?php echo parsePrice($item_order['sum']); ?></span> <br />
                            <a style="text-decoration:none;color:#828282;border-bottom:1px dotted" href="/orders/deleteOrder/<?php echo $item_order['uid'];?>/<?php echo $item_order['sum'];?>/<?php echo $item_order['kol'];?>/">Удалить</a>
                        </td>
                    </tr>

                <?php } ?>
            <?php } ?>

            </tbody>
        </table>

        <!-- <div class="line" style="margin-bottom:10px;">
            <textarea placeholder="Комментарий к заказу" name="pComment" id="" style="font-family:Tahoma;height:34px;padding:4px;width:95%;height: 80px;"></textarea>
        </div> -->


        <div class="b-order__total">
            <div class="col-xs-7">Сумма заказа:</div>
            <div class="col-xs-5"><span class="val_subTotal" id="big_sum"><?=$this->order['Sum']?></span> руб</div>

            <div class="col-xs-7">Накопительная скидка:</div>
            <div class="col-xs-5">
                <div class="nakop_sk" style="left:140px;"></div>
                <div class="nakop_id" style="right:auto;left:-185px;height: 150px; width: 430px; padding: 10px 20px; display: none;">
                     <span style="font-size:18px;display:block;padding-bottom:16px" class="georgia_head">Накопительные скидки в магазине Parfumoff.ru:</span>
                     3% от стоимости заказа при покупке на сумму от 3000 руб до 10 000 руб<br>
                     5%  от стоимости заказа при покупке на сумму от 10 001 руб до 20 000 руб<br>
                     7%  от стоимости заказа при покупке на сумму от 20 001 руб до 70 000 руб<br>
                     10% от стоимости заказа при покупке на сумму от 70 001 руб до 139 999 руб<br>
                     15% от стоимости заказа при покупке на сумму свыше 140 000 руб</div>

                <?=$this->cart->getDiscountData('percent_d')?>%
            </div>

            <div class="col-xs-7">Доставка:</div>
            <div class="col-xs-5">
                <span id="delivery_cost-text">0</span> <span>руб</span>
            </div>

            <input type="hidden" name="courier_address_id" id="courier_address_id" value="0"/>
            <input type="hidden" name="delivery_cost" id="delivery_cost" value="0"/>

            <div class="itogo color_purple">
                <div class="col-xs-7">Общая сумма:</div>
                <div class="col-xs-5">
                    <b><span class="val_total" id="big_sum_all"><?=($this->order['dSum']); ?></span></b> руб
                    <input type="hidden" name="dSum" id="dSum" value="<?=($this->order['dSum']); ?>"/>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="btn-wrap">
                <button type="submit" name="complete" class="btn1 cart_btn1" disabled>Отправить заказ</button>
            </div>
        </div>
    </div>
</div>
