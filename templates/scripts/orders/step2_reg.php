








<script>
    $(function(){
        //index suggest
        AddressSuggestions.initForm();
    })
</script>
<?
/** @var array $user */
$user = $cart->getUser();
?>
<div id="cart_step2" class="b-delivery container">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <div class="b-delivery__title">В какой регион и населенный пункт будем доставлять заказ?</div>
                <div class="row">
                    <label class="col-xs-4 col-sm-3 col-md-3" for="">Регион</label>
                    <div class="col-xs-8 col-sm-5 col-md-5">
                        <input value="<?php echo $user['address_region']; ?>" onclick="this.value=''" type="text" name="region">
                    </div>
                    <div class="clearfix"></div>
                    <label class="col-xs-4 col-sm-3 col-md-3 part" for="">Уточните район</label>
                    <div class="col-xs-8 col-sm-5 col-md-5 part">
                        <select name="part">
                            <? foreach ($this->capital_districts as $letter => $capital): ?>
                                <? foreach ($capital as $key => $item): ?>
                                    <option  <?=($this->cart->getUser('address_part')==$key)?'SELECTED':''?> style="display: none" data-location="<?= $letter ?>"
                                                                                                    value="<?= $key ?>"><?= $item ?></option>
                                <? endforeach; ?>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <label class="col-xs-4 col-sm-3 col-md-3 other" for="">Другой</label>
                    <div class="col-xs-8 col-sm-5 col-md-5 other">
                        <input  type="text" name="other">
                    </div>
                    <div class="clearfix"></div>
                    <label class="col-xs-4 col-sm-3 col-md-3" for="">Город / Район</label>
                    <div class="col-xs-8 col-sm-5 col-md-5">
                        <input value="<?php echo $user['address_city']; ?>" onclick="this.value=''" type="text" name="city" id="step2_town">
                    </div>
                    <div class="clearfix"></div>
                    <label class="col-xs-4 col-sm-3 col-md-3 settlement" style="display:none" for="">Населенный пункт</label>
                    <div class="col-xs-8 col-sm-5 col-md-5 settlement" style="display:none">
                        <input  type="text" value="<?php echo $user['address_settlement']; ?>" name="settlement" id="step2_settlement">
                    </div>
                </div>
                <div class="b-delivery__title">Выберите удобный для Вас способ доставки:</div>
                <table class="b-delivery__table">
                    <tr class="b-delivery__table_main">
                        <td class="bd__kind">Тип доставки</td>
                        <td class="bd__data">Срок</td>
                        <td class="bd__cost">Цена</td>
                        <td class="bd__select hidden-xs">Выбрать</td>
                    </tr>
                    <tr class="item_js b-delivery__courier" id="courier__delivery" data-id="" style="display: none;">
                        <td><strong>Курьерская доставка</strong></td>
                        <td></td>
                        <td class="cost"></td>
                        <td class="hidden-xs"><button type="button" class="btn2">Выбрать</button></td>
                    </tr>
                    <tr class="hide hide_this_block">
                        <td colspan="4" style="padding: 10px 0;">
                            <div class="row">
                                <div class="col-xs-12">                                    
                                    <div class="col-xs-7">
                                        <label class="col-xs-2 one_line" for="">Улица</label>
                                        <div class="col-xs-10">
                                            <input name="street" value="<?php echo $user['address_street']; ?>" type="text" autocomplete="off">
                                        </div>

                                    </div>
                                    <div class="col-xs-5 pl0 pr0">
                                        <div class="col-xs-5 pl0">
                                            <label class="" for="">Дом</label>
                                            <input name="house" class="input-house" value="<?php echo $user['address_house']; ?>" type="text">
                                        </div>
                                        <div class="col-xs-6 pr0">
                                            <label class="" for="">Корпус</label>
                                            <input name="corpus" class="input-corpus" value="<?php echo $user['address_corpus']; ?>" type="text">
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 pt10">

                                        <div class="col-xs-3 pr0">
                                            <label class=" " for="">Строение</label>
                                            <input name="stroenie" class="input-stroenie" value="<?php echo $user['address_stroenie']; ?>" type="text">   
                                        </div>
                                        <div class="col-xs-3 pr0">
                                            <label class="" for="">Квартира</label>
                                            
                                            <input name="flat" class="input-flat" value="<?php echo $user['address_flat']; ?>" type="text">
                                                                                
                                        </div>
                                        <div class="col-xs-3 pr0">
                                            <label class=" one_line" for="">Домофон</label>
                                            
                                            <input name="domophone" class="input-domophone" value="<?php echo $user['address_domophone']; ?>" type="text">
                                            
                                        </div>
                                        <div class="col-xs-3">
                                            <label class=" one_line" for="">Индекс</label>
                                            
                                            <input name="regs_postcode" class="input-regs_postcode" value="<?php echo $user['postcode']; ?>" type="text">
                                            
                                        </div>

                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="b-delivery__self" id="self__delivery" style="display: none;">
                        <td colspan="4"><strong>Пункты самовывоза</strong></td>
                    </tr>
                    <tr class="item_js b-delivery__russia" id="post__delivery" data-id="0" style="display: none;">
                        <td><strong>Почта России(наложенный платеж)</strong></td>
                        <td></td>
                        <td class="cost"></td>
                        <td class="hidden-xs"><button type="button" class="btn2">Выбрать</button></td>
                    </tr>
                    <tr class="hide hide_this_block">
                        <td colspan="4" style="padding: 10px 0;">
                            <div class="row">
                                <div class="col-xs-12">                                    
                                    <div class="col-xs-7">
                                        <label class="col-xs-2 one_line" for="">Улица</label>
                                        <div class="col-xs-10">
                                            <input name="street" value="<?php echo $user['address_street']; ?>" type="text" autocomplete="off">
                                        </div>

                                    </div>
                                    <div class="col-xs-5 pl0 pr0">
                                        <div class="col-xs-5 pl0">
                                            <label class="" for="">Дом</label>
                                            <input name="house" class="input-house" value="<?php echo $user['address_house']; ?>" type="text">
                                        </div>
                                        <div class="col-xs-6 pr0">
                                            <label class="" for="">Корпус</label>
                                            <input name="corpus" class="input-corpus" value="<?php echo $user['address_corpus']; ?>" type="text">
                                        </div>
                                    </div>                                    
                                </div>
                                <div class="col-xs-12 pt10">

                                        <div class="col-xs-3 pr0">
                                            <label class=" " for="">Строение</label>
                                            <input name="stroenie" class="input-stroenie" value="<?php echo $user['address_stroenie']; ?>" type="text">   
                                        </div>
                                        <div class="col-xs-3 pr0">
                                            <label class="" for="">Квартира</label>
                                            
                                            <input name="flat" class="input-flat" value="<?php echo $user['address_flat']; ?>" type="text">
                                                                                
                                        </div>
                                        <div class="col-xs-3 pr0">
                                            <label class=" one_line" for="">Домофон</label>
                                            
                                            <input name="domophone" class="input-domophone" value="<?php echo $user['address_domophone']; ?>" type="text">
                                            
                                        </div>
                                        <div class="col-xs-3">
                                            <label class=" one_line" for="">Индекс</label>
                                            
                                            <input name="regs_postcode" class="input-regs_postcode" value="<?php echo $user['postcode']; ?>" type="text">
                                            
                                        </div>

                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>

            <? include ('step2_sidebar.php');?>

        </div>
</div> <!-- b-delivery -->

<div id="dostavka_map" style="display: none">

</div>

<script>
    var yMap,
        ymapsDef = $.Deferred(),
        ymapsResolved = false;

    /**
     * Инициализация яндекс карт для служб доставки
     */
    function initYmaps() {

        yMap = new ymaps.Map('dostavka_map', {
            center: [55.753994, 37.622093],
            zoom: 8,
            controls: ['zoomControl', 'typeSelector']
        });

        ymapsDef.resolve();
    }

    ymaps.ready(initYmaps);

    /**
     * Функция заполнения служб доставки по указанному адресу пользователя
     * или же по уже выбранной службе, сначала заполняет все возможные варианты по адресу
     * дальше выбирает нужный, если таковой был выбран по оформлении заказа
     *
     * @param {Number} kladr_id - целочисленный идентификатор кладра
     * @param {String} city - название города
     * @param {Object} resp - объект XHR запроса на получение "логистической компании"
     */
    function fillCouriers(kladr_id, city, resp) {

        $.when(ymapsDef).then(function() {
            AddressSuggestions.getLogistic(kladr_id, city).then(function() {

                var $wrapper = $('.b-delivery__table');
                $wrapper.find('.item_js[data-id="'+resp.id+'"]').trigger('click');
            });
        });
    }

    $.ajax({
        url: "/api/logistic_get/",
        data: {
            k: $("#courier_address_id").val(),
            f: 1
        },
        success: function(resp) {

            // если ответ получен, то есть был выбран способ доставки
            if (resp.id != null && resp.id != 0) {

                fillCouriers(resp.kladr_id_city, resp.name_city, resp);
            // если же способ доставки не был выбран просто грузим по городу карту
            } else {

                var addr = [],
                    result,
                    answer,
                    $cart_step2 = $('#cart_step2');

                addr.push($cart_step2.find('[name="region"]').val());
                // заполнение адреса исходя из полей пользователя
                if ($cart_step2.find('[name="region"]').val() != $('#step2_town').val()){
                    addr.push($('#step2_town').val());
                }
                addr.push($('#step2_settlement').val());

                console.log(addr);

                answer = DadataApi.clean(addr.join(" "));
                answer.then(function(msg) {

                    if (typeof msg != 'undefined') {
                        result = msg.suggestions[0]['data'];
                        fillCouriers(result.kladr_id, addr, resp);
                    }
                });
            }
        }
    });


    $('#cart_step2').on('click', '.item_js', function() {
        $(this).find('.btn2').click();
    });
</script>
