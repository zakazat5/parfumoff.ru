<div class="row">
    <div class="b-step col-sm-12 col-md-10 hidden-xs">
        <div class="clearfix">
            <p class="pull-left fs20 ml10 color_green">1 этап: Контактные данные</p>

            <p class="pull-right fs20 mr10">2 этап: Адрес и способы доставки</p>
        </div>
        <div class="step-border">
            <div class="step-border__green" style="left:0;"></div>
            <div class="step-border__circle circle1 bd_green"></div>
            <div class="step-border__circle circle2"></div>
            <div class="step-border__circle circle3"></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8" style="padding-right:50px">
        <div class="listing_title">
            Ваша корзина
        </div>

        <table class="orders_list">
            <tr class="head hidden-xs">
                <td class="ol__product">Товар</td>
                <td class="ol__prices">Цена</td>
                <td class="ol__amount">Кол-во</td>
                <td class="ol__cost">Стоимость</td>
            </tr>

            <?php foreach ($this->cart->getItems() as $item_order) { ?>

                <tr class="tr_<?php echo $item_order['uid']; ?>">

                    <td class="first_td ol__product">
                        <div class="row">
                            <div class="col-sm-4">
                                <img width="70" align="left" style="margin-right:5px"
                                     src="<?php echo $item_order['img_s']; ?>" alt="" class="cart_img hidden-xs"/>
                            </div>
                            <div class="col-sm-8">
                                <span class=""><?php echo $item_order['title']; ?></span><br/>
                                <?php if ($item_order['is_block'] == 1) { ?>
                                    <span style="display:block" class="purple">На данный товар скидки не распространяются!</span>
                                <?php } ?>
                            </div>
                        </div>
                    </td>

                    <td class="ol__prices hidden-xs">
					<span class="price">

						<?php if ($item_order['is_block'] == 1) { ?>
                            <img width="24" height="24" src="/images/default/sale_icon.png" border="0" align="left">
                        <?php } ?>


                        <? if ($this->sell): ?>
                            <s>
                                <?= parsePrice(round(($item_order['price']) + (($item_order['price']) * $this->sell))) ?>
                            </s> &nbsp;&nbsp;&nbsp;
                        <? endif; ?>


                        <?php echo parsePrice($item_order['price']); ?>

					</span><span>руб.</span>
                    </td>

                    <td class="ta_center ol__amount">

                        <div class="korz_kol-vo">
                            <a data-action="desc" data-uid="<?= $item_order['uid']; ?>"
                               href="/orders/DescOrder/<?= $item_order['uid']; ?>/<?php echo $item_order['price']; ?>/"
                               class="btn-pm minus">&ndash;</a>
                            <span class="val"><?php echo $item_order['kol']; ?></span>
                            <a data-action="asc" data-uid="<?= $item_order['uid']; ?>"
                               href="/orders/AscOrder/<?= $item_order['uid']; ?>/<?php echo $item_order['price']; ?>/"
                               class="btn-pm plus">+</a>
                        </div>
                        <input type="hidden" name="kol_<?php echo $item_order['uid']; ?>" maxlength="2"
                               value="<?php echo $item_order['kol']; ?>" id="kol_<?php echo $item_order['uid']; ?>"/>
                        <a style="top:8px" class="lh20 relative color_blue dashed remove_from_cart" data-id_product="<?=$item_order['id']?>"
                           href="/korzina/deleteOrder/<?php echo $item_order['uid']; ?>/<?php echo $item_order['sum']; ?>/<?php echo $item_order['kol']; ?>/">Удалить</a>
<? $mixmarket_ids[]=$item_order['id'];?>
                    </td>

                    <td class="ol__cost">
                        <span class="fl_left lh20 relative sum"><?php echo parsePrice($item_order['sum']); ?></span><span>&nbsp;руб.</span>
                    </td>

                </tr>
            <?php } ?>
            <tr class="bg_gray1 ol__total">
                <td colspan="4" valign="top">
                    <div class="clearfix">
                        <div class="pull-left">Сумма заказа:</div>
                        <div class="pull-right"><span class="val_subTotal"
                                                      id="big_sum"><?= $this->cart->getSubTotal() ?></span><span>&nbsp;руб.</span>
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="pull-left">Накопительная скидка:</div>
                        <div
                            class="pull-right"><?= ($this->cart->getDiscountData('percent_d')) ? ($this->cart->getDiscountData('percent_d')) : 0 ?>
                            %
                        </div>
                    </div>
                    <div class="clearfix">
                        <div class="pull-left">
                            Итоговая сумма:
                        </div>
                        <div class="color_purple bold_fat pull-right">
                            <br>
                            <span class="val_total" id="big_sum_all"><?= $this->cart->getTotal() ?></span><span>&nbsp;руб.</span>
                        </div>
                    </div>
                </td>

            </tr>
        </table>

    </div>

    <div class="checkout-form col-xs-12 col-sm-12 col-md-4">

        <div class="listing_title">
            Оформление заказа
        </div>

        <? if (!$this->is_login): ?>
        <div class="row login_row">
            <form onsubmit="yaCounter21451555.reachGoal('order_step1_er');" id="orders_register" class="order-form" autocomplete="off" action="/korzina/" method="post"
                  enctype="multipart/form-data" name="orders_list">
                <div class="col-sm-6 col-md-12">
                    <p class="color_purple mb15">Я впервые покупаю на Parfumoff</p>

                    <div class="row">
                        <label class="col-xs-4" for="">Имя</label>

                        <div class="col-xs-8">
                            <input name="fio" type="text" value="<?= $this->data['fio'] ?>">
                            <input name="fio_data" value="" type="hidden">
                            <? if ($this->aErrors['fio'] != ''): ?>
                                <label class="error"><?= $this->aErrors['fio'] ?></label>
                            <? endif; ?>
                        </div>


                        <label class="col-xs-4" for="">E-mail</label>

                        <div class="col-xs-8">
                            <input id="email" name="email" type="text" value="<?= $this->data['email'] ?>">
                            <? if ($this->aErrors['email'] != ''): ?>
                                <label class="error"><?= $this->aErrors['email'] ?></label>
                            <? endif; ?>
                        </div>

                        <label class="col-xs-4" for="">Телефон</label>

                        <div class="col-xs-8">
                            <input name="phone" type="text" class="phone_input" value="<?= $this->data['phone'] ?>">
                            <? if ($this->aErrors['phone'] != ''): ?>
                                <label class="error"><?= $this->aErrors['phone'] ?></label>
                            <? endif; ?>
                        </div>
                    </div>
                    <div class="col-xs-4 step1_btn"></div>
                    <div class="col-xs-8 step1_btn">
                        <button type="submit" name="register" class="btn2 cursor_pointer">Оформить заказ</button>
                    </div>
                </div>
                <div class="border_hr hidden-sm"></div>
            </form>
        </div>

        <form id="orders_login" action="/users/login/" method="post" name="reg_form">
                <p class="color_green- color_purple mt20">Я уже покупал здесь и хочу оформить заказ через Личный кабинет. </p>
                <div class="row">

                    <div class="col-xs-4 step1_btn"></div>
                    <div class="col-xs-8 step1_btn mt10 mb10">
                        <a data-click="show_login_form" style="text-align:center;font-size: 18px;width: 278px; display: block;" class="btn1">Войти в личный кабинет</a>
                    </div>

                </div>
        </form>
    </div>
    <? else: ?>
        <form autocomplete="off" action="/korzina/" method="post" enctype="multipart/form-data" name="orders_list"
              id="orders">
            <div style="padding: 70px 0">
                <div class="rCol">
                    <button type="submit" name="purchase" class="btn2">Оформить заказ</button>
                </div>
            </div>
        </form>
    <? endif; ?>

</div>  <!-- checkout-form -->

</div> <!-- row -->


<script>
    $(function () {
        $("#orders_register").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                fio: {
                    required: true,
                },
                phone: {
                    required: true,
                }
            },
        });


        $("#orders_login").validate({
            rules: {
                log_email: {
                    required: true,
                    email: true
                },
                log_passw: {
                    required: true,
                },
            },
        });

    })
</script>

<div style="display:none">
    <div class="box-modal" id="login_form_popup" style="background:white;padding:45px 105px;">
        <div class="box-modal_close arcticmodal-close">закрыть</div>


        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#showEmail" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">Ввести email</a></li>
            <li role="presentation" class=""><a href="#showPhone" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">Ввести телефон</a></li>
        </ul>

        <div class="tab-content" style="border: 1px solid #dddddd;padding: 12px;    width: 347px;">
            <div role="tabpanel" class="tab-pane active" id="showEmail">
                <div class="title">Оформление через личный кабинет</div>
                <div style="margin-bottom:20px;">
                    Введите ваш e-mail использованный при регистрации на сайте
                </div>

                <form id="login_form" onsubmit="yaCounter21451555.reachGoal('order_user_er');" action="/users/login/" method="post" name="reg_form">
                <span class="error" style="display: none;color: #a61319;">
                    Данный E-mail не обнаружен в наше базе.
                    Пожалуйста, проверьте введенный e-mail адрес: <br><br>
                </span>
                    <div class="line"><input name="log_email" type="text" placeholder="e-mail" /></div>
                    <div>
                        <button style="text-align:center;" name="register" class="btn1 popup_btn1">Оформить заказ через личный кабинет</button>
                    </div>

                </form>

            </div>


            <div role="tabpanel" class="tab-pane" id="showPhone">
                <div class="title">Оформление через личный кабинет</div>
                <div style="margin-bottom:20px;">
                    Введите ваш телефон, использованный при регистрации на сайте
                </div>

                <form id="login_form_phone" action="/users/login/" method="post" name="reg_form_phone">
                <span class="error" style="display: none;color: #a61319;">
                    Данный телефон не обнаружен в наше базе.
                    Пожалуйста, проверьте введенный телефон: <br><br>
                </span>
                    <div class="line"><input name="phone" class="phone_mask" type="text" placeholder="Телефон" /></div>
                    <div>
                        <button style="text-align:center;" name="register" class="btn1 popup_btn1">Оформить заказ через личный кабинет</button>
                    </div>

                </form>

            </div>


            <div class="error" style="display: none">
                <br>
                <br>
                Или оформите как “Новый покупатель”:
            </div>

            <form id="login_form_register" action="/users/login/" method="post" name="reg_form" style="display: none;margin-top: 10px;">
                <div class="line"><input name="log_email" type="text" placeholder="e-mail" /></div>
                <div class="line"><input name="phone" type="text" placeholder="Телефон" /></div>
                <div class="line"><input name="fio" type="text" placeholder="ФИО" /></div>
                <input name="fio_data" value="" type="hidden">
                <div>
                    <button name="register" class="btn2 popup_btn2">Оформить заказ</button>
                </div>
            </form>
        </div>




    </div>
</div>

<script>
    $('#login_form').on('submit',function(e){
        e.preventDefault();
        $.ajax({ url: '/users/login-ajax/',data:$('#login_form').serialize(),method:'post'}).done(function(data){
            var data = $.parseJSON(data);
            if (data.login==true){
                document.location.href = '/korzina/thankyou_basket_force/';
            }else{
                $('#login_form_register').show();

                $('#login_form_popup .error').show();

                var email = $('#login_form').find('[name=log_email]').val();
                $('#login_form_register').find('[name=log_email]').val(email);

            }
        });
    });

    $('#login_form_phone').on('submit',function(e){
        e.preventDefault();
        $.ajax({ url: '/users/login-ajax/',data:$('#login_form_phone').serialize(),method:'post'}).done(function(data){
            var data = $.parseJSON(data);
            if (data.login==true){
                document.location.href = '/korzina/thankyou_basket_force/';
            }else{
                $('#login_form_register').show();
                $('#login_form_popup .error').show();
                var phone = $('#login_form_phone').find('[name=phone]').val();
                $('#login_form_register').find('[name=phone]').val(phone);
            }
        });
    });


    $('#login_form_register').on('submit',function(e){
        e.preventDefault();
        $.ajax({ url: '/users/register-ajax/',data:$('#login_form_register').serialize(),method:'post'}).done(function(data){
            var data = $.parseJSON(data);

            if (data.login==true){
                document.location.href = '/korzina/thankyou_basket_force/';
            }else{

                if (data.errors){
                    $('#login_form_register').find('input').css('border','1px solid #cbcbcb')
                    $.each(data.errors,function(i,v){
                        $('#login_form_register').find('[name=' + i + ']').css('border-color','red')
                    })
                }


            }
        });
    })
</script>
<!--Трэкер "Корзина"--> 
<script type="text/javascript">
window.__mixm__.push(['basket',{skulist:'<?php echo implode(",",$mixmarket_ids) ?>'}]); 
</script> 
<!--Трэкер "Корзина"-->


