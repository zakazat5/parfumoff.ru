<div class="bread_crumps">
    <a href="/">Главная</a>
    <span>&gt;</span>
    Отзывы о магазине
</div>


<h2 style="margin-bottom: 30px;">Отзывы о магазине</h2>
<? if ($this->thanks): ?>
    <h3>Ваш комментарий добавлен. Спасибо!</h3>
<? else: ?>
    <? if ($this->aErrors): ?>
        При добавлении комментария возникли следующие ошибки:
        <ul style="color:red">
            <? foreach ($this->aErrors as $error): ?>
                <li><?= $error ?></li>
            <? endforeach; ?>

        </ul>
    <? endif; ?>
    <form method="post">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <span class="sort_title d_inline-block mr25">Оцените магазин:</span>

                <div style="top:6px" class="relative stars d_inline-block">
                    <input name="rate" type="radio" value="1" <?= ($this->recommend['rate'] >= 1) ? 'checked' : '' ?>
                           class="star2 required"/>
                    <input name="rate" type="radio" value="2" <?= ($this->recommend['rate'] >= 2) ? 'checked' : '' ?>
                           class="star2"/>
                    <input name="rate" type="radio" value="3" <?= ($this->recommend['rate'] >= 3) ? 'checked' : '' ?>
                           class="star2"/>
                    <input name="rate" type="radio" value="4" <?= ($this->recommend['rate'] >= 4) ? 'checked' : '' ?>
                           class="star2"/>
                    <input name="rate" type="radio" value="5" <?= ($this->recommend['rate'] >= 5) ? 'checked' : '' ?>
                           class="star2"/>
                </div>
                <br>
                <input type="text" name="order_id" placeholder="Номер заказа" style="height:35px;"
                       value="<?= $this->recommend['order_id'] ?>">
                <input type="text" name="user_name" placeholder="Ваше имя" style="height:35px;margin-top: 10px;"
                       value="<?= $this->recommend['user_name'] ?>">
                <input type="hidden" name="bot" value="">
                <textarea name="text" placeholder="Текст отзыва"
                          style="margin-top: 10px; resize: none; height:120px;"><?= $this->recommend['text'] ?></textarea>
            </div>
            <div class="col-xs-12 col-sm-6">
                <p class="color color_gray1">Здесь Вы можете ознакомиться с отзывами о нашем магазине, а так же оставить
                    свой. Ваш отзыв очень важен для нас. Читайте отзывы о нас на
                    <a rel="nofollow" href="http://market.yandex.ru/shop/41090/reviews" class="color_blue td_underline">Яндекс
                        Маркете</a>
                </p>
                <br>
                <br>

                <p class="color color_gray1">Для того, чтобы оставить свой отзыв, необходимо
                    <a href="/users/register/" class="color_blue td_underline">зарегистрироваться</a> или <a
                        data-target="#user_popup" class="color_blue td_underline" href="/users/">авторизоваться</a>
                </p>
            </div>
        </div>
        <button type="submit" class="btn3">Отправить</button>
    </form>
<? endif; ?>
<div class="row" style="margin: 20px 0">
    <? foreach ($this->aRecommend as $recommend): ?>
        <div class="comment">
            <div class="comment__title"><?= $recommend['user_name'] ?> <a
                    href="/production/<?= $recommend['brand_url'] ?>/<?= $recommend['url'] ?>/"><?= $recommend['title'] ?></a>
            </div>
            <? if ($recommend['order_id']>0):?>
            <div class="comment__order">Заказ №<?=$recommend['order_id']?></div>
            <? endif;?>
            <div class="stars ">
                <div class="<?= ($recommend['rate'] >= 1) ? 'star_a' : 'star_ua' ?>"></div>
                <div class="<?= ($recommend['rate'] >= 2) ? 'star_a' : 'star_ua' ?>"></div>
                <div class="<?= ($recommend['rate'] >= 3) ? 'star_a' : 'star_ua' ?>"></div>
                <div class="<?= ($recommend['rate'] >= 4) ? 'star_a' : 'star_ua' ?>"></div>
                <div class="<?= ($recommend['rate'] >= 5) ? 'star_a' : 'star_ua' ?>"></div>
            </div>
            <div class="text">
                <?= $recommend['text'] ?>

            </div>
            <? if ($recommend['answer'] != ''): ?>
                <div class="comment__title answer">Ответ магазина</div>
                <div class="text answer">
                    <p><?= $recommend['answer'] ?></p>
                </div>
            <? endif; ?>
        </div>
    <? endforeach; ?>


    <? if ($this->aRecommend instanceof Zend_Paginator): ?>
        <?= $this->paginationControl($this->aRecommend, 'Elastic', 'partials/paginator.php'); ?>
    <? endif; ?>

</div>

<script>
    $('input.star2').rating({});
</script>