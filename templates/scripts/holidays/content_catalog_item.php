<?php
/**
 * @var $catalog_item array product_item
 */
?>
<div class="col-md-4">
<div class=" item" <?php echo ($_COOKIE['outstock'])?'style="display:block;"':''?>>
	<?= ($catalog_item['is_spec']) ? '<div class="badge_discount"></div>' : '' ?>

	<a href="<?php echo $catalog_item['url']; ?>">
		<div class="img hidden-xs">
			<img alt="<?php echo $catalog_item['title']; ?>" src="<?php echo $catalog_item['img']; ?>"/>
		</div>
		<div class="title hidden-xs">
			<span><?php echo $catalog_item['brend_title']; ?></span>
			<span class="color_purple"><?php echo $catalog_item['title']; ?></span>
		</div>
		<span class="visible-xs-inline-block color_blue td_underline"><?php echo $catalog_item['brend_title']; ?> <?php echo $catalog_item['title']; ?></span>
	</a>
	<div class="descr hidden-xs">
		<?php echo $catalog_item['anot'] ?>
	</div>
	<?php if ($catalog_item['price_s'] != 0): ?>
		<div class="price hidden-xs">
			<span class="color_purple">от <?php echo $catalog_item['price_s'] ?></span>
			до <?=($catalog_item['price_e'])?$catalog_item['price_e']:$catalog_item['price_s'] ?> руб.
		</div>
		<div class="price_mobile visible-xs-inline-block color color_def">от <?php echo $catalog_item['price_s'] ?> до <?php echo $catalog_item['price_e'] ?> руб.</div>
		<div class="bottom fl_left hidden-xs">
			<a href="<?php echo $catalog_item['url']; ?>" class="color_blue td_underline">Подробнее</a>
		</div>
		<div class="bottom fl_right hidden-xs">
			<a href="<?php echo $catalog_item['url']; ?>" class="btn1">Купить</a>
		</div>
	<?php else: ?>
		<div class="price hidden-xs">Нет в наличии</div>
		<div class="price_mobile visible-xs-inline-block color color_def">Нет в наличии</div>
		<div class="bottom fl_left hidden-xs">
			<a href="<?php echo $catalog_item['url']; ?>" class="color_blue td_underline">Подробнее</a>
		</div>
	<?php endif; ?>
</div>
</div>
