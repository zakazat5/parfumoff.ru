<div class="bread_crumps">
<a href="/">Главная</a>&nbsp;/
<a href="/accessories/">Аксессуары</a>&nbsp;/
<?php echo $this->brend_title; ?>&nbsp;/
<?php echo $this->catalog_full['title']; ?>
</div>

<div class="acc_nav">
	<?php foreach ($this->access_type as $k => $v)  { ?>
	<a href="/accessories/filter/type/<?php echo $k; ?>/brend/all/"><?php echo $v; ?></a>|
	<?php } ?>
	<div></div>
</div>

<div class="desc_wrap1">
	<div class="desc_img1">
		<img src="<?php echo $this->catalog_full['img']; ?>" />
	</div>
	<div class="desc_title">
		<h1><b><?php echo $this->brend_title; ?></b><br /> <?php echo $this->catalog_full['title']; ?></h1>
	</div>
	<div class="desc_desc desc_new">
	
		<?php if($this->catalog_full['text'] != '') { ?>
			<div class="notes new_notes"><?php echo $this->catalog_full['text'];?></div>
		<?php } ?>
		
		<?php if(is_array($this->catalog_item_galarey)) { ?>
		<div class="mini_slides">
			<div class="carousel vertical">
			<div class="prev"></div>
			<div id="gal_wr11">
			<div class="jCarouselLite">
				<ul>
					<?php foreach ($this->catalog_item_galarey as $catalog_item_galarey) { ?>
					<li><a href="/images/uploads/catalog/<?php echo $this->catalog_full['id'];?>/big/<?php echo $catalog_item_galarey['img'];?>" rel="group" class="zoom1"><img width="90" height="80" id="slide_img<?php echo $catalog_item_galarey['id'];?>" alt="" src="/images/uploads/catalog/<?php echo $this->catalog_full['id'];?>/small/<?php echo $catalog_item_galarey['img'];?>" /></a></li>
					<?php } ?>
				</ul>
			</div>
			</div>
			<div class="next"></div>
			<div class="clear"></div>
			</div>
		</div>
		<?php } ?>
	</div>
	
	<div class="clear"></div>
	<div id="cart_items">
		<table cellpadding="0" cellspacing="0" class="cart_table">
			<tr class="cart_item1">
				<td class="name"><?php echo $this->brend_title; ?></td>
				<td class="item_desc"><?php echo $this->catalog_full['title']; ?></td>
				<td class="item_price"><?php echo $this->catalog_full['price']; ?> руб</td>
				<td class="to_buy">
				<span class="cart_p_table_loader korzina_loader_<?php echo $this->OrderID;?>" style="display:none;">подождите...</span>
				<input value="купить" type="button" class="korzina" id="<?php echo $this->OrderID;?>" />
				</td>
			</tr>
		</table>
	</div>
	
	<div class="warning1">
		<noindex>
		<p>Доставка по Москве осуществляется в день заказа, если заказ сделан до 12-00, и на следующий день, если заказ сделан после 12-00. Заказы свыше 1000 руб доставляются бесплатно в пределах МКАД. При отсутсвии свободных курьеров и наличия товара на складе Parfumoff вправе изменить условия доставки.</p>
		</noindex>
	</div>
	
	<div class="warning1">
		<noindex>
		<p>Для того, чтобы совершить покупку в нашем магазине, Вам необходимо выбранный 
		Вами товар положить в корзину (нажать на кнопку справа от товара). После окончания выбора товаров, 
		Вам необходимо <a href="/orders/">перейти в корзину</a> (по ссылке в правом меню), заполнить форму заявки и ждать звонка оператора.</p>
		</noindex>
	</div>
		
</div>