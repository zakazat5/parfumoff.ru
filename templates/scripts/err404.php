
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>404</title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />	
	<meta name="description" content="" />
    <meta name="apple-mobile-webapp-capable" content="yes"/>

    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">

	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="google-site-verification" content="iIenX44wjOQwSqAas3KPi-VGArn1bbHhl8JSwnaqqqs" />
    <meta name='yandex-verification' content='409ef96cc5640923' />
	<meta name="mailru-verification" content="0afd7cdfb1b4f685" />
    <meta name="yandex-verification" content="c7a0d3eee56e5148" />

	<link rel="stylesheet" href="/templates/css/reset.css" type="text/css"  />
	<link rel="stylesheet" href="/templates/css/grid.css" type="text/css"  />
	<link rel="stylesheet" href="/templates/css/style.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/retina.css" type="text/css"  />
	<link rel="stylesheet" href="/templates/css/responsive.css" type="text/css"  />
	<link rel="stylesheet" href="/templates/css/cart.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/suggestions.css" type="text/css" />
    <link rel="stylesheet" href="/templates/css/button.css" type="text/css" />
    <link rel="stylesheet" href="/templates/css/bootstrap/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/templates/js/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="/templates/css/suggestions.css" type="text/css" />
    <link rel="stylesheet" href="/templates/lib/star-rating/jquery.rating.css" type="text/css" />
	
	<link rel="stylesheet" href="/templates/css/style_new.css" type="text/css" />

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>	
	<script type="text/javascript" src="/templates/js/script.js"></script>
	<script src="/templates/lib/iphone_checkbox/iphone_checkbox.js"></script>
	<link rel="stylesheet" href="/templates/lib/iphone_checkbox/style.css" />
    <script src="/templates/js/cart.js"></script>
	<script type="text/javascript" src="/templates/js/scripts.js"></script>
	<script type="text/javascript" src="/templates/js/filter_v2.js"></script>
    <script type="text/javascript" src="/templates/lib/star-rating/jquery.rating.pack.js"></script>

    <link href="/templates/lib/select2-3.5.1/select2.css" rel="stylesheet"/>
    <script src="/templates/lib/select2-3.5.1/select2.js"></script>

    <script type="text/javascript" src="/templates/js/dadata/suggestions-jquery-4.1.min.js"></script>
    <script type="text/javascript" src="/templates/js/dadata.js"></script>
    <script type="text/javascript" src="/templates/js/suggest.js"></script>
    <script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>
    <script src="/templates/js/bootstrap-select/bootstrap-select.js"></script>
    <script src="/templates/js/validate/jquery.validate.js"></script>
    <script src="/templates/js/validate/localization/messages_ru.min.js"></script>
    <script src="/templates/js/bootstrap.js"></script>
    <script src="/templates/js/jquery_lazyload/jquery.lazyload.js?v=1.9.1"></script>
    <script src="http://vk.com/js/api/openapi.js" type="text/javascript"></script>
    
    <link rel="stylesheet" href="/templates/js/jquery.scrollbar/jquery.scrollbar.css" />
    <script src="/templates/js/jquery.scrollbar/jquery.scrollbar.js"></script>


    <link rel="stylesheet" href="/templates/js/arcticmodal/jquery.arcticmodal-0.3.css?v=1" type="text/css" media="screen" />
    <script src="/templates/js/arcticmodal/jquery.arcticmodal-0.3.min.js?v=1"></script>

	
	

    <script>
        var region_name = 'Тульская область';
        var city_name = '';
    </script>
	
<!-- CallTouch-->	
<script type="text/javascript">
function ct(w,d,e,c){
var a='all',b='tou',src=b+'c'+'h';src='m'+'o'+'d.c'+a+src;
var jsHost="https://"+src,s=d.createElement(e),p=d.getElementsByTagName(e)[0];
s.async=1;s.src=jsHost+"."+"r"+"u/d_client.js?param;"+(c?"client_id"+c+";":"")+"ref"+escape(d.referrer)+";url"+escape(d.URL)+";cook"+escape(d.cookie)+";";
if(!w.jQuery){var jq=d.createElement(e);
jq.src=jsHost+"."+"r"+'u/js/jquery-1.7.min.js';
jq.onload=function(){
p.parentNode.insertBefore(s,p);};
p.parentNode.insertBefore(jq,p);}else{
p.parentNode.insertBefore(s,p);}}
if(!!window.GoogleAnalyticsObject){window[window.GoogleAnalyticsObject](function(tracker){ 
if (!!window[window.GoogleAnalyticsObject].getAll()[0])
{ct(window,document,'script', window[window.GoogleAnalyticsObject].getAll()[0].get('clientId'))}
else{ct(window,document,'script', null);}});
}else{ct(window,document,'script', null);}
</script>

<!-- Facebook Pixel Code -->

<script>

!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?

n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;

n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;

t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,

document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '1291083647577345');

fbq('track', 'PageView');

</script>

<noscript><img height="1" width="1" style="display:none"

src="https://www.facebook.com/tr?id=1291083647577345&ev=PageView&noscript=1"

/></noscript>

<!-- DO NOT MODIFY -->

<!-- End Facebook Pixel Code -->

<!-- Rees46 Code -->

<script>
(function(r){window.r46=window.r46||function(){(r46.q=r46.q||[]).push(arguments)};var s=document.getElementsByTagName(r)[0],rs=document.createElement(r);rs.async=1;rs.src='//cdn.rees46.com/v3.js';s.parentNode.insertBefore(rs,s);})('script');
// Init everything
r46('init', '32f7c3e11cc04298e31945b8ffba6f');
</script>

<!-- End Rees46 Code -->

</head> <body>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PX333L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PX333L');</script>
<!-- End Google Tag Manager -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<div class="header">
    <div class="top-bar">
        <div class="wrapper top-bar__wrapper">
            <div class="menu">
				<a class="menu__item" href="/about/#contact">Контакты</a>
				<a class="menu__item" href="/worktime/">Режим работы</a>
				<a class="menu__item" href="/about/#dostavka">Доставка</a>
				<a class="menu__item" href="/oplata/">Оплата</a>
				<a class="menu__item" href="/kachestvo/">Качество и гарантии</a>
				<a class="menu__item" href="/disc/">Скидки</a>
				<a class="menu__item" href="/article/">Статьи</a>
				<a class="menu__item" href="/recommend/">Отзывы</a>
				<a class="menu__item" href="/o_nas/">О нас</a>
				<a class="menu__item" href="/return/">Возврат</a>
			</div>
            <div class="enter">
									<a class="enter__link" href="/users/">Войти</a>
					<div class="enter__divider">/</div>
					<a class="enter__link" href="/users/register/">Регистрация</a>
				            </div>
        </div>
    </div>
    <div class="wrapper header__wrapper">
		<a class="logo" href="/">
			<img class="logo__picture" src="/templates/img/logo.png" alt="Parfumoff.ru – интернет-магазин парфюмерии.">
			<div class="logo__title">оригинальная парфюмерия</div>
            <div class="logo__age">Мы работаем с 2008 года</div>
		</a>
        <div class="phone">
            <div class="phone__header">Москва</div>
            <div class="phone__content">8 (495) 540-47-23</div>
			<a class="phone__callback callMe-js" href="">заказать обратный звонок</a>
        </div>
        <div class="search">
           	<form class="search__form" enctype="multipart/form-data" method="post" action="/find/">
				<input class="search__input" name="find" type="text" placeholder="Поиск товара по названию">
				<button class="search__button" type="submit"></button>
			</form>
        </div>
		<div>
						<input class="rees46 track cart" value='[]' type="hidden" />
			<a class="button header__cart" href="/korzina/">Корзина: 0 товаров</a>
			<div class="header__cart_block" style="display:none;">
				<div>
					<a class="" href="/korzina/">Оформить заказ</a>
				</div>
				<div>
					<a class="" href="/">Продолжить покупки</a>
				</div>
			</div>
		</div
    </div>
</div>
<div class="alphabet">
    <div class="wrapper">
        <div class="alphabet__header">
            <div class="alphabet__title">Торговые дома по алфавиту</div>
			<a class="alphabet__link" href="/production/">Посмотреть все бренды</a>
        </div>
        <div class="alphabet__content">
            <div class="alphabet__category">
				<a class="alphabet__item" href="/liter/a/">A</a>
				<a class="alphabet__item" href="/liter/b/">B</a>
				<a class="alphabet__item" href="/liter/c/">C</a>
				<a class="alphabet__item" href="/liter/d/">D</a>
				<a class="alphabet__item" href="/liter/e/">E</a>
				<a class="alphabet__item" href="/liter/f/">F</a>
				<a class="alphabet__item" href="/liter/g/">G</a>
				<a class="alphabet__item" href="/liter/h/">H</a>
				<a class="alphabet__item" href="/liter/i/">I</a>
				<a class="alphabet__item" href="/liter/j/">J</a>
				<a class="alphabet__item" href="/liter/k/">K</a>
				<a class="alphabet__item" href="/liter/l/">L</a>
				<a class="alphabet__item" href="/liter/m/">M</a>
				<a class="alphabet__item" href="/liter/n/">N</a>
				<a class="alphabet__item" href="/liter/o/">O</a>
				<a class="alphabet__item" href="/liter/p/">P</a>
				<a class="alphabet__item" href="/liter/q/">Q</a>
				<a class="alphabet__item" href="/liter/r/">R</a>
				<a class="alphabet__item" href="/liter/s/">S</a>
				<a class="alphabet__item" href="/liter/t/">T</a>
				<a class="alphabet__item" href="/liter/u/">U</a>
				<a class="alphabet__item" href="/liter/v/">V</a>
				<a class="alphabet__item" href="/liter/w/">W</a>
				<a class="alphabet__item" href="/liter/x/">X</a>
				<a class="alphabet__item" href="/liter/y/">Y</a>
				<a class="alphabet__item" href="/liter/z/">Z</a>
			</div>
            <div class="alphabet__category">
				<a class="alphabet__item" href="">1-9</a>
			</div>
            <div class="alphabet__category">
				<a class="alphabet__item" href="">А-Я</a>
			</div>
        </div>
    </div>
</div>
<div id="login" style="display:none">
    <span class="close">&times;</span>
    <form action="">
        <div class="mb20"><input type="text" placeholder="Имя" /></div>
        <div class="mb20"><input type="text" placeholder="Телефон" /></div>
        <button class="btn2">Сообщить о поступлении</button>
    </form>
</div>


<div id="register" style="display:none">
    <span class="close">&times;</span>
    <form action="">
        <div class="mb20"><input type="text" placeholder="Имя" /></div>
        <div class="mb20"><input type="text" placeholder="Телефон" /></div>
        <button class="btn2">Сообщить о поступлении</button>
    </form>
</div>

<script>

</script>

<div id="fast_view" class="popup" style="display:none">
    <span class="close">&times;</span>
    <div class="fast_view__content">

    </div>
</div>


<script>
    function show_fast_view(url) {

        url = url + '?fast_view=1'
        $.ajax({
            method: "POST",
            url: url
        })
            .done(function (msg) {
                var
                    fv_wrap = $('#fast_view'),
                    fv_close = fv_wrap.find('.close')
                fv_wrap.find('.fast_view__content').html(msg)

                fv_wrap.show()

                $('body').append($('<div/>').attr('id', 'overlay'))
				/*
                var fv_h = parseInt(fv_wrap.height() / 2) * -1 + 'px'
                fv_wrap.css('margin-top', fv_h)
				*/
				fv_wrap.css('margin-top', 0);	
				
                fv_close.click(function () {
                    $('#fast_view').hide()
                    $('#overlay').remove()
                })

                $('#overlay').click(function () {
                    $('.popup').hide()
                    $(this).remove()
                })

            });


    }
	
	$(document).ready(function() {
		if (screen.width <= 480) {
			$("html").css({ 'overflow-x': 'scroll'});
		}
	});

</script>

<div class="wrapper content clearfix">
    

<br />
<div class="news" style="margin-right:35px">



<h1>Страница не найдена (404 Not Found)</h1>
	<br>
 <p>Здравствуйте, уважаемый посетитель нашего сайта!</p>

<p>К сожалению, запрашиваемая Вами страница не найдена на сайте нашей компании.</p>
<p>Возможно, это случилось по одной из следующих причин:</p>
<ul style="list-style:disc;">
<li>Вы ошиблись при наборе адреса страницы (URL);</li>
<li>Вы перешли по «битой» (неработающей или неправильной) ссылке;</li>
<li>Запрашиваемой Вами страницы никогда не было на сайте либо она была удалена или перемещена.</li>
</ul>

<p>Мы приносим свои извинения за предоставленные неудобства и предлагаем следующие пути решения:</p>
<ul style="list-style:disc;">
<li>	вернуться назад при помощи кнопки браузера back;</li>
<li>	проверить правильность написания адреса страницы (URL);</li>
<li>	перейти на <a href="/">главную страницу сайта</a> или воспользоваться навигационным меню;</li>
<li>	связаться с нами по телефону +7 (495) 540-47-23;</li>
<li>	написать письмо на нашу электронную почту <a href="mailto:info@parfumoff.ru">info@parfumoff.ru</a>.</li>
</ul>




</div>    
	
	

</div>

<div class="footer">
    <div class="footer__content">
        <div class="wrapper footer__wrapper">
            <div class="footer__content-left">
				<div class="footer__city">Москва</div>
				<div class="footer__contacts">Адрес: ул. Николоямская, д. 48, стр. 2  <br>Тел.: 8 (495) 540-47-23 <br> Вопросы и предложения: info@parfumoff.ru</div>
				<div class="footer__menu">
					<a class="footer__menu-link" href="/about/#contact">Контакты</a>
					<a class="footer__menu-link" href="/worktime/">Режим работы</a>
					<a class="footer__menu-link" href="/about/#dostavka">Доставка</a>
					<a class="footer__menu-link" href="/oplata/">Оплата</a>
					<a class="footer__menu-link" href="/kachestvo/">Качество и гарантии</a>
					<a class="footer__menu-link" href="/disc/">Скидки</a>
					<a class="footer__menu-link" href="/article/">Статьи</a>
					<a class="footer__menu-link" href="/o_nas/">О нас</a>
					<a class="footer__menu-link" href="/return/">Возврат</a>
                    <a class="footer__menu-link" href="/map/">Карта сайта</a>
				</div>
            </div>
			<a class="footer__payments" href="/oplata/">
				<div class="footer__payments-header">Мы принимаем:</div>
				<img class="footer__payments-list" src="/templates/img/payments.png" alt="Payments">
			</a>
        </div>
    </div>
</div>
<div class="copyright">
    <div class="wrapper copyright__wrapper">
        <div class="copyright__content">© Parfumoff.ru 2008 - 2017</div>
		<div class="copyright__lv-link">
			<!-- Код тега ремаркетинга Google -->
<!--------------------------------------------------
С помощью тега ремаркетинга запрещается собирать информацию, по которой можно идентифицировать личность пользователя. Также запрещается размещать тег на страницах с контентом деликатного характера. Подробнее об этих требованиях и о настройке тега читайте на странице http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 958656022;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/958656022/?value=0&amp;guid=ON&amp;script=0"/>
    </div>
</noscript>

<!--LiveInternet counter--><script type="text/javascript"><!--
    document.write("<a href='http://www.liveinternet.ru/click' "+
    "target=_blank><img src='http://counter.yadro.ru/hit?t45.13;r"+
    escape(document.referrer)+((typeof(screen)=="undefined")?"":
    ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
        screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
    ";"+Math.random()+
    "' alt='' title='LiveInternet' "+
    "border='0' width='31' height='31'><\/a>")
    //--></script><!--/LiveInternet-->



    
        <!--
        BEGIN JIVOSITE CODE {literal} -->
        <script type='text/javascript'>
		if (screen.width > 480) {
			(function(){
			var widget_id = '1nW2vPalyZ';
			var s =
				document.createElement('script'); s.type = 'text/javascript'; s.async = true;
			s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss =
            document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s,
            ss);})();
		}	
		</script>
<!-- {/literal} END JIVOSITE CODE -->
    
	
	

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter21451555 = new Ya.Metrika({id:21451555,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21451555" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->




<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
 
    function getRetailCrmCookie(name) {
        var matches = document.cookie.match(new RegExp(
            '(?:^|; )' + name + '=([^;]*)'
        ));

        return matches ? decodeURIComponent(matches[1]) : '';
    }

    ga('create', 'UA-52259840-1', 'parfumoff.ru');
    ga('set', 'dimension1', getRetailCrmCookie('_ga'));
	ga('set','dimension2',''); 	
	ga('set','dimension3','product'); 	
    ga('send', 'pageview');

	
    /* Accurate bounce rate by time */
    if (!document.referrer || document.referrer.split('/')[2].indexOf(location.hostname) != 0)
        setTimeout(function () {
            ga('send', 'event', '15_seconds', location.pathname);
        }, 15000);
	
</script>

<!--Трэкер-контейнер "Основной"--> 
<script type="text/javascript">
window.__mixm__ = window.__mixm__ || [];
window.__mixm__.push(['mAdvId',1294983815]);
// только на страницах карточки товара передавайте его айди (вместо 'ID товара') из вашего прайса и раскомментируйте вызов этого параметра
window.__mixm__.push(['skulist', '']);

(function(){function t(){if(!e){e=1;var t=0,a="def";for(i=0;o.__mixm__.length>i;i++){if("uAdvArId"==o.__mixm__[i][0]){t="u"+o.__mixm__[i][1];break}"mAdvId"==o.__mixm__[i][0]&&(a="m"+o.__mixm__[i][1])}t||(t=a);var n=document.createElement("script");n.type="text/javascript",n.async=!0,n.src=("https:"==document.location.protocol?"https://":"http://")+"js.mixmarket.biz/a"+t+".js?t="+(new Date).getTime();var r=document.getElementsByTagName("script")[0];r.parentNode.insertBefore(n,r)}}var e=0,a=document,n=a.documentElement,o=window;"complete"==a.readyState||"loaded"==a.readyState||"interactive"==a.readyState?t():a.addEventListener?a.addEventListener("DOMContentLoaded",t,!1):a.attachEvent?(n.doScroll&&o==o.top&&function(){try{n.doScroll("left")}catch(e){return setTimeout(arguments.callee,0),void 0}t()}(),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t()})):o.onload=t})();
</script> 
<!--Трэкер-контейнер "Основной"-->


		</div>
    </div>
</div>


</body>
</html>