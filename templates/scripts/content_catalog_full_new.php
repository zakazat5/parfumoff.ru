<?php require_once(__DIR__ . '/../../includes/geo_function.php'); ?>
<div class="pl25 pr25 aromat">
    <div class="breadcrumbs fs14 mb30">
        <a class="td_underline" href="/production/">Каталог продукции</a>
        <span>&gt;</span>
        <a class="td_underline" href="/production/<?= $this->brend['url'] ?>/"><?= $this->brend['title'] ?></a>
        <span class="">&gt;</span>
        <span><?= $this->catalog_full['title'] ?></span>
    </div>

    <h1 class="listing_title" style="float: left">
		<span class="listing_title__item mr10">Духи <?= $this->brend['title'] ?> <?= $this->catalog_full['title'] ?></span>
    </h1>
    <div class="clear"></div>
    <span style="top:-25px" class="pr50 relative fs14 pol_sort_line"><?=$this->catalog_full['pol']?> аромат</span>
    <div class="clear"></div>

    <? require_once('partials/sort_rate_new.php'); ?>

    <div class="row top_aromat">

        <div class="col-sm-4 col-xs-12">
            







            <? if (count($this->aPhotos)>0) { ?>
                <div class="aromat_img mb30">
                    <img id="product_cat_<?php echo $this->catalog_full['id_catalog']; ?>" src="<?= $this->catalog_full['img']; ?>" alt="<?php echo $this->catalog_full['title']; ?>"/>
                </div>
                <div class="cardImageSlider hide_979">
                    <ul class="cardImageSlider-js">                    
                        <li>
                            <img data-big="<?= $this->catalog_full['img']; ?>" src="<?= $this->catalog_full['img']; ?>" alt="<?php echo $this->catalog_full['title']; ?>">
                        </li>
                        <? foreach ($this->aPhotos as $oPhoto):?>
                            <li><img data-big="/images/uploads/catalog/<?=$this->catalog_full['id']?>/small/<?=$oPhoto['img']?>" src="/images/uploads/catalog/<?=$this->catalog_full['id']?>/small/<?=$oPhoto['img']?>" alt="Заголовок"></li>
                        <? endforeach;?>

                        <? if ($this->catalog_full['video_key']!=''):?>
                        <li class="video" data-video='<iframe width="560" height="315" src="https://www.youtube.com/embed/<?=$this->catalog_full['video_key']?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>'>
                            <img data-big="<?= $this->catalog_full['img']; ?>" src="<?= $this->catalog_full['img']; ?>" alt="<?php echo $this->catalog_full['title']; ?>">
                        </li>
                        <? endif;?>
                    </ul>
                    <div id="video_popup" class="popup_text" style="top:50px;bottom:auto;display:none">
                        <div class="close" style="right:10px;top:10px">&times;</div>
                        <br>
                        <div class="ta_center video_content"></div>
                    </div>
                </div>

                <style>
                .cardImageSlider .bx-pager.bx-default-pager {
                    display: none;
                }
                .cardImageSlider .bx-wrapper {                    
                    border: 0;
                    box-shadow: none;
                }
                .cardImageSlider {
                    padding: 0 18px;
                    position: relative;
                    top: -10px;
                    margin-bottom: -10px;
                }
                .cardImageSlider .bx-wrapper .bx-controls-direction a {
                    height: 22px;
                    margin-top: -11px;
                    outline: 0 none;
                    position: absolute;
                    text-indent: -9999px;
                    top: 50%;
                    width: 10px;
                    z-index: 9;
                    -webkit-transition: none;
                    -moz-transition: none;
                    -ms-transition: none;
                    -o-transition: none;
                    transition: none;
                }
                .cardImageSlider .bx-wrapper .bx-prev {
                    background: url("/templates/lib/bxslider-4/dist/images/arrows.png") no-repeat scroll -21px -35px;
                    left: -25px;
                }
                .cardImageSlider .bx-wrapper .bx-prev:hover, .cardImageSlider .bx-wrapper .bx-prev:focus {
                    background: url("/templates/lib/bxslider-4/dist/images/arrows.png") no-repeat scroll -142px -37px;
                }
                .cardImageSlider .bx-wrapper .bx-next {
                    background: url("/templates/lib/bxslider-4/dist/images/arrows.png") no-repeat scroll -42px -35px;
                    right: -20px;
                    left: auto;
                }
                .cardImageSlider .bx-wrapper .bx-next:hover, .cardImageSlider .bx-wrapper .bx-next:focus {
                    background: url("/templates/lib/bxslider-4/dist/images/arrows.png") no-repeat scroll -163px -37px;
                }
                .cardImageSlider .bx-wrapper li img {
                    cursor: pointer;
                }
                .cardImageSlider li.video::after {
                    display: block;
                    content: '';
                    position: absolute;
                    width: 32px;
                    height: 26px;
                    background: url(/templates/images/icon-play.png) -24px -5px no-repeat;
                    z-index: 10;
                    left: 50%;
                    top: 50%;
                    margin-left: -16px;
                    margin-top: -13px;
                }
                .cardImageSlider li {
                    position: relative;
                    cursor: pointer;
                }
                </style>
                
                <link rel="stylesheet" href="/templates/lib/bxslider-4/dist/jquery.bxslider.min.css">
                <script src="/templates/lib/bxslider-4/dist/jquery.bxslider.min.js"></script>
                <script>
                $(function(){
                    $('.cardImageSlider-js').bxSlider({
                        autoHover: true,
                        minSlides: 2,
                        maxSlides: 2,
                        slideWidth: 120,
                        slideMargin: 9,
                        moveSlides: 1,
                        speed: 600,
                        auto: true,
                        useCSS: true,
                    })
                    $(document).on('click', '.cardImageSlider-js li', function(e){                    
                        var 
                            elem = $(this).find('img'),
                            type = (elem.parent().hasClass('video'))?'video':'img',
                            big = elem.data('big'),
                            src = $('.aromat_img img')

                        if (type == 'video') {

                            var popup = $('#video_popup').clone()
                            popup.addClass('second')
                            popup.find('.video_content').html(elem.parent().data('video'))
                            $('body').append(popup)
                            popup.show()
                            $('body').append('<div class="popup_overlay"></div>')                                                    

                        }
                        if (type == 'img') {
                            src.attr('src',big)
                        }
                    })

                    $(document).on('click', '#video_popup .close', function(e){
                        $('#video_popup.second').remove()
                        $('.popup_overlay').remove()
                    })
                })
                </script>





            <? } else { ?>
                <div class="aromat_img mb30">
                    <img id="product_cat_<?php echo $this->catalog_full['id_catalog']; ?>" src="<?= $this->catalog_full['img']; ?>" alt="<?php echo $this->catalog_full['title']; ?>"/>
                </div>
            <? } ?>

            <div style="text-align: center;margin-top: 10px;font-weight: bold;font-size: 14px;color: rgb(239, 135, 135)">Код товара: <?=str_replace('PF','',$this->catalog_full['articul'])?></div>


        </div>

        <div class="col-xs-12 col-sm-8">
            <div class="listing_sort mobile">
                <div class="top">
                    <span class="sort_title d_inline-block mr25">Рейтинг:</span>

                    <div class="stars d_inline-block mr30 relative" style="top:6px">
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_a"></div>
                        <div class="star_ua"></div>
                    </div>
                </div>
                <span class="sort_lh18 ml30 mr10 d_inline-block"><span class="br_line">Ароматы </span>в наличии</span>
                <input type="checkbox" class="filter_aromats"/>
                <span class="ml10 sort_lh18 d_inline-block"><span class="br_line">Все</span> ароматы</span>
            </div>
            <?=$this->render('content_catalog_full/_aromats.php')?>


<!--
            <? if (isset($this->is_order)) { ?>
                <div class="ta_center mb40 hide_979"><a href="/production/<?=$this->brend_url?>/<?=$this->catalog_full['url']?>/" class="btn2 round">Посмотреть все варианты и цены для <?= $this->catalog_full['title'] ?></a></div>
            <? } ?>
-->

        </div>
    </div>
	<div class="middle_cart_product">
		<ul class="plus_wrap wide">
                <li class="i1">
                    <a>
                        <i></i>

                        <div>
                            <span class="bold">Оригинальная</span> парфюмерия
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>В ассортименте нашего магазина представлена только оригинальная парфюмерия! Без исключений! В разделе "Качество и гарантии" Вы можете ознакомиться с примерами сертификатов и деклараций соответствия.</noindex>
                    </div>
                </li>
                <li class="i2">
                    <a>
                        <i></i>

                        <div>
                            Широкий <span class="bold">ассортимент</span>. <br>
                            Более 19000 товаров
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>Колоссальный ассортимент нашего магазина очередной раз подтверждает оригинальное происхождение продукции. У нас есть все - духи, туалетная вода, дезодоранты, гели для душа, крема для тела, лосьоны, бальзамы, шампуни, подарочные наборы.</noindex>
                    </div>
                </li>
                <li class="i3">
                    <a>
                        <i></i>

                        <div>
                            <span class="bold">Официальные</span> дистрибьюторы<br>
                            и надежные поставщики
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>Мы более 8 лет сотрудничаем с официальными дистрибьюторами и другими надежными поставщиками парфюмерии на территории РФ.</noindex>
                    </div>
                </li>
                <li class="i4">
                    <a>
                        <i></i>

                        <div>
                            Легкий <span class="bold">возврат</span> денег
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>В течение 14 дней с момента получения парфюмерии Вы можете вернуть или обменять товар с учетом сохранения всех товарных ценностей.</noindex>
                    </div>
                </li>
                <li class="i5">
                    <a>
                        <i></i>

                        <div>
                            Доставка <span class="bold">в течение суток</span> <br>
                            в 20 городах России
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>В 20 городах России доставка возможна уже на следующий рабочий день после заказа! Москва, Санкт-Петербург, Белгород, Брянск, Владимир, Вологда, Иваново, Калуга, Кострома, Нижний Новгород, Орел, Рязань, Смоленск, Тамбов, Тверь, Тула, Ярославль и др.</noindex>
                    </div>
                </li>
                <li class="i6">
                    <a>
                        <i></i>

                        <div>
                            Более <span class="bold">300 пунктов</span> самовывоза по России
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>По всей территории России свои заказы Вы можете получать в пунктах самовывоза. У нас более 300 пунктов, и их список постоянно расширяется.</noindex>
                    </div>
                </li>
                <li class="i7">
                    <a>
                        <i></i>

                        <div>
                            Лучшие цены и накопительная<br>
                            <span class="bold">система скидок</span>
                        </div>
                    </a>

                    <div class="li_popup">
                        <noindex>Мы нацелены на долгосрочные отношения с нашими клиентами, поэтому всегда держим самые лучшие цены. А накопительная система скидок дает возможность совершать покупки с выгодой до 15%.</noindex>
                    </div>
                </li>
        </ul>
			<script>
                $(function () {
                    $('.plus_wrap.sb .li_popup').each(function (index, item) {
                        var
                            e = $(item),
                            e_h = e.outerHeight() / 2

                        e.css('margin-top', -e_h)

                    })
                })
            </script>
		<div class="middle_cart_product_description">
            <?= $this->catalog_full['text'] ?>
        </div>
	</div>
	<div class="rees46 recommend similar"></div>
<style>
.article-thumb {
    width: 100%;
    margin-bottom: 20px;
}
.img-responsive, .thumbnail > img, .thumbnail a > img, .carousel-inner > .item > img, .carousel-inner > .item > a > img {
    display: block;
    max-width: 100%;
    height: auto;
}
.at__caption {
    padding: 15px 10px;
    border-bottom: 1px solid #f0f0f0;
}
.at__caption span {
    font-size: 14px;
    color: #abaaaa;
    font-style: italic;
}
.embed-responsive {
    position: relative;
    display: block;
    height: 0;
    padding: 0;
    overflow: hidden;
    margin-top: 20px !important;	
    margin-bottom: 20px !important;
}
.embed-responsive-16by9 {
    padding-bottom: 56.25%;
}
.embed-responsive .embed-responsive-item, .embed-responsive iframe, .embed-responsive embed, .embed-responsive object, .embed-responsive video {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 100%;
    border: 0;
}
.big_text{
    line-height: 28px;
	font-size: 16px;
    margin-bottom: 40px;	
}
.big_text a{
    text-decoration: underline;
}
.big_text ul {
    padding: 10px 10px 10px 35px;
    font-size: 16px;
    line-height: 22px;
	list-style: square outside;
}
.big_text h2 {
    padding: 30px 0px 20px 0px;
    line-height: 35px;
}
</style>
	
    <div class="big_text">
        <?= $this->catalog_full['text2'] ?>
    </div>

	

</div>



<?=$this->render('content_catalog_full/_tabs.php')?>

<div class="mb20">
    <? // require('partials/links_preview_horizontal.php')?>
</div>
<!--
<div class="mb20">
    <?php //echo $this->body_text; ?>
</div>
-->
<div id="no_aromat_popup" style="display:none">
    <span class="close">&times;</span>

    <form action="" method="post">
        <div class="mb20"><input type="text" name="name" placeholder="Имя"/></div>
        <div class="mb20"><input type="text" name="phone" placeholder="Телефон"/></div>
        <div class="mb20"><input type="text" name="email" placeholder="e-mail"/></div>
        <input type="hidden" name="articul" id="modal_articul">
        <button name="waitlist" value="1" class="btn2">Сообщить о поступлении</button>
    </form>
</div>
    