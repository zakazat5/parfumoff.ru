<div class="bread_crumps">
    <a href="/">Главная</a>
    <span>&gt;</span>
    <a href="/production/">Торговые дома</a>
    <span>&gt;</span>
    На букву <strong><?php echo strtoupper($this->liter); ?></strong>
</div>

<div class="brand__list">
    <div class="brand__list-item">
        <div class="brand__letter"><?php echo strtoupper($this->liter); ?></div>
        <div class="brand__letter-list clearfix">
            <? $chunk = array_chunk($this->brend_item,round(count($this->brend_item)/4))?>

            <? foreach ($chunk as $chunk_item):?>
                <div class="brand__letter-item col-md-3 col-xs-6">
                    <? foreach ($chunk_item as $brend_item): ?>
                        <p><a href="/production/<?php echo $brend_item['url']; ?>/">
                            <?php if ((int)$brend_item['is_top'] === 1): ?>
                                <strong><i class="icon icon-star"></i><?php echo $brend_item['title']; ?></strong>
                            <?php else: ?>
                                <?php echo $brend_item['title']; ?>
                            <?php endif; ?>
                        </a>
                        </p>
                    <? endforeach;?>
                </div>

            <? endforeach;?>

        </div>
    </div>
</div>