
<div class="bread_crumps">
	<?php if ($this->canon_name2 != "men" && $this->canon_name2 != "woman") { ?>
		<a href="/">Главная</a>
		<span>&gt;</span>
		Туалетная вода
	<?php } elseif ($this->canon_name2 == "men") { ?>
		<a href="/">Главная</a>
		<span>&gt;</span>
		<a href="/tualetnaya-voda/">Туалетная вода</a>
		<span>&gt;</span>
		Туалетная вода для мужчин
	<?php } elseif ($this->canon_name2 == "woman") { ?>
		<a href="/">Главная</a>
		<span>&gt;</span>
		<a href="/tualetnaya-voda/">Туалетная вода</a>
		<span>&gt;</span>
		Туалетная вода для женщин
	<?php } ?>
</div>


<div class="brand_filter">
    <? require_once('partials/filter_html.php'); ?>
</div>
<? require_once('partials/sort.php'); ?>

<?php if (($this->canon_name2 != "men" && $this->canon_name2 != "woman") || (isset($this->catalog_item_f) && isset($this->catalog_item_m))) { ?>
    <div class="listing_title">
        <h1 class="listing_title__item">Туалетная вода</h1>
        <div class="filter_hide">
            <a class="listing_title__item" href="/tualetnaya-voda/woman/">Женские ароматы</a>
            <a class="listing_title__item" href="/tualetnaya-voda/men/">Мужские ароматы</a>
			<a class="listing_title__item disabled active">Все ароматы</a>
        </div>
		<h2 class="h2_parfum_brend">Туалетная вода для женщин</h2>
    </div>
	<?php if (isset($this->catalog_item_f)) { ?>
    <div class="row">
        <div class="col-md-9 col-xs-12 n-col-m-4">
            <div class="items">
				<?php $i = 1;
					foreach ($this->catalog_item_f as $catalog_item) {
						include 'partials/catalog_item.php';
						$i++;
					} ?>
			</div>
        </div>
    </div>
	<?php } ?>
	<?php if (isset($this->catalog_item_m)) { ?>
	<div class="filter_hide">
        <div class="listing_title">
			<h2 class="h2_parfum_brend">Туалетная вода для мужчин</h2>
        </div>
        <div class="row">
            <div class="col-md-9 col-xs-12 n-col-m-4">
                <div class="items">
                    <?php $i = 1;
                    foreach ($this->catalog_item_m as $catalog_item): ?>
                        <? include 'partials/catalog_item.php'; ?>
                    <?php $i++; endforeach; ?>
                </div>
            </div>
        </div>
	</div>	
    <?php } ?>
<?php } elseif ($this->canon_name2 == "men") { ?>
	<div class="listing_title">
        <h1 class="listing_title__item">Туалетная вода для мужчин</h1>
        <div class="filter_hide">
            <a class="listing_title__item" href="/tualetnaya-voda/woman/">Женские ароматы</a>
            <a class="listing_title__item disabled active">Мужские ароматы</a>
			<a class="listing_title__item" href="/tualetnaya-voda/">Все ароматы</a>
        </div>
    </div>
	<?php if (isset($this->catalog_item_m)) { ?>
    <div class="row">
        <div class="col-md-9 col-xs-12 n-col-m-4">
            <div class="items">
				<?php $i = 1;
					foreach ($this->catalog_item_m as $catalog_item) {
						include 'partials/catalog_item.php';
						$i++;
					} ?>
			</div>
        </div>
    </div>	
	<?php } ?>
<?php } elseif ($this->canon_name2 == "woman") { ?>
	<div class="listing_title">
        <h1 class="listing_title__item">Туалетная вода для женщин</h1>
        <div class="filter_hide">
            <a class="listing_title__item disabled active">Женские ароматы</a>
            <a class="listing_title__item" href="/tualetnaya-voda/men/">Мужские ароматы</a>
			<a class="listing_title__item" href="/tualetnaya-voda/">Все ароматы</a>
        </div>
    </div>
	<?php if (isset($this->catalog_item_f)) { ?>
    <div class="row">
        <div class="col-md-9 col-xs-12 n-col-m-4">
            <div class="items">
				<?php $i = 1;
					foreach ($this->catalog_item_f as $catalog_item) {
						include 'partials/catalog_item.php';
						$i++;
					} ?>
			</div>
        </div>
    </div>	
	<?php } ?>
<?php } ?>


<!--
<div class="rees46 recommend recently_viewed"></div>
-->
<? //require('partials/links_preview_horizontal.php') ?>

<div class="brands">
    <h3 class="brands__header">Все бренды</h3>
		<div class="brands__alphabet">
			<?php foreach ($this->aBrands['letters'] as $letter => $groupItem) { ?>
				<a class="brands__alphabet-letter <?= ($letter == 'A') ? 'brands__alphabet-letter--active' : '' ?>" href=""><?= $letter ?></a>
			<?php } ?>
			<?php foreach ($this->aBrands['numbers'] as $letter => $groupItem) { ?>
				<a class="brands__alphabet-letter <?= ($letter == 'A') ? 'brands__alphabet-letter--active' : '' ?>" href=""><?= $letter ?></a>
			<?php } ?>	
		</div>
       	<?php foreach ($this->aBrands['letters'] as $letter => $groupItem) { ?>
			<div  class="brands__list" data-letter="<?= $letter ?>" <?=($letter!='A')?'style="display:none"':''?>>
				<?php foreach ($groupItem as $item) { ?>
					<a class="brands__item" href="/production/<?=$item['url']?>/"><?=($item['title'])?><?php if (trim($item['title_1']) != "") { echo " / " . $item['title_1']; } else { echo ""; } ?></a>
				<?php } ?>
			</div>
		<?php } ?>
		<?php foreach ($this->aBrands['numbers'] as $letter => $groupItem) { ?>
			<div  class="brands__list" data-letter="<?= $letter ?>" style="display:none">
				<?php foreach ($groupItem as $item) { ?>
					<a class="brands__item" href="/production/<?=$item['url']?>/"><?=($item['title'])?><?php if (trim($item['title_1']) != "") { echo " / " . $item['title_1']; } else { echo ""; } ?></a>
				<?php } ?>
			</div>
		<?php } ?>
</div>

<!--
<?php //if ($this->brend['anot']): ?>
    <div class="brand_info">
        <div class="brand_info__text">
            <?php //echo $this->brend['anot']; ?>
        </div>
    </div>
<?php //endif; ?>

<?php //if ($this->body_text) { ?>
	<div class="block_description_info">
		<div class="description_info_name">
			<?php //echo $this->brend_title; ?>
		</div>
		<div class="description_info_text">
			<?php //echo $this->body_text; ?>
		</div>
	</div>
<?php //} ?>
 
<?php //if ($this->seo_description) { ?>
	<div class="block_seo_description_brend">
		<?php //echo $this->seo_description; ?>
	</div>
<?php //} ?>
-->

<script>
    $(function () {
        if ($('.items .item').not('.outstock').length == 0) {
            console.log(234)
        }
        else {
            console.log('sfd')

        }
    })
</script>
<script>
        $(function () {
            $('.brands__alphabet a').click(function (e) {
                e.preventDefault()

                var elem = $(this),
                    letter = elem.text(),
                    letter_items = elem.parents('.brands__alphabet').next()

                elem.parents('.brands__alphabet').find('.brands__alphabet-letter').removeClass('brands__alphabet-letter--active')
                elem.addClass('brands__alphabet-letter--active')

                letter_items.parent().find('.brands__list').hide()
                letter_items.parent().find('.brands__list[data-letter=' + letter + ']').show()

            })
        })
</script>