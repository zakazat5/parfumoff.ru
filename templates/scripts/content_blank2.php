<?php require_once(__DIR__ . '/../../includes/geo_domain.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo geo_replace_utf8($city_utf8, $this->main['meta_title']);?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="description" content="<?php echo geo_replace_utf8($city_utf8, $this->main['meta_description']);?>" />
    <meta name="apple-mobile-webapp-capable" content="yes"/>

    <link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <? if ($this->main['noindex']==true) echo '<meta name="robots" content="noindex"/>';?>
    <? if ($this->main['og:title']!='') echo '<meta property="og:title" content="'.geo_replace_utf8($city_utf8, $this->main['og:title']).'" /> '?>
    <? if ($this->main['og:image']!='') echo '<meta property="og:image" content="'.$this->main['og:image'].'" /> '?>
    <? if ($this->main['og:description']!='') echo '<meta property="og:description" content="'.geo_replace_utf8($city_utf8, $this->main['og:description']).'" /> '?>
    <? if ($this->main['og:url']!='') echo '<meta property="og:url" content="'.$this->main['og:url'].'" /> '?>


    <meta name="viewport" content="width=device-width,minimum-scale=1.0,maximum-scale=1.0, initial-scale=1.0,">
    <meta name="google-site-verification" content="iIenX44wjOQwSqAas3KPi-VGArn1bbHhl8JSwnaqqqs" />
    <meta name='yandex-verification' content='409ef96cc5640923' />

    <link rel="stylesheet" href="/templates/css/reset.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/grid.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/style.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/retina.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/responsive.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/cart.css" type="text/css"  />
    <link rel="stylesheet" href="/templates/css/suggestions.css" type="text/css" />
    <link rel="stylesheet" href="/templates/css/button.css" type="text/css" />
    <link rel="stylesheet" href="/templates/css/bootstrap/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/templates/js/bootstrap-select_new/bootstrap-select.css">
    <link rel="stylesheet" href="/templates/css/suggestions.css" type="text/css" />
    <link rel="stylesheet" href="/templates/lib/star-rating/jquery.rating.css" type="text/css" />

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

    <script src="/templates/lib/iphone_checkbox/iphone_checkbox.js"></script>
    <link rel="stylesheet" href="/templates/lib/iphone_checkbox/style.css" />
    <script src="/templates/js/cart.js"></script>
    <script type="text/javascript" src="/templates/js/scripts.js"></script>
    <script type="text/javascript" src="/templates/js/filter_v2.js"></script>
    <script type="text/javascript" src="/templates/lib/star-rating/jquery.rating.pack.js"></script>

    <link href="/templates/lib/select2-3.5.1/select2.css" rel="stylesheet"/>
    <script src="/templates/lib/select2-3.5.1/select2.js"></script>

    <script type="text/javascript" src="/templates/js/dadata/suggestions-jquery-4.1.min.js"></script>
    <script type="text/javascript" src="/templates/js/dadata.js"></script>
    <script type="text/javascript" src="/templates/js/suggest_cart.js"></script>
    <script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>
    <script src="/templates/js/bootstrap-select_new/bootstrap-select.js"></script>
    <script src="/templates/js/validate/jquery.validate.js"></script>
    <script src="/templates/js/validate/localization/messages_ru.min.js"></script>
    <script src="/templates/js/bootstrap.js"></script>
    <script src="/templates/js/jquery_lazyload/jquery.lazyload.js?v=1.9.1"></script>
    <script src="http://vk.com/js/api/openapi.js" type="text/javascript"></script>
    <? if (isset($this->main['canonical'])):?>
        <link rel="canonical" href="http://<?php echo $host; ?>/<?=$this->main['canonical']?>">
    <? endif;?>

    <link rel="stylesheet" href="/templates/js/jquery.scrollbar/jquery.scrollbar.css" />
    <script src="/templates/js/jquery.scrollbar/jquery.scrollbar.js"></script>


    <link rel="stylesheet" href="/templates/js/arcticmodal/jquery.arcticmodal-0.3.css?v=1" type="text/css" media="screen" />
    <script src="/templates/js/arcticmodal/jquery.arcticmodal-0.3.min.js?v=1"></script>



    <script>
        var region_name = '<?=$this->main['location']['region_name']?>';
        var city_name = '<?=$this->main['location']['city_name']?>';
    </script>

    <? require_once('partials/comagic.php')?>
	
<script type="text/javascript">

var _alloka = {
objects: {
'614dbe6096cbe04d': {
block_class: 'phone_alloka'
}
},
trackable_source_types: ["utm"],
url_params: {'utm_source': ['yad', 'google_RF', 'YandexDirect', 'google_MSK']},
last_source: false,
use_geo: true
 };
</script>
<script src="https://analytics.alloka.ru/v4/alloka.js" type="text/javascript"></script>

</head>
<body>


<div class="container">
    <?php echo geo_replace_utf8($city_utf8, $this->main['content']); ?>
</div>
<footer>
    <div id="top_footer">
        <div class="container">
            <div id="bottom_phones">
                <div class="fs14 head pt10 mb10">Сделайте заказ по телефону:</div>
                <a href="tel:<?=$city_utf8['phone']?>"
                   class="phone d_block mobile_phone fs24 lite mb5 phone_alloka"><?=$city_utf8['phone']?></a>
                <br>
            </div>
            <div id="ya_markt">
                <img src="/templates/images/ya_markt.jpg" alt="" />
            </div>
            <div id="bottom_nav">
                <ul class="fs14 pay">
                    <li><a href="/oplata/">Мы принимаем:</a></li>
                    <li><img style="margin-top: 5px;" src="/templates/images/pay.png"></li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

</footer>



</body>
</html>