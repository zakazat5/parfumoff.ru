<div class="content__top">
	<div class="categories">
        <div class="categories__header">Категории</div>
		<?php if (isset($this->category_items)) { ?>
			<div class="categories__list">
				<?php foreach ($this->category_items as $category_item) { ?>
					<a class="categories__link<?php if ($category_item['id'] == 14) { echo ' category_sale_menu'; } if ($category_item['active'] === 1) { echo ' categories__link__active'; } ?>" href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/<?php echo $category_item['url']; ?>"><?php echo $category_item['name']; ?></a>
				<?php } ?>
			</div>
		<?php } ?>
    </div>
	<div class="content__catalog-top">
		<div class="header__category-top">
			<div class="name__category-top name__category-top--main">
				<h1 class="content__header"><?php echo $this->category['name']; ?></h1>
			</div>
			<div class="banner_free_ship">
				<img class="banner_free_ship_img" src="/templates/images/parfumoff1-1.png" alt="">
			</div>
		</div>
		<div class="category_filter_m_f"> 
			<a class="category_filter_f_link <?php if ($this->filter_pol == "f") { echo 'category_filter_active'; } ?>" href="/<?php echo $this->page_url; ?>/filter/pol/f/">для женщин</a>&nbsp;/&nbsp;<a class="category_filter_m_link <?php if ($this->filter_pol == "m") { echo 'category_filter_active'; } ?>" href="/<?php echo $this->page_url; ?>/filter/pol/m/">для мужчин</a>
		</div>
		<div class="content__catalog-top-items">
			<?php if (isset($this->catalog_items)) { 
				foreach ($this->catalog_items as $key=>$catalog_item) {
					if ($key <= 2) {
						include 'partials/catalog_item.php'; 
					}
				} 
			} ?>
		</div>
	</div>
</div>
	<div class="catalog clearfix">
		<?php if (isset($this->catalog_items)) { 
			foreach ($this->catalog_items as $key=>$catalog_item) {
				if ($key > 2) {
					include 'partials/catalog_item.php'; 
				}
			} 
		} ?>
	</div>
	<?php if ($this->page_max > 1) { ?>
		<?php if ($this->page_current >= 3) {
			$start_pagin = -2;
		} else {
			/*
			if ($this->page_current == 1) {
				$start_pagin = 0;
			} else {
				*/
				$start_pagin = 1 - $this->page_current;
			//}
		}?>
		<nav class="paginator_category ">
			<ul class="paginator_category_ul">
				<?php if ($this->page_current >= 4) { ?>
					<li class="paginator_category_li">
					<a class="paginator_category_link" href="/<?php echo $this->page_url; ?>/<?php echo $this->filter_url; ?>">
						<<
					</a>
					</li>
					<li class="paginator_category_li">
					<?php if ($this->page_current == 4) { ?>
						<a class="paginator_category_link" href="/<?php echo $this->page_url; ?>/<?php echo $this->filter_url; ?>">
					<?php } else { ?>
						<a class="paginator_category_link" href="/<?php echo $this->page_url; ?>/page/<?php echo $this->page_current - 1; ?>/<?php echo $this->filter_url; ?>">
					<?php } ?>
						<
						</a>
					</li>
				<?php } ?>
				<?php while (($start_pagin < 3) && (($this->page_current + $start_pagin) <= $this->page_max)) { ?>
					<li class="paginator_category_li">
						<?php if ($start_pagin == 0) { ?>
							<div class="paginator_category_link paginator_category_link_active">
						<?php } else { ?>
							<?php if  (($this->page_current + $start_pagin) == 1) { ?>
								<a class="paginator_category_link" href="/<?php echo $this->page_url; ?>/<?php echo $this->filter_url; ?>">
							<?php } else { ?>
										<a class="paginator_category_link" href="/<?php echo $this->page_url; ?>/page/<?php echo ($this->page_current + $start_pagin); ?>/<?php echo $this->filter_url; ?>">
							<?php } ?>
						<?php } ?>
				
							<?php echo ($this->page_current + $start_pagin); ?>
							
						<?php if ($start_pagin == 0) { ?>	
							</div>
						<?php } else { ?>
								</a>
						<?php } ?>
					</li>
				<?php  $start_pagin++;
				} ?>
				<?php if ($this->page_current <= $this->page_max - 3) { ?>
					<li class="paginator_category_li">
					<a class="paginator_category_link" href="/<?php echo $this->page_url; ?>/page/<?php echo $this->page_current + 1; ?>/<?php echo $this->filter_url; ?>">
						>
					</a>
					</li>
					<li class="paginator_category_li">
					<a class="paginator_category_link" href="/<?php echo $this->page_url; ?>/page/<?php echo $this->page_max; ?>/<?php echo $this->filter_url; ?>">
						>>
					</a>
					</li>
				<?php } ?>
			</ul>
		</nav>
	<?php } ?>
	<div class="rees46 recommend recently_viewed"></div>
	<?php if ($this->category['description']) { ?>
		<div class="block_description_info">
			<div class="description_info_name">
				<?php echo $this->category['name']; ?>
			</div>
			<div class="description_info_text">
				<?php echo $this->category['description']; ?>
			</div>
		</div>
	<?php } ?>
<? $suffix = '#'; ?>
		
