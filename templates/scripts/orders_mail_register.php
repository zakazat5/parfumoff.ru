<?php require_once(__DIR__ . '/../../includes/geo_domain.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="title" content="" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
</head>

<body>

<div id="wrapper">
    <div id="header">
    </div><!-- #header-->


    <div id="content">

        <div style="width:600px">

            <table>
                <tr>
                    <td valign="bottom" align="left">
                        <h2 style="font: 18px/18px Tahoma;font-weight:normal">Здравствуйте, <?php echo $this->data['fio'];?>!</h2>
                        <font style="font: 12px/18px Tahoma">Благодарим вас за выбор интернет-магазина <a style="font: 12px/18px Tahoma;color:black" target="_blank" href="http://<?=$host?>/">Parfumoff.ru</a></font><br>

                        <br />
                        <font color="#942A5E">
                            <b style="font: 12px/18px Tahoma;font-weight:bold;color:#942A5E;">
                                Вы зарегистрированы.<br />
                            </b>
                        </font>
                        <br />
                        <font style="font: 12px/18px Tahoma">
                            ваш Логин: <strong><?php echo $this->data['email'];?></strong><br />
                            ваш Пароль: <strong><?php echo $this->data['passw'];?></strong><br />
                            вход на сайт на странице: <a style="font: 12px/18px Tahoma;color:black" target="_blank" href="http://<?=$host?>/users/login/">Parfumoff.ru</a>
                        </font><br>

                    </td>
                </tr>
            </table>
            <br>


            <div style="clear:both;font:12px/18px Tahoma"></div><br /><br />
            телефон для справок: <strong><?=$city_utf8['phone']?></strong>

        </div>

    </div><!-- #content-->




    <div id="footer"></div><!-- #footer --></div><!-- #wrapper -->
</body>
</html>