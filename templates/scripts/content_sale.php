<div class="bread_crumps">
    <a href="/">Главная</a>
    <span>&gt;</span>
    <a href="/catalog/">Торговые дома</a>
    <span>&gt;</span>
    Распродажа
</div>

<div class="lines">
    <div id="first" class="act">Распродажа</div>
</div>

<div class="cat_wrap first">
    <?php foreach ($this->catalog_item as $catalog_item) { ?>

        <div class="comment_user_wrap">
            <div class="img_wr">
                <a href="<?php echo $catalog_item['url']; ?>"><img src="<?php echo $catalog_item['img']; ?>" width="70" alt="" title="" /></a>
            </div>

            <div class="desc_wr">

                <a class="title georgia_head" href="<?php echo $catalog_item['url']; ?>"><?php echo $catalog_item['brend_title']; ?>

                    <span class="purple"><?php echo $catalog_item['title']; ?></span></a>

                <? if ($catalog_item['pol'] == 'M') { ?>
                    <div style="font-size:12px">мужской</div>
                <? } ?>
                <? if ($catalog_item['pol'] == 'F') { ?>
                    <div style="font-size:12px">женский</div>
                <? } ?>

                <div class="text">

                    <?php if ( $catalog_item['tag']!='' ){ ?>
                        <div class="notes">
                            <?php foreach ($catalog_item['tag'] as $catalog_item_tag) { ?>
                                <a href="/tag/<?php echo $catalog_item_tag['url']; ?>/"><?php echo $catalog_item_tag['title']; ?></a>,
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <p><?php echo $catalog_item['anot']; ?></p>

                    <div class="undercomment" style="margin-top:3px;">

                        <? if($this->is_login==1){ ?>
                            <a id="<?php echo $catalog_item['id']; ?>" class="izbr bloknot <? if($catalog_item['is_note']) echo 'active'; ?>" style="cursor:pointer;">Добавить в избранное</a>
                        <? } ?>
                        <a style="margin-left:0" href="<?php echo $catalog_item['url']; ?>" class="more">Подбробнее</a>
                        <div class="clear"></div>

                    </div>
                </div>
            </div>
        </div>

        <?php //include("content_catalog_item.php");?>
    <?php } ?>


</div>



<div class="clear"></div>


<? echo $this->p['pages'];?>