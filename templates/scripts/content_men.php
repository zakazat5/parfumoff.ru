<div class="brand_filter">
    <? require_once('partials/filter_html.php');?>
</div>

<?php if (isset($this->catalog_item_m)) { ?>
    <a name="man"></a>
    <div class="listing_title">
		<h1 class="listing_title__item">Мужские духи</h1>
        <a href="/men/" class="listing_title__item disabled active">Мужские ароматы</a>
        <a href="/woman/" class="listing_title__item ">Женские ароматы</a>
    </div>

<!--
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="items main_page">
			-->
<div class="catalog clearfix">			
                <?php $i=1;foreach ($this->catalog_item_m as $catalog_item): ?>
                    <? include 'partials/catalog_item.php';?>
                    <?php $i++; endforeach; ?>
</div>			
<!--					
            </div>
        </div>
    </div>
	-->
<? } ?>
<nav class="paginator_category ">
	<ul class="paginator_category_ul">
		<?php if ($this->first_page_pointers_view) { ?>
			<li class="paginator_category_li">
				<a class="paginator_category_link" href="<?php echo $this->page_url; ?>">
					<<
				</a>
			</li>
			<li class="paginator_category_li">
				<a class="paginator_category_link" href="<?php echo $this->page_url; ?>?page<?php echo $this->prev_page; ?>">
					<
				</a>
			</li>
		<?php } ?>
		<?php if (!empty($this->numbered_page)) { ?>
			<?php foreach ($this->numbered_page as $page_item) { ?>
				<li class="paginator_category_li">
					<a class="paginator_category_link <?php if ($page_item == $this->page_number) { echo "paginator_category_link_active"; } ?>" href="<?php echo $this->page_url; ?>?page<?php echo $page_item; ?>">
						<?php echo $page_item; ?>
					</a>
				</li>
			<?php } ?>
		<?php } ?>
		<?php if ($this->last_page_pointers_view) { ?>
			<li class="paginator_category_li">
				<a class="paginator_category_link" href="<?php echo $this->page_url; ?>?page<?php echo $this->next_page; ?>">
					>
				</a>
			</li>
			<li class="paginator_category_li">
				<a class="paginator_category_link" href="<?php echo $this->page_url; ?>?page<?php echo $this->last_page; ?>">
					>>
				</a>
			</li>
		<?php } ?>
	</ul>
</nav>

    <div class="clear" style="height:50px"></div>
<? $suffix = 'men/'; ?>
<? require_once('partials/brands_list.php'); ?>