<? require_once('partials/filter_html.php');?>

<?php if (isset($this->catalog_items)) { ?>
    <div class="row">
        <? include 'partials/catalog_items.php';?>
    </div>
<? } ?>


<? if ($this->catalog_items instanceof Zend_Paginator): ?>
    <?= $this->paginationControl($this->catalog_items, 'Elastic', 'partials/paginator.php',['get'=>$this->get]); ?>
<? endif; ?>
