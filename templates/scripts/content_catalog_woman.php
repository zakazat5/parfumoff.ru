<div class="bread_crumps">
    <a href="/">Главная</a>
    <span>&gt;</span>
    <a href="/production/">Торговые дома</a>
    <span>&gt;</span>
	<a href="/production/<?= $this->brend_url; ?>/"> <?= $this->brend_title; ?></a>
	<span>&gt;</span>
	Женский парфюм
</div>


<div class="brand_filter">
    <? require_once('partials/filter_html.php'); ?>
</div>
<? require_once('partials/sort.php'); ?>

<?php if (isset($this->catalog_item_f)) { ?>
    <a name="woman"></a>
    <div class="listing_title">
        <h1 class="listing_title__item">Женские духи <?php echo $this->brend_title; ?></h1>
        <div class="filter_hide">
            <a class="listing_title__item disabled active">Женские ароматы</a>
            <a class="listing_title__item" href="/production/<?= $this->brend_url; ?>/men/">Мужские ароматы</a>
			<a class="listing_title__item" href="/production/<?= $this->brend_url; ?>/">Все ароматы</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 col-xs-12">
            <div class="items">
                <?php $i = 1;
                foreach ($this->catalog_item_f as $catalog_item): ?>
                    <? include 'partials/catalog_item.php'; ?>
                    <?php $i++; endforeach; ?>
            </div>
        </div>
        <div class="col-md-3 col-xs-12 relative hidden_480 hide_768">
            <?php if (isset($this->catalog_item_f)): ?>
				<h3>Все ароматы <?= $this->brend_title; ?> для женщин:</h3>
                <ul class="list-type1">
                    <?php foreach ($this->catalog_item_f as $catalog_item) { ?>
                        <?php if ($catalog_item['pol'] == 'F') { ?>
                            <li class="mb5">
                                <?php if (strtolower($catalog_item['url']) == $this->aromat_url) { ?>
                                    <?php echo $catalog_item['title']; ?>
                                <?php } else { ?>
                                    <a class="td_underline color_blue" href="<?php echo $catalog_item['url']; ?>">
                                        <?= ($catalog_item['is_top'] == 1) ? '<b>' : '' ?><?php echo $catalog_item['title']; ?><?= ($catalog_item['is_top'] == 1) ? '</b>' : '' ?>
                                    </a>
                                <?php } ?>
                            </li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
<? } else { ?>
	<div class="wrapper content clearfix">
		<br>
		<div class="news">
		<p>К сожалению, на данный момент женского парфюма <?= $this->brend_title; ?> нет в наличии. Возможно, Вы сможете подобрать подходящий парфюм на страницах:</p>
		<ul class="ul_brend">
			<li><a href="/woman/">Женские духи</a></li>
			<li><a href="/tualetnaya-voda/woman/">Женская туалетная вода</a></li>
		</ul>
		</div>    
	</div>
<?php } ?>

<div class="rees46 recommend recently_viewed"></div>

<? require('partials/links_preview_horizontal.php') ?>



<? if ($this->brend['anot']): ?>
    <div class="brand_info">
        <div class="brand_info__text">
            <?php echo $this->brend['anot']; ?>
        </div>
    </div>
<? endif; ?>

<?php if ($this->body_text) { ?>
	<div class="block_description_info">
		<div class="description_info_name">
			<?php echo $this->brend_title; ?>
		</div>
		<div class="description_info_text">
			<?php echo $this->body_text; ?>
		</div>
	</div>
<?php } ?>
 
<?php if ($this->seo_description && isset($this->catalog_item_f)) { ?>
	<div class="block_seo_description_brend">
		<?php echo $this->seo_description; ?>
	</div>
<?php } ?>
<script>
    $(function () {
        if ($('.items .item').not('.outstock').length == 0) {
            console.log(234)
        }
        else {
            console.log('sfd')

        }
    })
</script>
