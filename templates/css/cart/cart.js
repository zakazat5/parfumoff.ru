$(function(){

	$('.b-order__total .btn1').click(function(e){
		var
			elem = $(this),
			delivery_tr = $('.b-delivery__table .tr_delivery_item'),
			active_delivery = $('.b-delivery__table .tr_delivery_item.active_elem'),
			error_popup = $('#error_delivery_popup');

		if (!active_delivery.length) {
			e.preventDefault();
			delivery_tr.addClass('error_tr');
			error_popup.show()
		} else {
			delivery_tr.removeClass('error_tr');
			error_popup.hide();
		}
	});

	$('.b-delivery__table .cart_checkbox').on('click',function(){
		var
			elem = $(this),
			checkboxes = $('.b-delivery__table .cart_checkbox'),
			active_tr = elem.parents('tr'),
			tr = $('.b-delivery__table .tr_delivery_item'),
			error_popup = $('#error_delivery_popup'),
			full_tr = $('.b-delivery__table .full_tr'),
			full_tr_btn = $('.b-delivery__table .full_tr_btn');

		tr.removeClass('error_tr').removeClass('active_elem');
		active_tr.addClass('active_elem');
		checkboxes.removeClass('active');
		elem.addClass('active');
		error_popup.hide();
		full_tr.hide();
		full_tr_btn.hide();

		$('.b-delivery__table .b-delivery__self').hide();
		$('.b-delivery__self .btn2').removeClass('active').text('Выбрать');
		$('.b-delivery__self').removeClass('active');

		var cost = elem.parents('tr').find('.cost').text();
		if (cost){
			if (isNaN(parseInt(cost))) {
				$("#delivery_cost-text").text('уточняйте у оператора');
				$("#delivery_cost").val('');
				$('#courier_address_id').val('');
				$('#big_sum_all').text(parseInt($('#dSum').val()));
				$('.b-order__total .col-xs-7').each(function(){
					if ($(this).html() == 'Доставка:') {
						$(this).hide(0);
						$(this).next('.col-xs-5').hide(0);
					}
				});
			} else {
				$("#delivery_cost-text").text(parseInt(elem.parents('tr').find('.cost').text()));
				$("#delivery_cost").val(parseInt(elem.parents('tr').find('.cost').text()));
				$('#courier_address_id').val(elem.parents('tr').data('id'));
				$('#big_sum_all').text(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));
				$('.b-order__total .col-xs-7').each(function(){
					if ($(this).html() == 'Доставка:') {
						$(this).show(0);
						$(this).next('.col-xs-5').show(0);
					}
				});
			}
		}



		if (active_tr.attr('id') == 'courier__delivery') {
			$('#courier__delivery_full').removeClass('hide').show();
			$('#courier__delivery_full').next().removeClass('hide').show();
			$('#add_inf').val('');
			$('#delivery_type').val('Курьером');
		}

		if (active_tr.attr('id') == 'self__delivery_pvz') {
			$('.punkts').removeClass('active_el');
			$('.b-delivery__table .b-delivery__self_pvz').removeClass('hide').show();
			$('#add_info_row_pvz').removeClass('hide').show();
			$('.b-delivery__table #self__delivery_pvz').nextAll('.full_tr_self:first').removeClass('hide').show();
			if (ckt.setting.carriers['pvz'] !== undefined) {
				option = $('.b-delivery__table #self__delivery_pvz').next('.b-delivery__self_pvz').find('select option:selected');
				so = option.data('code');
				info = option.data('info');
				$('#self__delivery_pvz').addClass('active_el');
				$('#add_inf').val(info);
				$('#delivery_type').val('В ПВЗ');
				ckt.open_map('pvz');
			}
		}

		if (active_tr.attr('id') == 'self__delivery_postamat') {
			$('.punkts').removeClass('active_el');
			$('.b-delivery__table .b-delivery__self_postamat').removeClass('hide').show();
			$('#add_info_row_postamat').removeClass('hide').show();
			$('.b-delivery__table #self__delivery_postamat').nextAll('.full_tr_self:first').removeClass('hide').show();
			if (ckt.setting.carriers['postamat'] !== undefined) {
				option = $('.b-delivery__table #self__delivery_postamat').next('.b-delivery__self_postamat').find('select option:selected');
				so = option.data('code');
				info = option.data('info');
				$('#self__delivery_postamat').addClass('active_el');
				$('#add_inf').val(info);
				$('#delivery_type').val('В Постамат');
				ckt.open_map('postamat');
			}
		}

		if (active_tr.attr('id') == 'pochta__delivery') {
			$('#pochta__delivery_full').removeClass('hide').show();
			$('#pochta__delivery_full').next().removeClass('hide').show();
			$('#add_inf').val('');
			$('#delivery_type').val('Почтой РФ');
		}
	});


	$('.b-delivery__self .btn2').click(function(){
		var
			elem = $(this),
			elems = $('.b-delivery__self .btn2'),
			tr = $('.b-delivery__self')



		elems.removeClass('active').text('Выбрать');
		elem.addClass('active').text('Здесь');

		tr.removeClass('active');
		elem.parents('tr').addClass('active');

		$('.b-delivery__self').removeClass('warning_tr')
	})


});

function delivery_to_default() {
	var
		checkboxes = $('.b-delivery__table .cart_checkbox'),
		tr = $('.b-delivery__table .tr_delivery_item'),
		error_popup = $('#error_delivery_popup'),
		full_tr = $('.b-delivery__table .full_tr'),
		full_tr_btn = $('.b-delivery__table .full_tr_btn');

	tr.removeClass('error_tr').removeClass('active_elem');
	checkboxes.removeClass('active');
	error_popup.hide();
	full_tr.hide();
	full_tr_btn.hide();

	$('.b-delivery__table .b-delivery__self').hide();
	$('.b-delivery__self .btn2').removeClass('active').text('Выбрать');
	$('.b-delivery__self').removeClass('active');
}
