$('.wrapper>.container>.row').append('<div id="map" class="col-md-12" style="height:500px;"></div>');
ckt.before_carrier_ready = function(){
    $('.allert').remove();
    $(ckt.setting.map_body).prepend('<div class="loader"><b><img src="/checkoutru/ajax-loader.gif"/></b></div>');
}
ckt.carrier_ready = function(carrier){
    ckt.clear_map();
    $('.loader').remove();
    $('.allert').remove();
    if ((carrier['postamat'] == undefined) && (carrier['pvz'] == undefined)) {
        if ((carrier['express'] == undefined) && (carrier['express_own'] == undefined)) {
            $(ckt.setting.isset_carriers_body).hide();
            $(ckt.setting.map_body).prepend('<div class="allert"><b>Нет доставки в данный населенный пункт</b></div>');
        } else {
            $(ckt.setting.map_body).prepend('<div class="allert"><b>Нет пунктов выдачи в данном населенном пункте</b></div>');
            ckt.get_isset_delivery();
        }
    } else {
        ckt.open_map('all', true);
        ckt.get_isset_delivery();
    }
}
ckt.init({place_map: '#map',prod_price: 1, prod_assessed_price: 1});
