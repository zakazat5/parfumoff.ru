var dadata_clear_def = $.Deferred(),
    metro = {
        3: [
            'Библиотека имени В. И. Ленина',
            'Воробьёвы горы',
            'Комсомольская',
            'Красносельская',
            'Красные ворота',
            'Кропоткинская',
            'Лубянка',
            'Охотный ряд',
            'Парк культуры',
            'Преображенская площадь',
            'Проспект Вернадского',
            'Сокольники',
            'Спортивная',
            'Улица Подбельского',
            'Университет',
            'Фрунзенская',
            'Черкизовская',
            'Чистые пруды',
            'Юго-Западная'
        ],
        5: [
            'Академическая',
            'Алексеевская',
            'Бабушкинская',
            'Беляево',
            'Ботанический сад',
            'ВДНХ',
            'Выставочный центр',
            'Калужская',
            'Китай-город',
            'Коньково',
            'Ленинский проспект',
            'Медведково',
            'Новоясеневская',
            'Новые Черемушки',
            'Октябрьская',
            'Проспект Мира',
            'Профсоюзная',
            'Рижская',
            'Свиблово',
            'Сухаревская',
            'Теплый Стан',
            'Тургеневская',
            'Шаболовская',
            'Ясенево'
        ],
        1: [
            'Авиамоторная',
            'Марксистская',
            'Новогиреево',
            'Перово',
            'Площадь Ильича',
            'Третьяковская',
            'Шоссе Энтузиастов'
        ],
        10: [
            'Варшавская',
            'Каховская',
            'Каширская'
        ],
        7: [
            'Алтуфьево',
            'Аннино',
            'Бибирево',
            'Боровицкая',
            'Бульвар Дмитрия Донского',
            'Владыкино',
            'Дмитровская',
            'Менделеевская',
            'Нагатинская',
            'Нагорная',
            'Нахимовский проспект',
            'Отрадное',
            'Петровско-Разумовская',
            'Полянка',
            'Пражская',
            'Савеловская',
            'Севастопольская',
            'Серпуховская',
            'Тимирязевская',
            'Тульская',
            'Улица Академика Янгеля',
            'Улица Горчакова',
            'Цветной бульвар',
            'Чертановская',
            'Чеховская',
            'Южная'
        ],
        2: [
            'Автозаводская',
            'Аэропорт',
            'Водный стадион',
            'Войковская',
            'Динамо',
            'Домодедовская',
            'Кантемировская',
            'Коломенская',
            'Красногвардейская',
            'Маяковская',
            'Новокузнецкая',
            'Орехово',
            'Речной вокзал',
            'Сокол',
            'Тверская',
            'Театральная',
            'Царицыно'
        ],
        8: [
            'Баррикадная',
            'Беговая',
            'Волгоградский проспект',
            'Выхино',
            'Китай-город',
            'Кузнецкий мост',
            'Кузьминки',
            'Октябрьское поле',
            'Планерная',
            'Полежаевская',
            'Пролетарская',
            'Пушкинская',
            'Рязанский проспект',
            'Сходненская',
            'Таганская',
            'Текстильщики',
            'Тушинская',
            'Улица 1905 года',
            'Щукинская'
        ],
        4: [
            'Александровский сад',
            'Арбатская (Филевская)',
            'Багратионовская',
            'Выставочная',
            'Киевская',
            'Кунцевская',
            'Кутузовская',
            'Международная',
            'Пионерская',
            'Смоленская',
            'Студенческая',
            'Филевский парк',
            'Фили'
        ],
        9: [
            'Арбатская (Покровская)',
            'Бауманская',
            'Волоколамская',
            'Измайловская',
            'Киевская',
            'Крылатское',
            'Митино',
            'Молодежная',
            'Мякинино',
            'Парк Победы',
            'Партизанская',
            'Первомайская',
            'Площадь Революции',
            'Семеновская',
            'Славянский бульвар',
            'Смоленская',
            'Строгино',
            'Щелковская',
            'Электрозаводская'
        ],
        6: [
            'Братиславская',
            'Волжская',
            'Достоевская',
            'Дубровка',
            'Кожуховская',
            'Крестьянская застава',
            'Люблино',
            'Марьина Роща',
            'Марьино',
            'Печатники',
            'Римская',
            'Сретенский бульвар',
            'Трубная',
            'Чкаловская'
        ],
        11: [
            'Белорусская',
            'Добрынинская',
            'Киевская',
            'Комсомольская',
            'Краснопресненская',
            'Курская',
            'Новослободская',
            'Октябрьская',
            'Павелецкая',
            'Парк культуры',
            'Проспект Мира',
            'Таганская'
        ]
    };

// require dadata.js
(function () {
    "use strict";

    /**
     * Объединяет элементы массива через разделитель. При этом игнорирует пустые элементы.
     * @param arr Массив
     * @param separator Разделитель. Необязательный параметр, по умолчанию - запятая
     * @returns {string}
     */
    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }



    /**
     * Базовый объект подсказок
     * Синхронизирует изменение гранулярных полей с полем "одной строкой"
     */
    var Suggestions = {
        /**
         * Инициализирует подсказки на указанном элементе
         * @param $el   jQuery-элемент ввода одной строкой
         * @param parts Массив jQuery-элементов для гранулярных частей
         * @param separator Разделитель, через который нужно объединять гранулярные части
         * @constructor
         */
        init: function ($el, parts, separator) {
            parts.forEach(function ($part) {
                $part.change(function () {
                    var partialValues = parts.map(
                        function ($el) {
                            return $el.val()
                        }
                    );
                    $el.val(
                        join(partialValues, separator)
                    );
                });
            });

            //set options
        }
    };

    /**
     * Подскази по адресу
     */
    var AddressSuggestions = {


        initName: function(){
            /**
             * Если есть поле фио целяем к нему подсказки
             */
            if ($('input[name="fio"]').length>0){
                //подсказки
                $("input[name=fio]").suggestions({
                    serviceUrl: "https://dadata.ru/api/v1/suggest/fio",
                    selectOnSpace: true,
                    token: DadataApi.TOKEN,
                    /* Вызывается, когда пользователь выбирает одну из подсказок */
                    onSelect: function(suggestion) {
                        console.log(suggestion);
                        suggestion = suggestion;
                        $('input[name=fio_data]').val(JSON.stringify(suggestion));
                    }
                });
            }

        },

        initForm: function () {
            $('body').addClass('loading');



            $.when(dadata_clear_def).then(function(){
                $('body').removeClass('loading');
            },function(){
                $('body').removeClass('loading');
                $('body').addClass('error');
            });

            // обновляем список КС при выборе особых мест в мск и питере
            $('#select_part').on('change', function(e) {

                var addr = $("#region").val();
                addr += ", "+$(this).find("option[value='"+$(this).val()+"']").text();

                AddressSuggestions.getLogistic($(this).val(), addr);
            });

            //bind inputs

            var region_name = window.region_name;
            var city_name = window.city_name;
            //подсказки работают только на указанных полях
            AddressSuggestions.init($("[name=region]"), [], 'region');
            AddressSuggestions.init($("[name=city]"), [], 'city');
            AddressSuggestions.init($("[name=settlement]"), [], 'settlement');
            AddressSuggestions.init($("[name=street]"), [], 'street');

            $('[name="house"]').on('change',function(){
                $('[name="house"]').val($(this).val());
            });

            $('[name="corpus"]').on('change',function(){
                $('[name="corpus"]').val($(this).val());
            });

            $('[name="stroenie"]').on('change',function(){
                $('[name="stroenie"]').val($(this).val());
            });

            $('[name="flat"]').on('change',function(){
                $('[name="flat"]').val($(this).val());
            });

            $('[name="domophone"]').on('change',function(){
                $('[name="domophone"]').val($(this).val());
            });

            $('[name="regs_postcode"]').on('change',function(){
                $('[name="regs_postcode"]').val($(this).val());
            });


            if (typeof region_name != 'undefined') {
                if ($('[name=region]').val() == '') {
                    $('[name=region]').val(region_name);
                    $('[name=city]').val(city_name);

                }else{
                    region_name  = $('[name=region]').val();
                }



                this.correctValues();
                if (region_name == 'Москва' || region_name=='г Москва' || region_name == 'Санкт-Петербург' || region_name == 'г Санкт-Петербург') {
                    this.initParts(region_name);
                    $('.city').hide();
                } else {
                    $('.part').hide();
                    $('.other').hide();
                    $('#select_part').attr('disabled','disabled')
                }
            }else{
                console.log('region_name is not recognized');
                var region_name = $('[name=region]').val();
                if (region_name == 'г Москва' || region_name == 'г Санкт-Петербург') {
                    this.initParts(region_name);
                    $('.city').hide();
                } else {
                    $('.part').hide();
                    $('.other').hide();
                    $('#select_part').attr('disabled','disabled')
                }
            }

            if ($('#street').val()==''){
                $('.house').hide();
                $('.house input').val('');
                //fix
                $('[name="house"]').val('');
                $('[name="corpus"]').val('');
                $('[name="stroenie"]').val('');
                $('[name="flat"]').val('');
                $('[name="domophone"]').val('');
                $('[name="regs_postcode"]').val('');


                $('.house').show();
            }else{
                $('.house').show();
            }

            if ($('[name=settlement]').val()!=''){
                $('.settlement').show();
            }





            $('[name=city]').on('click', function () {
                $('[name=city]').change();
            });


            $('[name=part]').change(function () {
                if ($('[name=part]').val() == '0') {
                    $('.other').show();
                } else {
                    $('.other').hide();
                    $('#other').val('');
                }
            });


            $('[name=house]').on('focusout', function () {
                AddressSuggestions.clean();
            });

            $('[name=corpus]').on('focusout', function () {
                AddressSuggestions.clean();
            });

            $('[name=stroenie]').on('focusout', function () {
                AddressSuggestions.clean();
            });






            /*
            $('form[name="reg_form_new"]').find('input[name="register_submit"]').on('click', function () {

                var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
                var request = DadataApi.clean(q)

                request.done(function (msg) {
                    var postal_code = msg.data[0][0].postal_code
                    $('[name=regs_postcode]').val(postal_code)
                    $('[name=address_data]').val(JSON.stringify(msg.data[0][0]));
                    $('form[name="reg_form_new"]').submit();
                    return false;
                });
                return false;
            })
            */


        },

        /**
         * Инициализирует подсказки по адресу на указанном элементе
         * @param $el   jQuery-элемент ввода адреса одной строкой
         * @param parts Массив jQuery-элементов для гранулярных частей адреса
         * @constructor
         */
        init: function ($el, parts, type) {
            var self = this;
            Suggestions.init.call(self, $el, parts, ", ");


            $el.suggestions({
                serviceUrl: DadataApi.DADATA_API_URL + "/suggest/address",
                token: DadataApi.TOKEN,
                selectOnSpace: true,
                maxHeight: 310,
                count: 40,
                onSearchStart: function (params) {
                    return self.forceRegion(params, type)
                },
                transformResult: function (response) {
                    var query = this.params.query;
                    return self.trimResults(response, type, query)
                },
                formatResult: function (suggestion, currentValue) {
                    return self.formatResult(suggestion, currentValue, type);
                },
                onSelect: function (suggestion) {
                    if (suggestion.data) {
                        this.value = self.formatSelected(suggestion, type);
                        self.showSelected(suggestion, type);
                    }
                }
            });
        },

        /**
         * Ограничивает поиск регионом
         * @param params Параметры ajax-запроса
         */
        forceRegion: function (params, type) {
            var query = params["query"];

            var region_name = $('[name=region]').val();

            if (type == 'postal_code' || type == 'region') {

            } else if (type == 'street') {
                var city_name = $('[name=city]').val();
                if ($('[name=other]').val() != '') {
                    city_name = $('[name=other]').val();
                }

                //убираем дубль Москва Москва в запросе
                if (region_name==city_name){
                    city_name='';
                }

                if ($('[name=settlement]').val() != ''){
                    city_name = city_name + ' ' + $('[name=settlement]').val()
                }



                query = region_name + " " + city_name + " " + query;
            } else if (type == 'settlement') {
                var city_name = $('[name=city]').val();
                query = region_name + " " + city_name + " " + query;
            } else {
                query = region_name + " " + query;
            }
            params["query"] = query;

        },

        /**
         * Фильтрует список подсказок
         * @param response Ответ от сервера подсказок
         */
        trimResults: function (response, type, query) {
            var indexDelta = 0;

            $(response.suggestions).each(function (index, value) {
                if (value.data.region_type == 'г' && (value.data.city == null && value.data.settlement == null)) {
                    if (typeof (response.suggestions[index]) !== 'undefined') {
                        response.suggestions[index].data.city_type = value.data.region_type;
                        response.suggestions[index].data.city = value.data.region
                    }
                }
                var skip = false;

                if (type == 'city') {
                    var currentValue = $('[name=city]').val();
                    //исключаем результаты с улицами
                    if (value.data.street != null) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                    //исключаем результаты с деревнями
                    if (!skip && (value.data.settlement != null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                    //
                    if (!skip && !(value.data.city != null||value.data.area != null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                } else if (type == 'settlement') {
                    if (value.data.street != null) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                    if (!skip && (value.data.settlement == null && value.data.city == null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                } else if (type == 'region') {
                    //заполенение региона
                    //пропускаем элемент если: есть улица, область, город
                    if (value.data.street != null || value.data.area!=null || (value.data.city!=null && value.data.region!=value.data.city )) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                } else if (type == 'street') {
                    //фильтруем по выбранному городу либо деревне

                }

            });

            if (type == 'street') {
                var newArray = [];
                $.each(response.suggestions, function (i, v) {
                    if (v.data.house == null) {
                        newArray.push(response.suggestions[i])
                    }
                });
                response.suggestions = newArray
            }


            return response;
        },

        /**
         * Форматирование элемента списка подсказок в две строки.
         * При отрисовке списка подсказок вызывается для каждого элемента списка.
         * @param suggestion   Подсказка
         * @param currentValue Введенный пользователем текст
         * @returns {string} HTML для элемента списка подсказок
         */
        formatResult: function (suggestion, currentValue, type) {
            if (type == 'postal_code') {
                var address = suggestion.data;
                // первая строка
                var part1 = join([
                    address.postal_code
                ]);
                // вторая строка - населенный пункт, улица и дом
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                    join([address.house_type, address.house], " ")
                ]);
                suggestion.value = part1;
            } else if (type == 'region') {
                var address = suggestion.data;
                // первая строка
                var part1 = join([
                    join([address.region_type, address.region], " ")
                ]);
                // вторая строка
                var part2 = '';
                suggestion.value = part1

            } else if (type == 'city') {
                var address = suggestion.data;
                // первая строка
                if (address.city!=null){
                    var part1 = join([
                        join([address.city_type, address.city], " ")
                    ]);
                }else{
                    var part1 = join([
                        join([address.area_type, address.area], " ")
                    ]);
                }

                // вторая строка
                var part2 = join([
                    join([address.region_type,address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                ]);
                suggestion.value = part1;
            } else if (type == 'settlement') {
                console.log('settlement');

                var address = suggestion.data;
                if (address.settlement==null){
                    var part1 = join([
                        join([address.city_type, address.city], " ")
                    ]);
                }else{
                    var part1 = join([
                        join([address.settlement_type, address.settlement], " ")
                    ]);
                }


                // вторая строка
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                ]);
                suggestion.value = part1;

            } else if (type == 'street') {
                var address = suggestion.data;
                // первая строка
                var part1 = join([
                    join([address.street_type, address.street], " "),
                ]);
                // вторая строка
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " ")
                ]);
                suggestion.value = part1;

            } else {
                suggestion.value = suggestion.value.replace("Россия, ", "");
                var address = suggestion.data;
                // первая строка
                var part1 = join([
                    address.postal_code,
                    address.region,
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " ")
                ]);
                // вторая строка
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                    join([address.house_type, address.house], " ")
                ]);
            }


            // подсветка введенного пользователем текста
            var pattern = '(^|\\s+)(' + $.Suggestions.utils.escapeRegExChars(currentValue) + ')';
            part2 = part2.replace(new RegExp(pattern, 'gi'), '$1<strong>$2<\/strong>');
            var suggestedValue = part2 ?
                "<span class=\"autocomplete-suggestion-result\">" + part1 + "</span>" + "<br><span class=\"autocomplete-info\">&nbsp;&nbsp;" + part2 + "</span>"
                : part1;
            return suggestedValue;
        },

        /**
         * Формирует текстовое представление подсказки, когда пользователь выбирает ее из списка
         * Возвращает все, кроме страны и индекса.
         * @param suggestion
         * @returns {string}
         */
        formatSelected: function (suggestion, type) {
            if (type == 'postal_code') {
                var address = suggestion.data;
                return address.postal_code;
            }


            var address = suggestion.data;
            return join([
                join([address.region_type, address.region], " "),
                join([address.area_type, address.area], " "),
                join([address.city_type, address.city], " "),
                join([address.settlement_type, address.settlement], " "),
                join([address.street_type, address.street], " "),
                join([address.house_type, address.house], " ")
            ]);
        },

        /**
         * Заполняет поля формы гранулярными полями адреса из выбранной подсказки
         * @param suggestion Выбранная подсказка
         */
        showSelected: function (suggestion, type) {
            var address = suggestion.data;





            $('[name=region]').val('');
            $('[name=city]').val('');
            $('[name=settlement]').val('');
            $('[name=regs_postcode]').val('');



            if ((address.region == 'г Санкт-Петербург' || address.region == 'Санкт-Петербург'|| address.region == 'г Москва' || address.region == 'Москва') && (address.city == null && address.settlement == null)) {
                address.city = address.region;
                address.city_type = address.region_type
            }



            //dadata clean
            $("[name=regs_postcode]").val(address.postal_code);
            $("[name=region]").val(address.region_type + ' ' + address.region);
            $("[name=city]").val(address.city_type + ' ' +address.city);

            // после заполнения города подставляем службы доставки
            // если не мск или питер
            if ((address.city != null || address.settlement != null) && address.street == null) {

                var addr;
                if (address.city != null) {
                    addr = address.city_type + ' ' + address.city;
                } else {
                    addr = address.settlement;
                }

                //меняем город в шапке
                if ( $('.city_name').length>0){
                    $('.city_name').html(addr);
                    $.post('/api/geo/',{city_name:addr,region_name:address.region_type+ ' ' + address.region, action: 'set_location'})
                }


                AddressSuggestions.getLogistic(address.kladr_id, addr);
            }





            //город
            if (address.settlement==null){
                //Lipetsk
                if (address.area==null){
                    $("[name=city]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                    $('.settlement').hide();
                    //Elec
                }else{
                    $("[name=city]").val(join([
                        join([address.area_type, address.area], " ")
                    ]));

                    $("[name=settlement]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                    $('.settlement').show();
                }
            }else{
                //Приморский край, г Дальнегорск, село Рудная Пристань
                if (address.area==null && address.city!=null){
                    $("[name=city]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                    //Липецкая обл, Долгоруковский р-н, село Долгоруково
                }else{
                    $("[name=city]").val(join([
                        join([address.area_type, address.area], " ")
                    ]));
                }

                $("[name=settlement]").val(join([
                    join([address.settlement_type, address.settlement], " ")
                ]));
                $('.settlement').show();

            }

            $("[name=street]").val(join([
                join([address.street_type, address.street], " "),
            ]));






            if (address.house!=null){
                if (address.house_type=='д'){
                    var match = address.house.match(/(\d+)(.к.(\d+|\w+))?(.стр.\d+)?/);
                    if (typeof (match[1])!='undefined'){
                        $("[name=house]").val(match[1].match(/\d+/)[0]);
                        console.log('Дом:' + match[1].match(/\d+/)[0])
                    }
                    if (typeof (match[3])!='undefined'){
                        $("[name=corpus]").val(match[3].match(/\d+/)[0]);
                        console.log('Корпус:' + match[3].match(/\d+/)[0])
                    }

                    if (typeof (match[4])!='undefined'){
                        $("[name=stroenie]").val(match[4].match(/\d+/)[0]);
                        console.log('Строение:' + match[4].match(/\d+/)[0])
                    }
                }

                if (address.house_type=='стр'){
                    $("[name=stroenie]").val(address.house);
                    $("[name=house]").val('');
                    $("[name=corpus]").val('');
                    console.log('Строение:' + address.house)
                }


                if (address.house_type=='к'){
                    $("[name=stroenie]").val('');
                    $("[name=house]").val('');
                    $("[name=corpus]").val(address.house);
                    console.log('Корпус:' + address.house)
                }
            }

            if (address.region == 'Москва' || address.region == 'Санкт-Петербург' || address.region == 'г Москва' || address.region == 'г Санкт-Петербург') {
                this.initParts(address.region);
                $('.city').show();
            } else {
                $('.part').hide();
                $('.other').hide();
                $('#select_part').attr('disabled','disabled');
                $('.city').show();
            }

            // инициализация селекта метро для москвы
            var $metroHolder = $('.metro'),
                $metroItems = $metroHolder.find("option");

            $metroItems.removeClass('metro-1');
            $.each($metroItems, function(i, item) {

                var $item = $(item);

                for(var m in metro) {
                    $.each(metro[m], function(h, metroItem) {

                        if ($.trim(metroItem) == $.trim($item.text())) {
                            $item.addClass("metro-"+m);
                        }
                    });
                }
            });

            if (address.region == 'Москва'){
                $metroHolder.show();
            }else{
                $metroHolder.hide();
            }

            $('select[name=metro]').selectpicker({
                size: 8
            });
        },
        /**
         * Инициализация селекта выбора района Москвы
         * @param {Number} kladr_id - целочисленный идентификатор кладра
         * @param {String} city - название города
         * @returns {Object} - возвращается объект ajax запроса
         */
        initParts: function (region_name) {
            if (region_name == 'Москва' || region_name=='г Москва') {
                if ($('[name="part"] option:selected').attr('data-location')!='m'){
                    $('[name="part"] option').hide().removeAttr("selected");
                    $('[name="part"] option[data-location="m"]').show();
                    $('[name="part"] option[data-location="m"]:first').attr("selected", "selected");
                }else{
                    $('[name="part"] option[data-location="m"]').show();
                }
            } else if (region_name == 'Санкт-Петербург' || region_name == 'г Санкт-Петербург') {
                if ($('[name="part"] option:selected').attr('data-location')!='s'){
                    $('[name="part"] option').hide().removeAttr("selected");
                    $('[name="part"] option[data-location="s"]').show();
                    $('[name="part"] option[data-location="s"]:first').attr("selected", "selected");
                }else{
                    $('[name="part"] option[data-location="s"]').show();
                }
            }

            $('.city').hide();
            $('.city input').val('');
            $('.other').hide();
            $('.part').show();
            $('#select_part').removeAttr('disabled');

            if ($('[name=part]').val() == '0') {
                $('.other').show();
            }
        },
        /**
         * Функция заполнения карточек по типам служб доставки
         * для указанного города забираются все возможные службы доставки
         * и выставляются по типу доставки в разные карточки
         * для самовывоза - указываются метки на карте
         *
         * @param {Number} kladr_id - целочисленный идентификатор кладра
         * @param {String} city - название города
         * @returns {Object} - возвращается объект ajax запроса
         */
        getLogistic: function(kladr_id, city) {

            var $wrapper = $('.b-delivery__table'),
                currentCity = city;

            $wrapper.find('.btn2').removeClass('active');
            $wrapper.find('.item_js').closest('tr').next('tr').addClass('hide');
            $('.self_item').remove();

            $wrapper.off('click', '.btn2');
            $wrapper.on('click', '.btn2', function(e) {
                e.stopPropagation();

                var $this = $(this),
                    $row = $this.closest('tr'),
                    id = $row.data('id');

                $wrapper.find('.hide_this_block').not($row.next('tr')).addClass("hide");
                $row.next('tr').toggleClass('hide');
                $wrapper.find('.btn2').not($this).removeClass('active').text('Выбрать');

                /** хак с оберткой в зеленый бордер */
                $wrapper.find("td, tr").removeClass('active');
                $row.addClass('active');
                if ($row.prev().is(":visible")) {
                    $row.prev().find('td').addClass('active');
                } else {
                    $row.prev().prev().find('td').addClass('active');
                }
                /** закончился бред */

                $this.toggleClass('active');

                if ($this.hasClass('active')) {

                    $this.text('Выбрано');

                    //выставляем все суммы и id
                    $("#delivery_cost-text").text(parseInt($row.find('.cost').text()));
                    $("#delivery_cost").val(parseInt($row.find('.cost').text()));

                    $('#courier_address_id').val(id);
                    $("#orders").find('.cart_btn1').removeAttr('disabled');
                } else {
                    $this.text('Выбрать');

                    // сбрасываем стоимость доставки
                    $("#delivery_cost-text").text(0);
                    $("#delivery_cost").val(0);

                    $('#courier_address_id').val(0);
                    $("#orders").find('.cart_btn1').attr('disabled', 'disabled');
                }

                $('#big_sum_all').text(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));
            });

            /*if ( isMobile() ) {
                $wrapper.off('click', '.item_js');
                $wrapper.on('click', '.item_js', function() {
                    $(this).find('.btn2').trigger('click');
                });
            }*/

            

            return $.ajax({
                url: "/api/logistic/",
                data: {
                    k: kladr_id,
                    f: 1
                },
                success: function(response) {

                    var arData = [],
                        selfData = [],
                        $container = "",
                        hasCourier = false,
                        hasPost = false,
                        hasSelf = false,
                        count = 0,
                        dSelf = $.Deferred(),
                        resultsCount = 0,
                        geoAllAddedDef = $.Deferred();

                    // если был ответ, разбираем его
                    if (response != null) {

                        // прогоняем все КС перебирая по типу
                        for(var k in response) {

                            arData = [];

                            if (k == 'курьером') {

                                hasCourier = true;
                                $container = $('#courier__delivery').show(0);

                                // каждый раз очищаем контейнер, куда закидываем КС
                                $container.html("");

                                var city = "";

                                // прогоняем все адреса для доставки в текущей КС
                                $.each(response[k].addresses, function(j, addr) {

                                    city = addr.name_city;
                                    $container.data("id", addr.id);

                                    arData.push('<td><strong>Курьерская доставка</strong></td>');
                                    arData.push('<td><span class="days">'+addr.delivery_days+'</span> дн.</td>');
                                    arData.push('<td>от <span class="cost">'+addr.delivery_cost+'</span> руб.</td>');
                                    arData.push('<td class="hidden-xs"><button type="button" class="btn2">Выбрать</button></td>');
                                });

                                // склеиваем результат и вставляем в контейнер
                                $container.html(arData.join(""));

                            } else if (k == 'самовывоз') {

                                hasSelf = true;
                                $container = $('#self__delivery').show(0);

                                var city = "";

                                // прогоняем все данные
                                $.each(response[k], function(i, item) {

                                    // прогоняем все адреса для доставки в текущей КС
                                    $.each(item.addresses, function(j, addr) {

                                        city = addr.name_city;

                                        arData.push('<tr class="item_js self_item" data-id="'+addr.id+'">');
                                        arData.push('<td>'+addr.delivery_address+'</td>');
                                        arData.push('<td><span class="days">'+addr.delivery_days+'</span> дн.</td>');
                                        arData.push('<td><span class="cost">'+addr.delivery_cost+'</span> руб.</td>');
                                        arData.push('<td class="hidden-xs"><button type="button" class="btn2">Выбрать</button></td>');
                                        arData.push('</tr>');

                                        arData.push('<tr class="hide self_item hide_this_block">');
                                        arData.push('<td colspan="4"><p>'+addr.delivery_how_to+'</p></td>');
                                        arData.push('</tr>');
                                    });
                                });

                                // склеиваем результат и вставляем в контейнер
                                $(arData.join("")).insertAfter($container);

                            } else if (k == 'почта') {

                                hasPost = true;

                                $container = $('#post__delivery').show(0);
                                var city = "";

                                // прогоняем все данные
                                $.each(response[k], function(i, item) {

                                    // прогоняем все адреса для доставки в текущей КС
                                    $.each(item.addresses, function(j, addr) {

                                        //сохраняем город для позиционирования
                                        city = addr.name_city;

                                        arData.push('<td><strong>Почта России (наложенный платеж)</strong></td>');
                                        arData.push('<td><span class="days">'+addr.delivery_days+'</span> дн.</td>');
                                        arData.push('<td><span class="cost">'+addr.delivery_cost+'</span> руб.</td>');
                                        arData.push('<td class="hidden-xs"><button type="button" class="btn2">Выбрать</button></td>');
                                    });
                                });

                                // склеиваем результат и вставляем в контейнер
                                $container.html(arData.join(""));
                            }
                        }

                        // если не центрировали карту на
                        if (!hasSelf) {

                            if (city == null || city == "") {
                                city = currentCity;
                            }

                            $.when(ymapsDef).then(function() {
                                ymaps.geocode(city, {
                                    results: 1
                                }).then(function(res) {

                                    // Выбираем первый результат геокодирования
                                    var firstGeoObject = res.geoObjects.get(0),
                                    // Координаты геообъекта
                                        coords = firstGeoObject.geometry.getCoordinates(),
                                    // Область видимости геообъекта
                                        bounds = firstGeoObject.properties.get('boundedBy');

                                    // Масштабируем карту на область видимости геообъекта.
                                    yMap.setBounds(bounds, {
                                        checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                                    });
                                });
                            });
                        }

                        // очищаем всё, что само не очистилось
                        if (!hasCourier) {
                            $('#courier__delivery').hide(0).html("");
                        }

                        if (!hasPost) {
                            $('#post__delivery').hide(0).html("");
                        }

                        if (!hasSelf) {
                            $('#self__delivery').hide(0);
                            $('.self_item').remove();

                            $.when(ymapsDef).then(function() {
                                // очищаем карту
                                yMap.geoObjects.removeAll();
                            });
                        }
                    }
                }
            })
        },
        correctValues: function(){

            if ($('[name=region]').val()==$('[name=city]').val()){
                var q = $('[name=region]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            }else{
                var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            }


            if ($('[name=corpus]').val()!=''){
                q = q + ' корп ' + $('[name=corpus]').val();
            }
            if ($('[name=stroenie]').val()!=''){
                q = q + ' стр ' + $('[name=stroenie]').val();
            }

            var request = DadataApi.clean(q);
            var self = this;
            request.done(function (msg) {
                if (typeof msg !='undefined'){
                    dadata_clear_def.resolve();
                    var data = [];
                    data['data']= self.getCleanData(msg);
                    self.showSelected(data,'all');


                }else{
                    dadata_clear_def.reject();
                }

            });
            return request;
        },
        /**
         * получить данные из ответа подсказки
         * @param msg
         * @returns {*}
         */
        getCleanData:function(msg){
            if (typeof (msg.suggestions[0])!='undefined'){
                return msg.suggestions[0]['data'];
            }
        },

        clean:function(){
            if ($('[name=region]').val()==$('[name=city]').val()){
                var q = $('[name=region]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            }else{
                var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            }

            if ($('[name=corpus]').val()!=''){
                q = q + ' корп ' + $('[name=corpus]').val();
            }
            if ($('[name=stroenie]').val()!=''){
                q = q + ' стр ' + $('[name=stroenie]').val();
            }
            var request = DadataApi.clean(q);
            request.done(function (msg) {
                if (typeof (msg.suggestions[0])!='undefined'){
                    var data =  msg.suggestions[0]['data'];
                }
                if (data!=''){
                    var postal_code = data.postal_code;
                    console.log(postal_code);
                    $('[name=regs_postcode]').val(postal_code);
                    $('[name=address_data]').val(JSON.stringify(data));
                    $('[name=regs_postcode]').val(postal_code)
                }
            });
        },
        cleanName:function(input){
            var q = $(input).val();


            var request = DadataApi.cleanName(q);
            request.done(function (msg) {

                console.log(msg);
                /*
                if (typeof (msg.suggestions[0])!='undefined'){
                    var data =  msg.suggestions[0]['data'];
                }
                if (data!=''){
                    var postal_code = data.postal_code;
                    console.log(postal_code)
                    $('[name=regs_postcode]').val(postal_code)
                    $('[name=address_data]').val(JSON.stringify(data));
                    $('[name=regs_postcode]').val(postal_code)
                }
                */
            });
        }
    };
    window.AddressSuggestions = AddressSuggestions;
})();