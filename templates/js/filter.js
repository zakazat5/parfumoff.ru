$(function(){

	/* Открытие всплывающего окна фильтра */
	$('.filter a.open_list').click(function(e){
		e.preventDefault();

		// сделать автоматическое скрытие всплывающего окна при клике вне области фильтра
		var elem 	= $(this);
		var elem_w 	= parseInt(elem.width())+50;
		var wrap 	= elem.next();
		var b 		= true;

		if (elem.hasClass('active')) {
			elem.removeClass('active');
			wrap.slideUp(200).removeClass('opened');
		} else {
			
			$('.filter_select__open.opened').each(function(index,item){
				var item = $(item);
				item.slideUp(200).removeClass('opened');
				item.prev().removeClass('active');
			});

			elem.addClass('active');
			wrap.css('min-width',elem_w).slideDown(200).addClass('opened');
		}

		$(document).off('click.open_list_click');
		$(document).on('click.open_list_click', function(e) {
			if (!b) {
				if (!$(e.target).hasClass('filter_select__open') && !$(e.target).parents('.filter_select__open').length) {
					elem.removeClass('active');
					wrap.slideUp(200).removeClass('opened');
					$(document).off('click.open_list_click');
				}
			}

			b = false;
		});
	});
 	
 	/* Удаление элемента фильтра */
	$(document).on('click', '.filter .filter_options-selected .tag2 .close, .filter_options_mobile_wrap .close',function(){
		var elem 		= $(this).parent();
		var elem_text 	= elem.find('.text').text();

		elem.remove();

		$('.filter .filter_select label').each(function(index,item){
			var item 		= $(item);
			var item_text	= item.text();

			if (item_text == elem_text) {
				item.removeClass('c_on');
				item.find('input').removeAttr('checked');
		    }
		});
	});

	/* Обработка нажатия на чекбокс в фильтре */
	$('.filter_select label').click(function(){
		var elem 		= $(this);
		var elem_text 	= elem.text();
		var results 	= elem.parents('.filter_select').find('.filter_options-selected');
		var html 		= '<span class="tag2"><span class="text">'+elem_text+'</span><span class="close">&times;</span></span>';

		/* Дублирование состояний для одинаковых фильтров */
		setTimeout(function() {
			var elem_cond	= (elem.hasClass('c_on'))?'on':'off';
			var wrap 		= elem.parent();
			var wrap_index 	= wrap.index();
			var outer 		= wrap.parent();
			var outer_index = outer.parent().index();
			var pos 		= (elem.parents('.filter__bottom').length)?'bottom':'top';

			if (elem_cond === 'on') {
				$('.filter .empty_text').hide();
				results.show().append(html);
				//outer.next().append(html);
			} else {
				results.find('.tag2').each(function(index,item){
					if ($(item).find('.text').text() == elem.text()) $(item).remove();
				});
				if (!results.find('.tag2').length) {
					$('.filter .empty_text').show();
					results.hide();
				}
			}
			
			if (outer.hasClass('filter_select__open_double')) {
				var label = "";

				if ( pos === 'top') {
					label = $('.filter__bottom .lCol .filter_select:eq('+outer_index+') .filter_select__open div:eq('+wrap_index+') label');
				} else {
					label = $('.filter__top .filter_select:eq('+outer_index+') .filter_select__open div:eq('+wrap_index+') label');
				}

				if (elem_cond === 'off') {
					label.removeClass('c_on');
		        	label.find('input').removeAttr('checked');
				} else {
		        	label.addClass('c_on');
		        	label.find('input').attr('checked','checked');
		        }
			}
		},10);
	});
	
	/* Показать фильтр */
	$('#show_filter').click(function(e){
		e.preventDefault();
		var wrap 		= $(this).parents('.filter');
		var top_wrap 	= wrap.find('.filter__top');
		var bottom_wrap = wrap.find('.filter__bottom');

		bottom_wrap.show();
		top_wrap.hide();

		$('.filter_select__open').hide();
		$('.open_list').removeClass('active');
	});

	/* Спрятать фильтр */
	$('#hide_filter').click(function(e){
		e.preventDefault();
		var wrap 		= $(this).parents('.filter');
		var top_wrap 	= wrap.find('.filter__top');
		var bottom_wrap = wrap.find('.filter__bottom');

		top_wrap.show();
		bottom_wrap.hide();

		$('.filter_select__open').hide();
		$('.open_list').removeClass('active');
	});

	$('#hide_filter_mobile').click(function(e){
		e.preventDefault();
		var elem 		= $(this);
		var wrap 		= $(this).parents('.filter');
		var top_wrap 	= wrap.find('.filter__top');
		var bottom_wrap = wrap.find('.filter__bottom');

		if (elem.hasClass('clicked')) {
			top_wrap.hide();
			bottom_wrap.find('.filter_select').show();
			$('#empty_filter_wrap').show();

			$('.filter_select__open').hide();
			$('.open_list').removeClass('active');
			elem.removeClass('clicked').text('Скрыть параметры поиска')	
		} else {
			top_wrap.hide();
			bottom_wrap.find('.filter_select').hide();
			bottom_wrap.find('.filter_select:eq(0)').show();
			bottom_wrap.find('.filter_select:eq(1)').show();
			$('#empty_filter_wrap').hide();

			$('.filter_select__open').hide();
			$('.open_list').removeClass('active');
			elem.addClass('clicked').text('Показать параметры поиска');
		}
		
	});

	/* Очистить фильтр */
	$('#empty_filter').click(function(e){
		e.preventDefault();
		var wrap 		= $(this).parents('.filter');
		var results 	= wrap.find('.filter_options-selected');
		var empty_text  = wrap.find('.empty_text');

		results.html('').hide();
		empty_text.show();

		$('.filter .filter_select label').removeClass('c_on');
		$('.filter .filter_select label input').removeAttr('checked');
		$('.filter .results').html('');
		$('.filter .filter_options_mobile_wrap').html('');
		$('.filter .empty_text').show();
	})
	

});