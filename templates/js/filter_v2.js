$(function () {

    /* Открытие всплывающего окна фильтра */
    $('.filter a.open_list').click(function (e) {
        e.preventDefault();

        // сделать автоматическое скрытие всплывающего окна при клике вне области фильтра
        var elem = $(this);
        var elem_w = parseInt(elem.width()) + 50;
        var wrap = elem.next();
        var b = true;

        if (elem.hasClass('active')) {
            elem.removeClass('active');
            wrap.slideUp(200).removeClass('opened');
        } else {

            $('.filter_select__open.opened').each(function (index, item) {
                var item = $(item);
                item.slideUp(200).removeClass('opened');
                item.prev().removeClass('active');
            });

            elem.addClass('active');
            wrap.css('min-width', elem_w).slideDown(200).addClass('opened');
        }

        $(document).off('click.open_list_click');
        $(document).on('click.open_list_click', function (e) {
            if (!b) {
                if (!$(e.target).hasClass('filter_select__open') && !$(e.target).parents('.filter_select__open').length) {
                    elem.removeClass('active');
                    wrap.slideUp(200).removeClass('opened');
                    $(document).off('click.open_list_click');
                }
            }

            b = false;
        });
    });

    /* Удаление элемента фильтра */
    $(document).on('click', '.filter .filter_options-selected .tag2 .close, .filter_options_mobile_wrap .close', function () {
        var elem = $(this).parent();
        var elem_text = elem.find('.text').text();
        var elem_id = elem.data('id')
        
        $('.filter .filter_options-selected .tag2, .filter .mCol .results .tag2, .filter_options_mobile_wrap .tag2').each(function(index,item) {
        	var item = $(item)
        	console.log(item)
        	console.log(item.data('id'))
        	if (item.data('id') == elem_id) {
        		item.remove()
        		console.log(1)
        	}
        })
       
        
        elem.remove()

        $('.filter .filter_select label').each(function (index, item) {
            var item = $(item);
            var item_text = item.text();

            if (item_text == elem_text) {
                item.removeClass('c_on');
                item.find('input').removeAttr('checked').trigger('change');;
            }
        });
    });








    /* Обработка нажатия на чекбокс в фильтре */
    $('.filter_select label').click(function () {
        var elem = $(this);
        var elem_text = elem.text();
        var results = elem.parents('.filter_select').find('.filter_options-selected');
        var html = '<span class="tag2" data-id="' + elem.data('id') + '"><span class="text">' + elem_text + '</span><span class="close">&times;</span></span>';

        /* Дублирование состояний для одинаковых фильтров */
        setTimeout(function () {

            var form = elem.parents('form');
            var elem_cond = (elem.hasClass('c_on')) ? 'on' : 'off';
            var input  =  elem.find('input');
            var value = $(input).attr('value');
            var name = $(input).attr('name');

            var mCol = $('.filter__bottom .mCol')

            if (elem_cond === 'off') {
                $(form).find('[name="'+ name +'"][value="'+ value + '"]').removeProp('checked').trigger('change').parent().removeClass('c_on');
            }else{
                $(form).find('[name="'+ name +'"][value="'+ value + '"]').prop('checked', 'checked').trigger('change').parent().addClass('c_on');
            }


            if (elem_cond === 'on') {
                $('.filter .empty_text').hide();
                results.show().append(html);

                if (!mCol.is(':visible')) {
                    $('#show_filter').click()
                }
                mCol.append(html)
                elem.parents('.filter_select').find('.filter_options_mobile_wrap').append(html)

            } else {
                mCol.find('.tag2').each(function (index, item) {
                    if ($(item).find('.text').text() == elem.text()) $(item).remove();
                });

                elem.parents('.filter_select').find('.filter_options_mobile_wrap .tag2').each(function (index, item) {
                    if ($(item).find('.text').text() == elem.text()) $(item).remove();
                });

                if (!mCol.find('.tag2').length) {
                    $('.filter .empty_text').show();
                    results.hide();
                }
            }








            /*var wrap = elem.parent();
            var wrap_index = wrap.index();
            var outer = wrap.parent();
            var outer_index = outer.index();
            var pos = (elem.parents('.filter__bottom').length) ? 'bottom' : 'top';
            var mCol = $('.filter__bottom .mCol')

            if (elem_cond === 'on') {
                $('.filter .empty_text').hide();
                results.show().append(html);
                outer.next().append(html);

                if (!mCol.is(':visible')) {
                    $('#show_filter').click()
                }
                mCol.append(html)

            } else {
                mCol.find('.tag2').each(function (index, item) {
                    if ($(item).find('.text').text() == elem.text()) $(item).remove();
                });
                if (!mCol.find('.tag2').length) {
                    $('.filter .empty_text').show();
                    results.hide();
                }
            }

            if (outer.hasClass('filter_select__open_double')) {
                var label = "";

                console.log(outer_index)
                console.log(wrap_index)

                if (pos === 'top') {
                    label = $('.filter__bottom .lCol .filter_select:eq(' + outer_index + ') .filter_select__open div:eq(' + wrap_index + ') label');
                } else {
                    label = $('.filter__top .filter_select:eq(' + outer_index + ') .filter_select__open div:eq(' + wrap_index + ') label');
                }



                if (elem_cond === 'off') {
                    label.removeClass('c_on');
                    label.find('input').removeAttr('checked').trigger('change');
                } else {
                    label.addClass('c_on');
                    label.find('input').attr('checked', 'checked').trigger('change');
                }
            }*/
        }, 10);
    });


    $(document).on("click", ".filter__bottom .mCol .tag2 .close", function (e) {
        var elem = $(this).parent()
        var text = elem.text()
        var id = elem.data('id')

        $('.filter__bottom .lCol label[data-id=' + id + ']').removeClass('c_on').find('input').removeAttr('checked').trigger('change');
        $('.filter__top label[data-id=' + id + ']').removeClass('c_on').find('input').removeAttr('checked').trigger('change');

        elem.remove()

        if (!$('.filter__bottom .mCol .tag2').length) $('#empty_filter').click()
    })


    /* Показать фильтр */
    $('#show_filter').click(function (e) {
        e.preventDefault();
        var wrap = $(this).parents('.filter');
        var top_wrap = wrap.find('.filter__top');
        var bottom_wrap = wrap.find('.filter__bottom');

        bottom_wrap.show();
        top_wrap.hide();

        $('.filter_select__open').hide();
        $('.open_list').removeClass('active');
    });

    /* Спрятать фильтр */
    $('#hide_filter').click(function (e) {
        e.preventDefault();
        var wrap = $(this).parents('.filter');
        var top_wrap = wrap.find('.filter__top');
        var bottom_wrap = wrap.find('.filter__bottom');

        top_wrap.show();
        bottom_wrap.hide();

        $('.filter_select__open').hide();
        $('.open_list').removeClass('active');
    });

    $('#hide_filter_mobile').click(function (e) {
        e.preventDefault();
        var elem = $(this);
        var wrap = $(this).parents('.filter');
        var top_wrap = wrap.find('.filter__top');
        var bottom_wrap = wrap.find('.filter__bottom');

        if (elem.hasClass('clicked')) {
            top_wrap.hide();
            bottom_wrap.find('.filter_select').show();
            $('#empty_filter_wrap').show();

            $('.filter_select__open').hide();
            $('.open_list').removeClass('active');
            elem.removeClass('clicked').text('Скрыть параметры поиска')
        } else {
            top_wrap.hide();
            bottom_wrap.find('.filter_select').hide();
            bottom_wrap.find('.filter_select:eq(0)').show();
            bottom_wrap.find('.filter_select:eq(1)').show();
            $('#empty_filter_wrap').hide();

            $('.filter_select__open').hide();
            $('.open_list').removeClass('active');
            elem.addClass('clicked').text('Показать параметры поиска');
        }

    });

    /* Очистить фильтр */
    $('#empty_filter').click(function (e) {
        e.preventDefault();
        var wrap = $(this).parents('.filter');
        var results = wrap.find('.filter_options-selected');
        var empty_text = wrap.find('.empty_text');

        results.html('').hide();
        empty_text.show();

        $('.filter .filter_select label').removeClass('c_on');
        $('.filter .filter_select label input').removeAttr('checked').trigger('change');;
        $('.filter .results').html('');
        $('.filter .filter_options_mobile_wrap').html('');
        $('.filter .empty_text').show();
        $('.filter .mCol .tag2').remove()
    })


    if ($('.filter .filter__top  input:checked').length > 0) {
        var results = $('.results');

        $('.filter .empty_text').hide();
        $.each($('.filter .filter__bottom input:checked'), function (i, v) {
            var elem = $(v).parent();
            var wrap = elem.parent();
            var wrap_index = wrap.index();
            var outer = wrap.parent();
            var outer_index = outer.parent().index();
            var pos = (elem.parents('.filter__bottom').length) ? 'bottom' : 'top';
            var mCol = $('.filter__bottom .mCol');

            var elem_text = $(elem).text();
            var html = '<span class="tag2" data-id="' + elem.data('id') + '"><span class="text">' + elem_text + '</span><span class="close">&times;</span></span>';
            results.show();
            results.append(html);
            outer.next().append(html);
        });
        $('#show_filter').click();
    }


    $(".filter input[type='checkbox']").change(function() {
        var form  = $('.filter form');
        $(form).find('.filter__top input').removeProp('checked');
        $.ajax({'url':form.attr('action')+ '?ajax=1&' + $(form).serialize() },function(){

        }).done(function(data){
            $('.filter_hide').hide();
            $('.items').html(data);
            $(".items img.lazy").lazyload({
                effect : "fadeIn"
            });
        })

    })


});