$('#checkout_product_widget').append('<div class="widget_container"><div id="product_widget"></div></div>');

ckt.get_product = function(){
     ckt.setting.prod_count = 1;
     ckt.setting.prod_weight = 1;
     ckt.setting.prod_price = 5990;
     ckt.setting.prod_assessed_price = 5990;
}
ckt.before_carrier_ready = function(){
     $('#product_widget').html('');
     $('.allert').remove();
     if ($(ckt.setting.map_body).is(":visible")) {
          $(ckt.setting.map_body).prepend('<div class="loader"><b><img src="/checkoutru/ajax-loader.gif"/></b></div>');
     } else {
        //   $('#product_widget').append('<div class="loader" style="margin-top: 28px;"><b><img src="/checkoutru/ajax-loader.gif"/></b></div>');
     }
}
ckt.carrier_ready = function(carrier){
     $('#product_widget').hide();
     $('.allert').remove();
     $('.loader').remove();
     if ((carrier['postamat'] == undefined) && (carrier['pvz'] == undefined)) {
          if ((carrier['express'] == undefined) && (carrier['express_own'] == undefined)) {
               $(ckt.setting.isset_carriers_body).hide();
               $('#mapBody').prepend('<div class="allert"><b>Нет доставки в данный населенный пункт</b></div>');
          } else {
               $('#mapBody').prepend('<div class="allert"><b>Нет пунктов выдачи в данном населенном пункте</b></div>');
               ckt.get_isset_delivery();
          }
     } else {
          ckt.get_isset_delivery();
     }
     ckt.get_isset_delivery('#product_widget');
}
ckt.init({prod_price: 1, prod_assessed_price: 1, show_map_list: false});
