$(function () {

    $('.tovar_item_img_wrap').click(function(){
        location.href = $(this).find('a').eq(0).attr('href')
    })

    //сообщить о поступлении
    $('.aromat_table .btn4').click(function (e) {
        e.preventDefault();
        var articul = $(e.currentTarget).data('articul');

        // console.log(articul)
        $('#no_aromat_popup').show().find('#modal_articul').val(articul)
        yaCounter21451555.reachGoal('send_inform_er');
    });

    $('#no_aromat_popup .close').click(function () {
        $(this).parent().hide()
    });

    //call me
    $('#phone__callback').click(function (e) {
        e.preventDefault();
		$('#callMe_form_name_err').hide();
		$('#callMe_form_phone_err').hide();
        $('#callMe').show();
    });
    $('#callMe .close').click(function () {
        $(this).parent().hide()
    });

    $('.phone_input').mask("7(999) 999-99-99");

    $('.aromat .tabs_type1 a').click(function (e) {
        e.preventDefault();

        var elem = $(this);
        var cond = (elem.parent().hasClass('active')) ? 1 : 0;
        var target = elem.attr('href');
        var elems = elem.parents('ul').find('a');
        var li = elem.parents('ul').find('li');

        if (cond) return;

        $('#tab1, #tab2, #tab3').addClass('hidden');
        $(target).removeClass('hidden');
        li.removeClass('active');
        elem.parent().addClass('active')
    });

    $('[data-init="scroll_to"]').click(function (e) {
        e.preventDefault();

        var elem = $(this);
        var target = str_replace('#', '', elem.attr('href'));        
        var target_obj = $('[name=' + target + ']');
        var st = target_obj.offset().top;

        $('html,body').animate({'scrollTop': st}, 500)
    });

    /* Iphone Like Slider */
    $('.filter_aromats').iphoneStyle({
        checkedLabel: '',
        uncheckedLabel: '',
        onChange: function (obj) {
            if ($(obj).is(':checked')){
                $('.aromat_table tr.disabled').show();
                $('.items .item').show();
                setCookie('outstock',1,{expires: 3600})
            }else{
                $('.aromat_table tr.disabled').hide();
                $('.items .item.outstock').hide();
                setCookie('outstock',0,{expires: 3600})
            }

            console.log('change')
        }
    });


    /* Responsive JS */
    /*
    var top_search = $('#top_search');
    var logo = $('#logo');
    $(window).scroll(function () {
        var st = $(window).scrollTop();
        var w = $(window).width();

        if (w < 650 && st > 130) {
            top_search.addClass('fixed');
            logo.addClass("fix_hack")
        } else {
            top_search.removeClass('fixed');
            logo.removeClass("fix_hack")
        }
    });
    */

    $('#static_comments').html('')


    /*
     * Магия. Не трогать. Как работает - не знаю.
     ************************************************/
    $('#menu_btn,#mobile_phone,#mobile_user_cab,#user_cab,.user_login').on('click', function (e) {
        e.preventDefault();

        var elem = $(this);
        var menu = $(elem.data('target'));
        var b = true;

        elem.toggleClass('active');


        if (elem.hasClass('active')) {
            $('#phone_popup,#user_popup,#menu').not(elem.data('target')).animate({
                'top': '45px',
                'opacity': '0'
            }, 200);
            setTimeout(function () {
                $('#phone_popup,#user_popup,#menu').not(elem.data('target')).css('height', '0').hide();
            }, 200);
            $('#menu_btn,#mobile_phone,#mobile_user_cab,#user_cab,.user_login').not('#' + elem.attr('id')).removeClass('active');

            menu.css('height', 'auto').show();
            menu.animate({
                'top': '50px',
                'opacity': '1'
            }, 200);
        } else {
            menu.animate({
                'top': '45px',
                'opacity': '0'
            }, 200);
            setTimeout(function () {
                menu.css('height', '0').hide();
            }, 200);
        }

        $(document).off('click.menu_click');

        $(document).on('click.menu_click', function (e) {

            if (!b) {
                if ('#' + $(e.target).attr('id') !== elem.data('target')) {
                    if (!$(e.target).parents(elem.data('target')).length) {
                        menu.animate({
                            'top': '45px',
                            'opacity': '0'
                        }, 200);
                        setTimeout(function () {
                            menu.css('height', '0').hide();
                        }, 200);

                        elem.removeClass('active');
                        $(document).off('click.menu_click');
                    }
                }
            }
            b = false;
        });
    });
    /*
     * Конец магии
     ****************************************/


    // обработки для корзины
    $(".korzina").click(function () {

        var id = $(this).attr('id');
        var korzina = '#' + id;
        var korzina_loader = '.korzina_loader_' + id;

        $(korzina).hide();
        $(korzina_loader).show();

        $.post("/protect/price.php", {
            type: 'add',
            id: id
        }, function (xml) {
            $("message", xml).each(function (id) {
                message = $("message", xml).get(id);
                $("#korzina_kol").html($("kol", message).text());
                $(korzina_loader).hide();
                $(korzina).animate({opacity: "show"}, "slow");
                window.location = '/korzina/';
            });
        });
    });

    $(".korzina_delete").click(function () {
        var uid = $(this).attr('id');
        var sum = $('#sum_' + uid).text();
        var kol = $('#kol_' + uid).val();

        $("#tr_" + uid).animate({opacity: "hide"}, "slow");

        $.post("/protect/price.php", {
            type: 'delete',
            uid: uid,
            sum: sum,
            kol: kol
        }, function (xml) {
            $("message", xml).each(function (id) {
                message = $("message", xml).get(id);

                $("#korzina_kol").html($("kol", message).text());
                $("#tr_" + uid).remove();
                $("#korzinaS_sum").html($("sum", message).text());
            });
        });

        return false;
    });

    $("#radio2").click(function () {
        if ($(this).val() == "on" || $(this).val() == "2") {
            $("#town-wrap").show();
        }
    });

    $("#radio1").click(function () {
        $("#town-wrap").hide();
    });

    $("#pass3").click(function () {
        if ($(this).val() == "on" || $(this).val() == "2") {
            $("#pass-wrap").show();
        }
    });

    $("#pass2").click(function () {
        $("#pass-wrap").hide();
    });

    $('#mobile_enter_purchase').click(function (e) {
        e.preventDefault();
        $('.b-purchase.left').hide();
        $('.b-purchase.right').show()
    });

    $('#mobile_enter_purchase1').click(function (e) {
        e.preventDefault();
        $('.b-purchase.left').show();
        $('.b-purchase.right').hide()
    })

    $("img.lazy").lazyload({
        effect : "fadeIn"
    });

    $('.aromat_table_tabs li a').on('click',function(e){
        e.preventDefault();
        $('.aromat_table_tabs li').removeClass('active');
        $(this).parent().addClass('active');
        var group = $(this).data('group');

        if ($('.filter_aromats').is(':checked')==true){
            if (group=='all'){
                $('.aromat_table tbody tr').show();
            }else{
                $('.aromat_table tbody tr').hide();
                $('.aromat_table tbody tr[data-group="'+ group + '"]').show();
            }
        }else{
            if (group=='all'){
                $('.aromat_table tbody tr').not(".disabled").show();
            }else{
                $('.aromat_table tbody tr').hide();
                if ($('.aromat_table tbody tr[data-group="'+ group + '"]').not(".disabled").length==0){
                    $('.aromat_table tbody tr[data-group="'+ group + '"]').show();
                }else{
                    $('.aromat_table tbody tr[data-group="'+ group + '"]').not(".disabled").show();
                }

            }
        }
    })

    $('.card_tabs1 li a').on('click',function(e){
        e.preventDefault();
        $('.card_tabs1 li').removeClass('active');
        $(this).parent().addClass('active');
        var type = $(this).data('type');
        $(".tab_items .tab_item").hide();

        $('.tab_items [data-type="'+ type + '"]').show();

    })

});

function setupLabelCheck(obj) {
    $(obj).each(function (index, item) {

        var $item = $(item);
        $item.click(function (e) {

            e.preventDefault();
            var elem = $(this);

            if (elem.hasClass('c_on')) {
                elem.removeClass('c_on');
                elem.find('input:checked').removeAttr('checked')
            } else {
                elem.addClass('c_on');
                elem.find('input').attr('checked', 'checked')
            }
        });
    })
}

function setupLabelRadio(obj) {
    $(obj).each(function (index, item) {
        var item = $(item);
        var input = item.find('input');
        var input_name = input.attr('name');
        var inputs = $('[name=' + input_name + ']');

        if (input.attr('checked') == 'checked') {
            item.addClass('c_on')
        }

        item.click(function () {
            var elem = $(this);

            inputs.each(function (index, item) {
                var input = $(item);
                var label = input.parent();
                label.removeClass('c_on');
                input.removeAttr('checked')
            });

            elem.addClass('c_on');
            elem.find('input').attr('checked', 'checked')


        });
    })
}

$(document).ready(function () {
    setupLabelCheck($('.label_check'));
    setupLabelRadio($('.label_radio'));
});

function str_replace(search, replace, subject) {
    return subject.split(search).join(replace)
}


function check_form(form_obj) {

    var elems = form_obj.find('input[type="text"]');
    form_obj.find('input.phone_input').mask("(999) 999-99-99");

    form_obj.submit(function (e) {

        var b_error = 0;
        var count = 0;

        elems.each(function (index, item) {
            var item = $(item);
            var val = item.val();

            if (item.hasClass('required') && val == '') {
                if (count === 0) {
                    item.focus()
                }
                item.addClass('error');
                b_error = 1;
                var descr = item.next();
                if (descr.hasClass('error_descr')) descr.show();
                count++;
                console.log('req')
            }

            item.focus(function () {
                item.removeClass('error');
                if (item.data('error-text') != '') {
                    var descr = item.next();
                    if (descr.hasClass('error_descr')) descr.hide()
                }
            });
            item.keyup(function () {
                item.removeClass('error');
                if (item.data('error-text') != '') {
                    var descr = item.next();
                    if (descr.hasClass('error_descr')) descr.hide()
                }
            })
        });

        console.log(b_error);


        if (b_error == 0) {
            return true
        } else {
            return false
        }
    })
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires*1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for(var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}


// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
$(function() {
    "use strict";

    var $element = $(".listing_title");

    if (!$element.length) {
        $element = $('.brand__letter:first');
    }

    if (!$element.length) {
        return false;
    }

    if (isMobile()) {
        $('html, body').animate({
            scrollTop: $element.offset().top - 50
        }, 0);
    }
});