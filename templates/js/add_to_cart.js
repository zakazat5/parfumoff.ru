$(document).ready(function(){
	
	$(".add_to_cart").click(function (event) {
	event.preventDefault();
	var id_product_add = Number($(this).data('id_product'));
	var id_catalog = Number($(this).data('id_catalog'));
	if (id_product_add != 0) {
		if ($(document).width() <= 768) {
			$("#product_cat_" + id_catalog)
            .clone()
            .css({'position' : 'absolute', 'z-index' : '11100', 'top': $(this).offset().top, 'left': $(this).offset().left-100})
            .appendTo("body")
            .animate({opacity: 0.05,
                left: $(".header__mobile_cart").offset()['left'],
                top: $(".header__mobile_cart").offset()['top'],
                width: 20}, 1000, function() {
                $(this).remove();
            });
		} else {
			$("#product_cat_" + id_catalog)
            .clone()
            .css({'position' : 'absolute', 'z-index' : '11100', 'top': $(this).offset().top, 'left': $(this).offset().left-100})
            .appendTo("body")
            .animate({opacity: 0.05,
                left: $(".header__cart").offset()['left'],
                top: $(".header__cart").offset()['top'],
                width: 20}, 1000, function() {
                $(this).remove();
            });
		}
		
		$.ajax({
			type: "POST",
			url: "/korzina?ajax_add_to_cart=1",
			//dataType: "json",
			data: "id_product_add=" + id_product_add
		}).done(function(data) {
			$(".header__mobile_cart span").text(data);
			$(".header__mobile_cart").addClass("cart_mod");
			$(".header__cart").addClass("cart_mod");
			$(".header__cart").text("Корзина: " + data + " товаров");
			$(".header__cart_block").show();
			$('#fast_view').hide();
            $('#overlay').remove();
			if ($('#global_geo_segment').data('geo_segment') == '1') {
				yaCounter21451555.reachGoal('add_to_cart_tovar_success');
			} else {
				yaCounter42164749.reachGoal('add_to_cart_tovar_success');
			}
		});
	}
	yaCounter21451555.reachGoal('onclick_buy_tovar_er');
});

});