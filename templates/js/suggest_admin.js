var dadata_clear_def = $.Deferred(),
    metro = {
        1: [
            'Библиотека имени В. И. Ленина',
            'Воробьёвы горы',
            'Комсомольская',
            'Красносельская',
            'Красные ворота',
            'Кропоткинская',
            'Лубянка',
            'Охотный ряд',
            'Парк культуры',
            'Преображенская площадь',
            'Проспект Вернадского',
            'Сокольники',
            'Спортивная',
            'Улица Подбельского',
            'Университет',
            'Фрунзенская',
            'Черкизовская',
            'Чистые пруды',
            'Юго-Западная'
        ],
        2: [
            'Академическая',
            'Алексеевская',
            'Бабушкинская',
            'Беляево',
            'Ботанический сад',
            'ВДНХ',
            'Выставочный центр',
            'Калужская',
            'Китай-город',
            'Коньково',
            'Ленинский проспект',
            'Медведково',
            'Новоясеневская',
            'Новые Черемушки',
            'Октябрьская',
            'Проспект Мира',
            'Профсоюзная',
            'Рижская',
            'Свиблово',
            'Сухаревская',
            'Теплый Стан',
            'Тургеневская',
            'Шаболовская',
            'Ясенево'
        ],
        3: [
            'Авиамоторная',
            'Марксистская',
            'Новогиреево',
            'Перово',
            'Площадь Ильича',
            'Третьяковская',
            'Шоссе Энтузиастов'
        ],
        4: [
            'Варшавская',
            'Каховская',
            'Каширская'
        ],
        5: [
            'Алтуфьево',
            'Аннино',
            'Бибирево',
            'Боровицкая',
            'Бульвар Дмитрия Донского',
            'Владыкино',
            'Дмитровская',
            'Менделеевская',
            'Нагатинская',
            'Нагорная',
            'Нахимовский проспект',
            'Отрадное',
            'Петровско-Разумовская',
            'Полянка',
            'Пражская',
            'Савеловская',
            'Севастопольская',
            'Серпуховская',
            'Тимирязевская',
            'Тульская',
            'Улица Академика Янгеля',
            'Улица Горчакова',
            'Цветной бульвар',
            'Чертановская',
            'Чеховская',
            'Южная'
        ],
        6: [
            'Автозаводская',
            'Аэропорт',
            'Водный стадион',
            'Войковская',
            'Динамо',
            'Домодедовская',
            'Кантемировская',
            'Коломенская',
            'Красногвардейская',
            'Маяковская',
            'Новокузнецкая',
            'Орехово',
            'Речной вокзал',
            'Сокол',
            'Тверская',
            'Театральная',
            'Царицыно'
        ],
        7: [
            'Баррикадная',
            'Беговая',
            'Волгоградский проспект',
            'Выхино',
            'Китай-город',
            'Кузнецкий мост',
            'Кузьминки',
            'Октябрьское поле',
            'Планерная',
            'Полежаевская',
            'Пролетарская',
            'Пушкинская',
            'Рязанский проспект',
            'Сходненская',
            'Таганская',
            'Текстильщики',
            'Тушинская',
            'Улица 1905 года',
            'Щукинская'
        ],
        8: [
            'Александровский сад',
            'Арбатская (Филевская)',
            'Багратионовская',
            'Выставочная',
            'Киевская',
            'Кунцевская',
            'Кутузовская',
            'Международная',
            'Пионерская',
            'Смоленская',
            'Студенческая',
            'Филевский парк',
            'Фили'
        ],
        9: [
            'Арбатская (Покровская)',
            'Бауманская',
            'Волоколамская',
            'Измайловская',
            'Киевская',
            'Крылатское',
            'Митино',
            'Молодежная',
            'Мякинино',
            'Парк Победы',
            'Партизанская',
            'Первомайская',
            'Площадь Революции',
            'Семеновская',
            'Славянский бульвар',
            'Смоленская',
            'Строгино',
            'Щелковская',
            'Электрозаводская'
        ],
        10: [
            'Братиславская',
            'Волжская',
            'Достоевская',
            'Дубровка',
            'Кожуховская',
            'Крестьянская застава',
            'Люблино',
            'Марьина Роща',
            'Марьино',
            'Печатники',
            'Римская',
            'Сретенский бульвар',
            'Трубная',
            'Чкаловская'
        ],
        11: [
            'Белорусская',
            'Добрынинская',
            'Киевская',
            'Комсомольская',
            'Краснопресненская',
            'Курская',
            'Новослободская',
            'Октябрьская',
            'Павелецкая',
            'Парк культуры',
            'Проспект Мира',
            'Таганская'
        ]
    };

// require dadata.js
(function () {
    "use strict";

    /**
     * Объединяет элементы массива через разделитель. При этом игнорирует пустые элементы.
     * @param arr Массив
     * @param separator Разделитель. Необязательный параметр, по умолчанию - запятая
     * @returns {string}
     */
    function join(arr /*, separator */) {
        var separator = arguments.length > 1 ? arguments[1] : ", ";
        return arr.filter(function (n) {
            return n
        }).join(separator);
    }



    /**
     * Базовый объект подсказок
     * Синхронизирует изменение гранулярных полей с полем "одной строкой"
     */
    var Suggestions = {
        /**
         * Инициализирует подсказки на указанном элементе
         * @param $el   jQuery-элемент ввода одной строкой
         * @param parts Массив jQuery-элементов для гранулярных частей
         * @param separator Разделитель, через который нужно объединять гранулярные части
         * @constructor
         */
        init: function ($el, parts, separator) {
            parts.forEach(function ($part) {
                $part.change(function () {
                    var partialValues = parts.map(
                        function ($el) {
                            return $el.val()
                        }
                    );
                    $el.val(
                        join(partialValues, separator)
                    );
                });
            });

            //set options
        }
    };

    /**
     * Подскази по адресу
     */
    var AddressSuggestions = {
        initName: function(){
            /**
             * Если есть поле фио целяем к нему подсказки
             */
            if ($('input[name="fio"]').length>0){
                //подсказки
                $("input[name=fio]").suggestions({
                    serviceUrl: "https://dadata.ru/api/v1/suggest/fio",
                    selectOnSpace: true,
                    token: DadataApi.TOKEN,

                    /* Вызывается, когда пользователь выбирает одну из подсказок */
                    onSelect: function(suggestion) {
                        suggestion = suggestion;
                        $('input[name=fio_data]').val(JSON.stringify(suggestion));

                        if ($('select[name=pol]').length>0){
                            var gender = suggestion.data.gender;
                            if(gender=='FEMALE'){
                                $('select[name=pol]').val('F');
                            }
                            if(gender=='MALE'){
                                $('select[name=pol]').val('M');
                            }

                        }
                    }
                });
            }

        },

        initForm: function () {
            $('body').addClass('loading');



            console.log('initForm')

            $.when(dadata_clear_def).then(function(){
                $('body').removeClass('loading');
            },function(){
                $('body').removeClass('loading');
                $('body').addClass('error');
            })



            //bind inputs

            var region_name = window.region_name;
            var city_name = window.city_name;
            //подсказки работают только на указанных полях
            AddressSuggestions.init($("[name=region]"), [], 'region');
            AddressSuggestions.init($("[name=city]"), [], 'city');
            AddressSuggestions.init($("[name=settlement]"), [], 'settlement');
            AddressSuggestions.init($("[name=street]"), [], 'street');



            if (typeof region_name != 'undefined') {
                if ($('[name=region]').val() == '') {
                    $('[name=region]').val(region_name)
                    $('[name=city]').val(city_name);

                }else{
                    region_name  = $('[name=region]').val();
                }

                console.log('correctValues');
                this.correctValues();
                if (region_name == 'Москва' || region_name=='г Москва' || region_name == 'Санкт-Петербург' || region_name == 'г Санкт-Петербург') {
                    this.initParts(region_name);
                    $('.city').show();
                } else {
                    $('.part').hide();
                    $('.metro').hide();
                    $('.other').hide();
                    $('#select_part').attr('disabled','disabled')
                }
            }else{


                console.log('region_name is not recognized');
                var region_name = $('[name=region]').val();
                if (region_name == 'Москва' || region_name=='г Москва' || region_name == 'Санкт-Петербург' || region_name == 'г Санкт-Петербург') {
                    this.initParts(region_name);
                    $('.city').show();
                } else {
                    $('.part').hide();
                    $('.metro').hide();
                    $('.other').hide();
                    $('#select_part').attr('disabled','disabled')
                }
            }





            /*
            if ($('#street').val()==''){
                $('.house').hide();
                $('[name="house"]').val('');
                $('[name="corpus"]').val('');
                $('[name="stroenie"]').val('');
                $('[name="flat"]').val('');
                $('[name="domophone"]').val('');
                $('[name="regs_postcode"]').val('');
                $('.house').show();
            }else{
                $('.house').show();
            }
            */

            if ($('[name=settlement]').val()!=''){
                $('.settlement').show();
            }





            $('[name=city]').on('click', function () {
                $('[name=city]').change();
            });


            $('[name=part]').change(function () {
                if ($('[name=part]').val() == '0') {
                    $('.other').show();
                } else {
                    $('.other').hide();
                    $('#other').val('');
                }
            })


            $('[name=house]').on('focusout', function () {
                AddressSuggestions.clean();
            })

            $('[name=corpus]').on('focusout', function () {
                AddressSuggestions.clean();
            })

            $('[name=corpus]').on('focusout', function () {
                AddressSuggestions.clean();
            })






            /*
            $('form[name="reg_form_new"]').find('input[name="register_submit"]').on('click', function () {

                var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
                var request = DadataApi.clean(q)

                request.done(function (msg) {
                    var postal_code = msg.data[0][0].postal_code
                    $('[name=regs_postcode]').val(postal_code)
                    $('[name=address_data]').val(JSON.stringify(msg.data[0][0]));
                    $('form[name="reg_form_new"]').submit();
                    return false;
                });
                return false;
            })
            */


        },

        /**
         * Инициализирует подсказки по адресу на указанном элементе
         * @param $el   jQuery-элемент ввода адреса одной строкой
         * @param parts Массив jQuery-элементов для гранулярных частей адреса
         * @constructor
         */
        init: function ($el, parts, type) {
            var self = this;
            Suggestions.init.call(self, $el, parts, ", ");


            $el.suggestions({
                serviceUrl: DadataApi.DADATA_API_URL + "/suggest/address",
                token: DadataApi.TOKEN,
                selectOnSpace: true,
                maxHeight: 310,
                count: 40,
                onSearchStart: function (params) {
                    return self.forceRegion(params, type)
                },
                transformResult: function (response) {
                    var query = this.params.query;
                    return self.trimResults(response, type, query)
                },
                formatResult: function (suggestion, currentValue) {
                    return self.formatResult(suggestion, currentValue, type);
                },
                onSelect: function (suggestion) {
                    if (suggestion.data) {
                        this.value = self.formatSelected(suggestion, type);
                        self.showSelected(suggestion, type);
                    }
                }
            });
        },

        /**
         * Ограничивает поиск регионом
         * @param params Параметры ajax-запроса
         */
        forceRegion: function (params, type) {
            var query = params["query"];

            var region_name = $('[name=region]').val()

            if (type == 'postal_code' || type == 'region') {

            } else if (type == 'street') {
                var city_name = $('[name=city]').val()
                if ($('[name=other]').val() != '') {
                    city_name = $('[name=other]').val();
                }

                //убираем дубль Москва Москва в запросе
                if (region_name==city_name){
                    city_name='';
                }

                if ($('[name=settlement]').val() != ''){
                    city_name = city_name + ' ' + $('[name=settlement]').val()
                }
                query = region_name + " " + city_name + " " + query;
            } else if (type == 'settlement') {
                var city_name = $('[name=city]').val()
                query = region_name + " " + city_name + " " + query;
            } else {
                query = region_name + " " + query;
            }
            params["query"] = query;

        },

        /**
         * Фильтрует список подсказок
         * @param response Ответ от сервера подсказок
         */
        trimResults: function (response, type, query) {
            var indexDelta = 0;

            $(response.suggestions).each(function (index, value) {
                if (value.data.region_type == 'г' && (value.data.city == null && value.data.settlement == null)) {
                    if (typeof (response.suggestions[index]) !== 'undefined') {
                        response.suggestions[index].data.city_type = value.data.region_type
                        response.suggestions[index].data.city = value.data.region
                    }
                }
                var skip = false;

                if (type == 'city') {
                    var currentValue = $('[name=city]').val();
                    //исключаем результаты с улицами
                    if (value.data.street != null) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                    //исключаем результаты с деревнями
                    if (!skip && (value.data.settlement != null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                    //
                    if (!skip && !(value.data.city != null||value.data.area != null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                } else if (type == 'settlement') {
                    if (value.data.street != null) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                    if (!skip && (value.data.settlement == null && value.data.city == null)) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }
                } else if (type == 'region') {
                    //заполенение региона
                    //пропускаем элемент если: есть улица, область, город
                    if (value.data.street != null || value.data.area!=null || (value.data.city!=null && value.data.region!=value.data.city )) {
                        response.suggestions.splice(index - indexDelta, 1);
                        indexDelta = indexDelta + 1;
                        skip = true;
                    }

                } else if (type == 'street') {
                    //фильтруем по выбранному городу либо деревне

                }

            })

            if (type == 'street') {
                var newArray = new Array();
                $.each(response.suggestions, function (i, v) {
                    if (v.data.house == null) {
                        newArray.push(response.suggestions[i])
                    }
                });
                response.suggestions = newArray
            }


            return response;
        },

        /**
         * Форматирование элемента списка подсказок в две строки.
         * При отрисовке списка подсказок вызывается для каждого элемента списка.
         * @param suggestion   Подсказка
         * @param currentValue Введенный пользователем текст
         * @returns {string} HTML для элемента списка подсказок
         */
        formatResult: function (suggestion, currentValue, type) {
            if (type == 'postal_code') {
                var address = suggestion.data;
                // первая строка
                var part1 = join([
                    address.postal_code
                ]);
                // вторая строка - населенный пункт, улица и дом
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                    join([address.house_type, address.house], " ")
                ]);
                suggestion.value = part1;
            } else if (type == 'region') {
                var address = suggestion.data;
                // первая строка
                var part1 = join([
                    join([address.region_type, address.region], " ")
                ]);
                // вторая строка
                var part2 = '';
                suggestion.value = part1

            } else if (type == 'city') {
                var address = suggestion.data;
                // первая строка
                if (address.city!=null){
                    var part1 = join([
                        join([address.city_type, address.city], " ")
                    ]);
                }else{
                    var part1 = join([
                        join([address.area_type, address.area], " ")
                    ]);
                }

                // вторая строка
                var part2 = join([
                    join([address.region_type,address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                ]);
                suggestion.value = part1;
            } else if (type == 'settlement') {
                console.log('settlement')

                var address = suggestion.data;
                if (address.settlement==null){
                    var part1 = join([
                        join([address.city_type, address.city], " ")
                    ]);
                }else{
                    var part1 = join([
                        join([address.settlement_type, address.settlement], " ")
                    ]);
                }


                // вторая строка
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                ]);
                suggestion.value = part1;

            } else if (type == 'street') {
                var address = suggestion.data;
                // первая строка
                var part1 = join([
                    join([address.street_type, address.street], " "),
                ]);
                // вторая строка
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " ")
                ]);
                suggestion.value = part1;

            } else {
                suggestion.value = suggestion.value.replace("Россия, ", "");
                var address = suggestion.data;
                // первая строка
                var part1 = join([
                    address.postal_code,
                    address.region,
                    join([address.area_type, address.area], " "),
                    join([address.city_type, address.city], " ")
                ]);
                // вторая строка
                var part2 = join([
                    join([address.region_type, address.region], " "),
                    join([address.settlement_type, address.settlement], " "),
                    join([address.street_type, address.street], " "),
                    join([address.house_type, address.house], " ")
                ]);
            }


            // подсветка введенного пользователем текста
            var pattern = '(^|\\s+)(' + $.Suggestions.utils.escapeRegExChars(currentValue) + ')';
            part2 = part2.replace(new RegExp(pattern, 'gi'), '$1<strong>$2<\/strong>')
            var suggestedValue = part2 ?
                "<span class=\"autocomplete-suggestion-result\">" + part1 + "</span>" + "<br><span class=\"autocomplete-info\">&nbsp;&nbsp;" + part2 + "</span>"
                : part1;
            return suggestedValue;
        },

        /**
         * Формирует текстовое представление подсказки, когда пользователь выбирает ее из списка
         * Возвращает все, кроме страны и индекса.
         * @param suggestion
         * @returns {string}
         */
        formatSelected: function (suggestion, type) {
            if (type == 'postal_code') {
                var address = suggestion.data;
                return address.postal_code;
            }


            var address = suggestion.data;
            return join([
                join([address.region_type, address.region], " "),
                join([address.area_type, address.area], " "),
                join([address.city_type, address.city], " "),
                join([address.settlement_type, address.settlement], " "),
                join([address.street_type, address.street], " "),
                join([address.house_type, address.house], " ")
            ]);
        },

        /**
         * Заполняет поля формы гранулярными полями адреса из выбранной подсказки
         * @param suggestion Выбранная подсказка
         */
        showSelected: function (suggestion, type) {
            var address = suggestion.data;

            $('[name=region]').val('');
            $('[name=city]').val('');
            $('[name=settlement]').val('');
            $('[name=regs_postcode]').val('');



            if ((address.region == 'г Санкт-Петербург' || address.region == 'Санкт-Петербург'|| address.region == 'г Москва' || address.region == 'Москва') && (address.city == null && address.settlement == null)) {
                address.city = address.region
                address.city_type = address.region_type
            }


            //dadata clean
            /*
            if (typeof address.qc!='undefined'){
                $("[name=regs_postcode]").val(address.postal_code);
                $("[name=region]").val(address.region);
                $("[name=city]").val(address.city);
                console.log(address)

                //dadata suggest
            }else{
            */
            $("[name=regs_postcode]").val(address.postal_code);
            $("[name=region]").val(address.region_type + ' ' + address.region);
            $("[name=city]").val(address.city_type + ' ' +address.city);






            // после заполнения города подставляем службы доставки
            // если не мск или питер
            if ((address.city != null || address.settlement != null) && address.street == null) {

                var addr;
                if (address.city != null) {
                    addr = address.city_type + ' ' + address.city;
                } else {
                    addr = address.settlement;
                }

                //меняем город в шапке
                if ( $('.city_name').length>0){
                    $('.city_name').html(addr);
                    $.post('/api/geo/',{city_name:addr,region_name:address.region_type+ ' ' + address.region, action: 'set_location'})
                }


                AddressSuggestions.getLogistic(address.kladr_id, addr);
            }


            //город
            if (address.settlement==null){
                //Lipetsk
                if (address.area==null){
                    $("[name=city]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                    $('.settlement').hide();
                    //Elec
                }else{
                    $("[name=city]").val(join([
                        join([address.area_type, address.area], " ")
                    ]));

                    $("[name=settlement]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                    $('.settlement').show();
                }
            }else{
                //Приморский край, г Дальнегорск, село Рудная Пристань
                if (address.area==null && address.city!=null){
                    $("[name=city]").val(join([
                        join([address.city_type, address.city], " ")
                    ]));
                //Липецкая обл, Долгоруковский р-н, село Долгоруково
                }else{
                    $("[name=city]").val(join([
                        join([address.area_type, address.area], " ")
                    ]));
                }

                $("[name=settlement]").val(join([
                    join([address.settlement_type, address.settlement], " ")
                ]));
                $('.settlement').show();

            }

            $("[name=street]").val(join([
                join([address.street_type, address.street], " "),
            ]));


            if (address.house!=null){
                if (address.house_type=='д'){
                    var match = address.house.match(/(\d+)( к \d+)?( стр \d+)?/)
                    if (typeof (match[1])!='undefined'){
                        $("[name=house]").val(match[1].match(/\d+/)[0])
                        console.log('Дом:' + match[1].match(/\d+/)[0])
                    }
                    if (typeof (match[2])!='undefined'){
                        $("[name=corpus]").val(match[2].match(/\d+/)[0])
                        console.log('Корпус:' + match[2].match(/\d+/)[0])
                    }

                    if (typeof (match[3])!='undefined'){
                        $("[name=stroenie]").val(match[3].match(/\d+/)[0])
                        console.log('Строение:' + match[3].match(/\d+/)[0])
                    }
                }

                if (address.house_type=='стр'){
                    $("[name=stroenie]").val(address.house)
                    $("[name=house]").val('')
                    $("[name=corpus]").val('')
                    console.log('Строение:' + address.house)
                }


                if (address.house_type=='к'){
                    $("[name=stroenie]").val('')
                    $("[name=house]").val('')
                    $("[name=corpus]").val(address.house)
                    console.log('Корпус:' + address.house)
                }
            }



            /*
            if ($('[name=street]').val() != '') {
                $('.house').show();
            } else {
                $('.house').hide();
                $('.house input').val('');
            }
            */

            //if (type == 'region') {
            if (address.region == 'Москва' || address.region == 'Санкт-Петербург' || address.region == 'г Москва' || address.region == 'г Санкт-Петербург') {
                this.initParts(address.region);
                $('.city').show();
            } else {
                $('.part').hide();
                $('.metro').hide();
                $('.other').hide();
                $('#select_part').attr('disabled','disabled')
                $('.city').show();
            }
            //}

            if (address.region == 'Москва'||address.region == 'г Москва'){
                $('.metro').show();
            }else{
                $('.metro').hide();
            }
        },
        /**
         * Инициализация селекта выбора района Москвы
         * @param {Number} kladr_id - целочисленный идентификатор кладра
         * @param {String} city - название города
         * @returns {Object} - возвращается объект ajax запроса
         */
        initParts: function (region_name) {
            if (region_name == 'Москва' || region_name=='г Москва') {
                if ($('[name="part"] option:selected').attr('data-location')!='m'){
                    $('[name="part"] option').hide().removeAttr("selected");
                    $('[name="part"] option[data-location="m"]').show();
                    $('[name="part"] option[data-location="m"]:first').attr("selected", "selected");
                }else{
                    $('[name="part"] option[data-location="m"]').show();
                }
            } else if (region_name == 'Санкт-Петербург' || region_name == 'г Санкт-Петербург') {
                if ($('[name="part"] option:selected').attr('data-location')!='s'){
                    $('[name="part"] option').hide().removeAttr("selected");
                    $('[name="part"] option[data-location="s"]').show();
                    $('[name="part"] option[data-location="s"]:first').attr("selected", "selected");
                }else{
                    $('[name="part"] option[data-location="s"]').show();
                }
            }

            $('.other').hide();
            $('.part').show();
            $('.metro').show();
            $('#select_part').removeAttr('disabled')



            $('[name=part]').on('change',function(){
                if ($('[name=part]').val() == '0') {
                    $('.other').show();
                }

            })
        },
        /**
         * Функция заполнения карточек по типам служб доставки
         * для указанного города забираются все возможные службы доставки
         * и выставляются по типу доставки в разные карточки
         * для самовывоза - указываются метки на карте
         *
         * @param {Number} kladr_id - целочисленный идентификатор кладра
         * @param {String} city - название города
         * @returns {Object} - возвращается объект ajax запроса
         */
        getLogistic: function(kladr_id, city) {
            var $wrapper = $('#couriers__wrapper'),
                aColors = {
                    1: 'blue',
                    2: 'grey',
                    3: 'violet',
                    4: 'orange',
                    5: 'yellow',
                    6: 'black',
                    7: 'red',
                    8: 'green'
                },
                currentCity = city,
                $button = $('#submit_form');

            // блокируем кнопку
            $button.attr('disabled', 'disabled');

            $wrapper.find('.dostavka_item').removeClass('active');
            $wrapper.find('.dostavka_item__checkbox').removeClass('checked');

            $(document).off('click', '.courier__select');
            $(document).off('click', '#couriers__wrapper .dostavka_item__checkbox');

            $(document).on('click', '.courier__select', function(e) {

                e.preventDefault();

                var $this = $(this),
                    id = $this.data('id'),
                    $logistic = $('#self_selected'),
                    $courier_wrapper = $('#couriers__wrapper'),
                    $parent = $this.parents('ymaps[class$="balloon__content"]'),
                    arr = [];

                $courier_wrapper.find('.dostavka_item').removeClass('active');
                $courier_wrapper.find('.dostavka_item__checkbox').removeClass('checked');
                $('#courier_address_id').val(id);

                // при инициализации стоимости доставки, мы не меняем её по средствам js
                if (isDeliveryCostInited) {
                    // подставляем стоимость доставки
                    $("#delivery_cost-text").text($parent.find('.cost').text());
                    $("#delivery_cost").val($parent.find('.cost').text());
                } else {
                    isDeliveryCostInited = true;
                }

                // обновляем общую стоимость
                // обновляем общую стоимость
                var $dSum = $('#dSum'),
                    $bSum = $('#big_sum_all'),
                    total = $dSum.length ? $dSum.val() : $bSum.data('sum');

                $bSum.text(parseInt(total) + parseInt($("#delivery_cost").val()));

                arr.push("<div class='wrr'>");
                arr.push("<span>Вы выбрали: <b>"+$parent.find('ymaps[class$="balloon-content__header"]').text()+"</b></span>");
                arr.push("<span>Адрес пункта самовывоза: <b>"+$parent.find('.address').text()+"</b></span>");
                arr.push("<span>Время доставки от "+$parent.find('.days').text()+" дн.</span>");
                arr.push("<span>Стоимость доставки от "+$parent.find('.cost').text()+" р.</span>");
                arr.push("</div>");
                $logistic.html(arr.join(""));

                $button.removeAttr('disabled');
            });

            $('.dop_info input').on('change',function(){
                $('.dop_info input[name="'+ $(this).attr('name') + '"').val($(this).val())
            });

            $(document).on('click', '#couriers__wrapper .dostavka_item__checkbox', function(e) {

                e.preventDefault();

                var $this = $(this),
                    $parent = $this.parents('.courier__row'),
                    id = $parent.data('courier');

                $('#couriers__wrapper').find('.dostavka_item__checkbox').not($this).removeClass('checked');
                $('.dostavka_item').not($this.parents('.dostavka_item')).removeClass('active');
                $this.toggleClass('checked');

                $('#self_selected').empty();

                if ($this.hasClass('checked')) {

                    // раскрываем блок с адресом
                    $this.parents('.dostavka_item').addClass('active');

                    // при инициализации стоимости доставки, мы не меняем её по средствам js
                    if (isDeliveryCostInited) {
                        // подставляем стоимость доставки
                        $("#delivery_cost-text").text($parent.find('.cost').text());
                        $("#delivery_cost").val($parent.find('.cost').text());
                    } else {
                        isDeliveryCostInited = true;
                    }

                    $('#courier_address_id').val(id);
                    $button.removeAttr('disabled');
                } else {

                    // скрываем блок с адресом
                    $this.parents('.dostavka_item').removeClass('active');

                    // при инициализации стоимости доставки, мы не меняем её по средствам js
                    if (isDeliveryCostInited) {
                        // сбрасываем стоимость доставки
                        $("#delivery_cost-text").text(0);
                        $("#delivery_cost").val(0);
                    } else {
                        isDeliveryCostInited = true;
                    }

                    $('#courier_address_id').val(0);
                    $button.attr('disabled', 'disabled');
                }

                // обновляем общую стоимость
                var $dSum = $('#dSum'),
                    $bSum = $('#big_sum_all'),
                    total = $dSum.length ? $dSum.val() : $bSum.data('sum');

                $bSum.text(parseInt(total) + parseInt($("#delivery_cost").val()));
                //$("#user_big_sum_all").val(parseInt($('#dSum').val()) + parseInt($("#delivery_cost").val()));
            });

            return $.ajax({
                url: "/api/logistic/",
                data: {
                    k: kladr_id,
                    t: $('input[name="payment_type"]').val()
                },
                success: function(response) {

                    var arData = [],
                        selfData = [],
                        $container = "",
                        hasCourier = false,
                        hasPost = false,
                        hasSelf = false,
                        count = 0,
                        dSelf = $.Deferred(),
                        resultsCount = 0,
                        geoAllAddedDef = $.Deferred();

                    // если был ответ, разбираем его
                    if (response != null) {

                        // прогоняем все КС перебирая по типу
                        for(var k in response) {

                            arData = [];

                            if (k == 'курьером') {

                                hasCourier = true;
                                $container = $('#courier').show(0).find('.courier__table-body');

                                // каждый раз очищаем контейнер, куда закидываем КС
                                $container.html("");

                                var city = "";

                                // прогоняем все данные
                                $.each(response[k], function(i, item) {

                                    // прогоняем все адреса для доставки в текущей КС
                                    $.each(item.addresses, function(j, addr) {

                                        city = addr.name_city;

                                        arData.push('<tr class="courier__row" data-courier="'+addr.id+'">');

                                        arData.push('<td><div data-toggle="tooltip" class="dostavka_item__checkbox" data-content="'+addr.delivery_news_to_operator+'" class="dostavka_item__checkbox">'+item.name+'</div></td>');
                                        arData.push('<td><span class="days">'+addr.delivery_days+'</span> дн.</td>');
                                        arData.push('<td>от <span class="cost">'+addr.delivery_cost+'</span> р.</td>');

                                        arData.push('</tr>');
                                    });
                                });

                                // склеиваем результат и вставляем в контейнер
                                $container.html(arData.join(""));

                            } else if (k == 'самовывоз') {

                                hasSelf = true;
                                var city = "";

                                $container = $('#self').show(0).find('.courier__table-body');
                                $container.html("");

                                // очищаем карту
                                yMap.geoObjects.removeAll();

                                $.each(response[k], function(i, item) {
                                    $.each(item.addresses, function(j, addr) {
                                        ++resultsCount;
                                        city = addr.name_city;
                                    });
                                });

                                ymaps.geocode(city, {
                                    results: 1
                                }).then(function(res) {

                                    // Выбираем первый результат геокодирования
                                    var firstGeoObject = res.geoObjects.get(0),
                                    // Область видимости геообъекта
                                        bounds = firstGeoObject.properties.get('boundedBy');

                                    // Масштабируем карту на область видимости геообъекта.
                                    yMap.setBounds(bounds, {
                                        checkZoomRange: true
                                    });
                                });

                                $.each(response[k], function(i, item) {

                                    selfData.push('<tr class="courier courier__row active cour_'+i+'">');
                                    selfData.push('<td><a href="#">'+item.name);

                                    var aCount = 0,
                                        aCost = 100000,
                                        aDays = 100000,
                                        collection = new ymaps.GeoObjectCollection(null, { preset: "islands#"+aColors[i]+"Icon" }),
                                        addrLength = item.addresses.length;

                                    yMap.geoObjects.add(collection);

                                    // прогоняем все адреса для доставки в текущей КС
                                    $.each(item.addresses, function(j, addr) {

                                        ++aCount;
                                        if (aCost > addr.delivery_cost)
                                            aCost = addr.delivery_cost;

                                        if (aDays > addr.delivery_days)
                                            aDays = addr.delivery_days;

                                        ymaps.geocode(addr.name_city+", "+addr.delivery_address, {
                                            results: 1
                                        }).then(function(res) {

                                            // Выбираем первый результат геокодирования
                                            var firstGeoObject = res.geoObjects.get(0),
                                                // Координаты геообъекта
                                                coords = firstGeoObject.geometry.getCoordinates();

                                            var placemark = new ymaps.Placemark(coords, {
                                                balloonContentHeader: item.name,
                                                balloonContentBody: "<span class='address'>"+addr.delivery_address+"</span></br></br><p><strong>Как найти:</strong> "+addr.delivery_how_to+"</p><div class='text-center'><button class='btn btn-primary courier__select' data-id='"+addr.id+"'>Выбрать</button></div>",
                                                balloonContentFooter: "<span class='days'>" + addr.delivery_days + "</span> дн., <span class='cost'>" + addr.delivery_cost + "</span>руб.",
                                                clusterCaption: '<strong>'+ item.name +'#'+ count+1 +'</strong>'
                                            });

                                            // добавляем место в текущую коллекцию
                                            collection.add(placemark);

                                            // считаем до конца
                                            ++count;

                                            // по последнему запросу триггерим установку кластера
                                            if (count === resultsCount) {
                                                dSelf.resolve();
                                            }

                                            //
                                            if (aCount === addrLength){
                                                geoAllAddedDef.resolve();
                                            }
                                        });
                                    });

                                    selfData.push(' ('+aCount+')</a></td>');
                                    selfData.push('<td><span class="days">'+aDays+'</span> дн.</td>');
                                    selfData.push('<td>от <span class="cost">'+aCost+'</span> р.</td>');

                                    selfData.push('</tr>');

                                    // когда последний geocode прошел срабатывает resolve
                                    $.when(geoAllAddedDef).then(function() {

                                        // переопределяем контейнер
                                        $container = $('#self').find('.courier__table-body');

                                        // склеиваем результат и вставляем в контейнер
                                        $container.append(selfData.join(""));

                                        // очищаем только после заполнения
                                        selfData = [];

                                        $('.cour_'+i).find('a').toggle(function () {

                                            $(this).parents('tr.courier').removeClass('active');
                                            yMap.geoObjects.remove(collection);
                                        }, function () {

                                            $(this).parents('tr.courier').addClass('active');
                                            yMap.geoObjects.add(collection);
                                        });

                                        //
                                        geoAllAddedDef.promise();
                                    });
                                });
                            } else if (k == 'почта') {

                                hasPost = true;

                                $container = $('#post').show(0).find('.courier__table-body');

                                // каждый раз очищаем контейнер, куда закидываем КС
                                $container.html("");

                                var city = "";

                                // прогоняем все данные
                                $.each(response[k], function(i, item) {

                                    // прогоняем все адреса для доставки в текущей КС
                                    $.each(item.addresses, function(j, addr) {

                                        //сохраняем город для позиционирования
                                        city = addr.name_city;

                                        arData.push('<tr class="courier__row" data-courier="'+addr.id+'">');

                                        arData.push('<td><div class="dostavka_item__checkbox">'+item.name+'</div></td>');
                                        arData.push('<td><span class="days">'+addr.delivery_days+'</span> дн.</td>');
                                        arData.push('<td>от <span class="cost">'+addr.delivery_cost+'</span> р.</td>');

                                        arData.push('</tr>');
                                    });
                                });

                                // склеиваем результат и вставляем в контейнер
                                $container.html(arData.join(""));
                            }
                        }

                        // если не центрировали карту на
                        if (!hasSelf) {

                            if (city == null || city == "") {
                                city = currentCity;
                            }

                            $.when(ymapsDef).then(function() {
                                ymaps.geocode(city, {
                                    results: 1
                                }).then(function(res) {

                                    // Выбираем первый результат геокодирования
                                    var firstGeoObject = res.geoObjects.get(0),
                                    // Координаты геообъекта
                                        coords = firstGeoObject.geometry.getCoordinates(),
                                    // Область видимости геообъекта
                                        bounds = firstGeoObject.properties.get('boundedBy');

                                    // Масштабируем карту на область видимости геообъекта.
                                    yMap.setBounds(bounds, {
                                        checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                                    });
                                });
                            });
                        }

                        // очищаем всё, что само не очистилось
                        if (!hasCourier) {
                            $('#courier').hide(0).find('.courier__table-body').html("");
                        }

                        if (!hasPost) {
                            $('#post').hide(0).find('.courier__table-body').html("");
                        }

                        if (!hasSelf) {
                            $('#self').hide(0).find('.courier__table-body').html("");

                            $.when(ymapsDef).then(function() {
                                // очищаем карту
                                yMap.geoObjects.removeAll();
                            });
                        }

                        $('#couriers__wrapper .dostavka_item [data-toggle=tooltip]').hover(function() {
                            var elem = $(this),
                                content = elem.data('content'),
                                width = 200,
                                elem_l = 0;

                            if (content == "")
                                return;

                            if (elem.data('width') != undefined) {
                                width = elem.data('width');
                                elem_l = - 145;
                            }

                            elem.parent().append('<div style="left:-80px;bottom:'+50+'px;margin-left:'+elem_l+'px;width:'+width+'px" class="dostavka_tooltip">'+content+'</div>');
                            $('.dostavka_tooltip').fadeIn(150)
                        }, function() {

                            var elem = $('.dostavka_tooltip');
                            elem.fadeOut(150);
                            setTimeout(function(){
                                elem.remove();
                            },151);
                        });
                    }
                }
            })
        },
        correctValues: function(){

            console.log('correctValues')

            var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()

            if ($('[name=corpus]').val()!=''){
                q = q + ' корп ' + $('[name=corpus]').val();
            }
            if ($('[name=stroenie]').val()!=''){
                q = q + ' стр ' + $('[name=stroenie]').val();
            }

            var request = DadataApi.clean(q);
            var self = this;
            request.done(function (msg) {
                if (typeof msg !='undefined'){
                    dadata_clear_def.resolve();
                    var data = new Array();
                    data['data']= self.getCleanData(msg);
                    self.showSelected(data,'all');


                }else{
                    dadata_clear_def.reject();
                }

            });
            return request;
        },
        /**
         * получить данные из ответа подсказки
         * @param msg
         * @returns {*}
         */
        getCleanData:function(msg){
            if (typeof (msg.suggestions[0])!='undefined'){
                return msg.suggestions[0]['data'];
            }
        },

        clean:function(){

            var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()

            if ($('[name=corpus]').val()!=''){
                q = q + ' корп ' + $('[name=corpus]').val();
            }
            if ($('[name=stroenie]').val()!=''){
                q = q + ' стр ' + $('[name=stroenie]').val();
            }

            var request = DadataApi.clean(q);
            request.done(function (msg) {
                if (typeof (msg.suggestions[0])!='undefined'){
                    var data =  msg.suggestions[0]['data'];
                }
                if (data!=''){
                    var postal_code = data.postal_code;
                    console.log(postal_code)
                    $('[name=regs_postcode]').val(postal_code)
                    $('[name=address_data]').val(JSON.stringify(data));
                    $('[name=regs_postcode]').val(postal_code)
                }
            });
        }
    };
    window.AddressSuggestions = AddressSuggestions;
})();