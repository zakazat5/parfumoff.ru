function isMobile() {
    try{ document.createEvent("TouchEvent"); return true; }
    catch(e){ return false; }
}

$(function(){

	$('[data-click=show_login_form]').click(function(e){
		e.preventDefault();
		$('#login_form_popup').arcticmodal();
        yaCounter21451555.reachGoal('enter_user_er');
	});

    $('#login_form_register').find('[name=phone]').mask("7(999) 999-99-99");

    $('.phone_mask').mask("7(999) 999-99-99");


    $('form#orders').on('submit',function(){
        //only 1-st step
        if($('.cart_step1_wrap').length>0){
            var elems = $('.cart_step1_wrap').find('input[type="text"]');
            var b_error = 0;
            var count = 0;

            elems.each(function(index,item){
                var item = $(item);
                var val = item.val();
                if (item.hasClass('required') && val == '') {
                    if (count === 0) {
                        item.focus()
                    }
                    item.addClass('error');
                    b_error = 1;
                    var descr = item.next();
                    if (descr.hasClass('error_descr')){
                        descr.text('Поле обязательно для заполнения');
                        descr.show()
                    }
                    count++;
                    console.log('req')
                }
                item.focus(function() {
                    item.removeClass('error');
                    if (item.data('error-text') != '') {
                        var descr = item.next();
                        if (descr.hasClass('error_descr')) descr.hide()
                    }
                });
                item.keyup(function() {
                    item.removeClass('error');
                    if (item.data('error-text') != '') {
                        var descr = item.next();
                        if (descr.hasClass('error_descr')) descr.hide()
                    }
                })
            });

            console.log(b_error);


            if (b_error == 0) {
                return true
            } else {
                return false
            }
        }
    });

    //табы в корзине
    /*
	$('.cart_steps li').click(function(){
		var elem 		= $(this)
		var elem_index 	= elem.index()+1
		var container 	= $('.cart_steps')

		container.find('li').removeClass('active')
		container.find('li:eq('+elem.index()+')').addClass('active')
		$('#cart_step1,#cart_step2,#cart_step3').hide()
		$('#cart_step'+elem_index).show()

	})*/

	$('#cart_step2 .dostavka_item__checkbox').click(function(){
		var elem = $(this);
		var elems = $('.dostavka_item__checkbox');
		var wrap = elem.parents('.dostavka_item');
		var wraps = $('.dostavka_item');

		elems.removeClass('checked');
		elem.addClass('checked');
		wraps.removeClass('active');
		wrap.addClass('active');

		wraps.find('.dop_info').hide();
		wrap.find('.dop_info').show();

		var content_h = parseInt($('#cart_step2 .lCol').height()) + $('#cart_step2 .lCol').offset().top - $('#cart_step2 .slideBlock').height() - 60;
	

		cart_sb_scroll(scroll_top,scroll_block,content_h)
	});

	$('#cart_step3 .lCol .dostavka_item__checkbox').click(function(){
		var elem = $(this);
		var elems = $('.dostavka_item__checkbox');
		var wrap = $('#cart_step3 .pass');
		
		elems.removeClass('checked');
		elem.addClass('checked');
		if (!elem.hasClass('generate')) {
			wrap.show()
		} else {
			wrap.hide()
		}
	});

	$('#cart_step3 .rCol .dostavka_item__checkbox').click(function(){
		var elem = $(this);
		elem.toggleClass('checked')
	});

	//$('.cart_steps li:eq(1)').click()

	$('#cart_step2 .slideBlock .more_tovars a').click(function(e){
		e.preventDefault();
		$(this).parent().slideUp(200);
		$(this).parent().next().slideDown(200)
	});

    $('.korz_kol-vo a').on('click',function(e){
        e.preventDefault();
        var action  = $(this).data('action');
        var uid  = $(this).data('uid');
        $.post('/api/cart/',{action:action,uid:uid},function(result){
            $.each(result.items,function(i,v){
                console.log(v);
                $('.tr_' + v.uid).find('.korz_kol-vo span').text(v.kol);
                $('.tr_' + v.uid).find('.sum').text(v.sum)
            });

            $('.val_kol').text(result.kol);
            $('.val_subTotal').text(result.subTotal);
            $('.val_total').text(result.total);
            //$('.val_discount').text(result.discount)
        });
    });

    if (typeof ($('#cart_step2 .rCol').offset())!='undefined'){
        var scroll_top = $('#cart_step2 .rCol').offset().top;
        var scroll_block = $('#cart_step2 .slideBlock');
        var content_h = parseInt($('#cart_step2 .lCol').height()) + $('#cart_step2 .lCol').offset().top - $('#cart_step2 .slideBlock').height() - 60;

        cart_sb_scroll(scroll_top,scroll_block,content_h)
    }
});

function cart_sb_scroll(scroll_top,scroll_block,content_h) {
	var scroll_block_h = parseInt(scroll_block.height()),
    	win_h = parseInt($(window).height());
    
	if (scroll_block_h > parseInt($('#cart_step2 .lCol').height()) || scroll_block_h > win_h) {
    	$(window).unbind('scroll');
    	scroll_block.removeClass("scroll").removeClass('abs');
    	return
    }

	$('#cart_step2 .rCol').css('height',$('#cart_step2 .lCol').height());
	$(window).unbind('scroll');
	$(window).bind('scroll',function() {
		var st = $(window).scrollTop();

		if ( st > scroll_top - 40) {
			if (!scroll_block.hasClass('scroll')) {
				scroll_block.addClass("scroll")
			}			
		} else {
			scroll_block.removeClass("scroll")
		}

		if (st > content_h) {
			scroll_block.addClass("abs")	
		} else {
			scroll_block.removeClass("abs")
		}
	});
	$('body').animate({ scrollTop: $(window).scrollTop()+1 }, 0);
}
$(function() {
    "use strict";

    // для мобилы выключаем функционал
    if (isMobile()) {
        return false;
    }

    var $sidebar = $('.b-order');

    if ($sidebar.length) {
        var currWidth = Math.floor($sidebar.outerWidth()) - 1,
            topPos = $sidebar.offset().top;
    } else {
        return false;
    }

    $(window).on('scroll.sidebar',function() {
        var curPos = $(window).scrollTop(),
            $content = $sidebar.prev(),
            botPos = $content.offset().top + $content.outerHeight();

        if (curPos >= topPos && curPos < botPos - $sidebar.outerHeight()) {
            $sidebar.css({
                position: 'fixed',
                top: 0,
                left: $content.offset().left + $content.outerWidth(),
                width: currWidth
            });
        } else if (curPos >= botPos - $sidebar.outerHeight()) {
            $sidebar.css({
                position: 'absolute',
                top: $content.outerHeight() - $sidebar.height(),
                left: $content.outerWidth() - 15,
                width: currWidth
            });
        } else {
            $sidebar.css({
                position: 'static',
                top: 'auto',
                left: 'auto',
                width: currWidth
            });
        }
    });
});




//личный камбнет
$(function(){
    $('.nav.nav-tabs li a').click(function(e){
        e.preventDefault();
        $('.nav.nav-tabs li').removeClass('active');
        $(this).parents('li').addClass('active');

        $(this).parents('div').find('.tab-content .tab-pane').hide();

        $(this).parents('div').find('.tab-content .tab-pane' + $(this).attr('href') + '').show();
    });

});