$(function(){
    $("#obr_zvonok,#opt_zvonok").click(function(e){
        e.preventDefault()

        var docHeight = $(document).height();
        $("body").append("<div id='popup_overlay'></div>");
        $("#popup_overlay")
            .height(docHeight)
            .css({
                'opacity' : 0.4,
                'position': 'absolute',
                'top': 0,
                'left': 0,
                'background-color': 'black',
                'width': '100%',
                'z-index': 50000,
                'display': 'none'
            })
            .fadeIn(200)

        var modal = '<div id="obr_zvonok_modal" style="width:400px" class="b-modal">'+
            '<div class="b-modal_close arcticmodal-close">x</div>'+
            '<div class="body">'+
            '<form action="/thanks" method="POST">'+
            '<div style="margin-bottom:20px"><input placeholder="���� ���" name="name" id="zvonok_mail"></div>'+
            '<div style="margin-bottom:20px"><input placeholder="���� �������" name="phone" id="zvonok_phone"></div>'+
            '<div style="margin-bottom:20px"><input placeholder="���� email" name="email" id="zvonok_email"></div>'+
            '<button type="submit" class="btn_purple">���������</button>'+
            '<input type="hidden" name="subject" id="zvonok_subject" />'+
            '</form'+
            '</div>'+
            '</div>'
        $("body").append(modal)

        var modal_obj = $("#obr_zvonok_modal")
        var modal_obj_w = 400
        var modal_obj_h = 280

        if ($(this).attr("id") == 'opt_zvonok')	{
            modal_obj.find('form').attr('action','/thanks-opt/');
            modal_obj.find("#zvonok_phone").parent().remove()
            modal_obj.find("#zvonok_subject").val('������ �� ���')
        } else if ($(this).attr("id") == 'obr_zvonok')	{
            modal_obj.find('form').attr('action','/thanks/');
            modal_obj.find("#zvonok_subject").val('������ �� �������� ������')
        }

        modal_obj.css({
            'margin-left': '-'+modal_obj_w/2+'px',
            'margin-top': '-'+modal_obj_h/2+'px',
        })

        modal_obj.fadeIn(200)
    })






    $("#popup_overlay").live("click",function() {
        $(".b-modal_close").click()
    })

    $("#obr_zvonok_modal").live("submit",function(){
        var form = $(this)
        var inputs = form.find("input")

        inputs.removeClass("error")

        inputs.each(function(index,item){
            var input = $(item)
            var val = input.val()
            if (val == '') input.addClass("error")
        })

        var error = form.find(".error")
        if (error.length > 0) return false

        /*
         var data = form.serialize()
         $.ajax({
         type: 'POST',
         url: '',
         data: data,
         success: function(msg){
         form.html("<div style='font-size:18px'>����������! ��� �������� �������� � ����.</div>")
         setTimeout(function(){
         $(".b-modal_close").click()
         },3000)
         },error: function(msg) {

         }
         })
         return false;
         */
    })

    $(".b-modal_close").live("click",function() {
        $(".b-modal, #popup_overlay").fadeOut(200)
        setTimeout(function(){
            $("#popup_overlay,.b-modal").remove()
        },200)
    })

})