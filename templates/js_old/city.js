$(function(){
    $("#city").click(function(e){
        e.preventDefault()

        var docHeight = $(document).height();
        $("body").append("<div id='popup_overlay'></div>");
        $("#popup_overlay")
            .height(docHeight)
            .css({
                'opacity' : 0.4,
                'position': 'absolute',
                'top': 0,
                'left': 0,
                'background-color': 'black',
                'width': '100%',
                'z-index': 50000,
                'display': 'none'
            })
            .fadeIn(200)

        var modal = $('#city_modal');

        $("body").append(modal)
    })






    $("#popup_overlay").live("click",function() {
        $(".b-modal_close").click()
    })

    $("#obr_zvonok_modal").live("submit",function(){
        var form = $(this)
        var inputs = form.find("input")

        inputs.removeClass("error")

        inputs.each(function(index,item){
            var input = $(item)
            var val = input.val()
            if (val == '') input.addClass("error")
        })

        var error = form.find(".error")
        if (error.length > 0) return false

    })

    $(".b-modal_close").live("click",function() {
        $(".b-modal, #popup_overlay").fadeOut(200)
        setTimeout(function(){
            $("#popup_overlay,.b-modal").remove()
        },200)
    })

})