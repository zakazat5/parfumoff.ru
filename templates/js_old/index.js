function propertyChangeListener(el,property,func,param,forceCheck){
   forceCheck = forceCheck ? forceCheck : property=='value' ? true : false;
   var handler = function(e,o){
      if((o.ie ? e.propertyName.split('.')[0] : e.attrName)==o.prop){
         o.execute(o.param);
      }
   }
   YAHOO.util.Event.onDOMReady(function(){
      var element = YAHOO.lang.isObject(el) ? el : YAHOO.util.Dom.get(el);
      if(YAHOO.env.ua.ie>0 && !forceCheck){
         YAHOO.util.Event.on(element,'propertychange',handler,{ie:true,execute:func,param:param,prop:property});
      }else if(YAHOO.env.ua.webkit || forceCheck){
         var getProp = function(){
            return property=='value' ? element.value : element.getAttribute(property);
         }
         var oldProp = getProp();
         setInterval(function(){
            if(oldProp!=getProp()){
               func(param);
            }
            oldProp = getProp();
         },200);
      }else{
         YAHOO.util.Event.on(element,'DOMAttrModified',handler,{ie:false,execute:func,param:param,prop:property});
      }
   });
}
function changeHandler(){
$(".enter_d.pass label").css("top","-99999px");
}
$(document).ready(function(){
	
	

	
	propertyChangeListener('pass','value',changeHandler,null,null);
	
	$(".enter_d input").attr("value","");
	
	$(".enter_d input").click(function(){
		$(this).prev().css("top","-99999px");
	});
	$(".enter_d input").focus(function(){
		$(this).prev().css("top","-99999px");
	});
	$(".enter_d input").keydown(function(event){
		if (event.keyCode == '0xD') {
			$(".enter_d.pass label").css("top","-99999px");
		}
	});
	$(".enter_d input").dblclick(function(){
		$(".enter_d.pass label").css("top","-99999px");

	});
	$(".enter_d input").blur(function(){
		
		if ($(this).val() == '') {
			$(this).prev().css("top","1px");
		}
	});

	
	

	$("#parf_vipad a#ret").click(function() {
		return false;
	});


	animateIt();
	
	setInterval('animateIt()', 28000);
	

	/*
	var rnd = rand(1, 3);

	if (rnd == 1) {
		$("#fb_group").show();
		$("#vk_group, #tw_group").css("position","absolute");
		$("#vk_group, #tw_group").css("left","-9999px");
		$("#fb_href").addClass("active");
		$("#vk_href, #tw_group").removeClass("active");
	} if (rnd == 2) {
		$("#vk_group").show();
		$("#fb_group, #tw_group").css("position","absolute");
		$("#fb_group, #tw_group").css("left","-9999px");
		$("#vk_href").addClass("active");
		$("#fb_href, #tw_group").removeClass("active");
	} if (rnd == 3) {
		$("#tw_group").show();
		$("#fb_group, #vk_group").css("position","absolute");
		$("#fb_group, #vk_group").css("left","-9999px");
		$("#tw_href").addClass("active");
		$("#fb_href, #vk_group").removeClass("active");
	
	}
	*/
	$("#fb_group").show();
	$("#vk_group, #tw_group").css("position","absolute");
	$("#vk_group, #tw_group").css("left","-9999px");
	$("#fb_href").addClass("active");
	//$(".soc_button").removeClass("active").hide();
	
	/*
	$("#fb_href").click(function() {
		$("#fb_group").show();
		$("#fb_group").css("position","relative");
		$("#fb_group").css("left","0");
		$("#vk_group, #tw_group").css("position","absolute");
		$("#vk_group, #tw_group").css("left","-9999px");
		$(".soc_button").removeClass("active");
		$("#fb_href").addClass("active");
		
		return false
	});
	$("#vk_href").click(function() {
		$("#vk_group").show();
		$("#vk_group").css("position","relative");
		$("#vk_group").css("left","0");
		$("#fb_group, #tw_group").css("position","absolute");
		$("#fb_group, #tw_group").css("left","-9999px");
		$(".soc_button").removeClass("active");
		$("#vk_href").addClass("active");
		return false
	});
	$("#tw_href").click(function() {
		$("#tw_group").show();
		$("#tw_group").css("position","relative");
		$("#tw_group").css("left","0");
		$("#fb_group, #vk_group").css("position","absolute");
		$("#fb_group, #vk_group").css("left","-9999px");
		$(".soc_button").removeClass("active");
		$("#tw_href").addClass("active");
		return false
	});
	*/
	
	
	
	
	

	$('.first_price').click(function(){
		$('.second').hide();
		$('.third').hide();
		$('.fourth').hide();
		$('.first').show();

		$('.first_price').addClass('active_price');
		$('.second_price').removeClass('active_price');
		$('.third_price').removeClass('active_price');
		$('.fourth_price').removeClass('active_price');
	});

	$('.second_price').click(function(){
		$('.first').hide();
		$('.third').hide();
		$('.fourth').hide();
		$('.second').show();

		$('.second_price').addClass('active_price');
		$('.first_price').removeClass('active_price');
		$('.third_price').removeClass('active_price');
		$('.fourth_price').removeClass('active_price');
	});

	$('.third_price').click(function(){
		$('.second').hide();
		$('.first').hide();
		$('.fourth').hide();
		$('.third').show();

		$('.third_price').addClass('active_price');
		$('.second_price').removeClass('active_price');
		$('.first_price').removeClass('active_price');
		$('.fourth_price').removeClass('active_price');
	});

	$('.fourth_price').click(function(){
		$('.second').hide();
		$('.third').hide();
		$('.first').hide();
		$('.fourth').show();

		$('.fourth_price').addClass('active_price');
		$('.second_price').removeClass('active_price');
		$('.third_price').removeClass('active_price');
		$('.first_price').removeClass('active_price');
	});
	
	$(".letterMask, #middle").click(function(el) {
		$(".letterPopup").hide();
		$('.letterMask').hide();
		$(".s_letters a").removeClass("active");
	});
});
function rand( min, max ) { 

 
	if( max ) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	} else {
	   return Math.floor(Math.random() * (min + 1));
    }
}
function animateIt() {

	$(".tizer_banner .table-outer table").animate({ 
		top: "-310px"
	}, 14000 ).animate({ 
		top: "0px"
	}, 14000 );

}

function showLetterPopup(obj) {
	
	$(".letterPopup").hide();
	
	$(".s_letters a").not(obj).removeClass("active");
	
	var object = $(obj);
	var letter = object.text();
	var objPossition = object.position();
	
	if (objPossition.left > 260) {
		objPossition.left = 260;
	}
	if (object.hasClass("active")) {
		
		$(".letterPopup-" + letter).css("left",objPossition.left + "px").css("top", "287px").hide().removeClass("show");
		$(".letterMask").hide();
	} else {
		$(".letterPopup-" + letter).css("left",objPossition.left + "px").css("top", "287px").show().addClass("show");
		$(".letterMask").show();
	}
	
	object.toggleClass("active");
	
	
		
}
