function propertyChangeListener(el,property,func,param,forceCheck){
	forceCheck = forceCheck ? forceCheck : property=='value' ? true : false;
	var handler = function(e,o){
		if((o.ie ? e.propertyName.split('.')[0] : e.attrName)==o.prop){
			o.execute(o.param);
		}
	}
	YAHOO.util.Event.onDOMReady(function(){
		var element = YAHOO.lang.isObject(el) ? el : YAHOO.util.Dom.get(el);
		if(YAHOO.env.ua.ie>0 && !forceCheck){
			YAHOO.util.Event.on(element,'propertychange',handler,{ie:true,execute:func,param:param,prop:property});
		}else if(YAHOO.env.ua.webkit || forceCheck){
			var getProp = function(){
				return property=='value' ? element.value : element.getAttribute(property);
			}
			var oldProp = getProp();
			setInterval(function(){
				if(oldProp!=getProp()){
					func(param);
				}
				oldProp = getProp();
			},200);
		}else{
			YAHOO.util.Event.on(element,'DOMAttrModified',handler,{ie:false,execute:func,param:param,prop:property});
		}
	});
}

function changeHandler(){
	$(".enter_d.pass label").css("top","-99999px");
}

function check_form(form_obj) {
	
	var elems = form_obj.find('input[type="text"]')
	//form_obj.find('input.phone_input').mask("(999) 999-99-99")
				
	form_obj.submit(function(e) {

		var b_error = 0
		var count = 0

		elems.each(function(index,item){
			var item = $(item)
			var val = item.val()

			if (item.hasClass('required') && val == '') {
				if (count === 0) {
					item.focus()
				}
				item.addClass('error')
				b_error = 1
				var descr = item.next()
				if (descr.hasClass('error_descr')) descr.show()
				count++
                console.log('req')
			}
			
			item.focus(function() {
				item.removeClass('error')
				if (item.data('error-text') != '') {
					var descr = item.next()
					if (descr.hasClass('error_descr')) descr.hide()
				}
			})
			item.keyup(function() {
				item.removeClass('error')
				if (item.data('error-text') != '') {
					var descr = item.next()
					if (descr.hasClass('error_descr')) descr.hide()
				}
			})
		})

        console.log(b_error)

		
		if (b_error == 0) {
			return true
		} else {
			return false
		}
	})
}



$(document).ready(function(){
	$(".enter_d input").attr("value","");
	$("#how-to-get-skidka").mouseenter(function(){
		$(this).find('.how-to-get-skidka').show();
	});
	
	$(".how-to-get-skidka").mouseleave(function() {
		$('.how-to-get-skidka').hide();
	});
	
	$('.hide_text_value').click(function(){
	
		var text=$(this).attr('rel');
		
		if ($(this).val() == text || $(this).val() == '') {
			$(this).attr('value','');
		}
		
		$('.hide_text_value').blur(function(){
			if ($(this).val() == '') {
				$(this).attr('value',text);
			}
		});
	
	});
	
	$(".slider_text").click(function() {
	
		$(this).next().slideToggle();
		
	});
	
	$("#back_to").click(function() {
	
		$(this).next().toggle();
		return false;
	});
	
	propertyChangeListener('pass','value',changeHandler,null,null);

	$(".enter_d input").click(function(){
		$(this).prev().css("top","-99999px");
	});
	$(".enter_d input").focus(function(){
		$(this).prev().css("top","-99999px");
	});
	$(".enter_d input").keydown(function(event){
		if (event.keyCode == '0xD') {
			$(".enter_d.pass label").css("top","-99999px");
		}
	});
	$(".enter_d input").dblclick(function(){
		$(".enter_d.pass label").css("top","-99999px");

	});
	$(".enter_d input").blur(function(){

		if ($(this).val() == '') {
			$(this).prev().css("top","1px");
		}
	});
	
	
	$("#radio2").click(function() {
		if ($(this).val() == "on" || $(this).val() == "2") {
			$("#town-wrap").show();
		}
	});
	
	$("#radio1").click(function() {
		$("#town-wrap").hide();
	});
	
	$("#pass3").click(function() {
		if ($(this).val() == "on" || $(this).val() == "2") {
			$("#pass-wrap").show();
		}
	});
	
	$("#pass2").click(function() {
			$("#pass-wrap").hide();
	});
	
	$(".why-need").mouseover(function() {
	
		$(this).next().show();
	
	});
	$(".why-need").mouseout(function() {
	
		$(this).next().hide();
	
	});
	
	

	$(".nakop_sk").mouseover(function() {

		$(this).next().show();
		$(".nakop_sk").mouseleave(function() {

			$(this).next().hide();

		});
	});


	/*
	var rnd = rand(1, 3);

	if (rnd == 1) {
		$("#fb_group").show();
		$("#vk_group, #tw_group").css("position","absolute");
		$("#vk_group, #tw_group").css("left","-9999px");
		$("#fb_href").addClass("active");
		$("#vk_href, #tw_group").removeClass("active");
	} if (rnd == 2) {
		$("#vk_group").show();
		$("#fb_group, #tw_group").css("position","absolute");
		$("#fb_group, #tw_group").css("left","-9999px");
		$("#vk_href").addClass("active");
		$("#fb_href, #tw_group").removeClass("active");
	} if (rnd == 3) {
		$("#tw_group").show();
		$("#fb_group, #vk_group").css("position","absolute");
		$("#fb_group, #vk_group").css("left","-9999px");
		$("#tw_href").addClass("active");
		$("#fb_href, #vk_group").removeClass("active");
	
	}
	*/
	$("#fb_group").show();
	$("#vk_group, #tw_group").css("position","absolute");
	$("#vk_group, #tw_group").css("left","-9999px");
	$("#fb_href").addClass("active");
	//$(".soc_button").removeClass("active").hide();
	
	/*
	$("#fb_href").click(function() {
		$("#fb_group").show();
		$("#fb_group").css("position","relative");
		$("#fb_group").css("left","0");
		$("#vk_group, #tw_group").css("position","absolute");
		$("#vk_group, #tw_group").css("left","-9999px");
		$(".soc_button").removeClass("active");
		$("#fb_href").addClass("active");
		
		return false
	});
	$("#vk_href").click(function() {
		$("#vk_group").show();
		$("#vk_group").css("position","relative");
		$("#vk_group").css("left","0");
		$("#fb_group, #tw_group").css("position","absolute");
		$("#fb_group, #tw_group").css("left","-9999px");
		$(".soc_button").removeClass("active");
		$("#vk_href").addClass("active");
		return false
	});
	$("#tw_href").click(function() {
		$("#tw_group").show();
		$("#tw_group").css("position","relative");
		$("#tw_group").css("left","0");
		$("#fb_group, #vk_group").css("position","absolute");
		$("#fb_group, #vk_group").css("left","-9999px");
		$(".soc_button").removeClass("active");
		$("#tw_href").addClass("active");
		return false
	});
	*/
	
	
	
	
	
	
	$("#parf_vipad a#ret").click(function() {
		return false;
	});




	$('#to_blog').click(function(){
		$('#to_blog_wrap').toggle();
		$(this).toggleClass('to_blog_class');
	});
	$('#how').click(function(){
		$('#how_look').toggle();
		return false;
	});

	$("#textarea_to_blog").focus(function () {
		$(this).select();
	}).mouseup(function(e){
		e.preventDefault();
	});


	$('.lines #first').click(function(){
		$('.act').removeClass('act');
		$(this).addClass('act');
		$('.first').show();
		$('.second').hide();
	});
	$('.lines #second').click(function(){
		$('.act').removeClass('act');
		$(this).addClass('act');
		$('.first').hide();
		$('.second').show();
	});

	$('.first_price').click(function(){
		$('.second').hide();
		$('.third').hide();
		$('.fourth').hide();
		$('.first').show();

		$('.first_price').addClass('active_price');
		$('.second_price').removeClass('active_price');
		$('.third_price').removeClass('active_price');
		$('.fourth_price').removeClass('active_price');
	});

	$('.second_price').click(function(){
		$('.first').hide();
		$('.third').hide();
		$('.fourth').hide();
		$('.second').show();

		$('.second_price').addClass('active_price');
		$('.first_price').removeClass('active_price');
		$('.third_price').removeClass('active_price');
		$('.fourth_price').removeClass('active_price');
	});

	$('.third_price').click(function(){
		$('.second').hide();
		$('.first').hide();
		$('.fourth').hide();
		$('.third').show();

		$('.third_price').addClass('active_price');
		$('.second_price').removeClass('active_price');
		$('.first_price').removeClass('active_price');
		$('.fourth_price').removeClass('active_price');
	});

	$('.fourth_price').click(function(){
		$('.second').hide();
		$('.third').hide();
		$('.first').hide();
		$('.fourth').show();

		$('.fourth_price').addClass('active_price');
		$('.second_price').removeClass('active_price');
		$('.third_price').removeClass('active_price');
		$('.first_price').removeClass('active_price');
	});


	$('#other_aromats_wr').show();
	var kart_height = $('#content').height();
	var kart_height1 = kart_height - 193;

	var content = $('#content').height();
	var content_height1 = content + 14;
	var sb = $('.sidebar').height();
	var sb_height1 = sb + 14;
	if (content_height1 > sb_height1) {$('.sidebar').height(content_height1);}
	$('#other_aromats').addClass('act1');

	$("#other_aromats").click(function() {
		$('#other_aromats_wr').show();
		$('#com_wr').hide();
		$('#sh_price_wr').hide();

		$('#other_aromats').addClass('act1');
		$('#otzivi').removeClass('act1');
		$('#sh_price').removeClass('act1');
		var kart_height = $('#content').height();
		var kart_height1 = kart_height - 193;

		var content = $('#content').height();
		var content_height1 = content + 14;
		var sb = $('.sidebar').height();
		var sb_height1 = sb + 14;
		if (content_height1 > sb_height1) {$('.sidebar').height(content_height1);}
	});
	$("#otzivi").click(function() {
		$('#com_wr').show();
		$('#other_aromats_wr').hide();
		$('#sh_price_wr').hide();

		$('#otzivi').addClass('act1');
		$('#other_aromats').removeClass('act1');
		$('#sh_price').removeClass('act1');
		var kart_height = $('#content').height();
		var kart_height1 = kart_height - 193;
		$('.r_menu').height(kart_height1);
		var content = $('#content').height();
		var content_height1 = content + 14;
		var sb = $('.sidebar').height();
		var sb_height1 = sb + 14;
		if (content_height1 > sb_height1) {$('.sidebar').height(content_height1);}

	});
	$("#sh_price").click(function() {
		$('#sh_price_wr').show();
		$('#com_wr').hide();
		$('#other_aromats_wr').hide();

		$('#sh_price').addClass('act1');
		$('#otzivi').removeClass('act1');
		$('#other_aromats').removeClass('act1');
		var kart_height = $('#content').height();
		var kart_height1 = kart_height - 193;
		var content = $('#content').height();
		var content_height1 = content + 14;
		var sb = $('.sidebar').height();
		var sb_height1 = sb + 14;
		if (content_height1 > sb_height1) {$('.sidebar').height(content_height1);}

	});


	$("#acc_but_find").click( function () {
		var sel_brend = $("#acc_sel_brend option:selected");
		var sel_type = $("#acc_sel_type option:selected");

		if( sel_type.val() == 0 && sel_brend.val() == 0 ){
			window.location = '/accessories/';
		}else{
			window.location = '/accessories/filter/type/' + sel_type.val() + '/brend/' + sel_brend.val() + '/';
		}
	});

	
	
	// ========================
	// suv: NOTE
	//
	
	// add
	$(".bloknot").click(function(){

		var id = $(this).attr('id');
		var obj = $(this);

		$.post("/protect/note.php",{
			type: 'add',
			id: id
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				$( obj ).addClass('active');
			});
		});
	});
	
	// delete
	$(".bloknot_del").click(function(){

		var id = $(this).attr('id');
		var note_item = '.note-' + id;
		
		$.post("/protect/note.php",{
			type: 'del',
			id: id
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				$( note_item ).animate({ opacity: "hide" }, "slow");
			});
		});
	});
	
	$(".letterMask, #middle").click(function(el) {
		$(".letterPopup").hide();
		$('.letterMask').hide();
		$(".s_letters a").removeClass("active");
	});



    //select current region

});



function set_user_region()
{
    var code = $('#region_code').val();
    $.post('',{cmd:'set_region_code',code:code},function(d){
        location.reload();
    });
}




function rand( min, max ) { 

 
	if( max ) {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	} else {
	   return Math.floor(Math.random() * (min + 1));
    }
}

function showLetterPopup(obj) {
	
	$(".letterPopup").hide();
	
	$(".s_letters a").not(obj).removeClass("active");
	
	var object = $(obj);
	var letter = object.text();
	var objPossition = object.position();
	
	if (objPossition.left > 260) {
		objPossition.left = 260;
	}
	if (object.hasClass("active")) {
		
		$(".letterPopup-" + letter).css("left",objPossition.left + "px").css("top", "287px").hide().removeClass("show");
		$(".letterMask").hide();
	} else {
		$(".letterPopup-" + letter).css("left",objPossition.left + "px").css("top", "287px").show().addClass("show");
		$(".letterMask").show();
	}
	
	object.toggleClass("active");
	
	
		
}

$.fn.stickyfloat = function(options, lockBottom) {
    var $obj 				= this;
    var parentPaddingTop 	= parseInt($obj.parent().css('padding-top'));
    var startOffset 		= $obj.parent().offset().top;
    var opts 				= $.extend({ startOffset: startOffset, offsetY: parentPaddingTop, duration: 200, lockBottom:true, container_selector:'.container'}, options);

    if (!opts.onlycss) $obj.css({ position: 'absolute' });

    if(opts.lockBottom){
        var container = $(opts.container_selector);
        var bottomPos = container.height() + container.offset().top;
        if( bottomPos < 0 )
            bottomPos = 0;
    }

    $(window).scroll(function () {

        console.log(opts.startOffset);


        var pastStartOffset			= $(document).scrollTop() > (opts.startOffset-20);
        var objFartherThanTopPos	= $obj.offset().top > startOffset;
        var objBiggerThanWindow 	= $obj.outerHeight() < $(window).height();
        if (opts.onlycss) {

            if(pastStartOffset && objBiggerThanWindow){
                var newpos = ($(document).scrollTop() + 20);
                var obj_scroll_top = newpos + $obj.outerHeight() - 20;
                if ( obj_scroll_top > bottomPos ) {
                    $obj.addClass("absolute").removeClass("fixed").removeClass("relative")
                } else {
                    $obj.addClass("fixed").removeClass("absolute").removeClass("relative")
                }
            } else {
                $obj.addClass("relative").removeClass("absolute").removeClass("fixed")
            }

        } else {
            $obj.stop();
            if( (pastStartOffset || objFartherThanTopPos) && objBiggerThanWindow ){
                var newpos = ($(document).scrollTop() -startOffset + opts.offsetY);
                if ( newpos > bottomPos )
                    newpos = bottomPos;
                if ( $(document).scrollTop() < opts.startOffset )
                    newpos = parentPaddingTop;

                $obj.animate({ top: newpos }, opts.duration );
            }
        }
    });
};

function escapeHtml(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;");
}