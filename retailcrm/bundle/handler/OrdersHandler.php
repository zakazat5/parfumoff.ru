<?php
//Сделать соответствие статусов
class OrdersHandler implements  HandlerInterface {

    public function prepare($data)
    {
        $container = Container::getInstance();
        $status = $container->settings['status'];
        $managerId = $container->settings['manager'];

        $ordersCRM = [];
        /**
         * @var ApiClient $api
         */
        $api = new RequestProxy($container->settings['api']['url'], $container->settings['api']['key']);

        foreach ($data as $order) {
            //Проверка на существование заказов

            $order['externalId'] = $container->settings['general']['prefix_site'] . $order['externalId'];

            $r = $api->ordersList(array('externalId' => $order['externalId']));
            if (!$r['orders']) {
                $getItems = $this->getOrderItems($order['items']);
                $customerFIO = DataHelper::explodeFIO($order['fio']);

                //проверка на существование клиента в срм
                if(isset($order['customerId']) && !empty($order['customerId'])) {
                    $res = $api->customersGet($order['customerId']);
                    if(!$res->isSuccessful()){
                        $customer = array(
                            'externalId' => $order['customerId'],
                            'firstName' => $customerFIO['firstName'],
                            'lastName' => $customerFIO['lastName'],
                            'patronymic' => $customerFIO['patronymic'],
                            'email' => $order['email'],
                            'phones' => array(
                                '0' => array(
                                    'number' => $order['phone']
                                )
                            )
                        );
                        $api->customersCreate($customer);
                    }
                }

                $ordersCRM[] = array(
                    'number' => $order['externalId'],
                    'externalId' => $order['externalId'],
                    'customerId' => $order['customerId'],
                    'managerId' => array_key_exists($order['managerId'], $managerId) ? $managerId[$order['managerId']] : null,
                    'email' => $order['email'],
                    'firstName' => $customerFIO['firstName'],
                    'lastName' => $customerFIO['lastName'],
                    'patronymic' => $customerFIO['patronymic'],
                    'phone' => $order['phone'],
                    'createdAt' => $order['createdAt'],
                    'discountPercent' => $order['discountPercent'],
                    'paymentType' => $order['paymentType'],
                    'managerComment' => $order['managerComment'],
                    'delivery' => array(
                        'date' => (
                            $order['DateEnd'] != "0000-00-00 00:00:00"?
                                date("Y-m-d", strtotime($order['DateEnd'])):
                                null),
                        'address' => array(
                            'city' => $order['city'],
                            'region' => $order['region'],
                            'index' => $order['postcode'],
                            'street' => $order['street'],
                            'building' => $order['building'],
                            'block' => $order['block'],
                            'house' => $order['house'],
                            'floor' => $order['floor'],
                            'intercomCode' => $order['intercomCode'],
                            'flat' => $order['flat'],
                            'text' => $order['pAddress']
                        ),
                    ),
                    'status' => $status[$order['status']],
                    'orderMethod' => ($order['is_phone'] == 1?'phone':'shopping-cart'),
                    'source' => array(
                        'source' => $order['utm_source_last'],
                        'medium' => $order['utm_medium_last'],
                        'campaign' => $order['utm_campaign_last'],
                        'keyword' => $order['utm_term_last'],
                    ),
                    //Не забыть создать пользовательские поля
                    'customFields' => array(
                        'settlement_orders' => $order['settlement'], //Населенный пункт если есть
                        'margin2' => $getItems[1],
                        'delivery_self' => $order['delivery_self'],
                    ),
                    'items' => $getItems[0],
                );
            }
            //обновление первоисточника
            /*$r = $api->ordersList(array('email' => $order['email']));
            $minDate = date('Y-m-d h:i:s');
            $minId = 0;
            $source = '';
            foreach ($r['orders'] as $orderCrm){
                if(($minDate > $orderCrm['createdAt']) && isset($orderCrm['source']['source']) && ($orderCrm['source']['source'] != '-') ){
                    $minDate = $orderCrm['createdAt'];
                    $minId = $orderCrm['customer']['externalId'];
                    $source = $orderCrm['source']['source'];
                }

            }
            $api->customersEdit( array('externalId' => $minId, 'customFields' => array('primarysource' => $source)) );*/
        }
        return $ordersCRM;
    }

    private function getOrderItems($items) {
        $result = array();
        if (!$items) {return $items;}
        $itemsStrings = explode('|', $items);
        $allMargin = 0;
        foreach ($itemsStrings as $key => $is) {
            $itemString = explode('~', $is);
            $resultItems[$key] = array(
                'productId' => $itemString[0],
                'productName' => $itemString[1],
                'quantity' => $itemString[2],
                'initialPrice' => $itemString[3],
                'properties' => array(
                    ($itemString[4] != ''?
                    array(
                        'code' => 'article',
                        'name' => 'Артикул',
                        'value' => $itemString[4]
                    ):null),
                    ($itemString[5] != ''?
                    array(
                        'code' => 'margin',
                        'name' => 'Маржа',
                        'value' => $itemString[5]

                    ):null),
                    ($itemString[6] != ''?
                    array(
                        'code' => 'sklad',
                        'name' => 'Склад',
                        'value' => $itemString[6]
                    ):null),
                    array(
                        'code' => 'pol',
                        'name' => 'Пол',
                        'value' => trim($itemString[1])[1] == 'F' ? "Женский" : "Мужской"
                    ),
                ),
            );

            $resultItems[$key]['properties'] = array_filter($resultItems[$key]['properties']);
            $allMargin = $allMargin + ($itemString[5] * $itemString[2]);
        }
        $result[0] = $resultItems;
        $result[1] = $allMargin;
        return $result;
    }

}
