<?php

class OffersHandler implements HandlerInterface
{
    public function prepare($offers)
    {
        $categoryId = array(
            'F' => '2',
            'M' => '3',
        );

        $offersCRM = [];
        foreach ($offers as $value) {
            $offer = array(
                'id' => $value['id'],
                'name' => DataHelper::getName($value),
                'productId' => $value['productId'],
                'url' => $value['url'],
                'price' => $value['price'],
                'picture' => $value['picture'],
//                'productActivity' => $value['price'] == 0 ? 'N' : 'Y',
                'categoryId' => $value['categoryId'],
                'params' => array(
                    'article' => array(
                        'code' => 'article',
                        'name' => 'Артикул',
                        'value' => $value['article']
                    ),
                    'pol' => array(
                        'code' => 'pol',
                        'name' => 'Пол',
                        'value' => $value['pol'] == "M" ? "Мужской" : "Женский"
                    ),
                ),
            );
            $offersCRM[] = $offer;
        }
        return $offersCRM;
    }
}
