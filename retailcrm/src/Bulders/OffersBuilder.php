<?php

class OffersBuilder extends Builder
{
    /**
     * getOffers
     *
     * @return array
     */
    public function buildOffers($step, $chuck)
    {
        $query = $this->rule->getSQL('offers');
        $handler = $this->rule->getHandler('OffersHandler');
        $this->sql = $this->container->db->prepare($query);
        $this->sql->bindParam(':step', $step, PDO::PARAM_INT);
        $this->sql->bindParam(':chuck', $chuck, PDO::PARAM_INT);
        return $this->build($handler);
    }

    public function getCount()
    {
        $query = $this->rule->getSQL('offers_count');
        $this->sql = $this->container->db->prepare($query);
        $this->sql->execute();
        return $this->sql->fetch()[0];
    }
}
