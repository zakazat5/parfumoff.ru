<?php

class DataHelper
{
    public static function getDate($file)
    {
        if (file_exists($file) && 1 < filesize($file)) {
            $result = file_get_contents($file);
        } else {
            $result = date(
                'Y-m-d H:i:s',
                strtotime('-1 days', strtotime(date('Y-m-d H:i:s')))
            );
        }

        return $result;
    }

    public static function filterRecursive($haystack)
    {
        foreach ($haystack as $key => $value) {
            if (is_array($value)) {
                $haystack[$key] = self::filterRecursive($haystack[$key]);
            }

            if (is_null($haystack[$key]) || $haystack[$key] === '' || count($haystack[$key]) == 0) {
                unset($haystack[$key]);
            } elseif (!is_array($value)) {
                $haystack[$key] = trim($value);
            }
        }

        return $haystack;
    }

    public static function explodeFIO($string)
    {
        $result = array();
        $parse = (!$string) ? false : explode(" ", $string, 3);

        switch (count($parse)) {
            case 1:
                $result['firstName'] = $parse[0];
                $result['lastName'] = '';
                $result['patronymic'] = '';
                break;
            case 2:
                $result['firstName'] = $parse[1];
                $result['lastName'] = $parse[0];
                $result['patronymic'] = '';
                break;
            case 3:
                $result['firstName'] = $parse[1];
                $result['lastName'] = $parse[0];
                $result['patronymic'] = $parse[2];
                break;
            default:
                return false;
        }

        return $result;
    }

    public static function explodeUids($uids)
    {
        $uids   = explode(',', $uids);
        $ranges = array();

        foreach ($uids as $idx => $uid) {
            if (strpos($uid, '-')) {
                $range = explode('-', $uid);
                $ranges = array_merge($ranges, range($range[0], $range[1]));
                unset($uids[$idx]);
            }
        }

        $uids = implode(',', array_merge($uids, $ranges));

        return $uids;
    }

    public static function getName($offer)
    {
        //type, pol, vendor, name, v
        $types = array(
            'TW' => 'Туалетная вода',
            'TTW' => 'Туалетная вода тестер',
            'TD' => 'Туалетные духи',
            'TT' => 'Туалетные духи тестер',
            'PD' => 'parfum de toilette',
            'PDT' => ' parfum de toilette тестер',
            'D' => 'Дезодорант',
            'DT' => 'Дезодорант тестер',
            'DR' => 'Роликовый дезодорант',
            'DC' => 'Кремовый дезодорант',
            'DS' => 'Дезодорант стик',
            'O' => 'Одеколон',
            'OT' => 'Одеколон тестер',
            'P' => 'Парфюм',
            'PT' => 'Парфюм тестер',
            'EX' => 'Экстракт',
            'PO' => 'OSMO-PARFUM',
            'BL' => 'Лосьон для тела',
            'SG' => 'Гель для душа',
            'BS' => 'Спрей для тела',
            'A' => 'Аксессуар',
            'AS' => 'Лосьон после бритья',
            'BLT' => 'Лосьон для тела тестер',
            'SGT' => 'Гель для душа тестер',
            'BC' => 'Крем для тела',
            'SM' => 'Мусс для волос',
            'BM' => 'Молочко для тела',
            'AST' => 'Лосьон после бритья тестер',
            'ASB' => 'Бальзам после бритья',
            'ASBT' => 'Бальзам после бритья тестер',
            'ASG' => 'Гель после бритья',
            'ASE' => 'Эмульсия после бритья',
            'MN' => 'Миниатюра',
        );

        $offer['name'] = str_replace('&#039;', "`", $offer['name']);
        $offer['name'] = str_replace('&amp;#039;', "`", $offer['name']);
        $offer['name'] = str_replace('&raquo;', '"', $offer['name']);
        $offer['name'] = str_replace('&laquo;', '"', $offer['name']);
        $offer['vendor'] = str_replace('&#039;', "`", $offer['vendor']);
        $offer['vendor'] = str_replace('&amp;#039;', "`", $offer['vendor']);

        //[пол], бренд Названия аромата, Типа товара, Объема(мл). В приведенном примере делим так Caron / Nocturnes / Парфюмированная вода / 100 мл.
        if (array_key_exists($offer['type'], $types)) {
            return '[' . $offer['pol'] . '] ' . $offer['vendor'] . ' ' . $offer['name'] . ' ' . $types[$offer['type']] . ' ' . $offer['v'] . ' мл';
        }
        if ($offer['type'] == 'N' || $offer['type'] == 'US') {
            return '[' . $offer['pol'] . '] ' . $offer['vendor'] . ' ' . $offer['name'] . ' Набор ' . $offer['v'];
        }
        return '[' . $offer['pol'] . '] ' . $offer['vendor'] . ' ' . $offer['name'];
    }
}
