$(document).ready(function(){
	Nifty("div.kard_t_1","big");
	
	$("#category").mcDropdown("#categorymenu",
	{
		minRows: 8                  // specify the minimum rows before creating a new column
		, maxRows: 400                  // specify the maximum rows in a column
		, targetColumnSize: 3          // specify the default target column size (it will attempt to create
		// this many columns by default, unless the min/max row rules are not being met)
		, openFx: "show"          // the fx to use for showing the root menu
		, openSpeed: 0               // the speed of the openFx
		, closeFx: "hide"           // the fx to use for hiding the root menu
		, closeSpeed: 0              // the speed of the closeFx
		, hoverOverDelay: 200          // the delay before opening a submenu
		, hoverOutDelay: 200             // the delay before closing a submenu
		, showFx: "show"               // the fx to use when showing a submenu
		, showSpeed: 0                 // the speed of the showFx
		, hideFx: "hide"               // the fx to use when closing a submenu
		, hideSpeed: 0                 // the speed of the hideFx
		, screenPadding: 1            // the padding to use around the border of the screen
		// (this is used to make sure items stay on the screen)
		, dropShadow: false             // determine whether drop shadows should be shown on the submenus
		, autoHeight: true             // always uses the lineHeight options (much faster than calculating height)
		, valueAttr: "rel"             // the attribute that contains the value to use in the hidden field
	}
	);


	$("#example > ul").tabs();

	$(".korzina").click(function(){
		var id = $(this).attr('id');

		$.post("/protect/price.php",{
			id: id
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				$("#korzina_sum").html( $("sum",message).text() );
				$("#korzina_kol").html( $("kol",message).text() );
			});
		});
	});
});