/**
 * Created by loram on 20.06.14.
 */
function orderActions() {

    if (!$('#grid_table').find('.trSelected').length)
        return false;

    $('#download__modal').modal('show');
}

$(document).on('click', '.print_action_link', function(e) {
    e.preventDefault();
    var $this = $(this);

    $this.closest('table').find('tr').removeClass('trSelected');
    $this.closest('tr').addClass('trSelected');

    $('#download__modal').find('[data-action="sklad_print"]').trigger('click');
});

$(document).ready(function(){

    $("#filter_user__phone").mask("+7 (999) 999-9999");
    $('.form-datepicker').bootstrap_datepicker();

    $('#make__download').on('click', function(e) {
        e.preventDefault();

        if (!$('#grid_table').find('.trSelected').length)
            return;

        $('#download__modal').modal('show');
    });

    $('#download__list').find('.option-title').on('click', function(e) {
        e.preventDefault();

        var $this   = $(this),
            module  = $this.data('module'),
            action  = $this.data('action'),
            addict  = typeof $this.data('addict') == 'undefined' ? "" : $this.data('addict'),
            url     = $this.data('url'),
            items   = $('#grid_table').find('.trSelected'),
            itemlist = '';

        for (var i = 0; i < items.length; i++) {
            itemlist += items[i].id.substr(3) + ",";
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "?ajax=order_actions&action="+action+addict,
            data: {
                items: itemlist
            },
            success: function (data) {

                window.open("/images/uploads/"+url);
            }
        });
    });
});