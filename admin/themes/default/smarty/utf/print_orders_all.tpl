<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="content-type" content="text/html; charset=windows-1251" />
	<meta name="title" content="" />
	<meta name="description" content="" />
	{literal}
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
	<script type="text/javascript">
        $(function(){
            var $zakaz = $(".zakaz"),
                len = $zakaz.length -1;

            $zakaz.each(function(index,item) {

                var elem = $(item),
                    elem_h = elem.outerHeight(),
                    body_h = 1015;

                if (index % 2) {
                    if (elem_h > body_h) {
                        elem.addClass("one4page");
                    } else {
                        elem.css("min-height",(body_h/2) +"px");
                        if (index != len) elem.css("page-break-after","always");
                    }
                } else {
                    if (elem_h > body_h/2) {
                        elem.addClass("one4page");
                        elem.next().css("margin-top","20px");
                    } else {
                        elem.css("min-height",(body_h/2)-100 +"px");
                    }
                }
            });
        });
	</script>

	<style type="text/css">
	@page {
	margin: 0;
	padding:0
	}
	p {margin-top:0}
	@media print {
		.one4page {
			page-break-after:always;
			border:0 !important;
		}
		.hidden_print {
			display:none;
		}
	}
	</style>
	{/literal}
	
</head>


<body style="width:725px;font-family:Arial;margin:0 auto;padding:0;">



<table align="center" style="padding:5px;width: 100%;font-family:Arial;" valign="middle" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		
{$order_items}

		</td>
	</tr>
</table>
</body>

</html>