<?php /* Smarty version 2.6.18, created on 2014-10-28 15:30:43
         compiled from menu.tpl */ ?>
<div class="drawers-wrapper">
    	
	<ul class="drawers">
	<li class="drawer">
		<h2 class="drawer-handle content">КОНТЕНТ САЙТА</h2>
		<ul>
			<?php $_from = $this->_tpl_vars['item_menu']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['curr_key'] => $this->_tpl_vars['curr_item']):
?>
				<?php if ($this->_tpl_vars['curr_item']['is_menu'] == '1'): ?>
					<li class="item_menu sub"><img src="images/header/arrow.gif" width="3" height="5">
						<?php echo $this->_tpl_vars['curr_item']['name']; ?>

					</li>
				
				<?php elseif ($this->_tpl_vars['curr_item']['type'] == 'links' || $this->_tpl_vars['curr_item']['type'] == 'exist_cat'): ?>
					<li class="item_menu" >
						&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
						<span style="text-decoration:underline;color:#5e6aa7;"><?php echo $this->_tpl_vars['curr_item']['name']; ?>
</span>
					</li>
				<?php else: ?>
					<?php if ($_GET['id_cat'] == $this->_tpl_vars['curr_item']['id']): ?>
					<li class="item_menu act" style="background-color:#f2f5f9;">
					<?php else: ?>
					<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">
					<?php endif; ?>
					
					&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
					<a href="?main=content&module=<?php echo $this->_tpl_vars['curr_item']['type']; ?>
&action=mod&id_cat=<?php echo $this->_tpl_vars['curr_item']['id']; ?>
"><?php echo $this->_tpl_vars['curr_item']['name']; ?>
</a>
					</li>
				<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		</ul>
		
		<h2 class="drawer-handle banner">МАГАЗИН</h2>
		<ul>
			<?php if ($_GET['module'] == 'mag_orders'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_orders&action=mod&status=1">Заказы</a>
			</li>
			

			<?php if ($_GET['module'] == 'mag_orders_sklad'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_orders_sklad&action=mod&status=2">Склад</a>
			</li>


			<?php if ($_GET['module'] == 'mag_note'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_note&action=mod">Блокнот</a>
			</li>
			
			<?php if ($_GET['module'] == 'mag_client'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_client&action=mod">Клиенты магазина</a>
			</li>
			
			<?php if ($_GET['module'] == 'mag_comment'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_comment&action=mod">Коментарии</a>
			</li>
			
			<?php if ($_GET['module'] == 'mag_comment_ar'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_comment_ar&action=mod">Коментарии внутреннии</a>
			</li>
		
			<!--<?php if ($_GET['module'] == 'mag'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag&action=mod&status=1">Заказы</a>
			</li>-->
			
			<?php if ($_GET['module'] == 'param'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=param&action=mod">Цены</a>
			</li>
			
			<?php if ($_GET['module'] == 'sklad'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=sklad&action=mod">Склады</a>
			</li>
			
			<?php if ($_GET['module'] == 'mag_market'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_market&action=mod">Ya Market</a>
			</li>
			
			<?php if ($_GET['module'] == 'mag_subs'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_subs&action=mod">Рассылка</a>
			</li>

            <?php if ($_GET['module'] == 'catalog_moder_k'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
            <?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=mag&module=catalog_moder_k&action=mod">Модерация (косметика)</a>
            </li>

            <?php if ($_GET['module'] == 'catalog_moder'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
            <?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=mag&module=catalog_moder&action=mod">Модерация (парфюмерия)</a>
            </li>

            <?php if ($_GET['module'] == 'catalog_moder'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
            <?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=mag&module=catalog_moder_log&action=mod">Модерация отчет (парфюмерия)</a>
            </li>


			
		</ul>

        <h2 class="drawer-handle banner">Логистика</h2>
        <ul>
            <li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=logistic&module=logistic&action=index">Управление логистикой</a>
            </li>
            <li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=logistic&module=logistic&action=upload">Выгрузки/Загрузки</a>
            </li>
        </ul>
		
		<h2 class="drawer-handle banner">РЕКЛАМА</h2>
		<ul>
			<?php if ($_GET['module'] == 'banner'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=banner&module=banner&action=mod">Управление баннерами</a>
			</li>
			
			<?php if ($_GET['module'] == 'banner_client'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=banner&module=banner_client&action=mod">Клиенты</a>
			</li>
			
			<?php if ($_GET['module'] == 'banner_shablon'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=banner&module=banner_shablon&action=mod">Шаблоны</a>
			</li>
		</ul>
		
		<h2 class="drawer-handle utils">УТИЛИТЫ</h2>
		<ul>
			<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
			<a href="javascript:mcFileManager.open();">Файловый менеджер</a></li>		
		</ul>
		
		<h2 class="drawer-handle permitions">НАСТРОЙКИ</h2>
		<ul>
			
			<?php if ($_GET['module'] == 'cat'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=cat&action=mod">Управление контентом</a>
			</li>
			
			<?php if ($_GET['module'] == 'users'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=users&action=mod">Пользователи админки</a>
			</li>
			
			<?php if ($_GET['module'] == 'capacity'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=capacity&action=mod">Должности</a>
			</li>
			
			<?php if ($_GET['module'] == 'role'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=role&action=mod">Права доступа</a>
			</li>
			 
			<?php if ($_GET['module'] == 'cache'): ?><li class="item_menu act" style="background-color:#f2f5f9;">
			<?php else: ?><li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''"><?php endif; ?>
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=cache&action=mod">Кеш</a>
			</li>
		
		</ul>
	</li>
	</ul>
	
</div>