<?php /* Smarty version 2.6.18, created on 2014-10-28 15:30:43
         compiled from index.tpl */ ?>
<?php echo '
<html>
<head>
<title>Панель администратора</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<script language="javascript" type="text/javascript" src="js/func.js"></script>
<script language="javascript" type="text/javascript" src="tiny_mce/plugins/filemanager/js/mcfilemanager.js"></script>
<script language="javascript" type="text/javascript" src="/js/tooltip/tooltip.js"></script>

<script language="javascript" type="text/javascript" src="/js/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="https://dadata.ru/static/js/suggestions-jquery-4.1.min.js"></script>
<script language="javascript" type="text/javascript" src="/js/thickbox.js"></script>
<script language="javascript" type="text/javascript" src="/js/jquery/ui/ui.core.min.js"></script>
<script language="javascript" type="text/javascript" src="/js/jquery/ui/ui.tabs.min.js"></script>
<script language="javascript" type="text/javascript" src="js/flexigrid/flexigrid.js"></script>
<script src="//api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<link rel="stylesheet" href="/js/arcticmodal/jquery.arcticmodal-0.3.css" type="text/css" media="screen" />
<script src="/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<!--<script language="javascript" type="text/javascript" src="js/ui/ui.accordion.js"></script>-->
    <!-- kladr-->
    <script type="text/javascript" src="/templates/js/dadata.js"></script>
    <script type="text/javascript" src="/templates/js/suggest_admin.js"></script>
    <link href="/templates/css/suggestions.css" type="text/css" rel="stylesheet" />

    <link rel="stylesheet" href="/js/bootstrap/bootstrap.css"/>
    <link rel="stylesheet" href="/js/bootstrap/datepicker/datepicker.css"/>
    <script src="/js/bootstrap/bootstrap.js"></script>
    <script src="/js/bootstrap/datepicker/bootstrap-datepicker.js"></script>
    <script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>
    <script src="/js/admin/filter/filter.js"></script>
    <link rel="stylesheet" href="/js/admin/filter/filter.css"/>

    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/tabs/style.css">
    <link rel="stylesheet" type="text/css" href="css/flexigrid/flexigrid.css">
    <link rel="stylesheet" type="text/css" href="css/thickbox/thickbox.css">
    <link href="https://dadata.ru/static/css/suggestions.css" type="text/css" rel="stylesheet" />

    <script type="text/javascript">
$(document).ready(function(){
	$("#tabs > ul").tabs();
	$(".actions_hover").hover(
	function () {
		$(\'#actions_header\').show();
		$(\'#actions_header\').append($(this).attr(\'title\'));
	},
	function () {
		$(\'#actions_header\').empty();
		$(\'#actions_header\').hide();
	}
	);
});
</script>

</head>

<body>
'; ?>


<?php echo '<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0"><!-- HEADER --><tr><td height="50" bgcolor="#789cac"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr><td width="245" align="center" class="company">--= PARFUMOFF =--</td><td align="right"><table width="100%" height="51" border="0" cellpadding="0" cellspacing="0"><tr><td align="right" valign="bottom" class="host_header">www.'; ?><?php echo $this->_tpl_vars['host']; ?><?php echo '</td></tr><tr><td align="right" valign="top" class="user_header">'; ?><?php echo $_SESSION['admin_user_name']; ?><?php echo '<small>&nbsp;|&nbsp;'; ?><?php echo $_SESSION['admin_user_login']; ?><?php echo '&nbsp;|</small></td></tr></table></td><td width="60" align="center" class="exit" ><a href="?index.php&other=exit" style="color:#e5f2f4;">[EXIT]</a></td></tr></table></td></tr><!-- CONTENT --><tr><td valign="top" bgcolor="#FFFFFF"><table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >'; ?><?php if ($_GET['is_menu'] != '0'): ?><?php echo '<td width="245" valign="top" bgcolor="#f9f9f9">'; ?><?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "menu.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?><?php echo '</td>'; ?><?php endif; ?><?php echo '<td width="10"><img src="/images/default/1x1.gif" width="1" height="1" border="0"></td><td valign="top"><div class="text" ><br>'; ?><?php echo $this->_tpl_vars['content']; ?><?php echo '</div><br></td><td width="10"><img src="/images/default/1x1.gif" width="1" height="1" border="0"></td></tr></table></td></tr><!-- FOOTER --><tr><td height="10" bgcolor="#789cac"></td></tr></table></body></html>'; ?>