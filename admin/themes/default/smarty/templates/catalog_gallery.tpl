<script src="http://feather.aviary.com/js/feather.js"></script>
<script src="js/plupload/plupload.full.js"></script>

<script src="js/moderation.js?v=1"></script>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
    <tr>
        <td align="left" width="20%">&nbsp;&nbsp;Галерея:</td>
        <div class="prototype" style="float: left;border: 1px solid #979797;width: 130px;width: 200px;height: 150px;padding: 4px;overflow: hidden;margin-right: 5px;display: none">
            <a style="float: right" data-id = "" onclick="delete_gallery(this)" href="#">Удалить</a><br>
            <img style="height: 100px;" src="">

        </div>
        <td align="left" width="80%" class="photos">
            {foreach from=$aPhotos item=photo}
                <div style="float: left;border: 1px solid #979797;width: 200px;height: 150px;padding: 4px;overflow: hidden;margin-right: 5px;">
                    <a data-id = "{$photo.id}" onclick="delete_gallery(this);return false;" style="float: right" href="">Удалить</a><br>
                    <img style="height: 100px;" src="/images/uploads/{$module}/{$smarty.get.id}/small/{$photo.img}">

                </div>
            {/foreach}
        </td>
    </tr>

    <tr class="table_bg">
        <td align="left" colspan="2">
            &nbsp;&nbsp;Искать в google:
        </td>
    </tr>
    <tr>
        <td align="left" width="10%">&nbsp;&nbsp;Запрос:</td>
        <td align="left" width="90%">
            <input type="text" id="gallery_query" style="width:100%" class="form_add" value="{$brand.title} {$title}">
        </td>
    </tr>
    <tr>
        <td align="left" colspan="2">
            <input class="btn btn-primary" type="button" name="get_gallery" id="get_gallery" value="Go">
            &nbsp;
            <input class="btn btn-danger" type="button" name="select_foto_gallery" id="select_foto_gallery" value="Добавить выбранные">
        </td>
    </tr>
    <tr>
        <td align="center" id="out_photo_gallery" colspan="2">
            <div id="msg" style="height:350px;overflow: scroll"></div>
        </td>

    </tr>
</table>

<script>
    {literal}
    $(function () {


        $("#get_gallery").click(function () {

            var qw = $("#gallery_query").attr('value');
            var lang = $("#lang option:selected").val();

            if (qw == '') {
                alert('Введите запрос');
            }

            // чистим
            $("#last_more").remove();
            $("#out_photo_gallery #msg").html('');


            // делаем запрос
            $.post("?ajax=catalog_moder&action=google&type=gallery", {
                qw: qw,
                lang: lang
            }, function (xml) {
                $("message", xml).each(function (id) {

                    message = $("message", xml).get(id);
                    $("#out_photo_gallery #msg").html($("msg", message).text());

                });
            });
        })});


    $("#select_foto_gallery").click(function () {
        var selected = $('#out_photo_gallery #msg input:checked');
        $.each(selected,function(i,v){
            var image_src = $(v).val();
            // делаем запрос
            $.post("?ajax=catalog_moder&action=upload_gallery&id={/literal}{$smarty.get.id}{literal}", {
                dataType: "json",
                url: image_src
            }, function (obj) {
                if (typeof obj.error != 'undefined') {
                    alert(obj.error)
                } else {
                    update_gallery();
                }
            });
        });
    });


    function update_gallery(){
        $.post("?ajax=catalog_moder&action=show_gallery&id={/literal}{$smarty.get.id}{literal}", {
            dataType: "json",
        }, function (data) {
            if (typeof data.error != 'undefined') {
                alert(data.error)
            } else {
                $('.photos').html('');
                $.each(data,function(i,v){
                    var photo = $('.prototype').clone();
                    $(photo).removeClass('prototype');
                    $(photo).find('img').attr('src','/images/uploads/catalog/' +  v.id_catalog_data + '/small/' + v.img);
                    $(photo).find('a').data('id',v.id);
                    $(photo).show();

                    $('.photos').append(photo);
                });
               /* $('[name="image_path"]').val(obj.path)
                $('#image1').show();
                $('#image1').attr('src', obj.path);*/
            }
        });
    }


    function delete_gallery(elem){
        var id = $(elem).data('id');
        $.post("?ajax=catalog_moder&action=delete_gallery&id=" + id, {
            dataType: "json",
        }, function (data) {
            if (typeof data.error != 'undefined') {
                alert(data.error)
            } else {
               update_gallery();
            }
        });
    }


</script>
{/literal}


