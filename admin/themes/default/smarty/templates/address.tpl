<tr>
    <td align="left"></td>
    <td align="left" colspan="">Адрес доставки:</td>
</tr>

<tr>
    <td align="left">Регион *:</td>
    <td align="left">
        <input type="text" id="region" name="region" value="{$address.address_region}" style="width:100%">
    </td>
</tr>


<tr class="part">
    <td align="left">Уточните район *:</td>
    <td align="left">
        <select disabled name="part" id="select_part">

            {foreach from=$capital_districts item=capital key=letter}
                {foreach from=$capital key=code item=item}
                    <option style="display: none" {if $code==$address.address_part}SELECTED="" {/if}  data-location="{$letter}" value="{$code}">{$item}</option>
                {/foreach}
            {/foreach}
        </select>
    </td>
</tr>
<tr class="other">
    <td align="left">Другой:</td>
    <td align="left">
        <input type="text" id="other" name="other" value="{$address.address_other}" style="width:100%">
    </td>
</tr>

<tr class="metro">
    <td align="left">Метро:</td>
    <td align="left">
        <select name="metro">
            <option value="0">Не выбрано</option>
            {foreach from=$aMetro item=metro}
                <option {if $address.metro==$metro.id}SELECTED{/if}  value="{$metro.id}">{$metro.name}</option>
            {/foreach}
        </select>
    </td>
</tr>

<tr class="city">
    <td align="left">Город/Район *:</td>
    <td align="left">
        <input type="text" id="city" name="city" value="{$address.address_city}" style="width:100%">
    </td>
</tr>
<tr class="settlement" style="display: none">
    <td align="left">Нас. пункт *:</td>
    <td align="left">
        <input type="text" id="settlement" name="settlement" value="{$address.address_settlement}" style="width:100%">
    </td>
</tr>
<tr>
    <td align="left">Улица *:</td>
    <td align="left">
        <input type="text" id="street" name="street" value="{$address.address_street}" style="width:100%">
    </td>
</tr>

<tr class="house">
    <td align="left">Дом</td>
    <td align="left">
        <input type="text" id="house" name="house" value="{$address.address_house}" style="width:100%">
    </td>
</tr>
<tr class="house">
    <td align="left">Корпус</td>
    <td align="left">
        <input type="text" id="corpus" name="corpus" value="{$address.address_corpus}" style="width:100%">
    </td>
</tr>
<tr class="house">
    <td align="left">Строение</td>
    <td align="left">
        <input type="text" id="stroenie" name="stroenie" value="{$address.address_stroenie}" style="width:100%">
    </td>
</tr>
<tr class="house">
    <td align="left">Этаж</td>
    <td align="left">
        <input type="text" id="floor" name="floor" value="{$address.address_floor}" style="width:100%">
    </td>
</tr>
<tr class="house">
    <td align="left">Номер квартиры</td>
    <td align="left">
        <input type="text" id="flat" name="flat" value="{$address.address_flat}" style="width:100%">
    </td>
</tr>
<tr class="house">
    <td align="left">Номер домофона</td>
    <td align="left">
        <input type="text" id="domophone" name="domophone" value="{$address.address_domophone}" style="width:100%">
    </td>
</tr>
<tr class="house">
    <td align="left">Индекс</td>
    <td align="left">
        <input type="text" id="regs_postcode" name="regs_postcode" value="{$address.postcode}" style="width:100%">
    </td>
</tr>

<input hidden name="fio_data" value="">
<input hidden name="address_data" value="">
<input id="check_postalcode" style="width: 100%" placeholder="Проверка правильности адреса по индексу">
{literal}
    <script>
        $(function () {
            AddressSuggestions.initForm();
            AddressSuggestions.initName();

            //check by postalcode
            $("#check_postalcode").suggestions({
                serviceUrl: "https://dadata.ru/api/v2/suggest/address",
                token: DadataApi.TOKEN,
                /* Вызывается, когда пользователь выбирает одну из подсказок */
                onSelect: function(suggestion) {
                    console.log(suggestion);
                    var $res = $('#parse__result');

                    $res.html('');
                    $res.append("<b>Кладр Айди:</b> <span>"+suggestion.data.kladr_id+"</span><br/>");
                }
            });
        })

    </script>
{/literal}