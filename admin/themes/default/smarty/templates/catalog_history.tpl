<h3>История изменений:</h3>
<table class="form_add" style="width: 800px;">
    <tr>
        <td>Юзер</td>
        <td>Действие</td>
        <td>Исходные данные</td>
        <td>Время</td>
    </tr>
    {foreach from=$aHistory item=oHistory}
        <tr>
            <td>{$oHistory.login}</td>
            <td>{$oHistory.text}</td>
            <td>{$oHistory.source}</td>
            <td>{$oHistory.date_add}</td>
        </tr>
    {/foreach}
</table>