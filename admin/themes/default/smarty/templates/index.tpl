{literal}
<html>
<head>
<title>Панель администратора</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<script language="javascript" type="text/javascript" src="js/func.js"></script>
<script language="javascript" type="text/javascript" src="tiny_mce/plugins/filemanager/js/mcfilemanager.js"></script>
<script language="javascript" type="text/javascript" src="/js/tooltip/tooltip.js"></script>

<script language="javascript" type="text/javascript" src="/js/jquery/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/templates/js/dadata/suggestions-jquery-4.1.min.js"></script>
<script language="javascript" type="text/javascript" src="/js/thickbox.js"></script>
<script language="javascript" type="text/javascript" src="/js/jquery/ui/ui.core.min.js"></script>
<script language="javascript" type="text/javascript" src="/js/jquery/ui/ui.tabs.min.js"></script>
<script language="javascript" type="text/javascript" src="js/flexigrid/flexigrid.js"></script>
<link rel="stylesheet" href="/js/arcticmodal/jquery.arcticmodal-0.3.css" type="text/css" media="screen" />
<script src="/js/arcticmodal/jquery.arcticmodal-0.3.min.js"></script>
<!--<script language="javascript" type="text/javascript" src="js/ui/ui.accordion.js"></script>-->
    <!-- kladr-->
    <script type="text/javascript" src="/templates/js/dadata.js"></script>
    <script type="text/javascript" src="/templates/js/suggest_admin.js"></script>
    <link href="/templates/css/suggestions.css" type="text/css" rel="stylesheet" />

    <link rel="stylesheet" href="/js/bootstrap/bootstrap.css"/>
    <link rel="stylesheet" href="/js/bootstrap/datepicker/datepicker.css"/>
    <script src="/js/bootstrap/bootstrap.js"></script>
    <script src="/js/bootstrap/datepicker/bootstrap-datepicker.js"></script>
    <script src="/templates/js/jquery.maskedinput-1.2.2.js"></script>
    <script src="/js/admin/filter/filter.js"></script>
    <link rel="stylesheet" href="/js/admin/filter/filter.css"/>

    <link rel="stylesheet" href="style.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/tabs/style.css">
    <link rel="stylesheet" type="text/css" href="css/flexigrid/flexigrid.css">
    <link rel="stylesheet" type="text/css" href="css/thickbox/thickbox.css">
    <link href="/admin/css/suggestions.css" type="text/css" rel="stylesheet" />

	<script>
		(function (i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function () {
						(i[r].q = i[r].q || []).push(arguments)
					}, i[r].l = 1 * new Date();
			a = s.createElement(o),
					m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-52259840-1', 'auto');

		ga('require', 'ecommerce');

	</script>

    <script type="text/javascript">
$(document).ready(function(){
	$("#tabs > ul").tabs();
	$(".actions_hover").hover(
	function () {
		$('#actions_header').show();
		$('#actions_header').append($(this).attr('title'));
	},
	function () {
		$('#actions_header').empty();
		$('#actions_header').hide();
	}
	);
});
</script>

</head>

<body>
{/literal}

{strip}

<table width="100%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
<!-- HEADER -->
<tr><td height="50" bgcolor="#789cac">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td width="245" align="center" class="company">
				--= PARFUMOFF =--
			</td>
			
			<td align="right">
				<table width="100%" height="51" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="right" valign="bottom" class="host_header">
						www.{$host}
						</td>
					</tr>
					<tr>
						<td align="right" valign="top" class="user_header">
						{$smarty.session.admin_user_name}<small>&nbsp;|&nbsp;
						{$smarty.session.admin_user_login}&nbsp;|</small>
						</td>
					</tr>
				</table>
			</td>
			<td width="60" align="center" class="exit" >
				<a href="?index.php&other=exit" style="color:#e5f2f4;">[EXIT]</a>
			</td>
		</tr>
	</table>
</td></tr>

<!-- CONTENT -->
<tr><td valign="top" bgcolor="#FFFFFF">
	<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" >
		{if $smarty.get.is_menu neq '0'}
		<td width="245" valign="top" bgcolor="#f9f9f9">{include file="menu.tpl"}</td>
		{/if}
		
		<td width="10"><img src="/images/default/1x1.gif" width="1" height="1" border="0"></td>
		<td valign="top"><div class="text" ><br>{$content}</div><br></td>
		<td width="10"><img src="/images/default/1x1.gif" width="1" height="1" border="0"></td>
		</tr>
	</table>
</td></tr>

<!-- FOOTER -->
<tr><td height="10" bgcolor="#789cac"></td></tr>
</table>

</body>
</html>
{/strip}