<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>PARFUMOFF.RU - ЗАКАЗ №{$orders.id}</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="title" content="" />
	<meta name="description" content="" />
	
	
	{literal}
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
	<script type="text/javascript">
	$(function(){

		var elem = $(".zakaz:eq(0)");
		var elem_h = elem.height();
		var elem_double_h = elem_h*2;

		if (elem_double_h > 1020) {
			elem.addClass("one4page");
			$(".sep").addClass("hidden_print")
		} else {
			elem.css("height","500px")
		}
	})
	</script>

	<style type="text/css">
	p {margin-top:0}
	@page {margin:0;padding:0}
	@media print {
		.one4page {
			height:1050px;
		}
		.hidden_print {
			display:none;
		}
	}
	</style>
	{/literal}
	
</head>
<body style="width:725px;font-family:Arial;margin:0 auto;padding:0;">
<table align="center" style="padding:5px;width: 100%;font-family:Arial;" valign="middle" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
		<div class="Section1">
<div class="zakaz" style="position: relative;">
<table width="100%" cellspacing="0" cellpadding="0" border="1" style="width:100%;cellspacing:0cm;border:solid gray 1.0pt;padding-alt:7.5pt 7.5pt 7.5pt 7.5pt" >
 <tbody>
 
 
 <tr style="height:12.1pt">
  <td width="50%" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;
  height:12.1pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><u><span style="font-size:12.0pt;">Заказ № {$orders.prefix}-{$orders.id}</span></u><u></u></p>
  </td>
  <td width="50%" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;
  height:12.1pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><u><span style="font-size:12.0pt;">Доставка </span></u><u><span style="font-size:12.0pt;
  ">Parfumoff.ru</span></u></p>
  </td>
 </tr>
 <tr style="height:40.2pt">
  <td valign="top" style="border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:
  40.2pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Покупатель:&nbsp;<b>{$client.fio}</b></span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Телефон:&nbsp;<b>{$client.phone}</b></span></p>
  </td>
  <td valign="top" style="border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:
  40.2pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Адрес доставки заказа:&nbsp;
  <b>

{if $client.region eq 'moscow'}
Москва, {$client.address}
{elseif $client.region eq 'region'}
{$client.postcode}, {$client.city}, {$client.address}
{else}
{$client.address}
{/if}

  </b></span></p>
{if $orders.pComment}
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Комментарии: </span><span style="font-size:9.0pt;
  ">{$orders.pComment}</span></p>
 {/if}
  </td>
 </tr>
</tbody></table>

<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal">&nbsp;</p>

<table width="100%" cellpadding="0" border="1" style="width:100%;cellspacing:1.5pt;border:solid gray 1.0pt;padding-alt:3.75pt 3.75pt 3.75pt 3.75pt" >
 <tbody>
 <tr color="white" bgcolor="black">
  <td bgcolor="black" width="60%" style="width:60.0%;border:none;background-color:#000000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
  <p style="background:#000000;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><b><span bgcolor="black" style="background:#000000;font-size:12.0pt;color:white;">Наименование</span></b></p>
  </td>
  <td bgcolor="black" width="20%" style="width:20.0%;border:none;background-color:#000000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
  <p style="background:#000000;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
  <b><span style="background:#000000;font-size:12.0pt;color:white;">Количество</span></b></p>
  </td>
  <td bgcolor="black" width="20%" style="width:20.0%;border:none;background-color:#000000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
  <p style="background:#000000;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
  <b><span style="background:#000000;font-size:12.0pt;color:white;">Стоимость</span></b></p>
  </td>
 </tr>
 
{section name=i loop=$item}
<tr style="border:none;padding:3.75pt 3.75pt 3.75pt 3.75pt;">
	<td width="60%" style="width:60.0%;border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
		<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
			<span style="font-size:10.0pt;">{$item[i].title}</span>
		</p>
	</td>
	<td  style="border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
		<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">{$item[i].kol}</span></p>
	</td>
	<td style="border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
		<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
			<span style="font-size:10.0pt;">RUR {$item[i].sum}.00</span>
		</p>
	</td>
</tr>
{/section}
</tbody></table>



<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal"><span style="font-size:12.0pt;">&nbsp;</span></p>

<table width="100%" cellspacing="0" cellpadding="0" border="1" style="width:100%;cellspacing:0cm;border:solid gray 1.0pt;padding-alt:7.5pt 7.5pt 7.5pt 7.5pt" >
 <tbody><tr style="yfti-lastrow:yes">
  <td width="50%" valign="top" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Подытог:</span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Скидка, %:</span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Стоимость доставки:</span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:14.0pt;">Итого:</span></b></p>
  </td>
  <td width="50%" valign="top" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">RUR {$orders.Sum}.00</span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">{$orders.dPercent}</span></p>
  
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">
		{if $client.region eq 'region'}
			RUR 290.00
			{else}
			RUR 0.00
		{/if}
</span></p>

  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:14.0pt;">
  {if $orders.dType eq '0'}
		{if $client.region eq 'region'}
		RUR {$orders.Sum+290}.00
		{else}
		RUR {$orders.Sum}.00
		{/if}
	{else}
		{if $client.region eq 'region'}
		RUR {$orders.dSum+290}.00
		{else}
		RUR {$orders.dSum}.00
		{/if}
	{/if}
  </span></b></p>
  </td>
 </tr>
</tbody></table>
<br>
<div>{$orders.DateEnd|date_format:"%e %B %Y"}</div>
</div>

<div class="sep">
<br>
<p style="margin:5px 0;border-top:1px dashed black"></p>
<br>
</div>


<div class="zakaz" style="position: relative">
<table width="100%" cellspacing="0" cellpadding="0" border="1" style="width:100%;cellspacing:0cm;border:solid gray 1.0pt;padding-alt:7.5pt 7.5pt 7.5pt 7.5pt" >
 <tbody>
 
 
 <tr style="height:12.1pt">
  <td width="50%" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;
  height:12.1pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><u><span style="font-size:12.0pt;">Заказ № {$orders.prefix}-{$orders.id}</span></u><u></u></p>
  </td>
  <td width="50%" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;
  height:12.1pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><u><span style="font-size:12.0pt;">Доставка </span></u><u><span style="font-size:12.0pt;
  ">Parfumoff.ru</span></u></p>
  </td>
 </tr>
 <tr style="height:40.2pt">
  <td valign="top" style="border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:
  40.2pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Покупатель:&nbsp;<b>{$client.fio}</b></span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Телефон:&nbsp;<b>{$client.phone}</b></span></p>
  </td>
  <td valign="top" style="border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:
  40.2pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Адрес доставки заказа:&nbsp;
  <b>

{if $client.region eq 'moscow'}
Москва, {$client.address}
{elseif $client.region eq 'region'}
{$client.postcode}, {$client.city}, {$client.address}
{else}
{$client.address}
{/if}

  </b></span></p>
{if $orders.pComment}
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Комментарии: </span><span style="font-size:9.0pt;
  ">{$orders.pComment}</span></p>
 {/if}
  </td>
 </tr>
</tbody></table>

<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal">&nbsp;</p>

<table width="100%" cellpadding="0" border="1" style="width:100%;cellspacing:1.5pt;border:solid gray 1.0pt;padding-alt:3.75pt 3.75pt 3.75pt 3.75pt" >
 <tbody>
 <tr >
  <td width="60%" style="width:60.0%;border:none;background-color:#000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><b><span style="font-size:12.0pt;color:white;">Наименование</span></b></p>
  </td>
  <td width="20%" style="width:20.0%;border:none;background-color:#000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
  <b><span style="font-size:12.0pt;color:white;">Количество</span></b></p>
  </td>
  <td width="20%" style="width:20.0%;border:none;background-color:#000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
  <b><span style="font-size:12.0pt;color:white;">Стоимость</span></b></p>
  </td>
 </tr>
 
{section name=i loop=$item}
<tr style="border:none;padding:3.75pt 3.75pt 3.75pt 3.75pt;">
	<td width="60%" style="width:60.0%;border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
		<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
			<span style="font-size:10.0pt;">{$item[i].title}</span>
		</p>
	</td>
	<td  style="border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
		<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">{$item[i].kol}</span></p>
	</td>
	<td style="border:none;background:white;padding:3.75pt 3.75pt 3.75pt 3.75pt">
		<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
			<span style="font-size:10.0pt;">RUR {$item[i].sum}.00</span>
		</p>
	</td>
</tr>
{/section}
</tbody></table>



<p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal"><span style="font-size:12.0pt;">&nbsp;</span></p>

<table width="100%" cellspacing="0" cellpadding="0" border="1" style="width:100%;cellspacing:0cm;border:solid gray 1.0pt;padding-alt:7.5pt 7.5pt 7.5pt 7.5pt" >
 <tbody><tr style="yfti-lastrow:yes">
  <td width="50%" valign="top" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Подытог:</span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Скидка, %:</span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">Стоимость доставки:</span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:14.0pt;">Итого:</span></b></p>
  </td>
  <td width="50%" valign="top" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt">
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">RUR {$orders.Sum}.00</span></p>
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">{$orders.dPercent}</span></p>
  
  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><span style="font-size:10.0pt;">
		{if $client.region eq 'region'}
			RUR 290.00
			{else}
			RUR 0.00
		{/if}
</span></p>

  <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><b><span style="font-size:14.0pt;">
  {if $orders.dType eq '0'}
		{if $client.region eq 'region'}
		RUR {$orders.Sum+290}.00
		{else}
		RUR {$orders.Sum}.00
		{/if}
	{else}
		{if $client.region eq 'region'}
		RUR {$orders.dSum+290}.00
		{else}
		RUR {$orders.dSum}.00
		{/if}
	{/if}
  </span></b></p>
  </td>
 </tr>
</tbody></table>
    <br>
<div style="">{$orders.DateEnd|date_format:"%e %B %Y"}</div>
</div>
<div style="page-break-before: always;"></div>
</body>
</html>