{strip}
    <table style="width: 100%;" border="0" cellpadding="1" cellspacing="1" class="form">
        <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=order" method="post" enctype="multipart/form-data" name="formMod">
            <!-- Заголовок таблицы данных -->
            <tr align="center" class="table_header">
                <td width="1%" align="center" height="22"></td>
                <td width="15%">
                    <div align="left"><a href="/admin/?{$aSort.brand}"><b>Наименование товара</b></a></div>
                </td>
                <td width="15%">
                    <div align="center"><a href="/admin/?{$aSort.name}"><b> Имя </b></a></div>
                </td>
                <td width="10%">
                    <div align="center"><a href="/admin/?{$aSort.phone}"><b>Телефон</b></a></div>
                </td>
                <td width="10%">
                    <div align="center"><a href="/admin/?{$aSort.email}"><b>e-mail</b></a></div>
                </td>
                <td width="10%">
                    <div align="center"><a href="/admin/?{$aSort.created}"><b>Дата</b></a></div>
                </td>
                <td width="10%">
                    <div align="center"><a href="/admin/?{$aSort.price}"><b>Аромат</b></a></div>
                </td>
                <td width="10%">
                    <div align="center"><a href="/admin/?{$aSort.mail_sended}"><b>Письмо</b></a></div>
                </td>

            </tr>
            <!-- Заголовок таблицы данных -->
            {if $aItems}
                <!-- Вывод списка -->
                {foreach from=$aItems item=item}
                    <tr id="moder_{$item[i].id}" style="background-color:{$item[i].bgcolor};">
                        <td  align="center" height="25px">
                            <input type="checkbox" name="id[]" value="{$item.id}"></nobr></td>
                        <td align="left">{$item.brand} {$item.aromat} {$item.type}</td>
                        <td align="left">{$item.name}</td>
                        <td align="left">{$item.phone}</td>
                        <td align="left">{$item.email}</td>
                        <td align="left">{$item.created}</td>
                        <td align="left">{if $item.price}В наличии{else}{/if}</td>
                        <td align="left">{if $item.mail_sended}Отправлено{else}{/if}</td>
                    </tr>
                {/foreach}
            {else}
                <tr>
                    <td colspan="3"><h3>Заявок  не найдено.</h3></td>
                </tr>
            {/if}
            <!-- ------------ -->
    </table>
    <div style="width: 100%">
    <!-- Подключение футера -->
    {include file="table_footer.tpl"}
    </div>
    </form>
{/strip}

