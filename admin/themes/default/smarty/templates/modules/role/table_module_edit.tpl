{strip}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id={$smarty.get.id}
	&action=change_module" method="post" enctype="multipart/form-data" name="formMod">


<!--  Редактировать параметры модуля -->
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="form_add">
	<tr>
		<td align="left" bgcolor="#eef1f7">
		<img align="left" src="images/folder_edit.png" width="16" height="16"></img>
		<a href="#" onclick="hideshow('sh_edit_module'); return false;">
			<div align=left class="link_block">&nbsp;Редактировать параметры модуля</div>
		</a>
		</td>
	</tr>
</table>

<div style="display:none" id='sh_edit_module'>
<table width="100%" border="0"  cellpadding="5" cellspacing="0" class="form_add" style="border-top: #abb6d7 0px solid;">
	
	<tr>
		<td width="30%" align="left"><span style="color:red;">*</span>&nbsp;Название модуля:</td>
		<td width="70%" align="left">
			<input type="text" name="title" value="{$title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Тип раздела:</td>
		<td align="left">
			<input type="text" name="name" value="{$name}" style="width:100%">
		</td>
	</tr>
	
	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="save_module" value="Сохранить" style="width:100px">
	</td>
	</tr>
</table>
</div>


<!--  Добавить интерфейс -->
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="form_add">
	<tr>
		<td align="left" bgcolor="#eef1f7">
		<img align="left" src="images/folder_add.png" width="16" height="16"></img>
		<a href="#" onclick="hideshow('sh_add_interface'); return false;">
			<div align=left class="link_block">&nbsp;Добавить интерфейс</div>
		</a>
		</td>
	</tr>
</table>


<div style="display:none" id='sh_add_interface'>
<table width="100%" border="0"  cellpadding="5" cellspacing="0" class="form_add" style="border-top: #abb6d7 0px solid;">
	
	
	<tr>
		<td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Название интерфейса:</td>
		<td align="left" width="70%">
			<input type="text" name="name_interface" value="" style="width:100%;">
		</td>
	</tr>
	
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Подробное описание:</td>
		<td align="left">
			<input type="text" name="anot_interface" value="" style="width:100%;">
		</td>
	</tr>
	
	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="add_interface" value="Добавить" style="width:100px">
	</td>
	</tr>
</table>
</div>

<!--  Редактировать интерфейсы -->
<table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
      
	<!-- Заголовок таблицы данных -->
	<tr align="center" class="table_header">
      	<td width="3%" height="22" align="center"><b>X</b></td>
        <td width="20%" align="left"><b>&nbsp;Название интерфейса</b></td>
        <td width="" align="left"><b>&nbsp;Описание</b></td>
	</tr>

	{section name=i loop=$item_interface}
	<tr style="background-color:#f8f8f8">
		<td align="center" height="25px"><input type="checkbox" name="id_interface[]" value="{$item_interface[i].id}"></td>
		<td align="left">
			<input type="text" name="name_interface_{$item_interface[i].id}" value="{$item_interface[i].name}" style="width:100%">
		</td>
		<td align="left">
			<input type="text" name="anot_interface_{$item_interface[i].id}" value="{$item_interface[i].anot}" style="width:100%">
		</td>
	</tr>
	{/section}
	
	<tr>
		<td align="left" colspan="3">
		<div align="right" style="float: left;">
			<select name="actions">
			    <option value='null'>-- Действия --</option>
			    {html_options values=$sel_id output=$sel_names selected=$is_active}
			</select>
		</div>
			
		<div align="right">
			<input type="submit" name="Submit" value="Применить" style="width:100px">
		</div>
		</td>
	</tr> 
	
</table>

</form>
{/strip}