{strip}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=add_role
	&id={$smarty.get.id}" method="post" enctype="multipart/form-data" name="form">

<table width="100%" border="0" cellpadding="3" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Заголовок:</td>
		<td align="left" width="70%"><input type="text" name="title" value="" style="width:100%"></td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Описание правила:</td>
		<td align="left"><input type="text" name="anot" value="" style="width:100%"></td>
	</tr>
	
</table>

<table width="100%" border="0" cellpadding="3" cellspacing="0" class="form_add">
	<tr>
		<td align="left" bgcolor="#eef1f7">
		<img align="left" src="images/folder_edit.png" width="16" height="16"></img>
		<a href="#" onclick="hideshow('sh_static'); return false;">
			<div align=left class="link_block">&nbsp;Статические модули</div>
		</a>
		</td>
	</tr>
</table>

<div style="display:none" id='sh_static'>
<table width="100%" border="0"  cellpadding="2" cellspacing="0" class="form_add" style="border-top: #abb6d7 0px solid;">
	<!-- Вывод списка -->
	{section name=i loop=$item_static}
		<tr style="background-color:#f2f2f2;">
			<td align="center" height="22px" width="3%">&nbsp;</td>
			<td align="left">&nbsp;<b>{$item_static[i].title}</b></td>
			<td align="left">&nbsp;</td>
		</tr>
		
			{section name=j loop=$item_static[i].interface}
			<tr style="background-color:#f8f8f8;">
				<td align="center">
					<input type="checkbox" name="id_interface[]" value="{$item_static[i].interface[j].id_interface}" {$item_static[i].interface[j].checked}>
				</td>
				<td align="left">
					&nbsp;{$item_static[i].interface[j].name_interface}
				</td>
				<td align="left">
					&nbsp;{$item_static[i].interface[j].anot_interface}
				</td>
			</tr>
			{/section}
	{/section}
</table>
</div>

<table width="100%" border="0" cellpadding="3" cellspacing="0" class="form_add">
	<tr>
		<td align="left" bgcolor="#eef1f7">
		<img align="left" src="images/folder_edit.png" width="16" height="16"></img>
		<a href="#" onclick="hideshow('sh_dinamic'); return false;">
			<div align=left class="link_block">&nbsp;Динамические модули</div>
		</a>
		</td>
	</tr>
</table>

<div style="display:none" id='sh_dinamic'>
<table width="100%" border="0"  cellpadding="2" cellspacing="0" class="form_add" style="border-top: #abb6d7 0px solid;">
	
		<!-- Вывод списка -->
		{section name=i loop=$item_dinamic}
			{if $item_dinamic[i].is_menu eq '1'}
				<tr style="background-color:gray; color:#fff ">
					<td align="center" height="22px" width="3%">&nbsp;</td>
					<td align="left">&nbsp;<b>{$item_dinamic[i].title}</b></td>
					<td align="left">&nbsp;</td>
				</tr>
			{else}
				<tr style="background-color:#f2f2f2;">
					<td align="center" height="20px" width="3%">&nbsp;</td>
					<td align="left">&nbsp;<b>{$item_dinamic[i].title}</b></td>
					<td align="left">&nbsp;</td>
				</tr>
			{/if}
			
				{section name=j loop=$item_dinamic[i].interface}
				<tr style="background-color:#f8f8f8;">
					<td align="center">
						<input type="checkbox" name="id_interface[]" value="{$item_dinamic[i].interface[j].id_interface}" {$item_dinamic[i].interface[j].checked}>
					</td>
					<td align="left">
						&nbsp;{$item_dinamic[i].interface[j].name_interface}
					</td>
					<td align="left">
						&nbsp;{$item_dinamic[i].interface[j].anot_interface}
					</td>
				</tr>
				{/section}
		{/section}
		
</table>
</div>

<table width="100%" border="0" cellpadding="2" cellspacing="0" class="form_add" style="border-top: #abb6d7 0px solid;">
	<tr style="background-color:#f2f2f2;">
		<td align="right"><input type="submit" name="add" value="Добавить" style="width:150px"></td>
	</tr>
</table>

</form>
{/strip}