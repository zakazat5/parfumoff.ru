{strip}

<!-- Подключение хидера -->
{include file="modules/role/table_module_header.tpl"}

<table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=delete_module" method="post" enctype="multipart/form-data" name="formMod">
      
	<!-- Заголовок таблицы данных -->
	<tr align="center" class="table_header">
      	<td width="3%" height="22" align="center"><b>X</b></td>
        <td width="" align="left"><b>&nbsp;Описание модуля</b></td>
        <td width="10%" align="left"><b>&nbsp;Тип раздела</b></td>
        <td width="10%" align="left"><b>&nbsp;ID модуля</b></td>
	</tr>
	<!-- Заголовок таблицы данных -->
      
	
	<tr>
		<td align="left" colspan="4" height="25">&nbsp;<b>Статические модули</b></td>
	</tr>

	{section name=i loop=$item_static}
	<tr style="background-color:#f8f8f8">
		<td align="center" height="25px"><input type="checkbox" name="id[]" value="{$item_static[i].id}"></nobr></td>
		<td align="left">
			&nbsp;<a href="?main={$smarty.get.main}
						   &module={$smarty.get.module}
						   &action=edit_module
						   &id={$item_static[i].id}">{$item_static[i].title}</a>
		</td>
		<td align="left">&nbsp;{$item_static[i].name}</td>
		<td align="left">&nbsp;{$item_static[i].id_module}</td>
	</tr>
	{/section}
	
	
	<tr>
		<td align="left" colspan="4" height="25">&nbsp;<b>Динамические модули</b></td>
	</tr>
	{section name=i loop=$item_dinamic}
	<tr style="background-color:#f8f8f8;">
		<td align="center" height="25px"></td>
		<td align="left">&nbsp;{$item_dinamic[i].title}</td>
		<td align="left">&nbsp;{$item_dinamic[i].name}</td>
		<td align="left">&nbsp;{$item_dinamic[i].id_module}</td>
	</tr>
	{/section}
	
</table>

<!-- Подключение футера -->
{include file="modules/role/table_module_footer.tpl"}
</form>
{/strip}