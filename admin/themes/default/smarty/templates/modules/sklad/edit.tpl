{strip}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id={$smarty.get.id}
	&action=edit" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>Основные данные</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Параметры склада
		</td>
	</tr>

	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Заголовок:</td>
		<td align="left" width="80%">
			<input type="text" name="title" value="{$title}" style="width:100%" >
		</td>
	</tr>
	
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;Приоритет (значение):</td>
		<td align="left" width="80%">
			<input type="text" name="priority_val" value="{$priority_val}" style="width:100px" >
		</td>
	</tr>
		
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Обновление цен парфюмерии
		</td>
	</tr>
	
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;Выберите файл CSV:</td>
		<td align="left" width="80%">
			<input type="file" name="file" style="width:100%;">
		</td>
	</tr>
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;</td>
		<td align="left" width="80%">
			{if isset($smarty.get.load_ok)}
				Ок, файл успешно загружен !
			{/if}
		</td>
	</tr>

    <tr class="table_bg">
        <td align="left"  colspan="2">
            &nbsp;&nbsp;Обновление цен косметики
        </td>
    </tr>

    <tr>
        <td align="left" width="20%">&nbsp;&nbsp;Выберите файл CSV:</td>
        <td align="left" width="80%">
            <input type="file" name="file_k" style="width:100%;">
        </td>
    </tr>
    <tr>
        <td align="left" width="20%">&nbsp;&nbsp;</td>
        <td align="left" width="80%">
            {if isset($smarty.get.load_ok_k)}
                Ок, файл успешно загружен !
            {/if}
        </td>
    </tr>
	
	
</table>
</div>


<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
	</td>
	</tr>
</table>

</form>

{/strip}