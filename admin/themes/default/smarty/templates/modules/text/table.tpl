{include file="editor.tpl"}
{strip}
    <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
        <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=save_cont
	&id_cat={$smarty.get.id_cat}" method="post" enctype="multipart/form-data" name="formMod">

            <tr>
                <td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
                    <a href="javascript:toggleEditor('text');">[ОТКЛ/ВКЛ]</a><b>&nbsp;&nbsp;Изменить раздел "{$cat_name}
                        "</b>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <textarea id="text" name="cont" style="width:100%" rows=10 class="NoEditor">{$text}</textarea>
                </td>
            </tr>

            {if $type_2==3 or $type_2==2}
                <tr>
                    <td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
                        <a href="javascript:toggleEditor('text_2');">[ОТКЛ/ВКЛ]</a>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <textarea id="text_2" name="text_2" style="width:100%" rows=10 class="NoEditor">{$text_2}</textarea>
                    </td>
                </tr>
            {/if}

            {if $type_2==3}
                <tr>
                    <td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
                        <a href="javascript:toggleEditor('text_3');">[ОТКЛ/ВКЛ]</a>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <textarea id="text_3" name="text_3" style="width:100%" rows=10 class="NoEditor">{$text_3}</textarea>
                    </td>
                </tr>
            {/if}


            <tr>
                <td align="left" colspan="2">
                    <input type="submit" name="save_cont" value="Сохранить" style="width:100px;">
                </td>
            </tr>
        </form>
    </table>
{/strip}