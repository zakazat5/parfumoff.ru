{literal}
<script type="text/javascript">
$(document).ready(function(){
	$("#header_find").click(function () {$('#table_find').toggle();});
});
</script>
{/literal}

{strip}

<form action="?main={$smarty.get.main}&module={$smarty.get.module}&action=mod&id_cat={$smarty.get.id_cat}" 
	method="post" 
	enctype="multipart/form-data" 
	name="formSearche">

<div style="background-color: #3e726e;width:70px;height:15px;color:#fff;padding-top:2px;" align="center" id="header_find">
Фильтр</div>

<table id="table_find" width="500" border="0" cellpadding="0" cellspacing="6" style="border: 2px #3e726e solid; background-color: #f4edc9;color:#3e726e;">
	<tr>
		<td width="40%">Фильтрация по брендам:</td>
		<td width="60%">
			<select name="sel_brend" id="sel_brend" style="width:100%;">
				<option value="">-- Все бренды --</option>
				<option value="0">Без бренда</option>
				{$sel_brend}
			</select>
		</td>
	</tr>

	<tr>
		<td >Поиск наборов:</td>
		<td >
			<select name="sel_block" id="sel_block" style="width:100%;">
				<option value="null" selected>-- Выберите --</option>
				<option value="is_main">Ароматы на главной</option>
				<option value="is_subs">Ароматы на рассылку</option>
				<option value="is_sale">Распродажа</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td >Поиск по заголовку карточки:</td>
		<td >
			<input type="text" name="find_text" value="{$find_text}" style="width:100%;">
		</td>
	</tr>
    <tr>
		<td >Поиск url:</td>
		<td >
			<input type="text" name="url" value="{$url}" style="width:100%;">
		</td>
	</tr>

    <tr>
		<td >Без изображений:</td>
		<td >
			<input type="checkbox" {if $filter_image}CHECKED="" {/if} name="image" ">
		</td>
	</tr>
	
	
	
	<tr>
		<td align="right" colspan="2">
			<input type="submit" title="Найти" name="find_go" value="искать" style="width:100px;">
		</td>
	</tr>
</table>
</form>


<table width="100%" border="0" cellpadding="0" cellspacing="0" class="form_add">
	<tr>
		<td height="25px" bgcolor="#fafafa">
			&nbsp;Найдено записей: [&nbsp;{$all_items}&nbsp;]
		</td>
		
		<td align="right" bgcolor="#fafafa">Кол. страниц&nbsp;|&nbsp;
		{if $smarty.session.user_cpage eq '20'}
		<b>20</b>&nbsp;|&nbsp;
		{else}
		<a href="?main={$smarty.get.main}&module={$smarty.get.module}&action={$smarty.get.action}&id_cat={$smarty.get.id_cat}&cpag=20">20</a>&nbsp;|&nbsp;
		{/if}
		
		{if $smarty.session.user_cpage eq '50'}
		<b>50</b>&nbsp;|&nbsp;
		{else}
		<a href="?main={$smarty.get.main}&module={$smarty.get.module}&action={$smarty.get.action}&id_cat={$smarty.get.id_cat}&cpag=50">50</a>&nbsp;|&nbsp;
		{/if}
		
		{if $smarty.session.user_cpage eq '100'}
		<b>100</b>&nbsp;|&nbsp;
		{else}
		<a href="?main={$smarty.get.main}&module={$smarty.get.module}&action={$smarty.get.action}&id_cat={$smarty.get.id_cat}&cpag=100">100</a>&nbsp;|&nbsp;
		{/if}
		
		</td>
	</tr>
</table>
{/strip}