{strip}
    <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&id={$smarty.get.id}
	&action=edit_spec" method="post" enctype="multipart/form-data" name="form">

        <div id="tabs" class="flora">
            <ul class="ui-tabs-nav">
                <li class="ui-tabs-selected"><a href="#tab-1"><span>Основные данные</span></a></li>
                <li><a href="#tab-6"><span>Фото</span></a></li>
            </ul>
        </div>

        <div class="ui-tabs-panel ui-tabs-hide" id="tab-1">
            <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
                <tr>
                    <td align="left"><span style="color:red;">*</span>&nbsp;Заголовок:</td>
                    <td align="left">
                        <input type="text" name="title" value="{$m_catalog_data.title}" style="width:100%">
                    </td>
                </tr>

                <tr>
                    <td align="left">
                        <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;ЧПУ URL:
                                </td>
                                <td align="right" style="border:0px;">
                                    <a href="#"
                                       onmouseover="ddrivetip('Внимание!При ручном редактировании ЧПУ, не должно быть спецсимволов. Разрешается использовать в качестве разделителя только -', 200)"
                                       onmouseout="hideddrivetip()"
                                            >
                                        <img src="images/warnings.png" width="16" height="16" border="0"></a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td align="left">
                        <input disabled type="text" name="url" value="{$m_catalog_data.url}" style="width:100%;">
                    </td>
                </tr>



                <tr>
                    <td align="left" width="20%">&nbsp;&nbsp;Цена:</td>
                    <td align="left" width="80%">
                        <input type="text" name="price" value="{$m_catalog_data.price}" style="width:100%;">
                    </td>
                </tr>
            </table>
        </div>

        <div class="ui-tabs-panel" id="tab-6">
            {include file="catalog_photo.tpl"}
        </div>

        <div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
            <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
                <tr class="table_bg">
                    <td align="left" colspan="2">
                        &nbsp;&nbsp;Полное описание:
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        <textarea id="text" name="text" style="width:100%" rows=20>{$text}</textarea>
                    </td>
                </tr>

            </table>
        </div>


        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr class="table_bg_action">
                <td align="left" colspan="2">
                    <input type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
                    <input type="submit" name="save_add" value="Сохранить и добавить еще">&nbsp;
                    <input type="submit" name="save_list" value="Сохранить и перейти к списку">
                </td>
            </tr>
        </table>


    </form>
{/strip}


{literal}
    <script>
        $(function () {


            $("#get_foto").click(function () {

                var qw = $("#qw").attr('value');
                var lang = $("#lang option:selected").val();

                if (qw == '') {
                    alert('Введите запрос');
                }

                // чистим
                $("#last_more").remove();
                $("#msg").html('');


                // делаем запрос
                $.post("?ajax=catalog_moder&action=google", {
                    qw: qw,
                    lang: lang
                }, function (xml) {
                    $("message", xml).each(function (id) {

                        message = $("message", xml).get(id);
                        $("#msg").html($("msg", message).text());

                    });
                });
            });


            $("#select_foto").click(function () {
                var image_src = $('#msg input:checked').val();
                console.log(image_src)

                if (image_src == '') {
                    alert('Фото не выбрано');
                }


                // делаем запрос
                $.post("?ajax=catalog_moder&action=upload", {
                    dataType: "json",
                    url: image_src
                }, function (obj) {
                    if (typeof obj.error != 'undefined') {
                        alert(obj.error)
                    } else {
                        $('[name="image_path"]').val(obj.path)
                        $('#image1').show();
                        $('#image1').attr('src', obj.path);
                    }
                });

            });

            // еще фото
            $("#more_foto").live("click", function () {

                var start = parseInt($(this).attr('title')) + 1;
                var qw = $("#qw").attr('value');
                var lang = $("#lang option:selected").val();

                if (qw == '') {
                    alert('Введите запрос');
                }

                // чистим
                $("#last_more").hide();
                $("#more_foto").remove();
                $("#last_more").remove();

                // делаем запрос
                $.post("?ajax=catalog_moder&action=google", {
                    qw: qw,
                    lang: lang,
                    start: start
                }, function (xml) {
                    $("message", xml).each(function (id) {

                        message = $("message", xml).get(id);
                        $("#msg").append($("msg", message).text());

                    });
                });

            });

            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight',
                browse_button: 'upload_image',
                max_file_size: '20mb',
                multiple_queues: false,
                url: '?ajax=catalog_moder&action=upload',
                flash_swf_url: 'public/js/plupload/plupload.flash.swf',
                silverlight_xap_url: 'public/js/plupload/plupload.silverlight.xap',
                filters: [
                    {
                        title: "CSV только!",
                        extensions: "png,jpg,jpeg"
                    }
                ]
            });

            uploader.init();

            uploader.bind('FilesAdded', function (up, files) {
                /*
                 $.each(files, function(i, file) {
                 $('#filelist').append(
                 '<div id="' + file.id + '">' +
                 file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                 '</div>');
                 });
                 */

                up.refresh();
                uploader.start();
            });

            uploader.bind('UploadProgress', function (up, file) {
                $('#' + file.id + " b").html(file.percent + "%");
            });

            uploader.bind('Error', function (up, err) {
                up.refresh(); // Reposition Flash/Silverlight
            });

            uploader.bind('FileUploaded', function (up, file, info) {
                obj = JSON.parse(info.response);
                console.log(obj.path)

                $('[name="image_path"]').val(obj.path)
                $('#image1').show();
                $('#image1').attr('src', obj.path);

                _filename = info.response;
            });
        })


        <!-- Load Feather code -->


        <!-- Instantiate Feather -->

        var featherEditor = new Aviary.Feather({
            apiKey: 'niayffp54e4yc1wd',
            apiVersion: 3,
            theme: 'dark', // Check out our new 'light' and 'dark' themes!
            tools: 'all',
            appendTo: '',
            onSave: function (imageID, newURL) {
                //аплоадим новую картинку
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "?ajax=catalog_moder&action=upload",
                    data: {url: newURL, oldPath: $('[name="image_path"]').val()},
                    success: function (data) {
                        //обновляем картинку
                        $('#image1').attr('src', $('#image1').attr('src') + '?' + Math.random())
                        featherEditor.close();
                    }
                });


            },
            onError: function (errorObj) {
                alert(errorObj.message);
            }
        });


        function launchEditor(id, src) {
            $('[name="image_path"]').val($('#image1').attr('src'))

            featherEditor.launch({
                image: id,
                url: src
            });
            return false;
        }


    </script>
{/literal}