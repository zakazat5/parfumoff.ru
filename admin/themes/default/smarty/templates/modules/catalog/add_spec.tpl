{include file="editor.tpl"}
{strip}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&action=add_spec" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>Основные данные</span></a></li>

		{* <li class=""><a href="#tab-5"><span>Фото</span></a></li>*}
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">


	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Заголовок:</td>
		<td align="left">
			<input type="text" name="title" value="" style="width:100%" onkeyup="url_convert();" onchange="url_convert();">
		</td>
	</tr>
		
	<tr>
		<td align="left">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;ЧПУ URL:</td>
					<td align="right" style="border:0px;">
					<a href="#" 
					onmouseover="ddrivetip('Внимание!При ручном редактировании ЧПУ, не должно быть спецсимволов. Разрешается использовать в качестве разделителя только -', 200)" onmouseout="hideddrivetip()"
					>
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
					</td>
				</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="url" value="" style="width:100%;">
		</td>
	</tr>



	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;Цена:</td>
		<td align="left" width="80%">
			<input type="text" name="price" value="" style="width:100%;">
		</td>
	</tr>

	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;Превью:</td>
		<td align="left" width="80%">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	


</table>
</div>


<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="Сохранить и добавить еще">&nbsp;
		<input type="submit" name="save_list" value="Сохранить и перейти к списку">
	</td>
	</tr>
</table>

</form>
{/strip}