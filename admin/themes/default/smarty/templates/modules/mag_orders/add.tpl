{include file="calendar.tpl"}
{strip}
{literal}
    <style type="text/css">
        #map {
            min-height: 400px;
            background: #f5f5f5;
            border: 1px solid #c8c6c7;
            padding: 15px;
            margin-bottom: 30px;
        }
        * {
            font-family: Tahoma !important
        }
        .log_wrap {
            height: 233px;
            padding: 5px;
            font-size: 12pt;
            background: white;
            overflow-y: scroll;
            border: 1px solid silver;
        }
        .form_add td {
            color: black !important
        }
        .s_letters {
            font-size: 13px
        }
        .fl_left {
            float: left;
        }
        .sel_data {
            font-size: 12pt;
        }
        .clear {
            clear: both;
        }
        .line {
            margin-bottom: 15px;
        }
        .dostavka_item {
            background: #f5f5f5;
            border: 1px solid #c8c6c7;
            padding: 15px;
            position: relative;
            margin-bottom: 15px;
        }
        .dostavka_item .head {
            color: #969696
        }
        .dostavka_item .title {
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 10px;
        }
        .dostavka_item__checkbox {
            padding-left: 48px;
            position: relative;
            cursor: pointer;
            height: 33px;
            line-height: 33px;
        }
        .dostavka_item__checkbox.checked:after {
            background: url(/images/cart_v2/checkbox.png) 0px 0 no-repeat;
        }
        .dostavka_item__checkbox.gray:hover:after {
            background: url(/images/cart_v2/checkbox.png) 100% 0 no-repeat;
        }
        .dostavka_item__checkbox.checked.gray:after {
            background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
        }
        .dostavka_item__checkbox.checked.gray:hover:after {
            background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
        }
        .dostavka_item__checkbox:hover:after {
            background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
        }
        .dostavka_item__checkbox.checked:hover:after {
            background: url(/images/cart_v2/checkbox.png) 0px 0 no-repeat;
        }
        .dostavka_item__checkbox:after {
            display: block;
            position: absolute;
            content: '';
            width: 33px;
            background: url(/images/cart_v2/checkbox.png) 100% 0 no-repeat;
            height: 33px;
            left: 0;
            top: 0;
        }
        .ui-tabs-nav .ui-tabs-selected a span {
            font-size: 14px;
        }
        .text td {
            font-size: 14px !important;
            line-height:20px;
        }
        .abs_sel {
            font-size: 14px;

        }
        #msg3 {margin-bottom:20px;}
        #msg3 h4,#msg3 .abs_sel a {
            color: #f55d5c;
        }
        #msg4 h4, #msg4 .abs_sel a {
            color: #569ddb;
        }
        .brend_liter a {
            background-color:none !important;
            color:#000 !important;
            padding:5px;
            font-weight: bold;
            cursor:pointer;
        }
        .brend_liter a:hover {
            background-color:#000 !important;
            color:#fff !important;
            cursor:pointer;
        }

        .cat_item {
            width:80px;
            float:left;
            height:150px;
        }
        #sel_brend{font:12pt Arial;}

        .sel_data {width:100%;font-size:11px;color:#000;background:#fff;padding:2px;}
        .sel_catalog_data{cursor:pointer;}
        .sel_catalog_data_order{cursor:pointer;color:blue;}
        .sel_find_users{cursor:pointer;}
        .up {cursor:pointer;}
        .dn{cursor:pointer;}

        .mod_table td{background:#fff;}
        .mod_table_com td{background:#f7f7f7;}
        .dostavka_tooltip {
            position: absolute;
            background: #00cbf6;
            color: white;
            text-align: center;
            padding: 15px;
            display: none;
            width: 100px;
            z-index: 1000000;
        }
        .dostavka_tooltip:after {
            display: block;
            content: '';
            position: absolute;
            left: 50%;
            top: 100%;
            margin-left: -9px;
            border-left: 9px solid transparent;
            border-right: 9px solid transparent;
            border-top: 9px solid #00cbf6;
        }
        .dostavka_tooltip.left:after {
            display: block;
            content: '';
            position: absolute;
            left: 100%;
            top: 0;
            margin-left: -5px;
            margin-top: 9px;
            border-left: 18px solid #00cbf6;
            border-bottom: 9px solid transparent;
            border-top: 9px solid transparent;
        }

    </style>
{/literal}
    {include file="modules/mag_orders/add_js.tpl"}
{literal}
{/literal}

    <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=add_reg" method="post" enctype="multipart/form-data" id="form_add_reg">

    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
    <tr>
        <td>
            <div id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Поиск ароматов</span></a></li>
                </ul>
            </div>

            <div style="display: block;" class="ui-tabs-panel" id="tab-1">

                <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add">

                    <tr class="table_bg" >
                        <td align="left" colspan="2" class="brend_liter">
                            <div class="s_letters">
                                <a>A</a><a>B</a><a>C</a><a>D</a>
                                <a>E</a><a>F</a><a>G</a><a>H</a>
                                <a>I</a><a>J</a><a>K</a><a>L</a>
                                <a>M</a><a>N</a><a>O</a><a>P</a>
                                <a>Q</a><a>R</a><a>S</a><a>T</a>
                                <a>U</a><a>V</a><a>W</a><a>X</a>
                                <a>Y</a><a>Z</a>
                            </div>
                        </td>
                    </tr>

                    <tr >
                        <td align="center" width="30%"  bgcolor="White">
                            <div id="msg_order_img">image</div>
                        </td>
                        <td align="left" width="70%">
                            <select size="10" name="sel_brend" id="sel_brend" class="sel_data">
                                <option value="0" >-- Выберите Бренд --</option>
                                {$sel_brend}
                            </select>
                        </td>
                    </tr>
                    <tr >
                        <td align="right" ><h2 style="margin:0;">Аромат:</h2></td>
                        <td align="left" >
                            <span id="msg_order_title" ></span> <span id="msg_order_pol"></span>
                        </td>
                    </tr>


                </table>

                <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
                    <tr >
                        <td align="left" >
                            <a id="ar_comment_form_add" style="display:none;cursor:pointer;color:gray">-= <strong>Добавить</strong> Описание / Ошибку =-</a>
                            <div id="ar_comment_form" style="display:none;">
                                <textarea name="ar_comment_text" id="ar_comment_text" style="width:100%" rows=3></textarea>

                                <input type="button" name="ar_wiki" id="ar_wiki" value="Описание" style="color:blue">
                                <input type="button" name="ar_error" id="ar_error" value="Ошибка" style="color:red">
                            </div>
                        </td>
                    </tr>
                </table>

                <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
                    <tr >
                        <td align="left" >
                            <div id="ar_comment"></div>
                        </td>
                    </tr>
                </table>

                <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
                    <tr >
                        <td align="left" >
                            <div id="msg_order"></div>
                        </td>
                    </tr>
                </table>

            </div>

            <br>



        </td>
        <td valign="top" style="padding-left:30px">
            <div id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Список логирования</span></a></li>
                </ul>
            </div>


            <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add">
                <tr >
                    <td align="left" width="70%">
                        <div class="log_wrap">
                            <table width="100%">
                                <tr>
                                    <td style="width: 80px;"><b>Дата</b></td>
                                    <td><b>Юзер</b></td>
                                    <td><b>Действие</b></td>
                                    <td><b>Значение</b></td>

                                </tr>
                                {foreach from=$aHistory item=history}
                                    <tr>
                                        <td>{$history.date}<br>{$history.time}</td>
                                        <td><span style="padding: 4px;color:rgb(201, 0, 0)">{$history.user_name}</span></td>
                                        <td>{$history.action}</td>
                                        <td>{$history.text}</td>
                                    </tr>
                                {/foreach}

                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <div id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Поиск по коду товара</span></a></li>
                </ul>
            </div>
            <table width="100%" style="margin-bottom:25px">
                <tr>
                    <td valign="top"><input class="search_text"  type="text" style="width:100%;padding:9px 4px;"></td>
                    <td valign="top"><button class="btn btn-primary search">Искать</button></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">

            <table width="100%">
                <tr>
                    <td valign="top" width="100%">
                        <div style="margin-top:30px" id="tabs" class="flora">
                            <ul class="ui-tabs-nav">
                                <li class="ui-tabs-selected"><a href="#tab-1"><span>Корзина</span></a></li>
                            </ul>
                        </div>
                        <div style="display: block;" class="ui-tabs-panel" id="tab-1">


                            <table width="100%" border="0" cellpadding="5" cellspacing="2" class="form_add" id="table_add_orders" >
                                <tr class="table_bg">
                                    <td align="center" width="25" ></td>
                                    <td align="left" >Название</td>
                                    <td align="center" width="60">Цена</td>
                                    <td align="left" width="58">Кол-во</td>
                                    <td align="left" width="60">Стоимость</td>
                                    <td align="left" width="90">Наценка РК</td>
                                </tr>


                                {foreach from=$c_order_sum item=order_sum}
                                    <tr class="tr_item_orders_clear" id="tr_item_orders_{$order_sum.id_catalog_data_order}">
                                        <td>
                                            <input type="button" name="{$order_sum.id_catalog_data_order}" alt="{$order_sum.price}" class="delete_list" value="X" style="width:20px;color:red;font-weight: bold;border:0;background:none;">
                                        </td>
                                        <td align="left" >
                                            <input class="id_orders" type="hidden" name="id_orders[]" value="{$order_sum.id_catalog_data_order}">
                                            {$order_sum.title}
                                        </td>
                                        <td align="center" >
                                            <span id="price_orders_{$order_sum.id_catalog_data_order}">{$order_sum.sum}</span>
                                        </td>
                                        <td align="left" >
                                            <input style="width:25px;padding:1px;float:left;height:18px;" type="text" id="kol_orders_{$order_sum.id_catalog_data_order}" name="kol_orders_{$order_sum.id_catalog_data_order}" value="{$order_sum.kol}"  onkeydown="return false;">
                                            <a class="up" alt="{$order_sum.id_catalog_data_order}"><img src="images/nav/up.gif" ></a><br /><a alt="{$order_sum.id_catalog_data_order}" class="dn"><img src="images/nav/dn.gif" ></a>
                                        </td>
                                        <td align="left" >
					<span id="price_orders_sum_{$order_sum.id_catalog_data_order}">
					{$order_sum.price}
					</span>
                                        </td>
                                        <td align="center" >
					<span style="color:blue;">
					{$order_sum.gmargin}
					</span>
                                        </td>
                                    </tr>
                                {/foreach}

                            </table>

                            <table width="100%">
                                <tr>
                                    <td valign="top" width="60%">
                                        <table style="border-top:0" width="100%" border="0" cellpadding="3" cellspacing="1" class="form_add"  >
                                            <tr >
                                                <td width="250" style="padding-left:60px" align="left" >
                                                    Комментарий к заказу:
                                                </td>
                                                <td >
                                                    <textarea name="br_comment" rows="5" style="width:100%;">{$order.pComment}</textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="250" style="padding-left:60px" align="left">Комментарий склада:</td>
                                                <td align="left">
                                                    <textarea name="br_comment_sklad" style="width:100%;color:red" rows="3">{$order.pCommentSklad}</textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <div style="float:right;width:400px">
                                            <table width="400" border="0" cellpadding="5" cellspacing="2" >

                                                <tr >
                                                    <td align="right" width="220" >Сумма заказа:</td>
                                                    <td align="right" width="70" style=""><span id="big_sum">0</span> руб.</td>
                                                </tr>
                                                <tr >
                                                    <td align="right" width="220" style="color:blue;" >Скидка<span id="big_sum_percent_type">{if $order.dType eq '1'}(нак){elseif $order.dType eq '2'}(раз){/if}</span>:</td>
                                                    <td align="right" width="70" style="color:blue;"><span id="big_sum_percent">0</span> %</td>
                                                </tr>
                                                <tr>
                                                    <td align="right" width="220" style="color: green;">Доставка (курьером - b2c, почта РФ итд):</td>
                                                    <td width="70" align="right" style="color: green;"><span id="delivery_cost-text">0</span> руб.</td>
                                                </tr>
                                                <tr >
                                                    <td align="right" width="220" ><strong>Всего:</strong></td>
                                                    <td align="right" width="70"><strong><span id="big_sum_all" data-sum="0">0</span></strong> руб.
                                                        <input type="hidden" name="dSum" id="dSum" value="0"/></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                            <div style="clear:both"></div>

                            <!--<table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
		<td align="right">
		{if $order.status eq '1'}
			<input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
		{elseif $order.status eq '2'}
			<input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
		{elseif $order.status eq '5'}
			<input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
		{elseif $order.status eq '6'}
			<input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
		{elseif $order.status eq '3'}
			<input id="submit_form" type="submit" name="save" value="Заказ доставлен" style="color:grey;font-size:16pt;"  disabled>
		{elseif $order.status eq '0'}
			<input id="submit_form" type="submit" name="save" value="Заказ отклонен" style="color:grey;font-size:16pt;" disabled>
		{/if}
		</td>
		</tr>
	</table>-->

                        </div>
                        <br />


                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">

            <div id="msg"></div>
            <div id="msg1" style="position:relative;"></div>
            <div id="msg2" style="position:relative;"></div>




            <div style="clear:both"></div>




        </td>
        <td valign="top">
            <div id="msg3"></div>
            <div id="msg4"></div>
        </td>
    </tr>
    {include file="modules/mag_orders/user_form.tpl"}
    <tr>
        <td colspan="2">

            <div style="margin-top:30px" id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Варианты оплаты</span></a></li>
                </ul>
            </div>

            <table width="100%" class="checkbox_group_pay">
                <tr>
                    <td valign="top" >
                        <div class="dostavka_item">
                            <div data-value="1" data-width="250" data-toggle="tooltip" data-content="Если сумма заказа превышает 10000р, то данный тип оплаты не доступен" class="dostavka_item__checkbox checked">Наличными при получении</div>
                        </div>
                    </td>
                    <td>
                        <div class="dostavka_item">
                            <div data-value="2" data-width="250" data-toggle="tooltip" data-content="При этом способе оплате доставка будет стоить гораздо дешевле" class="dostavka_item__checkbox {if $order.payment_type==2}checked{/if}">Предоплата по квитанции</div>
                        </div>
                    </td>
                    <td>
                        <div class="dostavka_item">
                            <div data-value="3" data-width="250" data-toggle="tooltip" data-content="QIWI, Webmoney, Яндекс.Деньги, Rapida Online, MoneyMail, Деньги@mail.ru, Единый кошелек, EasyPay, LiqPay, Handy Bank" class="dostavka_item__checkbox  {if $order.payment_type==3}checked{/if}">Предоплата Robokassa</div>
                        </div>
                    </td>
                    <td>
                        <input class="payment_type" type="hidden" name="payment_type" value="1">
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td colspan="2">

            <div style="margin-top:30px" id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Доставка</span></a></li>
                </ul>
            </div>

            <table width="100%" class="">
                <tr id="couriers__wrapper">
                    <td valign="top" width="350">
                        <div class="dostavka_item" id="courier" data-type="курьером" data-modal="courier">
                            <div class="title">Доставка курьером</div>
                            <table width="100%">
                                <thead class="courier__table-header head">
                                <tr>
                                    <th>Служба доставки</th>
                                    <th>Срок</th>
                                    <th>Цена</th>
                                </tr>
                                </thead>
                                <tbody class="courier__table-body"></tbody>
                            </table>
                        </div>
                        <div class="dostavka_item" id="post" data-type="почта" data-modal="post">
                            <div class="title">Почта России</div>
                            <table width="100%">
                                <thead class="courier__table-header head">
                                <tr>
                                    <th>Служба доставки</th>
                                    <th>Срок</th>
                                    <th>Цена</th>
                                </tr>
                                </thead>
                                <tbody class="courier__table-body">
                                <tr class="courier__row" data-courier="0">
                                    <td style="position:relative;">
                                        <div data-width="250" data-placement="left" data-toggle="tooltip" data-content="Заказы высылаются в течение 1 дня ускоренной посылкой 1 Класса. Получить и оплатить товар Вы сможете на своем почтовом отделении. В момент прибытия посылки Вы получите извещение от Почты России. При оплате наложенным платежом Почта России взимает от 2 до 5% комиссию за перевод денег на наш счет." class="dostavka_item__checkbox">Почта России</div>
                                    </td>
                                    <td>4 дн.</td>
                                    <td>265 р.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="dostavka_item" id="self" data-type="самовывоз" data-modal="byself">
                            <div class="title">Самовывоз</div>
                            <table width="100%">
                                <thead class="courier__table-header head">
                                <tr>
                                    <th>Служба доставки</th>
                                    <th>Срок</th>
                                    <th>Цена</th>
                                </tr>
                                </thead>
                                <tbody class="courier__table-body"></tbody>
                            </table>
                            <div id="self_selected"></div>
                        </div>
                    </td>
                    <td valign="top" style="padding-left:20px">

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="map"></div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:60px" align="left">Дата доставки:</td>
                    <td align="left">
                        <input style="border:1px solid red;" type="text" name="date1" id="date1" value="{$order.DateEnd}" />
                        &nbsp;<input style="border:1px solid grey;" type="button" id="trigger1" value="изменить дату">
                        <input type="hidden" id="courier_address_id" value="{$order.courier_address_id}" name="courier_address_id"/>
                        <input type="hidden" id="delivery_cost" value="{$order.delivery_cost}" name="delivery_cost"/>
                    </td>
                </tr>


                <tr>
                    <td style="padding-left:60px"align="left">Отправить квитанцию:</td>
                    <td align="left">
                        <input type="checkbox" name="is_sending">
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:60px" align="left">Почт. идентификатор:</td>
                    <td align="left">
                        <input style="border:1px solid grey;width:190px;" maxlength="40" type="text" name="track" id="track" value="{$order.track}" />&nbsp;
                        Дата:&nbsp;<input style="border:1px solid grey;width:65px;" type="text" name="track_date" id="track_date" value="{$order.track_date}" />
                    </td>
                </tr>

            </table>
            <table width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td align="right">
                        <input id="clear_form" type="button" name="clear" value="Очистить"
                               style="color:grey;font-size:16pt;">&nbsp;
                        <input id="submit_form" type="submit" name="save" value="Сохранить заказ"
                               style="color:red;font-size:16pt;">
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    </form>
    </td>
    <td width="20">&nbsp;</td>
    <!-- Ароматы , правый блок -->
    <td valign="top">
    </td>
    </tr>
    </table>
{/strip}

{literal}
    <script type="text/javascript">
        Calendar.setup({
            inputField     	: "date1",
            ifFormat       	: "%Y-%m-%d %H:%M:%S",
            timeFormat     	: "24",
            button         	: "trigger1",
            singleClick    	: true,
            step           	: 1,
            showsTime		: true
        });
    </script>
{/literal}

{literal}
    <script type="text/javascript">
        $(function(){
            if ($('#track_date').length>0){
                Calendar.setup({
                    inputField     	: "track_date",
                    ifFormat       	: "%Y-%m-%d",
                    timeFormat     	: "24",
                    button         	: "date2",
                    singleClick    	: true,
                    step           	: 1,
                    showsTime		: false
                });
            }

            if ($('#date_birthday_input').length>0){

                Calendar.setup({
                    inputField     	: "date_birthday_input",
                    ifFormat       	: "%Y-%m-%d",
                    timeFormat     	: "24",
                    showsTime		: false
                });
            }

            if ($('#DateMoneyIncome').length>0){

                Calendar.setup({
                    inputField     	: "DateMoneyIncome",
                    ifFormat       	: "%Y-%m-%d",
                    timeFormat     	: "24",
                    showsTime		: false
                });
            }

            if ($('#payment_date').length>0){
                Calendar.setup({
                    inputField     	: "payment_date",
                    ifFormat       	: "%Y-%m-%d",
                    timeFormat     	: "24",
                    showsTime		: false
                });
            }
        })
    </script>
{/literal}

{literal}
    <script>
        var yMap,
                ymapsDef = $.Deferred(),
                ymapsResolved = false,
                isDeliveryCostInited = true;

        /**
         * Инициализация яндекс карт для служб доставки
         */
        function initYmaps() {

            yMap = new ymaps.Map('map', {
                center: [55.753994, 37.622093],
                zoom: 8,
                controls: ['zoomControl', 'typeSelector']
            });

            ymapsDef.resolve();
        }

        ymaps.ready(initYmaps);

        /**
         * Функция заполнения служб доставки по указанному адресу пользователя
         * или же по уже выбранной службе, сначала заполняет все возможные варианты по адресу
         * дальше выбирает нужный, если таковой был выбран по оформлении заказа
         *
         * @param {Number} kladr_id - целочисленный идентификатор кладра
         * @param {String} city - название города
         * @param {Object} resp - объект XHR запроса на получение "логистической компании"
         */
        function fillCouriers(kladr_id, city, resp) {

            $.when(ymapsDef).then(function() {
                AddressSuggestions.getLogistic(kladr_id, city).then(function() {

                    var $wrapper = $('#couriers__wrapper'),
                            $logistic = $('#self_selected'),
                            arr = [];

                    if (resp.delivery_type != "самовывоз") {
                        $wrapper.find('.courier__row[data-courier="'+resp.id+'"]').find('.dostavka_item__checkbox').trigger("click");//.addClass('checked');
                    } else {

                        arr.push("<span>Пользователь выбрал: <b>"+resp.courier_name+"</b></span>");
                        arr.push("<span>Время доставки от "+resp.delivery_days+" дн.</span>");
                        arr.push("<span>Стоимость доставки от "+resp.delivery_cost+" р.</span>");
                        $logistic.html(arr.join(""));
                    }

                    $("#courier_address_id").val(resp.id);
                });
            });
        }

        $.ajax({
            url: "/api/logistic_get/",
            data: {
                k: $("#courier_address_id").val(),
                t: $('input[name="payment_type"]').val()
            },
            success: function(resp) {

                // если ответ получен, то есть был выбран способ доставки
                if (resp.id != null && resp.id != 0) {

                    fillCouriers(resp.kladr_id_city, resp.name_city, resp);

                    // если же способ доставки не был выбран просто грузим по городу карту
                } else {

                    var addr = [],
                            result,
                            answer;

                    // заполнение адреса исходя из полей пользователя

                    //исключаем дублирование Москва Москва
                    if ($('#part_name').text() != "") {

                        fillCouriers($('#select_part').val(), resp.name_city, resp);
                    } else {

                        addr.push($('#region').val());
                        // заполнение адреса исходя из полей пользователя
                        if ($('#region').val() != $('#city').val()){
                            addr.push($('#city').val());
                        }
                        addr.push($('#step2_settlement').val());

                        addr = addr.join(" ");

                        answer = DadataApi.clean(addr);
                        answer.then(function(msg) {
                            if (typeof msg != 'undefined') {
                                result = AddressSuggestions.getCleanData(msg);
                                fillCouriers(result.kladr_id, addr, resp);
                            }
                        });
                    }
                }
            }
        });
    </script>
{/literal}