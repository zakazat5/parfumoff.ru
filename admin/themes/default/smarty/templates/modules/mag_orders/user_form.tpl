<tr>
    <td colspan="2">
        <div id="tabs" class="flora">
            <ul class="ui-tabs-nav">
                <li class="ui-tabs-selected"><a href="#tab-1"><span>Клиент
                                <input type="text" name="users_search" id="users_search" style="width:150px;margin-left: 10px;">
                                <select name="users_type" id="users_type" >
                                    <option value="id" selected="">ID</option>
                                    <option value="email">EMAIL</option>
                                    <option value="fio">FIO</option>
                                    <option value="phone">Телефон</option>
                                </select>
                                <button type="button" name="users_find" id="users_find" style="height: 18px;line-height: 1">Поиск</button>
                            </span>
                    </a></li>
            </ul>
            <div id="block_find_users">
                <div id="msg_find_user" style="width: 500px">

                </div>
            </div>

        </div>

    </td>
</tr>
<tr style="border:1px solid #789eab;background:#f5f5f5" id="table_info_users">
    <td width="650" valign="top" style="border-left:1px solid #789eab;border-bottom:1px solid #789eab;">

        <div style="display: block;" class="ui-tabs-panel" id="tab-1">

            <table width="100%"  border="0" cellpadding="5" cellspacing="2" class="form_add">
                <tr >
                    <td align="left" width="30%"><strong>ID</strong></td>
                    <td align="left" width="70%">
                        <input id="user_id" type="hidden" name="user_id" value="{$user.id}">
                        <input type="hidden" id="user_percent" name="user_percent" value="{$order.dPercent}" >
                        <input type="hidden" id="user_percent_type" name="user_percent_type" value="{$order.dType}" >
                        <input type="hidden" id="user_big_sum_all" name="user_big_sum_all" value="{$order.dSum}" >
                        <input type="hidden" id="user_big_sum" name="user_big_sum" value="{$order.Sum}" >
                        <input type="hidden" name="user_region" value="{$user.region}" >
                        <span class="user_id">{$user.id}</span>
                    </td>
                </tr>

                <tr >
                    <td align="left" width="30%"><strong>E-mail</strong></td>
                    <td align="left" width="70%"><span id="email">{$user.email}</span><input id="email_input" name="email"  class="hide"  value="{$user.email}" style="width: 250px;"></a>
                    </td>
                </tr>
                <tr >
                    <td align="left" width="30%"><strong>ФИО</strong></td>
                    <td align="left" width="70%"><span id="fio">{$user.fio}</span><input id="fio_input" name="fio" class="hide"  value="{$user.fio}" style="width: 250px;"></td>
                </tr>

                <tr >
                    <td align="left" width="30%"><strong>Телефон</strong></td>
                    <td align="left" width="70%"><span id="phone">{$user.phone}</span><input id="phone_input" name="phone" class="hide"  value="{$user.phone}" style="width: 250px;"></td>
                </tr>

                <tr >
                    <td align="left" width="30%"><b>Скидка</b></td>
                    <td align="left" width="70%"><span id="d_percent">{if $user}{$user.d_percent}{else}0{/if}</span>%</td>
                </tr>
                <tr >
                    <td align="left" width="30%"><b>Накопленная сумма</b></td>
                    <td align="left" width="70%"><span id="d_sum_all">{$user.d_sum_all}</span></td>
                </tr>

                <tr >
                    <td align="left" width="30%"><b>День рождения</b></td>
                    <td align="left" width="70%"><span id="date_birthday">{$user.date_birthday}</span><input name="date_birthday" id="date_birthday_input" class="hide"  value="{$user.date_birthday}"></td>
                </tr>
                <tr >
                    <td align="left" width="30%"><b>Пол</b></td>
                    <td align="left" width="70%"><span id="pol">{$user.pol}</span><select class="hide" name="pol"><option  {if $user.pol=='M'}SELECTED=""{/if} value="M">M<option {if $user.pol=='F'}SELECTED=""{/if} value="F">F</select>   </td>
                </tr>


            </table>

        </div>

    </td>
    <td style="border-right:1px solid #789eab;border-bottom:1px solid #789eab;" valign="top">


        <table width="100%"  border="0" cellpadding="5" cellspacing="2" class="form_add ">
            <tr >
                <td colspan="2">
                    <table class="address_edit" style="width: 70%;display: none">
                        {include file="address.tpl"}
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="address" style="width: 100%">
                        <tr>
                            <td align="left" width="30%"><strong>Область регион</strong></td>
                            <td align="left" width="70%"><span id="region">{$user.address_region}</span></td>
                        </tr>
                        <tr>
                            <td align="left" width="30%"><strong>Город</strong></td>
                            <td align="left" width="70%"><span id="city">{$user.address_city}</span></td>
                        </tr>

                        <tr>
                            <td align="left" width="30%"><strong>Район (Мск, Питер)</strong></td>
                            <td align="left" width="70%"><span id="part_name">{$list_capital_districts[$user.address_part]}</span></td>
                        </tr>

                        <tr>
                            <td align="left" width="30%"><strong>Метро</strong></td>
                            <td align="left" width="70%"><span id="metro">{$aMetro[$user.metro].name}</span></td>
                        </tr>



                        <tr>
                            <td align="left" width="30%"><strong>Населенный пункт</strong></td>
                            <td align="left" width="70%"><span id="settlement">{$user.address_settlement}</span></td>
                        </tr>

                        <tr>
                            <td align="left" width="30%"><strong>Улица</strong></td>
                            <td align="left" width="70%"><span id="street">{$user.address_street}</span></td>
                        </tr>
                        <tr>
                            <td valign="top" class="form_add" style="padding:0;border-bottom:0" colspan="2">
                                <table style="" cellpadding="4" cellspacing="2" width="100%">
                                    <tr>
                                        <td align="left" width="16%" ><strong>Дом:</strong></td>
                                        <td align="left" width="16%" ><span id="house">{$user.address_house}</span></td>
                                        <td align="left" width="16%" ><strong>Корпус:</strong></td>
                                        <td align="left" width="16%" ><span id="corpus">{$user.address_corpus}</span></td>
                                        <td align="left" width="16%" ><strong>Строение:</strong></td>
                                        <td align="left" width="16%" ><span id="stroenie">{$user.address_stroenie}</span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" class="form_add" style="padding:0;border-bottom:0" colspan="2">
                                <table style="" cellpadding="4" cellspacing="2" width="100%">
                                    <tr>
                                        <td align="left" width="16%" ><strong>Квартира:</strong></td>
                                        <td align="left" width="16%" ><span id="flat">{$user.address_flat}</span></td>
                                        <td align="left" width="16%" ><strong>Домофон:</strong></td>
                                        <td align="left" width="16%" ><span id="domophone">{$user.address_domophone}</span></td>
                                        <td align="left" width="16%" ><strong>Этаж:</strong></td>
                                        <td align="left" width="16%" ><span id="floor">{$user.address_floor}</span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" ><strong>Индекс</strong></td>
                            <td align="left" ><span id="postcode">{$user.postcode}</span></td>
                        </tr>
                    </table>
                </td>


            </tr>



            <tr>
                <td><a style="color:white" href="" class="btn btn-warning edit">Редактировать</a></td>
                <td><a style="color:white" href="" class="btn btn-primary save">Сохранить</a></td>
            </tr>
        </table>
        <br>
    </td>
</tr>