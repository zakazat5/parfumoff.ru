{include file="calendar.tpl"}
{strip}
{literal}
    <style type="text/css">
        #map {
            min-height: 400px;
            background: #f5f5f5;
            border: 1px solid #c8c6c7;
            padding: 15px;
            margin-bottom: 30px;
        }
        * {
            font-family: Tahoma !important
        }
        .log_wrap {
            height: 233px;
            padding: 5px;
            font-size: 12pt;
            background: white;
            overflow-y: scroll;
            border: 1px solid silver;
        }
        .form_add td {
            color: black !important
        }
        .s_letters {
            font-size: 13px
        }
        .fl_left {
            float: left;
        }
        .sel_data {
            font-size: 12pt;
        }
        .clear {
            clear: both;
        }
        .line {
            margin-bottom: 15px;
        }
        .dostavka_item {
            background: #f5f5f5;
            border: 1px solid #c8c6c7;
            padding: 15px;
            position: relative;
            margin-bottom: 15px;
        }
        .dostavka_item .head {
            color: #969696
        }
        .dostavka_item .title {
            font-size: 16px;
            font-weight: bold;
            margin-bottom: 10px;
        }
        .dostavka_item__checkbox {
            padding-left: 48px;
            position: relative;
            cursor: pointer;
            height: 33px;
            line-height: 33px;
        }
        .dostavka_item__checkbox.checked:after {
            background: url(/images/cart_v2/checkbox.png) 0px 0 no-repeat;
        }
        .dostavka_item__checkbox.gray:hover:after {
            background: url(/images/cart_v2/checkbox.png) 100% 0 no-repeat;
        }
        .dostavka_item__checkbox.checked.gray:after {
            background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
        }
        .dostavka_item__checkbox.checked.gray:hover:after {
            background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
        }
        .dostavka_item__checkbox:hover:after {
            background: url(/images/cart_v2/checkbox.png) -49px 0 no-repeat;
        }
        .dostavka_item__checkbox.checked:hover:after {
            background: url(/images/cart_v2/checkbox.png) 0px 0 no-repeat;
        }
        .dostavka_item__checkbox:after {
            display: block;
            position: absolute;
            content: '';
            width: 33px;
            background: url(/images/cart_v2/checkbox.png) 100% 0 no-repeat;
            height: 33px;
            left: 0;
            top: 0;
        }
        .ui-tabs-nav .ui-tabs-selected a span {
            font-size: 14px;
        }
        .text td {
            font-size: 14px !important;
            line-height:20px;
        }
        .abs_sel {
            font-size: 14px;

        }
        #msg3 {margin-bottom:20px;}
        #msg3 h4,#msg3 .abs_sel a {
            color: #f55d5c;
        }
        #msg4 h4, #msg4 .abs_sel a {
            color: #569ddb;
        }
        .brend_liter a {
            background-color:none !important;
            color:#000 !important;
            padding:5px;
            font-weight: bold;
            cursor:pointer;
        }
        .brend_liter a:hover {
            background-color:#000 !important;
            color:#fff !important;
            cursor:pointer;
        }

        .cat_item {
            width:80px;
            float:left;
            height:150px;
        }
        #sel_brend{font:12pt Arial;}

        .sel_data {width:100%;font-size:11px;color:#000;background:#fff;padding:2px;}
        .sel_catalog_data{cursor:pointer;}
        .sel_catalog_data_order{cursor:pointer;color:blue;}
        .sel_find_users{cursor:pointer;}
        .up {cursor:pointer;}
        .dn{cursor:pointer;}

        .mod_table td{background:#fff;}
        .mod_table_com td{background:#f7f7f7;}
        .dostavka_tooltip {
            position: absolute;
            background: #00cbf6;
            color: white;
            text-align: center;
            padding: 15px;
            display: none;
            width: 100px;
            z-index: 1000000;
        }
        .dostavka_tooltip:after {
            display: block;
            content: '';
            position: absolute;
            left: 50%;
            top: 100%;
            margin-left: -9px;
            border-left: 9px solid transparent;
            border-right: 9px solid transparent;
            border-top: 9px solid #00cbf6;
        }
        .dostavka_tooltip.left:after {
            display: block;
            content: '';
            position: absolute;
            left: 100%;
            top: 0;
            margin-left: -5px;
            margin-top: 9px;
            border-left: 18px solid #00cbf6;
            border-bottom: 9px solid transparent;
            border-top: 9px solid transparent;
        }

    </style>
{/literal}
    {include file="modules/mag_orders/edit_reg_js.tpl"}
{literal}
{/literal}

    <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=edit_reg&id_order={$smarty.get.id_order}&status={$smarty.get.status}" method="post" enctype="multipart/form-data" id="form_add_reg">

    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
    <tr>
        <td>
            <div id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Поиск ароматов</span></a></li>
                </ul>
            </div>

            <div style="display: block;" class="ui-tabs-panel" id="tab-1">

                <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add">

                    <tr class="table_bg" >
                        <td align="left" colspan="2" class="brend_liter">
                            <div class="s_letters">
                                <a>A</a><a>B</a><a>C</a><a>D</a>
                                <a>E</a><a>F</a><a>G</a><a>H</a>
                                <a>I</a><a>J</a><a>K</a><a>L</a>
                                <a>M</a><a>N</a><a>O</a><a>P</a>
                                <a>Q</a><a>R</a><a>S</a><a>T</a>
                                <a>U</a><a>V</a><a>W</a><a>X</a>
                                <a>Y</a><a>Z</a>
                            </div>
                        </td>
                    </tr>

                    <tr >
                        <td align="center" width="30%"  bgcolor="White">
                            <div id="msg_order_img">image</div>
                        </td>
                        <td align="left" width="70%">
                            <select size="10" name="sel_brend" id="sel_brend" class="sel_data">
                                <option value="" >-- Выберите Бренд --</option>
                                <option value="0" >Без бренда</option>
                                {$sel_brend}
                            </select>
                        </td>
                    </tr>
                    <tr >
                        <td align="right" ><h2 style="margin:0;">Аромат:</h2></td>
                        <td align="left" >
                            <span id="msg_order_title" ></span> <span id="msg_order_pol"></span>
                        </td>
                    </tr>


                </table>

                <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
                    <tr >
                        <td align="left" >
                            <a id="ar_comment_form_add" style="display:none;cursor:pointer;color:gray">-= <strong>Добавить</strong> Описание / Ошибку =-</a>
                            <div id="ar_comment_form" style="display:none;">
                                <textarea name="ar_comment_text" id="ar_comment_text" style="width:100%" rows=3></textarea>

                                <input type="button" name="ar_wiki" id="ar_wiki" value="Описание" style="color:blue">
                                <input type="button" name="ar_error" id="ar_error" value="Ошибка" style="color:red">
                            </div>
                        </td>
                    </tr>
                </table>

                <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
                    <tr >
                        <td align="left" >
                            <div id="ar_comment"></div>
                        </td>
                    </tr>
                </table>

                <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add" style="border-top:0px;">
                    <tr >
                        <td align="left" >
                            <div id="msg_order"></div>
                        </td>
                    </tr>
                </table>

            </div>

            <br>



        </td>
        <td valign="top" style="padding-left:30px">
            <div id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Список логирования</span></a></li>
                </ul>
            </div>


            <table width="500" border="0" cellpadding="5" cellspacing="2" class="form_add">
                <tr >
                    <td align="left" width="70%">
                        <div class="log_wrap">
                            <table width="100%">
                                <tr>
                                    <td style="width: 80px;"><b>Дата</b></td>
                                    <td><b>Юзер</b></td>
                                    <td><b>Действие</b></td>
                                    <td><b>Значение</b></td>

                                </tr>
                                {foreach from=$aHistory item=history}
                                    <tr>
                                        <td>{$history.date}<br>{$history.time}</td>
                                        <td><span style="padding: 4px;color:rgb(201, 0, 0)">{$history.user_name}</span></td>
                                        <td>{$history.action}</td>
                                        <td>{$history.text}</td>
                                    </tr>
                                {/foreach}

                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <div id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Поиск по коду товара</span></a></li>
                </ul>
            </div>
            <table width="100%" style="margin-bottom:25px">
                <tr>
                    <td valign="top"><input class="search_text"  type="text" style="width:100%;padding:9px 4px;"></td>
                    <td valign="top"><button class="btn btn-primary search">Искать</button></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">

            <table width="100%">
                <tr>
                    <td valign="top" width="100%">
                        <div style="margin-top:30px" id="tabs" class="flora">
                            <ul class="ui-tabs-nav">
                                <li class="ui-tabs-selected"><a href="#tab-1"><span>Корзина</span></a></li>
                            </ul>
                        </div>
                        <div style="display: block;" class="ui-tabs-panel" id="tab-1">


                            <table width="100%" border="0" cellpadding="5" cellspacing="2" class="form_add" id="table_add_orders" >
                                <tr class="table_bg">
                                    <td align="center" width="25" ></td>
                                    <td align="left" >Название</td>
                                    <td align="center" width="60">Цена</td>
                                    <td align="left" width="58">Кол-во</td>
                                    <td align="left" width="60">Стоимость</td>
                                    <td align="left" width="90">Наценка РК</td>
                                </tr>


                                {foreach from=$c_order_sum item=order_sum}
                                    <tr class="tr_item_orders_clear" id="tr_item_orders_{$order_sum.id_catalog_data_order}">
                                        <td>
                                            <input type="button" name="{$order_sum.id_catalog_data_order}" alt="{$order_sum.price}" class="delete_list" value="X" style="width:20px;color:red;font-weight: bold;border:0;background:none;">
                                        </td>
                                        <td align="left" >
                                            <input class="id_orders" type="hidden" name="id_orders[]" value="{$order_sum.id_catalog_data_order}">
                                            {$order_sum.title}
                                        </td>
                                        <td align="center" >
                                            <span id="price_orders_{$order_sum.id_catalog_data_order}">{$order_sum.sum}</span>
                                        </td>
                                        <td align="left" >
                                            <input style="width:25px;padding:1px;float:left;height:18px;" type="text" id="kol_orders_{$order_sum.id_catalog_data_order}" name="kol_orders_{$order_sum.id_catalog_data_order}" value="{$order_sum.kol}"  onkeydown="return false;">
                                            <input style="width:25px;padding:1px;float:left;height:18px;display: none" type="text" id="ordersSum_kol_{$order_sum.articul_catalog_data_order}" name="ordersSum_kol[{$order_sum.articul_catalog_data_order}]" value="{$order_sum.kol}"  onkeydown="return false;">
                                            <a class="up" data-articul="{$order_sum.articul_catalog_data_order}" alt="{$order_sum.id_catalog_data_order}"><img src="images/nav/up.gif" ></a><br /><a alt="{$order_sum.id_catalog_data_order}" data-articul="{$order_sum.articul_catalog_data_order}" class="dn"><img src="images/nav/dn.gif" ></a>
                                        </td>
                                        <td align="left" >
					<span class="price_item_sum" id="price_orders_sum_{$order_sum.id_catalog_data_order}">
					{$order_sum.price}
					</span>
                                        </td>
                                        <td align="center" >
					<span style="color:blue;">
					{$order_sum.gmargin}
					</span>
                                        </td>
                                    </tr>
                                {/foreach}

                            </table>

                            <table width="100%">
                                <tr>
                                    <td valign="top" width="60%">
                                        <table style="border-top:0" width="100%" border="0" cellpadding="3" cellspacing="1" class="form_add"  >
                                            <tr >
                                                <td width="250" style="padding-left:60px" align="left" >
                                                    Комментарий к заказу:
                                                </td>
                                                <td >
                                                    <textarea name="br_comment" rows="5" style="width:100%;">{$order.pComment}</textarea>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="250" style="padding-left:60px" align="left">Комментарий склада:</td>
                                                <td align="left">
                                                    <textarea name="br_comment_sklad" style="width:100%;color:red" rows="3">{$order.pCommentSklad}</textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td valign="top">
                                        <div style="float:right;width:400px">
                                            <table width="400" border="0" cellpadding="5" cellspacing="2" >

                                                <tr >
                                                    <td align="right" width="220" >Сумма заказа:</td>
                                                    <td align="right" width="70" style=""><span id="big_sum">{$order.Sum}</span> руб.</td>
                                                </tr>
                                                <tr >
                                                    <td align="right" width="220" style="color:blue;" >Скидка<span id="big_sum_percent_type">{if $order.dType eq '1'}(нак){elseif $order.dType eq '2'}(раз){/if}</span>:</td>
                                                    <td align="right" width="70" style="color:blue;"><span id="big_sum_percent">{$order.dPercent}</span> %</td>
                                                </tr>
                                                <tr>
                                                    <td align="right" width="220" style="color: green;">Доставка (курьером - b2c, почта РФ итд):</td>
                                                    <td width="70" align="right" style="color: green;"><span id="delivery_cost-text">{$order.delivery_cost}</span> руб.</td>
                                                </tr>
                                                <tr >
                                                    <td align="right" width="220" ><strong>Всего:</strong></td>
                                                    <td align="right" width="70"><strong><span id="big_sum_all" data-sum="{$order.dSum}">{$order.dSum+$order.delivery_cost}</span></strong> руб.
                                                        <input type="hidden" name="dSum" id="dSum" value="{$order.dSum}"/>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>

                            <div style="clear:both"></div>

                            <!--<table width="100%" border="0" cellpadding="5" cellspacing="0">
		<tr>
		<td align="right">
		{if $order.status eq '1'}
			<input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
		{elseif $order.status eq '2'}
			<input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
		{elseif $order.status eq '5'}
			<input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
		{elseif $order.status eq '6'}
			<input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
		{elseif $order.status eq '3'}
			<input id="submit_form" type="submit" name="save" value="Заказ доставлен" style="color:grey;font-size:16pt;"  disabled>
		{elseif $order.status eq '0'}
			<input id="submit_form" type="submit" name="save" value="Заказ отклонен" style="color:grey;font-size:16pt;" disabled>
		{/if}
		</td>
		</tr>
	</table>-->

                        </div>
                        <br />


                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td valign="top">

            <div id="msg"></div>
            <div id="msg1" style="position:relative;"></div>
            <div id="msg2" style="position:relative;"></div>




            <div style="clear:both"></div>




        </td>
        <td valign="top">
            <div id="msg3"></div>
            <div id="msg4"></div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Клиент {if $user==false}Не найден{/if}</span></a></li>
                </ul>
            </div>
        </td>
    </tr>
    <tr style="border:1px solid #789eab;background:#f5f5f5" id="table_info_users">
        <td width="650" valign="top" style="border-left:1px solid #789eab;border-bottom:1px solid #789eab;">

            <div style="display: block;" class="ui-tabs-panel" id="tab-1">

                <table width="100%"  border="0" cellpadding="5" cellspacing="2" class="form_add">
                    <tr >
                        <td align="left" width="30%"><strong>ID</strong></td>
                        <td align="left" width="70%"><input type="hidden" name="user_id" value="{$user.id}">
                            <input type="hidden" id="user_percent" name="user_percent" value="{$order.dPercent}" >
                            <input type="hidden" id="user_percent_type" name="user_percent_type" value="{$order.dType}" >
                            <input type="hidden" id="user_big_sum_all" name="user_big_sum_all" value="{$order.dSum}" >
                            <input type="hidden" id="user_big_sum" name="user_big_sum" value="{$order.Sum}" >
                            <input type="hidden" name="user_region" value="{$user.region}" >
                            {$user.id}
                        </td>
                    </tr>

                    <tr >
                        <td align="left" width="30%"><strong>E-mail</strong></td>
                        <td align="left" width="70%"><span id="email">{$user.email}</span><input name="email"  class="hide"  value="{$user.email}" style="width: 250px;"></a>
                        </td>
                    </tr>
                    <tr >
                        <td align="left" width="30%"><strong>ФИО</strong></td>
                        <td align="left" width="70%"><span id="fio">{$oUser->getFio()}</span><input name="fio" class="hide"  value="{$user.fio}" style="width: 250px;"></td>
                    </tr>

                    <tr >
                        <td align="left" width="30%"><strong>Телефон</strong></td>
                        <td align="left" width="70%"><span id="phone">{$user.phone}</span><input name="phone" class="hide"  value="{$user.phone}" style="width: 250px;"></td>
                    </tr>

                    {if $user.phone2!=null}
                        <tr >
                            <td align="left" width="30%"><strong>Телефон 2</strong></td>
                            <td align="left" width="70%"><span id="phone2">{$user.phone2}</span><input name="phone2" class="hide"  value="{$user.phone2}" style="width: 250px;"></td>
                        </tr>
                    {/if}

                    <tr >
                        <td align="left" width="30%"><b>Скидка</b></td>
                        <td align="left" width="70%"><span id="d_percent">{if $user}{$user.d_percent}{else}0{/if}</span>%</td>
                    </tr>
                    <tr >
                        <td align="left" width="30%"><b>Накопленная сумма</b></td>
                        <td align="left" width="70%"><span id="d_sum_all">{$user.d_sum_all}</span></td>
                    </tr>

                    <tr >
                        <td align="left" width="30%"><b>День рождения</b></td>
                        <td align="left" width="70%"><span id="date_birthday">{$user.date_birthday}</span><input name="date_birthday" id="date_birthday_input" class="hide"  value="{$user.date_birthday}"></td>
                    </tr>
                    <tr >
                        <td align="left" width="30%"><b>Пол</b></td>
                        <td align="left" width="70%"><span id="pol">{$user.pol}</span><select class="hide" name="pol"><option  {if $user.pol=='M'}SELECTED=""{/if} value="M">M<option {if $user.pol=='F'}SELECTED=""{/if} value="F">F</select>   </td>
                    </tr>


                </table>

            </div>

        </td>
        <td style="border-right:1px solid #789eab;border-bottom:1px solid #789eab;" valign="top">
            {if $order.pAddress!=''}<span style="color:red">Проверьте по старому адресу: </span>{$order.pAddress}{/if}

            <table width="100%"  border="0" cellpadding="5" cellspacing="2" class="form_add ">
                <tr>
                    <td colspan="2">
                        <table class="address_edit" style="width: 70%;display: none">
                            {include file="address.tpl"}
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table class="address" style="width: 100%">
                            <tr>
                                <td align="left" width="30%"><strong>Область регион</strong></td>
                                <td align="left" width="70%"><span id="region">{$user.address_region}</span></td>
                            </tr>
                            <tr>
                                <td align="left" width="30%"><strong>Город/район</strong></td>
                                <td align="left" width="70%"><span id="city">{$user.address_city}</span></td>
                            </tr>

                            <tr>
                                <td align="left" width="30%"><strong>Район (Мск, Питер)</strong></td>
                                <td align="left" width="70%"><span id="part_name">{$list_capital_districts[$user.address_part]}</span></td>
                            </tr>

                            <tr>
                                <td align="left" width="30%"><strong>Метро</strong></td>
                                <td align="left" width="70%"><span id="metro">{$aMetro[$user.metro].name}</span></td>
                            </tr>



                            <tr>
                                <td align="left" width="30%"><strong>Населенный пункт</strong></td>
                                <td align="left" width="70%"><span id="settlement">{$user.address_settlement}</span></td>
                            </tr>

                            <tr>
                                <td align="left" width="30%"><strong>Улица</strong></td>
                                <td align="left" width="70%"><span id="street">{$user.address_street}</span></td>
                            </tr>
                            <tr>
                                <td valign="top" class="form_add" style="padding:0;border-bottom:0" colspan="2">
                                    <table style="" cellpadding="4" cellspacing="2" width="100%">
                                        <tr>
                                            <td align="left" width="16%" ><strong>Дом:</strong></td>
                                            <td align="left" width="16%" ><span id="house">{$user.address_house}</span></td>
                                            <td align="left" width="16%" ><strong>Корпус:</strong></td>
                                            <td align="left" width="16%" ><span id="corpus">{$user.address_corpus}</span></td>
                                            <td align="left" width="16%" ><strong>Строение:</strong></td>
                                            <td align="left" width="16%" ><span id="stroenie">{$user.address_stroenie}</span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="form_add" style="padding:0;border-bottom:0" colspan="2">
                                    <table style="" cellpadding="4" cellspacing="2" width="100%">
                                        <tr>
                                            <td align="left" width="16%" ><strong>Квартира:</strong></td>
                                            <td align="left" width="16%" ><span id="flat">{$user.address_flat}</span></td>
                                            <td align="left" width="16%" ><strong>Домофон:</strong></td>
                                            <td align="left" width="16%" ><span id="domophone">{$user.address_domophone}</span></td>
                                            <td align="left" width="16%" ><strong>Этаж:</strong></td>
                                            <td align="left" width="16%" ><span id="floor">{$user.address_floor}</span></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" ><strong>Индекс</strong></td>
                                <td align="left" ><span id="postcode">{$user.postcode}</span></td>
                            </tr>
                        </table>
                    </td>


                </tr>



                <tr>
                    <td><a style="color:white" href="" class="btn btn-warning edit">Редактировать</a></td>
                    <td><a style="color:white" href="" class="btn btn-primary save">Сохранить</a></td>
                </tr>
            </table>
            <br>
        </td>
    </tr>
    <tr>
        <td colspan="2">

            <div style="margin-top:30px" id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Варианты оплаты</span></a></li>
                </ul>
            </div>

            <table width="100%" class="checkbox_group_pay">
                <tr>
                    <td valign="top" >
                        <div class="dostavka_item">
                            <div data-value="1" data-width="250" data-toggle="tooltip" data-content="Если сумма заказа превышает 10000р, то данный тип оплаты не доступен" class="dostavka_item__checkbox {if $order.payment_type==1}checked{/if}">Наличными при получении</div>
                        </div>
                    </td>
                    <td>
                        <div class="dostavka_item">
                            <div data-value="2" data-width="250" data-toggle="tooltip" data-content="При этом способе оплате доставка будет стоить гораздо дешевле" class="dostavka_item__checkbox {if $order.payment_type==2}checked{/if}">Предоплата по квитанции</div>
                        </div>
                    </td>
                    <td>
                        <div class="dostavka_item">
                            <div data-value="3" data-width="250" data-toggle="tooltip" data-content="QIWI, Webmoney, Яндекс.Деньги, Rapida Online, MoneyMail, Деньги@mail.ru, Единый кошелек, EasyPay, LiqPay, Handy Bank" class="dostavka_item__checkbox  {if $order.payment_type==3}checked{/if}">Предоплата Robokassa</div>
                        </div>
                    </td>
                    <td>
                        <input class="payment_type" type="hidden" name="payment_type" value="{$order.payment_type}">
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td colspan="2">

            <div style="margin-top:30px" id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Доставка</span></a></li>
                </ul>
            </div>

            <table width="100%" class="">
                <tr id="couriers__wrapper">
                    <td valign="top" width="350">
                        <div class="dostavka_item" id="courier" data-type="курьером" data-modal="courier">
                            <div class="title">Доставка курьером</div>
                            <table width="100%">
                                <thead class="courier__table-header head">
                                <tr>
                                    <th>Служба доставки</th>
                                    <th>Срок</th>
                                    <th>Цена</th>
                                </tr>
                                </thead>
                                <tbody class="courier__table-body"></tbody>
                            </table>
                        </div>
                        <div class="dostavka_item" id="post" data-type="почта" data-modal="post">
                            <div class="title">Почта России</div>
                            <table width="100%">
                                <thead class="courier__table-header head">
                                <tr>
                                    <th>Служба доставки</th>
                                    <th>Срок</th>
                                    <th>Цена</th>
                                </tr>
                                </thead>
                                <tbody class="courier__table-body">
                                <tr class="courier__row" data-courier="0">
                                    <td style="position:relative;">
                                        <div data-width="250" data-placement="left" data-toggle="tooltip" data-content="Заказы высылаются в течение 1 дня ускоренной посылкой 1 Класса. Получить и оплатить товар Вы сможете на своем почтовом отделении. В момент прибытия посылки Вы получите извещение от Почты России. При оплате наложенным платежом Почта России взимает от 2 до 5% комиссию за перевод денег на наш счет." class="dostavka_item__checkbox">Почта России</div>
                                    </td>
                                    <td>4 дн.</td>
                                    <td>265 р.</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="dostavka_item" id="self" data-type="самовывоз" data-modal="byself">
                            <div class="title">Самовывоз</div>
                            <table width="100%">
                                <thead class="courier__table-header head">
                                <tr>
                                    <th>Служба доставки</th>
                                    <th>Срок</th>
                                    <th>Цена</th>
                                </tr>
                                </thead>
                                <tbody class="courier__table-body"></tbody>
                            </table>
                            <div id="self_selected"></div>
                        </div>
                    </td>
                    <td valign="top" style="padding-left:20px">

                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div id="map"></div>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:60px" align="left">Дата доставки:</td>
                    <td align="left">
                        <input style="border:1px solid red;" type="text" name="date1" id="date1" value="{$order.DateEnd}" />
                        &nbsp;<input style="border:1px solid grey;" type="button" id="trigger1" value="изменить дату">
                        <input type="hidden" id="courier_address_id" value="{$order.courier_address_id}" name="courier_address_id"/>
                        <input type="hidden" id="delivery_cost" value="{$order.delivery_cost}" name="delivery_cost"/>
                    </td>
                </tr>

                {if $user.region eq 'region'}

                    {if $order.is_send_rect eq '0'}
                        <tr>
                            <td style="padding-left:60px"align="left">Отправить квитанцию:</td>
                            <td align="left">
                                <input type="checkbox" name="is_sending">
                            </td>
                        </tr>

                    {elseif $order.is_send_rect eq '1'}
                        <tr ><td align="left" colspan=2 bgcolor="white"></td></tr>
                        <tr ><td align="left" colspan=2 bgcolor="white"></td></tr>
                        <tr>
                            <td align="left" colspan="2"><strong>Квитанция отправлена ({$order.DateSendRect}):</strong>&nbsp;
                                <input type="checkbox" name="is_sending" id="is_sending"><label for="is_sending">Отправить повторно?:</label>&nbsp;</td>
                            <input type="hidden" name="is_sending_d" value="1"></td>
                        </tr>
                        <tr>
                            <td align="left"></td>
                            <td align="left">

                            </td>
                        </tr>

                    {/if}
                {/if}

                {if $user.region eq 'region'}

                    {if $order.is_send_post eq '1'}
                        <tr ><td align="left" colspan=2 bgcolor="white"></td></tr>
                        <tr ><td align="left" colspan=2 bgcolor="white"></td></tr>
                        <tr>
                            <td align="left" colspan="2">Идентификатор <strong>№{$order.pPostNumber}</strong> отправлен - {$order.DateSendPost}:</td>
                        </tr>
                        <input type="hidden" name="is_sending_post" value="1">
                        <input type="hidden" name="courier_address_id" id="courier_address_id" value="{$order.courier_address_id}">
                    {/if}
                    <tr>
                        <td style="padding-left:60px" align="left">Почт. идентификатор:</td>
                        <td align="left">
                            <input style="border:1px solid grey;width:190px;" maxlength="40" type="text" name="track" id="track" value="{$order.track}" />&nbsp;
                            Дата:&nbsp;<input style="border:1px solid grey;width:65px;" type="text" name="track_date" id="track_date" value="{$order.track_date}" />
                        </td>
                    </tr>
                {/if}
            </table>
            <table width="100%" border="0" cellpadding="5" cellspacing="0">
                <tr>
                    <td align="right">
                        {if $order.status eq '1'}
                            <input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
                        {elseif $order.status eq '2'}
                            <input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
                        {elseif $order.status eq '5'}
                            <input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
                        {elseif $order.status eq '6'}
                            <input id="submit_form" type="submit" name="save" value="Сохранить заказ" style="color:red;font-size:16pt;">
                        {elseif $order.status eq '3'}
                            <input id="submit_form" type="submit" name="save" value="Заказ доставлен" style="color:grey;font-size:16pt;"  disabled>
                        {elseif $order.status eq '0'}
                            <input id="submit_form" type="submit" name="save" value="Заказ отклонен" style="color:grey;font-size:16pt;" disabled>
                        {/if}
                        <p class="error_delivery">Вы не указали способ доставки</p>
                    </td>
                </tr>
            </table>

        </td>
    </tr>
    </form>
    <tr>
        <td colspan="2">


            <div style="margin-top:30px" id="tabs" class="flora">
                <ul class="ui-tabs-nav">
                    <li class="ui-tabs-selected"><a href="#tab-1"><span>Логистика и оплата</span></a></li>
                </ul>
            </div>

            <table class="form_add checkbox_logistic" width="100%" >
                <tr>
                    <td style="display: none"><input class="logistic allow" name="logistic" value="{$order.logistic_value}"></td>
                    <td style="padding:10px 0 10px 10px">
                        <div data-value="1" class="dostavka_item__checkbox {if in_array(1, $order.logistic)}checked{/if} {if $aAllows.LOGISTIC_SEND}allow{/if}">Отправлен</div>
                    </td>
                    <td style="padding:10px 0">
                        <div data-value="2" class="dostavka_item__checkbox {if in_array(2, $order.logistic)}checked{/if} {if $aAllows.LOGISTIC_PAY}allow{/if}">Оплачен</div>
                    </td>
                    <td style="padding:10px 0">
                        <div data-value="3" class="dostavka_item__checkbox {if in_array(3, $order.logistic)}checked{/if} {if $aAllows.LOGISTIC_MONEY}allow{/if}">Деньги получены</div>
                    </td>
                    <td style="padding:10px 0">
                        <div data-value="4" class="dostavka_item__checkbox {if in_array(4, $order.logistic)}checked{/if} {if $aAllows.LOGISTIC_REFOUND}allow{/if}">Возврат</div>
                    </td>
                    <td style="padding:10px 10px 10px 0">
                        <div data-value="5" class="dostavka_item__checkbox {if in_array(5, $order.logistic)}checked{/if} {if $aAllows.LOGISTIC_REFOUND_COMPLETE}allow{/if}">Возврат получен</div>
                    </td>
                </tr>
                <tr>
                    <td style="border:0" colspan="5">
                        <table width="100%">

                            <tr>
                                <td  style="padding:10px 0;border:0" width="50%">
                                    <table width="100%">
                                        <tr>
                                            <td style="border:0" width="170" align="right">Способ оплаты:</td>
                                            <td style="border:0">
                                                <span class="payment_type" style="font-weight: bold"">Наличными при получении</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="border:0">
                                    <table width="100%">
                                        <tr>
                                            <td style="border:0" width="170" align="right">Тип доставки:</td>
                                            <td style="border:0">
                                                <span class="payment_type" style="font-weight: bold">Курьером (b2c)</span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td  style="padding:10px 0;border:0" width="50%">
                                    <table width="100%">
                                        <tr>
                                            <td style="border:0" width="170" align="right">Дата оплаты</td>
                                            <td style="border:0">
                                                <input  disabled  id="payment_date" name="payment_date"  value="{$order.payment_date}" style="padding:6px" type="text" class="{if $aAllows.LOGISTIC_PAYMENT_DATE}allow{/if}">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:10px 0;border:0" >
                                    <table width="100%">
                                        <tr>
                                            <td style="border:0" width="170" align="right">Фактически получено</td>
                                            <td style="border:0">
                                                <input disabled name="TotalMoneyIncome" value="{$order.TotalMoneyIncome}" style="padding:6px" type="text" class="{if $aAllows.LOGISTIC_TOTALMONEYINCOME}allow{/if}">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:10px 0;border:0" >
                                    <table width="100%">
                                        <tr>
                                            <td style="border:0" width="170" align="right">Дата поступления денег</td>
                                            <td style="border:0">
                                                <input disabled id="DateMoneyIncome" value="{$order.DateMoneyIncome}" name="DateMoneyIncome" style="padding:6px" type="text" class="{if $aAllows.LOGISTIC_DATEMONEYINCOME}allow{/if}">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:10px 0;border:0" >
                                    <table width="100%">
                                        <tr>
                                            <td style="border:0" width="170" align="right">Расход на доставку (фактич)</td>
                                            <td style="border:0">
                                                <input disabled name="delivery_cost_fact" value="{$order.delivery_cost_fact}" style="padding:6px" type="text" class="{if $aAllows.LOGISTIC_DELIVERYCOST}allow{/if}">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="display: none">
                                <td><input  class="allow" type="hidden" name="order_id" value="{$order.id}"> </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <table width="100%">
                            <tr>
                                <td style="border:0" width="50%"></td>
                                <td style="border:0" width="50%">
                                    {if $aAllows.LOGISTIC}
                                        <table width="100%">
                                            <tr>
                                                <td style="border:0" width="170">&nbsp;</td>
                                                <td style="border:0">
                                                    <a style="color:white" href="" class="btn btn-warning edit">Редактировать</a>
                                                    <span style="padding:0 20px"></span>
                                                    <a style="color:white;display: none" href="" class="btn btn-primary save">Сохранить</a>
                                                </td>
                                            </tr>
                                        </table>
                                    {/if}
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


        </td>
    </tr>
    </td>
    <td width="20">&nbsp;</td>
    <!-- Ароматы , правый блок -->
    <td valign="top">
    </td>
    </tr>
    </table>
{/strip}

{literal}
    <script type="text/javascript">
        Calendar.setup({
            inputField     	: "date1",
            ifFormat       	: "%Y-%m-%d %H:%M:%S",
            timeFormat     	: "24",
            button         	: "trigger1",
            singleClick    	: true,
            step           	: 1,
            showsTime		: true
        });
    </script>
{/literal}

{literal}
    <script type="text/javascript">
        $(function(){
            if ($('#track_date').length>0){
                Calendar.setup({
                    inputField     	: "track_date",
                    ifFormat       	: "%Y-%m-%d",
                    timeFormat     	: "24",
                    button         	: "date2",
                    singleClick    	: true,
                    step           	: 1,
                    showsTime		: false
                });
            }

            if ($('#date_birthday_input').length>0){

                Calendar.setup({
                    inputField     	: "date_birthday_input",
                    ifFormat       	: "%Y-%m-%d",
                    timeFormat     	: "24",
                    showsTime		: false
                });
            }

            if ($('#DateMoneyIncome').length>0){

                Calendar.setup({
                    inputField     	: "DateMoneyIncome",
                    ifFormat       	: "%Y-%m-%d",
                    timeFormat     	: "24",
                    showsTime		: false
                });
            }

            if ($('#payment_date').length>0){
                Calendar.setup({
                    inputField     	: "payment_date",
                    ifFormat       	: "%Y-%m-%d",
                    timeFormat     	: "24",
                    showsTime		: false
                });
            }
        })
    </script>
{/literal}

{literal}
    <script>
        var yMap,
                ymapsDef = $.Deferred(),
                ymapsResolved = false,
        // глобальная переменная, переходит в состояние true, когда
        // мы инициализируем, первично, получение служб доставки по данному городу, через api
        // и при этом у нас при инициализации изменилась стоимость
                isDeliveryCostInited = false;

        /**
         * Инициализация яндекс карт для служб доставки
         */
        function initYmaps() {

            yMap = new ymaps.Map('map', {
                center: [55.753994, 37.622093],
                zoom: 8,
                controls: ['zoomControl', 'typeSelector']
            });

            ymapsDef.resolve();
        }

        ymaps.ready(initYmaps);

        /**
         * Функция заполнения служб доставки по указанному адресу пользователя
         * или же по уже выбранной службе, сначала заполняет все возможные варианты по адресу
         * дальше выбирает нужный, если таковой был выбран по оформлении заказа
         *
         * @param {Number} kladr_id - целочисленный идентификатор кладра
         * @param {String} city - название города
         * @param {Object} resp - объект XHR запроса на получение "логистической компании"
         */
        function fillCouriers(kladr_id, city, resp) {

            $.when(ymapsDef).then(function() {
                AddressSuggestions.getLogistic(kladr_id, city).then(function() {

                    var $wrapper = $('#couriers__wrapper'),
                            $logistic = $('#self_selected'),
                            arr = [];

                    if (resp.delivery_type != "самовывоз") {
                        $wrapper.find('.courier__row[data-courier="'+resp.id+'"]').find('.dostavka_item__checkbox').trigger('click');//addClass('checked');
                    } else {

                        arr.push("<span>Пользователь выбрал: <b>"+resp.courier_name+"</b></span>");
                        arr.push("<span>Адрес пункта самовывоза: <b>"+resp.delivery_address+"</b></span>");
                        arr.push("<span>Время доставки от "+resp.delivery_days+" дн.</span>");
                        arr.push("<span>Стоимость доставки от "+resp.delivery_cost+" р.</span>");
                        $logistic.html(arr.join(""));

                        $("#submit_form").removeAttr('disabled');

                        isDeliveryCostInited = true;
                    }

                    $("#courier_address_id").val(resp.id);
                });
            });
        }

        $.ajax({
            url: "/api/logistic_get/",
            data: {
                k: $("#courier_address_id").val(),
                t: $('input[name="payment_type"]').val()
            },
            success: function(resp) {

                // если ответ получен, то есть был выбран способ доставки
                if (resp.id != null && resp.id != 0) {

                    fillCouriers(resp.kladr_id_city, resp.name_city, resp);

                    // если же способ доставки не был выбран просто грузим по городу карту
                } else {

                    var addr = [],
                            result,
                            answer;

                    // заполнение адреса исходя из полей пользователя

                    //исключаем дублирование Москва Москва
                    if ($('#part_name').text() != "") {

                        fillCouriers($('#select_part').val(), resp.name_city, resp);
                    } else {

                        addr.push($('#region').val());
                        // заполнение адреса исходя из полей пользователя
                        if ($('#region').val() != $('#city').val()){
                            addr.push($('#city').val());
                        }
                        addr.push($('#step2_settlement').val());

                        addr = addr.join(" ");

                        answer = DadataApi.clean(addr);
                        answer.then(function(msg) {
                            if (typeof msg != 'undefined') {
                                result = AddressSuggestions.getCleanData(msg);
                                fillCouriers(result.kladr_id, addr, resp);
                            }
                        });
                    }
                }
            }
        });
    </script>
{/literal}