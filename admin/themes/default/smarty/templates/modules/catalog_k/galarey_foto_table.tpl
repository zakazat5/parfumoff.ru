{strip}

<form action="
		?main={$smarty.get.main}
		&worker={$smarty.get.module}
		&id_cat={$smarty.get.id_cat}
		&id_catalog_data={$smarty.get.id_catalog_data}
		&action=galarey_foto" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>Загрузка фото</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Выберите файл:</td>
		<td align="left" width="80%">
			<input type="file" name="img" style="width:100%" class="form_add">
		</td>
	</tr>
	
	<tr>
		<td align="left" ><span style="color:red;">*</span>&nbsp;Заголовок фотографии (TITLE):</td>
		<td align="left" >
			<input type="text" name="title" style="width:100%" class="form_add" value="{$img_title}">
		</td>
	</tr>
	
	<tr>
		<td align="left" >&nbsp;&nbsp;Описание фотографии (ALT):</td>
		<td align="left" >
			<input type="text" name="alt" style="width:100%" class="form_add" value="{$img_alt}">
		</td>
	</tr>
	
	<tr>
		<td align="left"  valign="top">&nbsp;&nbsp;Аннотация:</td>
		<td align="left" >
			<textarea name="anot" style="width:100%" rows=5></textarea>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Индекс сортировки:</td>
		<td align="left">
			<input type="text" name="sort" value="100" style="width:50px" value="{$img_sort}">
		</td>
	</tr>

	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="foto_add" value="Добавить">
	</td>
	</tr>
	</form>
</table>
</div>


<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_catalog_data={$smarty.get.id_catalog_data}
	&id_cat={$smarty.get.id_cat}
	&action=order" method="post" enctype="multipart/form-data" name="formMod">


<table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
<tr align="center" class="table_header" >
	<td height="22px">&nbsp;</td>
</tr>

<tr>
<td align="center">
	{section name=i loop=$item}
		<div style="float:left;height:120px;width:130px;border:1px solid #ccc; margin:5px; padding-top:5px; background-color:#fafafa;">
		<input type="checkbox" name="id[]" value="{$item[i].id}">
		<a href="?main={$smarty.get.main}
				   &module={$smarty.get.module}
				   &id_catalog_data={$smarty.get.id_catalog_data}
				   &id_foto={$item[i].id}
				   &id_cat={$smarty.get.id_cat}
				   &action=galarey_foto_edit">
		
		<img src="../images/uploads/catalog/{$smarty.get.id_catalog_data}/small/{$item[i].img}" width="80" border="1" align="top"
			onmouseover="return overlib('
				   <b>Заголовок</b>: {$item[i].title}<br>
				   <b>Описание</b>: {$item[i].alt}<br>
				   <b>Аннотация</b>: {$item[i].anot}<br>
				   <b>Сортировка:</b> {$item[i].sort}', CAPTION, 'Редактировать фото', LEFT);" 
			onmouseout="return nd();"
		>
		</a>
		</div>
	{/section}
</td>
</tr>

</table>

<!-- Подключение футера -->
{include file="table_footer.tpl"}
</form>
{/strip}