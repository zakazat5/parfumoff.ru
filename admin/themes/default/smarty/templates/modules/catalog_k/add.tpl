{include file="editor.tpl"}
{strip}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&action=add" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>Основные данные</span></a></li>
		<li class=""><a href="#tab-3"><span>Текстовые данные</span></a></li>
		<li class=""><a href="#tab-4"><span>Параметры публикации</span></a></li>
		<li class=""><a href="#tab-5"><span>Фото</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr >
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Раздел:</td>
		<td align="left" width="80%">
			<select name="sel_catalog" id="sel_catalog" style="width:100%">
				<option value="0">-- Выберите раздел --</option>
				{$sel_catalog}
			</select>
		</td>
	</tr>
	
	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Пол</td>
		<td align="left" width="80%">
			<select name="pol">
				<option value="M">Мужской</option>
				<option value="F">Женский</option>
			</select>&nbsp;
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Заголовок:</td>
		<td align="left">
			<input type="text" name="title" value="" style="width:100%" onkeyup="url_convert();" onchange="url_convert();">
		</td>
	</tr>
		
	<tr>
		<td align="left">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;ЧПУ URL:</td>
					<td align="right" style="border:0px;">
					<a href="#" 
					onmouseover="ddrivetip('Внимание!При ручном редактировании ЧПУ, не должно быть спецсимволов. Разрешается использовать в качестве разделителя только -', 200)" onmouseout="hideddrivetip()"
					>
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
					</td>
				</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="url" value="" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;TAG:</td>
		<td align="left">
			<input type="text" name="tag" value="{$tag}" style="width:100%">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Мета тэги
		</td>
	</tr>
	
	<tr>
		<td align="left">
		&nbsp;TITLE:
		<input type="checkbox" name="is_meta_title"
			onmouseover="return overlib('Ручное редактирование', LEFT);" 
			onmouseout="return nd();"
		>
		</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;TITLE_1:</td>
		<td align="left">
			<input type="text" name="meta_title_1" value="{$meta_title_1}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left" valign="top">
		&nbsp;DESCRIPTION:
		<input type="checkbox" name="is_meta_description"
			onmouseover="return overlib('Ручное редактирование', LEFT);" 
			onmouseout="return nd();"
		>
		</td>
		<td align="left">
			<input type="text" name="meta_description" value="{$meta_description}" style="width:100%" >
		</td>
	</tr>
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Полное описание:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="text" name="text" style="width:100%" rows=20></textarea>
		</td>
	</tr>
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Индекс сортировки:</td>
		<td align="left" width="80%">
			<input type="text" name="sort" value="100" style="width:50px">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Заблокировать:</td>
		<td align="left">
			<input type="checkbox" name="is_block">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Отображать на главной:</td>
		<td align="left">
			<input type="checkbox" name="is_main">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;В рассылку:</td>
		<td align="left">
			<input type="checkbox" name="is_subs" {$is_subs}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Распродажа:</td>
		<td align="left">
			<input type="checkbox" name="is_sale" {$is_sale}>
		</td>
	</tr>

</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-5">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;Превью:</td>
		<td align="left" width="80%">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" >&nbsp;&nbsp;Описание фотографии (ALT):</td>
		<td align="left" >
			<input type="text" name="alt" style="width:100%" value="{$img_alt}">
		</td>
	</tr>
</table>
</div>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="Сохранить и добавить еще">&nbsp;
		<input type="submit" name="save_list" value="Сохранить и перейти к списку">
	</td>
	</tr>
</table>

</form>
{/strip}