{strip}

<form action="
		?main={$smarty.get.main}
		&module={$smarty.get.module}
		&action=cat_add
		&id_cat={$smarty.get.id_cat}
		&id={$smarty.get.id}"
		method="post" enctype="multipart/form-data" name="form">

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	
	<tr>
		<td align="left" bgcolor="#eef1f7" colspan="2">
		&nbsp;&nbsp;Редактирование блока меню
		</td>
	</tr>
	
	<tr>
		<td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Название блока меню:</td>
		<td align="left" width="70%">
			<input type="text" name="title" value="{$cat.title}" style="width:100%" class="form_add">
		</td>
	</tr>

    <tr>
        <td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Сортировка:</td>
        <td align="left" width="70%">
            <input type="text" name="sort" value="{$cat.sort}" style="width:5%" class="form_add">
        </td>
    </tr>

    <tr>
        <td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Родительская категория:</td>
        <td align="left" width="70%">
            <select name="pid" style="width: 300px;">
                <option value="0">Без категории</option>
                {foreach from=$tree item=item}
                    <option {if $smarty.get.pid==$item.id}SELECTED="" {/if}value="{$item.id}">{$item.title}</option>
                {/foreach}
            </select>
        </td>
    </tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Заблокировать:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {if $cat.is_block}checked="checked" {/if}>
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2">
			<input type="submit" name="add" value="Добавить" style="width:100px">
		</td>
	</tr>
</table>
</form>
{/strip}