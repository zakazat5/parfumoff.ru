{strip}
<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&id_catalog_data={$smarty.get.id_catalog_data}
	&id_order={$smarty.get.id_order}
	&action=catalog_data_order_edit" method="post" enctype="multipart/form-data" id="form">

<table width="450" border="0" cellpadding="5" cellspacing="0" class="form_add">
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Основные параметры
		</td>
	</tr>
	
	<tr>
		<td align="left" width="50%">
		&nbsp;&nbsp;Тип:
		</td>
		<td align="left" width="50%">
			<select name="sel_type">
				{html_options options=$sel_type selected=$sel_type_active}
			</select>&nbsp;
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Объем (мл):</td>
		<td align="left">
			<input type="text" name="v" value="{$v}" style="width:150px;">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Артиклы
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-1:</td>
		<td align="left">
			<input type="text" name="article" value="{$article}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-2:</td>
		<td align="left">
			<input type="text" name="article2" value="{$article2}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-3:</td>
		<td align="left">
			<input type="text" name="article3" value="{$article3}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-4:</td>
		<td align="left">
			<input type="text" name="article4" value="{$article4}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-5:</td>
		<td align="left">
			<input type="text" name="article5" value="{$article5}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-6:</td>
		<td align="left">
			<input type="text" name="article6" value="{$article6}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-7:</td>
		<td align="left">
			<input type="text" name="article7" value="{$article7}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-8:</td>
		<td align="left">
			<input type="text" name="article8" value="{$article8}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-9:</td>
		<td align="left">
			<input type="text" name="article9" value="{$article9}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-10:</td>
		<td align="left">
			<input type="text" name="article10" value="{$article10}" style="width:100px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул-11:</td>
		<td align="left">
			<input type="text" name="article11" value="{$article11}" style="width:100px;">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Цены
		</td>
	</tr>

	<tr>
		<td align="left">&nbsp;&nbsp;Цена-1 ($):</td>
		<td align="left">
			<input type="text" name="price_usd1" value="{$price_usd1}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-2 ($):</td>
		<td align="left">
			<input type="text" name="price_usd2" value="{$price_usd2}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-3 ($):</td>
		<td align="left">
			<input type="text" name="price_usd3" value="{$price_usd3}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-4 ($):</td>
		<td align="left">
			<input type="text" name="price_usd4" value="{$price_usd4}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-5 ($):</td>
		<td align="left">
			<input type="text" name="price_usd5" value="{$price_usd5}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-6 ($):</td>
		<td align="left">
			<input type="text" name="price_usd6" value="{$price_usd6}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-7 ($):</td>
		<td align="left">
			<input type="text" name="price_usd7" value="{$price_usd7}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-8 ($):</td>
		<td align="left">
			<input type="text" name="price_usd8" value="{$price_usd8}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-9 ($):</td>
		<td align="left">
			<input type="text" name="price_usd9" value="{$price_usd9}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-10 ($):</td>
		<td align="left">
			<input type="text" name="price_usd10" value="{$price_usd10}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Цена-11 ($):</td>
		<td align="left">
			<input type="text" name="price_usd11" value="{$price_usd11}" style="width:50px;">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Цена (RUR):</td>
		<td align="left">
			{$price} руб.
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Склад:</td>
		<td align="left">
			Склад №{$sklad}
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Параметры публикации
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Распродажа:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Цена:</td>
		<td align="left">
			<input type="text" name="price" value="{$price}" style="width:50px;">
		</td>
	</tr>
	<tr>
		<td align="left">&nbsp;&nbsp;Старая цена:</td>
		<td align="left">
			<input type="text" name="price_old" value="{$price_old}" style="width:50px;">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Сумма комисии:</td>
		<td align="left">
			<input type="text" name="price_com" value="{$price_com}" style="width:50px;">
		</td>
	</tr>
	
</table>

<table width="450" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="Сохранить и добавить еще">&nbsp;
		<input type="submit" name="save_list" value="Сохранить и перейти к списку">
	</td>
	</tr>
</table>

</form>
{/strip}