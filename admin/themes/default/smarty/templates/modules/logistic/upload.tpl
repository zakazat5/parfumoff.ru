{strip}
    <form action="?main={$smarty.get.main}&worker={$smarty.get.module}&action=csv" method="post" enctype="multipart/form-data" id="form">

        <div id="tabs" class="flora">
            <ul class="ui-tabs-nav">
                <li class="ui-tabs-selected"><a href="#tab-1"><span>Основные данные</span></a></li>
            </ul>
        </div>

        <div style="display: block;" class="ui-tabs-panel" id="tab-1">
            <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
                <tr>
                    <td align="left" width="20%">&nbsp;&nbsp;Выберите файл CSV:</td>
                    <td align="left" width="80%">
                        <input type="file" name="file" style="width:100%;">
                    </td>
                </tr>

            </table>
        </div>

        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr class="table_bg_action">
                <td align="left" >
                    <input type="submit" name="save" value="Загрузить" style="width:100px">&nbsp;&nbsp;
                    <a href="?main={$smarty.get.main}&module={$smarty.get.module}&action=download">Выгрузить текущее состояние</a>
                </td>
            </tr>
        </table>

    </form>
    <br/>
    <br/>
    <div class="address__get">
        <label for="address">Введите адрес, для получения Кладр айди</label>
        <input id="address" name="address" type="text" size="100"/>
        <div id="parse__result">

        </div>
    </div>
    {literal}
        <script type="text/javascript">
            $("#address").suggestions({
                serviceUrl: "https://dadata.ru/api/v1/suggest/address",
                token: DadataApi.TOKEN,
                /* Вызывается, когда пользователь выбирает одну из подсказок */
                onSelect: function(suggestion) {
                    console.log(suggestion);
                    var $res = $('#parse__result');

                    $res.html('');
                    $res.append("<b>Кладр Айди:</b> <span>"+suggestion.data.kladr_id+"</span><br/>");
                }
            });
        </script>
    {/literal}
{/strip}