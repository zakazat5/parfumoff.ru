{include file="editor.tpl"}


<script src="js/moderation_k.js?v1"></script>

{strip}
    Фильтр по складу:
    <select id="select_sklad">
        <option value="0">Все склады</option>
        {section name=foo start=1 loop=16 step=1}
            <option {if $user_sel_sklad==$smarty.section.foo.index}SELECTED="" {/if}value="{$smarty.section.foo.index}">Склад {$smarty.section.foo.index}</option>
        {/section}

    </select>
    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
        <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=order" method="post" enctype="multipart/form-data" name="formMod">

            <!-- Заголовок таблицы данных -->
            <tr align="center" class="table_header">
                <td width="3%" align="center" height="22"></td>
                <td width="3%">
                    <div align="center"><b>Артикул</b></div>
                </td>
                <td width="30%">
                    <div align="center"><b>Наименование</b></div>
                </td>
                <td width="" align="left"><b>&nbsp;Комментарий</b></td>
                <td width="7%" align="left"><b>&nbsp;Склад</b></td>
                <td width="7%" align="left"><b>&nbsp;Время обновления</b></td>
                <td width="5%" align="center"><b>Проблема</b></td>
                <td width="3%" align="center"><b>Черн.список</b></td>
            </tr>
            <!-- Заголовок таблицы данных -->
            {if $item}
                <!-- Вывод списка -->
                {section name=i loop=$item}
                    <tr id="moder_{$item[i].id}" style="background-color:{$item[i].bgcolor};">
                        <td align="center" height="25px"><input type="checkbox" name="id[]"
                                                                value="{$item[i].id}"></nobr></td>
                        <td align="center">{$item[i].articul}</td>
                        <td align="left"><a href="?main={$smarty.get.main}
					   &module={$smarty.get.module}
					   &action=add
					   &id_cat={$smarty.get.id_cat}
					   &id={$item[i].id}">{$item[i].name}</a>
                        </td>
                        <td align="left">
                            &nbsp;<a href="?main={$smarty.get.main}
					   &module={$smarty.get.module}
					   &action=add
					   &id_cat={$smarty.get.id_cat}
					   &id={$item[i].id}">{if $item[i].comment}{$item[i].comment}{else}Нет комментария{/if}</a>

                        </td>
                        <td>
                            {$item[i].sklad}
                        </td>
                        <td>
                            {$item[i].timestamp}
                        </td>
                        <td align="center">
                            <a data-id="{$item[i].id}" class="problem" href="">
                                <img src="images/problem.png" width="16" height="16" border="0"></a>
                        </td>
                        <td align="center">
                            <a data-id="{$item[i].id}" class="blacklist" href=""><img src="images/blacklist.png" width="14" height="14" border="0"></a>
                        </td>

                        {*
                        <td align="center"><a onclick="return confirm('Удалить из модерации?')"
                                              href="?main={$smarty.get.main}
					   &module={$smarty.get.module}
					   &action=delete
					   &id_cat={$smarty.get.id_cat}
					   &id={$item[i].id}"><img src="images/delete.png" width="14" height="14" border="0"></a></td>
                        *}
                    </tr>
                {/section}
            {else}
                <tr>
                    <td colspan="3"><h3>Товаров на модерацию не найдено.</h3></td>
                </tr>
            {/if}
            <!-- ------------ -->
    </table>
    <!-- Подключение футера -->
    {include file="table_footer.tpl"}
    </form>
{/strip}


{include file="modules/catalog_moder/modal_blacklist.tpl"}

{literal}
    <script>
        $(function () {
            $('#select_sklad').change(function(){
                var url = updateURLParameter(window.location.href,'sel_sklad',$(this).val());
                window.location.href = url
            })
        })


        function updateURLParameter(url, param, paramVal){
            var newAdditionalURL = "";
            var tempArray = url.split("?");
            var baseURL = tempArray[0];
            var additionalURL = tempArray[1];
            var temp = "";
            if (additionalURL) {
                tempArray = additionalURL.split("&");
                for (i=0; i<tempArray.length; i++){
                    if(tempArray[i].split('=')[0] != param){
                        newAdditionalURL += temp + tempArray[i];
                        temp = "&";
                    }
                }
            }

            var rows_txt = temp + "" + param + "=" + paramVal;
            return baseURL + "?" + newAdditionalURL + rows_txt;
        }
    </script>

    {/literal}