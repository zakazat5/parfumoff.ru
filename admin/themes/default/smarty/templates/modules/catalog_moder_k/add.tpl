{include file="editor.tpl"}


<script src="js/moderation_k.js?v1"></script>

{literal}
    <style>
        .fs14 {
            font-size: 14px !important;
        }

        .fs15 {
            font-size: 15px !important;
        }

        .form-category .text {
            font-size: 15px !important;
        }

        .form-category .text ul {
            padding: 0px;
        }
    </style>
{/literal}


<h2>Добавление косметики</h2>

{strip}
    <form id="moder_form" action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&action=add" method="post" enctype="multipart/form-data" name="form">

    <div id="tabs" class="flora">
        <ul class="ui-tabs-nav">
            <li class="ui-tabs-selected"><a href="#tab-1"><span>Основные данные</span></a></li>
            <li class=""><a href="#tab-3"><span>Текстовые данные</span></a></li>
            <li class=""><a href="#tab-4"><span>Параметры публикации</span></a></li>
            <!--<li class=""><a href="#tab-5"><span>Фото</span></a></li>-->
        </ul>
    </div>

    <div style="display: block;" class="ui-tabs-panel" id="tab-1">
    <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
    <tr>
        <td align="left" width="20%">&nbsp;Информация о модерируем товаре:</td>
        <td align="left" width="80%">
            <table style="width:600px;">
                <tr>
                    <td class="fs15" width="30%">Наименование</td>
                    <td class="fs15">{$item.name}</td>
                </tr>
                <tr>
                    <td class="fs15">Артикул на складе № {$item.sklad}</td>
                    <td class="fs15">{$item.articul}</td>
                </tr>
                <tr>
                    <td class="fs15">Цена</td>
                    <td class="fs15">{$item.price}</td>
                </tr>
                <tr>
                    <td>Комментарий</td>
                    <td>{$item.comment}</td>
                </tr>
                <tr style="display: none">
                    <td><input name="id_moder" value="{$item.id}"></td>
                    <td></td>
                </tr>
            </table>

        </td>
    </tr>
    <tr>
        <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Тип товара:</td>
        <td align="left" width="80%">
            <select name="id_cat" id="id_cat" class="fs14" style="width:200px;">

                <option {if $item.id_cat==82}SELECTED{/if} value="82">Парфюмерия</option>

                <option {if $item.id_cat==96}SELECTED{/if} value="96">Косметика</option>

            </select>&nbsp;

            <select name="id_category" id="id_category" class="fs14"
                    style="width:200px;{if $item.id_cat!=96}display: none{/if}">
                <option value="0">Выберите категорию</option>
                {foreach from=$aCat item=category}
                    <option value="{$category.id}">{$category.title}</option>
                    {if $category.childrens}
                        {foreach from=$category.childrens item=category}
                            <option value="{$category.id}">--{$category.title}</option>
                            {if $category.childrens}
                                {foreach from=$category.childrens item=category}
                                    <option value="{$category.id}">----{$category.title}</option>
                                {/foreach}
                            {/if}
                        {/foreach}
                    {/if}
                {/foreach}
            </select>
            <input style="display: none" id="category_opt" name="category_opt">
        </td>
    </tr>

    <tr>
        <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Бренд</td>
        <td align="left" width="80%">
            <select class="fs14" name="id_brand" id="id_brand" style="width:200px;">
                <option value="0">-- Выберите бренд --</option>
                {$sel_catalog}
            </select>&nbsp;
            <a id="addBrand" style="" href="">Добавить новый</a>
        </td>
    </tr>
    <tr>
        <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Товар:</td>
        <td align="left" width="80%">
            <select name="id_product" id="id_product" class="fs14" style="width:200px;">
                <option value="0">Новый товар</option>
            </select>&nbsp;
        </td>
    </tr>

    <tr>
        <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Пол</td>
        <td align="left" width="80%">
            <select class="fs14" name="pol">
                <option value="F">Женский</option>
                <option value="M">Мужской</option>

            </select>&nbsp;
        </td>
    </tr>

    <tr>
        <td align="left"><span style="color:red;">*</span>&nbsp;Название:</td>
        <td align="left">
            <input data-required type="text" name="title" value="{$item.name}" style="width:100%"
                   onkeyup="url_convert();"
                   onchange="url_convert();">
        </td>
    </tr>

    <tr>
        <td align="left">
            <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;ЧПУ URL:</td>
                    <td align="right" style="border:0px;">
                        <a href="#"
                           onmouseover="ddrivetip('Внимание!При ручном редактировании ЧПУ, не должно быть спецсимволов. Разрешается использовать в качестве разделителя только -', 200)"
                           onmouseout="hideddrivetip()"
                                >
                            <img src="images/warnings.png" width="16" height="16" border="0"></a>
                    </td>
                </tr>
            </table>
        </td>
        <td align="left">
            <input type="text" name="url" value="" style="width:100%;">
        </td>
    </tr>

    <tr>
        <td align="left">&nbsp;&nbsp;TAG:</td>
        <td align="left">
            <input type="text" name="tag" value="{$tag}" style="width:100%">
        </td>
    </tr>

    <tr class="table_bg">
        <td align="left" colspan="2">
            &nbsp;&nbsp;Мета тэги
        </td>
    </tr>

    <tr>
        <td align="left">
            &nbsp;TITLE:
            <input type="checkbox" name="is_meta_title"
                   onmouseover="return overlib('Ручное редактирование', LEFT);"
                   onmouseout="return nd();"
                    >
        </td>
        <td align="left">
            <input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
        </td>
    </tr>

    <tr>
        <td align="left">&nbsp;TITLE_1:</td>
        <td align="left">
            <input type="text" name="meta_title_1" value="{$meta_title_1}" style="width:100%">
        </td>
    </tr>

    <tr>
        <td align="left" valign="top">
            &nbsp;DESCRIPTION:
            <input type="checkbox" name="is_meta_description"
                   onmouseover="return overlib('Ручное редактирование', LEFT);"
                   onmouseout="return nd();"
                    >
        </td>
        <td align="left">
            <input type="text" name="meta_description" value="{$meta_description}" style="width:100%">
        </td>
    </tr>
    <tr class="table_bg">
        <td align="left" style="font-size: 18px;height: 20px;line-height: 10px;" colspan="2">
            &nbsp;&nbsp;Модификации [ <b>{$item.name}</b> ]
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div id="modifications">
                <table style="width: 100%;max-width: 920px;">
                    <tr class="not_remove">
                        <td style="display: none"><input name="sklad_id" class="sklad_id" value="{$item.sklad}"></td>
                        <td class="articul">Артикул товара</td>
                        <td class="article">Артикул на складе {$item.sklad}</td>
                        <td class="price">Цена</td>
                        <td class="type">Тип</td>
                        <td class="v">Объем</td>
                        <td class="weight">Вес (г.)</td>
                        <td class="color">Цвет</td>
                        <td class="v">Действия</td>
                    </tr>
                    <tr class="not_remove new">
                        <td class="articul"><input disabled value="Новая модификация"></td>
                        <td class="article"><input name="article" value="{$item.articul}"></td>
                        <td class="price_usd"><input name="price_usd" value="{$item.price}"></td>
                        <td class="type">
                            <select name="type">
                                <option value="0">Без типа</option>
                                {foreach from=$aroma_type key=key item=option}
                                    <option value="{$key}">{$option}</option>
                                {/foreach}
                            </select>
                        </td>
                        <td class="v"><input name="v"></td>
                        <td class="weight"><input name="weight"></td>
                        <td class="color"><input name="color"></td>
                        <td class="actions"></td>
                    </tr>
                </table>

                <tr id="mod_item_template" style="display: none">
                    <td style="display: none">
                        <input disabled name="modification_id" class="modification_id">
                    </td>
                    <td class="articul"><input disabled name="articul"></td>
                    <td class="article"><input disabled name="article"></td>
                    <td class="price_usd"><input disabled name="price_usd"></td>
                    <td class="type">
                        <select disabled name="type">
                            <option value="0">Без типа</option>
                            {foreach from=$aroma_type key=key item=type}
                                <option value="{$key}">
                                    {$type}
                                </option>
                            {/foreach}
                        </select>
                    </td>
                    <td class="v"><input disabled name="v"></td>
                    <td class="weight"><input disabled name="weight"></td>
                    <td class="color"><input disabled name="color"></td>
                    <td class="actions">
                        <a href="" class="bindMod">Привязать</a>
                        <a href="" style="display: none" class="unbindMod">Отвязать</a>
                    </td>
                </tr>
            </div>


        </td>
    </tr>


    </table>
    </div>

    <div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr class="table_bg">
                <td align="left" colspan="2">
                    &nbsp;&nbsp;Полное описание:
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <textarea id="text" name="text" style="width:100%" rows=20></textarea>
                </td>
            </tr>

        </table>
    </div>

    <div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Индекс сортировки:</td>
                <td align="left" width="80%">
                    <input type="text" name="sort" value="100" style="width:50px">
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;Заблокировать:</td>
                <td align="left">
                    <input type="checkbox" name="is_block">
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;Отображать на главной:</td>
                <td align="left">
                    <input type="checkbox" name="is_main">
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;В рассылку:</td>
                <td align="left">
                    <input type="checkbox" name="is_subs" {$is_subs}>
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;Распродажа:</td>
                <td align="left">
                    <input type="checkbox" name="is_sale" {$is_sale}>
                </td>
            </tr>

        </table>
    </div>
    <!-- Load widget code -->


    <div class="ui-tabs-panel ui-tabs-hide" id="tab-5">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr>
                <td align="left" width="20%">&nbsp;&nbsp;Превью:</td>
                <td align="left" width="80%">
                    <input type="text" name="image_path" style="width:100%;display: none">
                    <a href="" id="upload_image">Загрузить</a><br>
                    <a href=""
                       onclick="return launchEditor('image1', 'http://' + document.location.host + $('#image1').attr('src') )">Редактировать</a><br>

                    <img id="image1" style="width:230px;display: none" src=""/>

                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;Описание фотографии (ALT):</td>
                <td align="left">
                    <input type="text" name="alt" style="width:100%" value="{$img_alt}">
                </td>
            </tr>
            <tr class="table_bg">
                <td align="left" colspan="2">
                    &nbsp;&nbsp;Искать в google:
                </td>
            </tr>
            <tr>
                <td align="left" width="10%">&nbsp;&nbsp;Запрос:</td>
                <td align="left" width="90%">
                    <input type="text" id="qw" style="width:100%" class="form_add" value="{$item.name}">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <input type="button" name="get_foto" id="get_foto" value="Go">
                </td>
            </tr>
            <tr>
                <td align="center" id="out_foto" colspan="2">
                    <div id="msg"></div>
                </td>
            </tr>
        </table>
    </div>

    <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
        <tr class="table_bg_action">
            <td align="left" colspan="2">
                <input type="button" id="save" name="save" value="Сохранить и загрузить фото">&nbsp;
                <!--<input type="submit" name="save_add" value="Сохранить и добавить еще">&nbsp;-->
                <input type="button" id="save_list" name="save_list" value="Сохранить и перейти к списку">
                <a href="#" data-type="blacklist"  class="addBlacklist" style="margin-left: 5px;color:#CF2A2A">В черный список</a>
                <a href="#" data-type="problem" class="addBlacklist" style="margin-left: 15px;color:#CF2A2A">В проблемный список</a>
            </td>
        </tr>
    </table>

    </form>
{/strip}
{literal}

{/literal}


{include file="modules/catalog_moder_k/modal_brand.tpl"}
{include file="modules/catalog_moder_k/modal_blacklist.tpl"}


<div id="form-category" style="display:none;" title="Добавление фильтра для товара">
    <form action="" class="form-category">
        <div class="text">

        </div>
    </form>
    <input type="button" value="Сохранить" onclick="saveCategoryOpt()"/>&nbsp;&nbsp;
    <input type="button" value="Отмена" onclick="closeFormCategory()"/>
</div>


<div id="form-check" style="display:none;" title="Добавление фильтра для товара">
    <form action="" class="form-check">
        <h3>Вы промодерировали товар {$item.articul} {$item.name}, склад №{$item.sklad} </h3>
        Проверьте правильность модерации:<br><br>

        <div class="text">

        </div>
    </form>
    <input type="button" value="Все верно - продолжить!" onclick="save('save')"/>&nbsp;&nbsp;
    <input type="button" value="Остановиться и исправить!" onclick="tb_remove();"/>
</div>


</div>


{literal}
    <style>
        #modifications input {
            width: 100%;
        }

        #modifications select {
            width: 100%;
        }
    </style>
{/literal}