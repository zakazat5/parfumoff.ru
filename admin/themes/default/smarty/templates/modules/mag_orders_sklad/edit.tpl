{strip}
{literal}

<style type="text/css">

	.cat_item {
		width:80px;
		float:left;
		height:150px;
	}
	
	.up {cursor:pointer;}
	.dn{cursor:pointer;}
	
	.mod_table td{background:#fff;}

</style>

<script type="text/javascript">
$(document).ready(function(){
	
});
</script>
{/literal}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=edit
	&id_order={$smarty.get.id_order}
	&date={$smarty.get.date}
	&folder={$smarty.get.folder}
	&status={$smarty.get.status}" method="post" enctype="multipart/form-data" id="form_add_reg">

<table width="100%" border="0" cellpadding="0" cellspacing="0" >
<tr >
<td width="450" valign="top">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>Корзина</span></a></li>
	</ul>
</div>
<div style="display: block;" class="ui-tabs-panel" id="tab-1">


	<table width="100%" border="0" cellpadding="5" cellspacing="2" class="form_add" id="table_add_orders" >
		<tr class="table_bg">
			<td align="center" width="25" ></td>
			<td align="left" >Название</td>
			<td align="center" width="60">Цена</td>
			<td align="left" width="38">Кол-во</td>
			<td align="left" width="60">Стоимость</td>
		</tr>
		
		{foreach from=$c_order_sum item=order_sum}
			<tr class="tr_item_orders_clear" id="tr_item_orders_{$order_sum.id_catalog_data_order}">
				<td>
					<input type="button" name="{$order_sum.id_catalog_data_order}" alt="{$order_sum.price}" class="delete_list" value="X" style="width:20px;color:red;font-weight: bold;border:0;background:none;">
				</td>
				<td align="left" >
					<input class="id_orders" type="hidden" name="id_orders[]" value="{$order_sum.id_catalog_data_order}">
					{$order_sum.title}
				</td>
				<td align="center" >
					<span id="price_orders_{$order_sum.id_catalog_data_order}">{$order_sum.sum}</span>
				</td>
				<td align="left" >
					<input style="width:25px;padding:1px;float:left;height:18px;" type="text" id="kol_orders_{$order_sum.id_catalog_data_order}" name="kol_orders_{$order_sum.id_catalog_data_order}" value="{$order_sum.kol}"  onkeydown="return false;">
					<a class="up" alt="{$order_sum.id_catalog_data_order}"><img src="images/nav/up.gif" ></a><br /><a alt="{$order_sum.id_catalog_data_order}" class="dn"><img src="images/nav/dn.gif" ></a>
				</td>
				<td align="left" >
					<span id="price_orders_sum_{$order_sum.id_catalog_data_order}">
					{$order_sum.price}
					</span>
				</td>
			</tr>
		{/foreach}
		
		
	</table>
	
	
	<table width="100%" border="0" cellpadding="5" cellspacing="2" >
		
		<tr >
			<td align="center" width="25" ></td>
			<td align="right" >Сумма заказа:</td>
			<td align="left" width="60" style=""><span id="big_sum">{$order.Sum}</span> руб.</td>
		</tr>
		<tr >
			<td align="center" width="25" ></td>
			<td align="right" style="color:blue;" >Скидка (раз):</td>
			<td align="left" width="60" style="color:blue;"><span id="big_sum_percent">{$order.dPercent}</span> %</td>
		</tr>
		<tr >
			<td align="center" width="25" ></td>
			<td align="right" ><strong>Всего:</strong></td>
			<td align="left" width="60" ><strong><span id="big_sum_all">{$order.dSum}</span></strong> руб.</td>
		</tr>
		
	</table>
	
</div>
<br />

<table width="100%" border="0" cellpadding="3" cellspacing="1" class="form_add"  >
	<tr >
		<td align="left" width="30%">
			Комментарий к заказу:
		</td>
		<td width="70%">
			<textarea disabled name="br_comment" style="width:100%" rows=2>{$order.pComment}</textarea>
		</td>
	</tr>

	<tr>
		<td align="left">Комментарий склада:</td>
		<td align="left">
			<textarea name="br_comment_sklad" style="width:100%" rows=2>{$order.pCommentSklad}</textarea>
		</td>
	</tr>
	
</table>

<br />

<table width="100%" border="0" cellpadding="5" cellspacing="0">
	<tr>
	<td align="right">
	{if $order.status eq '5' OR $order.status eq '6'}
		<input id="submit_form" type="submit" name="order_save" value="Изменить комментарий" style="color:red;font-size:16pt;">
	{else}
		<input id="submit_form" type="submit" name="order_delete" value="Отклонить заказ" style="color:red;font-size:16pt;">
		<input id="submit_form" type="submit" name="order_incomplete" value="Неполный заказ" style="color:orange;font-size:16pt;">
	{/if}
	</td>
	</tr>
</table>

</form>

</td>

<td width="200">&nbsp;</td>

</tr>
</table>


{/strip}