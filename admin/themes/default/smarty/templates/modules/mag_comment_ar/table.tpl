{literal}

<script type="text/javascript">

$(document).ready(function(){

	$("#grid_table").flexigrid({
		url: '{/literal}?ajax={$smarty.get.module}&action=mod{literal}',
		dataType: 'json',
		colModel : [
		{display: 'Дата', 		name : 'date_add', width : 100, 	sortable : true, 	align: 'center'},
		{display: 'Аромат',		name : 'catalog_title', 	width : 200, 	sortable : false, 	align: 'left'},
		{display: 'ТИП', 		name : 'type', 		width : 100, 	sortable : true, 	align: 'left'},
		{display: 'COMMENT', 	name : 'text', 		width : 'auto', sortable : false, 	align: 'left'},
		],
		buttons : [
		{name: 'Выделить все', bclass: 'check', onpress : action},{separator: true},
		{name: 'Снять выделение', bclass: 'uncheck', onpress : action},{separator: true},{separator: true},
		{name: 'Удалить', bclass: 'delete', onpress : action},{separator: true},
		],
		searchitems : [
		{display: 'Название аромата', name : 'catalog_title', isdefault: true}
		],
		sortname: "date_add",
		sortorder: "desc",
		usepager: true,
		showTableToggleBtn: true,
		width: 'auto',
		height: 'auto',
		useRp: true,
		rpOptions: [20,40],
		rp: 20,
		nowrap: false,
		pagestat: 'Показано с {from} по {to} из {total} ',
		procmsg: 'Обработка, пожалуйста, подождите ...',
		nomsg: 'Нет элементов',
	});
});

function action(com,grid){
	if (com=='Удалить'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=delete{literal}",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com=='Выделить все'){
		$('.bDiv tbody tr',grid).addClass('trSelected');

	}else if (com=='Снять выделение'){
		$('.bDiv tbody tr',grid).removeClass('trSelected');
	}
}
</script>
{/literal}


<table id="grid_table" class="flexigrid" style="display:none"></table>