{literal}

<script type="text/javascript">

$(document).ready(function(){

	$("#client_table").flexigrid({
		url: '{/literal}?ajax={$smarty.get.module}&action=mod{literal}',
		dataType: 'json',
		colModel : [
		{display: 'Дата регистрации', name : 'date_register', width : 100, sortable : true, align: 'left'},
		{display: 'ID', name : 'id', width : 40, sortable : false, align: 'center' },
		{display: 'EMAIL', name : 'email', width : 200, sortable : false, align: 'left'},
		{display: 'ФИО', name : 'fio', width : 200, sortable : false, align: 'left'},
		{display: 'Накопленная сумма', name : 'd_sum_all', width : 100, sortable : true, align: 'left'},
		{display: 'Скидка (%)', name : 'd_percent', width : 100, sortable : true, align: 'left'},
		{display: 'Экономия', name : 'd_sum_eco', width : 100, sortable : true, align: 'left'},
		{display: 'Избранное', name : 'note', width : 60, sortable : false, align: 'center'},
		{display: 'Дата посл. визита', name : 'date_last_visit', width : 100, sortable : false, align: 'left'},
		{display: 'A', name : 'is_block', width : 20, sortable : false, align: 'center'},
		],
		buttons : [
		{name: 'Выделить все', bclass: 'check', onpress : action},{separator: true},
		{name: 'Снять выделение', bclass: 'uncheck', onpress : action},{separator: true},{separator: true},
		{name: 'Активировать', bclass: 'active', onpress : action},{separator: true},
		{name: 'Заблокировать', bclass: 'deactive', onpress : action},{separator: true},
		{name: 'Удалить', bclass: 'delete', onpress : action},{separator: true},
		],
		searchitems : [
		{display: 'ID', name : 'id', isdefault: true},
		{display: 'EMAIL', name : 'email'},
		],
		sortname: "date_register",
		sortorder: "desc",
		usepager: true,
		showTableToggleBtn: true,
		width: 'auto',
		height: 'auto',
		useRp: true,
		rpOptions: [15,20,25,40],
		rp: 15,
		pagestat: 'Показано с {from} по {to} из {total} клиентов',
		procmsg: 'Обработка, пожалуйста, подождите ...',
		nomsg: 'Нет элементов',
	});
});

function action(com,grid){
	if (com=='Активировать'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=active{literal}",
				data: "items="+itemlist,
				success: function(data){
					$("#client_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com=='Заблокировать'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=deactive{literal}",
				data: "items="+itemlist,
				success: function(data){
					$("#client_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com=='Удалить'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=delete{literal}",
				data: "items="+itemlist,
				success: function(data){
					$("#client_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com=='Выделить все'){
		$('.bDiv tbody tr',grid).addClass('trSelected');

	}else if (com=='Снять выделение'){
		$('.bDiv tbody tr',grid).removeClass('trSelected');
	}
}

</script>
{/literal}

<table id="client_table" class="flexigrid" style="display:none"></table>