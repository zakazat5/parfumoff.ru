{include file="calendar.tpl"}
{strip}

    {literal}
        <script type="text/javascript">
            $(document).ready(function(){


            // проверка мыла
            $("#is_email").click( function () {

                var data = $("#email").val();
                $("#is_email").attr('value', 'Ожидаем ... ');

                $.post("{/literal}?ajax={$smarty.get.module}&action=is_email{literal}", {
                    email: data
                }, function (xml) {
                    $("message", xml).each(function (id) {
                        message = $("message", xml).get(id);

                        if ($("msg", message).text() == 'err') {
                            $("#is_email").attr('value', 'Email - занят');
                            $("#is_email").css('color', 'red');
                        } else {
                            $("#is_email").attr('value', 'Email - свободен');
                            $("#is_email").css('color', 'blue');
                        }

                    });
                });
            });




        // Проверка перед отправкой
        $(".submit_form").on("click", function () {

            //check filds
            var error = false;
            var error_text = '';

            if ($('[name="fio"]').val() == '') {
                error = true;
                error_text += 'ФИО не заполнено\n';
            }

            if ($('[name="phone"]').val() == '') {
                error = true;
                error_text += 'Поле телефон не заполнено\n';
            }



            if ($('[name=street]').val() == '') {
                error = true;
                error_text += 'Адрес не заполнен\n';
            }

            if (error) {
                alert(error_text);
                return false;
            }

            /*


            var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
            answer = DadataApi.clean(addr);
            request.done(function (answer) {
                var postal_code = msg.data[0][0].postal_code
                $('[name=regs_postcode]').val(postal_code)
                $('[name=address_data]').val(JSON.stringify(msg.data[0][0]));
                $('form').submit();
            });
            return false;
            */


        });

        });
    </script>
{/literal}
    <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=edit
	&id_client={$smarty.get.id_client}" method="post" enctype="multipart/form-data" name="form" autocomplete="off">

    <div id="tabs" class="flora">
        <ul class="ui-tabs-nav">
            <li class="ui-tabs-selected"><a href="#tab-1"><span>Основные данные</span></a></li>
            <li class=""><a href="#tab-3"><span>Параметры публикации</span></a></li>
        </ul>
    </div>

    <div style="display: block;" class="ui-tabs-panel" id="tab-1">
        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="form_add">
            <tr>
                <td align="left">E-Mail:&nbsp;</td>
                <td align="left">
                    <input type="text" name="email" id="email" value="{$data.email}" style="width:200px">&nbsp;<input
                            type="button" name="is_email" id="is_email" value="Проверить">
                    <input type="hidden" name="xemail" value="{$data.email}">
                </td>
            </tr>

            <tr>
                <td align="left">Пароль:</td>
                <td align="left">
                    <input type="password" name="passw" value=""
                           style="width:200px;color:red;font-weight:bold;">
                </td>
            </tr>

            <tr>
                <td align="left">Повторить пароль:</td>
                <td align="left">
                    <input type="password" name="re_passw" value=""
                           style="width:200px;color:red;font-weight:bold;">
                </td>
            </tr>

            <tr class="table_bg">
                <td align="left" colspan="2">
                    О себе
                </td>
            </tr>

            <tr>
                <td align="left">ФИО:</td>
                <td align="left">
                    <span>{$data.fio}</span>
                    {* <input type="text" name="fio" value="{$data.fio}" style="width:200px"> *}
                </td>
            </tr>
            <tr>
                <td align="left">Телефон:</td>
                <td align="left">
                    <span>{$data.phone}</span>
                    {*<input type="text" name="phone" value="{$data.phone}" style="width:200px">*}
               </td>
           </tr>

           <tr>
               <td align="left" width="20%">Пол</td>
               <td align="left" width="80%">
                   <select name="pol" style="width:200px">
                       <option value="M" {if $data.pol eq 'M'}selected{/if}>Мужской</option>
                       <option value="F" {if $data.pol eq 'F'}selected{/if}>Женский</option>
                   </select>&nbsp;
               </td>
           </tr>
           <tr>
               <td align="left">День рождения:</td>
               <td align="left">
                   <input type="text" name="date1" id="date1" value="{$data.date_birthday}"/>
                   &nbsp;<input type="button" id="trigger1" value=" ... ">
               </td>
           </tr>

           <tr class="table_bg">
               <td align="left" colspan="2">
                   Адресат
               </td>
           </tr>

           {* include file="address.tpl" *}

        </table>

    </div>

    <div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">


            <tr>
                <td align="left" width="20%">&nbsp;&nbsp;Заблокировать:</td>
                <td align="left" width="80%">
                    <input type="checkbox" name="is_block" {if $data.is_block eq '1'}checked{/if}>
                </td>
            </tr>

            <tr>
                <td align="left" width="20%">&nbsp;&nbsp;Участие в рассылке:</td>
                <td align="left" width="80%">
                    <input type="checkbox" name="is_block" {if $data.is_block eq '1'}checked{/if}>
                </td>
            </tr>

            <input hidden name="fio_data" value="{$data.fio_data|escape:'html'}">
            <input hidden name="address_data" value="{$data.address_data|escape:'html'}">


        </table>
    </div>

    <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
        <tr class="table_bg_action">
            <td align="left" colspan="2">
                <input class="submit_form" type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
                <input class="submit_form" type="submit" name="save_add" value="Сохранить и добавить еще">&nbsp;
                <input class="submit_form" type="submit" name="save_list" value="Сохранить и перейти к списку">
            </td>
        </tr>
    </table>

    </form>
{/strip}

{literal}
    <script type="text/javascript">
        Calendar.setup({
            inputField: "date1",
            ifFormat: "%Y-%m-%d",
            timeFormat: "24",
            button: "trigger1",
            singleClick: true,
            step: 1
        });


        $(function () {
            //suggetions fio
            $("input[name=fio]").suggestions({
                serviceUrl: "https://dadata.ru/api/v1/suggest/fio",
                selectOnSpace: true,
                token: "15309822a7c43aacc9f604670c10489fc4b19a0b",
                /* Вызывается, когда пользователь выбирает одну из подсказок */
                onSelect: function (suggestion) {
                    console.log(suggestion)
                    suggestion = suggestion;
                    $('input[name=fio_data]').val((JSON.stringify(suggestion)));


                    $('input#name').val(suggestion.data.name)
                    $('input#surname').val(suggestion.data.surname)
                    $('input#patronymic').val(suggestion.data.patronymic)
                }
            });

            //suggetions address
            $("input[name=address]").suggestions({
                serviceUrl: "https://dadata.ru/api/v1/suggest/address",
                selectOnSpace: true,
                token: "15309822a7c43aacc9f604670c10489fc4b19a0b",
                /* Вызывается, когда пользователь выбирает одну из подсказок */
                onSelect: function (suggestion) {
                    console.log(suggestion)
                    suggestion = suggestion;
                    $('input[name=address_data]').val((JSON.stringify(suggestion)));

                    $('#kladr_id').val(suggestion.data.kladr_id)
                    $('#postal_code').val(suggestion.data.postal_code)
                    $('#country').val(suggestion.data.country)
                    $('#region').val(suggestion.data.region_type + '. ' + suggestion.data.region)
                    $('#city').val(suggestion.data.city_type + '. ' + suggestion.data.city)
                    $('#street').val(suggestion.data.street_type + '. ' + suggestion.data.street)
                    $('#house').val(suggestion.data.house_type + '. ' + suggestion.data.house)


                }
            });

        })

    </script>
{/literal}