{strip}

<!-- Подключение хидера -->
<table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=order
	&id_cat={$smarty.get.id_cat}" method="post" enctype="multipart/form-data" name="form">
      
	<!-- Заголовок таблицы данных -->
	<tr align="center" class="table_header">
	      	<td width="3%" align="center" height="22"></td>
	        	<td width="" align="left"><b>&nbsp;Название раздела</b></td>
	        	<td width="20%" align="center"><b>&nbsp;Каноническое имя</b></td>
	        	<td width="5%" align="center"><b>SORT</b></td>
	        	<td width="3%" align="center"><b>A</b></td>
	        	<td width="3%" align="center"><b>T</b></td>
	</tr>
	<!-- Заголовок таблицы данных -->
      
	<!-- ПЕРВЫЙ УРОВНЬ -->
	{section name=i loop=$item}
	<tr style="background-color:#f2f2f2;">
		<td align="center" ><input type="checkbox" name="id[]" value="{$item[i].id}"></td>
		
		<td align="left">
			&nbsp;<a href="?main={$smarty.get.main}
						   &module={$smarty.get.module}
						   &action=catalog_edit
						   &id_catalog={$item[i].id}
						   &id_cat={$smarty.get.id_cat}">
			{$item[i].title}</a>
		</td>
		
		<td align="center">{$item[i].url}</td>
		<td align="center">{$item[i].sort}</td>
		<td align="center"><img src="images/{$item[i].is_block}" width="14" height="14" border="0"></td>
		<td align="center">{$item[i].is_top}</td>
	</tr>
	
	<!-- ВТОРОЙ УРОВНЬ -->
	{section name=j loop=$item_sub}
		{if $item_sub[j].id_parent eq $item[i].id}
		
		<tr style="background-color:#f8f8f8;">
			<td align="center" ><input type="checkbox" name="id[]" value="{$item_sub[j].id}"></td>
			<td align="left">
				&nbsp;<a href="?main={$smarty.get.main}
							   &module={$smarty.get.module}
							   &action=catalog_edit
							   &id_catalog={$item_sub[j].id}
							   &id_cat={$smarty.get.id_cat}">
				&nbsp;&nbsp;&nbsp;&raquo;&nbsp;{$item_sub[j].title}</a>
			</td>
			<td align="center">{$item_sub[j].url}</td>
			<td align="center">{$item_sub[j].sort}</td>
			<td align="center"><img src="images/{$item_sub[j].is_block}" width="14" height="14" border="0"></td>
		</tr>
		
		
		{/if}
	{/section}
	{/section}
	
	{section name=i loop=$item}
	
	{/section}
</table>

<!-- Подключение футера -->
{include file="table_footer.tpl"}
</form>
{/strip}