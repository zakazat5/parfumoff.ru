{literal}
<script type="text/javascript">
$(document).ready(function(){
	$("#sel_catalog").change( function () {
	 	var selected = $("#sel_catalog option:selected");
	 	window.location = '{/literal}?main={$smarty.get.main}&module={$smarty.get.module}&id_cat={$smarty.get.id_cat}&id_catalog=' + selected.val() + '&action=mod{literal}';
	 });
});
</script>
{/literal}

{strip}
<!-- Подключение хидера -->
{include file="modules/catalog_a/table_header.tpl"}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=order
	&id_cat={$smarty.get.id_cat}" method="post" enctype="multipart/form-data" name="formMod">

<table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">

	<!-- Заголовок таблицы данных -->
	<tr align="center" class="table_header">
		<td width="3%" align="center" height="25px"></td>
  		<td width="3%"><div align="center"><b>N</b></div></td>
  		<td width="40px"><div align="center"><b>IMG</b></div></td>
  		<td width="5%" align="center"><b onmouseover="ddrivetip('Галерея карточки')" onmouseout="hideddrivetip()">GAL</b></td>
		<td width="20%" align="center">
			<select name="sel_catalog" id="sel_catalog" style="width:98%;background-color: gray;color:#fff;">
				<option value="0">-- Все разделы --</option>
				{$sel_catalog}
			</select>
		</td>
		<td width="" align="left">
			&nbsp;<strong>Заголовок</strong>
		</td>
		<td width="10%" align="right"><b>Цена</b>&nbsp;</td>
		<td width="5%" align="center"><b>SORT</b></td>
		<td width="3%" align="center"><b>A</b></td>
		<td width="3%" align="center"><b>M</b></td>
	</tr>

	<!-- Вывод списка -->
	{section name=i loop=$item}
	<tr style="background-color:{$item[i].bgcolor};">
		<td align="center" height="22px"><input type="checkbox" name="id[]" value="{$item[i].id}"></td>
		<td align="center">{$item[i].n}.</td>
		
		<td align="center"><a href="../images/uploads/catalog/{$item[i].id}/big/{$item[i].img}" target="_blank">
		<img width="40" border="0" src="../images/uploads/catalog/{$item[i].id}/small/{$item[i].img}"></a></td>
		
		<td align="center" valign="middle">
				<a href="?main={$smarty.get.main}
							   &module={$smarty.get.module}
							   &action=galarey_foto
							   &id_cat={$smarty.get.id_cat}
							   &id_catalog_data={$item[i].id}">
				<img src="images/nav/next.gif" width="16" height="16" border="0">
				</a>
		</td>
		<td align="left">&nbsp;{$item[i].catalog_name}</td>
		<td align="left">
			&nbsp;<a href="?main={$smarty.get.main}
						   &module={$smarty.get.module}
						   &action=edit
						   &id_cat={$smarty.get.id_cat}
						   &id={$item[i].id}"
						   >
			{$item[i].title}</a>
		</td>
		
		<td align="right">{$item[i].price}&nbsp;<b>руб.</b>&nbsp;</td>
		<td align="center">{$item[i].sort}</td>
		<td align="center"><img src="images/{$item[i].is_block}" width="14" height="14" border="0"></td>
		<td align="center">{$item[i].is_main}</td>
	</tr>
	{/section}
	<!-- ------------ -->
</table>

{include file="table_footer.tpl"}
</form>
{/strip}