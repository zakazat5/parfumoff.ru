{literal}
<script type="text/javascript">
$(document).ready(function(){
	$("#grid_table").flexigrid({
		url: '{/literal}?ajax={$smarty.get.module}&action=mod&id_cat={$smarty.get.id_cat}&id_catalog_data={$smarty.get.id_catalog_data}{literal}',
		dataType: 'json',
		colModel : [
			{display: 'ID', name : 'id', width : 40, sortable : false, align: 'center'},
			{display: 'ARTICLE', name : 'article', width : 100, sortable : false, align: 'left'},
			{display: 'Заголовок', name : 'title', width : 200, sortable : false, align: 'left'},
			{display: 'Тип', name : 'type', width : 100, sortable : false, align: 'left'},
			{display: 'Объем (мл)', name : 'v', width : 50, sortable : false, align: 'left'},
			{display: 'Цена (RUB)', name : 'price', width : 60, sortable : false, align: 'left'},
			{display: 'Цена (USD)', name : 'price_usd', width : 60, sortable : false, align: 'left'},
			{display: 'A', name : 'is_block', width : 20, sortable : false, align: 'center'}
			],
		buttons : [
			{name: 'Выделить все', bclass: 'check', onpress : test},{separator: true},
			{name: 'Снять выделение', bclass: 'uncheck', onpress : test},{separator: true},{separator: true},
			{name: 'Удалить', bclass: 'delete', onpress : test},{separator: true},
			{name: 'Активировать', bclass: 'active', onpress : test},{separator: true},
			{name: 'Заблокировать', bclass: 'deactive', onpress : test},{separator: true}
			],
		sortname: "id",
		sortorder: "desc",
		usepager: false,
		title: '',
		useRp: false,
		rp: 10,
		showTableToggleBtn: true,
		width: 'auto',
		height: 200
	});
});

function test(com,grid){
    if (com=='Удалить'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=delete{literal}",
				data: "items="+itemlist,
				success: function(data){
				$("#grid_table").flexReload();
			}});
		}else{
			return false;
		}
		
     }else if (com=='Выделить все'){
    	$('.bDiv tbody tr',grid).addClass('trSelected');
    
    }else if (com=='Снять выделение'){
    	$('.bDiv tbody tr',grid).removeClass('trSelected');
    	
    }else if (com=='Активировать'){
    	if($('.trSelected',grid).length>0){
    		var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=active{literal}",
				data: "items="+itemlist,
				success: function(data){
				$("#grid_table").flexReload();
			}});
    	}else{
			return false;
		}
		
    }else if (com=='Заблокировать'){
    	if($('.trSelected',grid).length>0){
    		var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=deactive{literal}",
				data: "items="+itemlist,
				success: function(data){
				$("#grid_table").flexReload();
			}});
    	}else{
		return false;
	}
    }
}
</script>
{/literal}