{strip}
<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&id_catalog_data={$smarty.get.id_catalog_data}
	&action=catalog_data_order_add" method="post" enctype="multipart/form-data" id="form">

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Основные параметры
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Артикул:</td>
		<td align="left">
			<input type="text" name="article" value="{$article}" style="width:100px;">
		</td>
	</tr>
	
	<tr>
		<td align="left" width="20%">
		&nbsp;&nbsp;Тип:
		</td>
		<td align="left" width="80%">
			<select name="sel_type">
				{html_options options=$sel_type}
			</select>&nbsp;
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Объем (мл):</td>
		<td align="left">
			<input type="text" name="v" value="{$v}" style="width:150px;">
		</td>
	</tr>

	<tr>
		<td align="left">&nbsp;&nbsp;Цена ($):</td>
		<td align="left">
			<input type="text" name="price_usd" style="width:50px;">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Параметры публикации
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Использовать по умолчанию:</td>
		<td align="left">
			<input type="checkbox" name="is_def" {$is_def}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Заблокировать:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
</table>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="Сохранить и добавить еще">&nbsp;
		<input type="submit" name="save_list" value="Сохранить и перейти к списку">
	</td>
	</tr>
</table>

</form>
{/strip}