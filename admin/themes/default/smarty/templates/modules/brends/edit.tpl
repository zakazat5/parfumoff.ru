 {include file="editor.tpl"}

{strip}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=edit&id_brend={$id}" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a id="tab1" href="#tab-1"><span>Основные данные</span></a></li>
		<li class=""><a id="tab3" href="#tab-3"><span>Текстовые данные</span></a></li>
		<li class=""><a id="tab4" href="#tab-4"><span>Параметры публикации</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	
	
	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Название бренда (title) :</td>
		<td align="left" width="80%">
			<input type="text" name="title" value="{$title}" style="width:100%">
		</td>
	</tr>
	<tr>
		<td align="left" width="20%">&nbsp;Название бренда (title_1) :</td>
		<td align="left" width="80%">
			<input type="text" name="title_1" value="{$title_1}" style="width:100%">
		</td>
	</tr>	
	<tr>
		<td align="left" width="20%">&nbsp;Название бренда (title_2) :</td>
		<td align="left" width="80%">
			<input type="text" name="title_2" value="{$title_2}" style="width:100%">
		</td>
	</tr>
	<tr>
		<td align="left">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;SEO URL:</td>
					<td align="right" style="border:0px;">
					<a href="#" 
					onmouseover="ddrivetip('Внимание!При ручном редактировании URL, не должно быть спецсимволов. Разрешается использовать в качестве разделителя только -', 200)" onmouseout="hideddrivetip()"
					>
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
					</td>
				</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="url" value="{$url}" style="width:100%;">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Мета теги
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;HTML-тег Title :</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;HTML-тег Title_1 :</td>
		<td align="left">
			<input type="text" name="meta_title_1" value="{$meta_title_1}" style="width:100%">
		</td>
	</tr>
	<!--
	<tr>
		<td align="left">&nbsp;Мета-тег Keywords :</td>
		<td align="left">
			<input type="text" name="meta_keywords" value="{$meta_keywords}" style="width:100%">
		</td>
	</tr>
	-->
	<tr>
		<td align="left">&nbsp;Мета-тег Description :</td>
		<td align="left">
			<input type="text" name="meta_description" value="{$meta_description}" style="width:100%" >
		</td>
	</tr>
	
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Описание бренда:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="text" name="text" style="width:100%" rows=20>{$text}</textarea>
		</td>
	</tr>
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left">&nbsp;&nbsp;Заблокировать :</td>
		<td align="left">
			<input type="checkbox" name="is_block" value="" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;TOP :</td>
		<td align="left">
			<input type="checkbox" name="is_top" value="" {$is_top}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;TOP 300 :</td>
		<td align="left">
			<input type="checkbox" name="is_top300" value="" {$is_top300}>
		</td>
	</tr>
	
	<tr>
		<td align="left" width="20%">&nbsp;TOP 300 (ancor) :</td>
		<td align="left" width="80%">
			<input type="text" name="is_top300_ancor" value="{$is_top300_ancor}" style="width:100%">
		</td>
	</tr>

</table>
</div>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_edit">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
	</td>
	</tr>
</table>
</form>
{/strip} 