{strip}

<!-- Подключение хидера -->
{include file="modules/brends/table_header.tpl"}
<table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=order" method="post" enctype="multipart/form-data" name="form">
      
	<!-- Заголовок таблицы данных -->
	<tr align="center" class="table_header">
	      	<td width="3%" align="center" height="22"></td>
			<td width="5%"><div align="center"><b>N</b></div></td>
			<td width="5%" align="center"><b>ID</b></td>
	        <td width="31%" align="left"><b>&nbsp;Название бренда (Eng)</b></td>
			<td width="31%" align="left"><b>&nbsp;Название бренда (Рус)</b></td>
	        <td width="10%" align="center"><b>TOP</b></td>
			<td width="10%" align="center"><b>TOP 300</b></td>
	        <td width="5%" align="center"><b>Статус</b></td>
	</tr>
	<!-- Заголовок таблицы данных -->
      
	<!-- ПЕРВЫЙ УРОВНЬ -->
	{section name=i loop=$item}
	<tr style="background-color:#f2f2f2;">
		<td align="center" ><input type="checkbox" name="id[]" value="{$item[i].id}"></td>
		<td align="center">{$item[i].n_brend}.</td>
		<td align="center">{$item[i].id}</td>
		<td align="left">
			&nbsp;<a href="?main={$smarty.get.main}
						   &module={$smarty.get.module}
						   &action=brend_edit
						   &id_brend={$item[i].id}">
			{$item[i].name}</a>
		</td>
		<td align="left">
			&nbsp;<a href="?main={$smarty.get.main}
						   &module={$smarty.get.module}
						   &action=brend_edit
						   &id_brend={$item[i].id}">
			{$item[i].name1}</a>
		</td>
		<td align="center"><img src="images/{$item[i].top}" width="14" height="14" border="0"></td>
		<td align="center"><img src="images/{$item[i].top300}" width="14" height="14" border="0"></td>
		<td align="center"><img src="images/{$item[i].active}" width="14" height="14" border="0"></td>
	</tr>
	{/section}
</table>

<!-- Подключение футера -->
{include file="table_footer.tpl"}
</form>
{/strip}