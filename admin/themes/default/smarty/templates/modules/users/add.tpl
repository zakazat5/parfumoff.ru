{strip}

<form action="?main={$smarty.get.main}&worker={$smarty.get.module}&action=add" method="post" enctype="multipart/form-data" name="form" autocomplete="off">

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	
	<tr>
		<td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
		<b>&nbsp;&nbsp;Публикация</b>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Заблокировать:</td>
		<td align="left">
			<input type="checkbox" name="is_block">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Участие в публикации сайта:</td>
		<td align="left">
			<input type="checkbox" name="is_public" >
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
		<b>&nbsp;&nbsp;Права</b>
		</td>
	</tr>
	
	<tr>
		<td width="30%" align="left"><span style="color:red;">*</span>&nbsp;Должность:</td>
		<td width="70%" align="left">
			<select name="capacity" style="width:100%">
			    <option value='null'>-- Выберите должность --</option>
			    {html_options values=$sel_id output=$sel_names selected=$is_active}
			</select>
		</td>
	</tr>
	
	<tr>
		<td width="30%" align="left"><span style="color:red;">*</span>&nbsp;Право доступа (Роль):</td>
		<td width="70%" align="left">
			<select name="sel_role" style="width:100%">
			    <option value='null'>-- Выберите роль --</option>
			    {html_options values=$role_id output=$role_names selected=$role_active}
			</select>
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
		<b>&nbsp;&nbsp;Авторизация</b>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Логин:</td>
		
		<td align="left">
			<input type="text" name="login" value="" style="width:100%;color:blue;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
		<td align="left">
			<span style="color:red;">*</span>&nbsp;Пароль:
		</td>
		<td align="left">
			<input type="password" name="password" value="" style="width:100%;color:red;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Повторить пароль:</td>
		<td align="left">
			<input type="password" name="re_password" value="" style="width:100%;color:red;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="add" value="Сохранить" style="width:100px">
	</td>
	</tr>
</table>
</form>
{/strip}