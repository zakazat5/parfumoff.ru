{include file="editor.tpl"}
{strip}
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<form action="?main={$smarty.get.main}&worker={$smarty.get.module}&action=edit&id={$smarty.get.id}" method="post" enctype="multipart/form-data" name="form">
		
	
	<tr>
		<td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
		<b>&nbsp;&nbsp;Права</b>
		</td>
	</tr>
	
	<tr>
		<td align="left">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="border:0px"><span style="color:red;">*</span>&nbsp;Логин:</td>
				<td align="right" style="border:0px">
				<a href="#" 
					onmouseover="return overlib('
					Логин может быть не больше 30 знаков. <br>
					Использовать только латинские буквы [A-Z] и цифры [0-9]
					', LEFT);" 
					onmouseout="return nd();" >
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
				</td>
			</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="login" value="{$login}" style="width:100%;color:blue;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
		<td align="left">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="border:0px">&nbsp;&nbsp;Пароль:</td>
				<td align="right" style="border:0px">
				<a href="#" 
					onmouseover="return overlib('
					Пароль может быть не более 30 знаков
					', LEFT);" 
					onmouseout="return nd();" >
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
				</td>
			</tr>
			</table>
		</td>
		<td align="left">
			<input type="password" name="password" value="" style="width:100%;color:red;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Повторить пароль:</td>
		<td align="left">
			<input type="password" name="re_password" value="" style="width:100%;color:red;font-weight:bold;">
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2" height="25px" bgcolor="#eef1f7">
		<b>&nbsp;&nbsp;Личные даные</b>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;ФИО:</td>
		<td align="left">
			<input type="text" name="full_name" value="{$fullName}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;e-mail:</td>
		<td align="left">
			<input type="text" name="email" value="{$email}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left" width="30%">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="border:0px"><span style="color:red;">*</span>&nbsp;Загрузка аватара:</td>
				<td align="right" style="border:0px">
				<a href="#" 
					onmouseover="return overlib('
					По ширине 180px.
					', LEFT);" 
					onmouseout="return nd();" >
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
				</td>
			</tr>
			</table>
		</td>
		
		<td align="left" width="70%">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2">
			&nbsp;&nbsp;О себе<br>
			<textarea name="user_text" style="width:100%" rows=10>{$user_text}</textarea>
		</td>
	</tr>

	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="add" value="Сохранить" style="width:100px">
	</td>
	</tr>
</table>
</form>
{/strip}