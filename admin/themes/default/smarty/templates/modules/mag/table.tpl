
<!-- Подключение хидера -->
{include file="modules/mag/table_header.tpl"}

<!--<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=order" method="post" enctype="multipart/form-data" name="formMod">-->

<table width="100%" border="0" cellpadding="4" cellspacing="2" class="form">

	<!-- Заголовок таблицы данных -->
	<tr align="center" class="table_header" >
		<!--<td width="3%" align="center" height="22"></td>-->
		<td width="5%"><div align="center"><b>ID</b></div></td>
		
		<td width="6%" align="right">PRICE</td>
		<td width="5%" align="left">SUM</td>
		<td width="" align="left">
			<a href="?main={$smarty.get.main}&module={$smarty.get.module}&action=mod&status=1">
			<div style="background-color: #80aa70;float:left;width:15px;height:15px;border:1px #eee solid;margin-right:5px;"
			onmouseover="ddrivetip('<b>Новые</b> заказы')" onmouseout="hideddrivetip()"
			>&nbsp;</div></a>
			
			<a href="?main={$smarty.get.main}&module={$smarty.get.module}&action=mod&status=2">
			<div style="background-color: #aca200;float:left;width:15px;height:15px;border:1px #eee solid;margin-right:5px;"
			onmouseover="ddrivetip('<b>Подтвержденные</b> заказы')" onmouseout="hideddrivetip()"
			>&nbsp;</div></a>
			
			<a href="?main={$smarty.get.main}&module={$smarty.get.module}&action=mod&status=-1">
			<div style="background-color: #d95e5e;float:left;width:15px;height:15px;border:1px #eee solid;margin-right:5px;"
			onmouseover="ddrivetip('<b>Отклоненные</b> заказы')" onmouseout="hideddrivetip()"
			>&nbsp;</div></a>
			
			<a href="?main={$smarty.get.main}&module={$smarty.get.module}&action=mod&status=3">
			<div style="background-color: #6fabd9;float:left;width:15px;height:15px;border:1px #eee solid;margin-right:5px;"
			onmouseover="ddrivetip('<b>Доставленные</b> заказы')" onmouseout="hideddrivetip()"
			>&nbsp;</div></a>
		</td>
		
		
		<td width="20%" align="left">CLIENT</td>
		<td width="20%">ВРЕМЯ / ДЕЙСТВИЕ</td>
	</tr>
	<!-- Заголовок таблицы данных -->
      
	
	
	<!-- Вывод списка -->
	{section name=i loop=$item}
	<tr style="background-color:{$item[i].bgcolor};">
		
		<td align="center" valign="top" bgcolor="#f2f2f2">
			<b>{$item[i].id}</b><br />
			<!--<input type="checkbox" name="id[]" value="{$item[i].id}">-->
		</td>
		
		<td align="right" valign="top" bgcolor="#f8f8f8" style="border:1px {$item[i].bgcolor} solid;">{$item[i].price}</td>
		
		<td align="left" valign="top" bgcolor="#f8f8f8" style="border:1px {$item[i].bgcolor} solid;">
			{$item[i].sum}	<span style="color:red;">={$item[i].big_sum}</span>
		</td>
		
		<td align="left" style="text-decoration:underline;color:#f4f4f4" valign="top">
			{$item[i].title}
			
		</td>
		
		<td align="left" bgcolor="#f2f2f2" valign="top" style="border:1px {$item[i].bgcolor} solid;">
			<b>{$item[i].fio}</b><br>
			<a href="mailto:{$item[i].email}">{$item[i].email}</a><br>
			{$item[i].phone}<br>
			{$item[i].address}<br>
			{$item[i].city}<br>
			{$item[i].metro}<br /><br />
			[ <a target="_blank" href="?main=print&module=mag&id_orders={$item[i].id}&id_client={$item[i].id_client}">печать</a> ]
		</td>
		
		
		<td align="left"  valign="top" bgcolor="#f8f8f8" style="color:gray;">
			<form action="?main={$smarty.get.main}&worker={$smarty.get.module}&action=order&id={$item[i].id}&status={$smarty.get.status}" 
			method="post" enctype="multipart/form-data" name="formMod_{$item[i].id}">
			
			{$item[i].date}<br>
			{if $item[i].status eq '1'}
				<select name="status_{$item[i].id}" style="width:100%;">
				    <option value='2'>Подтвердить заказ</option>
				    <option value='-1'>Отклонить заказ</option>
				</select>
				<textarea name="comment_{$item[i].id}" rows="5" style="width:100%;">{$item[i].comment}</textarea>
				<input type="submit" name="Submit" value="Применить" style="width:100%;">
			{elseif $item[i].status eq '2'}
				<select name="status_{$item[i].id}" style="width:100%;">
				    <option value='3'>Доставлен</option>
				    <option value='-1'>Отклонить заказ</option>
				</select>
				<textarea name="comment_{$item[i].id}" rows="5" style="width:100%;">{$item[i].comment}</textarea>
				<input type="submit" name="Submit" value="Применить" style="width:100%;">
				
			{elseif $item[i].status eq '-1'}
				<b></b><br>
				<select name="status_{$item[i].id}" style="width:100%;">
				    <option value='1'>Вернуть в новые</option>
				</select>
				<textarea name="comment_{$item[i].id}" rows="5" style="width:100%;">{$item[i].comment}</textarea>
				<input type="submit" name="Submit" value="Применить" style="width:100%;">
			{else}
				<b>Комментарий:</b><br>
				{$item[i].comment}
			{/if}
			
			</form>
			
		</td>
		
	</tr>
	{/section}
	<!-- ------------ -->
</table>

<!-- Подключение футера -->
{include file="table_footer.tpl"}
<!--</form>-->

<!-- Подключение пагиса -->
{include file='pages.tpl'}
