

<table width="80%" border="1" cellpadding="10" cellspacing="0" >
	<tr>
		<td width="50%" height="40">Заказ</td>
		<td width="50%">Доставка</td>
	</tr>
	
	<tr>
		<td valign="top">
			<div>Номер заказа: <strong>{$id_orders}</strong></div>
			<div>Покупатель: <strong>{$client.fio}</strong></div>
			<div>Ближайшее метро: <strong>{$client.metro}</strong></div>
			<div>Телефон: <strong>{$client.phone}</strong></div>
		</td>
		
		<td valign="top">
			<div>Доставка: <strong>курьер</strong></div>
			<div>Получатель: <strong>{$client.fio}</strong></div>
			<div>Адрес доставки заказа: <strong>{$client.address}</strong></div>
		</td>
	</tr>
</table>

<br />
<table width="80%" border="1" cellpadding="5" cellspacing="0" >
	<tr>
		<td width="60%" align="left">Наименование</td>
		<td width="20%" align="left">Количество</td>
		<td width="20%" align="left">Стоимость</td>
	</tr>
	
	{section name=i loop=$item}
	<tr>
		<td >{$item[i].title}</td>
		<td >{$item[i].kol}</td>
		<td >RUR {$item[i].sum}.00</td>
	</tr>
	{/section}
</table>
<br />

<table width="80%" border="1" cellpadding="10" cellspacing="0" >
	<tr>
		<td valign="top" width="50%">
			<div>Подытог:</div>
			<div>Скидка, %:</div>
			<div>Стоимость доставки:</div>
			<div>Итого:</div>
		</td>
		
		<td valign="top" width="50%">
			<div>RUR {$total}.00</div>
			<div>0</div>
			<div>RUR 0.00</div>
			<div>RUR {$total}.00</div>
		</td>
	</tr>
</table>