{strip}

<!-- Подключение хидера -->

<table width="100%" border="0" cellpadding="3" cellspacing="0" class="form_add">
	<tr>
		<td align="left" bgcolor="#eef1f7">
		<img align="left" src="images/folder_add.png" width="16" height="16"></img>
		<div align=left class="link_block">&nbsp;Создать раздел</div>
		</td>
		
		<td align="right" bgcolor="#eef1f7">
			<select name="type_cat" style="width:300px" 
			onchange="hideshow_select(this); return false;">
				<option value='null'>-- Выберите модуль --</option>
				{html_options values=$module_value output=$module_names selected=null}
			</select>
		</td>
	</tr>
</table>

{include file="modules/cat/change_cat_catalog/add_catalog.tpl"}

<!-- Таблица вывода меню -->
<table width="100%"  border="0" cellpadding="1" cellspacing="1" class="form">
	<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id={$smarty.get.id}
	&action=order" 
	method="post" enctype="multipart/form-data" name="formMod">
      
	<!-- Заголовок таблицы данных -->
	<tr align="center" class="table_header">
      	<td width="3%" height="22" align="center"></td>
        <td width="60%" align="left"><b>&nbsp;Название</b></td>
        <td width="" align="left"><b>&nbsp;Каноническое имя</b></td>
        <td width="" align="left"><b>&nbsp;Тип</b></td>
        
        <td width="4%" align="center">
        	<a href="#" 
				onmouseover="return overlib('Индекс сортировки', LEFT);" 
				onmouseout="return nd();" >
        	<img src="images/b_comment.png" width="16" height="16" border="0">
        </td>
        
        <td width="3%" align="center">
        	<a href="#" 
				onmouseover="return overlib('Статус блокировки', LEFT);" 
				onmouseout="return nd();" >
        	<img src="images/b_comment.png" width="16" height="16" border="0">
        </td>
        
	</tr>
      
	<!-- Вывод списка -->
	{section name=i loop=$item_cat}
		<tr style="background-color:{$item_cat[i].bgcolor};">
		<td align="center" height="25px"><input type="checkbox" name="id[]" value="{$item_cat[i].id}"></td>
		<td align="left">
		&nbsp;<a href="?main={$smarty.get.main}
					   &module={$smarty.get.module}
					   &action=change_cat_edit_catalog
					   &id={$smarty.get.id}
					   &id_cat={$item_cat[i].id}">{$item_cat[i].name}</a>
		
		</td>
		<td align="left">&nbsp;{$item_cat[i].canon_name}</td>
		<td align="left">&nbsp;{$item_cat[i].type}</td>
		<td align="center">&nbsp;{$item_cat[i].sort}</td>
		<td align="center"><img src="images/{$item_cat[i].is_block}" width="14" height="14" border="0"></td>
		</tr>
	{/section}
</table>

<!-- Подключение футера -->
{include file="modules/cat/table_footer.tpl"}
</form>
{/strip}