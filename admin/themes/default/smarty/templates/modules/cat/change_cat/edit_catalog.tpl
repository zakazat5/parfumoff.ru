{strip}

<table width="100%" border="0" cellpadding="4" cellspacing="0" class="form_add">
	<tr>
		<td align="left" bgcolor="#eef1f7">
			<img align="left" src="images/folder_edit.png" width="16" height="16"></img>
			<div align=left class="link_block">&nbsp;Редактирование раздела - [{$name}]</div>
		</td>
	</tr>
</table>

<form action="
		?main={$smarty.get.main}
		&worker={$smarty.get.module}
		&id={$smarty.get.id}
		&id_cat={$smarty.get.id_cat}
		&action=edit_cat"
		method="post" enctype="multipart/form-data" name="form_news">


<table width="100%" border="0"  cellpadding="5" cellspacing="0" class="form_add" style="border-top: #abb6d7 0px solid;">
	
	
	<tr>
		<td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Название раздела:</td>
		<td align="left" width="70%">
			<input type="text" name="name" value="{$name}" style="width:100%" class="form_add">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Каноническое имя:</td>
		<td align="left">
			<input type="text" name="canon_name" value="{$canon_name}" style="width:100%" class="form_add">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Блокировка раздела:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Индекс сортировки:</td>
		<td align="left">
			<input type="text" name="sort" value="{$sort}" value="100" style="width:50px;">
		</td>
	</tr>
	
	<tr>
		<td align="left" bgcolor="#eef1f7" colspan="2">
		&nbsp;&nbsp;Мета тэги
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;TITLE:</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left" valign="top">&nbsp;&nbsp;DESCRIPTION:</td>
		<td align="left">
			<textarea name="meta_description" style="width:100%" rows=2>{$meta_description}</textarea>
		</td>
	</tr>

	<tr>
	<td align="left" colspan="2">
		<input type="hidden" name="type_cat" value="{$type}" >
		<input type="submit" name="edit_cat" value="Изменить" style="width:100px;">
		</form>
	</td>
	</tr>
</table>

{/strip}