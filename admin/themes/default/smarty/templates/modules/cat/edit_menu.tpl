{strip}

<form action="
		?main={$smarty.get.main}
		&worker={$smarty.get.module}
		&action=edit_menu
		&id={$smarty.get.id}" 
		method="post" enctype="multipart/form-data" name="form">

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	
	<tr>
		<td align="left" bgcolor="#eef1f7" colspan="2">
		&nbsp;&nbsp;Редактирование блока меню
		</td>
	</tr>
	
	<tr>
		<td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Название блока меню:</td>
		<td align="left" width="70%">
			<input type="text" name="title" value="{$title}" style="width:100%" class="form_add">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Заблокировать:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2">
			<input type="submit" name="add" value="Изменить" style="width:100px">
		</td>
	</tr>
</table>
</form>
{/strip}