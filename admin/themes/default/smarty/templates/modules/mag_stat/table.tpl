{include file="calendar.tpl"}
<!--{literal}
<script type="text/javascript">
$(document).ready(function(){

	$("#client_table").flexigrid({
		url: '{/literal}?ajax={$smarty.get.module}&action=mod{literal}',
		dataType: 'json',
		colModel : [
		{display: '№ заказа', name : 'id', width : 60, sortable : false, align: 'center'},
		{display: 'Товаров в заказе', name : 'count', width : 60, sortable : false, align: 'center'},
		{display: 'Дата доставки', name : 'DateEnd', width : 100, sortable : false, align: 'center'},
		{display: 'Наименование', name : 'title', width : 300, sortable : false, align: 'left'},
		{display: 'Склад', name : 'sklad', width : 50, sortable : false, align: 'center'},
		{display: 'Цена со скидкой', name : 'dSum', width : 50, sortable : false, align: 'center'},
		{display: 'Цена без скидки', name : 'Sum', width : 50, sortable : false, align: 'center'},
		{display: '% скидки', name : 'dPercent', width : 50, sortable : false, align: 'center'},
		{display: 'Распродажа?', name : 'sale', width : 50, sortable : false, align: 'center'},
		{display: 'Комиссия', name : 'commission', width : 50, sortable : false, align: 'center'},
		{display: 'Потери на скидках Сергей', name : 'partner_percent', width : 50, sortable : false, align: 'center'},
		{display: 'Наценка на товар', name : 'partner_percent', width : 50, sortable : false, align: 'center'},
		{display: 'Расход на курьера', name : 'partner_percent', width : 50, sortable : false, align: 'center'},
		{display: 'Доход Сергей', name : 'partner_percent', width : 50, sortable : false, align: 'center'},
		],
		/*buttons : [
		{name: 'Выделить все', bclass: 'check', onpress : action},{separator: true},
		{name: 'Снять выделение', bclass: 'uncheck', onpress : action},{separator: true},{separator: true},
		{name: 'Активировать', bclass: 'active', onpress : action},{separator: true},
		{name: 'Заблокировать', bclass: 'deactive', onpress : action},{separator: true},
		{name: 'Удалить', bclass: 'delete', onpress : action},{separator: true},
		],*/
		/*searchitems : [
		{display: 'ID', name : 'id', isdefault: true},
		{display: 'EMAIL', name : 'email'},
		],*/
		sortname: "",
		sortorder: "",
		usepager: false,
		showTableToggleBtn: false,
		width: 'auto',
		height: 'auto',
		useRp: false,
		nomsg: 'Нет элементов',
	});
});
</script>
{/literal}-->

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=send" method="post" enctype="multipart/form-data" id="form_add_reg">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>Отчет</span></a></li>
	</ul>
</div>
<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="400" border="0" cellpadding="5" cellspacing="1" class="form_add"  >
	<tr>
		<td align="left">E-Mail:</td>
		<td align="left">
			<input style="width:100%;" type="text" name="email"  value="" />
		</td>
	</tr>
	<tr>
		<td align="left" width="30%">Начальная дата:</td>
		<td align="left" width="70%">
			<input style="" type="text" name="date1" id="date1" value="" />
			&nbsp;<input style="border:1px solid grey;" type="button" id="trigger1" value="...">
		</td>
	</tr>
	<tr>
		<td align="left">Конечная дата:</td>
		<td align="left">
			<input style="" type="text" name="date2" id="date2" value="" />
			&nbsp;<input style="border:1px solid grey;" type="button" id="trigger2" value="...">
		</td>
	</tr>
	
	<tr>
		<td align="left">Фильтр:</td>
		<td align="left">
			<select name="filter" id="stat_filter">
				<option value="all">Все</option>
				<option value="region">Регионы</option>
			</select>
		</td>
	</tr>
    {literal}
    <script type="text/javascript">
    $(document).ready(function(){
        $('#stat_type').change(function(){
           if($(this).val()!='standart'){
              $('#stat_filter').attr('disabled','disabled');
              return;
           } 
           $('#stat_filter').removeAttr('disabled');
        });
        });
    </script>
    {/literal}
    <tr>
		<td align="left">Тип отчета:</td>
		<td align="left">
			<select id="stat_type" name="stat_type" >
				<option value="standart">Стандартный</option>
				<option value="manager">По манагерам</option>
                <option value="ccause">Причины отказа</option>
                <option value="source">Источники</option>
			</select>
		</td>
	</tr>
	
	<tr>
		<td align="right" colspan="2"><input id="submit_form" type="submit" name="save" value="Выслать" ></td>
	</tr>
	
</table>
</div>

</form>

{literal}
<script type="text/javascript">
Calendar.setup({
	inputField     	: "date1",
	ifFormat       	: "%Y-%m-%d %H:%M:%S",
	button         	: "trigger1",
	singleClick    	: true,
	step           	: 1,
	showsTime		: true
});
Calendar.setup({
	inputField     	: "date2",
	ifFormat       	: "%Y-%m-%d %H:%M:%S",
	button         	: "trigger2",
	singleClick    	: true,
	step           	: 1,
	showsTime		: true
});
</script>
{/literal}

<!--<table id="client_table" class="flexigrid" style="display:none"></table>-->