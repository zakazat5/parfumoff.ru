{include file="editor.tpl"}
{strip}

{literal}
<script type="text/javascript">
$(document).ready(function(){
	
	// вывод списка мест
	$("#sel_brend").change( function () {
		$("#sel_catalog_data").remove();
		$("#msg").html( "Ожидаем ... " );
		
		var selected = $("#sel_brend option:selected");

		$.post("{/literal}?ajax={$smarty.get.module}&action=select_catalog_data&id_cat={$smarty.get.id_cat}{literal}", {
			id_brend: selected.val()
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				$("#msg").html( $("msg",message).text() );
			});
		});
	});
});
</script>
{/literal}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&action=add" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>Основные данные</span></a></li>
		<li class=""><a href="#tab-2"><span>Текстовые данные</span></a></li>
		<li class=""><a href="#tab-3"><span>Параметры публикации</span></a></li>
		<li class=""><a href="#tab-4"><span>Фото</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Заголовок:</td>
		<td align="left" width="80%">
			<input type="text" name="title" value="" style="width:100%" onkeyup="url_convert();" onchange="url_convert();">
		</td>
	</tr>
		
	<tr>
		<td align="left">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;ЧПУ URL:</td>
					<td align="right" style="border:0px;">
					<img src="images/warnings.png" width="16" height="16" border="0" onmouseover="ddrivetip('Внимание!При ручном редактировании ЧПУ, не должно быть спецсимволов. Разрешается использовать в качестве разделителя только -')" onmouseout="hideddrivetip()">
					</td>
				</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="url" value="" style="width:100%;">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Принадлежность
		</td>
	</tr>
	
	<tr >
		<td align="left" width="20%">&nbsp;&nbsp;Бренд:</td>
		<td align="left" width="80%">
			<select name="sel_brend" id="sel_brend" style="width:100%">
				<option value="0" >-- Выберите Бренд --</option>
				{$sel_brend}
			</select>
		</td>
	</tr>
	
	<tr >
		<td align="left" >&nbsp;&nbsp;Аромат:</td>
		<td align="left" >
			<select name="sel_catalog_data" id="sel_catalog_data" style="width:100%">
				<option value="0">-- Выберите аромат --</option>
			</select>
			<div id="msg"></div>
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Мета тэги
		</td>
	</tr>
	
	<tr>
		<td align="left">
		<input type="checkbox" name="is_meta_title"
			onmouseover="ddrivetip('Ручное редактирование')" onmouseout="hideddrivetip()"
		>&nbsp;TITLE:</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left" valign="top">
		<input type="checkbox" name="is_meta_description"
			onmouseover="ddrivetip('Ручное редактирование')" onmouseout="hideddrivetip()"
		>&nbsp;DESCRIPTION:</td>
		<td align="left">
			<input type="text" name="meta_description" value="{$meta_description}" style="width:100%" >
		</td>
	</tr>
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-2">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Аннотация:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="anot" name="anot" style="width:100%" rows=10></textarea>
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;Полное описание:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="text" name="text" style="width:100%" rows=20></textarea>
		</td>
	</tr>
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;Индекс сортировки:</td>
		<td align="left" width="80%">
			<input type="text" name="sort" value="100" style="width:50px">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Заблокировать:</td>
		<td align="left">
			<input type="checkbox" name="is_block">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Отображать на главной:</td>
		<td align="left">
			<input type="checkbox" name="is_main">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Ставить скрытую ссылку:</td>
		<td align="left">
			<input type="checkbox" name="is_links" {$is_links} checked>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Выводить фото большое фото:</td>
		<td align="left">
			<input type="checkbox" name="is_imgf" {$is_imgf} checked>
		</td>
	</tr>

</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;Выберите фото:</td>
		<td align="left" width="80%">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" >&nbsp;&nbsp;Описание фото:</td>
		<td align="left">
			<input type="text" name="alt" value="" style="width:100%" >
		</td>
	</tr>
</table>
</div>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="Сохранить" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="Сохранить и добавить еще">&nbsp;
		<input type="submit" name="save_list" value="Сохранить и перейти к списку">
	</td>
	</tr>
</table>

</form>
{/strip}
