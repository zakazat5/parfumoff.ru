{include file="calendar.tpl"}
{strip}
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action={$smarty.get.action}
	&id_cat={$smarty.get.id_cat}
	&id={$smarty.get.id}" method="post" enctype="multipart/form-data" name="form">
	
	<tr>
		<td align="left" bgcolor="#eef1f7" colspan="2">
		&nbsp;&nbsp;Редактирование галереи
		</td>
	</tr>
	
	<tr>
		<td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Заголовок:</td>
		<td align="left" width="70%">
			<input type="text" name="title" value="{$title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Рубрика:</td>
		<td align="left">
			<select name="sel_rubric" style="width:100%">
				<option value="null">-- Выберите рубрику --</option>
				{$sel_rubric}
			</select>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Дата создания:</td>
		<td align="left">
			<input type="text" name="date1" id="date1" value="{$date1}" />
			&nbsp;<input type="button" id="trigger1" value=" ... ">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Индекс сортировки:</td>
		<td align="left">
			<input type="text" name="sort" value="{$sort}" style="width:50px">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;Заблокировать:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>

	<tr>
		<td align="left" >&nbsp;&nbsp;Превью для аннотации:</td>
		<td align="left">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2">
		&nbsp;&nbsp;<img src="/images/uploads/foto_galarey/{$smarty.get.id}/trumbs/{$img}" width="100" border="0">
		<input type="hidden" value="{$img}" name="del_img">
		</td>
	</tr>
	
	<tr>
		<td align="left" bgcolor="#eef1f7" colspan="2">
		&nbsp;&nbsp;Аннотация:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea name="anot" style="width:100%" rows=5>{$anot}</textarea>
		</td>
	</tr>
	
	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="save" value="Сохранить">&nbsp;
		<input type="submit" name="save_exit" value="Сохранить и перейти к списку">&nbsp;
		<input type="submit" name="save_foto" value="Сохранить и перейти к добавлению фото">
	</td>
	</tr>
</table>
</form>
{/strip}


{literal}
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "date1",
	ifFormat       :    "%Y-%m-%d %H:%M:%S",
	timeFormat     :    "24",
	button         :    "trigger1",
	showsTime      :    true,
	singleClick    :    true,
	step           :    1
	});
</script>
{/literal}