{strip}

<!-- Подключение хидера -->

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<form action="
		?main={$smarty.get.main}
		&worker={$smarty.get.module}
		&action=foto
		&id_cat={$smarty.get.id_cat}
		&id={$smarty.get.id}" 
		method="post" enctype="multipart/form-data" name="form">
	
	<tr>
		<td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Выберите файл:</td>
		<td align="left" width="70%">
			<input type="file" name="img" style="width:100%" class="form_add">
		</td>
	</tr>
	
	<tr>
		<td align="left" ><span style="color:red;">*</span>&nbsp;Заголовок фотографии (TITLE):</td>
		<td align="left" >
			<input type="text" name="title" style="width:100%" class="form_add" value="{$img_title}">
		</td>
	</tr>
	
	<tr>
		<td align="left" >&nbsp;&nbsp;Описание фотографии (ALT):</td>
		<td align="left" >
			<input type="text" name="alt" style="width:100%" class="form_add" value="{$img_alt}">
		</td>
	</tr>
	
	<tr>
		<td align="left"  valign="top">&nbsp;&nbsp;Аннотация:</td>
		<td align="left" >
			<textarea name="anot" style="width:100%" rows=5></textarea>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;Индекс сортировки:</td>
		<td align="left">
			<input type="text" name="sort" value="100" style="width:50px" value="{$img_sort}">
		</td>
	</tr>

	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="foto_add" value="Добавить">
	</td>
	</tr>
	</form>
</table>


<table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id={$smarty.get.id}
	&id_cat={$smarty.get.id_cat}
	&action=order" method="post" enctype="multipart/form-data" name="formMod">
      
	<!-- Заголовок таблицы данных -->
	<tr align="center" class="table_header">
	<td width="3%" align="center" height="22"></td>
	<td width="3%"><div align="center"><b>N</b></div></td>
	<td width="10%"></td>
	<td width="" align="left"><b>&nbsp;Заголовок фотографии</b></td>
	<td width="3%" align="center"><b>SORT</b></td>
	</tr>
	<!-- Заголовок таблицы данных -->
      
	<!-- Вывод списка -->
	{section name=i loop=$item}
	
	{if $item[i].id eq $smarty.get.id_foto}<tr style="background-color:#fffbb7;">
	{else}<tr style="background-color:{$item[i].bgcolor};">{/if}
	
		<td align="center" height="25px"><input type="checkbox" name="id[]" value="{$item[i].id}"></nobr></td>
		<td align="center">{$item[i].n}.</td>
		<td align="center">
			<a href="?main={$smarty.get.main}
					   &module={$smarty.get.module}
					   &action=foto_edit
					   &id={$smarty.get.id}
					   &id_foto={$item[i].id}
					   &id_cat={$smarty.get.id_cat}">
			<img src="../images/uploads/foto_galarey/{$smarty.get.id}/trumbs/{$item[i].img}" width="80" border="1">
			</a>
		</td>
		<td align="left">
		&nbsp;<a href="?main={$smarty.get.main}
					   &module={$smarty.get.module}
					   &action=foto_edit
					   &id={$smarty.get.id}
					   &id_foto={$item[i].id}
					   &id_cat={$smarty.get.id_cat}">{$item[i].title}</a>
		
		</td>
		<td align="center">{$item[i].sort}</td>
	</tr>
	{/section}
	<!-- ------------ -->
</table>

<!-- Подключение пагиса -->

<!-- Подключение футера -->
{include file="modules/galarey/table_footer.tpl"}
</form>
{/strip}