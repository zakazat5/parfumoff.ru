<div id="form-brand" style="display:none;" title="Добавление нового каталога">
    <form action="" class="form-brand">
        <div class="text">
            <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

                <tr class="table_bg">
                    <td align="left" colspan="2">
                        &nbsp;&nbsp;Основная информация
                    </td>
                </tr>
                <tr>
                    <td align="left" width="30%"><span style="color:red;">*</span>&nbsp;Родительский раздел:</td>
                    <td align="left" width="70%">
                        <select name="id_cat" id="id_cat"  style="width:200px;">
                            <option {if $item.id_cat==82}SELECTED{/if} value="82">Парфюмерия</option>
                            <option {if $item.id_cat==96}SELECTED{/if} value="96">Косметика</option>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td align="left"><span style="color:red;">*</span>&nbsp;Название раздела:</td>
                    <td align="left">
                        <input type="text" name="title" value="{$title}" style="width:100%;"
                               onkeyup="url_convert(this);" onchange="url_convert(this);">
                    </td>
                </tr>

                <tr>
                    <td align="left"><span style="color:red;">*</span>&nbsp;Каноническое имя (латиница):</td>
                    <td align="left">
                        <input type="text" name="url" value="{$url}" style="width:100%;">
                    </td>
                </tr>

                <tr>
                    <td align="left"><span style="color:red;">*</span>&nbsp;Пол:</td>
                    <td align="left">
                        <select name="pol">
                            <option value="M">Мужской</option>
                            <option value="G">Женский</option>
                            <option value="U" selected="">Унисекс</option>
                        </select>
                    </td>
                </tr>


                <tr class="table_bg">
                    <td align="left" colspan="2">
                        &nbsp;&nbsp;Мета тэги
                    </td>
                </tr>

                <tr>
                    <td align="left">&nbsp;TITLE:</td>
                    <td align="left">
                        <input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
                    </td>
                </tr>

                <tr>
                    <td align="left">&nbsp;TITLE_1:</td>
                    <td align="left">
                        <input type="text" name="meta_title_1" value="{$meta_title_1}" style="width:100%">
                    </td>
                </tr>

                <tr>
                    <td align="left" valign="top">&nbsp;DESCRIPTION:</td>
                    <td align="left">
                        <input type="text" name="meta_description" value="{$meta_description}" style="width:100%">
                    </td>
                </tr>
            </table>
        </div>
    </form>
    <input type="button" value="Сохранить" onclick="saveFormCatalog()"/>&nbsp;&nbsp;
    <input type="button" value="Отмена" onclick="closeFormCatalog()"/>
</div>