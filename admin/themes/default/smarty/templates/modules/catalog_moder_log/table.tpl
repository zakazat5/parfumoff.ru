{include file="editor.tpl"}
{include file="calendar.tpl"}


{strip}
    Фильтр по сотрунидкам:
    <select id="select_user">
        <option value="0">Все сотрудники</option>
        {foreach from=$aUser item=user}
            <option {if $sel_user==$user.id}SELECTED="" {/if} value="{$user.id}">{$user.login}</option>
        {/foreach}
    </select>
    <input placeholder="дата 1" name="date1" id="date1" value="{$date_start}">
    <input placeholder="дата 2" name="date2" id="date2" value="{$date_end}">
    <input type="button" value="Применить" id="apply">
    <input type="button" value="Очистить" id="clear">
    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="form">
        <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=order" method="post" enctype="multipart/form-data" name="formMod">
            <!-- Заголовок таблицы данных -->
            <tr align="center" class="table_header">
                <td width="3%" align="center" height="22"></td>
                <td width="3%">
                    <div align="center"><b> Сотрудник </b></div>
                </td>
                <td width="10%">
                    <div align="center"><b>Действие</b></div>
                </td>
                <td>Исх.данные</td>
                <td>Результат</td>
                <td width="" align="left"><b>&nbsp;Артикул</b></td>
                <td width="" align="left"><b>&nbsp;Склад</b></td>
                <td width="" align="left"><b>&nbsp;Артикул склада</b></td>
                <td width="" align="left"><b>&nbsp;Цена в прайсе</b></td>
                <td width="" align="left"><b>&nbsp;Тип+объем</b></td>
                <td width="" align="left"><b>&nbsp;Аромат</b></td>
                <td width="" align="left"><b>&nbsp;</b></td>
                <td width="" align="left"><b>&nbsp;</b></td>

                <td width="13%" align="left"><b>&nbsp;Время обновления</b></td>
            </tr>
            <!-- Заголовок таблицы данных -->
            {if $item}
                <!-- Вывод списка -->
                {section name=i loop=$item}
                    {assign var="user_id" value=`$item[i].user_id`}

                    <tr id="moder_{$item[i].id}" style="background-color:{$item[i].bgcolor};">
                        <td align="center" height="25px"><input type="checkbox" name="id[]"
                                                                value="{$item[i].id}"></nobr></td>
                        <td align="center">{$aUser.$user_id.login}</td>
                        <td align="left">{$item[i].action}</td>
                        <td>{$item[i].source}</td>
                        <td>[{$item[i].pol}] <b>{$item[i].aromat_brand}</b> {$item[i].aromat_title} {$item[i].type} {$item[i].volume}</td>
                        <td>{$item[i].articul}</td>
                        <td>{$item[i].sklad}</td>
                        <td>{$item[i].articul_sklad}</td>
                        <td align="left">{$item[i].price}</td>
                        <td align="left">{$item[i].type} {$item[i].volume}</td>
                        <td align="left"><a href="/admin/index.php?main=content&module=catalog&action=edit&id_cat=82&id={$item[i].aromat_id}">{$item[i].aromat_title}</a></td>
                        <td align="left"><a href="/admin/index.php?main=content&module=catalog&action=catalog_data_order&id_cat=82&id_catalog_data={$item[i].aromat_id}">Модификации</a></td>
                        <td align="left"><a href="/admin/index.php?main=content&module=catalog&action=catalog_data_order_edit&id_cat=82&id_catalog_data={$item[i].aromat_id}&id_order={$item[i].mod_id}">Модификация</a></td>
                        <td>{$item[i].date_add} </td>
                    </tr>
                {/section}
            {else}
                <tr>
                    <td colspan="3"><h3>Товаров на модерацию не найдено.</h3></td>
                </tr>
            {/if}
            <!-- ------------ -->
    </table>
    <!-- Подключение футера -->
    {include file="table_footer.tpl"}
    </form>
{/strip}


{include file="modules/catalog_moder/modal_blacklist.tpl"}

{literal}
    <script>
        $(function () {
            $('#select_user').change(function(){
                var url = updateURLParameter(window.location.href,'sel_user',$(this).val());
                window.location.href = url
            })

            $('#date1').change(function(){
                var url = updateURLParameter(window.location.href,'date_start',$(this).val());
                window.location.href = url
            })

            $('#date2').change(function(){
                var url = updateURLParameter(window.location.href,'date_end',$(this).val());
                window.location.href = url
            })

            $('#clear').click(function(){
                var url = window.location.href;
                var url = updateURLParameter(url,'date_start','');
                var url = updateURLParameter(url,'date_end','');
                window.location.href = url
            })

            $('#apply').click(function(){
                var url = window.location.href;
                var url = updateURLParameter(url,'date_start',$('#date1').val());
                var url = updateURLParameter(url,'date_end',$('#date2').val());
                window.location.href = url
            })
        })


        function updateURLParameter(url, param, paramVal){
            var newAdditionalURL = "";
            var tempArray = url.split("?");
            var baseURL = tempArray[0];
            var additionalURL = tempArray[1];
            var temp = "";
            if (additionalURL) {
                tempArray = additionalURL.split("&");
                for (i=0; i<tempArray.length; i++){
                    if(tempArray[i].split('=')[0] != param){
                        newAdditionalURL += temp + tempArray[i];
                        temp = "&";
                    }
                }
            }

            var rows_txt = temp + "" + param + "=" + paramVal;
            return baseURL + "?" + newAdditionalURL + rows_txt;
        }

        Calendar.setup({
            inputField     	: "date1",
            ifFormat       	: "%Y-%m-%d",
        });

        Calendar.setup({
            inputField     	: "date2",
            ifFormat       	: "%Y-%m-%d"
        });




    </script>

    {/literal}