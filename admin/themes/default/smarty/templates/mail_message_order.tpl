<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="title" content="" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
</head>

<body>

<div id="wrapper">
    <div id="header">
    </div><!-- #header-->


    <div id="content">

        <div style="width:600px">
            <table>
                <tbody>
                <tr>
                    <td valign="bottom" align="left">
                        <h2 style="font:18px/18px Tahoma;font-weight:normal">Здравствуйте, {$fio}!</h2>
                        <font style="font:12px/18px Tahoma">

                            Ваш заказ № {$order_id}<br>

                            {$orders}

                            <strong>Итого:</strong> RUR {$big_sum}.00
                            <br><br>

                            В квитанцию включена стоимость доставки 200руб.
                            <br><br>
                            Ссылка на Вашу квитанцию к заказу <a target="_blank" href="http://www.parfumoff.ru/kvitan/{$order_id}/{$hash}/{$loginHash}">тут</a>.<br />

                            Для оплаты Вам необходимо:<br>

                            -  распечатать квитанцию<br>
                            -  предъявить квитанцию в банке(при себе иметь паспорт)<br><br><br>

                            После оплаты выслать скан или фото оплаченной квитанции с отметкой банка на наш электронный адрес. В письме обязательно указывайте номер заказа.

                            Спасибо за выбор нашего магазина!

                        </font><br>

                        <div style="clear:both;font:12px/18px Tahoma"></div><br><br>
                        телефон для справок: <strong><span>8 (495)</span> 540-47-23</strong>

                    </td>
                </tr>
                </tbody>
            </table>
        </div>

    </div><!-- #content-->




    <div id="footer"></div><!-- #footer --></div><!-- #wrapper -->
</body>
</html>