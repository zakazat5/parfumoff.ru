<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
</head>

<body>

<div id="wrapper">
<div id="header">
</div><!-- #header-->


	<div id="content">
	
		<div style="width:600px">
		
			<table>
				<tr>
					<td width="130" valign="top"><img src="http://enigme.ru/images/default/enigme_l.png"></td>
					<td valign="bottom">
						<h2 style="font: 18px/18px Tahoma;font-weight:normal">Здравствуйте, {$fio}!</h2>
						<font style="font: 12px/18px Tahoma">
						
							Сообщаем  Вам, что заказ №{$order_id} был отправлен <strong>{$date2}</strong>. Номер почтового отправления(почтовый идентификатор) Вашей посылки <strong>{$post_code}</strong>.<br /><br />

							Вы можете отслеживать местонахождение посылки на сайте Почты России <a target="_blank" href="http://russianpost.ru/rp/servise/ru/home/postuslug/trackingpo">http://russianpost.ru/rp/servise/ru/home/postuslug/trackingpo</a><br /><br />

							Доводим до Вашего сведения, что время доставки посылки зависит от работы почты в каждом конкретном регионе и составляет  в среднем от  5 до 14 дней. <br /><br />
							
							По всем интересующим  вопросам просьба обращаться по телефону {$global_phone}.
							С Уважением, <a style="font: 12px/18px Tahoma;color:black" target="_blank" href="http://www.parfumoff.ru/">parfumoff.ru</a>

						</font><br>

					</td>
				</tr>
			</table>

		</div>
		
	</div><!-- #content-->

	
	
	
	<div id="footer"></div><!-- #footer --></div><!-- #wrapper -->
</body>
</html>