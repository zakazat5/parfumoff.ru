{strip}
    <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&id={$smarty.get.id}
	&action=edit" method="post" enctype="multipart/form-data" name="form">

    <div id="tabs" class="flora">
        <ul class="ui-tabs-nav">
            <li class=""><a href="#tab-1"><span>�������� ������</span></a></li>
            <li class=""><a href="#tab-3"><span>��������� ������</span></a></li>
            <li class=""><a href="#tab-4"><span>��������� ����������</span></a></li>
            <li class=""><a href="#tab-5"><span>�����������</span></a></li>
            <li class="ui-tabs-selected"><a href="#tab-6"><span>����</span></a></li>
        </ul>
    </div>

    <div class="ui-tabs-panel ui-tabs-hide" id="tab-1">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������:</td>
                <td align="left" width="80%">
                    <select name="sel_catalog" id="sel_catalog" style="width:100%">
                        <option value="0">-- �������� ������ --</option>
                        {$sel_catalog}
                    </select>
                </td>
            </tr>

            <tr>
                <td align="left"><span style="color:red;">*</span>&nbsp;&nbsp;���:</td>
                <td align="left">
                    <select name="pol">
                        {foreach from=$sel_pol_name item=option key=key}

                            <option {if $sel_pol_id[$key]==$sel_pol_act}SELECTED{/if}    value="{$key}">{$option}</option>
                        {/foreach}
                    </select>&nbsp;
                </td>
            </tr>

            <tr>
                <td align="left"><span style="color:red;">*</span>&nbsp;���������:</td>
                <td align="left">
                    <input type="text" name="title" value="{$title}" style="width:100%" onkeyup="url_convert();"
                           onchange="url_convert();">
                </td>
            </tr>

            <tr>
                <td align="left">
                    <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                        <tr>
                            <td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;��� URL:</td>
                            <td align="right" style="border:0px;">
                                <a href="#"
                                   onmouseover="ddrivetip('��������!��� ������ �������������� ���, �� ������ ���� ������������. ����������� ������������ � �������� ����������� ������ -', 200)"
                                   onmouseout="hideddrivetip()">
                                    <img src="images/warnings.png" width="16" height="16" border="0"></a>
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="left">
                    <input type="text" name="url" value="{$url}" style="width:100%;">
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;TAG:</td>
                <td align="left">
                    <input type="text" name="tag" value="{$tag}" style="width:100%">
                </td>
            </tr>

            <tr class="table_bg">
                <td align="left" colspan="2">
                    &nbsp;&nbsp;���� ����
                </td>
            </tr>

            <tr>
                <td align="left">
                    &nbsp;TITLE:
                </td>
                <td align="left">
                    <input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;TITLE_1:</td>
                <td align="left">
                    <input type="text" name="meta_title_1" value="{$meta_title_1}" style="width:100%">
                </td>
            </tr>

            <tr>
                <td align="left" valign="top">
                    &nbsp;DESCRIPTION:
                </td>
                <td align="left">
                    <input type="text" name="meta_description" value="{$meta_description}" style="width:100%">
                </td>
            </tr>
        </table>
    </div>

    <div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr class="table_bg">
                <td align="left" colspan="2">
                    &nbsp;&nbsp;������ ��������:
                </td>
            </tr>

            <tr>
                <td colspan="2">
                    <textarea id="text" name="text" style="width:100%" rows=20>{$text}</textarea>
                </td>
            </tr>

        </table>
    </div>

    <div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

            <tr>
                <td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������ ����������:</td>
                <td align="left" width="80%">
                    <input type="text" name="sort" value="{$sort}" style="width:50px">
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;�������������:</td>
                <td align="left">
                    <input type="checkbox" name="is_block" {$is_block}>
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;���������� �� �������:</td>
                <td align="left">
                    <input type="checkbox" name="is_main" {$is_main}>
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;� ��������:</td>
                <td align="left">
                    <input type="checkbox" name="is_subs" {$is_subs}>
                </td>
            </tr>

            <tr>
                <td align="left">&nbsp;&nbsp;����������:</td>
                <td align="left">
                    <input type="checkbox" name="is_sale" {$is_sale}>
                </td>
            </tr>


            <tr>
                <td align="left">&nbsp;&nbsp;14 �������:</td>
                <td align="left">
                    <input type="checkbox" name="is_holiday" {$is_holiday}>
                </td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;23 �������:</td>
                <td align="left">
                    <input type="checkbox" name="is_holiday_23" {$is_holiday_23}>
                </td>
            </tr>
            <tr>
                <td align="left">&nbsp;&nbsp;8 �����:</td>
                <td align="left">
                    <input type="checkbox" name="is_holiday_8" {$is_holiday_8}>
                </td>
            </tr>

        </table>
    </div>


    <div class="ui-tabs-panel ui-tabs-hide" id="tab-5">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr>
                <td>�������</td>
                <td>����� 1</td>
                <td>����� 2</td>
                <td>����� 3</td>
                <td>����� 4</td>
                <td>����� 5</td>
                <td>����� 6</td>
                <td>����� 7</td>
                <td>����� 8</td>
                <td>����� 9</td>
                <td>����� 10</td>
                <td>����� 11</td>
                <td>����� 12</td>
                <td>����� 13</td>
                <td>����� 14</td>
                <td>����� 15</td>
                <td>�����</td>
                <td>���</td>
                <td>���</td>
                <td>���� (���)</td>
                <td>���� (�����������)</td>
            </tr>
            {foreach from=$aMod item=mod}
                <tr>
                    <td>{$mod.articul}</td>
                    <td>{$mod.article}  [{$mod.price_usd1}]</td>
                    <td>{$mod.article2} [{$mod.price_usd2}]</td>
                    <td>{$mod.article3} [{$mod.price_usd3}]</td>
                    <td>{$mod.article4} [{$mod.price_usd4}]</td>
                    <td>{$mod.article5} [{$mod.price_usd5}]</td>
                    <td>{$mod.article6} [{$mod.price_usd6}]</td>
                    <td>{$mod.article7} [{$mod.price_usd7}]</td>
                    <td>{$mod.article8} [{$mod.price_usd8}]</td>
                    <td>{$mod.article9}  [{$mod.price_usd9}]</td>
                    <td>{$mod.article10} [{$mod.price_usd10}]</td>
                    <td>{$mod.article11} [{$mod.price_usd11}]</td>
                    <td>{$mod.article12} [{$mod.price_usd12}]</td>
                    <td>{$mod.article13} [{$mod.price_usd13}]</td>
                    <td>{$mod.article14} [{$mod.price_usd14}]</td>
                    <td>{$mod.article15} [{$mod.price_usd15}]</td>
                    <td>{$mod.v}</td>
                    <td>{$mod.type}</td>
                    <td>{$mod.weight}</td>
                    <td>{$mod.color} </td>
                    <td>
                        <div style="float: left">
                            <a id="color_{$mod.id}" class="color" data-id={$mod.id} href="">
                                <img data-default="/admin/images/color_add.png" id="image_{$mod.id}" style="width: 40px;" src="{if $mod.color_image}{$mod.color_image}{else}/admin/images/color_add.png{/if}">
                            </a>
                        </div>
                        <a href="#" onclick="delete_color_image({$mod.id})"><img style="width: 16px;"
                                                                                 src="/admin/images/color_del.png"></a>
                    </td>

                </tr>
            {/foreach}


        </table>
        {include file="catalog_history.tpl"}
    </div>

    <div style="display: block;" class="ui-tabs-panel" id="tab-6">
        {include file="catalog_photo.tpl"}

        {*
        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
        <tr>
            <td align="left" width="20%">&nbsp;&nbsp;�������� ������:</td>
            <td align="left" width="80%">
                <input type="file" name="img" style="width:100%;">
            </td>
        </tr>

        <tr>
            <td align="left" >&nbsp;&nbsp;�������� ���������� (ALT):</td>
            <td align="left" >
                <input type="text" name="alt" style="width:100%" value="{$alt}">
            </td>
        </tr>

        <tr>
            <td align="left" colspan="2">
            &nbsp;&nbsp;
            <a href="/images/uploads/catalog/{$smarty.get.id}/big/{$img}" target="_blank">
            <img src="/images/uploads/catalog/{$smarty.get.id}/small/{$img}" width="140" border="0"></a>
            <input type="hidden" value="{$img}" name="del_img">
            </td>
        </tr>

    </table>
           *}
    </div>


    <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
        <tr class="table_bg_action">
            <td align="left" colspan="2">
                <input type="submit" name="save" value="���������" style="width:100px">&nbsp;
                <input type="submit" name="save_add" value="��������� � �������� ���">&nbsp;
                <input type="submit" name="save_list" value="��������� � ������� � ������">
            </td>
        </tr>
    </table>


    </form>
{/strip}


{literal}
    <script>
        $(function () {


            $("#get_foto").click(function () {

                var qw = $("#qw").attr('value');
                var lang = $("#lang option:selected").val();

                if (qw == '') {
                    alert('������� ������');
                }

                // ������
                $("#last_more").remove();
                $("#msg").html('');


                // ������ ������
                $.post("?ajax=catalog_moder&action=google", {
                    qw: qw,
                    lang: lang
                }, function (xml) {
                    $("message", xml).each(function (id) {

                        message = $("message", xml).get(id);
                        $("#msg").html($("msg", message).text());

                    });
                });
            });


            $("#select_foto").click(function () {
                var image_src = $('#msg input:checked').val();
                console.log(image_src)

                if (image_src == '') {
                    alert('���� �� �������');
                }


                // ������ ������
                $.post("?ajax=catalog_moder&action=upload", {
                    dataType: "json",
                    url: image_src
                }, function (obj) {
                    var obj = JSON.parse(obj);

                    if (typeof obj.error != 'undefined') {
                        alert(obj.error)
                    } else {
                        $('[name="image_path"]').val(obj.path)
                        $('#image1').show();
                        $('#image1').attr('src', obj.path);
                    }


                });

            });

            // ��� ����
            $("#more_foto").livequery("click", function () {

                var start = parseInt($(this).attr('title')) + 1;
                var qw = $("#qw").attr('value');
                var lang = $("#lang option:selected").val();

                if (qw == '') {
                    alert('������� ������');
                }

                // ������
                $("#last_more").hide();
                $("#more_foto").remove();
                $("#last_more").remove();

                // ������ ������
                $.post("?ajax=catalog_moder&action=google", {
                    qw: qw,
                    lang: lang,
                    start: start
                }, function (xml) {
                    $("message", xml).each(function (id) {

                        message = $("message", xml).get(id);
                        $("#msg").append($("msg", message).text());

                    });
                });

            });

            var uploader = new plupload.Uploader({
                runtimes: 'gears,html5,flash,silverlight',
                browse_button: 'upload_image',
                max_file_size: '20mb',
                multiple_queues: false,
                url: '?ajax=catalog_moder&action=upload',
                flash_swf_url: 'public/js/plupload/plupload.flash.swf',
                silverlight_xap_url: 'public/js/plupload/plupload.silverlight.xap',
                filters: [
                    {
                        title: "CSV ������!",
                        extensions: "png,jpg,jpeg"
                    }
                ]
            });

            uploader.init();

            uploader.bind('FilesAdded', function (up, files) {
                /*
                 $.each(files, function(i, file) {
                 $('#filelist').append(
                 '<div id="' + file.id + '">' +
                 file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                 '</div>');
                 });
                 */

                up.refresh();
                uploader.start();
            });

            uploader.bind('UploadProgress', function (up, file) {
                $('#' + file.id + " b").html(file.percent + "%");
            });

            uploader.bind('Error', function (up, err) {
                up.refresh(); // Reposition Flash/Silverlight
            });

            uploader.bind('FileUploaded', function (up, file, info) {
                obj = JSON.parse(info.response);
                console.log(obj.path)

                $('[name="image_path"]').val(obj.path)
                $('#image1').show();
                $('#image1').attr('src', obj.path);

                _filename = info.response;
            });
        })


        <!-- Load Feather code -->


        <!-- Instantiate Feather -->

        var featherEditor = new Aviary.Feather({
            apiKey: 'niayffp54e4yc1wd',
            apiVersion: 3,
            theme: 'dark', // Check out our new 'light' and 'dark' themes!
            tools: 'all',
            appendTo: '',
            onSave: function (imageID, newURL) {
                //�������� ����� ��������
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "?ajax=catalog_moder&action=upload",
                    data: {url: newURL, oldPath: $('[name="image_path"]').val()},
                    success: function (data) {
                        //��������� ��������
                        $('#image1').attr('src', $('#image1').attr('src') + '?' + Math.random())
                        featherEditor.close();
                    }});


            },
            onError: function (errorObj) {
                alert(errorObj.message);
            }
        });


        function launchEditor(id, src) {
            $('[name="image_path"]').val($('#image1').attr('src'))

            featherEditor.launch({
                image: id,
                url: src
            });
            return false;
        }


    </script>
{/literal}






{literal}
    <script>
        $(function () {
            $.each($('.color'), function (i, item) {
                var color = new plupload.Uploader({
                    runtimes: 'gears,html5,flash,silverlight',
                    browse_button: 'color_' + $(item).attr('data-id'),
                    max_file_size: '20mb',
                    multiple_queues: false,
                    multipart_params: {id: $(item).attr('data-id')},
                    url: '?ajax=catalog_k&action=upload_color',
                    flash_swf_url: 'public/js/plupload/plupload.flash.swf',
                    silverlight_xap_url: 'public/js/plupload/plupload.silverlight.xap',
                    filters: [
                        {
                            title: "CSV ������!",
                            extensions: "png,jpg,jpeg"
                        }
                    ]
                });

                color.init();

                color.bind('FilesAdded', function (up, files) {
                    /*
                     $.each(files, function(i, file) {
                     $('#filelist').append(
                     '<div id="' + file.id + '">' +
                     file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                     '</div>');
                     });
                     */

                    up.refresh();
                    color.start();
                });

                color.bind('UploadProgress', function (up, file) {
                    $('#' + file.id + " b").html(file.percent + "%");
                });

                color.bind('Error', function (up, err) {
                    up.refresh(); // Reposition Flash/Silverlight
                });

                color.bind('FileUploaded', function (up, file, info) {
                    obj = JSON.parse(info.response);
                    console.log(obj.path)

                    var id = $(item).attr('data-id')

                    var date = new Date();


                    $('#image_' + id).attr('src', obj.path + '?v=' + date.getTime());

                    _filename = info.response;
                });


            })




        })

        function delete_color_image(id) {
            if (confirm("������� ������ �����?")) {

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "?ajax=catalog_k&action=upload_color",
                    data: {delete: id},
                    success: function (data) {
                        console.log(234)
                        //��������� ��������
                        $('#image_'+id).attr('src',$('#image_'+id).attr('data-default'));

                    }});






            } else {
               return false;
            }

        }

    </script>
{/literal}