{literal}

<script type="text/javascript">

$(document).ready(function(){

	$("#grid_table").flexigrid({
		url: '{/literal}?ajax={$smarty.get.module}&action=mod{literal}',
		dataType: 'json',
		colModel : [
		{display: '����', 		name : 'user_date', width : 100, 	sortable : true, 	align: 'center'},
		{display: '������',		name : 'aromat', 	width : 200, 	sortable : false, 	align: 'left'},
		{display: '���', 		name : 'name', 		width : 100, 	sortable : false, 	align: 'left'},
		{display: 'COMMENT', 	name : 'text', 		width : 'auto', sortable : false, 	align: 'left'},
		],
		buttons : [
		{name: '�������� ���', bclass: 'check', onpress : action},{separator: true},
		{name: '����� ���������', bclass: 'uncheck', onpress : action},{separator: true},{separator: true},
		{name: '�������', bclass: 'delete', onpress : action},{separator: true},
		],
		
		sortname: "user_date",
		sortorder: "desc",
		usepager: true,
		showTableToggleBtn: true,
		width: 'auto',
		height: 'auto',
		useRp: true,
		rpOptions: [20,40],
		rp: 20,
		nowrap: false,
		pagestat: '�������� � {from} �� {to} �� {total} ',
		procmsg: '���������, ����������, ��������� ...',
		nomsg: '��� ���������',
	});
});

function action(com,grid){
	if (com=='�������'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=delete{literal}",
				data: "items="+itemlist,
				success: function(data){
					$("#grid_table").flexReload();
				}});
		}else{
			return false;
		}

	}else if (com=='�������� ���'){
		$('.bDiv tbody tr',grid).addClass('trSelected');

	}else if (com=='����� ���������'){
		$('.bDiv tbody tr',grid).removeClass('trSelected');
	}
}
</script>
{/literal}


<table id="grid_table" class="flexigrid" style="display:none"></table>