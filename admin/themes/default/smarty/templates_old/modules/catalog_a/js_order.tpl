{literal}
<script type="text/javascript">
$(document).ready(function(){
	$("#grid_table").flexigrid({
		url: '{/literal}?ajax={$smarty.get.module}&action=mod&id_cat={$smarty.get.id_cat}&id_catalog_data={$smarty.get.id_catalog_data}{literal}',
		dataType: 'json',
		colModel : [
			{display: 'ID', name : 'id', width : 40, sortable : false, align: 'center'},
			{display: 'ARTICLE', name : 'article', width : 100, sortable : false, align: 'left'},
			{display: '���������', name : 'title', width : 200, sortable : false, align: 'left'},
			{display: '���', name : 'type', width : 100, sortable : false, align: 'left'},
			{display: '����� (��)', name : 'v', width : 50, sortable : false, align: 'left'},
			{display: '���� (RUB)', name : 'price', width : 60, sortable : false, align: 'left'},
			{display: '���� (USD)', name : 'price_usd', width : 60, sortable : false, align: 'left'},
			{display: 'A', name : 'is_block', width : 20, sortable : false, align: 'center'}
			],
		buttons : [
			{name: '�������� ���', bclass: 'check', onpress : test},{separator: true},
			{name: '����� ���������', bclass: 'uncheck', onpress : test},{separator: true},{separator: true},
			{name: '�������', bclass: 'delete', onpress : test},{separator: true},
			{name: '������������', bclass: 'active', onpress : test},{separator: true},
			{name: '�������������', bclass: 'deactive', onpress : test},{separator: true}
			],
		sortname: "id",
		sortorder: "desc",
		usepager: false,
		title: '',
		useRp: false,
		rp: 10,
		showTableToggleBtn: true,
		width: 'auto',
		height: 200
	});
});

function test(com,grid){
    if (com=='�������'){
		if($('.trSelected',grid).length>0){
			var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=delete{literal}",
				data: "items="+itemlist,
				success: function(data){
				$("#grid_table").flexReload();
			}});
		}else{
			return false;
		}
		
     }else if (com=='�������� ���'){
    	$('.bDiv tbody tr',grid).addClass('trSelected');
    
    }else if (com=='����� ���������'){
    	$('.bDiv tbody tr',grid).removeClass('trSelected');
    	
    }else if (com=='������������'){
    	if($('.trSelected',grid).length>0){
    		var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=active{literal}",
				data: "items="+itemlist,
				success: function(data){
				$("#grid_table").flexReload();
			}});
    	}else{
			return false;
		}
		
    }else if (com=='�������������'){
    	if($('.trSelected',grid).length>0){
    		var items = $('.trSelected',grid);
			var itemlist ='';
			for(i=0;i<items.length;i++){itemlist+= items[i].id.substr(3)+",";}
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "{/literal}?ajax={$smarty.get.module}&action=deactive{literal}",
				data: "items="+itemlist,
				success: function(data){
				$("#grid_table").flexReload();
			}});
    	}else{
		return false;
	}
    }
}
</script>
{/literal}