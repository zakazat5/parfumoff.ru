{include file="editor.tpl"}
{strip}
<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&id={$smarty.get.id}
	&action=edit" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class=""><a href="#tab-1"><span>�������� ������</span></a></li>
		<li class=""><a href="#tab-3"><span>��������� ������</span></a></li>
		<li class=""><a href="#tab-4"><span>��������� ����������</span></a></li>
		<li class="ui-tabs-selected"><a href="#tab-5"><span>����</span></a></li>
	</ul>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-1">
<table width="100%" border="0"  cellpadding="5" cellspacing="0" class="form_add">
	<tr >
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������:</td>
		<td align="left" width="80%">
			<select name="sel_catalog" id="sel_catalog" style="width:100%">
				<option value="0">-- �������� ������ --</option>
				{$sel_catalog}
			</select>
		</td>
	</tr>
	
	<tr >
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;���:</td>
		<td align="left" width="80%">
			<select name="sel_type" id="sel_type" style="width:100%">
				<option value="0">-- �������� ��� --</option>
				{$sel_type}
			</select>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;���������:</td>
		<td align="left">
			<input type="text" name="title" value="{$title}" style="width:100%" >
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;���� (RUR):</td>
		<td align="left">
			<input type="text" name="price" value="{$price}" style="width:50px;">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;���� ����
		</td>
	</tr>
	
	<tr>
		<td align="left">
		&nbsp;TITLE:</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;TITLE_1:</td>
		<td align="left">
			<input type="text" name="meta_title_1" value="{$meta_title_1}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left" valign="top">
		&nbsp;DESCRIPTION:</td>
		<td align="left">
			<input type="text" name="meta_description" value="{$meta_description}" style="width:100%" >
		</td>
	</tr>
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;������ ��������:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="text" name="text" style="width:100%" rows=20>{$text}</textarea>
		</td>
	</tr>
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������ ����������:</td>
		<td align="left" width="80%">
			<input type="text" name="sort" value="{$sort}" style="width:50px">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;�������������:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;���������� �� �������:</td>
		<td align="left">
			<input type="checkbox" name="is_main" {$is_main}>
		</td>
	</tr>

</table>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-5">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;�������� ������:</td>
		<td align="left" width="80%">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" >&nbsp;&nbsp;�������� ���������� (ALT):</td>
		<td align="left" >
			<input type="text" name="alt" style="width:100%" value="{$alt}">
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2">
		&nbsp;&nbsp;
		<a href="/images/uploads/catalog/{$smarty.get.id}/big/{$img}" target="_blank">
		<img src="/images/uploads/catalog/{$smarty.get.id}/small/{$img}" width="140" border="0"></a>
		<input type="hidden" value="{$img}" name="del_img">
		</td>
	</tr>
	
</table>
</div>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="���������" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="��������� � �������� ���">&nbsp;
		<input type="submit" name="save_list" value="��������� � ������� � ������">
	</td>
	</tr>
</table>

</form>
{/strip}