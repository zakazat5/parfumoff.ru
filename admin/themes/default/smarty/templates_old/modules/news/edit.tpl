{include file="editor.tpl"}
{strip}

{literal}
<script type="text/javascript">
$(document).ready(function(){
	
	// ����� ������ ����
	$("#sel_brend").change( function () {
		$("#sel_catalog_data").remove();
		$("#msg").html( "������� ... " );
		
		var selected = $("#sel_brend option:selected");

		$.post("{/literal}?ajax={$smarty.get.module}&action=select_catalog_data&id_cat={$smarty.get.id_cat}{literal}", {
			id_brend: selected.val()
		}, function(xml){
			$("message",xml).each(function(id){
				message = $("message",xml).get(id);
				$("#msg").html( $("msg",message).text() );
			});
		});
	});
});
</script>
{/literal}

<form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&id_cat={$smarty.get.id_cat}
	&id={$smarty.get.id}
	&action=edit" method="post" enctype="multipart/form-data" name="form">

<div id="tabs" class="flora">
	<ul class="ui-tabs-nav">
		<li class="ui-tabs-selected"><a href="#tab-1"><span>�������� ������</span></a></li>
		<li class=""><a href="#tab-2"><span>��������� ������</span></a></li>
		<li class=""><a href="#tab-3"><span>��������� ����������</span></a></li>
		<li class=""><a href="#tab-4"><span>����</span></a></li>
	</ul>
</div>

<div style="display: block;" class="ui-tabs-panel" id="tab-1">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;���������:</td>
		<td align="left" width="80%">
			<input type="text" name="title" value="{$title}" style="width:100%" onkeyup="url_convert();" onchange="url_convert();">
		</td>
	</tr>
		
	<tr>
		<td align="left">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" style="border:0px;"><span style="color:red;">*</span>&nbsp;��� URL:</td>
					<td align="right" style="border:0px;">
					<img src="images/warnings.png" width="16" height="16" border="0"
					onmouseover="ddrivetip('��������!��� ������ �������������� ���, �� ������ ���� ������������. ����������� ������������ � �������� ����������� ������ -')" onmouseout="hideddrivetip()">
					</td>
				</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="url" value="{$url}" style="width:100%;">
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;��������������
		</td>
	</tr>
	
	<tr >
		<td align="left" width="20%">&nbsp;&nbsp;�����:</td>
		<td align="left" width="80%">
			<select name="sel_brend" id="sel_brend" style="width:100%">
				<option value="0" >-- �������� ����� --</option>
				{$sel_brend}
			</select>
		</td>
	</tr>
	
	<tr >
		<td align="left" >&nbsp;&nbsp;������:</td>
		<td align="left" >
			<select name="sel_catalog_data" id="sel_catalog_data" style="width:100%">
				<option value="0">-- �������� ������ --</option>
				{$sel_catalog_data}
			</select>
			<div id="msg"></div>
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;���� ����
		</td>
	</tr>
	
	<tr>
		<td align="left">
		&nbsp;TITLE:</td>
		<td align="left">
			<input type="text" name="meta_title" value="{$meta_title}" style="width:100%">
		</td>
	</tr>
	
	<tr>
		<td align="left" valign="top">
		&nbsp;DESCRIPTION:</td>
		<td align="left">
			<input type="text" name="meta_description" value="{$meta_description}" style="width:100%" >
		</td>
	</tr>
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-2">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;���������:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="anot" name="anot" style="width:100%" rows=10>{$anot}</textarea>
		</td>
	</tr>
	
	<tr class="table_bg">
		<td align="left"  colspan="2">
		&nbsp;&nbsp;������ ��������:
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
			<textarea id="text" name="text" style="width:100%" rows=20>{$text}</textarea>
		</td>
	</tr>
	
</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

	<tr>
		<td align="left" width="20%"><span style="color:red;">*</span>&nbsp;������ ����������:</td>
		<td align="left" width="80%">
			<input type="text" name="sort" value="{$sort}" style="width:50px">
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;�������������:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;���������� �� �������:</td>
		<td align="left">
			<input type="checkbox" name="is_main" {$is_main}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;������� ������� ������:</td>
		<td align="left">
			<input type="checkbox" name="is_links" {$is_links}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;�������� ���� ������� ����:</td>
		<td align="left">
			<input type="checkbox" name="is_imgf" {$is_imgf}>
		</td>
	</tr>

</table>
</div>

<div class="ui-tabs-panel ui-tabs-hide" id="tab-4">
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr>
		<td align="left" width="20%">&nbsp;&nbsp;�������� ����:</td>
		<td align="left" width="80%">
			<input type="file" name="img" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" colspan="2">
		&nbsp;&nbsp;
		<a href="/images/uploads/news/{$smarty.get.id}/small/{$img}" target="_blank">
		<img src="/images/uploads/news/{$smarty.get.id}/trumbs/{$img}" width="150" border="0"></a>
		<input type="hidden" value="{$img}" name="del_img">
		</td>
	</tr>
	
</table>
</div>

<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<tr class="table_bg_action">
	<td align="left" colspan="2">
		<input type="submit" name="save" value="���������" style="width:100px">&nbsp;
		<input type="submit" name="save_add" value="��������� � �������� ���">&nbsp;
		<input type="submit" name="save_list" value="��������� � ������� � ������">
	</td>
	</tr>
</table>

</form>

{/strip}