{include file="editor.tpl"}
{include file="calendar.tpl"}
{strip}
<table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
	<form action="
		?main={$smarty.get.main}
		&worker={$smarty.get.module}
		&action=edit
		&id={$smarty.get.id}" 
		method="post" enctype="multipart/form-data" name="form">
	
	<tr>
		<td align="left" bgcolor="#eef1f7" colspan="2">
		&nbsp;&nbsp;�������� ����������
		</td>
	</tr>
	
	<tr>
		<td align="left" width="25%"><span style="color:red;">*</span>&nbsp;������:</td>
		<td align="left">
			<select name="sel_client" style="width:100%">
				<option value="null">-- �������� ������� --</option>
				{$sel_client}
			</select>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;������������:</td>
		<td align="left">
			<select name="sel_shablon" style="width:100%">
				<option value="null">-- �������� ������ --</option>
				{$sel_shablon}
			</select>	
		</td>
	</tr>
	
	<tr>
		<td align="left" valign="top">
		&nbsp;&nbsp;��������� ��������&nbsp;
		<input type="checkbox" name="is_code" onchange="toggleEditor('text');"><br>
		&nbsp;&nbsp;<b>���� �������� ��������, �� �� ����� <br>
		&nbsp;&nbsp;��� ����� �� ����������.</b>
		</td>
		<td align="left">
			<textarea name="text" style="width:100%" rows=10>{$text}</textarea>
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;TITLE:</td>
		<td align="left">
			<input type="text" name="title" value="{$title}" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="border:0px">&nbsp;&nbsp;URL:</td>
				<td align="right" style="border:0px">
					<a href="#" 
					onmouseover="return overlib('
					�������� ���� ������, ��� ���������� ������� ������
					', CAPTION, '����������', LEFT);" 
					onmouseout="return nd();" >
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
				</td>
			</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="url" value="{$url}" style="width:100%;">
		</td>
	</tr>
	
	<tr>
		<td align="left" bgcolor="#eef1f7" colspan="2">
		&nbsp;&nbsp;��������� ����������
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;����������:</td>
		<td align="left">
			<input type="checkbox" name="is_block" {$is_block}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;��������� � ����� ����:</td>
		<td align="left">
			<input type="checkbox" name="is_target" {$is_target}>
		</td>
	</tr>
	
	<tr>
		<td align="left">&nbsp;&nbsp;����������� ������:</td>
		<td align="left">
			<input type="text" name="clicks_end" value="{$clicks_end}" style="width:50px;">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;���� ��������:</td>
		<td align="left">
			<input type="text" name="date1" id="date1" value="{$date1}" />
			&nbsp;<input type="button" id="trigger1" value=" ... ">
		</td>
	</tr>
	
	<tr>
		<td align="left"><span style="color:red;">*</span>&nbsp;������ ����������:</td>
		<td align="left">
			<input type="text" name="date2" id="date2" value="{$date2}" />
			&nbsp;<input type="button" id="trigger2" value=" ... ">
		</td>
	</tr>
	
	<tr>
		<td align="left">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" style="border:0px">&nbsp;&nbsp;����� ����������:</td>
				<td align="right" style="border:0px">
					<a href="#" 
					onmouseover="return overlib('
					�������� ���� ������, ���� �� ������ ���������� ����������
					', CAPTION, '����������', LEFT);" 
					onmouseout="return nd();" >
					<img src="images/warnings.png" width="16" height="16" border="0"></a>
				</td>
			</tr>
			</table>
		</td>
		<td align="left">
			<input type="text" name="date3" id="date3" value="{$date3}" />
			&nbsp;<input type="button" id="trigger3" value=" ... ">
		</td>
	</tr>
	
	<tr>
	<td align="left" colspan="2">
		<input type="submit" name="add" value="��������" style="width:100px">
	</td>
	</tr>
</table>
</form>
{/strip}

{literal}
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "date1",
	ifFormat       :    "%Y-%m-%d %H:%M:%S",
	timeFormat     :    "24",
	button         :    "trigger1",
	singleClick    :    true,
	step           :    1
	});
</script>
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "date2",
	ifFormat       :    "%Y-%m-%d %H:%M:%S",
	timeFormat     :    "24",
	button         :    "trigger2",
	singleClick    :    true,
	step           :    1
	});
</script>
<script type="text/javascript">
	Calendar.setup({
	inputField     :    "date3",
	ifFormat       :    "%Y-%m-%d %H:%M:%S",
	timeFormat     :    "24",
	button         :    "trigger3",
	singleClick    :    true,
	step           :    1
	});
</script>
{/literal}