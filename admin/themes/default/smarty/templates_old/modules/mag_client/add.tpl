{include file="calendar.tpl"}
{strip}

    {literal}
        <script type="text/javascript">
            $(document).ready(function(){

                // �������� ����
                $("#is_email").click( function () {

                var data = $("#email").val();
                $("#is_email").attr('value', '������� ... ');

                $.post("{/literal}?ajax={$smarty.get.module}&action=is_email{literal}", {
                    email: data
                    }, function (xml) {
                        $("message", xml).each(function (id) {
                            message = $("message", xml).get(id);

                            if ($("msg", message).text() == 'err') {
                                $("#is_email").attr('value', 'Email - �����');
                                $("#is_email").css('color', 'red');
                            } else if ($("msg", message).text() == 'err_email') {
                                $("#is_email").attr('value', '����� email ���� �� �����');
                                $("#is_email").css('color', 'red');
                            } else {
                                $("#is_email").attr('value', 'Email - ��������');
                                $("#is_email").css('color', 'blue');
                            }

                        });
                    });
                });

            $('#email').on('focusout',function(){
                console.log('focusout')
                $("#is_email").click();
            })

            // �������� ����� ���������
            $(".submit_form").on("click", function () {

                //check filds
                var error = false;
                var error_text = '';

                if ($('[name="fio"]').val() == '') {
                    error = true;
                    error_text += '��� �� ���������\n';
                }

                if ($('[name="phone"]').val() == '') {
                    error = true;
                    error_text += '���� ������� �� ���������\n';
                }



                if ($('[name=street]').val() == '') {
                    error = true;
                    error_text += '����� �� ��������\n';
                }

                if (error) {
                    alert(error_text);
                    return false;
                }


                var q = $('[name=region]').val() + ' ' + $('[name=city]').val() + ' ' +  $('[name=settlement]').val() + ' ' + $('[name=street]').val() + ' ' + $('[name=house]').val()
                var request = DadataApi.clean(
                        { "structure": [ "ADDRESS" ], "data": [
                            [q]
                        ] }
                )
                request.done(function (msg) {
                    var postal_code = msg.data[0][0].postal_code
                    $('[name=regs_postcode]').val(postal_code)
                    $('[name=address_data]').val(JSON.stringify(msg.data[0][0]));
                    $('form').submit();
                });
                return false;


            });

        });
    </script>
{/literal}
    <form action="
	?main={$smarty.get.main}
	&worker={$smarty.get.module}
	&action=add" method="post" enctype="multipart/form-data" name="form" autocomplete="off">

        <div id="tabs" class="flora">
            <ul class="ui-tabs-nav">
                <li class="ui-tabs-selected"><a href="#tab-1"><span>�������� ������</span></a></li>
                <li class=""><a href="#tab-3"><span>��������� ����������</span></a></li>
            </ul>
        </div>

        <div style="display: block;" class="ui-tabs-panel" id="tab-1">
            <table width="100%" border="0" cellpadding="5" cellspacing="1" class="form_add">

                <tr>
                    <td align="left">E-Mail:&nbsp;</td>
                    <td align="left">
                        <input type="text" name="email" id="email" value="{$email}" style="width:200px">&nbsp;
                        <input type="button" name="is_email" id="is_email" value="���������">
                    </td>
                </tr>

                <tr>
                    <td align="left">������:</td>
                    <td align="left">
                        <input type="password" name="passw" value="" style="width:200px;color:red;font-weight:bold;">
                    </td>
                </tr>

                <tr>
                    <td align="left">��������� ������:</td>
                    <td align="left">
                        <input type="password" name="re_passw" value="" style="width:200px;color:red;font-weight:bold;">
                    </td>
                </tr>

                <tr class="table_bg">
                    <td align="left" colspan="2">
                        � ����
                    </td>
                </tr>

                <tr>
                    <td align="left">���:</td>
                    <td align="left">
                        <input type="text" name="fio" value="{$fio}" style="width:200px">
                    </td>
                </tr>
                <tr>
                    <td align="left">�������:</td>
                    <td align="left">
                        <input type="text" name="phone" value="{$phone}" style="width:200px">
                    </td>
                </tr>

                <tr>
                    <td align="left" width="20%">���</td>
                    <td align="left" width="80%">
                        <select name="pol" style="width:200px">
                            <option value="M">�������</option>
                            <option value="F">�������</option>
                        </select>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">���� ��������:</td>
                    <td align="left">
                        <input type="text" name="date1" id="date1" value="{$smarty.now|date_format:"%Y-%m-%d"}"/>
                        &nbsp;<input type="button" id="trigger1" value=" ... ">
                    </td>
                </tr>

                <tr class="table_bg">
                    <td align="left" colspan="2">
                        �������
                    </td>
                </tr>

                {include file="address.tpl"}

            </table>
        </div>

        <div class="ui-tabs-panel ui-tabs-hide" id="tab-3">
            <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">

                <tr>
                    <td align="left" width="20%">&nbsp;&nbsp;�������������:</td>
                    <td align="left" width="80%">
                        <input type="checkbox" name="is_block" {$data.is_block}>
                    </td>
                </tr>

                <tr>
                    <td align="left" width="20%">&nbsp;&nbsp;������� � ��������:</td>
                    <td align="left" width="80%">
                        <input type="checkbox" name="is_block" {$data.is_maillist}>
                    </td>
                </tr>

            </table>
        </div>

        <table width="100%" border="0" cellpadding="5" cellspacing="0" class="form_add">
            <tr class="table_bg_action">
                <td align="left" colspan="2">
                    <input class="submit_form" type="submit" name="save" value="���������" style="width:100px">&nbsp;
                    <input class="submit_form" type="submit" name="save_add" value="��������� � �������� ���">&nbsp;
                    <input class="submit_form" type="submit" name="save_list" value="��������� � ������� � ������">
                </td>
            </tr>
        </table>

    </form>
{/strip}

{literal}
    <script type="text/javascript">
        Calendar.setup({
            inputField: "date1",
            ifFormat: "%Y-%m-%d",
            timeFormat: "24",
            button: "trigger1",
            singleClick: true,
            step: 1
        });
    </script>
{/literal}