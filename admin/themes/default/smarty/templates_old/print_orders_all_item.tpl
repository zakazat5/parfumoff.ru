<div class="zakaz" style="position: relative;border-bottom:1px dashed black;padding-bottom:40px;margin-bottom:20px;margin-top:20px">
    <table width="100%" cellspacing="0" cellpadding="0" border="1"
           style="width:100%;cellspacing:0cm;border:solid gray 1.0pt;padding-alt:7.5pt 7.5pt 7.5pt 7.5pt">
        <tbody>
        <tr style="height:12.1pt">
            <td width="50%" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;
  height:12.1pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><u><span style="font-size:12.0pt;">����� � {$orders.id}</span></u><u></u></p>
            </td>
            <td width="50%" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;
  height:12.1pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal"><u><span style="font-size:12.0pt;">�������� </span></u><u><span style="font-size:12.0pt;
  ">Enigme.ru</span></u></p>
            </td>
        </tr>
        <tr style="height:40.2pt">
            <td valign="top" style="border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:
  40.2pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                    <span style="font-size:10.0pt;">����������:&nbsp;<b>{$client.fio}</b></span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal">
                    <span style="font-size:10.0pt;">�������:&nbsp;<b>{$client.phone}</b></span>
                </p>
                {if $orders.pComment}
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal">
                        <span style="font-size:10.0pt;">����������� � ������: </span>
        <span style="font-size:9.0pt;
  ">{$orders.pComment}</span>
                    </p>
                {/if}
            </td>
            <td valign="top" style="border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:
  40.2pt">
                {if $orders.is_region == false}
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
          <span style="font-size:10.0pt;">������� �����:&nbsp;
           <b>{$orders.delivery_metro}</b>
          </span>
                    </p>
                {/if}
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
        <span style="font-size:10.0pt;">����� �������� ������:&nbsp;
            <b>
                {if $orders.is_region == false}
                    {$client.fullAddress}
                {elseif $orders.is_region == true}
                    {$client.postcode}, {$client.fullAddress}
                {else}
                    {$client.fullAddress}
                {/if}
            </b>
        </span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
        <span style="font-size:10.0pt;">��� ��������:&nbsp;
            <b>{$orders.delivery_type}</b>
        </span>
                </p>
                {if $orders.is_self == true}
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                    <span style="font-size:10.0pt;">��� ������ ����������:&nbsp;
                        <b>{$orders.delivery_code}</b>
                    </span>
                    </p>
                {/if}
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
        <span style="font-size:10.0pt;">�������� ��:&nbsp;
            <b>{$orders.courier_name}</b>
        </span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
        <span style="font-size:10.0pt;">������ ������:&nbsp;
            <b>{$orders.payment_type}</b>
        </span>
                </p>
            </td>
        </tr>
        </tbody>
    </table>
    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">&nbsp;</p>
    <table width="100%" cellpadding="0" border="1"
           style="width:100%;cellspacing:1.5pt;border:solid gray 1.0pt;padding-alt:3.75pt 3.75pt 3.75pt 3.75pt">
        <tbody>
        <tr color="white" bgcolor="black">
            <td bgcolor="black" width="60%"
                style="width:60.0%;border:none;background-color:#000000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                <p style="background:#000000;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><b><span
                                bgcolor="black"
                                style="background:#000000;font-size:12.0pt;color:white;">������������</span></b></p>
            </td>
            <td bgcolor="black" width="20%"
                style="width:20.0%;border:none;background-color:#000000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                <p style="background:#000000;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                    <b><span style="background:#000000;font-size:12.0pt;color:white;">����������</span></b></p>
            </td>
            <td bgcolor="black" width="20%"
                style="width:20.0%;border:none;background-color:#000000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                <p style="background:#000000;margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                    <b><span style="background:#000000;font-size:12.0pt;color:white;">���������</span></b></p>
            </td>
        </tr>
        {section name=i loop=$item}
            <tr style="border:none;padding:3.75pt 3.75pt 3.75pt 3.75pt;">
                <td width="60%"
                    style="width:60.0%;border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                        <span style="font-size:10.0pt;">{$item[i].title}</span>
                    </p>
                </td>
                <td style="border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span
                                style="font-size:10.0pt;">{$item[i].kol}</span></p>
                </td>
                <td style="border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                        <span style="font-size:10.0pt;">RUR {$item[i].sum}.00</span>
                    </p>
                </td>
            </tr>
        {/section}
        </tbody>
    </table>
    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:12.0pt;">&nbsp;</span>
    </p>
    <table width="100%" cellspacing="0" cellpadding="0" border="1"
           style="width:100%;cellspacing:0cm;border:solid gray 1.0pt;padding-alt:7.5pt 7.5pt 7.5pt 7.5pt">
        <tbody>
        <tr style="yfti-lastrow:yes">
            <td width="50%" valign="top" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">�������:</span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">������, %:</span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">��������� ��������:</span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><b><span
                                style="font-size:14.0pt;">�����:</span></b></p>
            </td>
            <td width="50%" valign="top" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">RUR {$orders.Sum}
                        .00</span></p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span
                            style="font-size:10.0pt;">{$orders.dPercent}</span></p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">RUR {$orders.delivery_cost}
                        .00</span></p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><b><span
                                style="font-size:14.0pt;">RUR {$orders.totalCost}.00
  </span></b></p>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="position: absolute; bottom: 10px; right: 10px;">
        <strong style="display: inline-block; margin-right: 5px;">���� ��������:</strong><span>{$orders.deliveryDate}</span>
    </div>
</div>
<div class="zakaz" style="position: relative; padding-bottom: 20px;">
    <table width="100%" cellspacing="0" cellpadding="0" border="1"
           style="width:100%;cellspacing:0cm;border:solid gray 1.0pt;padding-alt:7.5pt 7.5pt 7.5pt 7.5pt">
        <tbody>
        <tr style="height:12.1pt">
            <td width="50%" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:12.1pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><u><span
                                style="font-size:12.0pt;">����� � {$orders.id}</span></u><u></u></p>
            </td>
            <td width="50%" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:12.1pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><u><span
                                style="font-size:12.0pt;">�������� </span></u><u><span style="font-size:12.0pt;">Enigme.ru</span></u>
                </p>
            </td>
        </tr>
        <tr style="height:40.2pt">
            <td valign="top" style="border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:40.2pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">����������:&nbsp;<b>{$client.fio}</b></span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">�������:&nbsp;<b>{$client.phone}</b></span>
                </p>
                {if $orders.pComment}
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal">
                        <span style="font-size:10.0pt;">����������� � ������: </span>
        <span style="font-size:9.0pt;
  ">{$orders.pComment}</span>
                    </p>
                {/if}
            </td>
            <td valign="top" style="border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt;height:40.2pt">
                {if $orders.is_region == false}
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
              <span style="font-size:10.0pt;">������� �����:&nbsp;
               <b>{$orders.delivery_metro}</b>
              </span>
                    </p>
                {/if}
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
        <span style="font-size:10.0pt;">����� �������� ������:&nbsp;
            <b>
                {if $orders.is_region == false}
                    {$client.fullAddress}
                {elseif $orders.is_region == true}
                    {$client.postcode}, {$client.fullAddress}
                {else}
                    {$client.fullAddress}
                {/if}
            </b>
        </span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
        <span style="font-size:10.0pt;">��� ��������:&nbsp;
            <b>{$orders.delivery_type}</b>
        </span>
                </p>

                {if $orders.is_self == true}
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                    <span style="font-size:10.0pt;">��� ������ ����������:&nbsp;
                        <b>{$orders.delivery_code}</b>
                    </span>
                </p>
                {/if}

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
        <span style="font-size:10.0pt;">�������� ��:&nbsp;
            <b>{$orders.courier_name}</b>
        </span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
        <span style="font-size:10.0pt;">������ ������:&nbsp;
            <b>{$orders.payment_type}</b>
        </span>
                </p>
            </td>
        </tr>
        </tbody>
    </table>
    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">&nbsp;</p>
    <table width="100%" cellpadding="0" border="1"
           style="width:100%;cellspacing:1.5pt;border:solid gray 1.0pt;padding-alt:3.75pt 3.75pt 3.75pt 3.75pt">
        <tbody>
        <tr>
            <td width="60%" style="width:60.0%;border:none;background-color:#000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><b><span
                                style="font-size:12.0pt;color:white;">������������</span></b></p>
            </td>
            <td width="20%" style="width:20.0%;border:none;background-color:#000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                    <b><span style="font-size:12.0pt;color:white;">����������</span></b></p>
            </td>
            <td width="20%" style="width:20.0%;border:none;background-color:#000;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                    <b><span style="font-size:12.0pt;color:white;">���������</span></b></p>
            </td>
        </tr>
        {section name=i loop=$item}
            <tr style="border:none;padding:3.75pt 3.75pt 3.75pt 3.75pt;">
                <td width="60%"
                    style="width:60.0%;border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                        <span style="font-size:10.0pt;">{$item[i].title}</span>
                    </p>
                </td>
                <td style="border:none;background-color:{$item[i].bgcolor};padding:3.75pt 3.75pt 3.75pt 3.75pt">
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span
                                style="font-size:10.0pt;">{$item[i].kol}</span></p>
                </td>
                <td style="border:none;background:white;padding:3.75pt 3.75pt 3.75pt 3.75pt">
                    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal">
                        <span style="font-size:10.0pt;">RUR {$item[i].sum}.00</span>
                    </p>
                </td>
            </tr>
        {/section}
        </tbody>
    </table>
    <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:12.0pt;">&nbsp;</span>
    </p>
    <table width="100%" cellspacing="0" cellpadding="0" border="1"
           style="width:100%;cellspacing:0cm;border:solid gray 1.0pt;padding-alt:7.5pt 7.5pt 7.5pt 7.5pt">
        <tbody>
        <tr style="yfti-lastrow:yes">
            <td width="50%" valign="top" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">�������:</span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">������, %:</span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">��������� ��������:</span>
                </p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><b><span
                                style="font-size:14.0pt;">�����:</span></b></p>
            </td>
            <td width="50%" valign="top" style="width:50.0%;border:none;padding:7.5pt 7.5pt 7.5pt 7.5pt">
                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">RUR {$orders.Sum}
                        .00</span></p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span
                            style="font-size:10.0pt;">{$orders.dPercent}</span></p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><span style="font-size:10.0pt;">RUR {$orders.delivery_cost}
                        .00</span></p>

                <p style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal"><b><span
                                style="font-size:14.0pt;">RUR {$orders.totalCost}.00</span></b></p>
            </td>
        </tr>
        </tbody>
    </table>
    <div style="position: absolute; bottom: 10px; right: 10px;">
        <strong style="display: inline-block; margin-right: 5px;">���� ��������:</strong><span>{$orders.deliveryDate}</span>
    </div>
</div>







