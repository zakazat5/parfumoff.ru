<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
.ttt {border-collapse: collapse;border:1px solid black}
.ttt .left {border-right:3px solid black}
.ttt .bottom {border-bottom:1px solid black}
.ttt td {font-size:12px;line-height:25px;padding:0px 1px 0px 10px;}
.ttt .spacer {padding:0}
.ttt .red {color:red}
.ttt .small {font-size:11px;position:relative;top:-6px}
</style>
</head>
<body>
	<table class="ttt" width="800" border="0" cellspacing="0" cellpadding="1">	
		<tr>
			<td width="120">
			</td><td width="5" class="spacer"></td>
			<td align="right">Форма № ПД-4	</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">Извещение № {$number}</td><td width="5" class="spacer"></td>
			<td class="bottom">ООО «Веб-Коммерц», ИНН/КПП 7723607738/772301001</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td align="center"><span class="small">(наименование получателя платежа)</span>	</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom"><table border="0" cellspacing="0" width="100%"><tr><td width="50%">ВТБ 24 (ЗАО)</td>
			<td  class="last" width="50%"><strong>40702810527000005232</strong></td></tr></table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td ><table border="0" cellspacing="0" width="100%">
		
		<tr>
			<td width="50%" align="center"><span class="small">Наименование банка</span></td>
			<td  width="50%" class="last" align="center"><span class="small">Расчетный счет</span></td></tr></table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom">К/с 30101810100000000716 ,БИК 044525716</td>
			<td>&nbsp;</td>
		</tr>	
	 	<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td><table border="0" cellspacing="0" width="100%"><tr><td width="25%"></td>
			<td  class="last" ><span class="small" >другие банковские реквизиты</span></td></tr></table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom" >{$fio}</td>
			<td>&nbsp;</td>
		</tr>		
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td align="center"><span class="small">ФИО плательщика</span>	</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom">{$postcode}, г.{$city}, {$address}</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom">Оплата заказа {$number} Без НДС</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td align="center"><span class="small">Назначение платежа</span>	</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left" align="center">Кассир
			</td><td width="5" class="spacer"></td>
			<td><table border="0" cellspacing="0" width="100%"><tr><td width="25%">Сумма</td><td class="last bottom" >
				{$big_sum} руб.00 коп.
			</td><td width="25%">&nbsp;</td></tr></table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left"><br />
			</td><td width="5" class="spacer"></td>
			<td><br /></td>
			<td>&nbsp;</td>
		</tr> 
		<tr>
			<td class="left"><br />
			</td><td width="5" class="spacer"></td>
			<td>Подпись плательщика
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><br />
			</td><td width="5" class="spacer"></td>
			<td><br /></td>
			<td>&nbsp;</td>
		</tr>
	</table>
	<table class="ttt" width="800" border="0" cellspacing="0" cellpadding="1">	
		
		<tr>
			<td  width="120" class="left">Извещение № {$number}</td><td width="5" class="spacer"></td>
			<td class="bottom">ООО «Веб-Коммерц», ИНН/КПП 7723607738/772301001</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td align="center"><span class="small">(наименование получателя платежа)</span>	</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom"><table border="0" cellspacing="0" width="100%"><tr><td width="50%">ВТБ 24 (ЗАО)</td>
			<td  class="last" width="50%"><strong>40702810527000005232</strong></td></tr></table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td ><table border="0" cellspacing="0" width="100%"><tr><td width="50%" align="center"><span class="small">Наименование банка</span></td>
			<td  width="50%" class="last" align="center"><span class="small">Расчетный счет</span></td></tr></table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom">К/с 30101810100000000716 ,БИК 044525716</td>
			<td>&nbsp;</td>
		</tr>	
	 	<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td><table border="0" cellspacing="0" width="100%"><tr><td width="25%"></td>
			<td  class="last" ><span class="small" >другие банковские реквизиты</span>	</td></tr></table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom" >{$fio}</td>
			<td>&nbsp;</td>
		</tr>		
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td align="center"><span class="small">ФИО плательщика</span>	</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom">{$postcode}, г.{$city}, {$address}</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td class="bottom">Оплата заказа {$number} Без НДС</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="left">
			</td><td width="5" class="spacer"></td>
			<td align="center"><span class="small">Назначение платежа</span>	</td>
			<td>&nbsp;</td>
		</tr>	
		<tr>
			<td class="left" align="center">Кассир
			</td><td width="5" class="spacer"></td>
			<td><table border="0" cellspacing="0" width="100%"><tr><td width="25%">Сумма</td><td class="last bottom" >
				{$big_sum} руб.00 коп.
			</td><td width="25%">&nbsp;</td></tr></table></td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td class="left"><br />
			</td><td width="5" class="spacer"></td>
			<td><br /></td>
			<td>&nbsp;</td>
		</tr> 
		<tr>
			<td class="left"><br />
			</td><td width="5" class="spacer"></td>
			<td>Подпись плательщика
			</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><br />
			</td><td width="5" class="spacer"></td>
			<td><br /></td>
			<td>&nbsp;</td>
		</tr> 
	</table>

</body>
</html>