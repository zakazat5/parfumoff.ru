<div class="drawers-wrapper">
    	
	<ul class="drawers">
	<li class="drawer">
		<h2 class="drawer-handle content">КОНТЕНТ САЙТА</h2>
		<ul>
			{foreach from=$item_menu item=curr_item key=curr_key}
				{if $curr_item.is_menu == '1'}
					<li class="item_menu sub"><img src="images/header/arrow.gif" width="3" height="5">
						{$curr_item.name}
					</li>
				
				{elseif $curr_item.type eq 'links' or $curr_item.type eq 'exist_cat'}
					<li class="item_menu" >
						&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
						<span style="text-decoration:underline;color:#5e6aa7;">{$curr_item.name}</span>
					</li>
				{else}
					{if $smarty.get.id_cat == $curr_item.id}
					<li class="item_menu act" style="background-color:#f2f5f9;">
					{else}
					<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">
					{/if}
					
					&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
					<a href="?main=content&module={$curr_item.type}&action=mod&id_cat={$curr_item.id}">{$curr_item.name}</a>
					</li>
				{/if}
			{/foreach}
		</ul>
		
		<h2 class="drawer-handle banner">МАГАЗИН</h2>
		<ul>
			{if $smarty.get.module == 'mag_orders'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_orders&action=mod&status=1">Заказы</a>
			</li>
			

			{if $smarty.get.module == 'mag_orders_sklad'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_orders_sklad&action=mod&status=2">Склад</a>
			</li>


			{if $smarty.get.module == 'mag_note'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_note&action=mod">Блокнот</a>
			</li>
			
			{if $smarty.get.module == 'mag_client'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_client&action=mod">Клиенты магазина</a>
			</li>
			
			{if $smarty.get.module == 'mag_comment'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_comment&action=mod">Коментарии</a>
			</li>
			
			{if $smarty.get.module == 'mag_comment_ar'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_comment_ar&action=mod">Коментарии внутреннии</a>
			</li>
		
			<!--{if $smarty.get.module == 'mag'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag&action=mod&status=1">Заказы</a>
			</li>-->
			
			{if $smarty.get.module == 'param'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=param&action=mod">Цены</a>
			</li>
			
			{if $smarty.get.module == 'sklad'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=sklad&action=mod">Склады</a>
			</li>
			
			{if $smarty.get.module == 'mag_market'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_market&action=mod">Ya Market</a>
			</li>
			
			{if $smarty.get.module == 'mag_subs'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=mag&module=mag_subs&action=mod">Рассылка</a>
			</li>

            {if $smarty.get.module == 'catalog_moder_k'}<li class="item_menu act" style="background-color:#f2f5f9;">
            {else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=mag&module=catalog_moder_k&action=mod">Модерация (косметика)</a>
            </li>

            {if $smarty.get.module == 'catalog_moder'}<li class="item_menu act" style="background-color:#f2f5f9;">
            {else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=mag&module=catalog_moder&action=mod">Модерация (парфюмерия)</a>
            </li>

            {if $smarty.get.module == 'catalog_moder'}<li class="item_menu act" style="background-color:#f2f5f9;">
            {else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=mag&module=catalog_moder_log&action=mod">Модерация отчет (парфюмерия)</a>
            </li>


			
		</ul>

        <h2 class="drawer-handle banner">Логистика</h2>
        <ul>
            <li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=logistic&module=logistic&action=index">Управление логистикой</a>
            </li>
            <li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">
                &nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
                <a href="?main=logistic&module=logistic&action=upload">Выгрузки/Загрузки</a>
            </li>
        </ul>
		
		<h2 class="drawer-handle banner">РЕКЛАМА</h2>
		<ul>
			{if $smarty.get.module == 'banner'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=banner&module=banner&action=mod">Управление баннерами</a>
			</li>
			
			{if $smarty.get.module == 'banner_client'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=banner&module=banner_client&action=mod">Клиенты</a>
			</li>
			
			{if $smarty.get.module == 'banner_shablon'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=banner&module=banner_shablon&action=mod">Шаблоны</a>
			</li>
		</ul>
		
		<h2 class="drawer-handle utils">УТИЛИТЫ</h2>
		<ul>
			<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
			<a href="javascript:mcFileManager.open();">Файловый менеджер</a></li>		
		</ul>
		
		<h2 class="drawer-handle permitions">НАСТРОЙКИ</h2>
		<ul>
			
			{if $smarty.get.module == 'cat'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=cat&action=mod">Управление контентом</a>
			</li>
			
			{if $smarty.get.module == 'users'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=users&action=mod">Пользователи админки</a>
			</li>
			
			{if $smarty.get.module == 'capacity'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=capacity&action=mod">Должности</a>
			</li>
			
			{if $smarty.get.module == 'role'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=role&action=mod">Права доступа</a>
			</li>
			 
			{if $smarty.get.module == 'cache'}<li class="item_menu act" style="background-color:#f2f5f9;">
			{else}<li class="item_menu" onmouseover="this.style.backgroundColor='#f2f5f9'" onmouseout="this.style.backgroundColor=''">{/if}
			&nbsp;&nbsp;&nbsp;<img src="images/header/arrow.gif" width="3" height="5">
				<a href="?main=permitions&module=cache&action=mod">Кеш</a>
			</li>
		
		</ul>
	</li>
	</ul>
	
</div>