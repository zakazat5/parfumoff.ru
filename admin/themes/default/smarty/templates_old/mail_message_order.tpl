<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="title" content="" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
</head>

<body>

<div id="wrapper">
<div id="header">
</div><!-- #header-->


	<div id="content">
	
		<div style="width:600px">
		
			<table>
				<tr>
					<td width="130" valign="top"><img src="http://enigme.ru/images/default/enigme_l.png"></td>
					<td valign="bottom">
						<h2 style="font: 18px/18px Tahoma;font-weight:normal">Здравствуйте, {$fio}!</h2>
						<font style="font: 12px/18px Tahoma">
						
							Благодарим вас за выбор интернет-магазина <a style="font: 12px/18px Tahoma;color:black" target="_blank" href="http://enigme.ru/">Enigme.ru</a>
							<br /><br />
							
							Направляем Вам квитанцию для оплаты Вашей покупки: <br /><br />
							Ваш заказ №{$order_id}<br />
							
							{$orders}

							<strong>Итого:</strong> RUR {$big_sum}.00
							<br /><br />

                            Ссылка на Вашу квитанцию к заказу <a target="_blank" href="http://enigme.ru/kvitan/{$order_id}/{$hash}/{$loginHash}">тут</a>.<br />
							<strong>Внимание!</strong> В квитанцию включена стоимость доставки 200руб.
							<br /><br />
							
							<strong></strong>ПОСЛЕ ОПЛАТЫ ВАМ НЕОБХОДИМО ПРИСЛАТЬ НА ЭТУ ЖЕ ПОЧТУ ОТСКАНИРОВАННУЮ (СФОТОГРАФИРОВАННУЮ) КОПИЮ КВИТАНЦИИ. ОБЯЗАТЕЛЬНО СОХРАНЯЙТЕ КВИТАНЦИЮ, ПОДТВЕЖДАЮЩУЮ ПЛАТЕЖ.
							<br /><br />
							
							Для оплаты Вам необходимо:<br />
							
							1)  Распечатать счет во вложении и предъявить в банке.<br />
					     	2)  При оплате иметь при себе паспорт.<br />
							3)  После оплаты внимательно проверьте реквизиты платежа.<br /><br />

							Счет для оплаты через Яндекс.Деньги: 41001363677374<br /><br />

							При возникновении вопросов просим обращаться к менеджерам магазина по тел. {$global_phone}. Или писать на Е-mail office@enigme.ru.<br /><br />

							Благодарим за понимание!<br />
							С уважением, администрация сайта.
							
						</font><br>

					</td>
				</tr>
			</table>

		</div>
		
	</div><!-- #content-->

	
	
	
	<div id="footer"></div><!-- #footer --></div><!-- #wrapper -->
</body>
</html>