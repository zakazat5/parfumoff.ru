<?php
/**
 *  Модуль модерации новых  товаров
 *
 */



if ($_SESSION['admin'] == 'allow'){
    // Создание экземпляра шаблона
    $sm = new Smarty_Admin($valinorConfig['interface.themes']);

    // Определение переменных
    $module 	= "catalog_moder";
    $table 		= "m_catalog_moder";
    $main 		= "content";
    $cat_name 	= 'Модерация ( парфюмерия - черный список )';


    if (isset($_GET['sel_sklad'])) $_SESSION['user_sel_sklad'] = intval($_GET['sel_sklad']);
    if (!isset($_SESSION['user_sel_sklad'])) $_SESSION['user_sel_sklad'] = 0;

    $user_sel_sklad = (int)$_SESSION['user_sel_sklad'];


    $where = " where status='blacklist' ";

    if ($user_sel_sklad>0){
        $where.= " AND sklad=$user_sel_sklad ";
    }

    // Запрос к базе
    $all_items = mysql_result(mysql_query("SELECT count(id) FROM $table $where"),0,0);
    $pages = pages($all_items, 40, 'pages.tpl');
    $start = $pages['start'];
    $limit =40;

    $query = mysql_query("SELECT * "
    ."FROM $table $where "
    ."ORDER BY id DESC "
    ."LIMIT $start, $limit");



    while ($content = mysql_fetch_array($query)){
        $i++;
        if ($i%2==0){
            $content['bgcolor'] = '#f8f8f8';
        }else{
            $content['bgcolor'] = '#f2f2f2';
        }
        $result[] = $content;
    }

    // Подключение заголовка / ACTIONS FORM
    myPrintHeader("$cat_name: <small><small></small></small>", 'news.png');

    // Подключение навигации
    myPrintHeaderNav(array(
        array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
        array("NAME"=>"Контент", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
        array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
    ));

    $sm->assign('item',$result);
    $sm->assign('user_sel_sklad',$user_sel_sklad);
    $sm->assign('pages',$pages['pages']);
    $sm->display("modules/$module/table_blacklist.tpl");
}

