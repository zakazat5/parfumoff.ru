<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$main 		= "mag";
	$module 	= "param";
	$cat_name 	= 'Цены';
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)) 
	err_message("У вас нет прав на просмотр.");
	
	// параметры магазина
	$sm->assign('p_usd', MAG_P_KURS);
	$sm->assign('p_price_1', MAG_P_PRICE_1);
	$sm->assign('p_price_2', MAG_P_PRICE_2);
	$sm->assign('p_price_3', MAG_P_PRICE_3);
	$sm->assign('p_price_4', MAG_P_PRICE_4);
	$sm->assign('p_price_5', MAG_P_PRICE_5);
	$sm->assign('p_price_6', MAG_P_PRICE_6);
	$sm->assign('p_price_7', MAG_P_PRICE_7);
	
	
	myPrintHeader("$cat_name", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Вывод шаблона
	$sm->display("modules/$module/table.tpl");
}
?>