<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	// Определение переменных
	$main 		= 'mag';
	$module 	= 'mag_xml';
	$cat_name 	= 'Выгрузка данных';
	
	// Проверка прав доступа

	// Подключение заголовка
	myPrintHeader("$cat_name: <small><small>XML</small></small>", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	$sm->display("modules/$module/table.tpl");
}

?>