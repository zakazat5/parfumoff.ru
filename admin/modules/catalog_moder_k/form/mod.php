<?php
/**
 *  Модуль модерации новых  товаров
 *
 */



if ($_SESSION['admin'] == 'allow'){
    // Создание экземпляра шаблона
    $sm = new Smarty_Admin($valinorConfig['interface.themes']);

    // Определение переменных
    $module 	= "catalog_moder_k";
    $table 		= "m_catalog_moder";
    $main 		= "content";
    $cat_name 	= 'Модерация (Косметика)';


    if (isset($_GET['sel_sklad'])) $_SESSION['user_sel_sklad'] = intval($_GET['sel_sklad']);
    if (!isset($_SESSION['user_sel_sklad'])) $_SESSION['user_sel_sklad'] = 0;

    $user_sel_sklad = (int)$_SESSION['user_sel_sklad'];


    $where = ' where block!=1 AND id_cat=96 ';

    if ($user_sel_sklad>0){
        $where.= " AND sklad=$user_sel_sklad ";
    }

    // Запрос к базе
    $all_items = mysql_result(mysql_query("SELECT count(id) FROM $table $where"),0,0);
    $pages = pages($all_items, 40, 'pages.tpl');
    $start = $pages['start'];
    $limit =40;



    $query = mysql_query("SELECT * "
    ."FROM $table $where "
    ."ORDER BY id DESC "
    ."LIMIT $start, $limit");



    $i=0;
    while ($content = mysql_fetch_array($query)){
        $i++;
        if ($i%2==0){
            $content['bgcolor'] = '#f8f8f8';
        }else{
            $content['bgcolor'] = '#f2f2f2';
        }

        $result[] = $content;
    }

    // Подключение заголовка / ACTIONS FORM

    myPrintHeader("$cat_name", 'news.png', array(
        array("IMG" => "blacklist.png", "ALT" => "Черный лист", "LINK" => "?main=$main&module=$module&action=blacklist&id_cat=$id_cat"),
        array("IMG" => "problem.png", "ALT" => "Проблемные товары", "LINK" => "?main=$main&module=$module&action=problem&id_cat=$id_cat"),
    ));

    // Подключение навигации
    myPrintHeaderNav(array(
        array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
        array("NAME"=>"Контент", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
        array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
    ));





    $sm->assign('item',$result);
    $sm->assign('user_sel_sklad',$user_sel_sklad);
    $sm->assign('pages',$pages['pages']);
    $sm->display("modules/$module/table.tpl");
}

