<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$main 		= "mag";
	$module 	= "sklad";
	$cat_name 	= 'Склады';
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)) 
	err_message("У вас нет прав на просмотр.");
	
	$i = 1;
	$query = mysql_query("SELECT * FROM m_catalog_sklad ");
	while ($content = mysql_fetch_array($query)){

		$item[] = array(
		'id' => $content['id'],
		'title' => $content['title'],
		'priority_val' => $content['priority_val'],
		'date' => $content['date'],
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		);
		$i++;
	}
	$sm->assign('item',$item);
	
	myPrintHeader("$cat_name", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Вывод шаблона
	$sm->display("modules/$module/table.tpl");
}
?>