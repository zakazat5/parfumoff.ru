<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	// Определение переменных
	$main 		= 'mag';
	$module 	= 'mag_market';
	$cat_name 	= 'Выгрузка данных';
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)) 
	err_message("У вас нет прав на просмотр.");
	
	// Подключение заголовка
	myPrintHeader("$cat_name: <small><small>YANDEX</small></small>", 'news.png');


    $task = $db->fetchRow($db->select()->from('tasks')->where('id=1'));


    $sm->assign('task',$task);



	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	$sm->display("modules/$module/table.tpl");
}

?>