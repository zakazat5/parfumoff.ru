<?php
if ($_SESSION['admin'] == 'allow') {
    $cat_module = $_GET['worker'];
    $cat_main = $_GET['main'];

    // Проверка прав доступа
    if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $cat_module))
        err_message("У вас нет прав на просмотр.");

    switch ($_GET['action']) {

        case "yandex":

            $strAll = '<?xml version="1.0" encoding="utf-8"?>';
            $strAll .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">';
            $strAll .= '<yml_catalog date="' . Date("Y-m-d H:i") . '">';
            $strAll .= "<shop>";
            $strAll .= "<name>Parfumoff.ru</name>";
            $strAll .= '<company>ООО "Смарт Студио" ОГРН 1167746299346 Юридический адрес: г. Москва, ул. Николоямская, д. 48, стр. 2 </company>';
            $strAll .= "<url>http://www." . BASE_URL . "</url>";

            $strTmp = "<currencies>";
            $strTmp .= '<currency id="RUR" rate="1"/>';
            $strTmp .= "</currencies>";
            $strAll .= $strTmp;


            // BREND
            $q_brend = mysql_query("SELECT * FROM m_catalog WHERE is_block=0 AND id != 106 AND id != 83 AND id != 176 AND id != 460");
            while ($c_brend = mysql_fetch_array($q_brend)) {


                // CATALOG
                $q_data = mysql_query("SELECT * FROM m_catalog_data WHERE is_block=0 AND id_catalog='{$c_brend['id']}' ");
                while ($c_data = mysql_fetch_array($q_data)) {

                    //ORDERS
                    $q_order = mysql_query("SELECT * FROM m_catalog_data_order WHERE id_catalog_data='{$c_data['id']}' AND price!=0 ");
                    while ($c_order = mysql_fetch_array($q_order)) {

                        $brend_title = eregi_replace('[^a-z0-9_ -]', '', format_text_out($c_brend['title']));
                        $data_title = eregi_replace('[^a-z0-9_ -]', '', format_text_out($c_data['title']));

                        if ($c_order['type'] == 'N') {
                            $data_title .= ' ' . $c_order['v'];
                        } else {
                            $data_title .= ' ' . $aroma_type[$c_order['type']] . ' ' . $c_order['v'] . ' мл';
                        }

                        $data_url = 'http://www.' . BASE_URL . "/production/{$c_brend['url']}/{$c_data['url']}/order/{$c_order['articul']}/?utm_source=YandexMarket&amp;utm_medium=cpc&amp;utm_campaign=Parfumoff_Market&amp;utm_term={$c_brend['url']}-{$c_data['url']}";
                        $data_img = 'http://www.' . BASE_URL . '/images/uploads/catalog/' . $c_data['id'] . '/small/' . $c_data['img'];
                        //$data_name = yandex_text2xml( $brend_title .' '. $data_title );
                        $data_desc = yandex_text2xml(strip_tags(html_entity_decode(format_text_out($c_data['text']))), true);

                        if ($c_data['pol'] == 'F') {
                            $data_categoryId = 2;
                        } else {
                            $data_categoryId = 3;
                        }
                        if ($c_data['pol'] == 'F') {
                            $data_pol = 'Женский';
                        } else {
                            $data_pol = 'Мужской';
                        }

                        $m_list_data .= '<offer id="' . $c_order['id'] . '" type="vendor.model" available="true" >';

                        $m_list_data .= "<url>{$data_url}</url>";
                        $m_list_data .= "<price>{$c_order['price']}</price>";
                        $m_list_data .= "<currencyId>RUR</currencyId>";
                        $m_list_data .= "<categoryId>{$data_categoryId}</categoryId>";
                        $m_list_data .= "<picture>{$data_img}</picture>";
                        $m_list_data .= "<delivery>true</delivery>";
                        $m_list_data .= "<vendor>{$brend_title}</vendor>";
                        $m_list_data .= "<model>{$data_title}</model>";
                        $m_list_data .= "<description>{$data_desc}</description>";

                        //if($c_brend['country'] != '') 	$m_list_data .= "<country_of_origin>{$c_brend['country']}</country_of_origin>";
                        if ($data_pol != '') $m_list_data .= "<param name=\"Пол\">{$data_pol}</param>";
                        if ($c_data['p_year'] != '') $m_list_data .= "<param name=\"Год выпуска\">{$c_data['p_year']}</param>";
                        if ($c_data['p_fam'] != '') $m_list_data .= "<param name=\"Семейства\">{$c_data['p_fam']}</param>";
                        if ($c_data['p_tag'] != '') $m_list_data .= "<param name=\"Присутствуют ноты\">{$c_data['p_tag']}</param>";


                        $m_list_data .= "</offer>";

                    }
                }
            }


            $strAll .= '<categories><category id="1">Парфюмерия</category><category id="2" parentId="1">Женская</category><category id="3" parentId="1">Мужская</category></categories>';

            $strAll .= "<offers>";
            $strAll .= $m_list_data;
            $strAll .= "</offers>";

            $strAll .= "</shop>";
            $strAll .= "</yml_catalog>";

            print_r(mysql_error());


            if ($fp = @fopen($_SERVER["DOCUMENT_ROOT"] . "/tmp/export_yandex.xml", 'wb')) {
                @fwrite($fp, $strAll);
                @fclose($fp);
                $load = 'ya';
            } else {
                $load = 0;
            }

            go("?main=$cat_main&module=$cat_module&action=mod&load=$load");
            break;

        case 'yandex_new';
            $db->update('tasks',['status'=>1,'date_start'=>time()],'id=1 and status=0');
            go("?main=$cat_main&module=$cat_module&action=mod&load=$load");
            break;

    }

}


function yandex_replace_special($arg)
{
    if (in_array($arg[0], array("&quot;", "&amp;", "&lt;", "&gt;")))
        return $arg[0];
    else
        return " ";
}

function yandex_text2xml($text, $bHSC = false)
{
    if ($bHSC)
        $text = htmlspecialchars($text);
    $text = ereg_replace("[\x1-\x8\xB-\xC\xE-\x1F]", "", $text);
    $text = ereg_replace("'", "&apos;", $text);
    return $text;
}

?>