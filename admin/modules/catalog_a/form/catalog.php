<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	$id_cat = intval($_GET['id_cat']);
	
	// Определение переменных
	$module = "catalog_a";
	$table = "m_catalog";
	$main = "content";
	$cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);


	// первый уровень
	$q1 = mysql_query("SELECT * FROM $table WHERE id_cat='$id_cat' and id_parent=0 ORDER BY sort, title");
	while ($c1 = @mysql_fetch_array($q1)){
		$item[] = array(
		'id' => $c1['id'],
		'id_parent' => $c1['id_parent'],
		'title' => $c1['title'],
		'url' => $c1['url'],
		'sort' => $c1['sort'],
		'is_block' => ($c1['is_block'] == 0) ? 'green.gif' : 'red.gif',
		'is_top' => ($c1['is_top'] == 0) ? '-' : '+',
		);
	}
	$sm->assign('item',$item);
	
	// второй уровень
	$q2 = mysql_query("SELECT * FROM $table WHERE id_cat='$id_cat' and id_parent !=0 ORDER BY sort desc, title");
	while ($c2 = @mysql_fetch_array($q2)){
		$item_sub[] = array(
		'id' => $c2['id'],
		'id_parent' => $c2['id_parent'],
		'title' => $c2['title'],
		'url' => $c2['url'],
		'sort' => $c2['sort'],
		'is_block' => ($c2['is_block'] == 0) ? 'green.gif' : 'red.gif',
		);
	}
	$sm->assign('item_sub',$item_sub);

	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name", 'news.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=catalog_add&id_cat=$id_cat")
	));

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1"),
	array("NAME"=>"Формирование каталога", "LINK"=>"", "ACTIVE"=>"0")
	));

	// Список действий.
	$sm->assign('footer_id', array(
	'delete_item_catalog',
	'active_catalog',
	'active_top'
	));

	$sm->assign('footer_names', array(
	'Удалить',
	'Активировать / Заблокировать',
	'TOP  да/нет'
	));
	$sm->assign('footer_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/catalog_table.tpl");
}
?>