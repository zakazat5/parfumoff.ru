<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	$id_cat = intval($_GET['id_cat']);

	// Определение переменных
	$module 	= "catalog_a";
	$table 		= "m_catalog_data";
	$main 		= "content";
	$cat_name 	= mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);

	$where = "WHERE id_cat='$id_cat' ";

	// Запрос к базе
	$all_items = mysql_result(mysql_query("SELECT count(id) FROM m_catalog_data $where"),0,0);
	$pages = pages($all_items, $_SESSION['user_cpage'], 'pages.tpl');
	$start = $pages['start'];
	$limit = $_SESSION['user_cpage'];

	$query = mysql_query("SELECT * "
	."FROM m_catalog_data $where "
	."ORDER BY sort "
	."");

	$i = $start + 1;
	while ($content = mysql_fetch_array($query)){
		$catalog_name = @mysql_result(@mysql_query("SELECT title FROM m_catalog WHERE id='{$content['id_catalog']}'"),0,0);

		$item[] = array(
		'id' => $content['id'],
		'title' => $content['title'],
		'img' => $content['img'],
		'price' => $content['price'],
		'catalog_name' => $catalog_name,
		'is_block' => ($content['is_block'] == 0) ? 'green.gif' : 'red.gif',
		'is_main' => ($content['is_main'] == 1) ? '+' : '-',
		'sort' => $content['sort'],
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		'n' => $i
		);

		$i++;
	}

	$sm->assign('item',$item);
	$sm->assign('all_items',$all_items);
	$sm->assign('pages',$pages['pages']);


	myPrintHeader("$cat_name", 'news.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=add&id_cat=$id_cat"),
	array("IMG" => "catalog.gif", "ALT" => "Сформировать каталог", "LINK" => "?main=$main&module=$module&action=catalog&id_cat=$id_cat")
	));

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1")
	));

	// Список действий.
	$sm->assign('footer_id', array(
	'delete_item',
	'active_item_catalog',
	'main_item_catalog'
	));

	$sm->assign('footer_names', array(
	'Удалить',
	'Активировать / Заблокировать',
	'На главную (убрать/поставить)'
	));
	$sm->assign('footer_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/table.tpl");
}
?>