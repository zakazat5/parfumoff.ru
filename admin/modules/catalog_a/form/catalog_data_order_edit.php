<?php
if ($_SESSION['admin'] == 'allow'){
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$id_cat = intval($_GET['id_cat']);
	$id_catalog_data = intval($_GET['id_catalog_data']);
	$id_order = intval($_GET['id_order']);
	
	$cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);
	$catalog_data = @mysql_result(mysql_query("SELECT title FROM m_catalog_data WHERE id='$id_catalog_data'"),0,0);
	
	$module = "catalog_a";
	$main = "content";

	// Запрос к базе
	$query = mysql_query("SELECT * FROM m_catalog_data_order WHERE id='$id_order' LIMIT 1");
	while ($content = mysql_fetch_array($query)){
		$title  = $content['v'] .' - '. $aroma_type["{$content['type']}"];

		$sm->assign('article', $content['article']);
		$sm->assign('sel_type', $aroma_type);
		$sm->assign('sel_type_active', $content['type']);
		
		$sm->assign('price', $content['price']);
		$sm->assign('price_usd', $content['price_usd']);
		$sm->assign('v', $content['v']);
		$sm->assign('is_block', ($content['is_block'] == 1) ? 'checked' : '');
		$sm->assign('is_def', ($content['is_def'] == 1) ? 'checked' : '');
		
	}

	myPrintHeader("$cat_name: <small><small>Редактирование [$catalog_data] - $title</small></small>", 'news.png');
	
	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1"),
	array("NAME"=>"[$catalog_data]", "LINK"=>"", "ACTIVE"=>"0"),
	array("NAME"=>"Формирование вариантов", "LINK"=>"?main=$main&module=$module&action=catalog_data_order&id_cat=$id_cat&id_catalog_data=$id_catalog_data", "ACTIVE"=>"1")
	));

	$sm->display("modules/$module/catalog_data_order_edit.tpl");
}
?>