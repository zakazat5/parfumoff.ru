<?php
if ($_SESSION['admin'] == 'allow'){
	$id_cat 	= intval($_GET['id_cat']);
	$cat_module = $_GET['worker'];
	$cat_main 	= $_GET['main'];
	$cat_name 	= mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);

	switch ($_GET['action']){

		//==========================================================+
		// добавление карточки каталога
		//==========================================================+
		case "add":

			// проверка на пустоту
			if ($_POST['sel_catalog'] == '0'
			or $_POST['title'] == ''
			or $_POST['price'] == ''
			){err_message("Не заполнены обязательные поля.");}

			// основыне данные
			$sel_catalog 	= $_POST['sel_catalog'];
			$sel_type 		= $_POST['sel_type'];
			$title 			= format_text(trim($_POST['title']));
			$price 			= intval($_POST['price']);

			// META TAG
			if (isset($_POST['is_meta_title'])) $is_meta_title = 1; else $is_meta_title = 0;
			if (isset($_POST['is_meta_description'])) $is_meta_description = 1; else $is_meta_description = 0;
			if ($is_meta_title) $meta_title = trim($_POST['meta_title']); else $meta_title = $title;
			//if ($is_meta_description) $meta_description = trim($_POST['meta_description']); else $meta_description = HTMLToTxt( maxsite_str_word($_POST['anot'], 50) );
			$meta_description = trim($_POST['meta_description']);
			$meta_title_1 = trim($_POST['meta_title_1']);

			// параметры публикации
			$sort = trim($_POST['sort']);
			if (isset($_POST['is_block'])){$is_block = 1;}else{$is_block = 0;}
			if (isset($_POST['is_main'])){$is_main = 1;}else{$is_main = 0;}

			// текстовые данные
			$text 	= ($_POST['text']);
			$alt 	= trim($_POST['alt']);

			$sql = "INSERT INTO m_catalog_data
			(id_cat, id_catalog, id_type, title, text, img, alt, registerDate, 
			is_block, is_main, sort, price,
			meta_title, meta_title_1, meta_description)
			VALUES (
			'$id_cat', '$sel_catalog', '$sel_type', '$title', '$text', '$img', '$alt', NOW(),
			'$is_block', '$is_main', '$sort', '$price',
			'$meta_title', '$meta_title_1', '$meta_description')";

			mysql_query($sql);
			$id_catalog = DGetLast();

			// добавляем модификацию
			$sql = "INSERT INTO m_catalog_data_order (id_catalog_data, type, v, price, is_block )
			VALUES ('$id_catalog', 'A', '$title', '$price', '1' )";
			mysql_query($sql);

			// Превью
			if ($_FILES['img']['name']!=''){

				// формирование директорий
				$path 			= '../images/';
				$mkdir 			= 'uploads/catalog/'.$id_catalog;
				$mkdir_trumbs 	= $mkdir .'/trumbs/';
				$mkdir_small 	= $mkdir .'/small/';
				$mkdir_big 		= $mkdir .'/big/';

				if (!file_exists($path.$mkdir)){
					mkdir($path.$mkdir);
					mkdir($path.$mkdir_trumbs);
					mkdir($path.$mkdir_small);
					mkdir($path.$mkdir_big);
				}

				$time = time();

				$img =	upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_small,  $valinorConfig['catalog.small.w'] ,  $valinorConfig['catalog.small.w']);
				upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_big,  $valinorConfig['catalog.big.w'] ,  $valinorConfig['catalog.big.w']);

			}else {
				$img = '';
			}

			// переход
			if (isset($_POST['save']))
			go("?main=$cat_main&module=$cat_module&action=edit&id_cat=$id_cat&id=$id_catalog");
			elseif (isset($_POST['save_add']))
			go("?main=$cat_main&module=$cat_module&action=add&id_cat=$id_cat");
			elseif (isset($_POST['save_list']))
			go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");

			break;

			//==========================================================+
			// редактирование карточки товара
			//==========================================================+
		case "edit":

			// проверка на пустоту
			if ($_POST['sel_catalog'] == '0'
			or $_POST['title'] == ''
			){err_message("Не заполнены обязательные поля.");}

			$id_catalog = $_GET['id'];

			// основыне данные
			$sel_catalog 	= $_POST['sel_catalog'];
			$sel_type 		= $_POST['sel_type'];
			$title 			= format_text(trim($_POST['title']));
			$price 			= intval($_POST['price']);

			// Meta
			$meta_title = trim($_POST['meta_title']);
			$meta_title_1 = trim($_POST['meta_title_1']);
			$meta_description = $_POST['meta_description'];

			// параметры публикации
			$sort = trim($_POST['sort']);
			if (isset($_POST['is_block'])){$is_block = 1;}else{$is_block = 0;}
			if (isset($_POST['is_main'])){$is_main = 1;}else{$is_main = 0;}

			// текстовые данные
			$text 	= ($_POST['text']);
			$alt 	= trim($_POST['alt']);

			// Превьюшки
			if ($_FILES['img']['name']!=''){
				@unlink('../images/uploads/catalog/'.$id_catalog. '/trumbs/' .$_POST['del_img']);
				@unlink('../images/uploads/catalog/'.$id_catalog. '/small/' .$_POST['del_img']);
				@unlink('../images/uploads/catalog/'.$id_catalog. '/big/' .$_POST['del_img']);

				// формирование директорий
				$path 			= '../images/';
				$mkdir 		= 'uploads/catalog/'.$id_catalog;
				$mkdir_trumbs 	= $mkdir .'/trumbs/';
				$mkdir_small 		= $mkdir .'/small/';
				$mkdir_big 		= $mkdir .'/big/';

				if (!file_exists($path.$mkdir)){
					mkdir($path.$mkdir);
					mkdir($path.$mkdir_trumbs);
					mkdir($path.$mkdir_small);
					mkdir($path.$mkdir_big);
				}

				$time = time();
				$img =	upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_small,  $valinorConfig['catalog.small.w'] ,  $valinorConfig['catalog.small.w']);
				upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_big,  $valinorConfig['catalog.big.w'] ,  $valinorConfig['catalog.big.w']);

			}else{
				$img = $_POST['del_img'];
			}

			$sql = "UPDATE m_catalog_data SET
			id_catalog='$sel_catalog', id_type='$sel_type',
			title='$title', text='$text', img='$img', alt='$alt',
			is_block='$is_block', is_main='$is_main', sort='$sort', price='$price',
			meta_title='$meta_title',meta_title_1='$meta_title_1',meta_description='$meta_description'
			WHERE id='$id_catalog' ";
			mysql_query($sql);
			
			// добавляем модификацию
			$sql = "UPDATE m_catalog_data_order SET
			v='$title', 
			price='$price' 
			WHERE id_catalog_data='$id_catalog' ";
			mysql_query($sql);

			// переход
			if (isset($_POST['save']))
			go("?main=$cat_main&module=$cat_module&action=edit&id_cat=$id_cat&id=$id_catalog");
			elseif (isset($_POST['save_add']))
			go("?main=$cat_main&module=$cat_module&action=add&id_cat=$id_cat");
			elseif (isset($_POST['save_list']))
			go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");

			break;

			//==========================================================+
			// добавление раздела каталога
			//==========================================================+
		case "catalog_add":
			if ($_POST['title'] == ''
			or $_POST['sort'] == ''
			or $_POST['url'] == ''
			){err_message("Не заполнены обязательные поля.");}

			//  проверка урла
			$url = translit($_POST['url']);
			$is_url = mysql_result(mysql_query("SELECT count(id) FROM m_catalog WHERE url='$url' AND id_cat='$id_cat' "),0,0);
			if ($is_url != 0) err_message("$url - такое каноническое имя уже существует в базе. Выберите другое или подредактируйте вручную");

			// основные переменные
			$title = format_text($_POST['title']);
			$anot = htmlentities($_POST['anot']);
			$sort = trim($_POST['sort']);
			if (isset($_POST['is_block'])){$is_block = 1;}else{$is_block = 0;}
			if (isset($_POST['is_top'])){$is_top = 1;}else{$is_top = 0;}

			// META TAG
			if (isset($_POST['is_meta_title'])) $is_meta_title = 1; else $is_meta_title = 0;
			if (isset($_POST['is_meta_description'])) $is_meta_description = 1; else $is_meta_description = 0;
			if ($is_meta_title) $meta_title = trim($_POST['meta_title']); else $meta_title = $title;
			//if ($is_meta_description) $meta_description = trim($_POST['meta_description']); else $meta_description = HTMLToTxt( maxsite_str_word($_POST['anot'], 50) );
			$meta_description = trim($_POST['meta_description']);
			$meta_title_1 = trim($_POST['meta_title_1']);

			// валидность принадлежности
			$sel_catalog = intval($_POST['sel_catalog']);
			if ($sel_catalog != 0) {
				$is_level = mysql_result(mysql_query("SELECT id_parent FROM m_catalog WHERE id='$sel_catalog'"),0,0);
				if ($is_level != 0) err_message("Не допускается вложенность больше первого уровня.");
			}

			$sql = "INSERT INTO m_catalog (id, id_cat, id_parent, title, anot, url, sort, is_block, is_top,
			meta_title, meta_title_1, meta_description)
			VALUES ('','$id_cat', '$sel_catalog', '$title', '$anot','$url', '$sort', '$is_block', '$is_top',
			'$meta_title', '$meta_title_1', '$meta_description')";

			mysql_query($sql);
			$id_catalog = DGetLast();

			// переход
			if (isset($_POST['save']))
			go("?main=$cat_main&module=$cat_module&action=catalog_edit&id_cat=$id_cat&id_catalog=$id_catalog");
			elseif (isset($_POST['save_add']))
			go("?main=$cat_main&module=$cat_module&action=catalog_add&id_cat=$id_cat");
			elseif (isset($_POST['save_list']))
			go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");
			break;

			//==========================================================+
			// редактирование раздела каталога
			//==========================================================+
		case "catalog_edit":
			if ($_POST['title'] == ''
			or $_POST['sort'] == ''
			or $_POST['url'] == ''
			){err_message("Не заполнены обязательные поля.");}
			$id_catalog = intval($_GET['id_catalog']);

			// проверка урла
			$url = translit($_POST['url']);
			$old_url = mysql_result(mysql_query("SELECT url FROM m_catalog WHERE id='$id_catalog'  "),0,0);
			if ($old_url != $url) {
				$is_url = mysql_result(mysql_query("SELECT count(id) FROM m_catalog WHERE url='$url' AND id_cat='$id_cat' "),0,0);
				if ($is_url != 0) err_message("$url - такое каноническое имя уже существует в базе.");
			}

			// основыные перем
			$title = format_text($_POST['title']);
			$anot = htmlentities($_POST['anot']);
			$sort = trim($_POST['sort']);
			if (isset($_POST['is_block'])) $is_block = 1; else$is_block = 0;
			if (isset($_POST['is_top'])){$is_top = 1;}else{$is_top = 0;}

			// Meta
			$meta_title = trim($_POST['meta_title']);
			$meta_title_1 = trim($_POST['meta_title_1']);
			$meta_description =$_POST['meta_description'];

			// валидность принадлежности
			$sel_catalog = intval($_POST['sel_catalog']);
			if ($sel_catalog != 0) {
				$is_level = mysql_result(mysql_query("SELECT id_parent FROM m_catalog WHERE id='$sel_catalog'"),0,0);
				if ($is_level != 0) err_message("Не допускается вложенность больше первого уровня.");
			}

			$sql = "UPDATE m_catalog SET
			id_parent='$sel_catalog',
			title='$title',
			anot='$anot',
			url='$url',
			sort='$sort',
			is_block='$is_block',
			is_top='$is_top',
			meta_title='$meta_title',
			meta_title_1='$meta_title_1',
			meta_description='$meta_description'
			WHERE id='$id_catalog'";
			mysql_query($sql);

			// переход
			if (isset($_POST['save']))
			go("?main=$cat_main&module=$cat_module&action=catalog_edit&id_cat=$id_cat&id_catalog=$id_catalog");
			elseif (isset($_POST['save_add']))
			go("?main=$cat_main&module=$cat_module&action=catalog_add&id_cat=$id_cat");
			elseif (isset($_POST['save_list']))
			go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");
			break;

			//==========================================================+
			// добавление варианта цены
			//==========================================================+
		case "catalog_data_order_add":
			if ($_POST['v'] == ''
			or $_POST['price_usd'] == ''
			){err_message("Не заполнены обязательные поля.");}

			$id_catalog_data 	= $_GET['id_catalog_data'];
			$article 			= $_POST['article'];
			$sel_type 			= $_POST['sel_type'];
			$v 					= intval($_POST['v']);
			if (isset($_POST['is_block'])){$is_block = 1;}else{$is_block = 0;}
			if (isset($_POST['is_def'])){$is_def = 1;}else{$is_def = 0;}

			$price_usd = ( str_replace(",",".", $_POST['price_usd']) );
			$price = catalog_creat_price($price_usd);

			$sql = "INSERT INTO m_catalog_data_order (id_catalog_data, article, type, v, price, price_usd, is_block, is_def)
			VALUES ('$id_catalog_data', '$article', '$sel_type', '$v', '$price', '$price_usd', '$is_block', '$is_def')";
			mysql_query($sql);
			$id_order = DGetLast();


			// меняем диапазон
			$pr = mysql_fetch_array(mysql_query("SELECT MAX(price) as max_price, MIN(price) as min_price FROM m_catalog_data_order WHERE id_catalog_data='$id_catalog_data' "));
			mysql_query("UPDATE m_catalog_data SET price_s='{$pr['min_price']}', price_e='{$pr['max_price']}' WHERE id='$id_catalog_data' ");


			// переход
			if (isset($_POST['save']))
			go("?main=$cat_main&module=$cat_module&action=catalog_data_order_edit&id_cat=$id_cat&id_catalog_data=$id_catalog_data&id_order=$id_order");
			elseif (isset($_POST['save_add']))
			go("?main=$cat_main&module=$cat_module&action=catalog_data_order_add&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
			elseif (isset($_POST['save_list']))
			go("?main=$cat_main&module=$cat_module&action=catalog_data_order&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
			break;


			//==========================================================+
			// редактирование варианта цены
			//==========================================================+
		case "catalog_data_order_edit":
			if ($_POST['v'] == ''
			or $_POST['price_usd'] == ''
			){err_message("Не заполнены обязательные поля.");}

			$id_catalog_data 	= $_GET['id_catalog_data'];
			$article 			= $_POST['article'];
			$id_order 			= $_GET['id_order'];
			$sel_type 			= $_POST['sel_type'];
			$v 					= intval($_POST['v']);
			if (isset($_POST['is_block'])){$is_block = 1;}else{$is_block = 0;}
			if (isset($_POST['is_def'])){$is_def = 1;}else{$is_def = 0;}

			$price_usd = ( str_replace(",",".", $_POST['price_usd']) );
			$price = catalog_creat_price($price_usd);

			$sql = "UPDATE m_catalog_data_order SET
			type='$sel_type', 
			article='$article', 
			v='$v', 
			price='$price', 
			price_usd='$price_usd', 
			is_block='$is_block',
			is_def='$is_def'
			WHERE id='$id_order'";
			mysql_query($sql);

			// меняем диапазон
			$pr = mysql_fetch_array(mysql_query("SELECT MAX(price) as max_price, MIN(price) as min_price FROM m_catalog_data_order WHERE id_catalog_data='$id_catalog_data' "));
			mysql_query("UPDATE m_catalog_data SET price_s='{$pr['min_price']}', price_e='{$pr['max_price']}' WHERE id='$id_catalog_data' ");

			// переход
			if (isset($_POST['save']))
			go("?main=$cat_main&module=$cat_module&action=catalog_data_order_edit&id_cat=$id_cat&id_catalog_data=$id_catalog_data&id_order=$id_order");
			elseif (isset($_POST['save_add']))
			go("?main=$cat_main&module=$cat_module&action=catalog_data_order_add&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
			elseif (isset($_POST['save_list']))
			go("?main=$cat_main&module=$cat_module&action=catalog_data_order&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
			break;

		case "galarey_foto":
			//==========================================================+
			// добавление фото
			//==========================================================+
			if (isset($_POST['foto_add'])){
				if ($_POST['sort'] == ''
				or $_POST['title'] == ''
				){err_message("Не заполнены обязательные поля.");}

				$id_catalog_data = intval($_GET['id_catalog_data']);

				$title 		= format_text(trim($_POST['title']));
				$anot 		= htmlentities($_POST['anot']);
				$sort 		= trim($_POST['sort']);
				if (trim($_POST['alt']) == '') $alt = format_text(trim($_POST['title']));
				else $alt = format_text(trim($_POST['alt']));

				if ($_FILES['img']['name']!=''){
					// формирование директорий
					$path 			= '../images/';
					$mkdir 			= 'uploads/catalog/'.$id_catalog_data;
					$mkdir_trumbs 	= $mkdir .'/trumbs/';
					$mkdir_small 	= $mkdir .'/small/';
					$mkdir_big 		= $mkdir .'/big/';

					if (!file_exists($path.$mkdir)){
						mkdir($path.$mkdir);
						mkdir($path.$mkdir_trumbs);
						mkdir($path.$mkdir_small);
						mkdir($path.$mkdir_big);
					}

					// уплоад
					$time = time();
					$img =	upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_small,  $valinorConfig['galarey.small.w'] ,  $valinorConfig['galarey.small.w']);
					upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_big,  $valinorConfig['galarey.big.w'] ,  $valinorConfig['galarey.big.w']);
				}else{
					err_message("Не заполнены обязательные поля.");
				}

				$sql = "INSERT INTO m_catalog_galarey_data
				(id_catalog_data, title, alt, anot, img, sort)
				VALUES ('$id_catalog_data', '$title', '$alt', '$anot', '$img', '$sort')";
				mysql_query($sql);

				go("?main=$cat_main&module=$cat_module&action=galarey_foto&id_cat=$id_cat&id_catalog_data=$id_catalog_data");


				//==========================================================+
				//  редактирование фото
				//==========================================================+
			}elseif (isset($_POST['foto_edit'])){
				if ($_POST['sort'] == ''
				or $_POST['title'] == ''
				){err_message("Не заполнены обязательные поля.");}

				$id_catalog_data = intval($_GET['id_catalog_data']);
				$id_foto 	= intval($_GET['id_foto']);

				$title 		= format_text(trim($_POST['title']));
				$alt 		= format_text(trim($_POST['alt']));
				$anot 		= format_text(trim($_POST['anot']));
				$sort 		= trim($_POST['sort']);

				if ($_FILES['img']['name']!='') {
					// формирование директорий
					$path 			= '../images/';
					$mkdir 			= 'uploads/catalog/'.$id_catalog_data;
					$mkdir_trumbs 	= $mkdir .'/trumbs/';
					$mkdir_small 	= $mkdir .'/small/';
					$mkdir_big 		= $mkdir .'/big/';

					if (!file_exists($path.$mkdir)){
						mkdir($path.$mkdir);
						mkdir($path.$mkdir_trumbs);
						mkdir($path.$mkdir_small);
						mkdir($path.$mkdir_big);
					}

					// Удаление старых фото
					@unlink($path.$mkdir_trumbs.$_POST['img_name']);
					@unlink($path.$mkdir_small.$_POST['img_name']);
					@unlink($path.$mkdir_big.$_POST['img_name']);

					$time = time();
					$img =	upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_small,  $valinorConfig['galarey.small.w'] ,  $valinorConfig['galarey.small.w']);
					upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_big,  $valinorConfig['galarey.big.w'] ,  $valinorConfig['galarey.big.w']);

				}else{
					$img = $_POST['img_name'];
				}

				$sql = "UPDATE m_catalog_galarey_data SET
				img='$img', 
				sort='$sort', 
				title='$title', 
				alt='$alt', 
				anot='$anot' 
				WHERE id='$id_foto'";

				mysql_query($sql);
				go("?main=$cat_main&module=$cat_module&action=galarey_foto&id_cat=$id_cat&id_catalog_data=$id_catalog_data");
			}
			break;

		case "delete_comment":
			$id = $_GET['id'];
			@mysql_query("DELETE FROM m_catalog_com WHERE id='$id'");
			go("index.php");
			break;

		case "order":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'DELETE', $id_cat)) err_message("У вас нет прав на удаление и блокировку записей.");

			if ($_POST['actions'] == 'null'){err_message("Не выбрано действие.");}
			//if (!isset($_POST['id']) and !isset($_POST['sel_total'])){err_message("Не выбраны элементы.");}

			// Удаление записей
			if ($_POST['actions'] == 'delete_item'){
				foreach ($_POST['id'] as $key=>$value){

					// подчищаем картинки и галерею
					delete_directory("../images/uploads/catalog/".$value);
					@mysql_query("DELETE FROM m_catalog_galarey WHERE id_catalog_data='$value'");
					@mysql_query("DELETE FROM m_catalog_galarey_data WHERE id_catalog_data='$value'");
					@mysql_query("DELETE FROM m_catalog_data_order WHERE id_catalog_data='$value'");

					// удаляем запись каталога
					@mysql_query("DELETE FROM m_catalog_data WHERE id='$value' ");
				}
				go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");


				// удаление раздела каталога
			}elseif ($_POST['actions'] == 'delete_item_catalog'){
				foreach ($_POST['id'] as $key=>$value){
					$is_parent = mysql_result(mysql_query("SELECT count(id) FROM m_catalog WHERE id_parent='$value'"),0,0);
					if($is_parent != 0){
						err_message("Раздел содержит вложенные элементы, удалите сначала их.");
					}else{
						//delete_directory("../images/uploads/catalog/".$value);
						@mysql_query("DELETE FROM m_catalog WHERE id='$value'");
						//@mysql_query("DELETE FROM m_catalog_data WHERE id_catalog='$value'");
						//@mysql_query("DELETE FROM m_catalog_galarey WHERE id_catalog='$value'");
					}
				}
				go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");

				// удаление фото в наборе
			}elseif ($_POST['actions'] == 'delete_item_galarey_foto'){
				$id_galarey = $_GET['id'];
				$id_catalog_data = $_GET['id_catalog_data'];

				foreach ($_POST['id'] as $key=>$value){
					$del_img = mysql_result(mysql_query("SELECT img FROM m_catalog_galarey_data WHERE id='$value'"),0,0);

					@unlink("../images/uploads/catalog/". $id_catalog_data ."/trumbs/". $del_img);
					@unlink("../images/uploads/catalog/". $id_catalog_data ."/small/". $del_img);
					@unlink("../images/uploads/catalog/". $id_catalog_data ."/big/". $del_img);

					mysql_query("DELETE FROM m_catalog_galarey_data WHERE id='$value'");
				}
				go("?main=$cat_main&module=$cat_module&action=galarey_foto&id_cat=$id_cat&id_catalog_data=$id_catalog_data&id=$id_galarey");

				// активация каталога
			}elseif ($_POST['actions'] == 'active_catalog'){
				foreach ($_POST['id'] as $key=>$value) {
					$is = mysql_result(mysql_query("SELECT is_block FROM m_catalog WHERE id='$value'"),0,0);
					if ($is == 0) {
						mysql_query("UPDATE m_catalog SET is_block=1 WHERE id='$value'");
					}else{
						mysql_query("UPDATE m_catalog SET is_block=0 WHERE id='$value'");
					}
				}
				go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");

				// активация каталога TOP
			}elseif ($_POST['actions'] == 'active_top'){
				foreach ($_POST['id'] as $key=>$value) {
					$is = mysql_result(mysql_query("SELECT is_top FROM m_catalog WHERE id='$value'"),0,0);
					if ($is == 0) {
						mysql_query("UPDATE m_catalog SET is_top=1 WHERE id='$value'");
					}else{
						mysql_query("UPDATE m_catalog SET is_top=0 WHERE id='$value'");
					}
				}
				go("?main=$cat_main&module=$cat_module&action=catalog&id_cat=$id_cat");

			}elseif ($_POST['actions'] == 'active_item_catalog'){
				foreach ($_POST['id'] as $key=>$value) {
					$is = mysql_result(mysql_query("SELECT is_block FROM m_catalog_data WHERE id='$value'"),0,0);
					if ($is == 0) {
						mysql_query("UPDATE m_catalog_data SET is_block=1 WHERE id='$value'");
					}else{
						mysql_query("UPDATE m_catalog_data SET is_block=0 WHERE id='$value'");
					}
				}
				go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");

			}elseif ($_POST['actions'] == 'main_item_catalog'){
				foreach ($_POST['id'] as $key=>$value) {
					$is = mysql_result(mysql_query("SELECT is_main FROM m_catalog_data WHERE id='$value'"),0,0);
					if ($is == 0) {
						mysql_query("UPDATE m_catalog_data SET is_main=1 WHERE id='$value'");
					}else{
						mysql_query("UPDATE m_catalog_data SET is_main=0 WHERE id='$value'");
					}
				}
				go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");
			}
			break;
	}
}
?>