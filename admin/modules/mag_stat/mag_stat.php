<?php
if ($_SESSION['admin'] == 'allow') {
    $cat_module = $_GET['worker'];
    $cat_main = $_GET['main'];


    switch ($_GET['action']) {

        case "send":
            $DateStart = trim($_POST['date1']);
            $DateEnd = trim($_POST['date2']);
            $email = trim($_POST['email']);

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                err_message('Неверный e-mail');
            }
            if ($DateStart == '') {
                err_message('Начальная дата не указана');
            }
            if ($DateEnd == '') {
                err_message('Конечная дата не указана');
            }
            $filename = 'sekret_data_' . generate_password_simple(4) . '.csv';

            // REPORT TYPE
            if ($_POST['stat_type'] == 'standart') {
                doStandartReport($filename);
            } elseif ($_POST['stat_type'] == 'manager') {
                doManagerReport($filename);
            } elseif ($_POST['stat_type'] == 'ccause') {
                doCCauseReport($filename);
            } elseif ($_POST['stat_type'] == 'source') {
                doSourceReport($filename);
            }
            /* -=========================================
            Отправка
            */
            require_once 'Zend/Mail.php';


            $mail = new Zend_Mail('utf-8');

            $at = $mail->createAttachment(file_get_contents("../images/uploads/$filename"));
            $at->filename = $filename;

            $mail->setBodyHtml('Отчет');
            $mail->setFrom('office@parfumoff.ru', 'parfumoff');
            $mail->addTo($email, 'parfumoff');
            $mail->setSubject("parfumoff.ru - отчет за $DateStart - $DateEnd");
            $mail->send();

            @unlink("../images/uploads/$filename");


            // переход
            if (isset($_POST['save'])) {
                go("?main=$cat_main&module=$cat_module&action=mod");
            }
            break;


        case "edit":

            // переход
            if (isset($_POST['save'])) {
                go("?main=$cat_main&module=$cat_module&action=edit&id_client=$id_client");
            } elseif (isset($_POST['save_add'])) {
                go("?main=$cat_main&module=$cat_module&action=add");
            } elseif (isset($_POST['save_list'])) {
                go("?main=$cat_main&module=$cat_module&action=mod");
            }
            break;

    }

}

function doManagerReport($filename)
{
    global $DateStart, $DateEnd;
    $csv = new CCSVData();

    $columns = [
        'Дата',
        'Аккаунт',
        'Всего заказов (кол-во)',
        'Всего заказов (сумма)',
        'Подтвержденные (кол-во)',
        'Подтвержденные (сумма)',
        'В обработке (кол-во)',
        'В обработке (сумма)',
        'Доставленные (кол-во)',
        'Доставленные (сумма)',
        'Отклоненные (кол-во)',
        'Отклоненные (сумма)',
        'Заказы МСК %',
        'Заказы МСК сумма',
        'Заказы Регион %',
        'Заказы Регион Сумма',
        'Заказы по телефону %',
        'Заказы по телефону Сумма',
        'Заказы через сайт %',
        'Заказы через сайт сумма',
        'Кол-во проданных услуг',
        'Код услуги 001 (кол-во)',
        'Код услуги 002 (кол-во)',
    ];



    $csv->SaveFile("../images/uploads/$filename", $columns,'utf8','cp1251');
    $csv->SaveFile("../images/uploads/$filename", array(
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
    ));
    $result = mysql_query("SELECT  `id_manager`,
                                                `login`,
                                                `status`,
                                            	IF(`m_mag_Orders`.`id_user`=0, `pRegion`, `m_mag_Users`.`region`) AS `out_region`,
                                            	`is_phone`,
                                            	COUNT(`m_mag_Orders`.`id`) AS orders_overall,
                                            	SUM(`m_mag_Orders`.`dSum`) AS sum_overall
                                            FROM m_mag_Orders
                                            	LEFT JOIN `base_users` ON `base_users`.`id` = m_mag_Orders.`id_manager`
                                            	LEFT JOIN `m_mag_Users` ON `m_mag_Users`.`id` =  m_mag_Orders.`id_user`
                                            WHERE 	`m_mag_Orders`.DateAdd BETWEEN '" . date('Y-m-d H:i:s',
            strtotime($DateStart)) . "' AND '" . date('Y-m-d H:i:s', strtotime($DateEnd)) . "'
                                                AND `id_manager`!=0 AND `status` IN (0,1,2,3,4)
                                            GROUP BY id_manager, `status`, `out_region`, `is_phone`;
                                            ");
    $managers = array();
    while ($row = mysql_fetch_assoc($result)) {
        $managers[$row['id_manager']]['name'] = $row['login'];
        $managers[$row['id_manager']]['orders_overall'] += $row['orders_overall'];
        $managers[$row['id_manager']]['sum_overall'] += $row['sum_overall'];
        $managers[$row['id_manager']]['status_' . $row['status'] . '_overall'] += $row['orders_overall'];
        $managers[$row['id_manager']]['status_' . $row['status'] . '_sum'] += $row['sum_overall'];

        /*$managers[$row['id_manager']][$row['out_region'].'_overall'] += ($row['status']!=2) ? 0 : $row['orders_overall'];
        $managers[$row['id_manager']][$row['out_region'].'_sum'] += ($row['status']!=2) ? 0 :  $row['sum_overall'];
        $managers[$row['id_manager']]['phone_'.$row['is_phone'].'_overall'] += ($row['status']!=2) ? 0 :  $row['orders_overall'];
        $managers[$row['id_manager']]['phone_'.$row['is_phone'].'_sum'] += ($row['status']!=2) ? 0 :  $row['sum_overall'];*/

        // убрал привязку по ($row['status']!=2) ? 0 :
        $managers[$row['id_manager']][$row['out_region'] . '_overall'] += $row['orders_overall'];
        $managers[$row['id_manager']][$row['out_region'] . '_sum'] += $row['sum_overall'];
        $managers[$row['id_manager']]['phone_' . $row['is_phone'] . '_overall'] += $row['orders_overall'];
        $managers[$row['id_manager']]['phone_' . $row['is_phone'] . '_sum'] += $row['sum_overall'];

    }
    @mysql_free_result($result);
    $result = mysql_query("SELECT `id_manager`,
                                                    `login`,
                                                    `status`,
                                                    COUNT(`m_mag_OrdersSum`.`id`) AS `service_sum`,
                                                    `articul`
                                           FROM m_mag_Orders
                                            	LEFT JOIN `base_users` ON `base_users`.`id` = m_mag_Orders.`id_manager`
                                            	LEFT JOIN `m_mag_OrdersSum` ON `m_mag_OrdersSum`.`id_order` = m_mag_Orders.`id`
                                            	LEFT JOIN `m_mag_Users` ON `m_mag_Users`.`id` =  m_mag_Orders.`id_user`
                                                LEFT JOIN `m_catalog_data` ON  `m_catalog_data`.`id` = `m_mag_OrdersSum`.`id_catalog_data`
                                            WHERE `m_mag_Orders`.DateAdd BETWEEN '" . date('Y-m-d H:i:s',
            strtotime($DateStart)) . "' AND '" . date('Y-m-d H:i:s', strtotime($DateEnd)) . "'
                                                 AND `id_manager`!=0 AND `status` IN (2)
                                                 AND `articul` IN ('PF0032028')
                                            GROUP BY `id_manager`, `articul`");

    $services = array();
    while ($row = mysql_fetch_assoc($result)) {
        $services[$row['id_manager']][$row['articul']] = $row['service_sum'];
    }
    @mysql_free_result($result);
    foreach ($managers as $manager_id => $manager) {
        $approved_orders = $manager['moscow_overall'] + $manager['region_overall'];
        $approved_orders_sum = $manager['moscow_sum'] + $manager['region_sum'];

        $columns = [
            date('H:i:s d.m.Y', strtotime($DateStart)) . '-' . date('H:i:s d.m.Y', strtotime($DateEnd)),
            $manager['name'],
            intval($manager['orders_overall']),
            intval($manager['sum_overall']),
            intval($manager['status_2_overall']),
            intval($manager['status_2_sum']),
            intval($manager['status_4_overall']),
            intval($manager['status_4_sum']),
            intval($manager['status_3_overall']),
            intval($manager['status_3_sum']),
            intval($manager['status_0_overall']),
            intval($manager['status_0_sum']),
            round($manager['moscow_overall'] / $approved_orders * 100, 2),
            intval($manager['moscow_sum']),
            round($manager['region_overall'] / $approved_orders * 100, 2),
            intval($manager['region_sum']),
            round($manager['phone_1_overall'] / $approved_orders * 100, 2),
            intval($manager['phone_1_sum']),
            round($manager['phone_0_overall'] / $approved_orders * 100, 2),
            intval($manager['phone_0_sum']),
            intval(array_sum($services[$manager_id])),
            intval($services[$manager_id]['PF0032028']),
            0
        ];



        $csv->SaveFile("../images/uploads/$filename",$columns,'utf8','cp1251');
    }

    @mysql_free_result($result);
    $result = mysql_query("SELECT 	`id_manager`,
									`login`,
									`m_mag_Orders`.`DateAdd`,
									`dSum`,
									`Sum`,
									`status`,
									`m_mag_Orders`.`id`
							FROM m_mag_Orders
									LEFT JOIN `base_users` ON `base_users`.`id` = m_mag_Orders.`id_manager`
							WHERE 	`m_mag_Orders`.DateAdd BETWEEN '" . date('Y-m-d H:i:s',
            strtotime($DateStart)) . "' AND '" . date('Y-m-d H:i:s', strtotime($DateEnd)) . "'
									AND `id_manager`!=0 AND `status` IN (0,1,2,3,4)
							");

    $csv->SaveFile("../images/uploads/$filename", array(
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        ''
    ));



    $csv->SaveFile("../images/uploads/$filename", array(
        'Аккаунт',
        'Статус заказа',
        'Дата заказа',
        'Номер заказа',
        'Сумма заказа',
        'Сумма заказа со скидкой'
    ),'utf8','cp1251');

    while ($row = mysql_fetch_assoc($result)) {

        $status = '';

        if ($row['status'] == 0) {
            $status = 'Отклонён';
        } elseif ($row['status'] == 3) {
            $status = 'Доставлен';
        } elseif ($row['status'] == 1) {
            $status = 'Новый';
        } elseif ($row['status'] == 2) {
            $status = 'Подтверждённый';
        } elseif ($row['status'] == 4) {
            $status = 'В обработке';
        } elseif ($row['status'] == 5) {
            $status = 'Отклонённый на складе';
        } elseif ($row['status'] == 6) {
            $status = 'Неполный';
        }

        $csv->SaveFile("../images/uploads/$filename", array(
            $row['login'],
            $status,
            date('H:i:s d.m.Y', strtotime($row['DateAdd'])),
            intval($row['id']),
            intval($row['Sum']),
            intval($row['dSum'])
        ),'utf8','cp1251');
    }
}

function doStandartReport($filename)
{
    global $DateStart, $DateEnd;
    // FILTER
    $filter = $_POST['filter'];
    if ($filter == 'region') {
        $where_filter = " AND mu.region='region' ";
    } else {
        $where_filter = "";
    }


    $csv = new CCSVData();

    $columns = [
        '№ заказа',
        'ID клинта',
        'ФИО',
        'Позиций в заказе',
        'Дата перевода',
        'Дата доставки',
        'ARTICUL',
        'Наименование',
        'Склад',
        'A1',
        'A2',
        'A3',
        'A4',
        'A5',
        'A6',
        'A7',
        'A8',
        'A9',
        'A10',
        'Кол-во одной ед.',
        'Цена одной ед.',
        'Сумма без скидки',
        'Сумма со скидкой',
        '% скидки',
        'Тип скидки',
        'Распродажа?',
        'Комиссия',
        'Потери на скидках Сергей',
        'Наценка на товар',
        'Расход на курьера',
        'Доход Сергей',
    ];




    $csv->SaveFile("../images/uploads/$filename", $columns,'utf8','cp1251');
    $csv->SaveFile("../images/uploads/$filename", array(
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
    ));

    /* -=========================================
    SQL
    */
    $result = mysql_query("
    				SELECT 
    					mo.*, UNIX_TIMESTAMP(mo.DateClose) AS DateClose, UNIX_TIMESTAMP(mo.DateEnd) AS DateEnd
    				FROM m_mag_Orders mo
    				LEFT JOIN m_mag_Users mu ON mu.id=mo.id_user
    				WHERE mo.status=3 AND (mo.DateClose>='$DateStart' AND mo.DateClose<='$DateEnd' ) {$where_filter}
    				ORDER BY mo.id DESC
    			");

    /* -=========================================
    DATA
    */
    while ($content = @mysql_fetch_array($result)) {


        //get user for take region/moscow
        if ($content['id_user'] != 0) {
            $c_user = mysql_fetch_assoc(mysql_query("SELECT * FROM m_mag_Users WHERE id='{$content['id_user']}' LIMIT 1 "));
            $content['prefix'] = ($c_user['region'] == 'region') ? 'RPF' : 'MPF';
        } else {
            $content['prefix'] = ($content['pRegion'] == 'region') ? 'RPF' : 'MPF';
        }

        // Клиент
        if ($content['id_user'] == 0) {
            $client_fio = $content['pFio'];
            $client_id = 0;
        } else {
            $client_fio = mysql_result(mysql_query("SELECT fio FROM m_mag_Users WHERE id='{$content['id_user']}'"), 0,
                0);
            $client_id = $content['id_user'];
        }


        $all_items = mysql_result(mysql_query("SELECT count(*) FROM m_mag_OrdersSum WHERE id_order='{$content['id']}'"),
            0, 0);

        // курьер
        if ($content['dSum'] > 1000) {
            if ($all_items > 1) {
                $curier_sum = 150 / $all_items;
            } else {
                $curier_sum = 150;
            }
        } else {
            $curier_sum = 0;
        }


        $q_sum = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$content['id']}' ");
        while ($order_sum = mysql_fetch_array($q_sum)) {

            $sum = $order_sum['sum'] * $order_sum['kol'];
            $percent = $content['dPercent'];
            $big_sum = show_users_percent($sum, $percent);
            $sklad = $order_sum['sklad'];
            $margin = $order_sum['margin'];
            $partner_minus_percent = $sum - $big_sum;


            $c_data_order = mysql_fetch_assoc(mysql_query("SELECT * FROM m_catalog_data_order WHERE articul='{$order_sum['articul_catalog_data_order']}' LIMIT 1 "));

            /* -=========================================
            Комиссия
            */


            if ($order_sum['is_sale'] == 1) {

                $price_com = $c_data_order['price_com'];

                if ($price_com == 0) {

                    $comission = 0;

                } else {

                    if ($all_items == 1 AND $order_sum['kol'] == 1) {
                        $comission = $price_com;
                    } else {
                        $comission = ($price_com * $order_sum['kol']) * 0.7;
                    }
                }


            } else {


                if ($all_items == 1 AND $order_sum['kol'] == 1) {
                    // один товар

                    if ($content['dType'] == 1) {

                        switch ($percent) {
                            case 3:
                                $comission = 80;
                                break;
                            case 5:
                                $comission = 70;
                                break;
                            case 7:
                                $comission = 50;
                                break;
                            case 10:
                                $comission = 30;
                                break;
                            case 15:
                                $comission = 20;
                                break;
                            default:
                                $comission = 100;
                                break;
                        }

                    } else {
                        $comission = 100;
                    }


                } else {
                    // больше одного товара

                    if ($content['dType'] == 1) {

                        switch ($percent) {
                            case 3:
                                $comission = 56;
                                break;
                            case 5:
                                $comission = 49;
                                break;
                            case 7:
                                $comission = 35;
                                break;
                            case 10:
                                $comission = 21;
                                break;
                            case 15:
                                $comission = 14;
                                break;
                            default:
                                $comission = 70;
                                break;
                        }

                    } else {
                        $comission = 70;
                    }

                }
                $comission = $comission * $order_sum['kol'];


            }


            // тип скидки
            if ($content['dType'] == 1) {
                $percent_type = 'нак';
            } elseif ($content['dType'] == 2) {
                $percent_type = 'раз';
            } else {
                $percent_type = '-';
            }


            // Доход Сергей
            $partner_all = ($margin * $order_sum['kol']) - $partner_minus_percent - $comission - $curier_sum;


            $data = [
                $content['prefix'] . '-' . $content['id'],
                $client_id,
                $client_fio,
                $all_items,
                rusdate($content['DateClose'], 3),
                rusdate($content['DateEnd'], 3),
                $c_data_order['articul'],
                format_text_out($order_sum['title']),
                $sklad,
                str_replace('.', ',', $c_data_order['price_usd1']),
                str_replace('.', ',', $c_data_order['price_usd2']),
                str_replace('.', ',', $c_data_order['price_usd3']),
                str_replace('.', ',', $c_data_order['price_usd4']),
                str_replace('.', ',', $c_data_order['price_usd5']),
                str_replace('.', ',', $c_data_order['price_usd6']),
                str_replace('.', ',', $c_data_order['price_usd7']),
                str_replace('.', ',', $c_data_order['price_usd8']),
                str_replace('.', ',', $c_data_order['price_usd9']),
                str_replace('.', ',', $c_data_order['price_usd10']),
                $order_sum['kol'],
                $order_sum['sum'],
                $sum,
                $big_sum,
                $percent,
                $percent_type,
                ($order_sum['is_sale'] == 1) ? '+' : '-',
                $comission,
                $partner_minus_percent,
                $margin,
                $curier_sum,
                $partner_all,
            ];




            /* -=========================================
            Пишет строку
            */
            $csv->SaveFile("../images/uploads/$filename", $data,'utf8','cp1251');

            $all_big_sum += $big_sum;
            $all_sum += $sum;
            $all_comission += $comission;
            $all_partner_minus_percent += $partner_minus_percent;
            $all_partner_all += $partner_all;

        }

    }
    $csv->SaveFile("../images/uploads/$filename", array('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
    ));

    $data = [
        'ИТОГО','', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', $all_sum, $all_big_sum, '', '', '', $all_comission, $all_partner_minus_percent, '', '', $all_partner_all
    ];


    $csv->SaveFile("../images/uploads/$filename", $data,'utf8','cp1251');
}

function doSourceReport($filename)
{
    global $DateStart, $DateEnd;
    // FILTER
    $filter = $_POST['filter'];
    if ($filter == 'region') {
        $where_filter = " AND mu.region='region' ";
    } else {
        $where_filter = "";
    }


    $csv = new CCSVData();

    $columns = [
        '№ заказа',
        'ID клинта',
        'ФИО',
        'e-mail',
        'телефон',
        'скидка клиента',
        'регион доставки',
        'город доставки',
        'способ оплаты',
        'способ доставки',
        'Курьерская служба',
        'Дата заказа',
        'Дата перевода в доставленные',
        'ARTICUL',
        'Наименование',
        'количество ед.',
        'цена 1 ед.',
        'Сумма N-ого кол-ва товара без скидки',
        'Сумма N-ого кол-ва товара со скидкой',
        '% скидки',
        'Менеджер заказа',
        'Статус заказа',
        'Причина отмены',
        'Заказ принят телефон или сайт',
        'First utm_source',
        'First utm_medium',
        'First utm_campaign',
        'First utm_term',
        'Last utm_source',
        'Last utm_medium',
        'Last utm_campaign',
        'Last utm_term'
    ];




    $csv->SaveFile("../images/uploads/$filename", $columns,'utf8','cp1251');
    $csv->SaveFile("../images/uploads/$filename", array(
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
    ));

    /* -=========================================
    SQL
    */
    $result = mysql_query("
    				SELECT
    					mo.*,base_users.login,  UNIX_TIMESTAMP(mo.DateClose) AS DateClose, UNIX_TIMESTAMP(mo.DateEnd) AS DateEnd,UNIX_TIMESTAMP(mo.DateAdd) AS DateAdd,
    					mu.utm_source_first,mu.utm_medium_first,mu.utm_campaign_first,mu.utm_term_first
    				FROM m_mag_Orders mo
    				LEFT JOIN m_mag_Users mu ON mu.id=mo.id_user
    				LEFT JOIN `base_users` ON `base_users`.`id` = mo.`id_manager`
    				WHERE (mo.DateAdd>='$DateStart' AND mo.DateAdd<='$DateEnd' ) {$where_filter}
    				ORDER BY mo.id DESC
    			");

    /* -=========================================
    DATA
    */
    while ($content = @mysql_fetch_array($result)) {


        //get user for take region/moscow
        if ($content['id_user'] != 0) {
            $c_user = mysql_fetch_assoc(mysql_query("SELECT * FROM m_mag_Users WHERE id='{$content['id_user']}' LIMIT 1 "));
            $content['prefix'] = ($c_user['region'] == 'region') ? 'RPF' : 'MPF';
        } else {
            $content['prefix'] = ($content['pRegion'] == 'region') ? 'RPF' : 'MPF';
        }

        // Клиент
        if ($content['id_user'] == 0) {
            $client_fio = $content['pFio'];
            $client_id = 0;
        } else {
            $client_fio = mysql_result(mysql_query("SELECT fio FROM m_mag_Users WHERE id='{$content['id_user']}'"), 0,
                0);
            $client_id = $content['id_user'];
        }


        $all_items = mysql_result(mysql_query("SELECT count(*) FROM m_mag_OrdersSum WHERE id_order='{$content['id']}'"),
            0, 0);

        // курьер
        if ($content['dSum'] > 1000) {
            if ($all_items > 1) {
                $curier_sum = 150 / $all_items;
            } else {
                $curier_sum = 150;
            }
        } else {
            $curier_sum = 0;
        }
        $login = $content['login'];
        $source = ($content['is_phone'] == 1) ? 'Телефон' : 'Сайт';

        $status = '';

        if ($content['status'] == 0) {
            $status = 'Отклонён';
        } elseif ($content['status'] == 3) {
            $status = 'Доставлен';
        } elseif ($content['status'] == 1) {
            $status = 'Новый';
        } elseif ($content['status'] == 2) {
            $status = 'Подтверждённый';
        } elseif ($content['status'] == 4) {
            $status = 'В обработке';
        } elseif ($content['status'] == 5) {
            $status = 'Отклонённый на складе';
        } elseif ($content['status'] == 6) {
            $status = 'Неполный';
        }


        $q_sum = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$content['id']}' ");
        while ($order_sum = mysql_fetch_array($q_sum)) {

            $sum = $order_sum['sum'] * $order_sum['kol'];
            $percent = $content['dPercent'];
            $big_sum = show_users_percent($sum, $percent);
            $sklad = $order_sum['sklad'];
            $margin = $order_sum['margin'];

            $partner_minus_percent = $sum - $big_sum;


            $c_data_order = mysql_fetch_assoc(mysql_query("SELECT * FROM m_catalog_data_order WHERE articul='{$order_sum['articul_catalog_data_order']}' LIMIT 1 "));

            /* -=========================================
            Комиссия
            */


            if ($order_sum['is_sale'] == 1) {

                $price_com = $c_data_order['price_com'];

                if ($price_com == 0) {

                    $comission = 0;

                } else {

                    if ($all_items == 1 AND $order_sum['kol'] == 1) {
                        $comission = $price_com;
                    } else {
                        $comission = ($price_com * $order_sum['kol']) * 0.7;
                    }
                }


            } else {


                if ($all_items == 1 AND $order_sum['kol'] == 1) {
                    // один товар

                    if ($content['dType'] == 1) {

                        switch ($percent) {
                            case 3:
                                $comission = 80;
                                break;
                            case 5:
                                $comission = 70;
                                break;
                            case 7:
                                $comission = 50;
                                break;
                            case 10:
                                $comission = 30;
                                break;
                            case 15:
                                $comission = 20;
                                break;
                            default:
                                $comission = 100;
                                break;
                        }

                    } else {
                        $comission = 100;
                    }


                } else {
                    // больше одного товара

                    if ($content['dType'] == 1) {

                        switch ($percent) {
                            case 3:
                                $comission = 56;
                                break;
                            case 5:
                                $comission = 49;
                                break;
                            case 7:
                                $comission = 35;
                                break;
                            case 10:
                                $comission = 21;
                                break;
                            case 15:
                                $comission = 14;
                                break;
                            default:
                                $comission = 70;
                                break;
                        }

                    } else {
                        $comission = 70;
                    }

                }
                $comission = $comission * $order_sum['kol'];


            }


            // тип скидки
            if ($content['dType'] == 1) {
                $percent_type = 'нак';
            } elseif ($content['dType'] == 2) {
                $percent_type = 'раз';
            } else {
                $percent_type = '-';
            }


            // Доход Сергей
            $partner_all = ($margin * $order_sum['kol']) - $partner_minus_percent - $comission - $curier_sum;



            $oOrder = (new Order())->getById($content['id']);


            /* -=========================================
            Пишет строку
            */
            $columns = [
                $content['prefix'] . '-' . $content['id'],
                $client_id,
                $client_fio,
                $c_user['email'],
                $c_user['phone'],
                $c_user['d_percent'],
                $c_user['address_region'],
                $c_user['address_city'].' '.$c_user['address_settlement'],
                $oOrder->getPaymentType(),
                $oOrder->getDeliveryType(),
                $oOrder->getCourier(),
                rusdate($content['DateAdd'], 3),
                rusdate($content['DateClose'], 3),
                $c_data_order['articul'],
                format_text_out($order_sum['title']),
                $order_sum['kol'],
                $order_sum['sum'],
                $sum,
                $big_sum,
                $percent,
                $login,
                $status,
                $oOrder->getCancelReason(),
                $source,
                $content['utm_source_first'],
                $content['utm_medium_first'],
                $content['utm_campaign_first'],
                $content['utm_term_first'],
                $content['utm_source_last'],
                $content['utm_medium_last'],
                $content['utm_campaign_last'],
                $content['utm_term_last'],
            ];




            $csv->SaveFile("../images/uploads/$filename", $columns,'utf8','cp1251');

            $all_big_sum += $big_sum;
            $all_sum += $sum;
            $all_comission += $comission;
            $all_partner_minus_percent += $partner_minus_percent;
            $all_partner_all += $partner_all;

        }

    }

    $csv->SaveFile("../images/uploads/$filename", array(
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
    ));
    $csv->SaveFile("../images/uploads/$filename", array(
        'ИТОГО',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        '',
        $all_sum,
        $all_big_sum,
        '',
        '',
        '',
        $all_comission,
        $all_partner_minus_percent,
        '',
        '',
        $all_partner_all,
    ),'utf8','cp1251');
}

function doCCauseReport($filename)
{
    global $DateStart, $DateEnd;
    $result = mysql_query("SET NAMES cp1251");
    $fields = array('Менеджер', 'Номер заказа', 'Дата заказа', 'Дата отмены', 'Регион');
    $result = mysql_query("SELECT `cause_id`, `cause_name` FROM `m_cancel_causes` WHERE `cause_active`='1'");
    while ($row = mysql_fetch_row($result)) {
        $causes[] = $row[0];
        $fields[] = $row[1];
    }
    @mysql_free_result($result);
    $causes[] = '0';
    $fields[] = 'Другое';
    $csv = new CCSVData();
    $csv->SaveFile("../images/uploads/$filename", $fields,'utf8','cp1251');
    $fl = array();
    for ($i = 0; $i < sizeof($fields); $i++) {
        $fl[] = '';
    }

    $csv->SaveFile("../images/uploads/$filename", $fl);
    unset($fl);

    $result = mysql_query("SELECT  `m_mag_Orders`.`id`,
                               `id_manager`,
                                 `login`,
                                 `cancel_cause_id`,
                                 `DateAdd`,
                                 `DateClose`,
                                 IF(`m_mag_Orders`.`id_user`=0, `pRegion`, `m_mag_Users`.`region`) AS `out_region`
                    FROM m_mag_Orders
                    LEFT JOIN `base_users` ON `base_users`.`id` = m_mag_Orders.`id_manager`
                    LEFT JOIN `m_mag_Users` ON `m_mag_Users`.`id` =  m_mag_Orders.`id_user`
                    WHERE `status`='0'
                        AND `id_manager`!=0
                        AND	`m_mag_Orders`.DateAdd BETWEEN '" . date('Y-m-d H:i:s',
            strtotime($DateStart)) . "' AND '" . date('Y-m-d H:i:s', strtotime($DateEnd)) . "'");
    while ($row = mysql_fetch_assoc($result)) {
        $line = array(
            $row['login'] . '(' . $row['id_manager'] . ')',
            $row['id'],
            $row['DateAdd'],
            $row['DateClose'],
            $row['out_region']
        );
        $order_causes = explode(',', $row['cancel_cause_id']);
        foreach ($causes as $cause_id) {
            $line[] = (in_array($cause_id, $order_causes)) ? '1' : '0';
        }
        $csv->SaveFile("../images/uploads/$filename", $line,'utf8','cp1251');
    }
    @mysql_free_result($result);

}