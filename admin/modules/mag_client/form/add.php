<?php
if ($_SESSION['admin'] == 'allow'){
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	// Определение переменных
	$main 		= "mag";
	$module 	= "mag_client";
	$cat_name 	= 'Клиенты магазина';

    $sm->assign('capital_districts', $capital_districts);
	
	myPrintHeader("$cat_name: <small><small>Добавление</small></small>", 'news.png');
	
	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1"),
	array("NAME"=>"Добавление", "LINK"=>"", "ACTIVE"=>"0")
	));

	$sm->display("modules/$module/add.tpl");
}
?>