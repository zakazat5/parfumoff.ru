<?php
if ($_SESSION['admin'] == 'allow') {
    // Создание экземпляра шаблона
    $sm = new Smarty_Admin($valinorConfig['interface.themes']);

    $id_cat = intval($_GET['id_cat']);
    $id = intval($_GET['id']);
    // Определение переменных
    $module = "catalog_k";
    $table = "k_category";
    $main = "content";
    $cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);


    //добавление
    if (isset($_POST['add'])) {
        $cat['title'] = $_POST['title'];
        $cat['sort'] = $_POST['sort'];
        $cat['pid'] = $_POST['pid'];
        $cat['is_block'] = (isset($_POST['is_block'])) ? 1 : 0;

        $sql = "INSERT INTO $table (title, sort, is_block,pid)
			VALUES ('{$cat['title']}', '{$cat['sort']}', '{$cat['is_block']}','{$cat['pid']}')";

        mysql_query($sql);

        go("?main=$main&module=$module&action=cat&id_cat=$id_cat&id={$cat['pid']}");
    }

    //список всех категорий
    $query = mysql_query("SELECT * FROM $table ");
    while ($row = mysql_fetch_assoc($query)) {
        $aCat[] = $row;
    };
    for ($q = 0; $q < count($aCat); $q++) {
        $elem = $aCat[$q];
        if ($elem['pid'] == 0) {
            $tree[$elem['id']] = $elem;
            $links[$elem['id']] = & $tree[$elem['id']];
        } else {
            $links[$elem['pid']]['childrens'][$elem['id']] = $elem;
            $links[$elem['id']] = & $links[$elem['pid']]['childrens'][$elem['id']];
        }
    }
    $sm->assign('aCat', $aCat);


    $i = 1;
    $cat = null;


    // Подключение заголовка / ACTIONS FORM
    myPrintHeader("$cat_name", 'news.png', array(
        array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=cat_add&id_cat=$id_cat")
    ));

    // Подключение навигации
    myPrintHeaderNav(array(
        array("NAME" => "Главная", "LINK" => "index.php", "ACTIVE" => "1"),
        array("NAME" => "Контент", "LINK" => "?main=content", "ACTIVE" => "1"),
        array("NAME" => "$cat_name", "LINK" => "?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE" => "1"),
        array("NAME"=>"Администрирование категорий", "LINK"=>"?main=$main&module=$module&action=cat&id_cat=$id_cat", "ACTIVE"=>"1")
    ));

    // Список действий.
    $sm->assign('footer_id', array(
        'delete_item_catalog',
        'active_catalog',
        'active_top'
    ));

    $sm->assign('footer_names', array(
        'Удалить',
        'Активировать / Заблокировать',
        'TOP  да/нет'
    ));
    $sm->assign('footer_active', 'null');
    $sm->assign('action', 'add');

    // Вывод шаблона
    $sm->display("modules/$module/cat_add.tpl");
}
?>