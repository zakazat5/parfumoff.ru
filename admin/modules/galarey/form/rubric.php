<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$module = "galarey";
	$table = "m_galarey_rubric";
	$main = "content";
	$id_cat = intval($_GET['id_cat']);
	$cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', $id_cat))
	err_message("У вас нет прав на простмотр.");

	$where = "WHERE id_cat='$id_cat'";
	$query = mysql_query("SELECT * FROM $table $where ORDER BY title");
	$i = 1;
	while ($content = mysql_fetch_array($query)){
		$item[] = array(
		'id' => $content['id'],
		'title' => $content['title'],
		'sort' => $content['sort'],
		'is_block' => ($content['is_block'] == 0) ? 'green.gif' : 'red.gif',
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		'n' => $i
		);
		$i++;
	}
	$sm->assign('item',$item);

	myPrintHeader("$cat_name", 'news.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=rubric_add&id_cat=$id_cat")
	));

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1"),
	array("NAME"=>"Формирование категорий", "LINK"=>"", "ACTIVE"=>"0")
	));

	// Список действий.
	$sm->assign('sel_id', array(
	'delete_item_rubric',
	'active_item_rubric'
	));

	$sm->assign('sel_names', array(
	'Удалить',
	'Активировать'
	));
	$sm->assign('is_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/rubric_table.tpl");
}
?>