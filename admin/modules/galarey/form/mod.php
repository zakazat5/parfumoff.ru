<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$module = "galarey";
	$main = "content";
	$id_cat = intval($_GET['id_cat']);
	$cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', $id_cat))
	err_message("У вас нет прав на простмотр.");

	$where = "WHERE id_cat='$id_cat' and is_block=0";
	// Запрос к базе
	$q_rubric = mysql_query("SELECT * FROM m_galarey_rubric $where ORDER BY sort desc, title");
	while ($c_rubric = mysql_fetch_array($q_rubric)){
		$item_rubric[] = array(
		'id' => $c_rubric['id'],
		'title' => $c_rubric['title']
		);
	}

	$q_galarey = mysql_query("SELECT * FROM m_galarey ORDER BY registerDate desc, sort");
	while ($c_galarey = mysql_fetch_array($q_galarey)){
		$item[] = array(
		'count_foto' => @mysql_result(@mysql_query("SELECT count(id) FROM m_galarey_data WHERE id_galarey='{$c_galarey['id']}'"),0,0),
		'id' => $c_galarey['id'],
		'id_rubric' => $c_galarey['id_rubric'],
		'title' => $c_galarey['title'],
		'img' => $c_galarey['img'],
		'is_block' => ($c_galarey['is_block'] == 0) ? 'green.gif' : 'red.gif',
		'sort' => $c_galarey['sort'],
		'date' => $c_galarey['registerDate']
		);
	}
	$sm->assign('item',$item);
	$sm->assign('item_rubric',$item_rubric);

	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name", 'news.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=add&id_cat=$id_cat"),
	array("IMG" => "catalog.gif", "ALT" => "Сформировать подрубрики", "LINK" => "?main=$main&module=$module&action=rubric&id_cat=$id_cat")
	));


	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1")
	));

	// Список действий.
	$sm->assign('sel_id', array(
	'delete_item',
	'active_item'
	));

	$sm->assign('sel_names', array(
	'Удалить',
	'Активация'
	));

	// Вывод шаблона
	$sm->display("modules/$module/table.tpl");
}
?>