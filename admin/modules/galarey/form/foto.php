<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$module = "galarey";
	$table = "m_galarey_data";
	$main = "content";
	$id_news = intval($_GET['id']);
	$id_cat = intval($_GET['id_cat']);
	$cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);
	$news_title = mysql_result(mysql_query("SELECT title FROM m_galarey WHERE id='$id_news'"),0,0);


	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', $id_cat))
	err_message("У вас нет прав на простмотр.");

	if (isset($_GET['id_foto'])) {
		$id_foto = intval($_GET['id_foto']);
		$q_foto = mysql_query("SELECT * FROM $table WHERE id='$id_foto' LIMIT 1");
		while ($c_foto = mysql_fetch_array($q_foto)){
			$sm->assign('img_title',$c_foto['title']);
			$sm->assign('img_sort',$c_foto['sort']);
		}
	}

	$where = "WHERE id_galarey='$id_news'";
	// Запрос к базе
	$query = mysql_query("SELECT * FROM $table $where ORDER BY title");

	$i = 1;
	while ($content = mysql_fetch_array($query)){
		$item[] = array(
		'id' => $content['id'],
		'img' => $content['img'],
		'title' => $content['title'],
		'sort' => $content['sort'],
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		'n' => $i
		);
		$i++;
	}
	$sm->assign('item',$item);

	// Подключение заголовка / ACTIONS FORM

	myPrintHeader("$cat_name: <small><small>Формирование фото для [$news_title]</small></small>", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1"),
	array("NAME"=>"$news_title", "LINK"=>"?main=$main&module=$module&action=edit&id_cat=$id_cat&id=$id_news", "ACTIVE"=>"1"),
	array("NAME"=>"Набор фото", "LINK"=>"", "ACTIVE"=>"0"),
	));

	// Список действий.
	$sm->assign('sel_id', array(
	'delete_item_foto'
	));

	$sm->assign('sel_names', array(
	'Удалить'
	));
	$sm->assign('is_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/foto_table.tpl");
}
?>