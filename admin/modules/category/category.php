<?php 
if ($_GET['ajax_check'] == 1) {
	session_start();
	if (isset($_POST['id_prod_check']) && isset($_POST['check_prod'])) {
		$_SESSION['prod_id_check'][(int)$_POST['id_prod_check']] = (int)$_POST['check_prod'];
	}
}
else {
if ($_SESSION['admin'] == 'allow'){
	$module = "category";
	$table = "m_category";
	$main = "content";
	$cat_name ="Категории";
	
	switch ($_GET['action']) {
		case "order":
			// Проверка прав доступа
			/*
            if (!MyIsValidUsers($_SESSION['user_id'], 'DELETE', $id_cat)) {
                err_message("У вас нет прав на удаление и блокировку записей.");
            }
			*/
			
			if ($_POST['actions'] == 'null') {
                err_message("Не выбрано действие.");
            }
			
			if ($_POST['actions'] == 'delete_item_category') {
				foreach ($_POST['id'] as $key => $value) {

                    // подчищаем картинки и галерею
                    delete_directory($_SERVER['DOCUMENT_ROOT'] . "/images/uploads/category/" . $value);
					
                    @mysql_query("DELETE FROM m_category WHERE id='$value'");
                    //@mysql_query("DELETE FROM m_catalog_galarey_data WHERE id_catalog_data='$value'");
                    //@mysql_query("DELETE FROM m_catalog_data_order WHERE id_catalog_data='$value'");

                    // удаляем привязку товаров к категории
                    @mysql_query("DELETE FROM m_category_product WHERE id_category='$value'");
                }
                go("?main=$main&module=$module&action=view_table");
			}
			
			if ($_POST['actions'] == 'active_category') {
                foreach ($_POST['id'] as $key => $value) {
                    $act = mysql_result(mysql_query("SELECT active FROM m_category WHERE id='$value'"), 0, 0);
                    if ($act == 0) {
                        mysql_query("UPDATE m_category SET active=1 WHERE id='$value'");
                    } else {
                        mysql_query("UPDATE m_category SET active=0 WHERE id='$value'");
                    }
                }
                go("?main=$main&module=$module&action=view_table");
            }
			
		break;
		
		case "add":
		
			// проверка на пустоту
            if ($_POST['name'] == '' || $_POST['seo_url'] == '') {
                err_message("Не заполнены обязательные поля.");
            }
			
			//проверка на существующий урл
            $sql = "SELECT * FROM m_category WHERE seo_url='{$_POST['seo_url']}'";
            $result = mysql_query($sql);
            if ($row = mysql_fetch_assoc($result)) {
                err_message("Такой url уже существует.");
            }
			
			// основыне данные
            $name = format_text(trim($_POST['name']));
            $seo_url = translit($_POST['seo_url']);
			$seo_h1 = trim($_POST['seo_h1']);
			
            // META TAG
            $meta_title = trim($_POST['meta_title']);
			$meta_keywords = trim($_POST['meta_keywords']);
            $meta_description = trim($_POST['meta_description']);
			
			 // параметры публикации
            $sort_order = (int)trim($_POST['sort_order']);
            if (isset($_POST['active'])) {
                $active = 1;
            } else {
                $active = 0;
            }
           
		   // текстовые данные
            $description = $_POST['description'];
			
			$query  = mysql_query("SHOW TABLE STATUS LIKE 'm_category'");
			$id = mysql_result($query, 0, 'Auto_increment');
			
			// Превью
            if ($_FILES['img']['name'] != '') {

                // формирование директорий
                $path = $_SERVER['DOCUMENT_ROOT'] . '/images/';
                $mkdir = 'uploads/category/' . $id;
                $mkdir_trumbs = $mkdir . '/trumbs/';
                $mkdir_small = $mkdir . '/small/';
                $mkdir_big = $mkdir . '/big/';

                if (!file_exists($path . $mkdir)) {
                    mkdir($path . $mkdir);
                    mkdir($path . $mkdir_trumbs);
                    mkdir($path . $mkdir_small);
                    mkdir($path . $mkdir_big);
                }

                $time = time();

                $img = upload_image($_FILES['img']['tmp_name'], $time, $path . $mkdir_big,
                    $valinorConfig['catalog.big.w'], $valinorConfig['catalog.big.w']);

            } else {
                $img = '';
            }
			
	
            $alt_image = trim($_POST['alt_img']);
						
			$sql = "INSERT INTO m_category
			(name, description, meta_title, meta_keywords, meta_description, seo_h1, seo_url, image, alt_image, sort_order, active, date_add)
			VALUES ('$name', '$description', '$meta_title', '$meta_keywords', '$meta_description', '$seo_h1', '$seo_url', '$img', '$alt_image', '$sort_order', '$active', NOW())";
			
			mysql_query($sql);
			
			if (isset($_POST['save'])) {
                go("?main=$main&module=$module&action=view_table");
            }
			
			if (isset($_POST['save_add'])) {
                go("?main=$main&module=$module&action=category_add");
            }
		break;
		
		case "edit":
			// проверка на пустоту
            if ($_POST['name'] == '' || $_POST['seo_url'] == '') {
                err_message("Не заполнены обязательные поля.");
            }
			
			//проверка на существующий урл
            $sql = "SELECT * FROM m_category WHERE seo_url='{$_POST['seo_url']}'";
            $result = mysql_query($sql);
			$row = mysql_fetch_assoc($result);
			$id_category = $_GET['id_category'];
            if (($row) && ($row['id'] != $id_category)) {
                err_message("Такой url уже существует.");
            }
			
			// основыне данные
            $name = format_text(trim($_POST['name']));
            $seo_url = translit($_POST['seo_url']);
			$seo_h1 = trim($_POST['seo_h1']);
			
            // META TAG
            $meta_title = trim($_POST['meta_title']);
			$meta_keywords = trim($_POST['meta_keywords']);
            $meta_description = trim($_POST['meta_description']);
			
			 // параметры публикации
            $sort_order = (int)trim($_POST['sort_order']);
            if (isset($_POST['active'])) {
                $active = 1;
            } else {
                $active = 0;
            }
           
		   // текстовые данные
            $description = $_POST['description'];
			
			/*
			$query  = mysql_query("SHOW TABLE STATUS LIKE 'm_category'");
			$id = mysql_result($query, 0, 'Auto_increment');
			*/
			/*
			$sql = "";
			$result = "";
			$sql = "SELECT * FROM m_category WHERE id='{$id_category}'";
            $result = mysql_query($sql);
			*/
			// Превью
			
			
            if ($_FILES['img']['name'] != '') {

                // формирование директорий
                $path = $_SERVER['DOCUMENT_ROOT'] . '/images/';
                $mkdir = 'uploads/category/' . $id_category;
                $mkdir_trumbs = $mkdir . '/trumbs/';
                $mkdir_small = $mkdir . '/small/';
                $mkdir_big = $mkdir . '/big/';

                if (!file_exists($path . $mkdir)) {
                    mkdir($path . $mkdir);
                    mkdir($path . $mkdir_trumbs);
                    mkdir($path . $mkdir_small);
                    mkdir($path . $mkdir_big);
                }

                $time = time();

                $img = upload_image($_FILES['img']['tmp_name'], $time, $path . $mkdir_big,
                    $valinorConfig['catalog.big.w'], $valinorConfig['catalog.big.w']);

            } else {
				if ($_POST['img_old'] == '') {
					$img = '';
				}
				else {
					$img = $_POST['img_old'];
				}
            }
			
	
            $alt_image = trim($_POST['alt_img']);
			
			
			$db->update('m_category', [
                'name' => $name,
                'description' => $description,
                'meta_title' => $meta_title,
                'meta_keywords' => $meta_keywords,
                'meta_description' => $meta_description,
                'seo_h1' => $seo_h1,
                'seo_url' => $seo_url,
                'image' => $img,
                'alt_image' => $alt_image,
                'sort_order' => $sort_order,
                'active' => $active,
                'date_upd' => date("Y-m-d H:i:s"),
            ], 'id=' . $id_category);
			
			
			if (isset($_SESSION['prod_id_check'])){
				foreach ($_SESSION['prod_id_check'] as $id_prod=>$ch_prod) {
					$sql = "SELECT id_category FROM m_category_product WHERE (id_product='{$id_prod}' AND id_category='{$id_category}')";
					$result = mysql_query($sql);
					if ($row = mysql_fetch_assoc($result)) {
						
							if ((int)$ch_prod == 0) {
								@mysql_query("DELETE FROM m_category_product WHERE (id_product='$id_prod' AND id_category='$id_category')");
							}
					
					}
					else {
						if ((int)$ch_prod == 1) {
							$sql = "INSERT INTO m_category_product (id_category, id_product) VALUES ('$id_category', '$id_prod')";
							mysql_query($sql);
						}
					}
				}
			}
			
			
			if (isset($_POST['save'])) {
                go("?main=$main&module=$module&action=view_table");
            }
			
		break;
		
	}
}
}
?> 
