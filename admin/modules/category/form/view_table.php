<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	//$id_cat = intval($_GET['id_cat']);
	
	// Определение переменных
	$module = "category";
	$table = "m_category";
	$main = "content";
	$cat_name ="Категории";
	//$cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);
	// Проверка прав доступа
	/*
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)) 
	err_message("У вас нет прав на просмотр.");
	*/

	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name", 'news.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить категорию", "LINK" => "?main=$main&module=$module&action=category_add")
	));
	
	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=view_table", "ACTIVE"=>"0"),
	//array("NAME"=>"Формирование каталога", "LINK"=>"", "ACTIVE"=>"0")
	));
	
	// первый уровень
	$q1 = mysql_query("SELECT * FROM $table ORDER BY sort_order");
	while ($c1 = @mysql_fetch_array($q1)){
		$item[] = array(
		'id' => $c1['id'],
		'name' => $c1['name'],
		'sort_order' => $c1['sort_order'],
		'active' => ($c1['active'] == 0) ? 'red.gif' : 'green.gif'
		);
	}
	$sm->assign('item',$item);
	
	// Список действий.
	$sm->assign('footer_id', array(
	'delete_item_category',
	'active_category'
	//'active_top'
	));

	$sm->assign('footer_names', array(
	'Удалить',
	'Активировать / Заблокировать'
	//'TOP  да/нет'
	));
	$sm->assign('footer_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/category_table.tpl");
}
?> 
