<?php
if ($_SESSION['admin'] == 'allow'){
	$id_cat 	= intval($_GET['id_cat']);
	$cat_module 	= $_GET['worker'];
	$cat_main 	= $_GET['main'];

	switch ($_GET['action']){
		case "add":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'ADD', $id_cat)) err_message("У вас нет прав на добавление.");

			if ($_POST['title'] == ''
			or $_POST['sort'] == ''
			or $_POST['url'] == ''
			){err_message("Не заполнены обязательные поля.");}

			if (isset($_POST['is_main'])){$is_main = 1;}else{$is_main = 0;}
			if (isset($_POST['is_block'])){$is_block = 1;}else{$is_block = 0;}
			if (isset($_POST['is_links'])){$is_links = 1;}else{$is_links = 0;}
			if (isset($_POST['is_imgf'])){$is_imgf = 1;}else{$is_imgf = 0;}
			if (isset($_POST['is_meta_title'])){$is_meta_title = 1;}else{$is_meta_title = 0;}
			if (isset($_POST['is_meta_description'])){$is_meta_description = 1;}else{$is_meta_description = 0;}

			$sel_brend = intval($_POST['sel_brend']);
			$sel_catalog_data = intval($_POST['sel_catalog_data']);
			
			$sort 	= trim($_POST['sort']);
			$url	= translit($_POST['url']);
			$title 	= format_text(trim($_POST['title']));
			$anot 	= ($_POST['anot']);
			$text 	= ($_POST['text']);
			$alt 	= format_text(trim($_POST['alt']));

			if ($is_meta_title) $meta_title = trim($_POST['meta_title']);
			else $meta_title = $title;
			if ($is_meta_description) $meta_description = trim($_POST['meta_description']);
			else $meta_description = HTMLToTxt($_POST['anot']);

			$sql = "INSERT INTO m_news
			(id_cat, id_brend, id_catalog_data,
			title, anot, text,
			img, alt,
			registerDate, 
			is_main, is_block, is_links, is_imgf,
			sort, url, 
			meta_title, meta_description)
			VALUES (
			'$id_cat', '$sel_brend', '$sel_catalog_data', 
			'$title', '$anot', '$text',
			'$img', '$alt', NOW(),
			'$is_main', '$is_block', '$is_links', '$is_imgf',
			'$sort', '$url', 
			'$meta_title', '$meta_description')";
			mysql_query($sql);
			$id_news = mysql_result(mysql_query("SELECT LAST_INSERT_ID()"),0,0);

			// Превьюшки
			if ($_FILES['img']['name']!=''){

				// формирование директорий
				$path 			= '../images/';
				$mkdir 			= 'uploads/news/'.$id_news;
				$mkdir_trumbs 	= $mkdir .'/trumbs/';
				$mkdir_small 	= $mkdir .'/small/';
				$mkdir_big 		= $mkdir .'/big/';

				if (!file_exists($path.$mkdir)){
					mkdir($path.$mkdir);
					mkdir($path.$mkdir_trumbs);
					mkdir($path.$mkdir_small);
					mkdir($path.$mkdir_big);
				}

				$time = time();
				$img =	upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_trumbs,  170,  170);
				upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_small, 400, 400);

				$sql = "UPDATE m_news SET img='$img' WHERE id='$id_news'";
				mysql_query($sql);
			}

			// переход
			if (isset($_POST['save']))
			go("?main=$cat_main&module=$cat_module&action=edit&id_cat=$id_cat&id=$id_news");
			elseif (isset($_POST['save_add']))
			go("?main=$cat_main&module=$cat_module&action=add&id_cat=$id_cat");
			elseif (isset($_POST['save_list']))
			go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");
			break;

		case "edit":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'EDIT', $id_cat)) err_message("У вас нет прав на редактирование.");

			if ($_POST['title'] == ''
			or $_POST['sort'] == ''
			or $_POST['url'] == ''
			){err_message("Не заполнены обязательные поля.");}

			$id_news = $_GET['id'];

			if (isset($_POST['is_main'])){$is_main = 1;}else{$is_main = 0;}
			if (isset($_POST['is_block'])){$is_block = 1;}else{$is_block = 0;}
			if (isset($_POST['is_links'])){$is_links = 1;}else{$is_links = 0;}
			if (isset($_POST['is_imgf'])){$is_imgf = 1;}else{$is_imgf = 0;}

			$sel_brend = intval($_POST['sel_brend']);
			$sel_catalog_data = intval($_POST['sel_catalog_data']);
			
			$sort 	= trim($_POST['sort']);
			$url 	= translit($_POST['url']);
			$title 	= format_text(trim($_POST['title']));
			$anot 	= mysql_real_escape_string($_POST['anot']);
			$text 	= mysql_real_escape_string($_POST['text']);
			$alt 	= format_text(trim($_POST['alt']));

			// Meta
			$meta_title = trim($_POST['meta_title']);
			$meta_description = trim($_POST['meta_description']);

			// Превьюшки
			if ($_FILES['img']['name']!=''){

				@unlink('../images/uploads/news/'.$id_news. '/trumbs/' .$_POST['del_img']);
				@unlink('../images/uploads/news/'.$id_news. '/small/' .$_POST['del_img']);

				// формирование директорий
				$path 			= '../images/';
				$mkdir 			= 'uploads/news/'.$id_news;
				$mkdir_trumbs 	= $mkdir .'/trumbs/';
				$mkdir_small 	= $mkdir .'/small/';
				$mkdir_big 		= $mkdir .'/big/';

				if (!file_exists($path.$mkdir)){
					mkdir($path.$mkdir);
					mkdir($path.$mkdir_trumbs);
					mkdir($path.$mkdir_small);
					mkdir($path.$mkdir_big);
				}

				$time = time();
				$img =	upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_trumbs,  170,  170);
				upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_small, 400, 400);

			}else {
				$img = $_POST['del_img'];
			}

			$sql = "UPDATE m_news SET
			id_brend='$sel_brend', id_catalog_data='$sel_catalog_data', 
			title='$title', anot='$anot', text='$text', 
			alt='$alt', img='$img',
			is_main='$is_main', is_block='$is_block', is_links='$is_links', is_imgf='$is_imgf',
			sort='$sort', url='$url',
			meta_title='$meta_title', meta_description='$meta_description'
			WHERE id='$id_news' ";

			mysql_query($sql);


			// переход
			if (isset($_POST['save']))
			go("?main=$cat_main&module=$cat_module&action=edit&id_cat=$id_cat&id=$id_news");
			elseif (isset($_POST['save_add']))
			go("?main=$cat_main&module=$cat_module&action=add&id_cat=$id_cat");
			elseif (isset($_POST['save_list']))
			go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");

			break;
			
			
		case "galarey_foto":
			if (isset($_POST['foto_add'])){
				$id_news = intval($_GET['id']);
				$alt = format_text(trim( $_POST['alt'] ));

				if ($_FILES['img']['name']!=''){
					$path 			= '../images/';
					$mkdir 			= 'uploads/news/'.$id_news;
					$mkdir_trumbs 	= $mkdir .'/trumbs/';
					$mkdir_small 	= $mkdir .'/small/';
					$mkdir_big 		= $mkdir .'/big/';

					if (!file_exists($path.$mkdir)){
						mkdir($path.$mkdir);
						mkdir($path.$mkdir_trumbs);
						mkdir($path.$mkdir_small);
						mkdir($path.$mkdir_big);
					}

					$time = time();
					$img =	upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_trumbs,  150,  150);
					upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_big);

				}else{
					err_message("Не выбран файл.");
				}

				$sql = "INSERT INTO m_news_foto
				(id_news, img, alt)
				VALUES ('$id_news', '$img', '$alt')";
				mysql_query($sql);

				go("?main=$cat_main&module=$cat_module&action=galarey_foto&id=$id_news&id_cat=$id_cat");


			}elseif (isset($_POST['foto_edit'])){

				$id_news = intval($_GET['id']);
				$alt = format_text(trim( $_POST['alt'] ));
				$id_foto = intval($_GET['id_foto']);

				if ($_FILES['img']['name']!='') {
					$path 			= '../images/';
					$mkdir 			= 'uploads/news/'.$id_news;
					$mkdir_trumbs 	= $mkdir .'/trumbs/';
					$mkdir_small 	= $mkdir .'/small/';
					$mkdir_big 		= $mkdir .'/big/';

					if (!file_exists($path.$mkdir)){
						mkdir($path.$mkdir);
						mkdir($path.$mkdir_trumbs);
						mkdir($path.$mkdir_small);
						mkdir($path.$mkdir_big);
					}

					@unlink($path.$mkdir_trumbs.$_POST['img_name']);
					@unlink($path.$mkdir_small.$_POST['img_name']);
					@unlink($path.$mkdir_big.$_POST['img_name']);

					$time = time();
					$img =	upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_trumbs,  150,  150);
					upload_image($_FILES['img']['tmp_name'],  $time,  $path.$mkdir_big);

				}else{
					$img = $_POST['img_name'];
				}

				$sql = "UPDATE m_news_foto SET
				img='$img', 
				alt='$alt'
				WHERE id='$id_foto' ";

				mysql_query($sql);
				go("?main=$cat_main&module=$cat_module&action=galarey_foto&id_cat=$id_cat&id=$id_news");
			}
			break;

		case "order":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'DELETE', $id_cat)){
				err_message("У вас нет прав на удаление и блокировку записей.");
			}

			if ($_POST['actions'] == 'null'){err_message("Не выбрано действие.");}
			if (!isset($_POST['id']) and !isset($_POST['sel_total'])){err_message("Не выбраны элементы.");}

			// Удаление записей
			if ($_POST['actions'] == 'delete_item') {
				foreach ($_POST['id'] as $key=>$value){
					delete_directory("../images/uploads/news/".$value);
					mysql_query("DELETE FROM m_news WHERE id='$value'");
				}
				go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");

				// активация/деактивация
			}elseif ($_POST['actions'] == 'active_item'){
				foreach ($_POST['id'] as $key=>$value) {
					$is = mysql_result(mysql_query("SELECT is_block FROM m_news WHERE id='$value'"),0,0);
					if ($is == 0) {
						mysql_query("UPDATE m_news SET is_block=1 WHERE id='$value'");
					}else{
						mysql_query("UPDATE m_news SET is_block=0 WHERE id='$value'");
					}
				}
				go("?main=$cat_main&module=$cat_module&action=mod&id_cat=$id_cat");

			}elseif ($_POST['actions'] == 'delete_item_foto'){
				$id_news = intval($_GET['id']);

				foreach ($_POST['id'] as $key=>$value){
					$del_img = mysql_result(mysql_query("SELECT img FROM m_news_foto WHERE id='$value'"),0,0);

					@unlink("../images/uploads/news/". $id_news ."/trumbs/". $del_img);
					@unlink("../images/uploads/news/". $id_news ."/small/". $del_img);
					@unlink("../images/uploads/news/". $id_news ."/big/". $del_img);

					mysql_query("DELETE FROM m_news_foto WHERE id='$value'");
				}
				go("?main=$cat_main&module=$cat_module&action=galarey_foto&id_cat=$id_cat&id=$id_news");

			}

			break;
	}
}
?>