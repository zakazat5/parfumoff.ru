<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$module 	= "news";
	$main 		= "content";
	$id_cat 	= intval($_GET['id_cat']);
	$cat_name 	= mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', $id_cat)) err_message("У вас нет прав на простмотр.");

	include_once('filter.php');

	// Запрос к базе
	$all_items = mysql_result(mysql_query("SELECT count(*) FROM m_news $where"),0,0);
	$pages = pages($all_items, $_SESSION['user_cpage'], 'pages.tpl');
	$start = $pages['start'];
	$limit = $_SESSION['user_cpage'];

	$query = mysql_query("
	SELECT * FROM m_news $where 
	ORDER BY registerDate desc 
	LIMIT $start, $limit");

	$i = $start + 1;
	while ($content = mysql_fetch_array($query)){

		$item[] = array(
		'id' => $content['id'],
		'title' => $content['title'],
		'sort' => $content['sort'],

		'date' => $content['registerDate'],
		'is_block' => ($content['is_block'] == 0) ? 'green.gif' : 'red.gif',
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		'n' => $i
		);
		
		$i++;
	}

	$sm->assign('item',$item);
	$sm->assign('all_items',$all_items);
	$sm->assign('pages',$pages['pages']);

	myPrintHeader("$cat_name", 'news.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=add&id_cat=$id_cat")
	));

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1")
	));

	// Список действий.
	$sm->assign('footer_id', array(
	'delete_item',
	'active_item'
	));

	$sm->assign('footer_names', array(
	'Удалить',
	'Активировать / Заблокировать'
	));
	$sm->assign('footer_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/table.tpl");
}
?>