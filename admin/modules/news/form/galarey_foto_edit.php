<?php
if ($_SESSION['admin'] == 'allow'){
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	$module 	= "news";
	$main 		= "content";
	$id_cat 	= intval($_GET['id_cat']);
	$cat_name 	= mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', $id_cat)) err_message("У вас нет прав на простмотр.");
	
	$id_foto = intval($_GET['id_foto']);
	$id_news = intval($_GET['id']);
	$news_title = mysql_result(mysql_query("SELECT title FROM m_news WHERE id='$id_news'"),0,0);

	$q_foto = mysql_query("SELECT * FROM m_news_foto WHERE id='$id_foto' LIMIT 1");
	while ($c_foto = mysql_fetch_array($q_foto)){
		$foto_title = $c_foto['alt'];
		$sm->assign('img_alt', $c_foto['alt']);
		$sm->assign('img_name',$c_foto['img']);
	}
	
	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name: <small><small>Редактирование фото [$id_foto]</small></small>", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1"),
	array("NAME"=>"Галерея [$news_title]", "LINK"=>"?main=$main&module=$module&action=galarey_foto&id_cat=$id_cat&id=$id_news", "ACTIVE"=>"1"),
	array("NAME"=>"$foto_title", "LINK"=>"", "ACTIVE"=>"0")
	));

	$sm->display("modules/$module/galarey_foto_edit.tpl");
}
?>