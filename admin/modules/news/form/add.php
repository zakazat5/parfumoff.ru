<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	$module = "news";
	$table = "m_news";
	$main = "content";
	$id_cat = intval($_GET['id_cat']);
	$cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'ADD', $id_cat)) err_message("У вас нет прав на добавление.");
	
	$q_brend = mysql_query("SELECT id,title FROM m_catalog ORDER BY title");
	while ($c_brend = mysql_fetch_array($q_brend)){
		$m_list_brend .= "<option value=" .$c_brend['id']. " >" . $c_brend['title'] . '</option>';
	}
	$sm->assign('sel_brend', $m_list_brend);
	
	// Подключение заголовка
	myPrintHeader("$cat_name: <small><small>Добавить</small></small>", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1")
	));

	$sm->display("modules/$module/add.tpl");
}
?>