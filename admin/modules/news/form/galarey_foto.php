<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	$module 	= "news";
	$main 		= "content";
	$id_cat 	= intval($_GET['id_cat']);
	$cat_name 	= mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', $id_cat)) err_message("У вас нет прав на простмотр.");
	
	$id_news = intval($_GET['id']);
	$news_title = mysql_result(mysql_query("SELECT title FROM m_news WHERE id='$id_news'"),0,0);

	$query = mysql_query("SELECT * FROM m_news_foto WHERE id_news='$id_news' ");
	$i=0;
	while ($content = mysql_fetch_array($query)){
		$item[] = array(
		'id' => $content['id'],
		'img' => $content['img'],
		'title' => $content['title'],
		'sort' => $content['sort'],
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		'n' => $i
		);
		$i++;
	}
	$sm->assign('item',$item);
	
	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name: <small><small>Формирование фото для [$news_title]</small></small>", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1"),
	array("NAME"=>"Галерея [$news_title]", "LINK"=>"", "ACTIVE"=>"0")
	));

	// Список действий.
	$sm->assign('footer_id', array(
	'delete_item_foto'
	));

	$sm->assign('footer_names', array(
	'Удалить'
	));
	$sm->assign('footer_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/galarey_foto_table.tpl");
}
?>