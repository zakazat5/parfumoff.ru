<?php
if ($_SESSION['admin'] == 'allow'){
	$start = 0;
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$module = "brends";
	$table = "m_catalog";
	$main = "content";
	$cat_name ="Торговые дома";

	// Проверка прав доступа
	/*
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)) 
	err_message("У вас нет прав на просмотр.");
	*/
	
	// количество запсей на странице
	if (isset($_GET['cpag'])) $_SESSION['user_cpage_brends'] = intval($_GET['cpag']);
	if (!isset($_SESSION['user_cpage_brends'])) $_SESSION['user_cpage_brends'] = 20;
	
	// Запрос к базе
    $all_items = mysql_result(mysql_query("SELECT count(id) FROM $table"), 0, 0);
    $pages = pages($all_items, $_SESSION['user_cpage_brends'], 'pages.tpl');
    $start = $pages['start'];
    $limit = $_SESSION['user_cpage_brends'];
	
	// Подключение заголовка / ACTIONS FORM
	
	myPrintHeader("$cat_name", 'news.png');
	
	/*
	myPrintHeader("$cat_name", 'news.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить бренд", "LINK" => "?main=$main&module=$module&action=brend_add")
	));
	*/
	
	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=view_table", "ACTIVE"=>"0"),
	));
	
	$i = $start + 1;
	// первый уровень
	$q1 = mysql_query("SELECT * FROM $table ORDER BY title LIMIT $start, $limit");
	while ($c1 = @mysql_fetch_array($q1)){
		$item[] = array(
		'id' => $c1['id'],
		'name' => $c1['title'],
		'name1' => $c1['title_1'],
		'top' => ($c1['is_top'] == 0) ? 'red.gif' : 'green.gif',
		'top300' => ($c1['is_top300'] == 0) ? 'red.gif' : 'green.gif',
		'active' => ($c1['is_block'] == 1) ? 'red.gif' : 'green.gif',
		'n_brend' => $i
		);
		$i++;
	}
	$sm->assign('item',$item);
    $sm->assign('pages', $pages['pages']);
	
	// Список действий.
	$sm->assign('footer_id', array(
	'active_brend',
	'active_top',
	'active_top300'
	));

	$sm->assign('footer_names', array(
	'Активировать / Заблокировать',
	'TOP  да/нет',
	'TOP300  да/нет'
	));
	$sm->assign('footer_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/brends_table.tpl");
}
?> 
