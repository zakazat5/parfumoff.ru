<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$cat_name = "Управление баннерами";
	$module = "banner_simple";
	$main = "banner";

	// Проверка прав доступа
	//if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)){
	//	err_message("У вас нет прав на простмотр.");
	//}

    $aBanners = $db->fetchAll($db->select()->from('m_banner_simple'));




	
	/* -=========================================
		ВЫВОД И ФОРМИРОВНИЕ ДАННЫХ
	*/

	$sm->assign('aBanners',$aBanners);

	// Заголовок
	myPrintHeader($cat_name, 'nastroyka.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить", "LINK" => "?main=$main&module=$module&action=add")));

	// Навигация
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Реклама", "LINK"=>"?main=banner", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Действия
	$sm->assign('sel_id', array(
	'delete_item',
	'active',
	'flush_hits',
	'flush_clicks'
	));

	$sm->assign('sel_names', array(
	'Удалить'
	));
	$sm->assign('is_active', 'null');

	// Вывод шаблона
	$sm->display("modules/{$_GET['module']}/table.tpl");
}
?>