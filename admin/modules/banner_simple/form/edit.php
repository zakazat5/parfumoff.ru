<?php
if ($_SESSION['admin'] == 'allow'){
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Проверка прав доступа
	//if (!MyIsValidUsers($_SESSION['user_id'], 'EDIT', 0, $module)){
	//	err_message("У вас нет прав на редактирование.");
	//}

	// Определение переменных
	$cat_name 	= "Управление баннерами";
	$module 	= "banner_simple";
	$main 		= "banner";
	$id 		= intval($_GET['id']);


    $banner = $db->fetchRow($db->select()->from('m_banner_simple')->where('id=?',$id));

    $sm->assign('banner',$banner);




	

	// Подключение заголовка
	myPrintHeader("$cat_name: <small><small>Изменить [$title]</small></small>", 'nastroyka.png');

	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Реклама", "LINK"=>"?main=banner", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Вывод шаблона
	$sm->display("modules/$module/edit.tpl");
}
?>