<?php
if ($_SESSION['admin'] == 'allow') {
    $cat_module = $_GET['worker'];
    $cat_main = $_GET['main'];
    $id = (int)$_GET['id'];

    error_reporting(E_ALL);
    ini_set('display_errors', 1);



    switch ($_GET['action']) {

        // Добавление
        case "add":

            if ($_POST['title'] == '' || $_FILES['img'] == '') {
                err_message("Не заполнены обязательные поля.");
            }

            $data = [];

            $ext = end(explode('.', $_FILES['img']['name']));

            $filename = time() . rand(0, 20) . '.' . $ext;
            $path = '/images/uploads/banners/' . $filename;


            move_uploaded_file($_FILES['img']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $path);

            //block | unblock
            if (isset($_POST['is_block'])) {
                $data['is_block'] = 1;
            } else {
                $data['is_block'] = 0;
            }
            $data['url'] = $_POST['url'];
            $data['title'] = $_POST['title'];
            $data['img'] = $path;


            $db->insert('m_banner_simple', $data);


            //mysql_query($sql);
            go("?main=$cat_main&module=$cat_module&action=mod");

            break;

        // Редактирование блока меню
        case "edit":
            if ($_POST['title'] == '') {
                err_message("Не заполнены обязательные поля.");
            }

            $data = [];

            if ($_FILES['img']['name']!=''){
                $ext = end(explode('.', $_FILES['img']['name']));
                $filename = time() . rand(0, 20) . '.' . $ext;
                $path = '/images/uploads/banners/' . $filename;
                move_uploaded_file($_FILES['img']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $path);
                $data['img'] = $path;
            }

            //block | unblock
            if (isset($_POST['is_block'])) {
                $data['is_block'] = 1;
            } else {
                $data['is_block'] = 0;
            }
            $data['url'] = $_POST['url'];
            $data['title'] = $_POST['title'];

            $db->update('m_banner_simple', $data,'id='.$id);
            go("?main=$cat_main&module=$cat_module&action=mod");
            break;


        case "order":
            if ($_POST['actions'] == 'null') {
                err_message("Не выбрано действие.");
            }
            if (!isset($_POST['id'])) {
                err_message("Не выбраны элементы.");
            }

            // Удаление записей
            if ($_POST['actions'] == 'delete_item') {
                foreach ($_POST['id'] as $key => $value) {
                    if ($value > 0) {
                        $banner = $db->fetchRow($db->select()->from('m_banner_simple')->where('id=?', $value));
                        //delete image
                        if ($banner['img'] != '') {
                            unlink($_SERVER['DOCUMENT_ROOT'] . $banner['img']);
                        }
                        $db->delete('m_banner_simple', 'id=' . $value);
                    }
                }
            }

            //go("?main=$cat_main&module=$cat_module&action=mod");
            break;
    }
}
?>