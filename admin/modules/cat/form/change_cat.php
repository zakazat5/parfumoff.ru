<?php
if ($_SESSION['admin'] == 'allow'){
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	// Определение переменных
	$cat_name = "Управление контентом";
	$module = "cat";
	$main = "permitions";
	
	$id_block = intval($_GET['id']);

	// Получение данных блока меню
	$query_menu = mysql_query("SELECT * FROM base_cat_group WHERE id='$id_block' LIMIT 1");
	while ($content_menu = mysql_fetch_array($query_menu)){
		$title_block = $content_menu['title'];
	}

	// Получение данных вложенных разделов в блок меню
	$q_base_cat = mysql_query("SELECT * FROM base_cat WHERE id_menu='$id_block' ORDER BY sort, name");
	while ($c_base_cat = mysql_fetch_array($q_base_cat)){
		
		$links = VLGetBaseOptions($c_base_cat['id'], 'LINKS' );
		if ($links == '') $canon_name = $c_base_cat['canon_name']; else $canon_name = $links;
		
		$item_cat[] = array(
		'id' => $c_base_cat['id'],
		'name' => $c_base_cat['name'],
		'canon_name' => $canon_name,
		'type' => $c_base_cat['type'],
		'type_2' => $c_base_cat['type_2'],
		'sort' => $c_base_cat['sort'],
		'is_block' => ($c_base_cat['is_block'] == 0) ? 'green.gif' : 'red.gif',
		'bgcolor' =>  '#f2f2f2'
		);
	}


	/* -=========================================
	ВЫВОД И ФОРМИРОВНИЕ ДАННЫХ
	*/

	// Занесение элементов меню
	$sm->assign('item_cat',$item_cat);

	// Занесение существующих элементов меню
	//$sm->assign('item_current_cat',cat_parent_select($id_block));

	$sm->assign('module_value', array(
	"text",
	"news",
	"catalog",
	));

	$sm->assign('module_names', array(
	"Одиночный текстовый раздел",
	"Лента новостей",
	"Каталог",
	));

	// Подключение заголовка
	myPrintHeader("$cat_name: <small><small>Настройка разделов - [$title_block]</small></small>", 'nastroyka.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1"),
	array("NAME"=>"[$title_block]", "LINK"=>"?main=$main&module=$module&action=edit_menu&id=$id_block", "ACTIVE"=>"1"),
	array("NAME"=>"Настройка разделов", "LINK"=>"", "ACTIVE"=>"0")
	));

	// Список действий (actions). для футера
	$sm->assign('footer_id', array(
	'delete_item_cat',
	'active_item_cat',
	'deactive_item_cat'
	));

	$sm->assign('footer_names', array(
	'Удалить',
	'Активировать записи',
	'Заблокировать записи'
	));
	$sm->assign('footer_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/change_cat.tpl");
}
?>