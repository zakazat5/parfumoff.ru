<?php
if ($_SESSION['admin'] == 'allow'){
	$cat_module 	= $_GET['worker'];
	$cat_main 	= $_GET['main'];
	$id_menu 	= $_GET['id'];

	switch ($_GET['action']) {

		// Добавление блока меню
		case "add_menu":
			if ($_POST['title'] == ''	){err_message("Не заполнены обязательные поля.");}

			if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";}
			$title = format_text($_POST['title']);

			$sql = "INSERT INTO
			base_cat_group (id, title, is_block)
			VALUES ('', '$title', '$is_block')";

			mysql_query($sql);
			go("?main=$cat_main&module=$cat_module&action=mod");

			break;

			// Редактирование блока меню
		case "edit_menu":
			if ($_POST['title'] == ''){err_message("Не заполнены обязательные поля.");}

			if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";}
			$title = format_text($_POST['title']);

			$sql = "UPDATE base_cat_group SET
				title='$title', 
				is_block='$is_block' 
				WHERE id='$id_menu'";

			mysql_query($sql);
			go("?main=$cat_main&module=$cat_module&action=mod");
			break;

			// добавление категории
		case "add_cat":
			switch ($_POST['type_cat']) {
				case 'news':
					if ($_POST['name'] == ''
					or $_POST['sort'] == ''
					){err_message("Не заполнены обязательные поля.");}

					// Создание и проверка переменных
					if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";}

					$sort 			= intval($_POST['sort']);
					$type_cat 		= trim($_POST['type_cat']);
					$name 			= format_text($_POST['name']);
					$canon_name 		= trim($_POST['canon_name']);

					// Дополнительные параметры
					$pagis_count 	= $_POST['pagis_count'];
					if(isset($_POST['is_pagis'])){$is_pagis = "1";}else{$is_pagis = "0";}

					// Meta
					$meta_description = trim($_POST['meta_description']);
					$meta_title = $name;

					mysql_query("INSERT INTO base_cat (id, id_menu, id_parent, name, canon_name, type, sort, is_block, meta_title, meta_description)
					VALUES ('', '$id_menu', '-1', '$name', '$canon_name', '$type_cat', '$sort', '$is_block','$meta_title','$meta_description')");
					$last_cat = mysql_result(mysql_query("SELECT LAST_INSERT_ID()"),0,0);

					// назначение правоф
					VLSetInterface($type_cat, $name, $last_cat);

					// Добавление доп. параметров к модулю
					VLSetBaseOptions($last_cat, 'PAGIS_COUNT', $pagis_count);
					VLSetBaseOptions($last_cat, 'IS_PAGIS', $is_pagis);

					break;
					
				case 'catalog':
					if ($_POST['name'] == ''
					or $_POST['sort'] == ''
					){err_message("Не заполнены обязательные поля.");}

					// Создание и проверка переменных
					if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";}

					$sort 			= intval($_POST['sort']);
					$type_cat 		= trim($_POST['type_cat']);
					$name 			= format_text($_POST['name']);
					$canon_name 	= trim($_POST['canon_name']);
					
					// Meta
					$meta_description = trim($_POST['meta_description']);
					$meta_title = $name;

					mysql_query("INSERT INTO base_cat (id, id_menu, id_parent, name, canon_name, type, sort, is_block, meta_title, meta_description)
					VALUES ('', '$id_menu', '-1', '$name', '$canon_name', '$type_cat', '$sort', '$is_block','$meta_title','$meta_description')");
					$last_cat = mysql_result(mysql_query("SELECT LAST_INSERT_ID()"),0,0);

					// назначение правоф
					VLSetInterface($type_cat, $name, $last_cat);
					break;

				case 'text':
					if ($_POST['name'] == ''
					or $_POST['sort'] == ''
					){err_message("Не заполнены обязательные поля.");}


					// Создание и проверка переменных
					if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";}
					$sort = intval($_POST['sort']);
					$type_cat = trim($_POST['type_cat']);
					$name = format_text($_POST['name']);
					$canon_name = trim($_POST['canon_name']);

					// Meta
					$meta_description = trim($_POST['meta_description']);
					$meta_title = $name;

					mysql_query("INSERT INTO base_cat (id, id_menu, id_parent, name, canon_name, type, sort, is_block, meta_title, meta_description)
					VALUES ('', '$id_menu', '-1', '$name', '$canon_name', '$type_cat', '$sort', '$is_block', '$meta_title', '$meta_description')");
					$last_cat = mysql_result(mysql_query("SELECT LAST_INSERT_ID()"),0,0);

					// назначение правоф
					VLSetInterface($type_cat, $name, $last_cat);
					break;

				default:
					break;
			}

			// Идем обратно
			go("?main=$cat_main&module=$cat_module&action=change_cat&id=$id_menu");


			break;

			// редактирование категории
		case "edit_cat":
			$id_cat = $_GET['id_cat'];

			switch ($_POST['type_cat']) {
				case 'news':
					if ($_POST['name'] == ''
					or $_POST['sort'] == ''
					){err_message("Не заполнены обязательные поля.");}

					// Создание и проверка переменных
					if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";}
					$sort = intval($_POST['sort']);
					$name = format_text($_POST['name']);
					$canon_name = trim($_POST['canon_name']);

					// Meta
					$meta_description = trim($_POST['meta_description']);
					$meta_title = trim($_POST['meta_title']);

					// Дополнительные параметры
					$pagis_count = $_POST['pagis_count'];
					if(isset($_POST['is_pagis'])){$is_pagis = "1";}else{$is_pagis = "0";}

					$sql = "UPDATE base_cat
							SET sort='$sort', 
								name='$name', 
								canon_name='$canon_name', 
								is_block='$is_block',
								meta_description='$meta_description',
								meta_title='$meta_title'
							WHERE id='$id_cat'";
					mysql_query($sql);

					// Изменение названия модуля в таблице base_module
					mysql_query("UPDATE base_module SET title='$name' WHERE id_module='$id_cat'");

					// Добавление доп. параметров к модулю
					VLSetBaseOptions($id_cat, 'IS_PAGIS', $is_pagis);
					VLSetBaseOptions($id_cat, 'PAGIS_COUNT', $pagis_count);

					break;
					
				case 'catalog':
					if ($_POST['name'] == ''
					or $_POST['sort'] == ''
					){err_message("Не заполнены обязательные поля.");}

					// Создание и проверка переменных
					if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";}
					$sort = intval($_POST['sort']);
					$name = format_text($_POST['name']);
					$canon_name = trim($_POST['canon_name']);
					
					// Meta
					$meta_description = trim($_POST['meta_description']);
					$meta_title = trim($_POST['meta_title']);

					$sql = "UPDATE base_cat
							SET sort='$sort', 
								name='$name', 
								canon_name='$canon_name', 
								is_block='$is_block',
								meta_description='$meta_description',
								meta_title='$meta_title'
							WHERE id='$id_cat'";
					mysql_query($sql);

					// Изменение названия модуля в таблице base_module
					mysql_query("UPDATE base_module SET title='$name' WHERE id_module='$id_cat'");
					break;

				case 'text':
					if ($_POST['name'] == ''
					or $_POST['sort'] == ''
					){err_message("Не заполнены обязательные поля.");}

					// Создание и проверка переменных
					if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";}
					$sort = intval($_POST['sort']);
					$name = format_text($_POST['name']);
					$canon_name = trim($_POST['canon_name']);

					// Meta
					$meta_description = trim($_POST['meta_description']);
					$meta_title = trim($_POST['meta_title']);
					$type_2 = trim($_POST['type_2']);

					// Добавление доп. параметров к модулю
					$shablon_name = trim($_POST['shablon_name']);

					$sql = "UPDATE base_cat
							SET sort='$sort', 
								name='$name', 
								canon_name='$canon_name', 
								type_2='$type_2',
								meta_description='$meta_description',
								meta_title='$meta_title', 
								is_block='$is_block' 
							WHERE id='$id_cat'";
					mysql_query($sql);

					// Изменение названия модуля в таблице base_module
					mysql_query("UPDATE base_module SET title='$name' WHERE id_module='$id_cat'");
					break;

				default:
					break;
			}

			go("?main=$cat_main&module=$cat_module&action=change_cat&id=$id_menu");
			break;

		case "order":
			if ($_POST['actions'] == 'null'){err_message("Не выбрано действие.");}
			if (!isset($_POST['id'])){err_message("Не выбраны элементы.");}

			// Удаление блока
			if ($_POST['actions'] == 'delete_item_menu') {

				// Удаление связей
				foreach ($_POST['id'] as $key=>$value) {
					$q_cat = mysql_query("SELECT * FROM base_cat WHERE id_menu='$value'");
					while ($c_cat = mysql_fetch_array($q_cat)){
						switch ($c_cat['type']) {
							case 'text':
								@mysql_query("DELETE FROM m_text WHERE id_cat='{$c_cat['id']}'");
								break;
							case 'news':
								@mysql_query("DELETE FROM m_news WHERE id_cat='{$c_cat['id']}'");
								VLDeleteBaseOptions($c_cat['id']);
								break;
							default:
								break;
						}
						VLClearModule($c_cat['id']);
					}
					@mysql_query("DELETE FROM base_cat WHERE id_menu='$value'");
					@mysql_query("DELETE FROM base_cat_group WHERE id='$value'");
				}
				mysql_free_result($q_cat);

				// Идем обратно
				go("?main=$cat_main&module=$cat_module&action=mod");


				// Удаление разделов блока меню
			}elseif ($_POST['actions'] == 'delete_item_cat') {
				$id = $_GET['id'];

				foreach ($_POST['id'] as $key=>$value) {

					$cat_type = @mysql_result(mysql_query("SELECT type FROM base_cat WHERE id='$value'"),0,0);
					switch ($cat_type) {
						case 'text':
							@mysql_query("DELETE FROM m_text WHERE id_cat='$value'");
							break;
						case 'news':
							@mysql_query("DELETE FROM m_news WHERE id_cat='$value'");
							VLDeleteBaseOptions($value);
							break;
						default:
							break;
					}
					VLClearModule($value);
					mysql_query("DELETE FROM base_cat WHERE id='$value'");
				}

				// Идем обратно
				go("?main=$cat_main&module=$cat_module&action=change_cat&id=$id");
			}
			break;
	}
}
?>