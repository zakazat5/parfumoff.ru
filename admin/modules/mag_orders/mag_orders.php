<?php

ini_set('include_path',ini_get('include_path').':includes/library');
require_once 'Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

require_once 'Int/Service/Amazon/Ses.php';
require_once 'Int/Service/Amazon/Ses/Email.php';
require_once 'Int/Service/Amazon/Ses/Exception.php';

if ($_SESSION['admin'] == 'allow'){
	$cat_module = $_GET['worker'];
	$cat_main 	= $_GET['main'];
    $cat_module = $_GET['worker'];
	$cat_main 	= $_GET['main'];

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $cat_module))
	err_message("У вас нет прав на просмотр.");

	switch ($_GET['action']){

		case "add_reg": /** DONE */

			// Массив заказов
			if(!is_array($_POST['id_orders'])) err_message("Заказы не сформированны.");


			if (isset($_POST['user_id'])){

				$id_user 			= intval($_POST['user_id']);
				$user_percent 		= intval($_POST['user_percent']);
				$user_percent_type 	= intval($_POST['user_percent_type']);
				$user_big_sum_all 	= intval($_POST['user_big_sum_all']);
				$user_big_sum 		= intval($_POST['user_big_sum']);
				$user_region 		= $_POST['user_region'];

			}else {
				err_message("Не выбран клиент.");
			}

			$data['comment'] = $_POST['br_comment'];
			$data['DateEnd'] = $_POST['date1'];
			if(isset($_POST['is_sending'])) $is_sending = 1; else $is_sending = 0;
            $data['payment_type'] = $_POST['payment_type'];
            $data['logistic'] = $_POST['logistic'];
            $data['track'] = mysql_real_escape_string($_POST['track']);
            $data['track_date'] = mysql_real_escape_string($_POST['track_date']);

            $user_data = $db->fetchRow($db->select()->from('m_mag_Users')->where('id=?',$id_user));

            /* -=========================================
			ЗАКАЗ
			*/
            $order_data = array(
                'id_user'=>$id_user,
                'status'=>2,
                'DateAdd'=> new Zend_Db_Expr('NOW()'),
                'DateEnd'=>$data['DateEnd'],
                'DateSendRect'=>new Zend_Db_Expr('NOW()'),
                'is_send_rect'=>$is_sending,
                'dType'=>$user_percent_type,
                'dPercent'=>$user_percent,
                'dSum'=>$user_big_sum_all,
                'Sum'=>$user_big_sum,
                'pComment'=>$data['comment'],
                'id_manager'=>$_SESSION['user_id'],
                'is_phone'=>1,
                'payment_type'=>$data['payment_type'],
				'track'=>$data['track'],
				'track_date'=>$data['track_date'],
                'courier_address_id' => $_POST['courier_address_id']
            );

            $db->insert('m_mag_Orders',$order_data);
            $id_orders = $db->lastInsertId();



			if($id_orders==0) err_message("Ошибка при  добавлении заказа");




			/* -=========================================
			ПОЗИЦИИ ЗАКАЗА
			*/
            $OMS_summ = 0;
            $OMS_offer = '';
			foreach ($_POST['id_orders'] as $value) {

				$id_order 	= intval( $value );
				$kol 		= intval( $_POST['kol_orders_'.$value] );

				if ($kol == 0) continue;

				$c_order = mysql_fetch_assoc(mysql_query("SELECT * FROM m_catalog_data_order WHERE id='$id_order' LIMIT 1 "));
				$c_catalog_data = mysql_fetch_assoc(mysql_query("SELECT id, title, id_catalog, pol FROM m_catalog_data WHERE id='{$c_order['id_catalog_data']}' LIMIT 1"));
				$c_brend = mysql_fetch_assoc(mysql_query("SELECT id,title FROM m_catalog WHERE id='{$c_catalog_data['id_catalog']}' LIMIT 1 "));

                $title = '';
                if ($c_catalog_data['pol']){
                    $title.= "[{$c_catalog_data['pol']}-{$c_order['sklad']}]";
                }
                if ($c_brend && $c_brend['title']){
                    $title.= ' '.$c_brend['title'];
                }
                $title.= $c_catalog_data['title'];
                if ($aroma_type[$c_order['type']]){
                    $title.= ' '.$aroma_type[$c_order['type']];
                }
                if ($c_order['v']!='' && $c_order['v']!=0){
                    $title.= " {$c_order['v']}ml";
                }

				//$title = "[{$c_catalog_data['pol']}-{$c_order['sklad']}] {$c_brend['title']} {$c_catalog_data['title']} {$aroma_type[$c_order['type']]} {$c_order['v']}ml";
				$title_mail .= "[{$c_catalog_data['pol']}] {$c_brend['title']} {$c_catalog_data['title']} {$aroma_type[$c_order['type']]} {$c_order['v']}ml<br />";

				// доп данные
				$price_usd = $c_order["price_usd".$c_order['sklad']];
				$price_usd 	= str_replace(",",".", $price_usd);
				$margin = catalog_creat_price_level($price_usd);

                $OMS_summ =+ $margin;
                $OMS_offer .= $c_catalog_data['id'].',';

				mysql_query("
				INSERT INTO m_mag_OrdersSum
					(
						id_order,
						id_user,
						id_brend,
						id_catalog_data,
						id_catalog_data_order,
						articul_catalog_data_order,
						sklad,
						price_usd,
						margin,
						title,
						sum,
						kol
					)
					VALUES
					(
						'$id_orders',
						'$id_user',
						'{$c_brend['id']}',
						'{$c_catalog_data['id']}',
						'{$c_order['id']}',
						'{$c_order['articul']}',
						'{$c_order['sklad']}',
						'$price_usd',
						'$margin',
						'$title',
						'{$c_order['price']}',
						'$kol'
					)
				");
			}

            $analytics->Purchase($id_orders);

            /** OMS */
            if(OMS_ON)
                file_get_contents('http://api.ohmystats.com/api/v1/projects/'.OMS_PROJECT_ID.'/orders.json?key='.OMS_REST_KEY.'&user_id='.$id_user.'&order_id='.$id_orders.'&offer_id='.rtrim($OMS_offer,',').'&sum='.intval($OMS_offer*OMS_K));
            /** ******/

			/* -=========================================
			Отправка квитанции
			*/
			if ($user_region=='region') {
				if ( $is_sending==1 ) {
					require_once 'Zend/Mail.php';

					$client = mysql_fetch_array(mysql_query("SELECT * FROM m_mag_Users WHERE id='$id_user' "));
					$client['order_id'] 	= $id_orders;
					$client['big_sum'] 		= $user_big_sum_all + 200;
					$client['number'] 		= $id_orders;
					$client['orders'] 		=  $title_mail;
					$client['hash']			= md5($id_orders.$id_user);
					$client['loginHash']    = md5($client['email'].$client['passw']);


					global $global_phone;
					$client['global_phone'] = $global_phone;


					$message = parser('receipt.tpl',  $client, 0);
					//$message = iconv("WINDOWS-1251", "UTF-8", $message);

                    $message_body = parser('mail_message_order.tpl', $client, 0);

                    $client['fio'] =  $client['fio'];

                    /*Int_Service_Amazon_Ses::setKeys('AKIAJRZF4VNOC6YNBAZQ', 'Z3XtxspW9qkUUf/8K3WiYYwGyeLxOvPKvU47gvFL');

			        $mail = new Int_Service_Amazon_Ses_Email();
			        $mail->addTo($client['email'])
			            ->setFrom('office@parfumoff.ru', 'parfumoff')
			            ->setSubject('parfumoff.ru - квитанция на оплату заказа '.$id_orders.'')
			            ->setReturnPath('office@parfumoff.ru')
			            ->setBodyHtml($message_body, 'utf-8');

			        $ses = new Int_Service_Amazon_Ses();
			        $messageId = $ses->sendEmail($mail);*/
				}
			}

			// переход
			if (isset($_POST['save'])){
				go("?main=$cat_main&module=mag_order_message&action=mod&id_order=$id_orders&date={$data['DateEnd']}&is_new=1");
			}

			break;


        // !
		case "edit_reg":

			// Массив заказов
			if(!is_array($_POST['id_orders'])) err_message("Заказы не сформированны.");

			if( $_GET['status']==0 ){
				err_message("Этот заказ отклонен. Редактирование невозможно.");
			}elseif ( $_GET['status']==3 ) {
				err_message("Этот заказ доставлен. Редактирование невозможно.");
			}

			$id_order = intval($_GET['id_order']);

            //выборка данных для сравнения до/после
            //до
            $order_before = $db->fetchRow("SELECT * FROM m_mag_Orders WHERE id=?",$id_order);
            $order_sum_before  =$db->fetchAll("SELECT * FROM m_mag_OrdersSum WHERE id_order=?",$id_order);

			if (isset($_POST['user_id'])){

				$id_user 			= intval($_POST['user_id']);
				$user_percent 		= intval($_POST['user_percent']);
				$user_percent_type 	= intval($_POST['user_percent_type']);
				$user_big_sum_all 	= intval($_POST['user_big_sum_all']);
				$user_big_sum 		= intval($_POST['user_big_sum']);
				$user_region 		= $_POST['user_region'];

			}else {
				err_message("Не выбран клиент.");
			}

			$data['pComment'] = $_POST['br_comment'];
			$data['DateEnd'] = $_POST['date1'];
            $data['payment_type'] = $_POST['payment_type'];
            $data['logistic'] = $_POST['logistic'];
            $data['track'] = mysql_real_escape_string($_POST['track']);
            $data['track_date'] = mysql_real_escape_string($_POST['track_date']);
            $data['courier_address_id'] = $_POST['courier_address_id'];



			// флаг для квитанции
			if(isset($_POST['is_sending'])) {
				$is_sending = 1;
				$is_sending_d = 1;
			}else {

				if(isset($_POST['is_sending_d'])) {
					$is_sending_d = 0;
					$is_sending = 1;
				}else{
					$is_sending_d = 0;
					$is_sending = 0;
				}
			}

			// флаг для идентификатора
			if( $_POST['PostCode']!='' AND $_POST['date2']!='' ) {
				$is_sending_post = 1;
				$is_sending_post_d = 1;
			}else {

				if(isset($_POST['is_sending_post'])) {
					$is_sending_post_d = 0;
					$is_sending_post = 1;
				}else{
					$is_sending_post_d = 0;
					$is_sending_post = 0;
				}
			}

            $data['id_manager'] = new Zend_Db_Expr("IF(`id_manager`>0, `id_manager`, '".intval($_SESSION['user_id'])."')");
            $data['status'] = 2;
            $data['dPercent'] = $user_percent;
            $data['dSum'] = $user_big_sum_all;
            $data['Sum'] = $user_big_sum;
            $data['DateEdit'] = new Zend_Db_Expr('NOW()');
            $data['DateSendRect'] = new Zend_Db_Expr('NOW()');
            $data['DateSendPost'] = new Zend_Db_Expr('NOW()');
            $data['DateSendPost'] = new Zend_Db_Expr('NOW()');
            $data['pPostNumber'] = $_POST['PostCode'];
            $data['is_send_rect'] = $is_sending;
            $data['is_send_post'] = $is_sending_post;
            $data['delivery_cost'] = $_POST['delivery_cost'];


            //order update
            $update = $db->update('m_mag_Orders',$data,'id ='.(int)$id_order);

            $magOrdersSumSrc = $db->fetchAll($db->select()->from('m_mag_OrdersSum')->where('id_order=?',$id_order));
            foreach($magOrdersSumSrc as $magOrdersSumItem){
                 $magOrdersSum[$magOrdersSumItem['articul_catalog_data_order']] = $magOrdersSumItem['kol'];
            }

            $intersect = array_intersect_assoc($_POST['ordersSum_kol'],$magOrdersSum);
            $diff = array_diff_assoc($_POST['ordersSum_kol'],$magOrdersSum);
            $diff_del = array_diff_assoc($magOrdersSum,$_POST['ordersSum_kol']);
            foreach ($_POST['ordersSum_kol'] as $articul=>$kol) {
                //пропускаем неизменные
                if (isset($intersect[$articul])){
                    continue;
                }
                //update kol
                if (isset($diff[$articul]) && isset($magOrdersSum[$articul])){
                    echo 'update'.$articul;
                    $where[] = $db->quoteInto('articul_catalog_data_order = ?',$articul);
                    $where[] = $db->quoteInto('id_order = ?',$id_order);
                    $db->update('m_mag_OrdersSum',array('kol'=>$diff[$articul]),implode(' AND ',$where));
                }
                //add new
                if (isset($diff[$articul]) && !isset($magOrdersSum[$articul])){
                    echo 'add new'.$articul;
                    $c_order = $db->fetchRow($db->select()->from('m_catalog_data_order')->where('articul=?',$articul));
                    $c_catalog_data = $db->fetchRow($db->select()->from('m_catalog_data')->where('id=?',$c_order['id_catalog_data']));
                    $c_brend = $db->fetchRow($db->select()->from('m_catalog')->where('id=?',$c_catalog_data['id_catalog']));

                    $title = '';
                    if ($c_catalog_data['pol']){
                        $title.= "[{$c_catalog_data['pol']}-{$c_order['sklad']}]";
                    }
                    if ($c_brend && $c_brend['title']){
                        $title.= ' '.$c_brend['title'];
                    }
                    $title.= $c_catalog_data['title'];
                    if ($aroma_type[$c_order['type']]){
                        $title.= ' '.$aroma_type[$c_order['type']];
                    }
                    if ($c_order['v']!='' && $c_order['v']!=0){
                        $title.= " {$c_order['v']}ml";
                    }

                    $title_mail .= "[{$c_catalog_data['pol']}] {$c_brend['title']} {$c_catalog_data['title']} {$aroma_type[$c_order['type']]} {$c_order['v']}ml<br />";
                    // доп данные
                    $price_usd = $c_order["price_usd".$c_order['sklad']];
                    $price_usd 	= str_replace(",",".", $price_usd);
                    $margin = catalog_creat_price_level($price_usd);
                    $m_mag_OrdersSum['id_order'] = $id_order;
                    $m_mag_OrdersSum['id_user'] = $id_user;
                    $m_mag_OrdersSum['id_brend'] = ($c_brend)?$c_brend['id']:0;
                    $m_mag_OrdersSum['id_catalog_data'] = $c_catalog_data['id'];
                    $m_mag_OrdersSum['id_catalog_data_order'] = $c_order['id'];
                    $m_mag_OrdersSum['articul_catalog_data_order'] = $c_order['articul'];
                    $m_mag_OrdersSum['sklad'] = $c_order['sklad'];
                    $m_mag_OrdersSum['price_usd'] = $price_usd;
                    $m_mag_OrdersSum['margin'] = $margin;
                    $m_mag_OrdersSum['title'] = $title;
                    $m_mag_OrdersSum['sum'] = $c_order['price'];
                    $m_mag_OrdersSum['kol'] = $kol;
                    $db->insert('m_mag_OrdersSum',$m_mag_OrdersSum);
                }
            }
            //delete items
            if (isset($diff_del)){
                foreach($diff_del as $articul=>$kol){
                    if (isset($_POST['ordersSum_kol'][$articul])){
                        continue;
                    }
                    $where[] = $db->quoteInto('articul_catalog_data_order = ?',$articul);
                    $where[] = $db->quoteInto('id_order = ?',$id_order);
                    $db->delete('m_mag_OrdersSum',implode(' AND ',$where));
                }
            }

			/* -=========================================
			Отправка квитанции
			*/
			if ( $user_region=='region' ) {
				if ( $is_sending_d==1 ) {
					require_once 'Zend/Mail.php';

					$client = mysql_fetch_array(mysql_query("SELECT * FROM m_mag_Users WHERE id='$id_user' "));
					$client['order_id'] 	= $id_order;
					$client['big_sum'] 		= $user_big_sum_all + 200;
					$client['number'] 		= $id_order;
					$client['orders'] 		= $title_mail;
					$client['hash']			= md5($id_order.$id_user);
					$client['loginHash']    = md5($client['email'].$client['passw']);


					global $global_phone;
					$client['global_phone'] = $global_phone;


					$message = parser('receipt.tpl', $client, 0);
					//$message = iconv("WINDOWS-1251", "UTF-8", $message);

                    $message_body = parser('mail_message_order.tpl', $client, 0);

                   /* $mail = new Zend_Mail('utf-8');
                    $mail->setBodyHtml($message_body, 'utf-8');
                    $mail->setFrom('info@parfumoff.ru', 'parfumoff');
                    $mail->addTo($client['email']);
                    $mail->setSubject('Ваша заявка принята [parfumoff.ru]');
                    $mail->send();*/

				}
			}

			/* -=========================================
			Отправка квитанции
			*/
			if ( $user_region=='region' ) {
				if ($is_sending_post_d==1) {

					if ( $_POST['PostCode']!='' AND $_POST['date2']!='' ) {
						require_once 'Zend/Mail.php';

						$client = mysql_fetch_array(mysql_query("SELECT * FROM m_mag_Users WHERE id='$id_user' "));
						$client['order_id'] = $id_order;
						$client['post_code'] = $_POST['PostCode'];
						$client['date2'] = $_POST['date2'];

						global $global_phone;
						$client['global_phone'] = $global_phone;


						/*$message_body = parser('mail_message_order_post.tpl', $client, 0);

                        $mail = new Zend_Mail('utf-8');
                        $mail->setBodyHtml($message_body, 'utf-8');
                        $mail->setFrom('info@parfumoff.ru', 'parfumoff');
                        $mail->addTo($client['email']);
                        $mail->setSubject('Ваша заявка принята [parfumoff.ru]');
                        $mail->send();*/

					}

				}
			}

            //выборка данных для сравнения до/после
            //после
            $order_after =  $db->fetchRow("SELECT * FROM m_mag_Orders WHERE id=?",$id_order);
            $order_sum_after = $db->fetchAll("SELECT * FROM m_mag_OrdersSum WHERE id_order=?",$id_order);

            //сравниваем данные заказа без товаров
            $result = array_diff_assoc($order_after,$order_before);
            $timestamp  = $result['DateEdit'];
            $pid = $id_order;

            foreach($result as $key=>$diff){

                switch($key){
                    case 'payment_type':
                        $aLog[] = array(
                            'action'=>'payment_type',
                            'value_before'=>$order_before['payment_type'],
                            'value_after'=>$order_after['payment_type'],
                        );
                        break;
                    case 'pComment':
                        $aLog[] = array(
                            'action'=>'pComment',
                            'value_before'=>$order_before['pComment'],
                            'value_after'=>$order_after['pComment'],
                        );
                        break;
                    case 'DateEnd':
                        $aLog[] = array(
                            'action'=>'DateEnd',
                            'value_before'=>$order_before['DateEnd'],
                            'value_after'=>$order_after['DateEnd'],
                        );
                        break;
                    case 'is_send_rect':
                        $aLog[] = array(
                            'action'=>'is_send_rect',
                            'value_before'=>$order_before['is_send_rect'],
                            'value_after'=>$order_after['is_send_rect'],
                        );
                        break;
                    case 'track':
                        $aLog[] = array(
                            'action'=>'track',
                            'value_before'=>$order_before['track'],
                            'value_after'=>$order_after['track'],
                        );
                        break;
                    case 'track_date':
                        $aLog[] = array(
                            'action'=>'track_date',
                            'value_before'=>$order_before['track_date'],
                            'value_after'=>$order_after['track_date'],
                        );
                        break;
                    case 'courier_address_id':
                        $aLog[] = array(
                            'action'=>'courier_address_id',
                            'value_before'=>$order_before['courier_address_id'],
                            'value_after'=>$order_after['courier_address_id'],
                        );
                        break;
                    case 'status':
                        $aLog[] = array(
                            'action'=>'status',
                            'value_before'=>$order_before['status'],
                            'value_after'=>$order_after['status'],
                        );
                        break;
                }
            }

            //выставляем ключ=id товара
            foreach($order_sum_before as $item){
                $aOrder_sum_before[$item['id_catalog_data_order']] = $item;
            }
            foreach($order_sum_after as $item){
                $aOrder_sum_after[$item['id_catalog_data_order']] = $item;
            }

            //поиск одинаковых ключей
            $aIntersect = array_intersect_key($aOrder_sum_before,$aOrder_sum_after);
            foreach($aIntersect as $key=>$item){
                //изменилось колчисевто
                if ($aOrder_sum_before[$key]['kol']!=$aOrder_sum_after[$key]['kol']){
                    $aLog[] = array(
                        'action'=>'change_kol',
                        'text'=>$aOrder_sum_before[$key]['title'],
                        'value_before'=>$aOrder_sum_before[$key]['kol'],
                        'value_after'=>$aOrder_sum_after[$key]['kol'],
                    );
                }
            }

            foreach($aIntersect as $key=>$item){
                unset($aOrder_sum_before[$key]);
                unset($aOrder_sum_after[$key]);
            }

            //delete
            foreach($aOrder_sum_before as $key=>$item){
                $aLog[] = array(
                    'action'=>'delete',
                    'text'=>$aOrder_sum_before[$key]['title'],
                    'value_before'=>$aOrder_sum_before[$key]['kol'],
                    'value_after'=>0,
                );
            }

            foreach($aOrder_sum_after as $key=>$item){
                $aLog[] = array(
                    'action'=>'add',
                    'text'=>$aOrder_sum_after[$key]['title'],
                    'value_before'=>0,
                    'value_after'=>$aOrder_sum_after[$key]['kol'],
                );
            }




            foreach($aLog as $log){
                $log['user_id'] = $_SESSION['user_id'];
                $log['pid'] = $id_order;
                $log['module_name'] = $cat_module;
                $log['date'] = new Zend_Db_Expr('NOW()');

                if (is_null($log['value_before'])){
                    $log['value_before'] ='';
                }

                $db->insert('m_mag_Log',$log);
            }

			// переход
			if (isset($_POST['save'])){
				go("?main=$cat_main&module=mag_order_message&action=mod&id_order=$id_order&date={$data['DateEnd']}");
			}
			break;



		case "add": /** DONE */

			// Массив заказов
			if(!is_array($_POST['id_orders'])) err_message("Заказы не сформированны.");


			if( $_POST['br_fio'] != '' ){

				if(empty($_POST['br_phone']))
				err_message("Не заполнены поля Телефон или Адресс клиента.");

				$data['fio'] 		= trim( $_POST['br_fio'] );
				$data['phone'] 		= trim( $_POST['br_phone'] );
				$data['address'] 	= trim( $_POST['br_address'] );
				$data['email'] 		= trim( $_POST['br_email'] );


                //new data
                $data['address-region'] = $_POST['region'];
                $data['address-part'] = $_POST['part'];
                $data['address-other'] = $_POST['other'];
                $data['address-city'] = $_POST['city'];
                $data['address-settlement'] = $_POST['settlement'];
                $data['address-street'] = $_POST['street'];
                $data['address-house'] = $_POST['house'];
                $data['address-corpus'] = $_POST['corpus'];
                $data['address-floor'] = $_POST['floor'];
                $data['address-stroenie'] = $_POST['stroenie'];
                $data['address-domophone'] = $_POST['domophone'];
                $data['address-flat'] = $_POST['flat'];
                $data['postcode'] = $_POST['regs_postcode'];

                $data['fio_data'] = (trim($_POST['fio_data']));
                $data['address_data'] = (trim($_POST['address_data']));


                $address_data = json_decode($data['address_data']);
                $region = $address_data->region;
                $data['region'] = ($region=='г Москва')?'moscow':'region' ;
			}else {
				err_message("Ошибка, клиент не определен.");
			}

			$data['comment'] 		= $_POST['br_comment'];
			$data['DateEnd'] 		= $_POST['date1'];

			$user_big_sum 			= intval($_POST['user_big_sum']);
			$user_percent 			= intval($_POST['user_percent']);
			$user_percent_type 		= ($_POST['user_percent']==0) ? 0 : 2;
			$user_big_sum_all 		= intval($_POST['user_big_sum_all']);


			/* -=========================================
			ЗАКАЗ
			*/
			mysql_query("
			INSERT INTO m_mag_Orders
				(
					status,
					dType,
					dPercent,
					dSum,
					Sum,
					DateAdd,
					DateEnd,
					pComment,
					pFio,
					pEmail,
					pPhone,
					pAddress,
					pRegion,
                    id_manager,
                    is_phone,
                    fio_data,
                    address_data,
                    address_region,
                    address_part,
                    address_other,
                    address_city,
                    address_settlement,
                    address_street,
                    address_house,
                    address_corpus,
                    address_stroenie,
                    address_floor,
                    address_flat,
                    address_domophone,
                    postcode
				)
				VALUES
				(
					2,
					'$user_percent_type',
					'$user_percent',
					'$user_big_sum_all',
					'$user_big_sum',
					NOW(),
					'{$data['DateEnd']}',
					'{$data['comment']}',
					'{$data['fio']}',
					'{$data['email']}',
					'{$data['phone']}',
					'{$data['address']}',
					'{$data['region']}',
                    '{$_SESSION['user_id']}',
                    '1',
                    '{$data['fio_data']}',
                    '{$data['address_data']}',
                    '{$data['address-region']}',
                    '{$data['address-part']}',
                    '{$data['address-other']}',
                    '{$data['address-city']}',
                    '{$data['address-settlement']}',
                    '{$data['address-street']}',
                    '{$data['address-house']}',
                    '{$data['address-corpus']}',
                    '{$data['address-stroenie']}',
                    '{$data['address-floor']}',
                    '{$data['address-flat']}',
                    '{$data['address-domophone']}',
                    '{$data['postcode']}'
				)
			");




			$id_orders = DGetLast();


			/* -=========================================
			ПОЗИЦИИ ЗАКАЗА
			*/
            $OMS_summ = 0;
            $OMS_offer = '';
			foreach ($_POST['id_orders'] as $value) {

				$id_order 	= intval( $value );
				$kol 		= intval( $_POST['kol_orders_'.$value] );

				if ($kol == 0) continue;

				$c_order = mysql_fetch_assoc(mysql_query("SELECT * FROM m_catalog_data_order WHERE id='$id_order' LIMIT 1 "));
				$c_catalog_data = mysql_fetch_assoc(mysql_query("SELECT id, title, id_catalog, pol FROM m_catalog_data WHERE id='{$c_order['id_catalog_data']}' LIMIT 1"));
				$c_brend = mysql_fetch_assoc(mysql_query("SELECT id,title FROM m_catalog WHERE id='{$c_catalog_data['id_catalog']}' LIMIT 1 "));

				$title = "[{$c_catalog_data['pol']}-{$c_order['sklad']}] {$c_brend['title']} {$c_catalog_data['title']} {$aroma_type[$c_order['type']]} {$c_order['v']}ml";


				// доп данные
				$price_usd = $c_order["price_usd".$c_order['sklad']];
				$price_usd 	= str_replace(",",".", $price_usd);
				$margin = catalog_creat_price_level($price_usd);

                $OMS_summ += $margin;
                $OMS_offer .= $c_catalog_data['id'].',';

				mysql_query("
				INSERT INTO m_mag_OrdersSum
					(
						id_order,
						id_brend,
						id_catalog_data,
						id_catalog_data_order,
						sklad,
						price_usd,
						margin,
						title,
						sum,
						kol
					)
					VALUES
					(
						'$id_orders',
						'{$c_brend['id']}',
						'{$c_catalog_data['id']}',
						'{$c_order['id']}',
						'{$c_order['sklad']}',
						'$price_usd',
						'$margin',
						'$title',
						'{$c_order['price']}',
						'$kol'
					)
				");
			}

            /** OMS */
            if(OMS_ON)
                file_get_contents('http://api.ohmystats.com/api/v1/projects/'.OMS_PROJECT_ID.'/orders.json?key='.OMS_REST_KEY.'&user_id=1&order_id='.$id_orders.'&offer_id='.rtrim($OMS_offer,',').'&sum='.intval($OMS_summ*OMS_K));
            /** ******/

			//сохраняем данные e-commerce
			//$_SESSION["gaq"] = gaq($id_orders);

			// переход
			if (isset($_POST['save'])){
				go("?main=$cat_main&module=mag_order_message&action=mod&id_order=$id_orders&date={$data['DateEnd']}");
			}
			break;

        /*
		case "edit":

			// Массив заказов
			if(!is_array($_POST['id_orders'])) err_message("Заказы не сформированны.");

			if( $_GET['status']==0 ){
				err_message("Этот заказ отклонен. Редактирование невозможно.");
			}elseif ( $_GET['status']==3 ) {
				err_message("Этот заказ доставлен. Редактирование невозможно.");
			}

			$id_order = intval($_GET['id_order']);



			//CLIENTS

			if( $_POST['br_fio'] != '' ){

				if(empty($_POST['br_phone']) AND empty($_POST['br_address']))
				err_message("Не заполнены поля Телефон или Адресс клиента.");

				$data['fio'] 		= trim( $_POST['br_fio'] );
				$data['phone'] 		= trim( $_POST['br_phone'] );
				$data['address'] 	= trim( $_POST['br_address'] );
				$data['email'] 		= trim( $_POST['br_email'] );

                //new data
                $data['address-region'] = $_POST['region'];
                $data['address-part'] = $_POST['part'];
                $data['address-other'] = $_POST['other'];
                $data['address-city'] = $_POST['city'];
                $data['address-settlement'] = $_POST['settlement'];
                $data['address-street'] = $_POST['street'];
                $data['address-house'] = $_POST['house'];
                $data['address-corpus'] = $_POST['corpus'];
                $data['address-floor'] = $_POST['floor'];
                $data['address-stroenie'] = $_POST['stroenie'];
                $data['address-domophone'] = $_POST['domophone'];
                $data['address-flat'] = $_POST['flat'];
                $data['postcode'] = $_POST['regs_postcode'];
                $data['payment_type'] = $_POST['payment_type'];

                $data['fio_data'] = (trim($_POST['fio_data']));
                $data['address_data'] = (trim($_POST['address_data']));


                $address_data = json_decode(iconv('cp1251','utf8',$data['address_data']));
                $region = (iconv('utf-8','cp1251',$address_data->region));
                $data['region'] = ($region=='г Москва')?'moscow':'region' ;

			}else {
				err_message("Ошибка, клиент не определен.");
			}

			$data['comment'] = $_POST['br_comment'];
			$data['DateEnd'] = $_POST['date1'];

			$user_big_sum 			= intval($_POST['user_big_sum']);
			$user_percent 			= intval($_POST['user_percent']);
			$user_percent_type 		= ($_POST['user_percent']==0) ? 0 : 2;
			$user_big_sum_all 		= intval($_POST['user_big_sum_all']);


			// ЗАКАЗ

			mysql_query("
				UPDATE
					m_mag_Orders
				SET
					`id_manager` = IF(`id_manager`>0, `id_manager`, '".intval($_SESSION['user_id'])."'),
                    dType='$user_percent_type',
					dPercent='$user_percent',
					dSum='$user_big_sum_all',
					Sum='$user_big_sum',
					pComment='{$data['comment']}',
					pFio='{$data['fio']}',
					pEmail='{$data['email']}',
					pPhone='{$data['phone']}',
					pAddress='{$data['address']}',
					pRegion='{$data['region']}',
					DateEdit=NOW(),
					DateEnd='{$data['DateEnd']}',
					fio_data='{$data['fio_data']}',
                    address_data='{$data['address_data']}',
                    address_region='{$data['address-region']}',
                    address_part='{$data['address-part']}',
                    address_other='{$data['address-other']}',
                    address_city='{$data['address-city']}',
                    address_settlement='{$data['address-settlement']}',
                    address_street='{$data['address-street']}',
                    address_house='{$data['address-house']}',
                    address_corpus='{$data['address-corpus']}',
                    address_stroenie='{$data['address-stroenie']}',
                    address_floor='{$data['address-floor']}',
                    address_flat= '{$data['address-flat']}',
                    address_domophone='{$data['address-domophone']}',
                    postcode= '{$data['postcode']}',
                    payment_type= '{$data['payment_type']}'
				WHERE id='$id_order'
			");


            print_r(mysql_error());




			//ПОЗИЦИИ ЗАКАЗА/

			//GAQ
			$result = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='$id_order' ");
			while($row = mysql_fetch_assoc($result)){
				$gaq_data['old'][$row['id_catalog_data_order']] =$row;
			}

			mysql_query("DELETE FROM m_mag_OrdersSum WHERE id_order='$id_order' ");

			foreach ($_POST['id_orders'] as $value) {

				$id_order_item = intval( $value );
				$kol = intval( $_POST['kol_orders_'.$value] );

				if ($kol == 0) continue;

				$c_order = mysql_fetch_assoc(mysql_query("SELECT * FROM m_catalog_data_order WHERE id='$id_order_item' LIMIT 1 "));
				$c_catalog_data = mysql_fetch_assoc(mysql_query("SELECT id, title, id_catalog, pol FROM m_catalog_data WHERE id='{$c_order['id_catalog_data']}' LIMIT 1"));
				$c_brend = mysql_fetch_assoc(mysql_query("SELECT id,title FROM m_catalog WHERE id='{$c_catalog_data['id_catalog']}' LIMIT 1 "));

				$title = "[{$c_catalog_data['pol']}-{$c_order['sklad']}] {$c_brend['title']} {$c_catalog_data['title']} {$aroma_type[$c_order['type']]} {$c_order['v']}ml";


				// доп данные
				$price_usd = $c_order["price_usd".$c_order['sklad']];
				$price_usd 	= str_replace(",",".", $price_usd);
				$margin = catalog_creat_price_level($price_usd);

				mysql_query("
				INSERT INTO m_mag_OrdersSum
					(
						id_order,
						id_brend,
						id_catalog_data,
						id_catalog_data_order,
						sklad,
						price_usd,
						margin,
						title,
						sum,
						kol
					)
					VALUES
					(
						'$id_order',
						'{$c_brend['id']}',
						'{$c_catalog_data['id']}',
						'{$c_order['id']}',
						'{$c_order['sklad']}',
						'$price_usd',
						'$margin',
						'$title',
						'{$c_order['price']}',
						'$kol'
					)
				");

			}

			//GAQ
			$gaq_data['delete'] = array();
			$gaq_data['new'] = array();
			$result = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='$id_order' ");
			while($row = mysql_fetch_assoc($result)){
				$gaq_data['new'][$row['id_catalog_data_order']] =$row;
			}

			foreach($gaq_data['old'] as $key=>$item){
				//проверка на удаленные
				if (!isset($gaq_data['new'][$key])){
					$gaq_data['delete'][$key]=1;
				}
			}

			foreach($gaq_data['new'] as $key=>$item){
				//проверка на добавленные
				if (!isset($gaq_data['old'][$key])){
					$gaq_data['add'][$key]=$item;
				}else{
					if (($dif_kol = $item['kol']-$gaq_data['old'][$key]['kol'])!=0){
						$gaq_data['kol'][$key]=$dif_kol;
					}

				}
			}


			$gaq = gaq_edit($id_order,$gaq_data);
			$_SESSION['gaq'] = $gaq;

			// переход
			if (isset($_POST)){
				go("?main=$cat_main&module=mag_order_message&action=mod&id_order=$id_order&date={$data['DateEnd']}");
			}
			break;
        */

	}
}