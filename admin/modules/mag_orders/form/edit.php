<?php
if ($_SESSION['admin'] == 'allow'){

    require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User.php";

	$sm = new Smarty_Admin($valinorConfig['interface.themes']);



	// Определение переменных
	$main 		= "mag";
	$module 	= "mag_orders";

    // Проверка прав доступа
    if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module))
        err_message("У вас нет прав на просмотр.");
	
	if($_GET['status']==1){
		$cat_name = 'Заказы (новые)';
	}elseif ($_GET['status']==2){
		$cat_name = 'Заказы (подтвержденные)';
	}elseif ($_GET['status']==3){
		$cat_name = 'Заказы (доставленные)';
	}elseif ($_GET['status']==0){
		$cat_name = 'Заказы (отклоненные)';
	}elseif ($_GET['status']==5){
		$cat_name = 'на складе (отклоненные)';
	}elseif ($_GET['status']==6){
		$cat_name = 'на складе (неполные)';
	}

	$id_order = intval($_GET['id_order']);
	$status = intval($_GET['status']);


	/* -=========================================
	листинг брендов
	*/
	$q_brend = mysql_query("SELECT id,title FROM m_catalog WHERE id_cat=82 ORDER BY title ");
	while ($c_brend = mysql_fetch_array($q_brend)){
		$m_list_brend .= "<option value=" .$c_brend['id']. " >" . $c_brend['title'] . '</option>';
	}
	$sm->assign('sel_brend', $m_list_brend);

	
	

	/* -=========================================
	ЗАКАЗ
	*/
	$c_order = mysql_fetch_assoc(mysql_query("SELECT * FROM m_mag_Orders WHERE id='$id_order' LIMIT 1 "));
    $c_order['logistic_value'] = $c_order['logistic'];
    $c_order['logistic'] = explode(';',$c_order['logistic']);


	$sm->assign('order', $c_order);
	$sm->assign('address', $c_order);

	
	
	/* -=========================================
	ЮЗЕР
	*/
	if($c_order['id_user']!=0){

		$c_user = mysql_fetch_assoc(mysql_query("SELECT * FROM m_mag_Users WHERE id='{$c_order['id_user']}' LIMIT 1 "));
        $sm->assign('address', $c_user);

        $content['prefix'] = ($c_user['region']=='region')?'RPF':'MPF';
        $prefix =$content['prefix'];

		$sm->assign('user', $c_user);

        //refactoring user
        $userClass = new User();
        $oUser = $userClass->getById($c_order['id_user']);
        $sm->assign('oUser', $oUser);

		$shablon = 'edit_reg.tpl';

    //редактиирование заказа старого типа
	}else {
        //готовим юзера
        $user_data = array();
        //temporary email
        $user_data['address'] = $c_order['pAddress'];
        $c_order['prefix'] = ($c_order['pRegion']?'RPF':'MPF');
        $prefix =$c_order['prefix'];

        //dadata cleaner
        if ($c_order['pRegion']=='moscow'){
            $c_order['pAddress'] = 'г Москва '.$c_order['pAddress'];
            $user_data['region'] = 'moscow';
        }else{
            $user_data['region'] = 'region';
        }

        $data = dadata_get('https://dadata.ru/api/v2/clean', $c_order['pAddress'],$c_order['pFio'],$c_order['pPhone']);
        $json = json_decode($data);
        print_r($json);




        if (isset($json->detail) && $json->detail=='Zero balance') return false;
        //set dadata values
        //address

        $user_data['address_data'] =$data;

        $user_data['fio'] = $json->data[0][1]->name . ' ' . $json->data[0][1]->surname . ' ' . $json->data[0][1]->patronymic;
        $user_data['address_region'] = $json->data[0][0]->region_type . ' ' . $json->data[0][0]->region;

        //fix moscow/piter
        if ($json->data[0][0]->city==null && ($user_data['address_region']=='г Москва'||$user_data['address_region']=='г Санкт-Петербург')){
            $user_data['address_city'] = $json->data[0][0]->region_type . ' ' . $json->data[0][0]->region;
        }else{
            $user_data['address_city'] = $json->data[0][0]->city_type . ' ' . $json->data[0][0]->city;
        }

        $user_data['address_settlement'] = $json->data[0][0]->settlement_type . ' ' . $json->data[0][0]->settlement;

        $user_data['address_street'] = $json->data[0][0]->street_type . ' ' . $json->data[0][0]->street;
        $user_data['address_house'] = $json->data[0][0]->house_type . ' ' . $json->data[0][0]->house;
        preg_match_all('/([\w\/]+) ([\d\w\/]+)/i', $user_data['address_house'],$matches);
        foreach($matches[1] as $key=>$match){
            switch($match){
                case 'к':
                    $user_data['address_corpus'] =  $matches[2][$key];
                    $user_data['address_house'] =  '';
                    break;
                case 'стр':
                    $user_data['address_stroenie'] =  $matches[2][$key];
                    $user_data['address_house'] =  '';
                    break;
                case 'д':
                    $user_data['address_house'] =  $matches[2][$key];
                    break;
                default:
                    $user_data['address_house'] =  $matches[1][$key].' '.$matches[2][$key];
                    break;
            }
        }


        $user_data['address_flat'] = $json->data[0][0]->flat_type . ' ' . $json->data[0][0]->flat;
        $user_data['postcode'] = ($json->data[0][0]->postal_code!=null)?$json->data[0][0]->postal_code:'';

        if ($json->data[0][1]->gender == "Ж") {
            $user_data['pol'] = 'F';
        }

        if ($json->data[0][1]->gender == "М") {
            $user_data['pol'] = 'M';
        }

        $user_data['name'] = ($json->data[0][1]->name)?$json->data[0][1]->name:'';
        $user_data['surname'] = ($json->data[0][1]->surname)?$json->data[0][1]->surname:'';
        $user_data['patronymic'] = ($json->data[0][1]->patronymic)?$json->data[0][1]->patronymic:'';
        $user_data['phone'] = preg_replace('/[^0-9]/', '', $json->data[0][2]->phone);
        $user_data['date_register'] = new Zend_Db_Expr('NOW()');

        if ($user_data['email']==''){
            //выставляем случайное значение email, после сохранения обновим
            $user_data['email'] = md5($c_order['id'].'@parfumoff.ru');
            //сохраняем юзера
            $db->insert('m_mag_Users', $user_data);
            $id_user = $db->lastInsertId();
            //выставляем правильный email
            $db->update('m_mag_Users', array('email'=>$id_user.'@parfumoff.ru'),'id='. $id_user);
        }else{
            $db->insert('m_mag_Users', $user_data);
        }

        $db->update('m_mag_Orders', array('id_user'=>$id_user),'id='. $c_order['id']);

        //reload page
        header('Location: '.$_SERVER['REQUEST_URI']);
		$shablon = 'edit.tpl';
	}

	/* -=========================================
	ПОЗИЦИИ ЗАКАЗА
	*/
	$q_order_sum = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='$id_order' ORDER BY id  ");
	while ($c_order_sum = mysql_fetch_array($q_order_sum)){
		$big_sum += $c_order_sum['sum']*$c_order_sum['kol'];

		$item_order_sum[] = array(
		'id' 						=> $c_order_sum['id'],
		'id_catalog_data_order' 	=> $c_order_sum['id_catalog_data_order'],
		'articul_catalog_data_order'=> $c_order_sum['articul_catalog_data_order'],
		'title' 					=> $c_order_sum['title'],
		'sum' 						=> $c_order_sum['sum'],
		'kol' 						=> $c_order_sum['kol'],
		'price' 					=> $c_order_sum['sum'] * $c_order_sum['kol'],
        'gmargin' 					=> $c_order_sum['gmargin'],
		);
	}
	$sm->assign('c_order_sum', $item_order_sum);
	$sm->assign('big_sum', $big_sum);


    $list_capital_districts = array();
    foreach($capital_districts as $key=>$item){
        $list_capital_districts+=$item;
    }
    $sm->assign('list_capital_districts', $list_capital_districts);
    $sm->assign('capital_districts', $capital_districts);

    //metro
    $q_metro = $db->fetchAll($db->select()->from('m_geo_metro'));
    foreach($q_metro as &$metro){
        $aMetro[$metro['id']] = array(
            'name' =>$metro['name'],
            'id' =>$metro['id'],
        );
    }
    $sm->assign('aMetro', $aMetro);


    $aAllows = array_flip(['LOGISTIC','LOGISTIC_DATEMONEYINCOME','LOGISTIC_DELIVERYCOST','LOGISTIC_MONEY','LOGISTIC_PAY','LOGISTIC_PAYMENT_DATE','LOGISTIC_REFOUND','LOGISTIC_REFOUND_COMPLETE','LOGISTIC_SEND','LOGISTIC_TOTALMONEYINCOME']);

    foreach($aAllows as $key=>$allow){
        $aAllows[$key] = MyIsValidUsers($_SESSION['user_id'], $key, 0,$module );
    }

    $sm->assign('aAllows', $aAllows);


    myPrintHeader("$cat_name: <small><small>Редактирование заказа № {$prefix}-$id_order</small></small>", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&status=$status", "ACTIVE"=>"1"),
            array("NAME"=>"Редактирование заказа № {$prefix}-$id_order", "LINK"=>"", "ACTIVE"=>"0")
	));

    $ModuleLogger = new MagLogger();

    $aHistory = $ModuleLogger->get_history('mag_orders',$id_order);

    $sm->assign('aHistory', $aHistory);


	$sm->display("modules/$module/$shablon");
}
?>