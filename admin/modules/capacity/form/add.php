<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Опеределение переменных
	$cat_name = "Должности";
	$module = "capacity";
	$main = "permitions";
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'ADD', 0, $module)){
		err_message("У вас нет прав на добавление.");
	}
	
	myPrintHeader("$cat_name: <small><small>Добавить</small></small>", 'dolzhnosty.png');

	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	$sm->display("modules/{$_GET['module']}/add.tpl");
}
?>