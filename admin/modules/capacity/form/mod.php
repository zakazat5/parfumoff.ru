<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$cat_name = "Должности";
	$module = "capacity";
	$table = "base_groups";
	$main = "permitions";
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)){
		err_message("У вас нет прав на простмотр.");
	}

	// Сортировка
	$where = '';
	
	// Запрос к базе
	$all_items = mysql_result(mysql_query("SELECT count(id) FROM $table $where"),0,0);
	$query = mysql_query("SELECT * FROM $table $where ");
	$i = 1;
	while ($content = mysql_fetch_array($query)){
		$item[] = array(
		'id' => $content['id'],
		'name' => $content['name'],
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		'n' => $i
		);
		$i++;
	}

	$sm->assign('item',$item);

	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name", 'dolzhnosty.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=add")
	));

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Настройки", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Список действий.
	$sm->assign('sel_id', array(
	'delete_item'
	));

	$sm->assign('sel_names', array(
	'Удалить'
	));
	$sm->assign('is_active', 'null');

	// Вывод шаблона
	if ($all_items != 0){
		$sm->display("modules/{$_GET['module']}/table.tpl");
	}else {
		$sm->assign('info', 'Раздел пуст');
		$sm->display("info.tpl");
	}
}
?>