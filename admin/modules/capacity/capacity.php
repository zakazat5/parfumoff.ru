<?php
if ($_SESSION['admin'] == 'allow'){
	$cat_module = $_GET['worker'];
	$cat_main = $_GET['main'];
	$table = "base_groups";

	switch ($_GET['action']) {
		case "add":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'ADD', 0, $cat_module)){
				err_message("У вас нет прав на добавление.");
			}

			if ($_POST['name'] == ''
			){err_message("Не заполнены обязательные поля.");}

			$name = format_text($_POST['name']);

			mysql_query("INSERT INTO base_groups (name) VALUES ('$name') ");
			go("?main=$cat_main&module=$cat_module&action=mod");
			break;

		case "edit":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'EDIT', 0, $cat_module)){
				err_message("У вас нет прав на редактирование.");
			}

			if ($_POST['name'] == ''
			){err_message("Не заполнены обязательные поля.");}

			$name = format_text($_POST['name']);
			$id = intval($_GET['id']);
			
			mysql_query("UPDATE $table SET name='$name' WHERE id='$id'");

			go("?main=$cat_main&module=$cat_module&action=mod");
			break;

		case "order":
			if ($_POST['actions'] == 'null'){err_message("Не выбрано действие.");}
			if (!isset($_POST['id'])){err_message("Не выбраны элементы.");}
			
			/*=============================================*/
			// Блокировка изменения системной записи
			/*=============================================*/
			$del_id = '';
			foreach ($_POST['id'] as $key=>$value) $del_id .= "$value,";
			$del_id = substr($del_id, 0, -1);
			
			// Удаление записей
			if ($_POST['actions'] == 'delete_item') {

				// Проверка прав доступа
				if (!MyIsValidUsers($_SESSION['user_id'], 'DELETE', 0, $cat_module)){
					err_message("У вас нет прав на удаление.");
				}

				$del_id = '';
				foreach ($_POST['id'] as $key=>$value) $del_id .= "$value,";
				$del_id = substr($del_id, 0, -1);

				$all_items = mysql_result(mysql_query("SELECT count(id) FROM base_users WHERE id_group IN ($del_id)"),0,0);
				if ($all_items != 0) {err_message("Нельзя удалить должность. <br>Есть пользователи принадлежащие данной должности, удалите сначала их.");}

				mysql_query("DELETE FROM $table WHERE id IN ($del_id)");
			}
			// Идем обратно
			go("?main=$cat_main&module=$cat_module&action=mod");
			break;
	}
}
?>