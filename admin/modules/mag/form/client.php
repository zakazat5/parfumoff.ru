<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$table 		= "m_catalog_client";
	$main 		= "mag";
	$module 	= "mag";
	$cat_name 	= 'Заказы';
	
	// Запрос к базе
	$all_items = mysql_result(mysql_query("SELECT count(id) FROM $table"),0,0);
	$pages = pages($all_items, 20, 'pages.tpl');
	$start = $pages['start'];
	$limit = 20;

	$query = mysql_query("SELECT * FROM $table ORDER by date desc LIMIT $start, $limit");
	while ($content = mysql_fetch_array($query)){
		$item[] = array(
		'id' => $content['id'],
		'fio' => $content['fio'],
		'phone' => $content['phone'],
		'address' => $content['address'],
		'city' => $content['city'],
		'metro' => $content['metro'],
		
		'date' => $content['date'],
		'bgcolor' =>  $bgcolor,
		);
	}
	
	$sm->assign('item',$item);
	$sm->assign('all_items',$all_items);
	$sm->assign('pages',$pages['pages']);

	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name", 'news.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить клиента", "LINK" => "?main=$main&module=$module&action=client_add")
	));

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"Заказы", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1"),
	array("NAME"=>"Клиенты", "LINK"=>"", "ACTIVE"=>"0")
	));

	// Список действий.
	$sm->assign('footer_id', array(
	'delete_item_client',
	'active_client'
	));

	$sm->assign('footer_names', array(
	'Удалить',
	'Активировать / Заблокировать'
	));
	$sm->assign('footer_active', 'null');

	// Вывод шаблона
	$sm->display("modules/$module/client_table.tpl");
}
?>