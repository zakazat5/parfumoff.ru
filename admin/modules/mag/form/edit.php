<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$id_cat = intval($_GET['id_cat']);

	$cat_name = mysql_result(mysql_query("SELECT name FROM base_cat WHERE id='$id_cat'"),0,0);
	$module = "news";
	$main = "content";
	$table = "m_news";

	$id = intval($_GET['id']);

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'EDIT', $id_cat)){
		err_message("У вас нет прав на редактирование.");
	}

	$query = mysql_query("SELECT * FROM $table WHERE id='$id' LIMIT 0,1");
	while ($content = mysql_fetch_array($query)){
		$title = $content['title'];

		$sm->assign('date1',$content['registerDate']);
		$sm->assign('date2',$content['startDate']);
		$sm->assign('date3',($content['endDate'] == '0000-00-00 00:00:00') ? '' : $content['endDate']);

		$sm->assign('sort',$content['sort']);
		$sm->assign('url',$content['url']);
		$sm->assign('tag',$content['tag']);

		$sm->assign('img', $content['img']);
		$sm->assign('alt',$content['alt']);

		$sm->assign('title',$content['title']);
		$sm->assign('meta_title',$content['meta_title']);
		$sm->assign('meta_description',$content['meta_description']);
		$sm->assign('text', html_entity_decode($content['text']));
		$sm->assign('anot', html_entity_decode($content['anot']));
		$sm->assign('is_main', ($content['is_main'] == 1) ? 'checked' : '');
		$sm->assign('is_block', ($content['is_block'] == 1) ? 'checked' : '');
	}

	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name: <small><small>Изменить [$title]</small></small>", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&id_cat=$id_cat", "ACTIVE"=>"1")
	));

	$sm->display("modules/$module/edit.tpl");
}
?>