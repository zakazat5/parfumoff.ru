<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$main 		= "mag";
	$module 	= "mag";
	$cat_name 	= 'Заказы';

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)) 
	err_message("У вас нет прав на просмотр.");

	include_once('filter.php');

	// Запрос к базе
	$all_items = mysql_result(mysql_query("SELECT count(id) FROM m_catalog_client_orders $where"),0,0);
	$pages = pages($all_items, $_SESSION['user_cpage'], 'pages.tpl');
	$start = $pages['start'];
	$limit = $_SESSION['user_cpage'];

	$query = mysql_query("SELECT *, UNIX_TIMESTAMP(date) as date FROM m_catalog_client_orders $where ORDER by date desc LIMIT $start, $limit");
	while ($content = mysql_fetch_array($query)){
		
		$client = mysql_fetch_array(mysql_query("SELECT * FROM m_catalog_client WHERE id={$content['id_client']} "));
		
		$orders_id 	= explode(',', $content['id_order']);
		$orders_kol = explode(',', $content['kol']);
		
		$q_sum = mysql_query("SELECT * FROM m_catalog_client_sum WHERE id_order='{$content['id']}' ");
		while ($c_sum = mysql_fetch_array($q_sum)){
			$price 		.= $c_sum['sum']. ' * ' .$c_sum['kol']. '<br>';
			$sum 		.= '='. $c_sum['sum'] * $c_sum['kol']. '<br>';
			$big_sum 	= $big_sum + $c_sum['sum'] * $c_sum['kol'];
			$title 		.= $c_sum['title'] . '<br>';
		}
		
		if ( $content['status'] == 1) {
			$bgcolor = '#80aa70';
		}elseif ( $content['status'] == 2 ){
			$bgcolor = '#aca200';
		}elseif ( $content['status'] == -1 ){
			$bgcolor = '#d95e5e';
		}elseif ( $content['status'] == 3 ){
			$bgcolor = '#6fabd9';
		}

		$item[] = array(
		'id' 		=> $content['id'],
		'comment' 	=> $content['comment'],
		'status' 	=> $content['status'],
		
		'id_client' => $client['id'],
		'email' 	=> $client['email'],
		'fio' 		=> $client['fio'],
		'phone' 	=> $client['phone'],
		'address' 	=> $client['address'],
		'city' 		=> $client['city'],
		'metro' 	=> $client['metro'],
		
		'sum' 		=> $sum,
		'price' 	=> $price,
		'big_sum' 	=> $big_sum,
		
		'title' 	=> $title,

		'date' 		=> rusdate($content['date'],3),
		'bgcolor' 	=> $bgcolor,
		);
		
		$title = '';$price = '';$sum = '';$big_sum = 0;
	}

	$sm->assign('item',$item);
	$sm->assign('all_items',$all_items);
	$sm->assign('pages',$pages['pages']);

	myPrintHeader("$cat_name", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod&status=1", "ACTIVE"=>"1")
	));
	
	// Вывод шаблона
	$sm->display("modules/$module/table.tpl");
}
?>