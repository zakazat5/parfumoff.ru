<?php
///////////////////////////////////////////
// Модуль управления правами доступа
///////////////////////////////////////////

if ($_SESSION['admin'] == 'allow'){
	$cat_module = $_GET['worker'];
	$cat_main = $_GET['main'];

	switch ($_GET['action']) {
		case "add_role":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'ADD', 0, $cat_module)){
				err_message("У вас нет прав на добавление.");
			}

			// Определение переменных
			if ($_POST['title'] == ''
			){err_message("Не заполнены обязательные поля.");}

			$anot = format_text($_POST['anot']);
			$title = format_text($_POST['title']);

			// Добавление записи в правило
			mysql_query("INSERT INTO base_role VALUES ('','$title','$anot')");
			$id_role = mysql_result(mysql_query("SELECT LAST_INSERT_ID()"),0,0);

			// Получение значений массива из запроса
			if (isset($_POST['id_interface'])){
				foreach ($_POST['id_interface'] as $key=>$value){
					$a_interface[$value] = '1';
				}
			}

			// Заполнение табл. base_role_items из полученных значений.
			$query = mysql_query("SELECT * FROM base_module ORDER BY type");
			while ($content = mysql_fetch_array($query)){
				$all_interface = mysql_result(mysql_query("SELECT count(id) FROM base_interface WHERE id_module='{$content['id']}'"),0,0);
				if ($all_interface != 0){
					$interface = mysql_query("SELECT * FROM base_interface WHERE id_module='{$content['id']}' order by name");
					while ($content_interface = mysql_fetch_array($interface)){
						if (array_key_exists($content_interface['id'], $a_interface)) {$is_value = '1';}else {$is_value = '0';}
						mysql_query("INSERT INTO base_role_items VALUES ('','$id_role','{$content_interface['id']}', '{$content_interface['id_module']}', '$is_value')");
					}
				}
			}

			// Идем обратно
			go("index.php?main=$cat_main&module=$cat_module&action=mod");
			break;

		case "edit_role":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'EDIT', 0, $cat_module)){
				err_message("У вас нет прав на редактирование.");
			}

			// Определение переменных
			if ($_POST['title'] == ''
			){err_message("Не заполнены обязательные поля.");}

			$anot = format_text($_POST['anot']);
			$title = format_text($_POST['title']);
			$id_role = intval($_GET['id']);

			// Изменение записи в правило
			mysql_query("UPDATE base_role SET title='$title', anot='$anot' WHERE id='$id_role'");

			// Получение значений массива из запроса
			if (isset($_POST['id_interface'])){
				foreach ($_POST['id_interface'] as $key=>$value){
					$a_interface[$value] = '1';
				}
			}

			// Заполнение табл. base_role_items из полученных значений.
			$query = mysql_query("SELECT * FROM base_module ORDER BY type");
			while ($content = mysql_fetch_array($query)){
				$all_interface = mysql_result(mysql_query("SELECT count(id) FROM base_interface WHERE id_module='{$content['id']}'"),0,0);
				if ($all_interface != 0){
					$interface = mysql_query("SELECT * FROM base_interface WHERE id_module='{$content['id']}' order by name");
					while ($content_interface = mysql_fetch_array($interface)){
						if (array_key_exists($content_interface['id'], $a_interface)) {$is_value = '1';}else {$is_value = '0';}
						mysql_query("UPDATE base_role_items SET value='$is_value' WHERE id_interface='{$content_interface['id']}' and id_role='$id_role'");
					}
				}
			}

			// Идем обратно
			go("index.php?main=$cat_main&module=$cat_module&action=mod");
			break;

		case "delete_role":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'DELETE', 0, $cat_module)){
				err_message("У вас нет прав на удаление ролей.");
			}

			if ($_POST['actions'] == 'null'){err_message("Не выбрано действие.");}
			$del_id = '';
			foreach ($_POST['id'] as $key=>$value){$del_id .= "$value,";}
			$del_id = substr($del_id, 0, -1);

			/*=============================================*/
			// Блокировка изменения системной записи
			/*=============================================*/
			$all_items = mysql_result(mysql_query("SELECT count(id) FROM base_users WHERE id_role IN ($del_id) and login='Admin'"),0,0);
			if ($all_items != 0) {err_message("Невозможно редактировать системную запись.");}
			/*=============================================*/

			mysql_query("DELETE FROM base_role_items WHERE id_role IN ($del_id)");
			mysql_query("DELETE FROM base_role WHERE id IN ($del_id)");
			go("index.php?main=$cat_main&module=$cat_module&action=mod");
			break;

		case "add_module":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'PERM_MODULE', 0, $cat_module)){
				err_message("У вас нет доступа к разделу интерфейсов.");
			}

			$type = 'static';
			$name = format_text($_POST['name']);
			$title = format_text($_POST['title']);

			if ($_POST['name'] == '' or
			$_POST['title'] == ''
			){err_message("Не заполнены обязательные поля.");}

			mysql_query("INSERT INTO base_module VALUES ('','$title','$name', '$type', '')");

			//$name_module = DGetNameCat($id_module);
			//$type_module = DGetTypeCat($id_module);
			//mysql_query("INSERT INTO base_module VALUES ('','$name_module','$type_module','$type', '$id_module')");

			go("index.php?main=$cat_main&module=$cat_module&action=add_module");

			break;

		case "delete_module":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'PERM_MODULE', 0, $cat_module)){
				err_message("У вас нет доступа к разделу интерфейсов.");
			}

			if ($_POST['actions'] == 'null'){err_message("Не выбрано действие.");}
			$del_id = '';
			foreach ($_POST['id'] as $key=>$value){$del_id .= "$value,";}
			$del_id = substr($del_id, 0, -1);

			// Проверка допустимости
			$count = mysql_result(mysql_query("SELECT count(id) FROM base_interface WHERE id_module IN ($del_id)"),0,0);
			if ($count > 0) {err_message("В модуле присутствуют интерфейсы, удалите сначала их.");}

			mysql_query("DELETE FROM base_module WHERE id IN ($del_id)");
			go("index.php?main=$cat_main&module=$cat_module&action=add_module");
			break;

			// Обработка функций модуля
		case "change_module":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'PERM_MODULE', 0, $cat_module)){
				err_message("У вас нет доступа к разделу интерфейсов.");
			}

			// Добавление инетерфейса
			if (isset($_POST['add_interface'])) {
				if ($_POST['name_interface'] == '' or
				$_POST['anot_interface'] == ''
				){err_message("Не заполнены поля.");}

				$id_module = intval($_GET['id']);
				$name_interface = format_text($_POST['name_interface']);
				$anot_interface = format_text($_POST['anot_interface']);
				mysql_query("INSERT INTO base_interface VALUES ('','$id_module', '$name_interface', '$anot_interface')");

				// Сохранение модуля
			}elseif (isset($_POST['save_module'])){
				if ($_POST['title'] == '' or
				$_POST['name'] == ''
				){err_message("Не заполнены поля.");}

				$id_module = intval($_GET['id']);
				$title_module = format_text($_POST['title']);
				$name_module = format_text($_POST['name']);

				mysql_query("UPDATE base_module SET title='$title_module', name='$name_module' WHERE id='$id_module'");


			}elseif (isset($_POST['actions'])){
				// Проверка прав доступа
				if (!MyIsValidUsers($_SESSION['user_id'], 'PERM_MODULE', 0, $cat_module)){
					err_message("У вас нет доступа к разделу интерфейсов.");
				}
				if ($_POST['actions'] == 'null'){err_message("Не выбрано действие.");}


				// Сохраниение изменений интерфейсов
				if ($_POST['actions'] == 'save_interface'){
					$id_module = intval($_GET['id']);

					$q_interface = mysql_query("SELECT * FROM base_interface WHERE id_module='$id' order by name");
					while ($c_interface = mysql_fetch_array($q_interface)){
						$id_name = "name_interface_" . $c_interface['id'];
						$name_interface = format_text($_POST[$id_name]);
						$id_anot = "anot_interface_" . $c_interface['id'];
						$anot_interface = $_POST[$id_anot];
						mysql_query("UPDATE base_interface SET name='$name_interface', anot='$anot_interface' WHERE id='{$c_interface['id']}'");
					}

					// Удаление интерфейсов
				}elseif ($_POST['actions'] == 'delete_interface') {
					$id_module = intval($_GET['id']);
					if (!isset($_POST['id_interface'])) {err_message("Не выбраны элементы полей интерфейсов.");}

					if (array_count_values($_POST['id_interface']) != 0) {
						$del_id = '';
						foreach ($_POST['id_interface'] as $key=>$value) $del_id .= "$value,";
						$del_id = substr($del_id, 0, -1);

						$count = mysql_result(mysql_query("SELECT count(id) FROM base_role_items WHERE id_interface IN ($del_id)"),0,0);
						if ($count > 0) {err_message("Невозможно удалить интерфейс, \
										он присутствует в правах доступа, \
										уберите роль в которой участвуют данные интерфесы, потом можете удалить.");}

						mysql_query("DELETE FROM base_interface WHERE id IN ($del_id)");
					}
				}
			}

			go("index.php?main=$cat_main&module=$cat_module&action=edit_module&id=$id_module");
			break;
		default:
			err_message("Ошибка в запросе, такой комманды не существует.");
			break;
	}
}
?>