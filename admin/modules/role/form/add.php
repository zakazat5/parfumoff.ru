<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	// Опеределение переменных
	$cat_name = "Права доступа";
	$module = "role";
	$main = "permitions";
	$table = "base_module";
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'ADD', 0, $module)){
		err_message("У вас нет прав на добавление ролей.");
	}

	$sm->assign('item_static',VLGetStaticInterface(''));
	$sm->assign('item_dinamic', VLGetDinamicInterface(''));

	// Подключение заголовка
	myPrintHeader("$cat_name: <small><small>Добавить роль</small></small>", 'prava_dostupa.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Настройки", "LINK"=>"?main=permitions", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Вывод шаблона
	$sm->display("modules/{$_GET['module']}/add.tpl");
}
?>