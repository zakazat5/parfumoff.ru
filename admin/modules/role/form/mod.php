<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$cat_name = "Права доступа";
	$module = "role";
	$table = "base_role";
	$main = "permitions";

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)){
		err_message("У вас нет прав на просмотр ролей.");
	}

	// Сортировка
	$where = '';

	// Запрос к базе
	$all_items = mysql_result(mysql_query("SELECT count(id) FROM $table $where"),0,0);

	$query = mysql_query("SELECT * FROM $table $where ORDER BY title");
	$i = 1;
	while ($content = mysql_fetch_array($query)){
		$item[] = array(
		'id' => $content['id'],
		'title' => $content['title'],
		'anot' => $content['anot'],
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		'n' => $i
		);
		$i++;
	}
	$sm->assign('item',$item);

	// Подключение заголовка / ACTIONS FORM
	//if (MyIsValidUsers($_SESSION['user_id'], 'PERM_MODULE', 0, $module)){
	myPrintHeader("$cat_name", 'prava_dostupa.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=add"),
	array("IMG" => "catalog.gif", "ALT" => "Управление модулями", "LINK" => "?main=$main&module=$module&action=add_module")
	));
	//}else {
	//	myPrintHeader("$cat_name", 'title_iblock', array(
	//	array("IMG" => "document_add.png", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=add")
	//	));
	//}

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Настройки", "LINK"=>"?main=permitions", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Список действий.
	$sm->assign('sel_id', array(
	'delete_role'
	));

	$sm->assign('sel_names', array(
	'Удалить'
	));
	$sm->assign('is_active', 'null');

	// Вывод шаблона
	$sm->display("modules/{$_GET['module']}/table.tpl");
}
?>