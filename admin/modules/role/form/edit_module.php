<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$cat_name = "Модули и интерфейсы";
	$module = "role";
	$main = "permitions";
	$id = intval($_GET['id']);

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'PERM_MODULE', 0, $module)){
		err_message("У вас нет доступа к разделу интерфейсов.");
	}

	// Запрос к базе
	$query = mysql_query("SELECT * FROM base_module WHERE id='$id' LIMIT 0, 1");
	while ($content = mysql_fetch_array($query)){
		$title = $content['title'];
		$sm->assign('id',$content['id']);
		$sm->assign('name',$content['name']);
		$sm->assign('title',$content['title']);
		$sm->assign('type',$content['type']);
		$sm->assign('id_module',$content['id_module']);
	}

	// Список интерфейсов
	$rec_parent = mysql_query("SELECT * FROM base_interface WHERE id_module='$id' order by name");
	while ($rec = mysql_fetch_array($rec_parent)){
		$item_interface[] = array(
		'id' => $rec['id'],
		'name' => $rec['name'],
		'anot' => $rec['anot']
		);
	}

	$sm->assign('item_interface',$item_interface);
	$sm->assign('cat_name',$m_cat);
	
	// Список действий.
	$sm->assign('sel_id', array(
	'save_interface',
	'delete_interface'
	));

	$sm->assign('sel_names', array(
	'Сохранить изменения',
	'Удалить интерфейсы'
	));
	$sm->assign('is_active', 'null');

	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name: <small><small>Сформировать модуль [$title]</small></small>", 'prava_dostupa.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Настройки", "LINK"=>"?main=permitions", "ACTIVE"=>"1"),
	array("NAME"=>"Права доступа", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=add_module", "ACTIVE"=>"1")
	));

	// Вывод шаблона
	$sm->display("modules/$module/table_module_edit.tpl");
}
?>