<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Опеределение переменных
	$cat_name = "Права доступа";
	$module = "role";
	$main = "permitions";
	$table = "base_module";
	$id = intval($_GET['id']);

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'EDIT', 0, $module)){
		err_message("У вас нет прав на редактирование ролей.");
	}
	
	// Запрос к базе
	$query_role = mysql_query("SELECT * FROM base_role WHERE id='$id' LIMIT 0,1");
	while ($content_role = mysql_fetch_array($query_role)){
		$title = $content_role['title'];
		$sm->assign('title', $content_role['title']);
		$sm->assign('anot', $content_role['anot']);
	}

	$sm->assign('item_static',VLGetStaticInterface($id));
	$sm->assign('item_dinamic', VLGetDinamicInterface($id));
	

	// Подключение заголовка
	myPrintHeader("$cat_name: <small><small>Изменить роль [$title]</small></small>", 'prava_dostupa.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Настройки", "LINK"=>"?main=permitions", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Вывод шаблона
	$sm->display("modules/{$_GET['module']}/edit.tpl");
}
?>