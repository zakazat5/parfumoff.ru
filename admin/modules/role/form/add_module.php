<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$cat_name = "Модули и интерфейсы";
	$module = "role";
	$main = "permitions";

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'PERM_MODULE', 0, $module)){
		err_message("У вас нет доступа к разделу интерфейсов.");
	}

	// Запрос к базе
	$query = mysql_query("SELECT * FROM base_module");

	while ($content = mysql_fetch_array($query)){

		if ($content['type']=='static') {
			$item_static[] = array(
			'id' => $content['id'],
			'name' => $content['name'],
			'title' => $content['title'],
			'type' => $content['type'],
			'id_module' => $content['id_module']
			);
			

		}elseif ($content['type']=='dinamic'){
			$item_dinamic[] = array(
			'id' => $content['id'],
			'name' => $content['name'],
			'title' => $content['title'],
			'type' => $content['type'],
			'id_module' => $content['id_module']
			);			
		}
	}
	$sm->assign('item_dinamic',$item_dinamic);
	$sm->assign('item_static',$item_static);


	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name", 'prava_dostupa.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Настройки", "LINK"=>"?main=permitions", "ACTIVE"=>"1"),
	array("NAME"=>"Права доступа", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=add_module", "ACTIVE"=>"1")
	));

	// Список действий.
	$sm->assign('sel_id', array(
	'delete_module'
	));

	$sm->assign('sel_names', array(
	'Удалить'
	));
	$sm->assign('is_active', 'null');

	// Вывод шаблона
	$sm->display("modules/{$_GET['module']}/table_module.tpl");

}
?>