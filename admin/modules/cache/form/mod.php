<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$main 		= "permitions";
	$module 	= "cache";
	$cat_name 	= 'Управление кэшем';

	myPrintHeader("$cat_name", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Вывод шаблона
	$sm->display("modules/$module/table.tpl");
}
?>