<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	// Определение переменных
	$main 		= 'mag';
	$module 	= 'mag_subs';
	$cat_name 	= 'Рассылка';
	
	// Проверка прав доступа
	//if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)) err_message("У вас нет прав на просмотр.");
	
	$all_items = mysql_result(mysql_query("SELECT count(*) FROM m_subscribe "),0,0);
	$all_send = mysql_result(mysql_query("SELECT count(*) FROM m_subscribe WHERE invite_send=1 "),0,0);
	$all_used = mysql_result(mysql_query("SELECT count(*) FROM m_subscribe WHERE invite_used=1 "),0,0);
	$sm->assign('all_items', $all_items);
	$sm->assign('all_send', $all_send);
	$sm->assign('all_used', $all_used);
	
	// Подключение заголовка
	myPrintHeader("$cat_name", 'news.png');

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=content", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	$sm->display("modules/$module/table.tpl");
}

?>