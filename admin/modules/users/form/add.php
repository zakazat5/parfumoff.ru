<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);
	
	// Опеределение переменных
	$cat_name = "Пользователи";
	$module = "users";
	$main = "permitions";
	
	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'ADD', 0, $module)){
		err_message("У вас нет прав на добавление.");
	}

	myPrintHeader("$cat_name: <small><small>Добавить</small></small>", 'users.png');

	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Список групп
	$query = mysql_query("SELECT id, name FROM base_groups");
	while ($content = mysql_fetch_array($query)){
		$sel_id[] = $content['id'];
		$sel_names[] = $content['name'];
	}
	$sm->assign('sel_id', $sel_id);
	$sm->assign('sel_names', $sel_names);
	$sm->assign('is_active', 'null');
	
	// Список ролей
	$query_role = mysql_query("SELECT * FROM base_role ORDER BY title");
	while ($content_role = mysql_fetch_array($query_role)){
		$role_id[] = $content_role['id'];
		$role_names[] = $content_role['title'];
	}
	$sm->assign('role_id', $role_id);
	$sm->assign('role_names', $role_names);
	$sm->assign('role_active', 'null');
	
	$sm->display("modules/{$_GET['module']}/add.tpl");
}
?>