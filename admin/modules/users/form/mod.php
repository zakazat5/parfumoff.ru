<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$cat_name = "Пользователи";
	$module = "users";
	$main = "permitions";

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)){
		err_message("У вас нет прав на просмотр.");
	}

	// Запрос к базе
	$query_group = mysql_query("SELECT * FROM base_groups");
	while ($content_group = mysql_fetch_array($query_group)){
		$item_group[] = array(
		'id' => $content_group['id'],
		'name' => $content_group['name']
		);
	}

	$all_items = mysql_result(mysql_query("SELECT count(id) FROM base_users"),0,0);
	$query = mysql_query("SELECT * FROM base_users ");
	while ($content = mysql_fetch_array($query)){
		$name_role = @mysql_result(mysql_query("SELECT title FROM base_role WHERE id='{$content['id_role']}'"),0,0);
		
		$item[] = array(
		'id' => $content['id'],
		'id_group' => $content['id_group'],
		'login' => $content['login'],
		'name_role' => $name_role,
		'registerDate' => $content['registerDate'],
		'is_block' => ($content['is_block'] == 0) ? 'green.gif' : 'red.gif',
		'is_public' => ($content['is_public'] == 0) ? 'нет' : 'да',
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8'
		);
		$i++;
	}

	$sm->assign('item',$item);
	$sm->assign('item_group',$item_group);
	$sm->assign('pages',$pages['pages']);

	// Подключение заголовка / ACTIONS FORM
	myPrintHeader("$cat_name", 'users.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить запись", "LINK" => "?main=$main&module=$module&action=add")
	));

	// Подключение навигации
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Настройки", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Список действий.
	$sm->assign('sel_id', array(
	'delete_item',
	'active_item',
	'deactive_item'
	));

	$sm->assign('sel_names', array(
	'Удалить',
	'Активировать',
	'Заблокировать'
	));
	$sm->assign('is_active', 'null');

	// Вывод шаблона
	$sm->display("modules/{$_GET['module']}/table.tpl");
}
?>