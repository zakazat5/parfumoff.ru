<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Опеределение переменных
	$cat_name = "Пользователи";
	$main = "permitions";
	$module = "users";
	$table = "base_users";
	$id = intval($_GET['id']);

	// Проверка прав доступа
	if (!MyIsValidUsers($_SESSION['user_id'], 'EDIT', 0, $module)){
		err_message("У вас нет прав на редактирование.");
	}

	$query = mysql_query("SELECT * FROM $table WHERE id='$id' LIMIT 1");
	while ($content = mysql_fetch_array($query)){
		$id_group = $content['id_group'];
		$id_role = $content['id_role'];
		$sm->assign('login',$content['login']);
		$sm->assign('is_block', ($content['is_block'] == 1) ? 'checked' : '');
		$sm->assign('is_public', ($content['is_public'] == 1) ? 'checked' : '');
	}

	// список групп
	$query_group = mysql_query("SELECT id, name FROM base_groups ORDER BY name");
	while ($content_group = mysql_fetch_array($query_group)){
		$sel_id[] = $content_group['id'];
		$sel_names[] = $content_group['name'];
	}
	$sm->assign('sel_id', $sel_id);
	$sm->assign('sel_names', $sel_names);
	$sm->assign('is_active', $id_group);

	// Список ролей
	$query_role = mysql_query("SELECT * FROM base_role ORDER BY title");
	while ($content_role = mysql_fetch_array($query_role)){
		$role_id[] = $content_role['id'];
		$role_names[] = $content_role['title'];
	}
	$sm->assign('role_id', $role_id);
	$sm->assign('role_names', $role_names);
	$sm->assign('role_active', $id_role);

	myPrintHeader("$cat_name: <small><small>Изменить [$full_name]</small></small>", 'users.png');

	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Контент", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	$sm->display("modules/{$_GET['module']}/edit.tpl");
}

?>