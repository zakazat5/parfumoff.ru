<?php
if ($_SESSION['admin'] == 'allow'){
	$cat_module = $_GET['worker'];
	$cat_main = $_GET['main'];

	switch ($_GET['action']){
		case "add":
			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'ADD', 0, $cat_module)){
				err_message("У вас нет прав на добавление.");
			}

			if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";};
			if(isset($_POST['is_public'])){$is_public = "1";}else{$is_public = "0";};
			if ($_POST['capacity'] == 'null') err_message("Ошбика. Не выбрана должность.");	else $id_group = $_POST['capacity'];
			if ($_POST['sel_role'] == 'null') {err_message("Ошбика. Не выбрана роль.");}else {$id_role = intval($_POST['sel_role']);}
			if ($_POST['login'] == '') err_message("Ошбика. Не введен логин.");

			// Проверка логина на совпадение
			$login = trim($_POST['login']);
			$login = eregi_replace('[\/!@#$%^&*()+=";:.,?|`~\']+', "", $login);
			$login = substr($login, 0, 30);

			$login_count = mysql_result(mysql_query("SELECT count(*) FROM base_users WHERE login='$login'"),0,0);
			if ($login_count != 0){
				err_message("Ошибка. Логин уже существует, введите другой.");
			}

			// Пароль
			if ($_POST['password'] == '' and $_POST['re_password'] == ''){
				err_message("Ошбика. Пустой пароль.");
			}elseif($_POST['password'] != $_POST['re_password']){
				err_message("Ошбика. Пароли не совпадают.");
			}else {
				$password = md5(addslashes(trim($_POST['password'])));
			}

			mysql_query("INSERT INTO base_users
			(id_group, id_role, login, password, is_block, is_public) 
			VALUES ('$id_group', '$id_role', '$login', '$password', '$is_block', '$is_public' )");

			go("?main=$cat_main&module=$cat_module&action=mod");
			break;

		case "edit":
			$id = intval($_GET['id']);

			// Проверка прав доступа
			if (!MyIsValidUsers($_SESSION['user_id'], 'EDIT', 0, $cat_module)){
				err_message("У вас нет прав на редактирование.");
			}


			if(isset($_POST['is_block'])){$is_block = "1";}else{$is_block = "0";};
			if(isset($_POST['is_public'])){$is_public = "1";}else{$is_public = "0";};

			if ($_POST['capacity'] == 'null') err_message("Ошбика. Не выбрана должность.");	else $id_group = $_POST['capacity'];
			if ($_POST['sel_role'] == 'null') {err_message("Ошбика. Не выбрана роль.");}else {$id_role = intval($_POST['sel_role']);}
			if ($_POST['login'] == '') err_message("Ошбика. Не введен логин.");

			// Проверка логина на совпадение
			$login_new = trim($_POST['login']);
			$login_new = eregi_replace('[\/!@#$%^&*()+=";:.,?|`~\']+', "", $login_new);
			$login_new = substr($login_new, 0, 30);

			$login_old = mysql_result(mysql_query("SELECT login FROM base_users WHERE id='$id'"),0,0);

			if ($login_new == $login_old){
				$login = $login_new;
			}else{
				//$login_count = mysql_result(mysql_query("SELECT count(id) FROM base_users WHERE login='$login_new'"),0,0);
				//if ($login_count != 0) {err_message("Ошибка. Логин уже существует, введите другой.");}
				$login = $login_new;
			}

			// Пароль
			if ($_POST['password'] == '' and $_POST['re_password'] == ''){
				$is_password = 0;
			}elseif($_POST['password'] != $_POST['re_password']){
				err_message("Ошбика. Пароли не совпадают.");
			}else {
				$is_password = 1;
				$password = md5(addslashes(trim($_POST['password'])));
			}

			if ($is_password) {
				mysql_query("UPDATE base_users SET
					id_role='$id_role', 
					id_group='$id_group', 
					login='$login', 
					password='$password', 
					is_block='$is_block', 
					is_public='$is_public'
					WHERE id='$id'");
			}else {
				mysql_query("UPDATE base_users SET
					id_role='$id_role', 
					id_group='$id_group', 
					login='$login', 
					is_block='$is_block', 
					is_public='$is_public'
					WHERE id='$id'");
			}

			go("?main=$cat_main&module=$cat_module&action=mod");
			break;

		case "order":
			if ($_POST['actions'] == 'null'){err_message("Не выбрано действие.");}
			if (!isset($_POST['id']) and !isset($_POST['sel_total'])){err_message("Не выбраны элементы.");}

			/*=============================================*/
			// Блокировка изменения системной записи
			/*=============================================*/
			$del_id = '';
			foreach ($_POST['id'] as $key=>$value) $del_id .= "$value,";
			$del_id = substr($del_id, 0, -1);
			$all_items = mysql_result(mysql_query("SELECT count(id) FROM base_users WHERE id IN ($del_id) and login='Admin'"),0,0);
			if ($all_items != 0) {err_message("Невозможно редактировать системную запись.");}
			/*=============================================*/

			// Удаление записей
			if ($_POST['actions'] == 'delete_item') {
				// Проверка прав доступа
				if (!MyIsValidUsers($_SESSION['user_id'], 'DELETE', 0, $cat_module)){
					err_message("У вас нет прав на удаление.");
				}
				$del_id = '';
				foreach ($_POST['id'] as $key=>$value) $del_id .= "$value,";
				$del_id = substr($del_id, 0, -1);

				mysql_query("DELETE FROM base_users WHERE id IN ($del_id)");

				// Блокировка записей
			}elseif ($_POST['actions'] == 'deactive_item'){
				// Проверка прав доступа
				if (!MyIsValidUsers($_SESSION['user_id'], 'BLOCK', 0, $cat_module)){
					err_message("У вас нет прав на деактивацию.");
				}

				$del_id = '';
				foreach ($_POST['id'] as $key=>$value) $del_id .= "$value,";
				$del_id = substr($del_id, 0, -1);
				mysql_query("UPDATE base_users SET is_block=1 WHERE id IN ($del_id)");

				// Активация записей
			}elseif ($_POST['actions'] == 'active_item'){
				// Проверка прав доступа
				if (!MyIsValidUsers($_SESSION['user_id'], 'BLOCK', 0, $cat_module)){
					err_message("У вас нет прав на активацию.");
				}
				$del_id = '';
				foreach ($_POST['id'] as $key=>$value) $del_id .= "$value,";
				$del_id = substr($del_id, 0, -1);
				mysql_query("UPDATE base_users SET is_block=0 WHERE id IN ($del_id)");

			}
			// Идем обратно
			go("?main=$cat_main&module=$cat_module&action=mod");
			break;
		default:
			break;
	}
}


?>