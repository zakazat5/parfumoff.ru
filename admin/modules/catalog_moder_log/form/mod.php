<?php
/**
 *  Модуль модерации новых  товаров
 *
 */



if ($_SESSION['admin'] == 'allow'){
    // Создание экземпляра шаблона
    $sm = new Smarty_Admin($valinorConfig['interface.themes']);

    // Определение переменных
    $module 	= "catalog_moder_log";
    $table 		= "m_catalog_history";
    $main 		= "content";
    $cat_name 	= 'Модерация (парфюмерия)';


    if (isset($_GET['sel_user'])) $_SESSION['sel_user'] = intval($_GET['sel_user']);
    if (!isset($_SESSION['sel_user'])) $_SESSION['sel_user'] = 0;

    $sel_user = (int)$_SESSION['sel_user'];

    $res = mysql_query("SELECT * FROM base_users WHERE is_block=0");

    while($user=mysql_fetch_assoc($res)){
        $aUser[$user['id']] = $user;
    }

    $where = ' where 1=1 ';
    if ($sel_user>0){
        $where.= " AND a.user_id=$sel_user  ";
    }

    if ($date_start = strtotime($_GET['date_start'])){
        $where.= " AND a.date_add>'".date('Y-m-d 00:00:00',$date_start)."'";
    }

    if ($date_end = strtotime($_GET['date_end'])){
        $where.= " AND a.date_add<'".date('Y-m-d 23:59:59',$date_end)."'";
    }



    // Запрос к базе
    $all_items = mysql_result(mysql_query("SELECT count(id) FROM $table a $where"),0,0);



    $pages = pages($all_items, 100, 'pages.tpl');
    $start = $pages['start'];
    $limit =100;

    $query = mysql_query("SELECT a.*, b.title as aromat_title, b.id as aromat_id, c.id as mod_id, d.title as aromat_brand, b.pol "
        ."FROM $table a  "
        ."LEFT JOIN m_catalog_data b on a.target_id=b.id "
        ."LEFT JOIN m_catalog_data_order c on c.articul=a.articul "
        ."LEFT JOIN m_catalog d on b.id_catalog=d.id "
        ." $where "
        ."ORDER BY a.id DESC "
        ."LIMIT $start, $limit");

    print_r(mysql_error());




    $i=0;
    while ($content = mysql_fetch_array($query)){
        $i++;
        if ($content['pol']=='M'){
            $content['bgcolor'] = '#CAD0FF';
        }else{
            $content['bgcolor'] = '#F6C9C9';
        }
        $result[] = $content;
    }

    // Подключение заголовка / ACTIONS FORM

    myPrintHeader("$cat_name", 'news.png', array(
        array("IMG" => "blacklist.png", "ALT" => "Черный лист", "LINK" => "?main=$main&module=$module&action=blacklist&id_cat=$id_cat"),
        array("IMG" => "problem.png", "ALT" => "Проблемные товары", "LINK" => "?main=$main&module=$module&action=problem&id_cat=$id_cat"),
    ));

    // Подключение навигации
    myPrintHeaderNav(array(
        array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
        array("NAME"=>"Контент", "LINK"=>"?main=$main", "ACTIVE"=>"1"),
        array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
    ));



    $sm->assign('item',$result);
    $sm->assign('aUser',$aUser);
    if ($date_start){
        $sm->assign('date_start',date('Y-m-d',$date_start));
    }

    if ($date_end){
        $sm->assign('date_end',date('Y-m-d',$date_end));
    }

    $sm->assign('sel_user',$sel_user);
    $sm->assign('user_sel_sklad',$user_sel_sklad);
    $sm->assign('pages',$pages['pages']);
    $sm->display("modules/$module/table.tpl");
}