<?php

/**
 * Created by PhpStorm.
 * User: loram
 * Date: 16.04.14
 * Time: 21:06
 */
class CsvExporter
{
    /**
     * ������ ���� ������� ��� ��������
     * @var array
     */
    public $rows = array();

    /**
     * ��������� �����
     * @var string
     */
    public $delimiter = ";";

    /**
     * ������ �����������
     * @var string
     */
    public $enclosure = '"';

    /**
     * ������� ������� ��� ��������
     * @var string
     */
    protected $table = "couriers_addresses";

    /**
     * ������� ��������
     * @var string
     */
    protected $table_c = "couriers";

    /**
     * ���������� �������� ������� � ��
     */
    public function export()
    {

        $this->rows[0] = array(
            '���������� ��� ������� (� ����� ������)',
            '���������� ��� ������ (� ����� ������)',
            '������������ �������',
            '������������ ������'
        );

        $sql = mysql_query("SELECT name FROM $this->table_c ORDER BY $this->table_c.couriersId ASC");
        while ($courier = mysql_fetch_array($sql)) {

            $this->rows[0][] = $courier['name'] . "/��� �������";
            $this->rows[0][] = $courier['name'] . "/��� ������";
            $this->rows[0][] = $courier['name'] . "/������� (��/���)";
            $this->rows[0][] = $courier['name'] . "/���� �������� (���)";
            $this->rows[0][] = $courier['name'] . "/������ �������� (��������/���������)";
            $this->rows[0][] = $courier['name'] . "/��� ������ ����������";
            $this->rows[0][] = $courier['name'] . "/�����";
            $this->rows[0][] = $courier['name'] . "/����� ������ ����������(�������� ������� � ������� ��� ������ ������)";
            $this->rows[0][] = $courier['name'] . "/��� ����� ����� ����������";
            $this->rows[0][] = $courier['name'] . "/������ (��������� ��� ���������/100% ����������)";
            $this->rows[0][] = $courier['name'] . "/��������� �������� (�� 1 ��)";
            $this->rows[0][] = $courier['name'] . "/��������� �������� (�� 3 ��)";
            $this->rows[0][] = $courier['name'] . "/������� � ��������� � �������� ������ - ��� ������ � ������� ��� ����������";
            $this->rows[0][] = $courier['name'] . "/����� ���������� � ������ ��� ������ � ������� ����������";
            $this->rows[0][] = $courier['name'] . "/��� ������ � ����� ����";
            $this->rows[0][] = $courier['name'] . "/% ��������� ������������";
            $this->rows[0][] = $courier['name'] . "/% ���������";
        }

        $sql = mysql_query("SELECT $this->table.* FROM $this->table ORDER BY $this->table.courier_id ASC");
        while ($result = mysql_fetch_array($sql)) {

            $row = array();
            $unique = $result['kladr_id_region'] . "_"
                . $result['kladr_id_city'] . "_"
                . $result['name_region'] . "_"
                . $result['name_city'] . "_"
                . $result['delivery_code'];

            // ��������� ����� ������ �� ������
            // �������� +2 ������ ��� ���������� id � courier_id
            for ($j = 0; $j < 4; $j++) {
                $row[$j] = $result[$j + 2];
            }

            // ��������� ������ �� ������ ��
            $k = 5;
            for ($i = 4; $i < 21; $i++) {

                $j = ($result['courier_id'] - 1) * 17 + $i;
                $row[$j] = $result[++$k];
            }

            // ���� ������� ������������ ������
            // �� ������ ������������� ��������
            // ����� ��������� ������� � ������� ������ 4 ��������
            // ����� �� �����������
            if (!isset($this->rows[$unique])) {
                $this->rows[$unique] = $row;
            } else {
                for ($j = 0; $j < 4; $j++) {
                    unset($row[$j]);
                }

                //$this->rows[$unique] = array_merge($this->rows[$unique], $row);
                $this->rows[$unique] += $row;
            }
        }

        $this->proccessOutput();
    }

    /**
     * Create CSV file
     */
    public function proccessOutput()
    {
        // ��������� ������ ����, ��� ��� ����
        // ����� ���� ����������� ������
        foreach ($this->rows as $key => $row) {
            for ($i = 0; $i < 140; $i++) {
                if (!isset($row[$i])) {
                    $this->rows[$key][$i] = "";
                }
            }

            ksort($this->rows[$key]);
        }

        header("Content-type: application/octet-stream;");
        header("Content-Disposition: attachment; filename=\"couriers.csv\"");

        foreach ($this->rows as $row) {

            foreach ($row as $l) {
                echo $this->enclosure . str_replace(
                        $this->enclosure,
                        $this->enclosure . $this->enclosure,
                        $l
                    ) . $this->enclosure . $this->delimiter;
            }

            echo "\r\n";
        }

        exit();
    }
}