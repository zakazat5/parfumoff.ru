<?php


if ($_SESSION['admin'] == 'allow') {
    // Создание экземпляра шаблона
    $sm = new Smarty_Admin($valinorConfig['interface.themes']);

    // Определение переменных
    $main = 'mag';
    $module = 'mag_waitlist';
    $cat_name = 'Лист ожидания';
    $table = 'm_mag_Waitlist';

    // Проверка прав доступа
    //if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)) err_message("У вас нет прав на просмотр.");

    $order_params = [
        'brand',
        'name',
        'phone',
        'email',
        'date',
        'price',
        'mail_sended'
    ];

    $order = "";
    if (in_array($_GET['name'],$order_params) && ($_GET['sort']=='asc' ||$_GET['sort']=='desc')){
        $order = ' order by '.$_GET['name'].' '.$_GET['sort'];
    }


    // Запрос к базе pagination
    $all_items = mysql_result(mysql_query("SELECT count(id) FROM $table $where"), 0, 0);
    $pages = pages($all_items, 10, 'pages.tpl');
    $start = $pages['start'];
    $limit = 10;



    $sql = "SELECT a.*,b.price,d.title as brand, c.title as aromat,b.type,b.v FROM $table a
    LEFT JOIN m_catalog_data_order b on a.articul=b.articul
    LEFT JOIN m_catalog_data c on b.id_catalog_data=c.id
    LEFT JOIN m_catalog d on c.id_catalog=d.id
    $order
    LIMIT $start,$limit";

    $q = mysql_query($sql);
    while ($row = mysql_fetch_assoc($q)) {
        //$row['type'] = $aroma_type[$row['type']];
        if ($row['type'] == 'N') {
            $row['type'] = $row['v'];
        } else {
            $row['type'] = $aroma_type[$row['type']] . ' ' . $row['v'] . ' мл';
        }
        $aItems[] = $row;
    }

    if ($_GET['sort'] == 'asc') {
        $sort['sort'] = 'desc';
    } else {
        $sort['sort'] = 'asc';
    }




    $aUrl = array_merge($_GET, $sort);

    foreach ($order_params as $key => $param) {
        $aSort[$param] = http_build_query(array_merge($_GET,$sort,['name'=>$param]));
    }

    $sm->assign('aSort', $aSort);
    $sm->assign('aItems', $aItems);
    $sm->assign('pages', $pages['pages']);

    // Список действий (actions). для футера
    $sm->assign('footer_id', array(
            'delete',
        ));

    $sm->assign('footer_names', array(
            'Удалить',
        ));
    $sm->assign('footer_active', 'null');

    // Подключение заголовка
    myPrintHeader("$cat_name", 'news.png');

    // Подключение навигации
    myPrintHeaderNav(
        array(
            array("NAME" => "Главная", "LINK" => "index.php", "ACTIVE" => "1"),
            array("NAME" => "Контент", "LINK" => "?main=content", "ACTIVE" => "1"),
            array("NAME" => "$cat_name", "LINK" => "?main=$main&module=$module&action=mod", "ACTIVE" => "1")
        )
    );

    $sm->display("modules/$module/table.tpl");
}

?>