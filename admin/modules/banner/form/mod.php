<?php
if ($_SESSION['admin'] == 'allow'){
	// Создание экземпляра шаблона
	$sm = new Smarty_Admin($valinorConfig['interface.themes']);

	// Определение переменных
	$cat_name = "Управление баннерами";
	$module = "banner";
	$main = "banner";

	// Проверка прав доступа
	//if (!MyIsValidUsers($_SESSION['user_id'], 'READ', 0, $module)){
	//	err_message("У вас нет прав на простмотр.");
	//}
	
	$query_group = mysql_query("SELECT * FROM m_banner_shablon ORDER by name");
	while ($content_group = mysql_fetch_array($query_group)){
		$item_group[] = array(
		'id' => $content_group['id'],
		'name' => $content_group['name']
		);
	}

	// Запрос к базе
	$query = mysql_query("SELECT * FROM m_banner ORDER BY id");
	$i = 1;
	while ($content = mysql_fetch_array($query)){
		$client = mysql_result(mysql_query("SELECT name FROM m_banner_client WHERE id='{$content['id_client']}'"),0,0);
		
		$item[] = array(
		'id' => $content['id'],
		'id_shablon' => $content['id_shablon'],
		'title' => $content['title'],
		'text' => $content['text'],
		'url' => $content['url'],
		'client' => $client,
		
		'clicks' => $content['clicks'],
		'hits' => $content['hits'],
		'clicks_end' => $content['clicks_end'],
		'ctr' => @intval(( $content['clicks'] / $content['hits']) * 100 ),
		
		'startDate' => $content['startDate'],
		'registerDate' => $content['registerDate'],
		'endDate' => $content['endDate'],
		'is_block' => ($content['is_block'] == 0) ? 'green.gif' : 'red.gif',
		'bgcolor' =>  ($i % 2 == 0) ? '#f2f2f2' : '#f8f8f8',
		'n' => $i
		);
		$i++;
	}
	mysql_free_result($query);
	
	
	
	/* -=========================================
		ВЫВОД И ФОРМИРОВНИЕ ДАННЫХ
	*/

	$sm->assign('item',$item);
	$sm->assign('item_group',$item_group);

	// Заголовок
	myPrintHeader($cat_name, 'nastroyka.png', array(
	array("IMG" => "add.gif", "ALT" => "Добавить", "LINK" => "?main=$main&module=$module&action=add")));

	// Навигация
	myPrintHeaderNav(array(
	array("NAME"=>"Главная", "LINK"=>"index.php", "ACTIVE"=>"1"),
	array("NAME"=>"Реклама", "LINK"=>"?main=banner", "ACTIVE"=>"1"),
	array("NAME"=>"$cat_name", "LINK"=>"?main=$main&module=$module&action=mod", "ACTIVE"=>"1")
	));

	// Действия
	$sm->assign('sel_id', array(
	'delete_item',
	'active',
	'flush_hits',
	'flush_clicks'
	));

	$sm->assign('sel_names', array(
	'Удалить',
	'Изменить активность',
	'Сбросить счетчик показов',
	'Сбросить счетчик посещений'
	));
	$sm->assign('is_active', 'null');

	// Вывод шаблона
	$sm->display("modules/{$_GET['module']}/table.tpl");
}
?>