<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User/Row/Item.php";

if ($_SESSION['admin'] == 'allow') {

    // Создание экземпляра шаблона
    $sm = new Smarty_Admin($valinorConfig['interface.themes']);

    $status = intval($_GET['status']);
    $date = $_GET['date'];

    // items
    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);

    $filename = "print_all_post.html";
    $filename_path = $_SERVER['DOCUMENT_ROOT'] . "/images/uploads/$filename";
    @unlink($filename_path);
    $order_items = null;
    $total_items = count($items);

    $w = 1;
    $total = 0;
    $total_orders = 0;

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach($orders as $content) {

        if ($total_items - 1 == $w) {
            $is_end = 1;
        } else {
            $is_end = 0;
        }

        $id_orders = $content->id;
        $id_client = $content->id_user;

        // позиции
        $q_sum = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$content->id}' ");
        $i = 0;
        $item = array();
        while ($c_sum = mysql_fetch_array($q_sum)) {
            $item[] = array(
                'kol' => $c_sum['kol'],
                'sum' => $c_sum['sum'],
                'title' => $c_sum['title'],
                'bgcolor' => ($i % 2 == 0) ? '#ffffff' : '#f8f8f8',
            );
            $i++;
        }

        if ($id_client != 0) {
            $client = User_Row_Item::collectUserInfo($id_client, $content);
            $client['fio'] = $client['fullName'];
            $client['address'] = $client['fullAddress'];
        } else {
            $client['region'] = '';
            $client['fio'] = $content->pFio;
            $client['phone'] = User_Row_Item::formatPhone($content->pPhone);
            $client['address'] = $content->pAddress;
        }

        foreach ($item as $sum) {
            $total += $sum['sum'] * $sum['kol'];
            $total_orders += 1 * $sum['kol'];
        }

        global $global_phone;
        $id = $content->getFullId();
        $content = $content->toArray();
        $content['id'] = $id;
        $sm->assign('is_end', $is_end);
        $sm->assign('orders', $content);
        $sm->assign('total', $total);
        $sm->assign('total_orders', $total_orders);
        $sm->assign('global_phone', $global_phone);
        $sm->assign('item', $item);
        $sm->assign('client', $client);
        $sm->assign('w', ($w % 2 == 0) ? '1' : '0');
        $sm->assign('i', $w);
        $sm->assign('total_items', $total_items);


        $order_items .= $sm->fetch("print_orders_all_post_item.tpl");
        $item = array();
        $total_orders = null;
        $w++;
    }

    $sm->assign('order_items', $order_items);

    file_put_contents($filename_path, $sm->fetch("print_orders_all_post.tpl"));

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    //$item['query'] = $sql;
    $item['total'] = $total;

    $json = Zend_Json::encode($item);
    echo $json;
}