<?php
/*
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
*/

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";

/**
 * Created by PhpStorm.
 * User: loram
 * Date: 24.06.14
 * Time: 19:33
 */
if ($_SESSION['admin'] == 'allow') {

    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);


    require_once 'Zend/Date.php';
    $zDate = new Zend_Date();
    $currDate = $zDate->toString("dd.MM.YYYY HH:mm");
    $currDateShort = $zDate->toString("YYYYMMdd");

    $filename = "parfumoff-boxberry.xml";
    $filename_path = $_SERVER["DOCUMENT_ROOT"] . "/images/uploads/$filename";
    @unlink($filename_path);

    $strAll = "<?xml version='1.0' encoding='UTF-8'?>\r\n";
    $strAll .= "<orders date=\"$currDate\">\r\n";
    $strAll .= "<OrdersDay day=\"$currDateShort\">\r\n";

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach($orders as $content) {

        $kol = '';
        $item_names = '';

        foreach ($content->getProducts() as $c_mod) {

            $order = mysql_fetch_array(
                mysql_query("SELECT * FROM m_catalog_data_order WHERE articul='{$c_mod['articul_catalog_data_order']}' ")
            );

            $title = format_text_out($c_mod['title']);
            $item_names .= $title . '; ';
            $kol .= $c_mod['kol'] . ';';
        }

        $item_names = trim($item_names);
        $item_names = rtrim($item_names, ';');
        $item_names = str_replace('&', ' ', $item_names);
        $kol = trim($kol);
        $kol = rtrim($kol, ';');

        $data = $content->getUser();
        $contact = "{$data['fullName']}, {$data['phone']}";
        $fio = $data['fullName'];
        $phone = substr($data['phone'], 1);
        $address = $data['fullAddress'];

        $isSelf = $content->isLogisticSelf() === true ? 1 : 2;
        $total = $content->getTotal(true);
        $logistic = $content->getLogistic();
        $delivery_code = $content->getDeliveryCode();
        $delivery_cost = $content->getDeliveryCost();

        $data['city'] = preg_replace('/г /', '', $data['city']);
        $id = $content->getFullId();

        $strAll .= "\t<order id=\"$id\">\r\n";
        $strAll .= "\t\t<price>{$total}</price>\r\n";
        $strAll .= "\t\t<payment_sum>{$total}</payment_sum>\r\n";
        $strAll .= "\t\t<delivery_sum>$delivery_cost</delivery_sum>\r\n";
        $strAll .= "\t\t<more_currency></more_currency>\r\n";
        $strAll .= "\t\t<declared_sum></declared_sum>\r\n";
        $strAll .= "\t\t<send_date>{$content->DateEnd}</send_date>\r\n";
        $strAll .= "\t\t<barcode></barcode>\r\n";
        $strAll .= "\t\t<torg12>{$content->id}</torg12>\r\n";
        $strAll .= "\t\t<vid>{$isSelf}</vid>\r\n";

        $strAll .= "\t\t<supplier>\r\n";
        $strAll .= "\t\t\t<name>ООО \"Смарт Студио\"</name>\r\n";
        $strAll .= "\t\t\t<name1>parfumoff.ru</name1>\r\n";
        $strAll .= "\t\t\t<inn>7709488180</inn>\r\n";
        $strAll .= "\t\t\t<kpp>770901001</kpp>\r\n";
        $strAll .= "\t\t</supplier>\r\n";

        if ($content->isLogisticSelf()) {

            $strAll .= "\t\t<kurdost>\r\n";
            $strAll .= "\t\t\t<index></index>\r\n";
            $strAll .= "\t\t\t<citi></citi>\r\n";
            $strAll .= "\t\t\t<addressp></addressp>\r\n";
            $strAll .= "\t\t\t<timesfrom1></timesfrom1>\r\n";
            $strAll .= "\t\t\t<timesto1></timesto1>\r\n";
            $strAll .= "\t\t\t<comentk></comentk>\r\n";
            $strAll .= "\t\t</kurdost>\r\n";
        } else {

            $strAll .= "\t\t<kurdost>\r\n";
            $strAll .= "\t\t\t<index>{$data['postcode']}</index>\r\n";
            $strAll .= "\t\t\t<citi>{$data['city']}</citi>\r\n";
            $strAll .= "\t\t\t<addressp>{$data['fullAddress']}</addressp>\r\n";
            $strAll .= "\t\t\t<timesfrom1>10:00</timesfrom1>\r\n";
            $strAll .= "\t\t\t<timesto1>18:00</timesto1>\r\n";
            $strAll .= "\t\t\t<comentk></comentk>\r\n";
            $strAll .= "\t\t</kurdost>\r\n";
        }

        $strAll .= "\t\t<shop>\r\n";
        $strAll .= "\t\t\t<name>{$delivery_code}</name>\r\n";
        $strAll .= "\t\t\t<name1>010</name1>\r\n";
        $strAll .= "\t\t</shop>\r\n";

        $strAll .= "\t\t<customer>\r\n";
        $strAll .= "\t\t\t<fio>{$data['fullName']}</fio>\r\n";
        $strAll .= "\t\t\t<phone>{$phone}</phone>\r\n";
        $strAll .= "\t\t\t<phone2></phone2>\r\n";
        $strAll .= "\t\t\t<email>{$data['email']}</email>\r\n";
        $strAll .= "\t\t\t<name></name>\r\n";
        $strAll .= "\t\t\t<address></address>\r\n";
        $strAll .= "\t\t\t<inn></inn>\r\n";
        $strAll .= "\t\t\t<kpp></kpp>\r\n";
        $strAll .= "\t\t\t<r_s></r_s>\r\n";
        $strAll .= "\t\t\t<bank></bank>\r\n";
        $strAll .= "\t\t\t<kor_s></kor_s>\r\n";
        $strAll .= "\t\t\t<bik></bik>\r\n";
        $strAll .= "\t\t</customer>\r\n";

        $strAll .= "\t\t<items>\r\n";

        foreach($content->getProducts() as $c_mod) {

            $mod_title = escapeHtml($c_mod['title']);

            $strAll .= "\t\t\t<item id=\"$c_mod[articul]\">\r\n";
            $strAll .= "\t\t\t\t<name>{$mod_title}</name>\r\n";
            $strAll .= "\t\t\t\t<UnitName>шт.</UnitName>\r\n";
            $strAll .= "\t\t\t\t<nds>0</nds>\r\n";
            $strAll .= "\t\t\t\t<price>{$c_mod['sum']}</price>\r\n";
            $strAll .= "\t\t\t\t<quantity>{$c_mod['kol']}</quantity>\r\n";
            $strAll .= "\t\t\t</item>\r\n";
        }
        $strAll .= "\t\t</items>\r\n";

        $strAll .= "\t\t<weights>\r\n";
        $strAll .= "\t\t\t<weight>900</weight>\r\n";
        $strAll .= "\t\t</weights>\r\n";

        $strAll .= "\t</order>\r\n";
    }

    $strAll .= "</OrdersDay>\r\n";
    $strAll .= "</orders>";
    //$strAll = iconv('WINDOWS-1251', 'UTF-8', $strAll);

    if ($fp = @fopen($filename_path, 'wb')) {
        @fwrite($fp, $strAll);
        @fclose($fp);
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    echo Zend_Json::encode(array('ok'));
}