<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User/Row/Item.php";

if ($_SESSION['admin'] == 'allow') {

    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);


    $filename = "parfumoff-logistics.csv";
    $filename_path = $_SERVER["DOCUMENT_ROOT"] . "/images/uploads/$filename";
    @unlink($filename_path);
    $csv = new CCSVData();

    $data = array( 'код услуги',
        'дата доставки',
        'время (с)',
        'время(до)',
        'номер заказа',
        'город отправления',
        'город назначения',
        'адрес в городе назначения',
        'примечание',
        'Получатель',
        'телефон',
        'вес',
        'объем',
        'стоимость заказа',
        'Оценочная стоимость');

    foreach ($data as &$col) {
        $col = iconv('utf-8','cp1251',$col);
    }


    $csv->SaveFile(
        "$filename_path",
        $data
    );

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach ($orders as $content) {

        $c_user = User_Row_Item::collectUserInfo($content->id_user, $content);

        if ($content->isLogisticSelf()) {
            $nko = "С24КО";
        } else {
            $nko = "24КО";
        }

        $city = $c_user['city'];
        $address = preg_replace(
            '/ , /',
            ' ',
            trim(
                trim(
                    implode(
                        ', ',
                        array(
                            $c_user['address_street'],
                            $c_user['address_house'] == null ? "" : "д " . $c_user['address_house'],
                            $c_user['address_corpus'] == null ? "" : "корп " . $c_user['address_corpus'],
                            $c_user['address_stroenie'] == null ? "" : "стр " . $c_user['address_stroenie'],
                            $c_user['address_flat'] == null ? "" :"кв " . $c_user['address_flat']
                        )
                    ),
                    " "
                ),
                ","
            )
        );
        $fio = $c_user['fullName'];
        $phone = $c_user['phone'];
        $logistic = $content->getLogistic();
        $delivery_code = $content->getDeliveryCode();;

        $price = $content->getTotal(true);

        $data = [
            $nko,
            $content->DateEnd,
            "",
            "",
            $content->getFullId(),
            "Москва",
            preg_replace('/г /', '', $city),
            $address,
            $delivery_code,
            $fio,
            $phone,
            "0,9",
            "",
            $price,
            $price,
        ];

        foreach ($data as &$col) {
            $col = iconv('utf-8','cp1251',$col);
        }

        $csv->SaveFile(
            $filename_path,
            $data
        );
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    echo Zend_Json::encode(array('ok'));
}