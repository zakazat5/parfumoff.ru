<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User/Row/Item.php";

if ($_SESSION['admin'] == 'allow') {

    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);

    $filename = "parfumoff-post.csv";
    $filename_path = $_SERVER["DOCUMENT_ROOT"] . "/images/uploads/$filename";
    @unlink($filename_path);

    $data = [
        'KodBan',
        'PostInd',
        'Oblast',
        'Rayon',
        'Gorod',
        'Adres',
        'RecName',
        'SumNP',
        'SumOC',
        'Телефон'
    ];

    foreach ($data as &$col) {
        $col = iconv('utf-8','cp1251',$col);
    }

    $csv = new CCSVData();
    $csv->SaveFile(
        "$filename_path",
        $data
    );

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach($orders as $content) {

        $data = User_Row_Item::collectUserInfo($content->id_user, $content);
        $price = $content->getTotal(true);

        $data = [
            $content->getFullId(),
            $data['postcode'],
            $data['region'],
            $data['rayon'],
            preg_replace('/г /', '', $data['city']),
            $data['address'],
            $data['fullName'],
            $price,
            $price,
            $data['phone']
        ];

        foreach($data as &$col){
            $col = iconv('utf-8','cp1251',$col);
        }

        $csv->SaveFile(
            $filename_path,
            $data
        );
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    echo Zend_Json::encode(array('ok'));
}