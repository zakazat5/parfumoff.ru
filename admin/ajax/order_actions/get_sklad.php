<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";

if ($_SESSION['admin'] == 'allow') {

    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);


    $filename = "note_sklad.csv";
    $filename_path = $_SERVER["DOCUMENT_ROOT"] . "/images/uploads/$filename";
    @unlink($filename_path);
    $csv = new CCSVData();

    $csv->SaveFile($filename_path, array('', ''));

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach ($orders as $content) {

        $q_mod = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$content->id}' ");
        while ($c_mod = mysql_fetch_array($q_mod)) {

            $order = mysql_fetch_array(
                mysql_query("SELECT * FROM m_catalog_data_order WHERE articul='{$c_mod['articul_catalog_data_order']}' ")
            );


            $data = array(
                $content->getFullId(),
                $c_mod['kol'],
                $order['articul'],
                format_text_out($c_mod['title']),
                $c_mod['sklad'],
                $c_mod['sum'],
                str_replace('.', ',', $order['price_usd1']),
                str_replace('.', ',', $order['price_usd2']),
                str_replace('.', ',', $order['price_usd3']),
                str_replace('.', ',', $order['price_usd4']),
                str_replace('.', ',', $order['price_usd5']),
                str_replace('.', ',', $order['price_usd6']),
                str_replace('.', ',', $order['price_usd7']),
                str_replace('.', ',', $order['price_usd8']),
                str_replace('.', ',', $order['price_usd9']),
                str_replace('.', ',', $order['price_usd10']),
                str_replace('.', ',', $order['price_usd11']),
                str_replace('.', ',', $order['price_usd12']),
                str_replace('.', ',', $order['price_usd13']),
                str_replace('.', ',', $order['price_usd14']),
                str_replace('.', ',', $order['price_usd15']),
                str_replace('.', ',', $order['price_usd16']),
                str_replace('.', ',', $order['price_usd17']),
                str_replace('.', ',', $order['price_usd18']),
                $content->pComment
            );

            foreach ($data as &$col) {
                $col = iconv('utf-8', 'cp1251', $col);
            }

            $csv->SaveFile(
                $filename_path,
                $data
            );

        }

    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    echo Zend_Json::encode(array('ok'));
}