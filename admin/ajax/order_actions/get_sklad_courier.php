<?php
/**
 * Отключили данный формат выгрузок
 */
die();

require_once $_SERVER['DOCUMENT_ROOT'] . "/app/Order.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/app/User/Row/Item.php";

if ($_SESSION['admin'] == 'allow') {

    $items = rtrim($_POST['items'], ",");
    $items_array = explode(',', $items);

    $filename = "parfumoff-ru-courier.xml";
    $filename_path = $_SERVER["DOCUMENT_ROOT"] . "/images/uploads/$filename";
    @unlink($filename_path);

    $strAll = "<?xml version='1.0' encoding='UTF-8'?>\r\n";
    $strAll .= "<orders>\r\n";

    $resourceOrders = new Order();
    $orders = $resourceOrders->getByArray($items_array);

    /**
     * @var $content Order_Row_Item
     */
    foreach($orders as $content) {

        $kol = '';
        $item_names = '';

        $q_mod = mysql_query("SELECT * FROM m_mag_OrdersSum WHERE id_order='{$content->id}' ");
        while ($c_mod = mysql_fetch_array($q_mod)) {

            $order = mysql_fetch_array(
                mysql_query("SELECT * FROM m_catalog_data_order WHERE articul='{$c_mod['articul_catalog_data_order']}' ")
            );

            $title = format_text_out($c_mod['title']);
            $item_names .= $title . '; ';
            $kol .= $c_mod['kol'] . ';';

        }

        $item_names = trim($item_names);
        $item_names = rtrim($item_names, ';');
        $item_names = str_replace('&', ' ', $item_names);
        $kol = trim($kol);
        $kol = rtrim($kol, ';');

        $data = User_Row_Item::collectUserInfo($content->id_user, $content);
        $contact = "{$data['fullName']}, {$data['phone']}";
        $total = $content->getTotal(true);
        $id = $content->getFullId();

        $strAll .= "\t<order>\r\n";
        $strAll .= "\t\t<date>{$content->DateEnd}</date>\r\n";
        $strAll .= "\t\t<number>{$id}</number>\r\n";
        $strAll .= "\t\t<order_type>Доставка с чеком Принципала</order_type>\r\n";
        $strAll .= "\t\t<item_names>{$item_names}</item_names>\r\n";
        $strAll .= "\t\t<item_counts>{$kol}</item_counts>\r\n";
        $strAll .= "\t\t<weight>0.9</weight>\r\n";
        $strAll .= "\t\t<address>{$data['fullAddress']}</address>\r\n";
        $strAll .= "\t\t<contact_info>{$contact}</contact_info>\r\n";
        $strAll .= "\t\t<summ>{$total}</summ>\r\n";
        $strAll .= "\t\t<delivery_type>плановая</delivery_type>\r\n";
        $strAll .= "\t</order>\r\n";
    }

    $strAll .= "</orders>";

    if ($fp = @fopen($filename_path, 'wb')) {
        @fwrite($fp, $strAll);
        @fclose($fp);
    }

    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");

    echo Zend_Json::encode(array('ok'));
}