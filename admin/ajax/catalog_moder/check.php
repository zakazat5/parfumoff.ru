<?php
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");

header("Content-type: text/x-json;charset=utf8");

/*
error_reporting(E_ALL);
ini_set("display_errors", 1);
*/


if ($_SESSION['admin'] == 'allow') {
    $id_cat = rtrim($_POST['id_cat']);
    if ($id_cat == 96) {
        $table_prefix = 'k_';
        $folder = 'catalog_k';
    } elseif ($id_cat == 82) {
        $table_prefix = 'm_';
        $folder = 'catalog';
    }

    $id_category = (int)$_POST['id_category'];
    if ($id_category == 0 && $id_cat != 82) $errors[] = "Поле категория не выбрано\n";

    $id_brand = (int)$_POST['id_brand'];
    if ($id_brand == 0) $errors[] ="Поле бренд не выбрано\n";

    $id_product = (int)$_POST['id_product'];
    //проверяем поля для нового продукта
    if ($id_product == 0) {
        $title = $_POST['title'];
        if ($title == '') $errors[] = "Поле имя продукта не заполнено\n";
        $url = $_POST['url'];
        if ($url == '') $errors[] ="Поле url продукта не заполнено\n";

        //проверка на существующий урл
        $sql = "SELECT * FROM m_catalog_data WHERE url='{$url}' and id_catalog={$id_brand}";

        $result = mysql_query($sql);
        if ($row = mysql_fetch_assoc($result)){
            $errors[] = "Такое поле url уже существует\n";
        }
    }

    $pol = $_POST['pol'];
    $meta_title = $_POST['meta_title'];
    $meta_title_1 = $_POST['meta_title_1'];
    $meta_description = $_POST['meta_description'];
    $text = $_POST['text'];
    $sort = $_POST['sort'];

    //по подификациям и складам
    $mod['sklad'] = (int)$_POST['sklad_id'];
    $mod['article_sklad'] = $_POST['article'];
    $mod['price_usd'] = $_POST['price_usd'];
    $mod['type'] = $_POST['type'];
    $mod['v'] = $_POST['v'];
    $mod['modification_id'] = $_POST['modification_id'];

    if ($mod['price_usd'] == '') $errors[] = "Поле 'цена' молификации не заполнено\n";
    if ($mod['article_sklad'] == '') $errors[] =  "Поле 'артикул' для модификации не заполнено\n";
    if ($mod['v'] == '' && $mod['modification_id'] == '') $errors[] = "Поле 'объем' для модификации не заполнено\n";

    /*
    if ($id_product>0){
        $sql = "SELECT * FROM {$table_prefix}catalog_data WHERE id=$id_product";
        $product = mysql_fetch_assoc(mysql_query($sql));

        if ($product['img']=='' && $_POST['image_path']==''){
            $errors[] = iconv("WINDOWS-1251", "UTF-8", "Фотография не загружена\n");
        }
    }else{
        if ($_POST['image_path']==''){
            $errors[] = iconv("WINDOWS-1251", "UTF-8", "Фотография не загружена\n");
        }
    }
    */

    if ($id_product>0){
        //проверка на существование модификации с такими данными
        $sql = "SELECT * FROM {$table_prefix}catalog_data_order WHERE type='{$mod['type']}' AND v='{$mod['v'] }' AND id_catalog_data={$id_product}";
        if ($row = mysql_fetch_assoc(mysql_query($sql))){
            $errors[] = "Уже существует модификация с существующим типом и объмом\n";
        }
    }



    if (count($errors) == 0) {
        //brend
        $brand_name = mysql_result(mysql_query("SELECT title FROM {$table_prefix}catalog WHERE id= $id_brand"),0,0);
        $type =  $aroma_type[$mod['type']];
        $v =  $mod['v'];
        $product_type =  ($id_cat==96)?'Косметика':'Парфюмерия';


        $text = '['.$pol.'] '.$brand_name.' '.$type.' '.$v.' мл <br>';
        $text.= 'Тип товара: '.$product_type.' <br> ';
        $pol = ($pol=='F')?'Женский':'Мужской';

        if ($id_category!=''){
            $category = mysql_fetch_assoc(mysql_query("SELECT title,pid FROM {$table_prefix}category WHERE id= $id_category"));
            $sCategory = $category['title'];
            if ($category['pid']>0){
                $category = mysql_fetch_assoc(mysql_query("SELECT title,pid FROM {$table_prefix}category WHERE id={$category['pid']}"));
                $sCategory = $category['title'].' -> '.$sCategory;
                if ($category['pid']>0){
                    $category = mysql_fetch_assoc(mysql_query("SELECT title,pid FROM {$table_prefix}category WHERE id={$category['pid']}"));
                    $sCategory = $category['title'].' -> '.$sCategory;
                }
            }
            $text.= 'Категория: '.$sCategory.' <br>';
        }
        $text.= 'Бренд: '.$brand_name.' <br> ';
        $text.= 'Аромат: '.$_POST['title'].' <br> ';
        $text.= 'Модификация+Объем : '.$type.' '.$v.' мл.<br>';
        $text.= 'Пол товара : '.$pol.'<br> ';
        echo $json = Zend_Json::encode(array('complete' => $text));
    }else {
        echo $json = Zend_Json::encode(array('errors' => implode('', $errors)));
    }



    //проверка данных
}
?>