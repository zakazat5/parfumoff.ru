<?php
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: text/x-json;charset=utf8");


/**
 * Created by JetBrains PhpStorm.
 * User: truth4oll
 * Date: 20.11.13
 * Time: 18:50
 * To change this template use File | Settings | File Templates.
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);


$dir = $_SERVER['DOCUMENT_ROOT'] . '/images/uploads/temp/';
@mkdir($dir);

//если загружаем по ссылке
if (isset($_POST['url'])) {

    //сохраняем картинку после редактирования редатором

    //$ext = explode('.', $_REQUEST['url']);
    //$filename = time() . '.' . end($ext);
    //file_put_contents($dir . $filename, $image_data);
    //$path = $dir . $filename;

    $id_catalog = $_GET['id'];


    $path = $_SERVER['DOCUMENT_ROOT'] . '/images/';
    $mkdir = 'uploads/catalog/' . $id_catalog;
    $mkdir_trumbs = $mkdir . '/trumbs/';
    $mkdir_small = $mkdir . '/small/';
    $mkdir_big = $mkdir . '/big/';


    if (!file_exists($path . $mkdir)) {
        mkdir($path . $mkdir);
        mkdir($path . $mkdir_trumbs);
        mkdir($path . $mkdir_small);
        mkdir($path . $mkdir_big);
    }

    $time = time();
    $img = upload_image($_POST['url'], $time, $path . $mkdir_small,
        $valinorConfig['catalog.big.w'], $valinorConfig['catalog.big.w']);

    $data = [
        'id_catalog_data' => $id_catalog,
        'img' => $img
    ];

    $db->insert('m_catalog_gallery', $data);


    /* $time = date('Y-m-d H:i:s');
     $sql = "UPDATE m_catalog_data SET `img`='{$img}', `img_update`=NOW() WHERE `id`={$id_catalog}";
     mysql_query($sql);*/

    /*//логирование

    $user_id = $_SESSION['user_id'];
    $target_id = $id_catalog;
    $id_cat = $id_cat;
    $log_text = 'Обновлена фотография';
    $data['action'] = 'Обновлена фотография';

    //логируем событие
    $log_text = mysql_real_escape_string($log_text);
    $sql = "INSERT INTO `m_catalog_history` (`user_id`,`target_id`,`text`,`id_cat`,`action`) VALUES ($user_id,$target_id,'{$log_text}',$id_cat,{$data['action']}";

    mysql_query($sql);*/


    echo $json = Zend_Json::encode(array('path' => str_replace($_SERVER['DOCUMENT_ROOT'], '', $path)));
} else {
    echo $json = Zend_Json::encode(array(
        'path' => '',
        'error' => 'Не удалось загрузить выбранное фото. Выберите другое.'
    ));


}


