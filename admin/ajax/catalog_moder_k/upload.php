<?php
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: text/x-json;charset=utf8");


/**
 * Created by JetBrains PhpStorm.
 * User: truth4oll
 * Date: 20.11.13
 * Time: 18:50
 * To change this template use File | Settings | File Templates.
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);


$dir = $_SERVER['DOCUMENT_ROOT'].'/images/uploads/temp/';
@mkdir($dir);

//если загружаем по ссылке
if (isset($_POST['url'])){
    if ($image_data = @file_get_contents($_REQUEST['url'])){
        //сохраняем картинку после редактирования редатором
        if (isset($_POST['oldPath'])){
            $aOldPath = explode('?',$_POST['oldPath']);
            $path= current($aOldPath);


            unlink($_SERVER['DOCUMENT_ROOT'].$path);
            file_put_contents($_SERVER['DOCUMENT_ROOT'].$path,$image_data);
            //сохраняем картинку из гугл
        }else{
            $ext = explode('.',$_REQUEST['url']);
            $filename = time().'.'.end($ext);
            file_put_contents( $dir.$filename,$image_data);
            $path= $dir.$filename;
        }

        echo $json = Zend_Json::encode(array('path'=>str_replace($_SERVER['DOCUMENT_ROOT'],'',$path)));
    }else{
        echo $json = Zend_Json::encode(array('path'=>'','error'=>'Не удалось загрузить выбранное фото. Выберите другое.'));
    }

}



if (isset($_FILES['file'])){
    $filename = $_FILES['file']['name'];
    if (move_uploaded_file( $_FILES['file']['tmp_name'], $dir . $filename)) {

    }
    $path= $dir.$filename;
    echo $json = Zend_Json::encode(array('path'=>str_replace($_SERVER['DOCUMENT_ROOT'],'',$path)));
}
