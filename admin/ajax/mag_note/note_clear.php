<?php
if ($_SESSION['admin'] == 'allow'){
	
	$_SESSION['orders_note'] = array();
	
	header("Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . "GMT" );
	header("Cache-Control: no-cache, must-revalidate" );
	header("Pragma: no-cache" );
	header("Content-type: text/x-json");

	$json = Zend_Json::encode($item);
	echo $json;
}
?>