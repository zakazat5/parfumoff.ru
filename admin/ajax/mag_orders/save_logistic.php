<?php
if ($_SESSION['admin'] == 'allow') {

    $id = $_POST['order_id'];
    if (isset($_POST['logistic'])) {
        $data['logistic'] = $_POST['logistic'];
    }
    if (isset($_POST['payment_date'])) {
        $data['payment_date'] = $_POST['payment_date'];
    }

    if (isset($_POST['TotalMoneyIncome'])) {
        $data['TotalMoneyIncome'] = round(str_replace(',', '.', $_POST['TotalMoneyIncome']), 2);
    }

    if (isset($_POST['DateMoneyIncome'])) {
        $data['DateMoneyIncome'] = $_POST['DateMoneyIncome'];
    }

    if (isset($_POST['delivery_cost_fact'])) {
        $data['delivery_cost_fact'] = round(str_replace(',', '.', $_POST['delivery_cost_fact']), 2);
    }


    //log start
    $order_before = $db->fetchRow("SELECT * FROM m_mag_Orders WHERE id=?", $id);
    //update
    if ($id > 0) {
        $data['DateEdit'] = new Zend_Db_Expr('NOW()');
        $update = $db->update('m_mag_Orders', $data, 'id =' . (int)$id);
        //add
    }


    //log stop& save
    $order_after = $db->fetchRow("SELECT * FROM m_mag_Orders WHERE id=?", $id);
    $result = array_diff_assoc($order_after, $order_before);

    $timestamp = $result['DateEdit'];
    $pid = $id;
    foreach ($result as $key => $diff) {
        switch ($key) {
            case 'logistic':
                $aBefore = array_flip(explode(';', $order_before['logistic']));
                $aAfter = array_flip(explode(';', $order_after['logistic']));
                $aIntersect = array_intersect_key($aBefore, $aAfter);
                foreach ($aIntersect as $key => $item) {
                    unset($aBefore[$key]);
                    unset($aAfter[$key]);
                }
                foreach ($aBefore as $key => $item) {
                    if ($key != '') {
                        $aLog[] = array(
                            'action' => 'logistic_delete',
                            'value_before' => $key,
                            'value_after' => '',
                        );
                    }

                }

                foreach ($aAfter as $key => $item) {
                    if ($key != '') {
                        $aLog[] = array(
                            'action' => 'logistic_add',
                            'value_before' => '',
                            'value_after' => $key,
                        );
                    }

                }

                break;
            case 'payment_date':
                $aLog[] = array(
                    'action' => 'payment_date',
                    'value_before' => $order_before['payment_date'],
                    'value_after' => $order_after['payment_date'],
                );
                break;
            case 'DateMoneyIncome':
                $aLog[] = array(
                    'action' => 'DateMoneyIncome',
                    'value_before' => $order_before['DateMoneyIncome'],
                    'value_after' => $order_after['DateMoneyIncome'],
                );
                break;
            case 'TotalMoneyIncome':
                $aLog[] = array(
                    'action' => 'TotalMoneyIncome',
                    'value_before' => $order_before['TotalMoneyIncome'],
                    'value_after' => $order_after['TotalMoneyIncome'],
                );
                break;
            case 'delivery_cost_fact':
                $aLog[] = array(
                    'action' => 'delivery_cost_fact',
                    'value_before' => $order_before['delivery_cost_fact'],
                    'value_after' => $order_after['delivery_cost_fact'],
                );
                break;
        }
    }

    foreach ($aLog as $log) {
        $log['user_id'] = $_SESSION['user_id'];
        $log['pid'] = $id;
        $log['module_name'] = 'mag_orders';
        $log['date'] = $timestamp;

        $db->insert('m_mag_Log', $log);
    }


    $select = $db->select()->from('m_mag_Orders')->where('id=?', $id);
    $result = $db->fetchRow($select);


    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
    header("Cache-Control: no-cache, must-revalidate");
    header("Pragma: no-cache");
    header("Content-type: text/x-json");


    $json = Zend_Json::encode($result);
    echo $json;
}
?>