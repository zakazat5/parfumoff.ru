$(function () {
    /**
     * Actions
     */
        //�������� ����� �����
    $('#addBrand').click(function () {
        addBrand();
        return false;
    });
    $('.addBlacklist').click(function () {
        addBlacklist($(this).attr('data-type'));
        return false;
    })


    $('#id_cat').change(function () {
        updateCatalogList();
        updateProductList();
        if ($('#id_cat').val() == 96) {
            $('#id_category').show();
        } else {
            $('#id_category').hide();
        }
        return false;
    })

    $('#id_category').change(function () {
        var id_category = $(this).val();

        //��������� ��������� ��� ���������
        $.ajax({
            type: "POST",
            dataType: "text",
            url: "?ajax=catalog_moder&action=category_opt&id_category=" + id_category,
            success: function (data) {
                if (data.length > 10) {
                    $('.form-category .text').html(data)
                    tb_show("HAI", "#TB_inline?height=400&amp;width=600&amp;inlineId=form-category&modal=true", null);
                }
            }});
        return false;
    })


    $('#id_brand').change(function () {
        var id_catalog = $('#id_brand').val();
        updateProductList(id_catalog);
        return false;
    })

    $('#id_product').change(function () {
        var id_product = $('#id_product').val();
        updateProductFields(id_product);
    })

    $('.bindMod').live('click', function () {
        $('.bindMod').show();
        $('.unbindMod').hide();


        $(this).hide();
        $(this).parent().find('a.unbindMod').show();
        id = $(this).parents('tr').attr('data-id');
        bindMod(id);
        return false;
    })

    $('.unbindMod').live('click', function () {
        $(this).hide();
        $(this).parent().find('a.bindMod').show();
        id = $(this).parents('tr').attr('data-id');
        unbindMod(id);
        return false;
    })


    /**
     * blacklist
     */
    $('#blacklist').click(function () {


    })


    $("#moder_form input[type=submit]").click(function () {
        $("input[type=button]", $(this).parents("form")).removeAttr("clicked");
        $(this).attr("clicked", "true");
    });

    $('#save_list').click(function () {
        save('save_list')
    })

    $('#save').click(function () {
        check('save')
    })

    $('.blacklist').click(function(){
        var id= $(this).attr('data-id');
        tb_show("HAI", "#TB_inline?height=190&amp;width=600&amp;inlineId=form-blacklist&modal=true", null);
        $('.form-blacklist input#blacklist').attr('checked',true);
        $('.form-blacklist .block_title').html('��������� � ������ ������')
        $('.form-blacklist #id_moder').val(id);
        return false;

    })


    $('.problem').click(function(){
        var id= $(this).attr('data-id');
        tb_show("HAI", "#TB_inline?height=190&amp;width=600&amp;inlineId=form-blacklist&modal=true", null);
        $('.form-blacklist input#problem').attr('checked',true);
        $('.form-blacklist .block_title').html('��������� � ���������� ������')
        $('.form-blacklist #id_moder').val(id)
        return false;
    })











})


function check(val) {
    //check form
    var form = $('#moder_form').serialize();


    $.ajax({
        type: "POST",
        dataType: "json",
        data: form,
        url: "?ajax=catalog_moder&action=check",
        success: function (data) {
            if (typeof data.errors !== 'undefined') {
                alert(data.errors);

                console.log(data.errors)
            } else {
                var id_product = data.complete
                var id_cat = $('#id_cat').val();
                if (id_cat == 82) {
                    var module = 'catalog'
                } else {
                    var module = 'catalog_k'
                }


                $('.form-check .text').html(data.complete)
                tb_show("HAI", "#TB_inline?height=400&amp;width=600&amp;inlineId=form-check&modal=true", null);
                /*

                 if (val=='save'){
                 window.location.href = 'index.php?main=content&module='+ module + '&action=edit&id_cat=' + id_cat+ '&id='+ id_product;
                 }

                 if (val=='save_list'){
                 window.location.href = 'index.php?main=mag&module=catalog_moder&action=mod';
                 }
                 console.log(val)
                 */
            }
        }});

}


function save(val) {
    var form = $('#moder_form').serialize();


    $.ajax({
        type: "POST",
        dataType: "json",
        data: form,
        url: "?ajax=catalog_moder&action=save",
        success: function (data) {
            if (typeof data.errors !== 'undefined') {
                alert(data.errors);

                console.log(data.errors)
            } else {
                var id_product = data.complete
                var id_cat = $('#id_cat').val();
                if (id_cat == 82) {
                    var module = 'catalog'
                } else {
                    var module = 'catalog_k'
                }

                //var val = $("input[type=button][clicked=true]").attr('name')

                if (val == 'save') {
                    window.location.href = 'index.php?main=content&module=' + module + '&action=edit&id_cat=' + id_cat + '&id=' + id_product;
                }

                if (val == 'save_list') {
                    window.location.href = 'index.php?main=mag&module=catalog_moder&action=mod';
                }
                console.log(val)
            }
        }});


}

/**
 * ����� ���������� ���������
 */
function addBrand() {
    $('.form-brand [name="id_cat"]').val($('#id_cat').val())
    tb_show("HAI", "#TB_inline?height=400&amp;width=600&amp;inlineId=form-brand&modal=true", null);
}

/**
 * ����� ���������� blacklist
 */
function addBlacklist(type) {
    tb_show("HAI", "#TB_inline?height=190&amp;width=600&amp;inlineId=form-blacklist&modal=true", null);
    $('.form-blacklist input#'+type).attr('checked',true);

    if (type=='blacklist'){
        $('.form-blacklist .block_title').html('��������� � ������ ������')
    }else{
        $('.form-blacklist .block_title').html('��������� � ���������� ������')
    }
}
/**
 * ��������� �����
 */
function saveFormCatalog() {
    if (!checkFormCatalog()) {
        alert('�������� �� ��� ���� ���� ���������');
        return false;
    }
    var id_cat = $('#id_cat').val();
    var data = $('.form-brand').serialize();

//send ajax for save new catalog
    $.ajax({
        type: "POST",
        data: data,
        dataType: "json",
        url: "?ajax=catalog_moder&action=catalogsave&id_cat=" + id_cat,
        success: function (data) {
            console.log(data)
            if (data.error) {
                alert(data.error)
            } else {
                updateCatalogList(data);
                closeFormCatalog();
            }
        }});


}

/**
 * blacklist
 * @returns {boolean}
 */
function saveFormBlacklist() {
    var error = false;

    //check radio button
    if ($('.form-blacklist input:radio:checked[name="block"]').val() == undefined) {
        error = true;
    }

    if (error) {
        alert('�������� �� ��� ���� ���� ���������');
        return false;
    }

    var form = $('.form-blacklist').serialize();


    $.ajax({
        type: "POST",
        dataType: "json",
        data: form,
        url: "?ajax=catalog_moder&action=saveblock",
        success: function (data) {
            if (typeof data.errors !== 'undefined') {
                alert(data.errors);

                console.log(data.errors)
            } else {
                var id_product = data.complete
                var id_cat = $('#id_cat').val();
                if (id_cat == 82) {
                    var module = 'catalog'
                } else {
                    var module = 'catalog_k'
                }

                var tr =$('#moder_'+$('.form-blacklist #id_moder').val());

                if (tr.length>0){
                    $('#moder_'+$('.form-blacklist #id_moder').val()).hide();
                    closeForm();

                }else{
                    window.location.href = 'index.php?main=mag&module=catalog_moder&action=mod';
                }


            }
        }});


    /*
     if (!checkFormCatalog()) {
     alert('�������� �� ��� ���� ���� ���������');
     return false;
     }
     var id_cat = $('#id_cat').val();
     var data = $('.form-brand').serialize();

     //send ajax for save new catalog
     $.ajax({
     type: "POST",
     data: data,
     dataType: "json",
     url: "?ajax=catalog_moder&action=catalogsave&id_cat=" + id_cat,
     success: function (data) {
     console.log(data)
     if (data.error) {
     alert(data.error)
     } else {
     updateCatalogList(data);
     closeFormCatalog();
     }
     }});
     */

}
/**
 * check form catalog fields
 * @returns {boolean}
 */
function checkFormCatalog() {
    if ($('.form-brand input[name="title"]').val() == '') {
        return false
    }
    ;
    if ($('.form-brand input[name="url"]').val() == '') {
        return false
    }
    ;
    return true;
}
/**
 * ������� �����
 */
function closeFormCatalog() {
    tb_remove();
    $('#form-brand').html('');
}

function closeForm() {
    tb_remove();
}

function saveCategoryOpt() {
    var aCategory_opt = [];
    $.each($('.form-category input:checked'), function (i, v) {
        aCategory_opt[i] = $(v).val();
    })
    $('#category_opt').val(aCategory_opt.join());
    $('#form-category').html('');
    tb_remove();
}

function closeFormCategory() {
    tb_remove();
    $('#form-category').html('');
}


/**
 * ���������� ��������� � ����� �������������� ���� ������
 */
function updateCatalogList(selected) {
    var id_cat = $('#id_cat').val();
    $.ajax({
        type: "GET",
        dataType: "text",
        url: "?ajax=catalog_moder&action=catalog&id_cat=" + id_cat,
        success: function (data) {
            console.log(selected)
            $('#id_brand').find('option[value!=0]')
                .remove()
                .end()
                .append(data);
            if (typeof selected != 'undefined') {
                $('#id_brand').val(selected)
            }
        }});
}

function updateProductList(id_catalog) {
    var id_cat = $('#id_cat').val();
    if (typeof id_catalog == 'undefined') {
        $('#id_product').find('option[value!=0]').remove().end();
        return false;
    }
    data = {id_catalog: id_catalog};
    $.ajax({
        type: "POST",
        data: data,
        dataType: "text",
        url: "?ajax=catalog_moder&action=product&id_cat=" + id_cat,
        success: function (data) {
            $('#id_product').find('option[value!=0]')
                .remove()
                .end()
                .append(data);
            $('#modifications table > tbody:first').find('tr').not(".not_remove").remove()
        }});
}


function updateProductFields(id_product) {
    var id_cat = $('#id_cat').val();

    //clear modifications table
    $('#modifications table > tbody:first').find('tr').not(".not_remove").remove()

    data = {id_product: id_product};
    $.ajax({
        type: "POST",
        data: data,
        dataType: "json",
        url: "?ajax=catalog_moder&action=product&id_cat=" + id_cat,
        success: function (data) {
            //��������� ����������� ����
            $('input[name="title"]').val(data.title);
            $('input[name="title_1"]').val(data.title_1);
            $('input[name="text"]').val(data.text);
            $('input[name="alt"]').val(data.alt);
            $('input[name="url"]').val(data.url);
            $('input[name="tag"]').val(data.tag);
            $('input[name="tagl"]').val(data.tagl);
            $('input[name="meta_title"]').val(data.meta_title);
            $('input[name="meta_title_1"]').val(data.meta_title_1);
            $('input[name="meta_description"]').val(data.meta_description);
            $('input[name="sort"]').val(data.sort);

            $('select[name="id_category"]').val(data.id_type);
            $('select[name="pol"]').val(data.pol);
            if (data.is_block != '') {
                $('input[name="is_block"]').attr('checked', true)
            } else {
                $('input[name="is_block"]').attr('checked', false)
            }
            if (data.is_main != '') {
                $('input[name="is_main"]').attr('checked', true)
            } else {
                $('input[name="is_main"]').attr('checked', false)
            }
            if (data.is_subs != '') {
                $('input[name="is_subs"]').attr('checked', true)
            } else {
                $('input[name="is_subs"]').attr('checked', false)
            }
            if (data.is_sale != '') {
                $('input[name="is_sale"]').attr('checked', true)
            } else {
                $('input[name="is_sale"]').attr('checked', false)
            }

            var sklad_id = $('.sklad_id').val()
            //modifications
            $.each(data.mod, function (i, v) {
                a = v
                template = $('#mod_item_template').clone();
                template.attr('data-id', v.id);
                template.find('.modification_id').val(v.id)
                template.find('.articul input').val(v.articul)

                //article|article2
                article_name = 'article' + ((sklad_id > 1) ? sklad_id : '');
                price_name = 'price_usd' + sklad_id;


                //���� �������
                //save old value
                template.find('.article input').attr('data-value', (v[article_name]));
                template.find('.article input').val(v[article_name])
                template.find('.price_usd input').attr('data-value', (v[price_name]));
                template.find('.price_usd input').val(v[price_name])


                template.find('.type select').val(v.type)
                template.find('.v input').val(v.v)
                template.addClass('mod_item');
                template.removeAttr('id');
                template.toggle();
                template.find('input').attr('disabled', true);
                template.find('select').attr('disabled', true);

                //set values

                //insert

                $('#modifications table > tbody:last').append(template);
            })


        }});
}


function bindMod(id) {
    //�������� �������� �����
    $.each($('tr[data-id] input[data-value]'), function (i, value) {
        $(value).val($(value).attr('data-value'))
    })

    var article = $('#modifications .not_remove.new').find('input[name="article"]').val();
    var price = $('#modifications .not_remove.new').find('input[name="price_usd"]').val();
    //�������������� ������ � ����� ������������
    $('#modifications .not_remove.new input').attr('disabled', true)
    $('#modifications .not_remove.new select').attr('disabled', true)
    //������ ����� �����������
    //$('#modifications .not_remove.new').hide();
    //block isset modification
    $('tr[data-id]').find('input[name="article"]').attr('disabled', true)
    //$('tr[data-id]').find('input[name="article"]').val(article)

    $('tr[data-id]').find('input[name="price_usd"]').attr('disabled', true)
    //$('tr[data-id]').find('input[name="price_usd"]').val(price)

    $('tr[data-id]').find('input[name="modification_id"]').attr('disabled', true)

    //unblock isset modification

    $('tr[data-id="' + id + '"]').find('input[name="article"]').attr('disabled', false)
    //recovery article
    $('tr[data-id="' + id + '"] input[name="article"]').attr('data-article', $('tr[data-id="' + id + '"] input[name="article"]').val())

    $('tr[data-id="' + id + '"]').find('input[name="article"]').val(article)

    $('tr[data-id="' + id + '"]').find('input[name="price_usd"]').attr('disabled', false)
    $('tr[data-id="' + id + '"]').find('input[name="price_usd"]').val(price)

    $('tr[data-id="' + id + '"]').find('input[name="modification_id"]').attr('disabled', false)

}

function unbindMod(id) {

    $.each($('tr[data-id] input[data-value]'), function (i, value) {
        $(value).val($(value).attr('data-value'))
    })


    var article = $('#modifications .not_remove.new').find('input[name="article"]').val();
    var price = $('#modifications .not_remove.new').find('input[name="price_usd"]').val();
    //������������ ����� �����������
    $('#modifications .not_remove.new input[name]').attr('disabled', false)
    $('#modifications .not_remove.new select[name]').attr('disabled', false)
    //������ ����� �����������
    $('#modifications .not_remove.new').show();

    //block isset modification
    $('tr[data-id]').find('input[name="article"]').attr('disabled', true)
    $('tr[data-id]').find('input[name="price_usd"]').attr('disabled', true)
    $('tr[data-id]').find('input[name="modification_id"]').attr('disabled', true)


    //$('tr[data-id]').find('input[name="article"]').val(article)


    //$('tr[data-id]').find('input[name="price_usd"]').val(price)


}


/**
 * ������� ��������� ���������� ���� ������
 */
function changeCatalogType() {
    //��������� ������

    //
}


function saveAction() {


}

